<?php
/**
 * The Header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?><!DOCTYPE html>
<!-- saved from url=(0039)http://getbootstrap.com/examples/theme/ -->
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="http://getbootstrap.com/assets/ico/favicon.ico">

    <title>Peterlouis</title>

    <!-- Bootstrap core CSS -->
    <!-- Bootstrap theme -->

    <!-- Custom styles for this template -->
    <link href="<?php echo get_template_directory_uri();?>/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri();?>/css/mont/stylesheet.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri();?>/css/font-awesome-4.2.0/font-awesome-4.2.0/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri();?>/css/font-awesome-4.2.0/font-awesome-4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri();?>/css/alegr/stylesheet.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  <style id="holderjs-style" type="text/css"></style><?php wp_head(); ?></head>

  <body role="document">

    <!-- Fixed navbar -->
    	
        <div class="row rwbdy">
        	<div class="header">
            	<div class="container">
                	<div class="main_box">
                    	<div class="row rwbdy">
                        	<div class="col-md-2 padbdy">
                            	<div class="logo"><a href="#"><img src="<?php echo get_template_directory_uri();?>/images/logo.png" class="img-responsive"></a></div>
                            </div>
                            <div class="col-md-8 padbdy">
                            	<div class="navbar navbar-inverse navbar" role="navigation">
                                  
                                    <div class="navbar-header">
                                      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                      </button>

                                    </div>
                                    <div class="navbar-collapse collapse">
                                      <ul class="nav navbar-nav">
                                        <li class="active"><a href="#">Home</a></li>
                                        <li><a href="#about">ABOUT TA-65</a></li>
                                        <li><a href="#contact">FAQ</a></li>
                                         <li><a href="#contact">VIDEO</a></li>
                                          <li><a href="#contact">ORDER NOW</a></li>
                                          <li><a href="#contact">CONTACT</a></li>
                                        
                                      </ul>
                                    </div><!--/.nav-collapse -->
                                  
                                </div> 
                            </div>
                            <div class="col-md-2">
                            	<div class="social_box">
                                	<ul class="soc_menu">
                                    	<li style="border-left:1px solid #e6e7e8;"><a href="#">&#xf09a;</a></li>
                                        <li><a href="#">&#xf099;</a></li>
                                        <li><a href="#">&#xf09e;</a></li>
                                        <li><a href="#">&#xf0d5;</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>