<?php
/**
 * Template Name: FAQ Page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

 <div class="row rwbdy">
        	<div class="container">
				 <div class="row rwbdy">
				 	<div class="unit_box">
					<?php
						if ( is_front_page() && twentyfourteen_has_featured_posts() ) {
							// Include the featured content template.
							get_template_part( 'featured-content' );
						}
					?>

        <?php

									$args = array(

									 'cat' => '8',

									 'order'    => 'ASC'

									);

									// The Query

									query_posts( $args );

									// The Loop

									while ( have_posts() ) : the_post();
									
										echo'
                    							<div class="row rwbdy">
                									<h2 class="units_text">';
														the_title();
														
										echo'		</h2>
                            						<div class="onecapsule_txt">';
														the_content();
														
										echo'		</div>
                       	 						</div>
											';
										
										
										endwhile;

									// Reset Query

									wp_reset_query();
				?>
		

			

		</div><!-- #content -->
	</div><!-- #primary -->
</div>	
	<?php //get_sidebar( 'content' ); ?>
</div><!-- #main-content -->
<div class="row rwbdy">
	<div class="greybox">
	<div class="container">
		<div class="row rwbdy">
             <div class="produ_txt">
                    	<?php if(is_page('video')||is_page('faq')||is_page('about-ta-65')){
		echo do_shortcode('[widgets_on_pages id=2]');
	} ?></div>
	</div>
		</div>
	</div>
</div>
<?php
//get_sidebar();
get_footer();
