<table width="720" cellspacing="0" cellpadding="0" style=" background-repeat:repeat-x; max-width: 720px;">
<tr>
<td colspan="3">
<span><? include($template_include_file);?></span>
</td>
</tr>
<tr>
<td colspan="2"  style="background-image: url('./images/hdny_gradientB.png') ;">
<p style="color: #000000; font-size: 16px; margin-left: 1em; margin-right: 1em;">

<strong>NEW: HAIRDREAMS JEWELS</strong>
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
In tune with the trendy "Swarovski-Look"
 and for that extra attention: new Hairdreams Jewels
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
Extra-large sparkling Swarovski "Jewels", blended into your hair with ultra-fine and color-matched threads.
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
Jewels are attached with small clips and can easily be put in or taken out of your hair at any time.
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">

Hairdreams Jewels are available in 8 different shapes and colors, which can be worn individually or mixed and matched as you please.
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
Available now at <a href="http://www.peterlouissalon.com">Peterlouissalon</a>.
</p>
</td>
<td align="right" style=" vertical-align: bottom; background-image: url('./images/919.jpg') ;">
<img  src="/images/919.jpg"/>
</td>
</tr>
<tr>
<td colspan="3" style=" background-image: url('./images/hdny_bottom.png') ;">
<img src="./images/hdny_bottom.png"/>
</td>
</tr>
</table>
