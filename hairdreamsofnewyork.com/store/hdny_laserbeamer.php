<table width="720" cellspacing="0" cellpadding="0" style=" background-repeat:repeat-x; max-width: 720px;">
<tr>
<td colspan="3">
<span><? include($template_include_file);?></span>
</td>
</tr>
<tr>
<td colspan="2"  style="background-image: url('./images/hdny_gradientB.png') ;">
<p style="color: #000000; font-size: 16px; margin-left: 1em; margin-right: 1em;">
<strong>
THE LASERBEAMER SYSTEM</strong>
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
The Laserbeamer System is a particularly innovative method of attaching hair extensions. Here up to eight strands are simultaneously bonded with your own hair using a special device. In this way hair extensions can be applied quickly and efficiently– and also at a lower cost than ever before. This method is especially suitable for effect strands and simple, inexpensive hair extensions and thickenings. The durability of the strands with this method is around 4 months.</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
<table border="0" cellpadding="3" cellspacing="3" width="100"><tbody><tr><td align="center"><a href="#" onmouseover="document.getElementById('gal40').src='/images/2350.jpg';" onmouseout="document.getElementById('gal40').src='/images/313.jpg';"><img src="/images/313.jpg" onclick="$('#image').attr('src', '/images/2351.jpg');" name="gal40" id="gal40" border="0"></a><br>step 1</td><td align="center"><a href="#" onmouseover="document.getElementById('gal28').src='/images/2354.jpg';" onmouseout="document.getElementById('gal28').src='/images/2355.jpg';"><img src="/images/2355.jpg" onclick="$('#image').attr('src', '/images/2361.jpg');" name="gal28" id="gal28" border="0"></a><br>step 2</td><td align="center"><a href="#" onmouseover="document.getElementById('gal29').src='/images/2357.jpg';" onmouseout="document.getElementById('gal29').src='/images/2358.jpg';"><img src="/images/2358.jpg" onclick="$('#image').attr('src', '/images/2359.jpg');" name="gal29" id="gal29" border="0"></a><br>step 3</td></tr></table></p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
<? include('store/hdny_nav_how_it_works.php'); ?>
</p>
</td>
<td align="right" style=" vertical-align: bottom; background-image: url('./images/198.jpg') ;">
<img width="350" height="450" id="image" src="/images/198.jpg"/>
</td>
</tr>
<tr>
<td colspan="3" style=" background-image: url('./images/hdny_bottom.png') ;">
<img src="./images/hdny_bottom.png"/>
</td>
</tr>
</table>
