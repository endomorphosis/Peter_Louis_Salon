<table width="720" cellspacing="0" cellpadding="0" style=" background-repeat:repeat-x; max-width: 720px;">
<tr>
<td colspan="3">
<span><? include($template_include_file);?></span>
</td>
</tr>
<tr>
<td colspan="2"  style="background-image: url('./images/hdny_gradientB.png') ;">
<p style="color: #000000; font-size: 16px; margin-left: 1em; margin-right: 1em;">
<strong>
HAIRDREAMS SPECIAL QUALITY</strong>
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
Hairdreams Special Hair is made from especially high-grade human hair of European type and with natural color shades ranging from black to blond.
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
This hair is hand-selected and hand-sorted according to color. The hair may be adjusted by max. one shade to match the color ring.

</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
CHARACTERISTICS:
</p>
<p style="color: #000000; font-size: 12px; margin-left: 1em; margin-right: 1em;">
<ul><li>::
	Best possible Hairdreams hair quality with natural shine and unsurpassed wearing comfort - the "Non-Plus-Ultra"!
</li><li>::
	Suitable for all types of hair extensions - even for the highest of expectations!
</li><li>:: 	Minimally tapered; max. 3-4 cm
</li><li>::
	Wearing period of 6 months up to 1.5 years with rebonding!
</li></ul>
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
The HAIRDREAMS SPECIAL QUALITY can be identified by the SPECIAL QUALITY guarantee card. 
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
<? include('store/hdny_nav_qualities.php'); ?>
</p>
</td>
<td align="right" style=" vertical-align: bottom; background-image: url('./images/2813.jpg') ;">
<img width="350" height="450" src="/images/2813.jpg"/>
</td>
</tr>
<tr>
<td colspan="3" style=" background-image: url('./images/hdny_bottom.png') ;">
<img src="./images/hdny_bottom.png"/>
</td>
</tr>
</table>
