<table width="720" cellspacing="0" cellpadding="0" style=" background-repeat:repeat-x; max-width: 720px;">
<tr>
<td colspan="3">
<span><? include($template_include_file);?></span>
</td>
</tr>
<tr>
<td colspan="2"  style="background-image: url('./images/hdny_gradientB.png') ;">

<p style="color: #000000; font-size: 16px; margin-left: 1em; margin-right: 1em;">
<strong>FINE HAIR</strong>
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
<strong>Problem:</strong> Hair breakage and thin hair
When the protective cuticle layer of the hair is damaged or destroyed, then the hair gets thinner and thinner, and even the slightest strain can lead to the hair breaking off completely.
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
<strong>The causes:</strong> frequent, often incorrect washing, combing and brushing of the hair - especially when wet, since wet hair is especially delicate. But heat-styling with flat iron and blow-dryer, as well as frequent chemical color treatments put an enormous strain on the hair. Add to that environmental influences such as sun, salt water, chlorine, and even diminished hair strength due to increasing age.
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
<strong>The consequence:</strong> The hair keeps getting thinner and duller - especially in the lengths and ends. Attractive hairstyles get more and more difficult to achieve.
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
<strong>The solution:</strong> The existing hair is gently supplemented and replenished with high-grade, hand-selected human hair. This hair matches the own hair in color, length and structure, allowing for an absolutely natural look. The integration process is completely gentle and non-damaging; wearing it feels just like one's own hair, without any noteworthy lifestyle limitations. The additional volume looks completely natural and can’t be distinguished from own hair.
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
<? include('store/hdny_nav_fine_hair.php'); ?>
</p>
</td>
<td align="right" style=" background-image: url('./images/hdny_thickeningb.png') ;">
<img  src="/images/hdny_thickeningB.png"/>
</td>
</tr>
<tr>
<td colspan="3" style=" background-image: url('./images/hdny_bottom.png') ;">
<img src="./images/hdny_bottom.png"/>
</td>
</tr>
</table>
