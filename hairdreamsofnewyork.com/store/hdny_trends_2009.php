<table width="720" cellspacing="0" cellpadding="0" style=" background-repeat:repeat-x; max-width: 720px;">
<tr>
<td colspan="3">
<span><? include($template_include_file);?></span>
</td>
</tr>
<tr>
<td colspan="2"  style="background-image: url('./images/hdny_gradientB.png') ;">

<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;"><strong>
GET GORGEOUS WITH HAIRDREAMS NEW STYLES FOR 2009</strong><br/>
Glamorous Headlining Fashion & Hair Looks from Hairdreams' Trend Scouts
</p>
<p style="color: #000000; font-size: 12px; margin-left: 1em; margin-right: 1em;">
<strong>LOS ANGELES, Calif. (February 2009)</strong> Keeping up with the hottest styles in fashion and hair can be challenging. Especially if you cut off your locks to get last season's ultra short bob or last year's chic Posh 'do. Are you having issues growing out your mane to get the new look this year? Don't fret, the experts at Hairdreams are here to help. The leading European hair extension system can instantly lengthen, volumize or just add more style to help obtain the hottest new looks of the season.
</p>
<p style="color: #000000; font-size: 12px; margin-left: 1em; margin-right: 1em;">
From glamorous locks, colorful strands to glitzy styles, Hairdreams outlines four trends that you will surely be looking for this year: it's all about volume to capture the longer lengths, lift curls and bring hair more texture and fullness.
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
<? include('store/hdny_nav_trends_2009.php'); ?>
</p>
</td>
<td align="right" style=" vertical-align: bottom; background-image: url('./images/3204.jpg') ;">
<img  src="/images/3204.jpg"/>
</td>
</tr>
<tr>
<td colspan="3" style=" background-image: url('./images/hdny_bottom.png') ;">
<img src="./images/hdny_bottom.png"/>
</td>
</tr>
</table>
