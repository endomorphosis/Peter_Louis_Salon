<table width="720" cellspacing="0" cellpadding="0" style=" background-repeat:repeat-x; max-width: 720px;">
<tr>
<td colspan="3">
<span><? include($template_include_file);?></span>
</td>
</tr>
<tr>
<td style="background-image: url('./images/hdny_gradientB.png') ;" >
<img id="large" src="" />
</td>
<td colspan="2" style="background-image: url('./images/hdny_gradientB.png') ;" >
<table>
<tr>
<td colspan="3">
<div id="title"><p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
<strong>Hair Thickening with Hairdreams HighLines - how it works</strong></p>
</div><p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;"><strong>
<div id="step">
</div></strong></p>
</td>
</tr>
<td colspan="3">
<div id="info"><p style="color: #000000; font-size: 12px; margin-left: 1em; margin-right: 1em;">
The HighLines are permanently attached in the crown area with use of the Bonding Ring Technique. The client’s own hair is pulled through the net and is naturally blended with the Hairdreams hair. The HighLines System makes possible any hair style requiring body and volume on top of the head. One wearing period with HighLines is approx. 6-8 months.</p>
</div>
</td>
<tr>
<td>
<img id="2985" onload="swap(this)" onclick="swap(this);" name="2983" src="./images/2984.jpg" name="step1" text="Step 1: Consultation" class="thumb"/>
</td>
<td>
<img src="./images/2987.jpg" id="2988" onclick="swap(this);"  name="2986" text="Step 2: Application" class="thumb"/>
</td>
<td>
<img src="./images/2990.jpg" id="2991" onclick="swap(this);" name="2989" text="Step 3: Integrate client's own hair" class="thumb"/>
</td>
</tr>
<tr>
<td>
<img src="./images/2993.jpg" id="2994" onclick="swap(this);" name="2992" text="Step 4: Styling" class="thumb"/>
</td>
<td>
<img src="./images/2996.jpg" id="2997" onclick="swap(this);" name="2995" text="IMAGE" class="thumb"/>
</td>
<td>

</td>
</tr>
<tr>
</table>
</tr>
</td>
</tr>
<tr>
<td colspan="3" style=" background-image: url('./images/hdny_bottom.png') ;">
<img src="./images/hdny_bottom.png"/>
</td>
</tr>
</table>
<script type="text/javascript">
function swap(some_var)
{ $("#large").attr("src" , "./images/" + $(some_var).attr("id") + ".jpg"); 
  $("#step").text($(some_var).attr("text"));
}
var src = '';
    $("img.thumb").hover(
      function () {
        $(this).attr('swap', $(this).attr('src'));
	$(this).attr('src', './images/' + $(this).attr('name') + '.jpg');
      }, 
      function () {
	$(this).attr('src', $(this).attr('swap'));
      }
    );


</script>