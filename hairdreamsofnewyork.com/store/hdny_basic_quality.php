<table width="720" cellspacing="0" cellpadding="0" style=" background-repeat:repeat-x; max-width: 720px;">
<tr>
<td colspan="3">
<span><? include($template_include_file);?></span>
</td>
</tr>
<tr>
<td colspan="2"  style="background-image: url('./images/hdny_gradientB.png') ;">
<p style="color: #000000; font-size: 16px; margin-left: 1em; margin-right: 1em;">
<strong>
HAIRDREAMS BASIC QUALITY
</strong>
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
Hairdreams Basic Hair is made from selected dark human hair.
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
The hair is lightened using an elaborate 24-hour process and then colored until the desired shade is attained.
CHARACTERISTICS:

</p>
<p style="color: #000000; font-size: 12px; margin-left: 1em; margin-right: 1em;">
<ul>
<li>::	Top-quality radiant hair with great elasticity, a natural feel as well as great and natural wearing comfort.
</li><li>:: 	Suitable for all types of hair extensions
</li><li>:: 	Tapered by max. 7-10cm
</li><li>:: 	Wearing period of up to 6 months
</li></ul>
</p>
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
The HAIRDREAMS BASIC QUALITY can be identified by the BASIC QUALITY guarantee card. 
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
<? include('store/hdny_nav_qualities.php'); ?>
</p>
</td>
<td align="right" style=" vertical-align: bottom; background-image: url('./images/2814.jpg') ;">
<img width="350" height="450" src="/images/2814.jpg"/>
</td>
</tr>
<tr>
<td colspan="3" style=" background-image: url('./images/hdny_bottom.png') ;">
<img src="./images/hdny_bottom.png"/>
</td>
</tr>
</table>
