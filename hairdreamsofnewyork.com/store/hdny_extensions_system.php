<table width="720" cellspacing="0" cellpadding="0" style=" background-repeat:repeat-x; max-width: 720px;">
<tr>
<td colspan="3">
<span><? include($template_include_file);?></span>
</td>
</tr>
<tr>
<td style="background-image: url('./images/hdny_gradientB.png') ;" >
<img id="large" src="" />
</td>
<td colspan="2" style="background-image: url('./images/hdny_gradientB.png') ;" >
<table>
<tr>
<td colspan="3">
<div id="title"><p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
<strong>Hair Thickening with Hairdreams strands - how it works </strong></p>
</div><p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;"><strong>
<div id="step">
</div></strong></p>
</td>
</tr>
<td colspan="3">
<div id="info"><p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
With Hair Thickening with Hairdreams strands, individual strands are manually applied exactly where they are needed. The client's own hair is gently supplemented with human hair strands of the same length. 4 different strand sizes allow for an optimal match with the client's own hair. One wearing period for Hair Thickening with strands is approx. 4 months.</p>
</div>
</td>
<tr>
<td>
<img id="2970" onload="swap(this)" onclick="swap(this);" name="2968" src="./images/2969.jpg" name="step1" text="Step 1: Section hair" class="thumb"/>
</td>
<td>
<img src="./images/2972.jpg" id="2973" onclick="swap(this);"  name="2971" text="Step 2: Position Selector" class="thumb"/>
</td>
<td>
<img src="./images/2975.jpg" id="2976" onclick="swap(this);" name="2974" text="Step 3: Place strand on client's hair" class="thumb"/>
</td>
</tr>
<tr>
<td>
<img src="./images/2978.jpg" id="2979" onclick="swap(this);" name="2977" text="Step 4: Bond the strand" class="thumb"/>
</td>
<td>
<img src="./images/2981.jpg" id="2982" onclick="swap(this);" name="2980" text="IMAGE" class="thumb"/>
</td>
<td>

</td>
</tr>
<tr>
</table>
</tr>
</td>
</tr>
<tr>
<td colspan="3" style=" background-image: url('./images/hdny_bottom.png') ;">
<img src="./images/hdny_bottom.png"/>
</td>
</tr>
</table>
<script type="text/javascript">
function swap(some_var)
{ $("#large").attr("src" , "./images/" + $(some_var).attr("id") + ".jpg"); 
  $("#step").text($(some_var).attr("text"));
}
var src = '';
    $("img.thumb").hover(
      function () {
        $(this).attr('swap', $(this).attr('src'));
	$(this).attr('src', './images/' + $(this).attr('name') + '.jpg');
      }, 
      function () {
	$(this).attr('src', $(this).attr('swap'));
      }
    );


</script>