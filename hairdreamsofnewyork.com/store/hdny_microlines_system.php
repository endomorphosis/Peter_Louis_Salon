<table width="720" cellspacing="0" cellpadding="0" style=" background-repeat:repeat-x; max-width: 720px;">
<tr>
<td colspan="3">
<span><? include($template_include_file);?></span>
</td>
</tr>
<tr>
<td style="background-image: url('./images/hdny_gradientB.png') ;" >
<img id="large" src="" />
</td>
<td colspan="2" style="background-image: url('./images/hdny_gradientB.png') ;" >
<table>
<tr>
<td colspan="3">
<div id="title"><p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
<strong>Hair Thickening with Hairdreams MicroLines - how it works </strong></p>
</div><p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;"><strong>
<div id="step">
</div></strong></p>
</td>
</tr>
<td colspan="3">
<div id="info"><p style="color: #000000; font-size: 12px; margin-left: 1em; margin-right: 1em;">
The MicroLines method integrates a micro-fine, virtually invisible hair net into the client's own hair. With the use of a so-called "Bonding Ring" the client's own hair is gently integrated for a very durable result.
The MicroLines has high-grade human hair attached to it, which match the client's own hair in terms of color and length, and which gives it natural body and volume in the crown area. One wearing period with a MicroLines is approx. 6-8 months</p>
</div>
</td>
<tr>
<td>
<img id="3000" onload="swap(this)" onclick="swap(this);" name="2998" src="./images/2999.jpg" name="step1" text="Step 1: Consultation" class="thumb"/>
</td>
<td>
<img src="./images/3003.jpg" id="3004" onclick="swap(this);"  name="3002" text="Step 2: Application" class="thumb"/>
</td>
<td>
<img src="./images/3006.jpg" id="3007" onclick="swap(this);" name="3005" text="Step 3: Integrate client's own hair" class="thumb"/>
</td>
</tr>
<tr>
<td>
<img src="./images/3009.jpg" id="3010" onclick="swap(this);" name="3008" text="Step 4: Styling" class="thumb"/>
</td>
<td>
<img src="./images/3012.jpg" id="3013" onclick="swap(this);" name="3011" text="IMAGE" class="thumb"/>
</td>
<td>

</td>
</tr>
<tr>
</table>
</tr>
</td>
</tr>
<tr>
<td colspan="3" style=" background-image: url('./images/hdny_bottom.png') ;">
<img src="./images/hdny_bottom.png"/>
</td>
</tr>
</table>
<script type="text/javascript">
function swap(some_var)
{ $("#large").attr("src" , "./images/" + $(some_var).attr("id") + ".jpg"); 
  $("#step").text($(some_var).attr("text"));
}
var src = '';
    $("img.thumb").hover(
      function () {
        $(this).attr('swap', $(this).attr('src'));
	$(this).attr('src', './images/' + $(this).attr('name') + '.jpg');
      }, 
      function () {
	$(this).attr('src', $(this).attr('swap'));
      }
    );


</script>