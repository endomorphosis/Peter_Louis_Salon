<table width="720" cellspacing="0" cellpadding="0" style=" background-repeat:repeat-x; max-width: 720px;">
<tr>
<td colspan="3">
<span><? include($template_include_file);?></span>
</td>
</tr>
<tr>
<td colspan="2"  style="background-image: url('./images/hdny_gradientB.png') ;">
<p style="color: #000000; font-size: 16px; margin-left: 1em; margin-right: 1em;">
<strong>HAIRDREAMS STRUCTURES
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
Perfect waves and curls for any need
</strong></p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
You can fulfil any customer’s wish with Hairdreams hair structures; no matter if straight or curly. Naturally curly hair is available to achieve a specially natural look.
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
Hairdreams hair is naturally straight and can be permed at the salon if desired. As an alternative pre-permed hair is available in the above mentioned 3 structures.
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
Straight - Normal straight hair (slight natural wave when wet)
Wavy 1 - Light curl     Wavy 2 - Medium curl     Curly - Tight curl
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
HAIRDREAMS SPECIALITY<br/>
NATURAL CURL
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
<? include('store/hdny_nav_cwl.php'); ?>
</p>
</td>
<td align="right" style=" vertical-align: bottom; background-image: url('./images/2811.jpg') ;">
<img  src="/images/2808.jpg"/>
</td>
</tr>
<tr>
<td colspan="3" style=" background-image: url('./images/hdny_bottom.png') ;">
<img src="./images/hdny_bottom.png"/>
</td>
</tr>
</table>
