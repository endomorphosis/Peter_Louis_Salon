<table width="720" cellspacing="0" cellpadding="0" style=" background-repeat:repeat-x; max-width: 720px;">
<tr>
<td colspan="3">
<span><? include($template_include_file);?></span>
</td>
</tr>
<tr>
<td colspan="2"  style="background-image: url('./images/hdny_gradientB.png') ;">
<p style="color: #000000; font-size: 16px; margin-left: 1em; margin-right: 1em;">
<strong>
DREAM HAIR WITH GUARANTEE
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
 
The Hairdreams Quality Service
</strong></p>
<p style="color: #000000; font-size: 12px; margin-left: 1em; margin-right: 1em;">
 
Hairdreams’ top priority is perfect hair quality. Therefore every single hair strand is manually produced according to stringent quality criteria. With "Special Hair", our prime quality, even every single hair is selected and sorted by hand. Only the best and healthiest hair strands are thus used for original Hairdreams hair extensions.
 </p>
<p style="color: #000000; font-size: 12px; margin-left: 1em; margin-right: 1em;">
With the same accuracy every hair consignment is closely examined before leaving our house. After passing this test, tags with quality control codes (starting March 2008) are attached to every strand. These tags are your personal quality certificates.
</p>
<p style="color: #000000; font-size: 12px; margin-left: 1em; margin-right: 1em;">
Provided that the hair has been properly cared for, Hairdreams guarantees that the quality of Hairdreams hair will be maintained during the whole wearing period.*
</p>
<p style="color: #000000; font-size: 12px; margin-left: 1em; margin-right: 1em;">
For the unlikely case that a guarantee claim has to be filed, your Hairdreams quality certificate serves as your receipt for possible repair- or substitution arrangements. 
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
<? include('store/hdny_nav_qualities.php'); ?>
</p>
</td>
<td align="right" style=" vertical-align: bottom; background-image: url('./images/2812.jpg') ;">
<img width="350" height="450" src="/images/2812.jpg"/>
</td>
</tr>
<tr>
<td colspan="3" style=" background-image: url('./images/hdny_bottom.png') ;">
<img src="./images/hdny_bottom.png"/>
</td>
</tr>
</table>
