<span><? include($template_include_file);?></span>
<table width="720" cellspacing="0" cellpadding="0" style=" background-repeat:repeat-x; max-width: 720px;">
<tr>
<td colspan="2"  style="background-image: url('./images/hdny_gradient.png') ;">
<p style="color: #000000; font-size: 16px; margin-left: 1em; margin-right: 1em;">
<strong>Problem "Not Enough Hair" </strong>
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
<strong>Causes of fine hair and hair loss</strong>
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
<strong>Genetic predisposition:</strong> Many women are genetically predisposed to have very fine and thin hair.
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
<strong>Environmental influences:</strong> Environmental factors such as stress and toxins can lead to hair loss as well.
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
<strong>Nutrition:</strong> Diet and nutrition play a great part in the aging process of skin and hair: with increasing age the hair becomes less plentiful, as well as more tired and frail.
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
<strong>Hormonal imbalances:</strong> Various diseases, medications, but also pregnancy can lead to hormonal hair loss.
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
Affected women usually suffer greatly from their hair problem, and they try to help the situation with styling products, scalp balms and similar products. However, none of those products usually work too well. 
</p>
</td>
<td align="right" style=" background-image: url('./images/hdny_thickening.png') ;">
<img  src="/images/hdny_thickening.png"/>
</td>
</tr>
<tr>
<td colspan="3" style=" background-image: url('./images/hdny_bottom.png') ;">
<img src="./images/hdny_bottom.png"/>
</td>
</tr>
</table>
