<table width="720" cellspacing="0" cellpadding="0" style=" background-repeat:repeat-x; max-width: 720px;">
<tr>
<td colspan="3">
<span><? include($template_include_file);?></span>
</td>
</tr>
<tr>
<td style="background-image: url('./images/hdny_gradientB.png') ;" >
<img id="large" src="" />
</td>
<td colspan="2" style="background-image: url('./images/hdny_gradientB.png') ;" >
<table>
<tr>
<td colspan="3">
<div id="title"><p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
<strong>Hair Thickening with Hairdreams TopHair - how it works </strong></p>
</div><p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;"><strong>
<div id="step">
</div></strong></p>
</td>
</tr>
<td colspan="3">
<div id="info"><p style="color: #000000; font-size: 12px; margin-left: 1em; margin-right: 1em;">
Hairdreams TopHair can easily be attached on top of the head with clips. It makes the hair in the crown area thicker and longer, and even makes possible styles with exact partings.
The client can remove it and re-attach it herself at any time and without any difficulty. If the client would like a permanent solution, then it can also be attached permanently with use of the Bonding Ring technique.
One wearing period of the TopHair is about 6-8 months.</p>
</div>
</td>
<tr>
<td>
<img id="3016" onload="swap(this)" onclick="swap(this);" name="3014" src="./images/3015.jpg" name="step1" text="Step 1: Consultation" class="thumb"/>
</td>
<td>
<img src="./images/3018.jpg" id="3019" onclick="swap(this);"  name="3017" text="Step 2: Positioning" class="thumb"/>
</td>
<td>
<img src="./images/3021.jpg" id="3022" onclick="swap(this);" name="3020" text="Step 3: Application" class="thumb"/>
</td>
</tr>
<tr>
<td>
<img src="./images/3024.jpg" id="3025" onclick="swap(this);" name="3023" text="Step 4: Styling" class="thumb"/>
</td>
<td>
<img src="./images/3027.jpg" id="3028" onclick="swap(this);" name="3026" text="IMAGE" class="thumb"/>
</td>
<td>

</td>
</tr>
<tr>
</table>
</tr>
</td>
</tr>
<tr>
<td colspan="3" style=" background-image: url('./images/hdny_bottom.png') ;">
<img src="./images/hdny_bottom.png"/>
</td>
</tr>
</table>
<script type="text/javascript">
function swap(some_var)
{ $("#large").attr("src" , "./images/" + $(some_var).attr("id") + ".jpg"); 
  $("#step").text($(some_var).attr("text"));
}
var src = '';
    $("img.thumb").hover(
      function () {
        $(this).attr('swap', $(this).attr('src'));
	$(this).attr('src', './images/' + $(this).attr('name') + '.jpg');
      }, 
      function () {
	$(this).attr('src', $(this).attr('swap'));
      }
    );


</script>