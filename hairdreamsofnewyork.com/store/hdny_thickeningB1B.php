<span><? include($template_include_file);?></span>
<script type="text/javascript">
function swap(some_var)
{ $("#large").attr("src" , "./images/" + $(some_var).attr("id") + ".jpg"); 
  $("#step").text($(some_var).attr("text"));
}


</script>

<table width="720" cellspacing="0" cellpadding="0" style=" background-repeat:repeat-x; max-width: 720px;">
<tr>
<td style="background-image: url('./images/hdny_gradientB.png') ;" >
<img id="large" src="/images/hdny_thickeningB1B.png" />
</td>
<td colspan="2" style="background-image: url('./images/hdny_gradientB.png') ;" >
<div style="width: 380px; height: 478px; overflow:scroll;"> <h1>Styling Options for Hair Thickening with Various Systems. </h1>
<p>There are various hair thickening options depending on the <br/>desired hairstyle and the condition of the client&rsquo;s own hair. <br/>

<br/>
Discover your personal solution here&hellip;</p>

<table width=100 border=0 cellspacing=3 cellpadding=3><tr><td align=center><a href="/en/style_examples_hair_thickening_extensions?gallery2image=265" onMouseOver="document.getElementById('gal265').src='/images/user/2925.jpg';" 
                         onMouseOut="document.getElementById('gal265').src='/images/user/2926.jpg';" ><img src="/images/user/2926.jpg" name=gal265 id=gal265 border=0></a><br>Extensions</td><td align=center><a href="/en/style_examples_hair_thickening_extensions?gallery2image=266" onMouseOver="document.getElementById('gal266').src='/images/user/2936.jpg';" 
                         onMouseOut="document.getElementById('gal266').src='/images/user/2937.jpg';" ><img src="/images/user/2937.jpg" name=gal266 id=gal266 border=0></a><br>MicroLines</td><td align=center><a href="/en/style_examples_hair_thickening_extensions?gallery2image=267" onMouseOver="document.getElementById('gal267').src='/images/user/2941.jpg';" 
                         onMouseOut="document.getElementById('gal267').src='/images/user/2942.jpg';" ><img src="/images/user/2942.jpg" name=gal267 id=gal267 border=0></a><br>HighLines</td></tr><tr><td align=center><a href="/en/style_examples_hair_thickening_extensions?gallery2image=268" onMouseOver="document.getElementById('gal268').src='/images/user/2948.jpg';" 
                         onMouseOut="document.getElementById('gal268').src='/images/user/2949.jpg';" ><img src="/images/user/2949.jpg" name=gal268 id=gal268 border=0></a><br>Extensions</td><td align=center><a href="/en/style_examples_hair_thickening_extensions?gallery2image=269" onMouseOver="document.getElementById('gal269').src='/images/user/2953.jpg';" 
                         onMouseOut="document.getElementById('gal269').src='/images/user/2954.jpg';" ><img src="/images/user/2954.jpg" name=gal269 id=gal269 border=0></a><br>MicroLines</td><td align=center><a href="/en/style_examples_hair_thickening_extensions?gallery2image=270" onMouseOver="document.getElementById('gal270').src='/images/user/2960.jpg';" 
                         onMouseOut="document.getElementById('gal270').src='/images/user/2961.jpg';" ><img src="/images/user/2961.jpg" name=gal270 id=gal270 border=0></a><br>MicroLines</td></tr><tr><td align=center><a href="/en/style_examples_hair_thickening_extensions?gallery2image=291" onMouseOver="document.getElementById('gal291').src='/images/user/3033.jpg';" 
                         onMouseOut="document.getElementById('gal291').src='/images/user/3034.jpg';" ><img src="/images/user/3034.jpg" name=gal291 id=gal291 border=0></a><br>HighLines</td><td align=center><a href="/en/style_examples_hair_thickening_extensions?gallery2image=292" onMouseOver="document.getElementById('gal292').src='/images/user/3038.jpg';" 
                         onMouseOut="document.getElementById('gal292').src='/images/user/3039.jpg';" ><img src="/images/user/3039.jpg" name=gal292 id=gal292 border=0></a><br>TopHair</td><td align=center><a href="/en/style_examples_hair_thickening_extensions?gallery2image=293" onMouseOver="document.getElementById('gal293').src='/images/user/3043.jpg';" 
                         onMouseOut="document.getElementById('gal293').src='/images/user/3044.jpg';" ><img src="/images/user/3044.jpg" name=gal293 id=gal293 border=0></a><br>TopHair</td></tr><tr><td align=center><a href="/en/style_examples_hair_thickening_extensions?gallery2image=294" onMouseOver="document.getElementById('gal294').src='/images/user/3048.jpg';" 
                         onMouseOut="document.getElementById('gal294').src='/images/user/3049.jpg';" ><img src="/images/user/3049.jpg" name=gal294 id=gal294 border=0></a><br>MicroLines</td><td align=center><a href="/en/style_examples_hair_thickening_extensions?gallery2image=295" onMouseOver="document.getElementById('gal295').src='/images/user/3057.jpg';" 
                         onMouseOut="document.getElementById('gal295').src='/images/user/3058.jpg';" ><img src="/images/user/3058.jpg" name=gal295 id=gal295 border=0></a><br>TopHair</td><td align=center><a href="/en/style_examples_hair_thickening_extensions?gallery2image=296" onMouseOver="document.getElementById('gal296').src='/images/user/3062.jpg';" 
                         onMouseOut="document.getElementById('gal296').src='/images/user/3063.jpg';" ><img src="/images/user/3063.jpg" name=gal296 id=gal296 border=0></a><br>TopHair</td></tr><tr><td align=center><a href="/en/style_examples_hair_thickening_extensions?gallery2image=297" onMouseOver="document.getElementById('gal297').src='/images/user/3067.jpg';" 
                         onMouseOut="document.getElementById('gal297').src='/images/user/3068.jpg';" ><img src="/images/user/3068.jpg" name=gal297 id=gal297 border=0></a><br>MicroLines</td><td align=center><a href="/en/style_examples_hair_thickening_extensions?gallery2image=298" onMouseOver="document.getElementById('gal298').src='/images/user/3078.jpg';" 
                         onMouseOut="document.getElementById('gal298').src='/images/user/3079.jpg';" ><img src="/images/user/3079.jpg" name=gal298 id=gal298 border=0></a><br>HighLines</td><td align=center><a href="/en/style_examples_hair_thickening_extensions?gallery2image=299" onMouseOver="document.getElementById('gal299').src='/images/user/3083.jpg';" 
                         onMouseOut="document.getElementById('gal299').src='/images/user/3084.jpg';" ><img src="/images/user/3084.jpg" name=gal299 id=gal299 border=0></a><br>TopHair</td></tr><tr><td align=center><a href="/en/style_examples_hair_thickening_extensions?gallery2image=26" onMouseOver="document.getElementById('gal26').src='/images/user/187.jpg';" 
                         onMouseOut="document.getElementById('gal26').src='/images/user/287.jpg';" ><img src="/images/user/287.jpg" name=gal26 id=gal26 border=0></a><br>Jeanette</td><td align=center><a href="/en/style_examples_hair_thickening_extensions?gallery2image=27" onMouseOver="document.getElementById('gal27').src='/images/user/189.jpg';" 
                         onMouseOut="document.getElementById('gal27').src='/images/user/288.jpg';" ><img src="/images/user/288.jpg" name=gal27 id=gal27 border=0></a><br>Nina</td><td align=center></td></tr></table>

 </div>

</td>
</tr>
</tr>
<tr>
<td colspan="3" style=" background-image: url('./images/hdny_bottom.png') ;">
<img src="./images/hdny_bottom.png"/>
</td>
</tr>
</table>
