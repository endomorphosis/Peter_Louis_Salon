<style>
table, tr, td, img{
padding: 0;
margin: 0;
border: 0;
}


tr{
vertical-align: top; padding: 0; margin: 0px; width: 715px; border-width: 0px; font-size: 14px; color: #000000;
}

td{
vertical-align: top; padding: 0; margin: 0px; border-width: 0px; font-size: 14px; color: #000000; 
}

</style>

<table style="position: relative; top: -1px; vertical-align: top; padding: 0px; margin: 0px; width: 716px; border-width: 0px; font-size: 14px; color: #000000; background-color: #90b2c1; " cellspacing="0">
<tr>
<td colspan="2"><? include($template_include_file);?>
</td>
</tr>
<tr>
<td style="background-image: url('images/phyto_gradient0.png')"><br/>

<p style="color: #000000; font-weight: 700; font-size: 14px; margin-left: 1em; margin-right: 1em;">
Exclusive Formulas 
<br/>
<span style="font-weight: 500; "> Products Derived from Creative Research </span>
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
Our Phytosolba Laboratories are led by a team of scientists including doctors, pharmacists, biochemists, engineers, and botanists. The experience and talent of our distinguished experts allow us to offer our advanced hair care range based on highly innovative concepts and formulas. 
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
Identifying and selecting the active molecules in plants, and choosing their appropriate mode of extraction constitute the scientific approach adopted by the group in order to develop superior quality products. PHYTO scientists learned that every plant's chemical structure is composed of more than 200 components where the juxtaposition of each element combines to form a single element. Following an initial selection of plants, it is necessary to choose the botanical parts of the plant that contain the greatest wealth of active substances (root, flower, bud, whole plant, fruit, gum, resin, sap, seeds, etc.) It is also essential to define the extraction (decantation, maceration, decoction, etc.) method capable of yielding the most powerful active agents. 
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
These scientific techniques ensure the quality and effectiveness of all PHYTO products. PHYTO products are not tested on animals.
</p>

</td>
<td>
<a href="http://www.peterlouissalon.com/template.php?f=prod_detail&ref=phyto&pid=11"><img src="./images/phyto_formulas.png"/></a>
</td>
</tr>
<tr>
<td>
<img src="./images/phyto10.png"/>
</td>
<td>
<img src="./images/phyto11.png"/>
</td>
</tr>
<tr>
<td>
</td>
<td>
</td>
</tr>
</table>