<style>
table, tr, td, img{
padding: 0;
margin: 0;
border: 0;
}


tr{
vertical-align: top; padding: 0; margin: 0px; width: 715px; border-width: 0px; font-size: 14px; color: #000000;
}

td{
vertical-align: top; padding: 0; margin: 0px;  border-width: 0px; font-size: 14px; color: #000000; 
}

</style>

<table style="position: relative; top: -1px; vertical-align: top; padding: 0px; margin: 0px; width: 716px; border-width: 0px; font-size: 14px; color: #000000; background-color: #90b2c1; " cellspacing="0">
<tr>
<td colspan="2"><? include($template_include_file);?>
</td>
</tr>
<tr>
<td style="background-image: url('images/phyto_gradient0.png')"><br/>

<p style="color: #000000; font-weight: 700; font-size: 12px; margin-left: 1em; margin-right: 1em;">
SECRET INGREDIENT
</p>
<p style="color: #000000; font-size: 12px; margin-left: 1em; margin-right: 1em;">
	
Every brand has a defining ingredient that sets it apart and makes it unique. Whether it's the special polymer that magically makes your lashes curl, the peptide that de-puffs your tired eyes, or the vitamin complex that turns your dull hair shiny, it's those special, signature ingredients that often mean the difference between a product you love and one you simply can't live without. Read on to learn more about the special something's that make your favorite products so darn fabulous.
</p>
<p style="color: #000000; font-weight: 700; font-size: 12px; margin-left: 1em; margin-right: 1em;">
WHAT'S IN A BOTTLE? <br/>
<span style="font-weight: 500;" > Keeping It Fresh </span>
</p>
<p style="color: #000000; font-size: 12px; margin-left: 1em; margin-right: 1em;">
It's no secret: The number of plants and botanicals in Phyto products are endless. From the cleansing properties of lemon extract to the moisturizing properties of egg oil, Phyto offers a virtual garden of ingredients that competes with the most well stocked produce section. When founder Patrick Ales first started the company, he was driven by his passion for harnessing active plant botanicals to keep hair looking healthy and beautiful. Case in point: In his Phytopolleine Botanical Scalp Treatment, $30, there are over four pounds of condensed plant extracts; it's touted as "a mini spa for the scalp." Some of the ingredients that thread through his entire line, either combined with other botanicals or used alone are: 
</p>
<p style="color: #000000; font-size: 12px; margin-left: 1em; margin-right: 1em;">
Lemon Oil: A natural cleanser and astringent 
</p>
<p style="color: #000000; font-size: 12px; margin-left: 1em; margin-right: 1em;">
Rosemary Oil: Increases blood circulation to stimulate the scalp 
</p>
<p style="color: #000000; font-size: 12px; margin-left: 1em; margin-right: 1em;">
Cypress Oil: A natural deodorant that helps balance oily scalps 
</p>
<p style="color: #000000; font-size: 12px; margin-left: 1em; margin-right: 1em;">
So how do these botanical-packed concoctions stay fresh? With airtight glass bottles and aluminum tubes that are 100 percent recyclable. Since their creation, Phyto products have been packaged in glass bottles and aluminum tubes to minimize the use of preservatives and to maximize the effectiveness of the botanical formulas. While the stay-fresh nozzle of the aluminum tubes keep air out, the boxes protect the formulas from light and heat creating packaging that's not only pretty, but goes beyond its primary function to symbolize Phyto's passion for natural ingredients and protecting the environment. Botanical treatments for gorgeous hair and good for the planet? Now that's something women-including Mother Nature herself-can definitely appreciate.
</p>


</td>
<td>
<A href="http://www.peterlouissalon.com/template.php?f=prod_detail&ref=phyto&pid=8"><img src="./images/phyto_ingredients.png"/></a>
</td>
</tr>
<tr>
<td>
<img src="./images/phyto10.png"/>
</td>
<td>
<img src="./images/phyto11.png"/>
</td>
</tr>
<tr>
<td>
</td>
<td>
</td>
</tr>
</table>