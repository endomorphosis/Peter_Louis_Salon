<style>
table, tr, td, img{
padding: 0;
margin: 0;
border: 0;
}


tr{
vertical-align: top; padding: 0; margin: 0px; width: 715px; border-width: 0px; font-size: 14px; color: #000000;
}

td{
vertical-align: top; padding: 0; margin: 0px;  border-width: 0px; font-size: 14px; color: #000000; 
}

</style>

<table style="position: relative; top: -1px; vertical-align: top; padding: 0px; margin: 0px; width: 720px; border-width: 0px; font-size: 14px; color: #000000; background-color: #90b2c1; " cellspacing="0">
<tr>
<td colspan="2"><? include($template_include_file);?>
</td>
</tr>
<tr>
<td colspan="2">
<table cellspacing="0" width="720">
<tr>
<td width="300" style="background-image: url('images/phyto_gradient0.png')"><br/>

<p style="color: #000000; font-weight: 700; font-size: 14px; margin-left: 1em; margin-right: 1em;">
Unique Packaging<br/>
<span style="font-weight: 500;">Eco-Friendly Packaging</span>
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
Since their creation, PHYTO products have been packaged in glass bottles and aluminum tubes to minimize the use of preservatives and to maximize the effectiveness of the botanical formulas. While the stay-fresh nozzle of the aluminum tubes prohibits air from entering the containers, the boxes in which the products are packaged protect the formulas from light and heat. These two safeguards further preserve the integrity of our formulas. 
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
Beyond its primary function, this packaging symbolizes the pharmaceutical identity of the PHYTO brand while contributing to environmental protection.
</p>

</td>
<td style="background-image: url('images/phyto_gradient0.png')">
<img src="./images/phyto_packaging.png"/>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td>
<img src="./images/phyto10.png"/>
</td>
<td>
<img src="./images/phyto11.png"/>
</td>
</tr>
<tr>
<td>
</td>
<td>
</td>
</tr>
</table>