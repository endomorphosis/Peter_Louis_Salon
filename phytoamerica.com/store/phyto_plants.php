<style>
table, tr, td, img{
padding: 0;
margin: 0;
border: 0;
}


tr{
vertical-align: top; padding: 0; margin: 0px; width: 715px; border-width: 0px; font-size: 12px; color: #000000;
}

td{
vertical-align: top; padding: 0; margin: 0px;  border-width: 0px; font-size: 12px; color: #000000; 
}

</style>
<? include($template_include_file);?>
<table style="position: relative; top: -1px; vertical-align: top; padding: 0px; margin: 0px; width: 716px; border-width: 0px; font-size: 12px; color: #000000; background-color: #90b2c1; " cellspacing="0">
<tr>
<td colspan="2">
</td>
</tr>
<tr>
<td style="background-image: url('images/phyto_gradient0.png')"><br/>
<p style="color: #000000; font-size: 12px; margin-left: 1em; margin-right: 1em;">
Mr. Patrick Ales is an innovator, an artist and a true visionary.
</p>
<p style="color: #000000; font-size: 12px; margin-left: 1em; margin-right: 1em;">
At a very young age, he discovered that he had a dual passion for hair styling and the healing power of plants. While becoming one of the most renowned hair stylists in Paris, styling the hair of legendary women such as Jacqueline Kennedy Onassis and Catherine Deneuve, he also experimented with different plants and essential oils to create a variety of natural hair care products.
</p>
<p style="color: #000000; font-size: 12px; margin-left: 1em; margin-right: 1em;">
In 1969, he opened his own salon (which is still in existence today) where he perfected his craft and tested his own botanical mixtures and concoctions.
</p>
<p style="color: #000000; font-size: 12px; margin-left: 1em; margin-right: 1em;">
What grew from his vision is a complete and comprehensive line of hair care products that employs the synergy of plants to treat a variety of hair types and conditions. Today, each PHYTO formula stays true to Mr. Ales' philosophy and is scientifically designed and tested in order to reveal beautiful, healthy hair.
</p>
<p style="color: #000000; font-size: 12px; margin-left: 1em; margin-right: 1em;">
While Phyto products were conceived long before the emergence of environmental awareness and botanical ingredient trends, the brand continues to deliver natural botanicals backed by nearly four decades of research, all while staying true to their eco-friendly philosophy (all packaging is 100 percent recyclable; for more information see Secret Ingredient). Over 35 years later, the Phytosolba Laboratories in France are still family owned, and if you're lucky, you'll get a glimpse of Ales himself knocking around Phyto's greenhouses and laboratories just outside of Paris. To this day, the company operates under Ales's trademark motto inspired by years of research and discovery:
</p>
<p style="color: #000000; font-size: 12px; margin-left: 1em; margin-right: 1em;">
Beauty insiders have been pampering their strands with Phyto's coveted line of botanically derived, good-for-your-hair care for the last four decades. Phyto, which is Greek for 'derived from plants,' merges active plant botanicals with modern science for wholesome formulas that travel to the root (literally) of the problem to snap hair back to its healthiest condition. Clinically tested for performance, these naturally derived, cruelty-free formulas are packed in 100 percent recyclable containers, bringing eco-friendly hair care to a shower near you.
</p>
</td>
<td>
<img src="./images/phyto00.png"/>
</td>
</tr>
<tr>
<td>
<img src="./images/phyto10.png"/>
</td>
<td>
<img src="./images/phyto11.png"/>
</td>
</tr>
<tr>
<td>
</td>
<td>
</td>
</tr>
</table>