<style>
table, tr, td, img{
padding: 0;
margin: 0;
border: 0;
}


tr{
vertical-align: top; padding: 0; margin: 0px; width: 715px; border-width: 0px; font-size: 14px; color: #000000;
}

td{
vertical-align: top; padding: 0; margin: 0px;  border-width: 0px; font-size: 14px; color: #000000; 
}

</style>

<table style="position: relative; top: -1px; vertical-align: top; padding: 0px; margin: 0px; width: 716px; border-width: 0px; font-size: 14px; color: #000000; background-color: #90b2c1; " cellspacing="0">
<tr>
<td colspan="2"><? include($template_include_file);?>
</td>
</tr>
<tr>
<td style="background-image: url('images/phyto_gradient0.png')"><br/>

<p style="color: #000000; font-weight: 700; font-size: 14px; margin-left: 1em; margin-right: 1em;">
The Science of Plants
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
Inspiration Drawn from Nature and Plants
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
PHYTO drew inspiration from plants well before the emergence of environmental awareness. To this day, our family-owned Phytosolba Laboratories carefully select the plants with the most beneficial properties to use in each product. Our solutions boast extremely high concentrations of active botanical extracts with clinically proven results.
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">

<span style="font-weight: 700;"> <a href="http://www.peterlouissalon.com/template.php?f=prod_detail&ref=phyto&pid=40">PHYTOPOLLEINE</a></span>
<br/>
Botanical Scalp Stimulant with essential oils
<br/>
"A mini spa for the scalp"
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
This .8 fl oz bottle contains nearly <br/> 4 pounds of condensed plant extracts
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
<a href="http://www.peterlouissalon.com/template.php?f=prod_detail&ref=phyto&pid=40"><img src="./images/phytopolleine2.jpg"/></a>
</p>
</td>
<td>
<a href="http://www.peterlouissalon.com/template.php?f=prod_detail&ref=phyto&pid=54"> <img src="./images/phyto_science.png"/></a>
</td>
</tr>
<tr>
<td>
<img src="./images/phyto10.png"/>
</td>
<td>
<img src="./images/phyto11.png"/>
</td>
</tr>
<tr>
<td>
</td>
<td>
</td>
</tr>
</table>