<style>
table, tr, td, img{
padding: 0;
margin: 0;
border: 0;
}


tr{
vertical-align: top; padding: 0; margin: 0px; width: 715px; border-width: 0px; font-size: 14px; color: #000000;
}

td{
vertical-align: top; padding: 0; margin: 0px; border-width: 0px; font-size: 14px; color: #000000; 
}

</style>

<table style="position: relative; top: -1px; vertical-align: top; padding: 0px; margin: 0px; width: 716px; border-width: 0px; font-size: 14px; color: #000000; background-color: #90b2c1; " cellspacing="0">
<tr>
<td style="width: 716px;" colspan="2"><? include($template_include_file);?>
</td>
</tr>
<tr>
<td colspan="2">

<table>
<tr>
<td colspan="2">
</p><p style="color: #000000; font-size: 12px; margin-left: 1em; margin-right: 1em;">
<span style="font-size: 14px; font-weight: 700;">COLLECTIONS</span> <br/>
	
Phyto solves your hair concerns with the best botanicals Mother Nature has to offer. Backed by decades of research, Phyto products are packaged in eco-friendly containers to satisfy even the most earth-friendly beauty junkies. This comprehensive line comes with color-coded collections for every hair condition from ultra-dry hair to dandruff, with regimens that blend seamlessly with busy lifestyles. Whether you're in search of a true botanical hair care line or have a specific hair concern, Phyto has just the right bouquet of plants and herbal essences to keep your locks healthy-the natural way.
</p>
<p style="color: #000000; font-size: 12px; margin-left: 1em; margin-right: 1em;">
<span style="font-size: 14px; font-weight: 700;">PETAL POWER </span><br/>
Score Healthier Hair with These Botanicals-In-A-Bottle
</p>

</td>
<td align="center"><a href="http://www.peterlouissalon.com/template.php?f=permedcolored&ref=phyto">
<img src="./images/C13345_sm.jpg"/><br/>Haircare Collection for Colored Hair<br/>
</a></td>
</tr>
<tr>
<td align="center"><a href="http://www.peterlouissalon.com/template.php?f=dryhair&ref=phyto">
<img src="./images/C13711_sm.jpg"/><br/>Phyto Collection For Dry Hair<br/>
</a></td>
<td align="center"><a href="http://www.peterlouissalon.com/template.php?f=thinninghair&ref=phyto">
<img src="./images/C13712_sm.jpg"/><br/>Phyto Collection for Hair Loss<br/>
</a></td>
<td align="center"><a href="http://www.peterlouissalon.com/template.php?f=phytospecific&ref=phyto">
<img src="./images/C17652_sm.jpg"/><br/>Phytospecific Collection<br/>
</a></td>
</tr>
</table>

</td>
</tr>
<tr>
<td>
<img src="./images/phyto10.png"/>
</td>
<td>
<img src="./images/phyto11.png"/>
</td>
</tr>
<tr>
<td>
</td>
<td>
</td>
</tr>
</table>