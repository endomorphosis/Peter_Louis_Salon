<style>
table, tr, td, img{
padding: 0;
margin: 0;
border: 0;
}

tr{
vertical-align: top; padding: 0; margin: 0px; width: 715px; border-width: 0px; font-size: 12px; color: #ffffff; 
}

td{
vertical-align: top; padding: 0; margin: 0px; width: 715px; border-width: 0px; font-size: 12px; color: #ffffff; 
}

</style>
<table style="position: relative; top: -1px; vertical-align: top; padding: 0px; margin: 0px; width: 715px; border-width: 0px; font-size: 12px; color: #ffffff; background-color: #0e891e; " cellspacing="0">
<tr>
<td colspan="2"><? include($template_include_file);?>
</td>
</tr>
<tr>
<td><br/>
<p style="color: #ffffff; font-size: 12px; margin-left: 1em; margin-right: 1em;">
Rene Furterer uses a unique and original method for optimizing the effectiveness of its personalized treatments in the salon and at home.
</p>
<p style="color: #ffffff; font-size: 12px; margin-left: 1em; margin-right: 1em;">
Observing that hair can only be beautiful if the scalp is healthy, Rene Furterer developed a special method based on 4 major principles:
</p>
<p style="color: #ffffff; font-size: 12px; margin-left: 1em; margin-right: 1em;">
Diagnosis - A thorough diagnosis of the scalp and hair. We invite you to conduct your own pre-diagnosis online.
</p>
<p style="color: #ffffff; font-size: 12px; margin-left: 1em; margin-right: 1em;">
Brushing - Special brushing techniques. Daily brushing is of primary importance. It eliminates dust and loose strands, restoring volume to the rest of the hair.
</p>
<p style="color: #ffffff; font-size: 12px; margin-left: 1em; margin-right: 1em;">
Massages - Special massages techniques. During a personalized Rene Furterer hair care treatment, special massages ensure relaxation, while reinforcing effectiveness.
</p>
<p style="color: #ffffff; font-size: 12px; margin-left: 1em; margin-right: 1em;">
Personalized treatments - The personalized treatments are based on an original hair care program divided into 3 phases.
</p>
</td>
<td>
<img src="./images/rene_method.jpg"/>
</td>
</tr>
<tr>
<td>
<img src="./images/rene10.png"/>
</td>
<td>
<img src="./images/rene11.png"/>
</td>
</tr>
<tr>
<td>
</td>
<td>
</td>
</tr>
</table>