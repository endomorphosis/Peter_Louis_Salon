<table width="100%" border="0" cellspacing="0" cellpadding="10">
<tr>
<td style="padding-right:0px;">
  <h1 style="font-weight: bold; font-size: 16px; font-family: verdana,arial,helvetica,sans-serif;">What Is Laser Light Hair Therapy?</h1>
  <h3 align="center">LASER HAIR REJUVENATION THERAPY</h3>
  <h3>&quot;WHAT IS LASER HAIR REJUVENATION THERAPY&quot;</h3>
  <p>Laser Hair Rejuvenation is a "NEW" non-surgical scientific approach to treating hair loss, thinning hair, baldness and problems associated with the scalp. &nbsp; Hair Rejuvenation is a medically tested effective and proven method using Low Level Laser Energy, or ?soft? laser light to effectively treat and control problem hair loss. </p>
  
  <p>The Laser Hair Rejuvenation non-surgical treatments utilize a device containing therapeutic low level lasers developed in Europe for the treatment of hair loss and diseases of the scalp. &nbsp; The treatment delivers light energy directly from secured laser positions designed to increase the blood flow to the scalp. &nbsp; The laser functions on the same scientific principal of that of photo biotherapy where the laser light stimulates cell metabolism and causes damaged cells to be repaired. &nbsp; This breakthrough technology has recently been featured on national newscasts across the USA. &nbsp; Physicians are praising this technology as an effective treatment to combat hair loss. </p>
  <p>The laser treatments have an overall good effect on all types of hair. &nbsp; It is recommended for men and women at any age.</p>
  <h3>&quot;WHEN CAN YOU EXPECT RESULTS&quot;</h3>
  <p>In most cases noticeable improvement is seen in the first four to six months of treatment. &nbsp; If you have a high rate of hair loss it is possible that you may see a reduced rate after only a few treatments. &nbsp; Of course, results will vary with the individual, and strict adherence to your protocol schedule is required to get the best results. &nbsp; One of the main advantages of the LASER HAIR REJUVENATION THERAPY is that they give a steady and consistent application of laser light that is engineered for perfect intensity. </p>
  
  <h3>&quot;IS LASER HAIR REJUVENATION SAFE&quot;</h3>
  <p><strong>Yes!</strong> &nbsp; The Laser Hair Rejuvenation device which is classified as a Certified Class 111A cosmetic laser is recognized by the FDA and is approved for cosmetic use in the United States and Canada.</p>
  <p>The energy produced by the photos does not have the thermal component to cause thermal injuries to users or operators. &nbsp; Laser Hair Rejuvenation energy does not change or alter molecular structures: it stimulates the body?s mechanism to repair and heal itself. </p>
  <h3>WHAT ARE THE BENEFITS OF LASER HAIR REJUVENATION</h3>
  <ul style="font-size: 11px;">
  <li>Increases blood supply to the scalp by 70% after only one treatment </li>
  <li>Stops the progression of hair loss in 85% of the patients (clients). </li>
  
  <li>Stimulate rejuvenation of hair. </li>
  <li>Repairs and improves hair shaft quality resulting in a 25% increase in volume (thickness). </li>
  <li>Dramatically prolongs the life of hair color and perms. </li>
  <li>Relieves irritating scalp conditions (psoriasis etc.)</li></ul>
  <h3>&quot;HAVE THE BENEFITS OF LASER HAIR REJUVENATION BEEN SCIENTIFICALLY MEASURED&quot;</h3>
  <p><strong>Yes!</strong> &nbsp; A double-blind placebo controlled study was conducted to evaluate the effect of Laser Hair Rejuvenation by comparing it with a placebo laser. </p>
  <p>All patients with the exception of one in the laser treated group showed a complete stop of hair loss. </p>
  
  <p>All patients except three showed a clear re-growth of hair with a reduction of at least one category in the Norwood Classification.</p>
  <p>Out of 18 patients, 14 showed an increase in hair thickness and all 18 showed improvements tin the general hair shaft quality. &nbsp; The results showed no improvement in the placebo group or any adverse effects of the treatments. </p>
  <p>The present double-blind study confirmed by histological examination, revealed that re-growth of new hairs can be achieved in the most middle-age and younger females and males with typical male pattern baldness, when the scalp is irradiated with Laser Hair Rejuvenation for 20 minutes twice a week for 5 weeks. &nbsp; The treatments are pain free, completely safe without side effects. </p>
  <p>&nbsp;</p>
</td>
</tr>
</tbody>
</table>
</td>
<td align="left" valign="top" width="200">
<table class="ae_noborder" border="0" cellpadding="5" cellspacing="0" width="100%">
<tbody>
<tr>
<td>
  <div class="rightcontainer">
    <h1 style="font-weight: bold; font-size: 16px; font-family: verdana,arial,helvetica,sans-serif;">Scientific Studies</h1>
    <p>Dr Brian E. Johnson in 1969 reported his attempts to Stimulate hair growth by 260 nm irradiation. Inhibitions Were reported in the wavelength range 280-310 nm.</p>
    <p>In 1968 Professor E. Mestor reported that unfocused 694 nm Rubin laser light initially increased hair growth.</p>
    <p>Dr Trelles showed in 1984 in one study that patients with Alopecia areata who were treated with the HeNe Laser 632.8 nm showed an excellent response. Dr Trelles reported that most of the patients with alopecia areata responded well after only 6 to 8 treatments administered twice a week for four weeks. The HeNe laser was placed 30 centimeters from the alopecia area with dosages ranging from 3-4 joule per sq cm. No fibers or lenses were used. In the same study, microscopic evaluation of the hair shaft structure on the alopecia areata irradiated areas showed a clear modulla rich in Keratin after treatment. </p>
    <p>In 1992 at the 4th Annual meeting of the Japan Laser Therapy Association success was reported with an Increase in both hair growth and the density of the hair Follicle in the laser treated areas of both male and Female stress alopecia areata patients, with only one Patient out of 40 not getting stellar results.</p>
  </div>
</td>
</tr>
</table>
