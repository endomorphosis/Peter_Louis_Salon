<table border="0" cellpadding="5" cellspacing="0" width="100%">
	<tr>
		<td>
			<p align="justify">&nbsp;<img alt="" src="images/header_home5.gif" border="0"></p>
      <div align="justify">
        <p><strong>Peter's Biography</strong></p>
        <p><img src="Peter.jpg" alt="" width="200" height="149" border="0" align="right" />PETER   LOUIS AGRO is an industry leader in creative styling, healthy hair management   and beauty. He has an adroit understanding of beauty trends and fashion imaging.   Creative strengths include conceptualizing personal development, styling advice,   instruction and precision cutting.<br />
            <br />
        As an entrepreneur he provides an analytical understanding of the dynamics of   &ldquo;beautiful hair&rdquo; and his abilities include training creative talent   to help people celebrate their own individual looks and styles.</p>
        <p>Since 1992 Peter Louis Agro has been cutting and styling hair, working in creative   strategy with international &ldquo;hair gurus&rdquo; at the top tier of the beauty   scene. Servicing a loyal clientele in fashion, beauty, entertainment and society,   Agro maintains ongoing relationships with prominent figures in politics, literature,   film &amp; media.</p>
        <p>Working with celebrities and local New Yorkers, Agro decided to launch his   own successful salon, The Peter Louis Salon, in 1999 and also created an innovative   Website, www.peterlouissalon.com . Servicing New York City&rsquo;s high-end Sutton   Corridor, the salon provides full service beauty and haircare ( 7 days a week!)   and carries some of the top hair and beauty products. Top lines such as Phytologie,   which is available to purchase at the salon and also by mail order through the   website.</p>
        <p>Prior to pursuing his degree in Cosmetology, Agro earned a BBA in Marketing   Management from Baruch College C.U.N.Y.. Agro also enjoyed a successful business   career while attending The School of Visual Arts for sculpture, which gave him   the background and courage to follow his dream to open his own salon.</p>
        <p>Agro resides in New York, and divides his time between   his<br />
          business, physical fitness and various artistic pursuits.</p>
        <p><a href="http://www.PeterLouis.blogspot.com"><strong>Chat with Peter Louis</strong></a></p>
      </div>
		</td>
  </tr>
</table>
