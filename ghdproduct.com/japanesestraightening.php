<table id="table1" width="100%" border="0">
<tbody>
<tr>
<td width="20">&nbsp;</td>
<td>&nbsp;</td></tr>
<tr>
<td width="20">&nbsp;</td>
<td>
<table cellspacing="0" cellpadding="0" width="100%" border="0">
<tbody>
<tr>
<td><font class="small"><b><img height="27" alt="Hair Services" src="hair_services_147x27.gif" width="147" align="absBottom" border="0" /><font size="1">&nbsp;</font></b></font> 
<p><font class="title" face="Arial" color="#333333" size="2"><b>A Straight Hair Story: Japanese Hair Straightening Fan</b></font></p>
<table id="table1" cellspacing="0" cellpadding="0" width="100%" border="0">
<tbody>
<tr>
<td align="middle"><a href="jap4big.jpg" target="_blank"></a></td></tr></tbody></table>
<p><font class="bodytext"><font face="Arial" color="#333333" size="2"><img height="93" alt="" src="coarse_dry200.jpg" width="200" align="right" border="0" /></font><font face="Arial" size="2" style="text-align:justify;">It was by coincidence that I rented "The Stepford Wives" only days before becoming a model for the Japanese Hair Straightening System aka Thermal Reconditioning. Like the women in the film, <b><i>all Japanese hair straightening women share a common beauty trait: Straight, silky, shiny hair!</i></b> Like most curly tops, I own a variety of hats, and know every trick to smooth the frizzies.</font></font></p>
<p><font class="bodytext"><font face="Arial" size="2"><img height="93" alt="" src="Curly200.jpg" width="200" align="left" border="0" />But, there's one big advantage of living in the 21st Century; it's the constant stream of scientific breakthroughs in the billion-dollar beauty business that help put things like the frizzies behind us once and for all. The Japanese Hair Straightening System is a great example of this, and I'd like to share my experience with you. Originating in Japan, <b>this natural, Japanese Straightening System is used in conjunction with a straightening iron that seals in the hair's moisture and shine once the Japanese straightening process is completed.</b></font></font></p>
<p align="left"><font class="bodytext"><font face="Arial" size="2"><a href="jap1big.jpg" target="_blank"><img height="93" alt="" src="LCourse200.jpg" width="200" align="left" border="0" /></a> starting the process though, you're invited to <i>have a consultation where you'll learn all about the product and what you can expect. </i>I found the most impressive part of the meeting was the "after" pictures they showed me of their clients. The hair looked amazing, but it was really the satisfied expressions of the clients that convinced me to go ahead. I had never opted, in all my years of sleeking back and pony tailing, to have my hair straightening because I was convinced it would be ruined for good. But this time I thought things were going to be different. </font></font></p>
<p><font class="bodytext"><font face="Arial" size="2"><img height="93" alt="" src="ultr_dry200.jpg" width="200" align="left" border="0" /></font> <b>The Japanese Straightening took five hours - with chemical smells low - and excitement high! The team was meticulous and they processed small sections of hair at a time. The stylists took detailed notes on the health and color or my mane for future maintenance. And since Japanese hair straightening never loses its straightness, you simply return for two or three touchups a year.</b></font></font></p>
<table id="table2" cellspacing="0" cellpadding="0" width="100%" border="0">
<tbody>
<tr>
<td align="middle"><a href="/jap6big.jpg" target="_blank"></a></td></tr></tbody></table>
<p><font class="bodytext"><font face="Arial" size="2">After the process, I was so excited that I cut my hair to my chin, fulfilling my lifelong dream of an "Isabella Rosellini" like hairdo. After washing my hair and realizing this was no longer a fantasy, I started wondering what the heck was in this stuff: <b>The Japanese Straightening System AKA Thermal Reconditioning loosens the hair's cystine protein bonds, reshapes the bonds by straightening the hair cells with the use of a heat iron then makes it permanent using a bromide neutralizer. This is the basis for the The Japanese Straightening System.</b></font></font></p>
<p><font face="Arial" color="#000000" size="2"><a href="/jap3big.jpg" target="_blank"></a>To test the claim of hair becoming <b>humidity proof</b>, I journeyed to the humid Santa Monica Pier. Walking through, I eyed a mirror on the side of the carousel. Well, I had to sneak a peek, and lo and behold - I was Isabella like! My hair anyway! It was straighter than anyone on the pier! The Japanese System Hair Straightening is serious hair processing with results that are no laughing matter. ... On the practical side, I no longer scare the FedEx driver when I answer the door in the morning, and this new beauty process has cut down my making up time by 75% - resulting in an increase of my making out time by 25%! I'm sure it's the hair!!! </font></p>
<p><font face="Arial" color="#000000" size="2">My new straight hair gives me added confidence (and gets me extra compliments). Now I can focus on color over cut. I have the time for lining my eyes extra well, for cleaning my brushes first - and even having that third cup of coffee!</font></p>
<table id="table3" cellspacing="0" cellpadding="0" width="100%" border="0">
<tbody>
<tr>
<td align="middle"><a href="/jap2big.jpg" target="_blank"></a></td>
<td align="middle"><a href="/jap5big.jpg" target="_blank"></a></td></tr></tbody></table><font face="Arial" size="2">
<hr />
</font>
<p><font class="bodytext"><font size="2"><font face="Arial"><b>What:</b> This unique service can take anywhere from three-and-a-half to six hours and costs upwards of $500. After washing and drying hair, a Japanese essential oil/aloe straightening serum is applied to small sections of hair at a time, then left to soak in. Following more washing and drying, baby irons are used to flatten hair almost strand by strand. Next comes a neutralizing solution and, yes, another wash and dry. <br /><br /><b>Why:</b> With this treatment, even curly or coarse hair is not only straight and frizz-free for months, but actually healthier and softer, boasting more shine and gloss. Clients can still curl and style, but most are happy to wake up everyday with a blow-out-straight 'do. And many swear they pinch pennies all year for the pleasure.</font></font></font></p><font face="Arial" size="2">
<hr />
</font>
<p><font class="title" face="Arial" color="#333333" size="2"><b>JAPANESE HAIR STRAIGHTENING HISTORY</b></font></p>
<p><font class="bodytext" face="Arial" size="2">The first question that is asked when the clients hear about Japanese Hair Straightening aka Thermal Reconditioning ( which is like saying Champagne aka sparkling wine ) is "Why do the Japanese need a relaxer? They already have straight hair!" Contrary to popular belief, a high percentage of the Japanese population has wavy or kinky curl to their hair.</font></p>
<p><font class="bodytext" face="Arial" size="2">LISCIO "The First Japanese Straightening System"</font></p>
<p><font class="bodytext" face="Arial" size="2">To respond to the desire of clients to have straight hair, Milbon introduced Liscio in Japan on 1996. Unlike existing relaxers with active ingredients based on Hydroxide and lye, thio-based Liscio is more gentle for the hair thereby straightening the hair while leaving it in wonderful condition. Milbon was the first manufacturer to receive Japanese governmental approval for its unique thermal reconditioning system. Liscio contains a specially formulated solution and iron, which protects and repairs hair from heat damage.</font></p>
<p><font class="bodytext" face="Arial" size="2">In 1998 Shinbi International brought Liscio to the US and has developed the product and procedure even further to enable the process to be performed on just about any client that walks through the salon door. With added protein treatments and supporting products for the thermal reconditioning procedure, it is not uncommon to hear clients proclaim that their hair condition actually has improved after the procedure. We at the Peter Louis Salon are confident that our line of products and unique techniques will meet your desire to have beautiful straight hair.</font></p>

<p>
    <font class="title" face="Arial" color="#333333" size="2" style="font-weight:bold;">
    What is Thermal Reconditioning?</font>
    <br /><font class="bodytext" face="Arial" size="2">With thermal reconditioning, your hair becomes silky, smooth, and shiny. After the treatment, the hair becomes more manageable and looks healthier.</font>
</p>
<p>
    <font class="title" face="Arial" color="#333333" size="2" style="font-weight:bold;">Milbon Straight Liscio</font>
    <br /><font class="bodytext" face="Arial" size="2">Liscio makes resistant/curly/wavy hair look naturally straight & shiny and manageable.</font>
</p>    
<p align="center"><img src="images/Image1_japanis.jpg" border="0" width="221" height="300" /></p>
<p>
    <font class="bodytext" face="Arial" size="2">To get the best result of thermal reconditioning, it is crucial to use the right products, the right technique, and most importantly, to choose the right hairstylist with extensive knowledge and experience of this treatment. Before the treatment, hairstylists need to diagnose the hair accurately, then decide which products to use, adjust their flat-iron technique and determine processing time.</font>
</p>

<p>
    <font class="title" face="Arial" color="#333333" size="2" style="font-weight:bold;">Hair Consultation</font><br />
    <font class="bodytext" face="Arial" size="2">First, you need a hair consultation with your hairstylist before having thermal reconditioning treatment. This is to see your condition and decide whether or not your hair can have the treatment.</font>
</p>

<p>
    <font class="title" face="Arial" color="#333333" size="2" style="font-weight:bold;">Straighteners & Hair Protections</font><br />
    <font class="bodytext" face="Arial" size="2">Different products are used for virgin hair and hair for touch-up. Hairstylists determine which product to use according to your hair condition.</font>
</p>
<p align="center"><img src="images/image4_japanis.gif" border="0" width="535" height="86" /></p>

<p>
    <font class="title" face="Arial" color="#333333" size="2" style="font-weight:bold;">Hair Conditioning</font><br />
    <font class="bodytext" face="Arial" size="2">It is important to condition hair before thermal reconditioning if the hair is damaged. By adding Keratin and Collagen, hair becomes more healthy and ready for the treatment.</font>
</p>
<p align="center"><img src="images/image5_japanis.gif" border="0" width="156" height="57" /></p>

<p>
	<font class="title" face="Arial" color="#333333" size="2" style="font-weight:bold;">Flat-Ironing Technique</font><br /><font class="bodytext" face="Arial" size="2">This process is to restructure hair texture. By adding heat on every section of hair for 3 seconds, the chemical ingredients go into hair deeply and make hair straight.</font>
</p>
<p align="center"><img src="images/image3_japanis.jpg" border="0" width="163" height="71"/></p>

<p>
	 <font class="title" face="Arial" color="#333333" size="2" style="font-weight:bold;">
     	Hair Care at Home is Crucial</font><br />
      <font class="bodytext" face="Arial" size="2">Maintain your glossy hair with salon quality products at home.
Say good-bye to bad hair days!</font>  
</p>
<p align="center"><img src="images/image2_japanis.gif" border="0" width="209" height="67"/></p>

<p></p></td></tr></tbody></table></td></tr></tbody></table>