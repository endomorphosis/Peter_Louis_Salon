<table class="ae_noborder" border="0" cellpadding="0" cellspacing="3" width="100%">
<tr>
<td>
  <table class="ae_noborder" border="0" cellpadding="0" cellspacing="3" width="100%">
  <tbody>
  <tr>
  <td>
    <h2>A NEW DIMENSION IN LASER HAIR REJUVENATION TECHNOLOGY</h2>
    <h3>HAIR SHAFT TREATMENT </h3>
    <p>More than any other light or treatment mechanism, The LASER HAIR REJUVENATION THERAPY will penetrate through the cortex of the hair shaft. Treatments are sealed deeply within the hair. Conditioners that normally lay on the hair's surface are micro-sized and are driven all the way through the cortex.</p>
    <h3>COLOR</h3>
    <p>The LASER HAIR REJUVENATION THERAPY are used with color procedures as a single process applicator where the hair color is applied in the usual manner and processed with the laser for the normal processing time. The laser color process reduces fading dramatically and especially red colors are added shine.</p>
    <h3>PERM WAVING </h3>
    <p>Perms are processed with the LASER HAIR REJUVENATION THERAPY for the last 33% of the normal chemical action time. When combined with acid waves, the laser is used for neutralizing only. Perms last extraordinarily longer. </p>
    <h3>CONDITIONING OF SPLIT ENDS ( DAMAGED HAIR ) </h3>
    <p>Conditioners offer an excellent all-round treatment when used with the&nbsp;LASER HAIR REJUVENATION THERAPY. They can be used 1-2 weeks after any chemical service, or at any time the professional recommends treatments. A 5-week conditioning program is recommended for over processed, sun damaged or otherwise unhealthy hair. </p>
    <h3>CAUSE OF LASER HAIR REJUVENATION THERAPY </h3>
    <p>The cause of action is explained with the possible closure of the outer cuticles. An additional hypothesis is the possibility that disulfide, or sulfer-sulfer, bonds found within hairs protein molecules are repaired closing in perm chemicals and colors accelerating the repair of the shaft bound molecules with the LASER HAIR REJUVENATION THERAPY treatments. During a perm, such bonds are initially broken with a chemical treatment that erases the "memory of the hair". Once the hair is held in the desired shape, new bonds are formed with a second chemical step using the LASER HAIR REJUVENATION THERAPY.</p>
  </td>
  </tr>
  </tbody>
  </table>
</td>
<td valign="top" width="200">
  <table class="ae_noborder" border="0" cellpadding="0" cellspacing="2" width="100%">
  <tbody>
  <tr>
  <td>
  <div class="rightcontainer">
    <h1 style="font-weight: bold; font-size: 16px; font-family: verdana,arial,helvetica,sans-serif;">Example</h1><img alt="example of multi colors " src="blond.JPG"> 
    <p></p>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
  </div>
  </td>
  </tr>
  </tbody>
  </table>
</td>
</tr>
</table>