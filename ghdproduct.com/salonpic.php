<table  border="0" cellpadding="5" cellspacing="0" width="100%">
<tr>
<td colspan="2">
	<img src="about_the_salon_h.gif" border="0" height="23" width="535">
</td>
</tr>
<tr>
<td valign="top">
<p><b><font class="bodytext"><img src="peter_rounded1.jpg" align="right" border="0" height="258" width="157">Enjoy an escape from modern day life.</font></b></p>
<p><font class="bodytext">Pollution, stress, medications, hormonal changes result in a lack of essential nutritional elements. The Peter Louis Salon offers specific treatments to fortify and revitalize the scalp and the hair.</font></p>
<p><font class="bodytext">Beauty, health and vitality come from an inner glow that stems from excellent nutrition and proper hygiene, this is all evident in our skin and hair. </font></p>
<p><b><font class="bodytext">We can create any look you desire...</font></b></p>
<p><font class="bodytext"><img src="peter_rounded2.jpg" align="left" border="0" height="258" width="157">For a decade, the quality of Peter Louis products allows the hairstylist to fully accomplish his or her calling to make hair beautiful. Thanks&nbsp;for invaluable resources and to the laboratories expertise, you will find with your hairstylist that using&nbsp;Peter Louis&nbsp;Products will give you the most reliable, non-aggressive and best solutions&nbsp;for your&nbsp;hair. </font></p>

<p><b><font class="bodytext">A Stylish Precision Cut</font></b></p>
<p><font class="bodytext">Over exposure to sun, salt, chlorine and wind can lead to breakage, dryness, fragile and brittle hair. The Peter Louis solution is to provide you with a full line of botanical products that offer extraordinary protection from the sun, sea and wind.
</font>
</p>
</td>
</tr>

</table>