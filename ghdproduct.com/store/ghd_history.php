<? include($template_include_file);?>
<table width="720" cellspacing="0" cellpadding="0" style=" background-repeat:repeat-x; max-width: 720px;">
<tr>
<td  style="background: url(/images/ghd_gradient.png); backround-repeat: repeat-x;  background-color: #3a3a3a;">

<p style="color: #ffffff; font-size: 12px; margin-left: 1em; margin-right: 1em;">
When ghd (the acronym for "good hair day") launched in 2001, women discovered a new way to live: gorgeously. Not surprisingly, word of mouth spread like wildfire and demand for their beyond-brilliant tools and products soon surpassed supply. (It even got to the point where women traveled to the ghd headquarters to beg for styling irons at their doors!) Thus began a belief in ghd so powerful that the brand was dubbed the "new religion for hair."
</p>
<p style="color: #ffffff; font-size: 12px; margin-left: 1em; margin-right: 1em;">
What sets ghd styling tools apart from others? As opposed to flattening irons, ghd stylers allow you to achieve so much more-although you'll still get the glossiest, smoothest, straightest hair, if you so wish. Add texture, volume, and movement; go for curls, ringlets, sexy waves, flicks, twists, and volume. Basically, your hair styling repertoire will increase exponentially with one single tool.
</p>
<p style="color: #ffffff; font-size: 12px; margin-left: 1em; margin-right: 1em;">
They're always looking for ways to up their game, too. The new ghd styler, launched in 2007, represents the next generation of ghd. With a rounder barrel for even better curling, a sleep mode for no-fear styling, and universal voltage so you can take it anywhere, the new ghd styler is as hot as it gets, both literally and figuratively.
</p>
<p style="color: #ffffff; font-size: 12px; margin-left: 1em; margin-right: 1em;">
Not to be forgotten, ghd styling products are equally as instrumental in attaining perfect ghd hair. With advanced heat-activated protection properties, ghd thermodynamics formulas let you expand your creativity with fantastic results, while ensuring hair stays in great condition.
</p>
<p style="color: #ffffff; font-size: 12px; margin-left: 1em; margin-right: 1em;">
Beyond revolutionizing the everyday hair experience, ghd directs styles for Fashion Week collections in New York, London, Paris, and Sydney, and styles shoots for the world's top, glossy magazines. Celebrities love them, the media raves about them, and stylists insist upon them-it's no wonder that everyone wants to follow the ghd gospel.       
</p>
<p style="color: #ffffff; font-size: 12px; margin-left: 1em; margin-right: 1em;">
Desired around the world and insisted upon by the top fashion stylists, ghd has revolutionized styling with their multipurpose manestays, which have dominated catwalks, photo shoots, and personal vanities since 2001. So versatile and handy that you basically never need to sport the same style twice, ghd styling irons and brushes use ghd thermodynamics, which actually improves the condition and look of your locks in conjunction with heat styling. From scoring runway-worthy creations to attaining everyday perfection, ghd has the goods to make every hair day a good one. 
</p>

</td>
<td  style="">
<img src="/images/ghd_history.png"/>
</td>
</tr>
<tr>
<td colspan="2">
<img src="./images/ghd_bottom.png"/> 
</td>
</tr>
</table>
 
 
