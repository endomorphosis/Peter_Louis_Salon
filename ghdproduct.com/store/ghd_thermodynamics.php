<? include($template_include_file);?>
<table width="720" cellspacing="0" cellpadding="0" style=" background-repeat:repeat-x; max-width: 720px;">
<tr>
<td>
<img src="/images/ghd_thermodynamics.png"/>
</td>
</tr>
<td  style="background: url(/images/ghd_gradient.png); backround-repeat: repeat-x;  background-color: #3a3a3a;">
</p>
<p style="color: #ffffff; font-size: 12px; margin-left: 1em; margin-right: 1em;">
Stay cool with ghd thermodynamics protection. ghd believe that hair shouldn't suffer for looking great. That's why we have developed a comprehensive range of products designed to protect all types of hair from potential heat damage.
</p>
<p style="color: #ffffff; font-size: 12px; margin-left: 1em; margin-right: 1em;">
The 4-step process within thermodynamics will protect the hair at every stage from step 1 cleanse & nourish, step 2 protect, step 3 style and step 4 finish. So you can create red hot looks without worrying about damaging your hair - or your reputation.
</p>
<p style="color: #ffffff; font-size: 12px; margin-left: 1em; margin-right: 1em;">
<table>
<tr>
<td style="text-align: center">
<br/><img src="./images/ghd_thermodynamics00.png"/><br/>
</td>
<td>
</p>
<p style="color: #f7941e; font-weight: 700; font-size: 14px; margin-left: 1em; margin-right: 1em;">
STEP 1 Cleanse & Nourish
</p>
<p style="color: #ffffff; font-size: 12px; margin-left: 1em; margin-right: 1em;">
Prescriptive ghd shampoos, conditioners and treatments replenish hair radiance by restoring moisture and shine for everlasting health. With sunflower seed extract to protect hair from UVA and UV damage, Step 1 offers divine healing properties while locking in intense colour.
</p>
<p style="color: #ffffff; font-size: 12px; margin-left: 1em; margin-right: 1em;">
</td>
</tr>
<tr>
<td style="text-align: center">
<br/><img src="./images/ghd_thermodynamics01.png"/><br/>
</td>
<td>
</p>
<p style="color: #f7941e; font-weight: 700; font-size: 14px;; margin-left: 1em; margin-right: 1em;">
STEP 2 Protect
</p>
<p style="color: #ffffff; font-size: 12px; margin-left: 1em; margin-right: 1em;">
ghd thermal protectors defend hair against heat damage with an invisible armour. Their Unique Heat Shield Protection technology protects the hair from any heat styling appliance, while secret ingredients smooth and condition hair for a healthy glow.
</p>
<p style="color: #ffffff; font-size: 12px; margin-left: 1em; margin-right: 1em;">
</td>
</tr>
<tr>
<td style="text-align: center">
<br/><img src="./images/ghd_thermodynamics10.png"/><br/>
</td>
<td>
</p>
<p style="color: #f7941e; font-weight: 700; font-size: 14px; margin-left: 1em; margin-right: 1em;">
STEP 3 Style
</p>
<p style="color: #ffffff; font-size: 12px; margin-left: 1em; margin-right: 1em;">
Heat-activated ghd styling sprays, balms, lotions and mousse create exquisite looks on healthy, protected hair. Meadow seed oil delivers intense, everlasting moisture and supple elasticity whilst sunflower seed extract protects hair from UVA and UV damage and helps hair retain colour vibrancy.
</p>
<p style="color: #ffffff; font-size: 12px; margin-left: 1em; margin-right: 1em;">
</td>
</tr>
<tr>
<td style="text-align: center">
<br/><img src="./images/ghd_thermodynamics11.png"/><br/><br/>
</td>
<td>
</p>
<p style="color: #f7941e; font-weight: 700; font-size: 14px; margin-left: 1em; margin-right: 1em;">
STEP 4 Finish
</p>
<p style="color: #ffffff; font-size: 12px; margin-left: 1em; margin-right: 1em;">
Finishing waxes, serums and sprays polish hair to perfection whilst protecting from heat. Complete UV protection is the ultimate finishing touch.
</p>
<p style="color: #ffffff; font-size: 12px; margin-left: 1em; margin-right: 1em;">
</td>
</tr>
</table>

</p>

</td>
</tr>
<tr>
<td >
<img src="./images/ghd_bottom.png"/> 
</td>
</tr>
</table>
 
 
