<? include($template_include_file);?>
<table width="720" cellspacing="0" cellpadding="0" style=" background-repeat:repeat-x; max-width: 720px;">
<tr>
<td>
</td>
</tr>
<td  style="background: url(/images/ghd_gradient.png); backround-repeat: repeat-x;  background-color: #3a3a3a;">
<p style="color: #ffffff; font-size: 14px; font-weight: 700; margin-left: 1em; margin-right: 1em;"><br/> HOW-TO VIDEOS</p>
<p style="color: #ffffff; font-size: 12px; margin-left: 1em; margin-right: 1em;">
Finally, there's a tool that allows you to shape your mane according to your mood. Feeling sexy? Go for sleek, straight styles. Or-for fits of fun and flirtation-pump up the volume with big, bouncy curls. No matter what style you're headed for, ghd stylers can create them in a hot minute. Watch these how-to videos to score four universally gorgeous looks. You'll fool everyone with your straight-from-the-salon allure </p>
<style>
.flash{
text-decoration: none;
position: relative;
left: 1em;
padding-top: 4px;
padding-bottom: 4px;

}
.flash:hover{
text-decoration: underline;
font: #ff0000;
position: relative;
left: 1em;

}
</style>
<center><table cellspacing="0" style=" border: solid 3px #ffffff; background-color: #ffffff;">
<tr style="background-color: #aaaaaa; ">
<td colspan="2">
<p style="padding-top: 4px; padding-bottom: 4px; color: #666666; font-weight: 700; font-size: 14px; margin-left: 1em; margin-right: em;">GHD IN: <span style="color: ff0000;">HOW-TO VIDEOS</span></p>
</td>
</tr>
<tr style=" margin-left: 1em; margin-right: 1em; background-color: #aaaaaa; ">
<td>
<span class="flash" onclick=" flashembed('flash',{ src:'./FlowPlayerDark.swf', width: 600, height: 480 }, {config: { autoPlay: true, autoBuffering: true,
controlBarBackgroundColor:'0x3a3a3a', initialScale: 'scale', videoFile: './style1_straight.flv' }} ); "><a>>&nbsp;The Look: This Year's Straight</a></span>
</td>
<td>
<span class="flash" onclick=" flashembed('flash',{ src:'./FlowPlayerDark.swf', width: 600, height: 480 }, {config: { autoPlay: true, autoBuffering: true,
controlBarBackgroundColor:'0x3a3a3a', initialScale: 'scale', videoFile: './style2_volume.flv' }} ); "><a>>&nbsp;The Look: The Volumizer</a></span>
</td>
</tr>
<tr style="margin-left: 1em; margin-right: 1em; background-color: #aaaaaa; ">
<td>
<span class="flash" onclick=" flashembed('flash',{ src:'./FlowPlayerDark.swf', width: 600, height: 480 }, {config: { autoPlay: true, autoBuffering: true,
controlBarBackgroundColor:'0x3a3a3a', initialScale: 'scale', videoFile: './style3_curls.flv' }} ); "><a>>&nbsp;The Look: High-Glamour Curls</a></span>
</td>
<td>
<span class="flash" onclick=" flashembed('flash',{ src:'./FlowPlayerDark.swf', width: 600, height: 480 }, {config: { autoPlay: true, autoBuffering: true,
controlBarBackgroundColor:'0x3a3a3a', initialScale: 'scale', videoFile: './style4_waves.flv' }} ); "><a>>&nbsp;The Look: The Summer Waves</a></span>
</td>
</tr>
<tr>
<td colspan="2">
<p><!-- 
	A minimal setup to get you started. This configuration is the same
	as in our Quick Start documentation:
	
	http://flowplayer.org/player/quick-start.html
--><!-- 
		include flashembed - which is a general purpose tool for 
		inserting Flash on your page. Following line is required.
	--><script src="./flashembed.min.js" type="text/javascript"></script></p><!-- some minimal styling, can be removed --><p><link rel="stylesheet" type="text/css" href="/css/common.css" /><script type="text/javascript">
	
	 /*
		use flashembed to place flowplayer into HTML element 
		whose id is "example" (below this script tag)
	 */
document.onload = flashembed('flash',{ src:'./FlowPlayerDark.swf', width: 600, height: 480 }, {config: { autoPlay: false, autoBuffering: false,
controlBarBackgroundColor:'0x3a3a3a', initialScale: 'scale', videoFile: './style4_waves.flv' }} );

window.onload = flashembed('flash',{ src:'./FlowPlayerDark.swf', width: 600, height: 480 }, {config: { autoPlay: false, autoBuffering: false,
controlBarBackgroundColor:'0x3a3a3a', initialScale: 'scale', videoFile: './style4_waves.flv' }} );

document.body.onload = flashembed('flash',{ src:'./FlowPlayerDark.swf', width: 600, height: 480 }, {config: { autoPlay: false, autoBuffering: false,
controlBarBackgroundColor:'0x3a3a3a', initialScale: 'scale', videoFile: './style4_waves.flv' }} );
	 flashembed("flash", 
	
		/* 
			first argument supplies standard Flash parameters. See full list:
			http://kb.adobe.com/selfservice/viewContent.do?externalId=tn_12701
		*/
		{
			src:'./FlowPlayerDark.swf',
			width: 600, 
			height: 480
		},
		
		/*
			second argument is Flowplayer specific configuration. See full list:
			http://flowplayer.org/player/configuration.html
		*/
		{config: {   
			autoPlay: false,
			autoBuffering: false,
			controlBarBackgroundColor:'0x3a3a3a',
			initialScale: 'scale',
			videoFile: './style1_straight.flv'
		}} 
	);
// </script></p>

<p></p>

<div id="flash"></div><br/></td></tr></table></center><br/>


</td>
</tr>
<tr>
<td >
<img src="./images/ghd_bottom.png"/> 
</td>
</tr>
</table>
 
 
