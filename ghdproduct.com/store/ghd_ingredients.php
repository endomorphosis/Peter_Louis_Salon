<? include($template_include_file);?>
<table width="720" cellspacing="0" cellpadding="0" style=" background-repeat:repeat-x; max-width: 720px;">
<tr>
<td  style="background: url(/images/ghd_gradient.png); backround-repeat: repeat-x;  background-color: #3a3a3a;">
<p style="color: #ffffff; font-size: 14px; margin-left: 1em; margin-right: 1em;">
Every brand has a defining ingredient that sets it apart and makes it unique. Whether it's the special polymer that magically makes your lashes curl, the peptide that depuffs your tired eyes, or the vitamin complex that turns your dull hair shiny, it's those special, signature ingredients that often mean the difference between a product you love and one you simply can't live without. Read on to learn more about the special somethings that make your favorite products so darn fabulous.
</p>
<p style="color: #ffffff; font-size: 14px; margin-left: 1em; margin-right: 1em;">
The world's first hair beauty program designed for heat-styling (including hairdryers, styling irons, and beyond), ghd thermodynamics helps you manage your hair beautifully and safely, using four simple steps: Cleanse and Nourish (cream-colored packaging), Protect (gold packaging), Style (purple packaging), and Finish (black packaging).
</p>
<p style="color: #ffffff; font-size: 14px; margin-left: 1em; margin-right: 1em;">
Throughout the multi-step line, sunflower seed extract is a key ingredient, which provides hair with UV protection and prevents damage and color-fade. It also gives long-lasting shine and condition to hair.
</p>
<p style="color: #ffffff; font-size: 14px; margin-left: 1em; margin-right: 1em;">
The ghd thermal protectors contain heat-deflecting sodium polystyrene, which forms a strong, thermally stable, heat-absorbing film around each hair. This shrink-wrap effect also protects the hair from dehydration and the effects of stress.
</p>
<p style="color: #ffffff; font-size: 14px; margin-left: 1em; margin-right: 1em;">
All of the ghd styling products contain meadowfoam seed oil, which provides heat-activated protection and penetrates the hair cortex, improving moisture over an extended time and adding elasticity to the hair.
</p>
<p style="color: #ffffff; font-size: 14px; margin-left: 1em; margin-right: 1em;">
So no matter how much heat your hair routine is packing, you can be sure that ghd thermodynamics is on damage control from start to finish. 
</p>

</td>
<td  style="">
<img src="/images/ghd_ingredients.png"/>
</td>
</tr>
<tr>
<td colspan="2">
<img src="./images/ghd_bottom.png"/> 
</td>
</tr>
</table>
 
 
