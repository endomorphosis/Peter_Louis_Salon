<style>
table, tr, td, img{
padding: 0;
margin: 0;
border: 0;
}

tr{
vertical-align: top; padding: 0; margin: 0px; width: 720px; border-width: 0px; font-size: 12px; color: #ffffff; 
}

td{
vertical-align: top; padding: 0; margin: 0px; font-size: 12px; color: #ffffff; 
}

</style>
<table style=" vertical-align: top; padding: 0px; margin: 0px; width: 720px; border-width: 0px; font-size: 12px; color: #ffffff; background-color: #0e891e; " cellspacing="0">
<tr>
<td colspan="2"><? include($template_include_file);?>
</td>
</tr>
<tr>
<td><br/>

<p style="color: #ffffff; font-size: 12px; margin-left: 1em; margin-right: 1em;">
<font style="font-weight: 700;" >How-To</font>
</p>
<p style="color: #ffffff; font-size: 12px; margin-left: 1em; margin-right: 1em;">
Maintaining healthy hair isn't always as simple as "lather, rinse, repeat." Therefore, Rene Furterer integrated some pampering pick-me-ups into his expert regimens to give your tresses a little TLC. From luxurious products to self-spoiling routines, when you treat yourself to these invigorating activities, you'll get brilliant results.
</p>
<p style="color: #ffffff; font-size: 12px; margin-left: 1em; margin-right: 1em;">
<font style="font-weight: 700;">TRESSED OUT</font><br>Three Relaxing Ways to Maximize Your Mane</p>
</p>

<p style="color: #ffffff; font-size: 12px; margin-left: 1em; margin-right: 1em;">
<hr></p>
<p style="color: #ffffff; font-size: 12px; margin-left: 1em; margin-right: 1em;">	
<font style="font-weight: 700;" >INDULGE</font><br> Complexe 5 is a cornerstone product in the René Furterer collection and is a delight for the scalp and the senses. Before you shampoo, massage the extract into your hair once a week. The refreshing aromas of orange, lavender, and verbena will stimulate in more ways than one.
</p>
<p style="color: #ffffff; font-size: 12px; margin-left: 1em; margin-right: 1em;">
<a href="http://www.peterlouissalon.com/template.php?f=prod_detail&ref=rene&pid=104"><img src="http://a248.g.akamai.net/7/248/8278/20060807214917/www.sephora.com/assets/brands/bh/C14787_1.jpg" border="0" height="150" width="150"></a><br>
<a href="http://www.peterlouissalon.com/template.php?f=prod_detail&ref=rene&pid=104">Complexe 5 Regenerating Extract</a></p>

<p style="color: #ffffff; font-size: 12px; margin-left: 1em; margin-right: 1em;">A complete treat for your tresses, these exceptionally beneficial steps will help you take hold of your unruly hair and help you unwind in the process.
</p>


</td>
<td valign="bottom" style=" vertical-align: bottom; background-color: #000000;">
<img src="./images/howto.png"/>
</td>
</tr>
<tr>
<td colspan="2">
<table>
<tr>
<td>
<table>
<tr ><td colspan="2"><hr></td></tr>	<tr><td><br/></td></tr>
	<tr ><td colspan="2"><font style="font-weight: 700;" >MASSAGE</font><br>Special massages can get you maximally relaxed <i>and</i> stimulate microcirculation to increase treatment penetration—a win-win hair situation. But don't fear if you're not the "kneady" type. Try these simple self-massages to heighten your manipulative skills and enliven your scalp. No fancy finger work needed.

</td></tr><tr><td><br/></td></tr>
	<tr >
		<td align="left" valign="top"><img src="http://a248.g.akamai.net/7/248/8278/20060807214918/www.sephora.com/assets/brands/bh/C14787_img1.jpg" border="0" height="150" width="150"></td>
		
		<td>With your fingers together, slowly massage your scalp in a circular motion as if you're trying to push the nape of the neck up to the top of the head.</td>
	</tr>		
	<tr >
		<td align="left" valign="top"><img src="http://a248.g.akamai.net/7/248/8278/20060807214918/www.sephora.com/assets/brands/bh/C14787_img2.jpg" border="0" height="150" width="150"></td>

		
		<td>Now make small, side-to-side vibrating movements from the forehead toward the top of the head. This tones the scalp. </td>
	</tr>		
	<tr >
		<td align="left" valign="top"><img src="http://a248.g.akamai.net/7/248/8278/20060807214918/www.sephora.com/assets/brands/bh/C14787_img3.jpg" border="0" height="150" width="150"></td>
		
		<td>Finally, with both hands flat, finish the massage by exerting pressure on the scalp and then letting your hands slide from the forehead toward the nape of the neck.</td>
	</tr>		<tr><td><br/></td></tr>
</table>
</td>
<td>
<table>
<tr ><td colspan="2"><hr></td></tr>	<tr><td><br/></td></tr>
	<tr ><td colspan="2"><font style="font-weight: 700;" >BRUSH</font><br>Give bothersome buildups the big brush off. Daily brushing is an important practice for healthy hair, as it eliminates dust and loose strands while restoring volume. Keep your tresses in top shape with these tips for a clean sweep.  

</td></tr><tr><td><br/></td></tr>
	<tr >
		<td align="left" valign="top"><img src="http://a248.g.akamai.net/7/248/8278/20060807214918/www.sephora.com/assets/brands/bh/C14787_img4.jpg" border="0" height="150" width="150"></td>
		
		<td>With one brush in each hand, and the head forward, brush hair from the nape of the neck toward the forehead.</td>
	</tr>		
	<tr >
		<td align="left" valign="top"><img src="http://a248.g.akamai.net/7/248/8278/20060807214918/www.sephora.com/assets/brands/bh/C14787_img5.jpg" border="0" height="150" width="150"></td>

		
		<td>Then brush from the sides toward the top of the head. </td>
	</tr>		
	<tr  width="400">
		<td align="left" valign="top"><img src="http://a248.g.akamai.net/7/248/8278/20060807214918/www.sephora.com/assets/brands/bh/C14787_img6.jpg" border="0" height="150" width="150"></td>
		
		<td width="250">Finish by brushing from the forehead toward the nape of the neck. (Be sure to brush gently without pulling on hair and without causing friction to the scalp.)</td>
	</tr>	
<tr><td><br/></td></tr>
</table></table>
</td>
</tr>
<tr  width="400">
<td>
<img src="./images/rene10.png"/>
</td>
<td>
<img src="./images/rene11.png"/>
</td>
</tr>
<tr  width="400">
<td>
</td>
<td>
</td>
</tr>
</table>