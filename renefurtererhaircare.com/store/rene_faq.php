<style>
table, tr, td, img{
padding: 0;
margin: 0;
border: 0;
}

tr{
vertical-align: top; padding: 0; margin: 0px; width: 720px; border-width: 0px; font-size: 12px; color: #ffffff; 
}

td{
vertical-align: top; padding: 0; margin: 0px;  border-width: 0px; font-size: 12px; color: #ffffff; 
}

</style>
<table style=" vertical-align: top; padding: 0px; margin: 0px; width: 720px; border-width: 0px; font-size: 12px; color: #ffffff; background-color: #0e891e; " cellspacing="0">
<tr>
<td colspan="2"><? include($template_include_file);?>
</td>
</tr>
<tr>
<td>
<style>
.contenu15_template{
width: 10px;
padding-left: 5px;
padding-right: 5px;
}
.faqtext{
width: 400px;
}
</style>
<table  bgcolor="" border="0" cellpadding="0" cellspacing="0">

		<tbody><tr>

		

		<td style="" align="left" bgcolor="">
				<br/>
				<table  cellpadding="0" id="td0" border="0" cellspacing="0" >
				<tbody><tr>
					<td class="contenu15_template" align="left" valign="top" width="1"><img bgcolor="" src="/images/cma/puce_faq_off.gif" valign="top" align="left">
					</td>

					
<td class="faqtext" width="300"  align="left" height="1" valign="middle"  ><a href="#q0" onclick="javascript:VisibleInvisible31(0)">Does COMPLEXE 5 concentrate of active essential oils leave hair oily?</a></td>
						
			</tr>

				
					</tbody></table>  
											   		
					<table style="display: none;" id="31t0" border="0" >
						<tbody><tr><td class="faqtext" colspan="2" align="left">
						<div style="padding: 5px 0px; display: none;" class="text_non_opaque" id="31champ0" align="left">No, the essential oils have an affinity with sebum as well as high penetration power into the scalp.
In addition, the term « oil » in « essential oils » does not correspond to the chemical definition, but to the fact that the essences are soluble in oil and fat.</font>
						</div>
						</td></tr>
					</tbody></table>
		
			</td>

		</tr>
		<tr>
			<td colspan="2"><img src="/vig/images/vide.gif" height="5"></td>
		</tr>		

		<tr>

		<td style="" align="left" bgcolor="">
				
				<table cellpading="0" id="td1" border="0" cellspacing="0" >
				<tbody><tr>
					<td class="contenu15_template" align="left" valign="top" width="1"><img bgcolor="" src="/images/cma/puce_faq_off.gif" valign="top" align="left">
					</td>

					
					<td class="faqtext" align="left" height="1" valign="middle" ><a href="#q1" onclick="javascript:VisibleInvisible31(1)">Hair should not be washed every day.</a></td>
						
			</tr>

				
					</tbody></table>  
											   		
					<table style="display: none;" id="31t1" border="0" >
						<tbody><tr><td class="faqtext" colspan="2" align="left">
						<div style="padding: 5px 0px; display: none;" class="text_non_opaque" id="31champ1" align="left">False.  You can wash your hair every day with a single-application frequent-use shampoo between treatment shampoos, applied by delicately massaging the scalp without friction.</font>
						</div>
						</td></tr>
					</tbody></table>
		
			</td>
		</tr>
		<tr>
			<td colspan="2"><img src="/vig/images/vide.gif" height="5"></td>
		</tr>		

		<tr>

		

		
		<td style="" align="left" bgcolor="">
				
				<table cellpading="0" id="td2" border="0" cellspacing="0" >
				<tbody><tr>
					<td class="contenu15_template" align="left" valign="top" width="1"><img bgcolor="" src="/images/cma/puce_faq_off.gif" valign="top" align="left">
					</td>

					
					<td class="faqtext" align="left" height="1" valign="middle" ><a href="#q2" onclick="javascript:VisibleInvisible31(2)">Do color treatments cause hair to fall out?</a></td>
						
			</tr>

				
					</tbody></table>  
											   		
					<table style="display: none;" id="31t2" border="0" >
						<tbody><tr><td class="faqtext" colspan="2" align="left">
						<div style="padding: 5px 0px; display: none;" class="text_non_opaque" id="31champ2" align="left">No.  Color treatments have no deep-down action at the roots.  Color adheres to hair alone.</font>
						</div>
						</td></tr>
					</tbody></table>
		
			</td>
		</tr>
		<tr>
			<td colspan="2"><img src="/vig/images/vide.gif" height="5"></td>
		</tr>		

		<tr>

		

		<td style="" align="left" bgcolor="">
				
				<table cellpading="0" id="td3" border="0" cellspacing="0" >
				<tbody><tr>
					<td class="contenu15_template" align="left" valign="top" width="1"><img bgcolor="" src="/images/cma/puce_faq_off.gif" valign="top" align="left">
					</td>

					
					<td class="faqtext" align="left" height="1" valign="middle" ><a href="#q3" onclick="javascript:VisibleInvisible31(3)">Does rinse-off after-shampoo care weigh down hair?</a></td>
						
			</tr>

				
					</tbody></table>  
											   		
					<table style="display: none;" id="31t3" border="0" >
						<tbody><tr><td class="faqtext" colspan="2" align="left">
						<div style="padding: 5px 0px; display: none;" class="text_non_opaque" id="31champ3" align="left">No.  You should apply these products in small amounts, only to hair lengths and hair ends, and not to the scalp, and rinse off thoroughly.</font>
						</div>
						</td></tr>
					</tbody></table>
		
			</td>

		</tr>
		<tr>
			<td colspan="2"><img src="/vig/images/vide.gif" height="5"></td>
		</tr>		

		<tr>

		


		<td style="" align="left" bgcolor="">
				
				<table cellpading="0" id="td4" border="0" cellspacing="0" >
				<tbody><tr>
					<td class="contenu15_template" align="left" valign="top" width="1"><img bgcolor="" src="/images/cma/puce_faq_off.gif" valign="top" align="left">
					</td>

					
					<td class="faqtext" align="left" height="1" valign="middle" ><a href="#q4" onclick="javascript:VisibleInvisible31(4)">Why do styling products tend to dry out hair?</a></td>
						
			</tr>

				
					</tbody></table>  
											   		
					<table style="display: none;" id="31t4" border="0" >
						<tbody><tr><td class="faqtext" colspan="2" align="left">
						<div style="padding: 5px 0px; display: none;" class="text_non_opaque" id="31champ4" align="left">This process is linked to the presence of alcohol, which in association with hair setting resins, produces a drying out effect.
Rene FURTERER's styling products have the advantage of setting hair without drying it out thanks to Anatide (exclusive Rene Furterer patent), a protective and anti-dryness active ingredient, and to Vitamin B5.</font>
						</div>
						</td></tr>
					</tbody></table>
		
			</td>
		</tr>
		<tr>
			<td colspan="2"><img src="/vig/images/vide.gif" height="5"></td>
		</tr>		

		<tr>

		

		<td style="" align="left" bgcolor="">
				
				<table cellpading="0" id="td5" border="0" cellspacing="0" >
				<tbody><tr>
					<td class="contenu15_template" align="left" valign="top" width="1"><img bgcolor="" src="/images/cma/puce_faq_off.gif" valign="top" align="left">
					</td>

					
					<td class="faqtext" align="left" height="1" valign="middle" ><a href="#q5" onclick="javascript:VisibleInvisible31(5)">I haven't found a solution for my oily roots and dry ends.  Should I use a shampoo for combination hair?</a></td>
						
			</tr>

				
					</tbody></table>  
											   		
					<table style="display: none;" id="31t5" border="0" >
						<tbody><tr><td class="faqtext" colspan="2" align="left">
						<div style="padding: 5px 0px; display: none;" class="text_non_opaque" id="31champ5" align="left">You must use a suitable shampoo for oily scalp (like Curbicia), and a specific treatment for dry hair (Carthame or Karite).</font>
						</div>
						</td></tr>
					</tbody></table>
		
			</td>
		</tr>
		<tr>
			<td colspan="2"><img src="/vig/images/vide.gif" height="5"></td>
		</tr>		

		<tr>

		


		<td style="" align="left" bgcolor="">
				
				<table cellpading="0" id="td6" border="0" cellspacing="0" >
				<tbody><tr>
					<td class="contenu15_template" align="left" valign="top" width="1"><img bgcolor="" src="/images/cma/puce_faq_off.gif" valign="top" align="left">
					</td>

					
					<td class="faqtext" align="left" height="1" valign="middle" ><a href="#q6" onclick="javascript:VisibleInvisible31(6)">I have dry hair and I am using a shampoo for dry hair.  Am I right to do so?</a></td>
						
			</tr>

				
					</tbody></table>  
											   		
					<table style="display: none;" id="31t6" border="0" >
						<tbody><tr><td class="faqtext" colspan="2" align="left">
						<div style="padding: 5px 0px; display: none;" class="text_non_opaque" id="31champ6" align="left">You should use a suitable shampoo for your scalp, irrespective of your hair type.</font>
						</div>
						</td></tr>
					</tbody></table>
		
			</td>
		</tr>
		<tr>
			<td colspan="2"><img src="/vig/images/vide.gif" height="5"></td>
		</tr>		

		<tr>

		

		<td style="" align="left" bgcolor="">
				
				<table cellpading="0" id="td7" border="0" cellspacing="0" >
				<tbody><tr>
					<td class="contenu15_template" align="left" valign="top" width="1"><img bgcolor="" src="/images/cma/puce_faq_off.gif" valign="top" align="left">
					</td>

					
					<td class="faqtext" align="left" height="1" valign="middle" ><a href="#q7" onclick="javascript:VisibleInvisible31(7)">Can I use my treatment shampoo every day?</a></td>
						
			</tr>

				
					</tbody></table>  
											   		
					<table style="display: none;" id="31t7" border="0" >
						<tbody><tr><td class="faqtext" colspan="2" align="left">
						<div style="padding: 5px 0px; display: none;" class="text_non_opaque" id="31champ7" align="left">No.  The treatment shampoo should be used once to twice a week, in 2 applications.  As an alternative, for all other days of the week, use a frequent-use shampoo (Naturia) in a single application.</font>
						</div>
						</td></tr>
					</tbody></table>
		
			</td>
		</tr>
		<tr>
			<td colspan="2"><img src="/vig/images/vide.gif" height="5"></td>
		</tr>		

		<tr>

		


		<td style="" align="left" bgcolor="">
				
				<table cellpading="0" id="td8" border="0" cellspacing="0" >
				<tbody><tr>
					<td class="contenu15_template" align="left" valign="top" width="1"><img bgcolor="" src="/images/cma/puce_faq_off.gif" valign="top" align="left">
					</td>

					
					<td class="faqtext" align="left" height="1" valign="middle" ><a href="#q8" onclick="javascript:VisibleInvisible31(8)">Are Rene FURTERER's tested on animals?</a></td>
						
			</tr>

				
					</tbody></table>  
											   		
					<table style="display: none;" id="31t8" border="0" >
						<tbody><tr><td class="faqtext" colspan="2" align="left">
						<div style="padding: 5px 0px; display: none;" class="text_non_opaque" id="31champ8" align="left">No.  Our products are subject to in vitro or ex-vivo studies, followed by volunteer testing under dermatological supervision.</font>
						</div>
						</td></tr>
					</tbody></table>
		
			</td>

		</tr>
		<tr>
			<td colspan="2"><img src="/vig/images/vide.gif" height="5"></td>
		</tr>		

		<tr>

		


		<td style="" align="left" bgcolor="">
				
				<table cellpading="0" id="td9" border="0" cellspacing="0" >
				<tbody><tr>
					<td class="contenu15_template" align="left" valign="top" width="1"><img bgcolor="" src="/images/cma/puce_faq_off.gif" valign="top" align="left">
					</td>

					
					<td class="faqtext" align="left" height="1" valign="middle" ><a href="#q9" onclick="javascript:VisibleInvisible31(9)">Hair loss treatments are pointless.  I will end up bald like my father!</a></td>
						
			</tr>

				
					</tbody></table>  
											   		
					<table style="display: none;" id="31t9" border="0" >
						<tbody><tr><td class="faqtext" colspan="2" align="left">
						<div style="padding: 5px 0px; display: none;" class="text_non_opaque" id="31champ9" align="left">Hair loss treatments for hereditary hair loss (Triphasic) help delay hair loss and consequently maintain hair capital for longer.</font>
						</div>
						</td></tr>
					</tbody></table>
		
			</td>

		</tr>
		<tr>
			<td colspan="2"><img src="/vig/images/vide.gif" height="5"></td>
		</tr>		

		<tr>

		


		<td style="" align="left" bgcolor="">
				
				<table cellpading="0" id="td10" border="0" cellspacing="0" >
				<tbody><tr>
					<td class="contenu15_template" align="left" valign="top" width="1"><img bgcolor="" src="/images/cma/puce_faq_off.gif" valign="top" align="left">
					</td>

					
					<td class="faqtext" align="left" height="1" valign="middle" ><a href="#q10" onclick="javascript:VisibleInvisible31(10)">After implants, is it necessary to continue maintaining and strengthening your hair?</a></td>
						
			</tr>

				
					</tbody></table>  
											   		
					<table style="display: none;" id="31t10" border="0" >
						<tbody><tr><td class="faqtext" colspan="2" align="left">
						<div style="padding: 5px 0px; display: none;" class="text_non_opaque" id="31champ10" align="left">Yes.  You can absolutely continue to maintain your hair with hair care.</font>
						</div>
						</td></tr>
					</tbody></table>
		
			</td>

		</tr>
		<tr>
			<td colspan="2"><img src="/vig/images/vide.gif" height="5"></td>
		</tr>		

		<tr>

		


		<td style="" align="left" bgcolor="">
				
				<table cellpading="0" id="td11" border="0" cellspacing="0" >
				<tbody><tr>
					<td class="contenu15_template" align="left" valign="top" width="1"><img bgcolor="" src="/images/cma/puce_faq_off.gif" valign="top" align="left">
					</td>

					
					<td class="faqtext" align="left" height="1" valign="middle" ><a href="#q11" onclick="javascript:VisibleInvisible31(11)">Can I apply hair loss treatment during chemotherapy?</a></td>
						
			</tr>

				
					</tbody></table>  
											   		
					<table style="display: none;" id="31t11" border="0" >
						<tbody><tr><td class="faqtext" colspan="2" align="left">
						<div style="padding: 5px 0px; display: none;" class="text_non_opaque" id="31champ11" align="left">No.  You must wait for one month after ending your chemotherapy in order to begin hair loss treatment.</font>
						</div>
						</td></tr>
					</tbody></table>
		
			</td>
		</tr>
		<tr>
			<td colspan="2"><img src="/vig/images/vide.gif" height="5"></td>
		</tr>		

		<tr>

		


		<td style="" align="left" bgcolor="">
				
				<table cellpading="0" id="td12" border="0" cellspacing="0" >
				<tbody><tr>
					<td class="contenu15_template" align="left" valign="top" width="1"><img bgcolor="" src="/images/cma/puce_faq_off.gif" valign="top" align="left">
					</td>

					
					<td class="faqtext" align="left" height="1" valign="middle" ><a href="#q12" onclick="javascript:VisibleInvisible31(12)">I have an oily scalp with dandruff and my hair has been color-treated.  What type of shampoo should I use?</a></td>
						
			</tr>

				
					</tbody></table>  
											   		
					<table style="display: none;" id="31t12" border="0" >
						<tbody><tr><td class="faqtext" colspan="2" align="left">
						<div style="padding: 5px 0px; display: none;" class="text_non_opaque" id="31champ12" align="left">You must use the Melaleuca shampoo for oily dandruff twice a week, and as an alternative, if needed, the Okara Protect Color shampoo for color treated hair. 
After one month of use, replace the anti-dandruff Melaleuca shampoo with Curbicia shampoo for oily scalp.</font>
						</div>
						</td></tr>
					</tbody></table>
		
			</td>

		</tr>
		<tr>
			<td colspan="2"><img src="/vig/images/vide.gif" height="5"></td>
		</tr>		

		<tr>

		


		<td style="" align="left" bgcolor="">
				
				<table cellpading="0" id="td13" border="0" cellspacing="0" >
				<tbody><tr>
					<td class="contenu15_template" align="left" valign="top" width="1"><img bgcolor="" src="/images/cma/puce_faq_off.gif" valign="top" align="left">
					</td>

					
					<td class="faqtext" align="left" height="1" valign="middle" ><a href="#q13" onclick="javascript:VisibleInvisible31(13)">I was recommended a treatment shampoo used in 2 applications on the scalp.  Why?</a></td>
						
			</tr>

				
					</tbody></table>  
											   		
					<table style="display: none;" id="31t13" border="0" >
						<tbody><tr><td class="faqtext" colspan="2" align="left">
						<div style="padding: 5px 0px; display: none;" class="text_non_opaque" id="31champ13" align="left">The 1st application washes and the second treats the scalp, hence the need to be left in for 3 minutes.</font>
						</div>
						</td></tr>
					</tbody></table>
		
			</td>

		</tr>
		<tr>
			<td colspan="2"><img src="/vig/images/vide.gif" height="5"></td>
		</tr>		

		<tr>

		


		<td style="" align="left" bgcolor="">
				
				<table cellpading="0" id="td14" border="0" cellspacing="0" >
				<tbody><tr>
					<td class="contenu15_template" align="left" valign="top" width="1"><img bgcolor="" src="/images/cma/puce_faq_off.gif" valign="top" align="left">
					</td>

					
					<td class="faqtext" align="left" height="1" valign="middle" ><a href="#q14" onclick="javascript:VisibleInvisible31(14)">Can Rene FURTERER's products be used on African-American hair?</a></td>
						
			</tr>

				
					</tbody></table>  
											   		
					<table style="display: none;" id="31t14" border="0" >
						<tbody><tr><td class="faqtext" colspan="2" align="left">
						<div style="padding: 5px 0px; display: none;" class="text_non_opaque" id="31champ14" align="left">Yes, especially the Karite product range.</font>
						</div>
						</td></tr>
					</tbody></table>
		
			</td>

		</tr>
		<tr>
			<td colspan="2"><img src="/vig/images/vide.gif" height="5"></td>
		</tr>		

		
		<script>
		function Invisible31() {
			var dodu = document.getElementsByTagName("div");
			var totote = document.getElementsByTagName("table");
			for(i=0;i<15;i++){
				dodu[31+'champ'+i].style.display = 'none';
				totote[31+'t'+i].style.background = '';
				totote[31+'t'+i].style.display = 'none'; 
			}
		}
		Invisible31();
		function VisibleInvisible31(k) {
			var doudou = document.getElementsByTagName("div");
			var totote = document.getElementsByTagName("table");  
			var d = doudou[31+'champ'+k].style.display;
			
			Invisible31();
			
			doudou[31+'champ'+k].style.display =(d=='none'?'block':'none');
			totote[31+'t'+k].style.display = doudou[31+'champ'+k].style.display;
			
			if (navigator.appName=="Microsoft Internet Explorer"){
					totote[31+'t'+k].style.background="url(/images/pix_transparent.gif)";
					totote[31+'t'+k].style.filter="progid:DXImageTransform.Microsoft.Alpha(opacity=50);"
			}else{
					totote[31+'t'+k].setAttribute("class", "opaque");
			}
			var h = eval('document.all.tabh'+k+'.style.visibility;');
			var b = eval('document.all.tabb'+k+'.style.visibility;');
			for(i=0;i<15;i++){
				if (navigator.appName=="Microsoft Internet Explorer"){
					eval('document.all.td'+i+'.style.background = "";');
					eval('document.all.td'+i+'.style.filter = "";');
				}else{
					eval('document.all.td'+i+'.setAttribute("class", "");');
				}
				eval('document.all.tabh'+i+'.style.visibility = "hidden";');
				eval('document.all.tabb'+i+'.style.visibility = "hidden";');
			}
			if(d=='none'){
				if (navigator.appName=="Microsoft Internet Explorer"){
					eval('document.all.td'+k+'.style.background="url(/images/pix_transparent.gif)";');
					eval('document.all.td'+k+'.style.filter="progid:DXImageTransform.Microsoft.Alpha(opacity=50);";');
				}else{
					eval('document.all.td'+k+'.setAttribute("class", "opaque");');
				}
			}
			eval('document.all.tabh'+k+'.style.visibility =(h=="visible"?"hidden":"visible");');
			eval('document.all.tabb'+k+'.style.visibility =(b=="visible"?"hidden":"visible");');
			
			
			
		}
		</script>
		

	</tbody></table>
</td>
<td valign="bottom" style=" vertical-align: bottom; background-image: url('images/faq-bg.png')">
<img src="./images/faq.png"/>
</td>
</tr>
<tr>
<td>
<img src="./images/rene10.png"/>
</td>
<td>
<img src="./images/rene11.png"/>
</td>
</tr>
<tr>
<td>
</td>
<td>
</td>
</tr>
</table>