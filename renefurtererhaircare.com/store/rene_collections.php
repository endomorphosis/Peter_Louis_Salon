<style>
table, tr, td, img{
padding: 0;
margin: 0;
border: 0;
}

tr{
vertical-align: top; padding: 0; margin: 0px; width: 720px; border-width: 0px; font-size: 12px; color: #ffffff; 
}

td{
vertical-align: top; padding: 0; margin: 0px; border-width: 0px; font-size: 12px; color: #ffffff; 
}

</style>
<table style=" vertical-align: top; padding: 0px; margin: 0px; width: 720px; border-width: 0px; font-size: 12px; color: #ffffff; background-color: #0e891e; " cellspacing="0">
<tr>
<td colspan="2"><? include($template_include_file);?>
</td>
</tr>
<tr>
<td><br/>
<p style="color: #ffffff; font-weight: 900; font-size: 14px; margin-left: 1em; margin-right: 1em;">
COLLECTIONS
</p>
<p style="color: #ffffff; font-size: 12px; margin-left: 1em; margin-right: 1em;">
Master stylist Rene Furterer created his foolproof line of products, making sure to leave no hair behind. Today that goal continues to thrive, with comprehensive collections created specifically to target a multitude of mane-related troubles and maintain a healthy head of hair. From shower staples to styling must-haves to specialty formulas filled with the most beneficial fruits of the earth, the exclusive treatments cover all the bases resulting in a strong, supple, and silky hair home run.
</p>
<p style="color: #ffffff; font-weight: 900; font-size: 14px; margin-left: 1em; margin-right: 1em;">	
HAIR WOES BE GONE
</p>
<p style="color: #ffffff; font-size: 12px; margin-left: 1em; margin-right: 1em;">
Here are some Specialized Solutions for Healthy Hair
</p>
<p style="color: #ffffff; font-size: 12px; margin-left: 1em; margin-right: 1em;">

<table style="text-align: center;">
<tr>
<td><a href="http://renefurtererhaircare.com/template.php?f=rene_beauty">
<img src="images/229(2).jpg"/><br/>Beautifying</a>
</td>
<td><a href="http://renefurtererhaircare.com/template.php?f=rene_colorperm">
<img src="images/C14830_sm.jpg"/><br/>Permed & Colored Hair Treatment</a>
</td>
</tr>
<tr>
</tr>
<td colspan="2"><br/>
</td>
<tr>
<td><a href="http://renefurtererhaircare.com/template.php?f=rene_hairloss">
<img src="images/C14832_sm.jpg"/><br/>Thinning Hair Treatment<br/></a>
</td>
<td><a href="http://renefurtererhaircare.com/template.php?f=rene_dryscalp">
<img src="images/C14825_sm.jpg"/><br/>Dry Scalp & Hair Program</a>
</td>
</tr>
<tr><td><br/></td></tr>
</table>
</td>
<td valign="bottom" style=" vertical-align: top; background-image: url('images/collections-bg.png')">
<img src="./images/collections.png"/>
</td>
</tr>
<tr>
<td>
<img src="./images/rene10.png"/>
</td>
<td>
<img src="./images/rene11.png"/>
</td>
</tr>
<tr>
<td>
</td>
<td>
</td>
</tr>
</table>