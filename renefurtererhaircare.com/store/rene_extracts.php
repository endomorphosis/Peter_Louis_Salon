<style>
table, tr, td, img{
padding: 0;
margin: 0;
border: 0;
}

tr{
vertical-align: top; padding: 0; margin: 0px; width: 720px; border-width: 0px; font-size: 12px; color: #ffffff; 
}

td{
vertical-align: top; padding: 0; margin: 0px; border-width: 0px; font-size: 12px; color: #ffffff; 
}

</style>
<table style=" vertical-align: top; padding: 0px; margin: 0px; width: 720px; border-width: 0px; font-size: 12px; color: #ffffff; background-color: #0e891e; " cellspacing="0">
<tr>
<td colspan="2"><? include($template_include_file);?>
</td>
</tr>
<tr>
<td><br/>
<p style="color: #ffffff; font-size: 12px; margin-left: 1em; margin-right: 1em;">
There are more than 250,000 plant species on Earth!
</p>
<p style="color: #ffffff; font-size: 12px; margin-left: 1em; margin-right: 1em;">
Scientists continue to learn about their properties and activities in very different areas.
</p>
<p style="color: #ffffff; font-size: 12px; margin-left: 1em; margin-right: 1em;">
The botanical universe, explored for the sake of our health, is equally important for our beauty, and has visible beneficial actions.
</p>
<p style="color: #ffffff; font-size: 12px; margin-left: 1em; margin-right: 1em;">
Rene Furterer has consistently combined essential oils with plant extracts. Those used for the formulation of his hair care treatments are rigorously selected for their targeted actions. These plants naturally provide hair with the elements that contribute to its health and beauty.
</p>
<p style="color: #ffffff; font-size: 12px; margin-left: 1em; margin-right: 1em;">
Plant extracts are used in various forms, as extracts, oils, essence, pectin or butter derived from plants. 
</p>
</td>
<td>
<img src="./images/rene_extracts.jpg"/>
</td>
</tr>
<tr>
<td>
<img src="./images/rene10.png"/>
</td>
<td>
<img src="./images/rene11.png"/>
</td>
</tr>
<tr>
<td>
</td>
<td>
</td>
</tr>
</table>