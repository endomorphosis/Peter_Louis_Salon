<style>
table, tr, td, img{
padding: 0;
margin: 0;
border: 0;
}

tr{
vertical-align: top; padding: 0; margin: 0px; width: 720px; border-width: 0px; font-size: 12px; color: #ffffff; 
}

td{
vertical-align: top; padding: 0; margin: 0px; width: 720px; border-width: 0px; font-size: 12px; color: #ffffff; 
}

</style>
<table style=" vertical-align: top; padding: 0px; margin: 0px; width: 720px; border-width: 0px; font-size: 12px; color: #ffffff; background-color: #0e891e; " cellspacing="0">
<tr>
<td colspan="2"><? include($template_include_file);?>
</td>
</tr>
<tr>
<td><br/>
<p style="color: #ffffff; font-weight: 700; font-size: 12px; margin-left: 1em; margin-right: 1em;"> 
SPOTLIGHT
</p>
<p style="color: #ffffff; font-size: 12px; margin-left: 1em; margin-right: 1em;"> 
Losing your hair can make you lose your cool but instead of capitulating to the horrid female comb-over (you wouldn't dare!), you can find strength in Rene Furterer's expert treatments for thinning manes. Keeping with the brand's scalp-centric approach, the products target the roots and invigorate circulation to resuscitate lethargic locks due to unlucky genes, stress, or any other inauspicious circumstance. Soon enough you'll be swapping flip-outs fueled by shear frustration for worry-free flips of your resilient hair.
</p>
<p style="color: #ffffff; font-weight: 700; font-size: 12px; margin-left: 1em; margin-right: 1em;"> 
INTO THIN HAIR
</p><p style="color: #ffffff; font-size: 12px; margin-left: 1em; margin-right: 1em;"> 
Make Hair-Loss Problems Disappear
Yet again, Rene Furterer turns to the powers of the plant to counter a common mane malady: thinning hair. Pfaffia extract, derived from an Amazonian plant, is the ingredient to watch in this restorative lineup, as it naturally stimulates hair growth and soups up the texture of your limp locks. This fortuitous plant can be found in both of the fortifying treatments: RF80 for weak or temporarily thinning hair, and the Triphasic formula, which targets hereditary and hormonal-induced loss. Introduce this multi-phased regimen into your weekly routine, and you'll be tressed for success.
</p>
<table>
<tr>
<td style="text-align: center;">
<a href="http://www.peterlouissalon.com/template.php?f=prod_detail&ref=rene&pid=104"><img src="images/C14788_1.jpg"><br/> Complexe 5 Regenerating Extract</a>
</td>
<td>
<font style="font-weight: 700;">PREPARE:</font><a href="http://www.peterlouissalon.com/template.php?f=prod_detail&ref=rene&pid=104"> Complexe 5 Regenerating Extract</a> has stimulating essential oils from lavender and orange that deep clean and tone the scalp. It also strengthens tired, limp hair by promoting microcirculation. This pre-game product also preps the hair for the proceeding formulas to ensure optimal effects from start to finish.
</td>
</tr>
<tr><td><br/></td></tr>
<tr>
<td style="text-align: center;">
<a href="http://www.peterlouissalon.com/template.php?f=prod_detail&ref=rene&pid=110"><img src="images/C14788_2.jpg"><br/>Forticea Stimulating Shampoo</a>
</td>
<td>
<font style="font-weight: 700;">SHAMPOO:</font> <a href="http://www.peterlouissalon.com/template.php?f=prod_detail&ref=rene&pid=110">Forticea Stimulating Shampoo</a> combines essential oil biospheres and plant peptides to stimulate microcirculation and restore beautiful, radiant hair. You can maximize its actions by massaging your scalp in a circular motion from the nape of the neck forward.
</td>
</tr>
<tr><td><br/></td></tr>
<tr>
<td style="text-align: center;">
<a href="http://www.peterlouissalon.com/template.php?f=prod_detail&ref=rene&pid=111"><img src="images/C14788_3.jpg"><br/>RF80 Concentrated Hair Strengthening Formula</a>
</td>
<td>
<font style="font-weight: 700;">TREAT:</font> <a href="http://www.peterlouissalon.com/template.php?f=prod_detail&ref=rene&pid=111">RF80 Concentrated Hair Strengthening Formula</a> is a once-a-week, leave-in treatment that continues to fortify your locks by restoring volume and radiance to hair. The strengthening solution works on two fronts: It offers nutritional value thanks to vitamins A, B5, F, amino proteins, copper, and zinc, and promotes imperative vascular circulation via pfaffia extract and the essential oils of lemon and sage.
</td>
</tr>
</table>

</td>
<td valign="bottom" style=" vertical-align: top; background-image: url('images/hairloss-bg.png')">
<img src="./images/hairloss.png"/>
</td>
</tr>
<tr  width="400">
<td>
<img src="./images/rene10.png"/>
</td>
<td>
<img src="./images/rene11.png"/>
</td>
</tr>
<tr  width="400">
<td>
</td>
<td>
</td>
</tr>
</table>