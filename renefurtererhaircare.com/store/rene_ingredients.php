<style>
table, tr, td, img{
padding: 0;
margin: 0;
border: 0;
}

tr{
vertical-align: top; padding: 0; margin: 0px; width: 720px; border-width: 0px; font-size: 12px; color: #ffffff; 
}

td{
vertical-align: top; padding: 0; margin: 0px;  border-width: 0px; font-size: 12px; color: #ffffff; 
}

</style>
<table style=" vertical-align: top; padding: 0px; margin: 0px; width: 720px; border-width: 0px; font-size: 12px; color: #ffffff; background-color: #0e891e; " cellspacing="0">
<tr>
<td colspan="2"><? include($template_include_file);?>
</td>
</tr>
<tr>
<td><br/>
<p style="color: #ffffff; font-weight: 700; font-size: 14px; margin-left: 1em; margin-right: 1em;">
SECRET INGREDIENT
</p>
<p style="color: #ffffff; font-size: 12px; margin-left: 1em; margin-right: 1em;">
Every brand has a defining ingredient that sets it apart and makes it unique. Whether it's the special polymer that magically makes your lashes curl, the peptide that depuffs your tired eyes, or the vitamin complex that turns your dull hair shiny, it's those special, signature ingredients that often mean the difference between a product you love and one you simply can't live without. Read on to learn more about the special somethings that make your favorite products so darn fabulous.
</p>
<p style="color: #ffffff; font-weight: 700; font-size: 14px; margin-left: 1em; margin-right: 1em;">
ONLY THE ESSENTIALS 
</p>
<p style="color: #ffffff; font-size: 12px; margin-left: 1em; margin-right: 1em;">
Rene Furterer Picks the Finest Ingredients for His Plant-Based Products
</p>
<p style="color: #ffffff; font-size: 12px; margin-left: 1em; margin-right: 1em;">		
Totaling 24 in all, the essential oils and plant extracts in Furterer's fertile products are earthly elixirs fully prepared to cure your every hair ailment. Rare and infinitely valuable, essential oils are a pure concentrate of active ingredients that quickly absorb into the papilla (the origin of living hair), leaving luscious locks from root to tip. Likewise, exotic plant extracts also work wonders on less-than-tempting tresses, and are carefully chosen for their specific treatment abilities and aromatic qualities. When paired together, these natural wonders make for one dynamic duo. Here are some of your best bets for lathering up:
</p>
<p style="color: #ffffff; font-size: 12px; margin-left: 1em; margin-right: 1em;">
<font style="font-weight: 700;"> Okara:</font> Okara extract contains coating sugars and energizing mineral salts, including trace elements such as iron, zinc, and magnesium. Thanks to amino acids, okara has high repairing powers that protect the scalp and preserve valuable proteins on chemically treated and colored hair.
</p>
<p style="color: #ffffff; font-size: 12px; margin-left: 1em; margin-right: 1em;">
<font style="font-weight: 700;">Karite:</font> This extract is derived from the almonds found inside the fruit of the tree of the same name, also referred to as the "butter tree" in Africa. Rich in mega moisturizers like vitamins A, E, and F, karite butter has been used for ages for its softening, nourishing, and restructuring properties in both hair and skincare.
</p>
<p style="color: #ffffff; font-size: 12px; margin-left: 1em; margin-right: 1em;">
<font style="font-weight: 700;">Melaleuca:</font> Commonly referred to as the "tea tree," melaleuca deep-cleans the scalp and reduces microbial proliferation, a.k.a. the yeast responsible for the appearance of dandruff. To boot, its aromatic qualities boost energy and reduce stress.
</p>
<p style="color: #ffffff; font-size: 12px; margin-left: 1em; margin-right: 1em;">
<font style="font-weight: 700;">Pfaffia:</font> A relative of ginseng and papaya wood, pfaffia is native to the Amazon region and is noted for its role in combating hair loss. The extract improves blood microcirculation, stimulates growth, and maintains the nutrition of the hair follicle.
</td>
<td valign="bottom" style=" vertical-align: top; background-image: url('images/ingredients-bg.png')">
<img src="./images/ingredients.png"/>
</td>
</tr>
<tr>
<td>
<img src="./images/rene10.png"/>
</td>
<td>
<img src="./images/rene11.png"/>
</td>
</tr>
<tr>
<td>
</td>
<td>
</td>
</tr>
</table>