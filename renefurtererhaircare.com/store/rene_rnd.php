<style>
table, tr, td, img{
padding: 0;
margin: 0;
border: 0;
}

tr{
vertical-align: top; padding: 0; margin: 0px; width: 720px; border-width: 0px; font-size: 12px; color: #ffffff; 
}

td{
vertical-align: top; padding: 0; margin: 0px;  border-width: 0px; font-size: 12px; color: #ffffff; 
}

</style>
<table style=" vertical-align: top; padding: 0px; margin: 0px; width: 720px; border-width: 0px; font-size: 12px; color: #ffffff; background-color: #0e891e; " cellspacing="0">
<tr>
<td colspan="2"><? include($template_include_file);?>
</td>
</tr>
<tr>
<td><br/>
<p style="color: #ffffff; font-weight: 900; font-size: 14px; margin-left: 1em; margin-right: 1em;">
RESEARCH & DEVELOPMENT
</p>
<p style="color: #ffffff; font-weight: 700; font-size: 12px; margin-left: 1em; margin-right: 1em;">
The development of a new product made with natural active ingredients:
</p>
<p style="color: #ffffff; font-size: 12px; margin-left: 1em; margin-right: 1em;">
For the development of natural active ingredients, the Phytochemistry department is in charge of researching the concerned plant active ingredients in collaboration with the research center on natural substances (CRSN) in Ramonville, France. Thanks to this relationship, the development of new plant active ingredients is supported by complete research of analyzed plants and from access to high-output pharmacological targeting. This targeting helps select, from a very large number of available plants, the best suited species for the desired activity.
Once the active ingredients have been identified and their non toxicity proven, the hair care product galenics department proceeds with the formulation of the product.
</p>
<p style="color: #ffffff; font-size: 12px; margin-left: 1em; margin-right: 1em;">	

The clinical department assists in the galenic development. In collaboration with the Centre Jean-Louis Alibert (Hôtel-Dieu St Jacques, Toulouse, France), it is in charge of carrying out all clinical studies under dermatological control intended to demonstrate the effectiveness, tolerance and acceptability of each product, required prior to their introduction to the market.
It has at its disposal a sensory analysis laboratory, which helps guide the galenic research by garnering consumer reactions to the formulation (texture, fragrance, comfort of use, etc.).
Moreover, a cosmeto-vigilance determent collects and analyzes all possible intolerance, irritation or allergy reactions that may be reported on the marketed product.
</p>
<p style="color: #ffffff; font-size: 12px; margin-left: 1em; margin-right: 1em;">
	
Designed with the same quality requirements in mind, as those used for medicine, RenÃ© Furterer products benefit from the research and evaluation techniques of Pierre Fabre Pharmaceutical Laboratories, based on modern and original investigative methods such as cellular culture evaluations, and functional skin and hair exploration by non invasive technology.
</p>
<p style="color: #ffffff; font-size: 12px; margin-left: 1em; margin-right: 1em;">
After the research phase and the corresponding assessment under dermatological control, the RenÃ© Furterer products in development are submitted for evaluation by a panel of renowned hair stylists.
It is their final responsibility to evaluate the textures and the results of the developed products. In this manner, they help ensure that, through beautiful hair, hair styling may become an art
</p>
</td>
<td valign="bottom" style=" vertical-align: top; background-color: #012108;">
<img src="./images/research.png"/>
</td>
</tr>
<tr>
<td>
<img src="./images/rene10.png"/>

</td>
<td>
<img src="./images/rene11.png"/>
</td>
</tr>
<tr>
<td>
</td>
<td>
</td>
</tr>
</table>