<style>
table, tr, td, img{
padding: 0;
margin: 0;
border: 0;
}

tr{
vertical-align: top; padding: 0; margin: 0px; width: 720px; border-width: 0px; font-size: 12px; color: #ffffff; 
}

td{
vertical-align: top; padding: 0; margin: 0px;  border-width: 0px; font-size: 12px; color: #ffffff; 
}

</style>
<table valign="top" style="text-align: top; padding: 0px; margin: 0px; width: 720px; border-width: 0px; font-size: 12px; color: #ffffff; background-color: #0e891e; " cellspacing="0">
<tr>
<td colspan="2"><? include($template_include_file);?>
</td>
</tr>
<tr>
<td><br/>
<p style="color: #ffffff; font-size: 12px; margin-left: 1em; margin-right: 1em;">
Rare and infinitely valuable, Rene Furterer knows that essential oils are the purest part of the plant.  It is only this true concentrate of active ingredients which Rene Furterer has always used for its products.
</p>
<p style="color: #ffffff; font-size: 12px; margin-left: 1em; margin-right: 1em;">
Essential oils are fragrant, volatile substances secreted by the plants and stored in special secretion pouches found in different parts of the plant: leaves, flowers, stalks and roots. Rene Furterer understands their essential function is to protect the plant from external aggressions such as viruses, microbes and parasites.
</p>
<p style="color: #ffffff; font-size: 12px; margin-left: 1em; margin-right: 1em;">
The term "essence" dates back to the 16th century, when Paracelse worked on the extraction of the plant "soul" in the form of "quintessence" (fifth essence), later referred to as the "spirit", then essence and essential oil.
</p>
<p style="color: #ffffff; font-size: 12px; margin-left: 1em; margin-right: 1em;">
The term "oil" does not correspond to the chemical definition but rather to the fact that essences are soluble in oil and fat. 
</p>
</td>
<td>
<img src="./images/rene_oils.jpg"/>
</td>
</tr>
<tr>
<td>
<img src="./images/rene10.png"/>
</td>
<td>
<img src="./images/rene11.png"/>
</td>
</tr>
<tr>
<td>
</td>
<td>
</td>
</tr>
</table>