<?php
/** 
 *  PHP Version 5
 *
 *  @category    Amazon
 *  @package     Amazon_FWSOutbound
 *  @copyright   Copyright 2007 Amazon Technologies, Inc.
 *  @link        http://aws.amazon.com
 *  @license     http://aws.amazon.com/apache2.0  Apache License, Version 2.0
 *  @version     2007-08-02
 */
/******************************************************************************* 
 *    __  _    _  ___ 
 *   (  )( \/\/ )/ __)
 *   /__\ \    / \__ \
 *  (_)(_) \/\/  (___/
 * 
 *  Amazon FWS Outbound PHP5 Library
 *  Generated: Wed Mar 19 13:03:03 PDT 2008
 * 
 */

/**
 * Create Fulfillment Order  Sample
 */

include_once ('/home/peterlou/public_html/drupal/amazon/src/Amazon/FWSOutbound/.config.inc.php'); 
include_once ('/home/peterlou/public_html/drupal/amazon/src/Amazon/FWSOutbound/Client.php'); 
include_once ('/home/peterlou/public_html/drupal/amazon/src/Amazon/FWSOutbound/Address.php'); 
include_once ('/home/peterlou/public_html/drupal/amazon/src/Amazon/FWSOutbound/CreateFulfillmentOrder.php'); 

/************************************************************************
 * Instantiate Implementation of Amazon FWSOutbound
 * 
 * AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY constants 
 * are defined in the .config.inc.php located in the same 
 * directory as this sample
 ***********************************************************************/
 $service = new Amazon_FWSOutbound_Client(AWS_ACCESS_KEY_ID, 
                                       AWS_SECRET_ACCESS_KEY);
 
/************************************************************************
 * Uncomment to try out Mock Service that simulates Amazon_FWSOutbound
 * responses without calling Amazon_FWSOutbound service.
 *
 * Responses are loaded from local XML files. You can tweak XML files to
 * experiment with various outputs during development
 *
 * XML files available under Amazon/FWSOutbound/Mock tree
 *
 ***********************************************************************/
 // $service = new Amazon_FWSOutbound_Mock();

/************************************************************************
 * Setup request parameters and uncomment invoke to try out
 * sample for Create Fulfillment Order Action
 ***********************************************************************/
 // @TODO: set request. Action can be passed as Amazon_FWSOutbound_Model_CreateFulfillmentOrder
 // object or array of parameters

// Create Request

//Create Address
$address = new Amazon_FWSOutbound_Model_Address();

//Set Address Parameters
$address-> setName('John Richards');
$address-> setLine1('DO NOT SEND');
$address-> setLine2('TESTING');
$address-> setCity('Mclean');
$address-> setStateOrProvinceCode('VA');
$address-> setCountryCode('US');
$address-> setPostalCode('22122');
$address-> setPhoneNumber('941-544-2102');

//Create item
$item = new Amazon_FWSOutbound_Model_CreateFulfillmentOrderItem();

//Set item parameters
$item-> setMerchantSKU('V4-TWFV-K9LV');
$item-> setMerchantFulfillmentOrderItemId('Test-00000-2');
$item-> setQuantity('2');

//Create Request
$request = new Amazon_FWSOutbound_Model_CreateFulfillmentOrder();

//Set Request Parameters
$request-> setDestinationAddress($address);
$request-> setMerchantFulfillmentOrderId('1');
$request-> setDisplayableOrderId('100');
$request-> setDisplayableOrderDateTime('2008-04-04T08:00:00Z');
$request-> setDisplayableOrderComment('Thank you for your order!');
$request-> setShippingSpeedCategory('Standard');
$request-> setItem($item);

invokeCreateFulfillmentOrder($service, $request);

/**
  * Create Fulfillment Order Action Sample
  * Request for Amazon to send items from the merchant's inventory to a
  * destination address.
  *   
  * @param Amazon_FWSOutbound_Interface $service instance of Amazon_FWSOutbound_Interface
  * @param mixed $request Amazon_FWSOutbound_Model_CreateFulfillmentOrder or array of parameters
  */
  function invokeCreateFulfillmentOrder(Amazon_FWSOutbound_Interface $service, $request) 
  {
      try {
              $response = $service->createFulfillmentOrder($request);
              
                echo ("Service Response\n");
                echo ("=============================================================================\n");

                echo("        CreateFulfillmentOrderResponse\n");
                if ($response->isSetResponseMetadata()) { 
                    echo("            ResponseMetadata\n");
                    $responseMetadata = $response->getResponseMetadata();
                    if ($responseMetadata->isSetRequestId()) 
                    {
                        echo("                RequestId\n");
                        echo("                    " . $responseMetadata->getRequestId() . "\n");
                    }
                } 

     } catch (Amazon_FWSOutbound_Exception $ex) {
         echo("Caught Exception: " . $ex->getMessage() . "\n");
         echo("Response Status Code: " . $ex->getStatusCode() . "\n");
         echo("Error Code: " . $ex->getErrorCode() . "\n");
         echo("Error Type: " . $ex->getErrorType() . "\n");
         echo("Request ID: " . $ex->getRequestId() . "\n");
         echo("XML: " . $ex->getXML() . "\n");
     }
 }
