<?php

if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' );

/* Joomla! 1.5 One Page Checkout Module for VirtueMart
*
* @version $Id: opco_config.php 05-24-2011
 * author: Polished Geek
 * Website: PolishedGeek.com
 * Email: info@PolishedGeek.com
 * Phone: 919-374-2425
 *
 * license http://www.gnu.org/copyleft/gpl.html GNU/GPL
 *
*/

global $VM_LANG, $vm_mainframe, $sess, $mm_action_url, $_SESSION, $db, $_REQUEST, $GLOBALS, $ps_html, $VM_LANG, $my, $vm_mainframe, $mainframe, $page, $shipping_rate_id, $total, $tax_total, $mosConfig_live_site;

class OpcoConfig extends JObject
{
    var $general;
    var $styles;

    var $reviewCart;
    var $billingShipping;
    var $shippingInstr;
    var $paymentMethod;
    var $confirmPlaceOrder;
    var $confirmationPage;

    function getCustomStyle()
    {
        $styles = "";
        
        // for the styles object, ask overall styles
        $styles .= $this->styles->getCustomStyle();
        
        // for each section, ask it's styles
        $styles .= $this->reviewCart->getCustomStyle();
        $styles .= $this->billingShipping->getCustomStyle();
        $styles .= $this->shippingInstr->getCustomStyle();
        $styles .= $this->paymentMethod->getCustomStyle();
        $styles .= $this->confirmPlaceOrder->getCustomStyle();
        $styles .= $this->confirmationPage->getCustomStyle();

        return $styles;
    }
    

    function load($params)
    {
        $this->general = new PgGeneralOptions();
        $this->general->init($params);
    
        $this->styles = new PgStyles();
        $this->styles->init($params);
        
        $this->reviewCart = new PgOpcoCartSection();
        $this->reviewCart->init($params, 'review_cart');
        
        $this->billingShipping = new PgBillShipSection();
        $this->billingShipping->init($params, 'bill_ship');
        
        $this->shippingInstr = new PgShippingInstructionsSection();
        $this->shippingInstr->init($params, 'ship_instr');
        
        $this->paymentMethod = new PgPaymentMethodSection();
        $this->paymentMethod->init($params, 'payment');
        
        $this->confirmPlaceOrder = new PgConfirmPlaceOrderSection();
        $this->confirmPlaceOrder->init($params, 'confirm_place');
        
        $this->confirmationPage = new PgConfirmationPageSection();
        $this->confirmationPage->init($params, 'confirm_page');    

/*	    if ($this->shippingInstr->showCalcShippingButton == 0)
	    {
	        $this->shippingInstr->showSection = 0;
	    }
 */
	if($this->shippingInstr->showSection == 0) {
		$this->shippingInstr->showCalcShippingButton = 0;
	}
		
    }
    
    function updateCheckout($ps_checkout, $userIdSet)
    {
        if ($ps_checkout->noShipToNecessary()) 
        {
	        $this->billingShipping->showShippingSection = -1;
	        $this->shippingInstr->showSection = -1;
	        $this->shippingInstr->showCalcShippingButton = 0;
		$this->confirmationPage->showCalculateTaxButton = 0;
		$this->billingShipping->sideBySideLayout = 0;
        }
        
        if ($userIdSet)
        {
	        $this->confirmationPage->showCalculateTaxButton = 0;
	    }
    }
}

class PgGeneralOptions extends JObject
{
    var $redirectWhenEmpty;
    var $customThankyou;

    function init($params)
    {
        $this->redirectWhenEmpty = $params->get('redirect_empty', 1);
        $this->customThankyou = $params->get('custom_thankyou', 'checkout.thankyou');
    }
}

class PgStyles extends JObject
{
    public static $moduleClass = "onepagecheckoutmodule";
    
    var $moduleclassSfx;
    var $totalWidth;
    var $additionalStyle;
        
    var $sectionTitle;
    //var $section_intruction;
    var $error;
    var $button;
    
    function getCustomStyle()
    {
        $styles = "";
        $styles .= "div#" . PgStyles::$moduleClass ." { width: " . $this->totalWidth . "; }";
        $styles .= "\n";
        $styles .= $this->sectionTitle->getCustomStyle();
        $styles .= "\n";
        $styles .= $this->error->getCustomStyle();
        $styles .= "\n";
        $styles .= $this->button->getCustomStyle();
        $styles .= "\n";
        $styles .= $this->additionalStyle;
        $styles .= "\n";
        return $styles;
    }

    function init($params)
    {
        $this->totalWidth = $params->get('total_width', '100%');
        $this->moduleclassSfx = $params->get('moduleclass_sfx', '');
        
        PgStyles::$moduleClass .= $this->moduleclassSfx;

        $this->sectionTitle = new PgTextElementStyle();
        $this->sectionTitle->init($params, 'section_titles', 'h2', 'pgSectionTitle', '');
        /*$this->section_intruction = new PgTextElementStyle();
        $this->section_intruction->init($params, 'section_intructions', 'div', 'pgSectionInstr');*/
        $this->error = new PgTextElementStyle();
        $this->error->init($params, 'error', 'div', 'pgError', '');
        $this->button = new PgButtonElementStyle();
        $this->button->init($params, 'button', '', 'button', '');
        
        $this->additionalStyle = $params->get('additional_style', '');
    }
}

class PgSection extends JObject
{
    var $showSection;
    var $customStyle;
    
    var $sectionPrefix;
    
    function getTitle()
    {
        return JText::_(strtoupper($this->sectionPrefix) . "_TITLE");
    }
    
    function getInstructions()
    {
        return JText::_(strtoupper($this->sectionPrefix) . "_INSTRUCTIONS");
    }
    
    function getSectionCssPath()
    {
        return "div#" . PgStyles::$moduleClass . " div#" . $this->sectionPrefix . ".opco_section";
    }
    
    function _getSectionCss()
    {
        $rv = '';
    
        if (strlen($this->customStyle) > 0)
        {
            $rv .= $this->customStyle . "\n";
        }

        return $rv;
    }
    
    function _getAdditionalStyleElements()
    {
        return "";
    }
    
    function getCustomStyle()
    {
        // section style first
        $rv = '';
        
        $id = $this->getSectionCssPath();
        $styleValue = $this->_getSectionCss();
        
        if (strlen($id) > 0 && strlen($styleValue) > 0)
        {
            $rv = $id . " { " . $styleValue . " }\n";
        }
        
        // additional elements
        $rv .= $this->_getAdditionalStyleElements();
        
        return $rv; 
    }
    
    function init($params, $sectionPrefix)
    {
        $this->sectionPrefix = $sectionPrefix;
        $this->showSection = $params->get($sectionPrefix . '_show', 1);
        $this->customStyle = $params->get($sectionPrefix . '_style', '');
    }
}

class PgOpcoCartSection extends PgSection
{
    var $showCart;
    var $allowCartUpdates;
    var $showEditCartLink;
    var $editCartLinkAlignment;
    var $showProductSkus;
    var $showCoupon;

    function init($params, $sectionPrefix)
    {
        parent::init($params, $sectionPrefix);
        $this->showCart = $params->get($sectionPrefix . '_show_cart', 1);
        $this->allowCartUpdates = $params->get($sectionPrefix . '_allow_cart_updates', 1);
        $this->showEditCartLink = $params->get($sectionPrefix . '_show_edit_link', 1);
        $this->editCartLinkAlignment = $params->get($sectionPrefix . '_edit_link_align', 'right');
        $this->showProductSkus = $params->get($sectionPrefix . '_show_sku', 1);
        $this->showCoupons = $params->get($sectionPrefix . '_show_coupons', 1);
    }
}

class PgBillShipSection extends PgSection
{
    var $sideBySideLayout;
    var $showShippingSection;
    var $accountSectionStyle;
    var $shippingSectionStyle;
    var $paramOnSingleLine;
    var $usernameIsEmail;
    var $useJanrain;
    
    var $labelStyle;
    var $inputBoxStyle;
    var $dropDownStyle;

    function _getAdditionalStyleElements()
    {
        $styles = "";
        
        if ($this->paramOnSingleLine)
        {
            $styles .= $this->getSectionCssPath() . " * .formLabel { clear: both; float: left; }\n";
            $styles .= $this->getSectionCssPath() . " * .formField .inputbox { float: left; }\n";
        }
        
        if (strlen($this->accountSectionStyle) > 0)
        {
            $styles .= $this->getSectionCssPath() . " * div#account_section { " . $this->accountSectionStyle . " }\n";
        }
        
        if (strlen($this->shippingSectionStyle) > 0)
        {
            $styles .= $this->getSectionCssPath() . " * div#shipping_section { " . $this->shippingSectionStyle . " }\n";
        }
        
        
        $styles .= $this->labelStyle->getCustomStyle();

        $styles .= $this->inputBoxStyle->getCustomStyle();

        $styles .= $this->dropDownStyle->getCustomStyle();

        $styles .= parent::_getAdditionalStyleElements();

        return $styles;
    }
    
    function init($params, $sectionPrefix)
    {
        parent::init($params, $sectionPrefix);
        $this->useJanrain = $params->get($sectionPrefix . '_janrain', 0);
        $this->sideBySideLayout = $params->get($sectionPrefix . '_side_by_side', 1);
        $this->showShippingSection = $params->get($sectionPrefix . '_show_shipping', 1);
        $this->accountSectionStyle = $params->get($sectionPrefix . '_account_style', '');
        $this->shippingSectionStyle = $params->get($sectionPrefix . '_shipping_style', '');
        $this->paramOnSingleLine = $params->get($sectionPrefix . '_param_on_single_line', 1);
        $this->usernameIsEmail = $params->get($sectionPrefix . '_usernameisemail', 0);

        $this->labelStyle = new PgLabelElementStyle();
        $this->labelStyle->init($params, $sectionPrefix . '_label', '', '', $this->getSectionCssPath() . ' * .formLabel');

        $this->inputBoxStyle = new PgInputBoxElementStyle();
        $this->inputBoxStyle->init($params, $sectionPrefix . '_inputbox', '', '', $this->getSectionCssPath() . ' * .formField input.inputbox');

        $this->dropDownStyle = new PgDropdownElementStyle();
        $this->dropDownStyle->init($params, $sectionPrefix . '_dropdown', '', '', $this->getSectionCssPath() . ' * .formField select.inputbox');
    }
}

class PgShippingInstructionsSection extends PgSection
{
    var $showCalcShippingButton;
    var $hideNonDefaultMethods;
    
    function init($params, $sectionPrefix)
    {
        parent::init($params, $sectionPrefix);
        $this->showCalcShippingButton = $params->get($sectionPrefix . '_show_calc_ship_button', 1);
	$this->hideNonDefaultMethods = $params->get($sectionPrefix . '_hide_non_def_methods', 0);
	
    }
}

class PgPaymentMethodSection extends PgSection
{
    var $enabled;
    
    function init($params, $sectionPrefix)
    {
        parent::init($params, $sectionPrefix);
        $this->enabled = $params->get($sectionPrefix . '_enabled', 1);
    }
}

class PgConfirmPlaceOrderSection extends PgSection
{
    var $showTotals;
    var $combineTax;
    var $showUpdateTotalsButton;
    var $showPaymentCharge;
    var $showCalculateTaxButton;
    var $showOrderInstructionsPrompt;
    var $placeOrderButton;
    
    function _getAdditionalStyleElements()
    {
        $styles = "";
        $styles .= $this->placeOrderButton->getCustomStyle();
        $styles .= "\n";
        $styles .= parent::_getAdditionalStyleElements();
        return $styles;
    }
    
    function init($params, $sectionPrefix)
    {
        parent::init($params, $sectionPrefix);
        $this->showTotals = $params->get($sectionPrefix . '_show_totals', 1);
        $this->combineTax = $params->get($sectionPrefix . '_combine_tax', 0);
        $this->showPaymentCharge = $params->get($sectionPrefix . '_show_payment_charge', 0); 
        
        $this->showUpdateTotalsButton = $params->get($sectionPrefix . '_show_update_totals_button', 1);
        
        $this->showCalculateTaxButton = $params->get($sectionPrefix . '_show_calc_tax_button', 1);
        $this->showOrderInstructionsPrompt = $params->get($sectionPrefix . '_show_order_instructions', 1);
        
        $this->placeOrderButton = new PgButtonElementStyle();
        $this->placeOrderButton->init($params, $sectionPrefix . '_place_order', '', '', $this->getSectionCssPath() . ' * input#formSubmitButton.button');
    }
}

class PgConfirmationPageSection extends PgSection
{
    function init($params, $sectionPrefix)
    {
        parent::init($params, $sectionPrefix);
    }
}

class PgTextElementStyle extends JObject
{
    var $tag;
    var $cssClass;
    var $cssPath;
    var $fontType;
    var $fontValue;
    var $colorType;
    var $colorValue;
    var $bgType;
    var $bgValue;
    var $customStyle;
    
    function getCustomStyle()
    {
        $rv = "";
    
        if (strlen($this->tag) > 0 || strlen($this->cssClass) > 0)
        {
            $style = $this->getStyleAttributeValue();
            if (strlen($style) > 0) 
            {
                $rv .= "div#" . PgStyles::$moduleClass . " * ";

                if (strlen($this->tag) > 0) 
                {
                    $rv .= $this->tag;
                }
                
                if (strlen($this->cssClass) > 0)
                {
                    $rv .= "." . $this->cssClass;
                }
                
                $rv .= " { " . $style . " }\n";
            }
        }
        else if (strlen($this->cssPath) > 0)
        {
            $style = $this->getStyleAttributeValue();
            if (strlen($style) > 0) 
            {
                $rv .= $this->cssPath . " { " . $style . " }\n";
            }
        }
        
        return $rv;
    }
        
    function init($params, $paramPrefix, $defaultTag, $defaultCssClass, $cssPath)
    {
        $this->tag = $params->get($paramPrefix . '_tag', $defaultTag);
        $this->cssClass = $params->get($paramPrefix . '_class', $defaultCssClass);
        $this->fontType = $params->get($paramPrefix . '_font', '');
        $this->fontValue = $params->get($paramPrefix . '_font_custom', '');
        $this->colorType = $params->get($paramPrefix . '_color', 0);
        $this->colorValue = $params->get($paramPrefix . '_color_custom', '');
        $this->bgType = $params->get($paramPrefix . '_bg', '');
        $this->bgValue = $params->get($paramPrefix . '_bg_custom', '');
        $this->cssClass = $params->get($paramPrefix . '_class', '');
        $this->customStyle = $params->get($paramPrefix . '_style', '');

        $this->cssPath = $cssPath;
    }
    
    protected function _buildStylePart($attr, $value)
    {
        return $attr . ':' . $value . ';';
    }

    function getStyleAttributeValue()
    {
        $rv = '';
        if ($this->fontType == 1)
        {
            $rv = $rv . $this->_buildStylePart('font', $this->fontValue);
        }
        if ($this->colorType == 1)
        {
            $rv = $rv . $this->_buildStylePart('color', $this->colorValue);
        }
        if ($this->bgType == 1)
        {
            $rv = $rv . $this->_buildStylePart('background-color', $this->bgValue);
        }
        if ($this->bgType == 2)
        {
            $rv = $rv . $this->_buildStylePart('background-color', 'transparent');
        }
        if (strlen($this->customStyle) > 0)
        {
            $rv = $rv . $this->customStyle;
        }
        return $rv;
    }
    
    function styleText($text)
    {
        $rv = "<" . $this->tag;
        if (strlen($this->cssClass) > 0)
        {
            $rv = $rv . " class='" . $this->cssClass . "'";
        }
        $rv = $rv . ">" . $text . "</" . $this->tag . ">";
        
        return $rv;
    }
}

class PgSizeableElementStyle extends PgTextElementStyle
{
    var $widthType;
    var $widthValue;
    
    
    function getStyleAttributeValue()
    {
        $styles = "";
        
        if ($this->widthType == 1) 
        {
            $styles .= $this->_buildStylePart('width', $this->widthValue);
        }
        
        $styles .= parent::getStyleAttributeValue();
        
        return $styles;
    }
    
    function init($params, $paramPrefix, $defaultTag, $defaultCssClass, $cssPath)
    {
        parent::init($params, $paramPrefix, $defaultTag, $defaultCssClass, $cssPath);
        $this->widthType = $params->get($paramPrefix . '_width', 0);
        $this->widthValue = $params->get($paramPrefix . '_width_custom', -1);
    }
}

class PgLabelElementStyle extends PgSizeableElementStyle
{
}

class PgInputBoxElementStyle extends PgSizeableElementStyle
{
}

class PgDropDownElementStyle extends PgSizeableElementStyle
{
}

class PgButtonElementStyle extends PgSizeableElementStyle
{
}

?>
