<?php
/* Joomla! 1.5 One Page Checkout Module for VirtueMart
*
* @version $Id: coupon_ajax.php 05-24-2011
 * author: Polished Geek
 * Website: PolishedGeek.com
 * Email: info@PolishedGeek.com
 * Phone: 919-374-2425
 *
 * license http://www.gnu.org/copyleft/gpl.html GNU/GPL
 *
*/

defined('_JEXEC') or define( '_JEXEC', 1 );

$myPath = dirname(__FILE__).DS;

global $_SESSION, $_REQUEST, $CURRENCY_DISPLAY;

defined( 'DS') or define( 'DS', DIRECTORY_SEPARATOR );
defined( 'JPATH_BASE') or define('JPATH_BASE', dirname('..'.DS.'..'.DS.'configuration.php'));



require_once ( JPATH_BASE .DS.'includes'.DS.'defines.php' );
require_once ( JPATH_BASE .DS.'includes'.DS.'framework.php' );


$mainframe =& JFactory::getApplication('site');
$mainframe->initialise();

$language =& JFactory::getLanguage();
$language->load('mod_virtuemart_onepagecheckout', JPATH_BASE, $language->getTag());

// now must get params from the opco module
require_once ( 'mod_load_ajax.php' );

$shipping_coupon_discount = 0;

if (isset($_SESSION['coupon_awo_shipping_discount'])) 
{
	$shipping_coupon_discount = $_SESSION['coupon_awo_shipping_discount'];
}

if (isset($_SESSION['coupon_redeemed']) && $_SESSION['coupon_redeemed'] == '1') 
{
	echo '<div id="coupon_redeemed">';
	echo JText::sprintf('COUPON_REDEEMED', $_SESSION['coupon_code']); 
	echo '</div>';

	echo '<div id="coupon_value">';
    $discountValue = $CURRENCY_DISPLAY->getFullValue($_SESSION['coupon_discount'] + $shipping_coupon_discount);
	echo JText::sprintf('COUPON_REDEEMED_EXPLAINED', $discountValue); 
	echo '</div>';

	echo '<input type="hidden" id="coupon_code" name="coupon_code" value="'.$_SESSION['coupon_code'].'" />';
}
else
{
	echo '<div id="coupon_section">';
	echo '<div id="coupon_instr">' . JText::_('COUPON_INSTRUCTIONS') . '</div>';
	echo '<input id="coupon_code" class="inputbox" width="10" type="text" maxlength="30" name="coupon_code">';
	echo '<input class="button" type="button" value="'.JText::_('SUBMIT_COUPON').'" onclick="javascript:couponsubmit()">';

	if(isset($couponInvalid))
	{
		echo $styleHelper->showError($couponInvalidMessage, '');
	}

	echo '</div>';
}

?>


