<?php
/* Joomla! 1.5 VirtueMart One Page Checkout Module
*
* @version $Id: mod_virtuemart_onepagecheckout.php 05-24-2011
 * author: Polished Geek
 * Website: PolishedGeek.com
 * Email: info@PolishedGeek.com
 * Phone: 919-374-2425
 *
 * license http://www.gnu.org/copyleft/gpl.html GNU/GPL
 *
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );


$bAjaxMode = "true";


$myPath = dirname(__FILE__).DS;
 
JHTML::_('behavior.calendar');

JHTML::stylesheet('assets/css/onepage.css', 'modules/mod_virtuemart_onepagecheckout/');


JHTML::script('onepage.js', 'modules/mod_virtuemart_onepagecheckout/assets/js/', true);
JHTML::script('joomla.javascript.js', 'includes/js/', true);
JHTML::_('behavior.tooltip');
//JHTML::script('wz_tooltip.js', 'components/com_virtuemart/js/', true);



require_once( JPATH_ADMINISTRATOR.DS.'components'.DS.'com_virtuemart'.DS.'version.php' );

$myVmVersion = new vmVersion();

switch ($myVmVersion->RELEASE) {
	case '1.1.3':
		echo '<script type="text/javascript" src="/components/com_virtuemart/js/wz_tooltip.js"></script>';
		break;

	case '1.1.4':
	default:
		JHTML::script('wz_tooltip.js', 'components/com_virtuemart/js/', true);
		break;
}



global $VM_LANG, $vm_mainframe, $sess, $mm_action_url, $_SESSION, $db, $_REQUEST, $GLOBALS, $ps_html, $VM_LANG, $my, $vm_mainframe, $mainframe, $page, $shipping_rate_id, $total, $tax_total, $mosConfig_live_site, $order_total, $default, $tax_display;

$application = JFactory::getApplication();


require_once('lib' . DS . 'opco_config.php' );
require_once('lib' . DS . 'style_helper.php' );


// LOAD ALL PARAMETERS HERE
$opcoConfig = new OpcoConfig();
$opcoConfig->load($params);

if($opcoConfig->general->redirectWhenEmpty)
{
    if(!isset($_SESSION['cart']['idx']) || $_SESSION['cart']['idx'] == 0) 
    {
	    $application->redirect('index.php?option=com_virtuemart&page=shop.cart');
    }
}

// update session variables
$_SESSION['combine_tax'] = $opcoConfig->confirmPlaceOrder->combineTax;


$hash_secret = "VirtueMartIsCool";
$user_info_id = md5(uniqid( $hash_secret));

if(isset( $_SESSION['auth'])) {
	$auth = $_SESSION['auth'];

} else {
	$auth = array();
	$auth['user_id'] = 0;
}
$GLOBALS['auth'] = $auth;

if(isset($_REQUEST['Itemid'])) {
	$Itemid = $_REQUEST['Itemid'];
} else {
	$Itemid = null;
}

if(!isset($user_id)) {
	$user_id = '0';
}

jimport( 'joomla.plugin.plugin' );
jimport( 'joomla.application.module.helper');


$vmCompatFile = JPATH_ADMINISTRATOR.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_virtuemart'.DIRECTORY_SEPARATOR.'compat.joomla1.5.php';
if(file_exists($vmCompatFile)) {
	require_once($vmCompatFile);
	$my = $GLOBALS['my'];
}


$vmCfgFile = JPATH_ADMINISTRATOR.DS.'components'.DS.'com_virtuemart'.DS.'virtuemart.cfg.php';
if(file_exists($vmCfgFile))
	defined('ADMINPATH') or require_once($vmCfgFile);

/*
	$GLOBALS['VM_THEMECLASS'] = 'vmTheme';
	$vmThemePath = $mosConfig_absolute_path.DS.'components'.DS.'com_virtuemart'.DS.'themes'.DS.'default';
$vmThemeURL = $mosConfig_absolute_path.DS.'components'.DS.'com_virtuemart'.DS.'themes'.DS.'default'.DS.'theme.php';

if(file_exists($vmThemeURL)) {
	require_once($vmThemeURL);
		if( empty( $GLOBALS['vmThemeConfig'] ) || !empty( $_REQUEST['vmThemeConfig'])) {
			$GLOBALS['vmThemeConfig'] = new vmParameters( @file_get_contents($vmThemePath.DS.'theme.config.php'), $vmThemePath.DS.'theme.xml', 'theme');

		}

	$vmTheme = new vmTheme();

}

 */

// THE OLD PARAMETERS!!!

/*
$show_login = $params->get( 'show_login', 1);
$show_cart = $params->get( 'show_cart', 1);
$show_calculate_shipping = $params->get( 'show_calculate_shipping', 1);
$show_shipping = $params->get( 'show_shipping', 0);
$section_titles_color = $params->get('section_titles_color', '#000000');
$section_titles_bg = $params->get('section_titles_bg', '#ffffff');
$section_titles_tag = $params->get('section_titles_tag', 'h2');
$show_final_confirmation_page = $params->get('show_final_confirmation_page', 0);
$custom_thankyou = $params->get('custom_thankyou', 'checkout.thankyou');

$calc_tax_button = $params->get('calc_tax_button', 0);
$calc_tax_button_text = JText::_($params->get('calc_tax_button_text', 'Calculate Tax to proceed'));
$please_wait_message = JText::_($params->get('please_wait_message', 'Please wait, processing your request'));

$hide_non_default_shipping = $params->get('hide_non_default_shipping', 0);
$usernameisemail = $params->get('usernameisemail', 0);


$show_shipping_address = $show_shipping;

*/


$vmParserFile = JPATH_SITE.DS.'components'.DS.'com_virtuemart'.DS.'virtuemart_parser.php';
if( file_exists($vmParserFile)) {
	require_once( $vmParserFile);
}




$vmGlobalFile = JPATH_ADMINISTRATOR.DS.'components'.DS.'com_virtuemart'.DS.'global.php';
if(file_exists($vmGlobalFile)) {
	require_once($vmGlobalFile);
}



$vmDBFile = JPATH_ADMINISTRATOR.DS.'components'.DS.'com_virtuemart'.DS.'classes'.DS.'ps_database.php';

$basket_html = '';


require_once(CLASSPATH. 'ps_product.php' );
$ps_product = new ps_product;
require_once(CLASSPATH. 'ps_shipping_method.php' );
require_once(CLASSPATH. 'ps_checkout.php' );
$ps_checkout = new ps_checkout;

$opcoConfig->updateCheckout($ps_checkout, isset($auth['user_id']) && $auth['user_id'] != '0');

// clear message queue

// clear message queue
$session =& JFactory::getSession();
$session->set('application.queue', null);

$user = JFactory::getUser();

if(file_exists($vmDBFile)) {
	require_once($vmDBFile);


	if($_SESSION['auth']['user_id'] != 0) {
		$db = new ps_DB();
		$q =  "SELECT * FROM #__{vm}_user_info
                        WHERE user_id='" . $_SESSION['auth']['user_id'] . "'
                        AND address_type='BT' ";
		$db->query($q);
		$db->next_record();
		$ship_to_info_id = $db->f("user_info_id");

	} else {
		$db = null;
		$ship_to_info_id = "";
	}


	$shipping_rate_id = null;

	if(isset($_REQUEST['shipping_rate_id'])) {
		$shipping_rate_id = $_REQUEST['shipping_rate_id'];
	}

	$payment_method_id = null;
	if(isset($_REQUEST['payment_method_id'])) {
		$payment_method_id = $_REQUEST['payment_method_id'];
	}
}

$styleHelper = new StyleHelper();
$styleHelper->init($opcoConfig);

// must change it to be in the title....
echo $styleHelper->writeOpcoStyle();


if(!is_array($default)) {
	$default = array();
}

echo '<div id="' . PgStyles::$moduleClass . '">';

// REVIEW CART DETAILS SECTION
if ($opcoConfig->reviewCart->showSection
    && ($opcoConfig->reviewCart->showCart || $opcoConfig->reviewCart->showCoupons)) {

    $styleHelper->startSection($opcoConfig->reviewCart);
//	echo '<font color="'.$section_titles_color.'" ><'.$section_titles_tag.' style="BACKGROUND-COLOR: '.$section_titles_bg.'"  bgcolor="'.$section_titles_bg.'">'.JText::_('Review Cart Details').'</'.$section_titles_tag.'></font>';

    if ($opcoConfig->reviewCart->showCart)
    {
    	echo '<div class="vmcartmodule">';
	    $_session['vmusegreybox'] = '0';
	    $_session['vmcartdirection'] = '1';
	    $_session['vmminicart'] = '0';

	    $mm_action_url = JURI::root(true);
	    include('shop.basket_short.php');
	    $cart = $_SESSION['cart'];

        //$vmCartFile = JPATH_ADMINISTRATOR.DS.'components'.DS.'com_virtuemart'.DS.'html'.DS.'ro_basket.php';
        //if(file_exists($vmCartFile)) {
        //	require_once($vmCartFile);
        //}

	    $_REQUEST["page"] = 'checkout.index';
	    include_once( PAGEPATH . 'ro_basket.php' );

        //echo $basket_html;

        //$_SESSION['opco'] = '1';
        //$_SESSION['opco']['weight_total'] = $weight_total;

        $_SESSION['opco']['weight_total'] = $weight_total;

        $basket_html = '';

	    echo '</div>'; //vmcartmodule
    }

    // ENTER COUPON SECTION
    if (PSHOP_COUPONS_ENABLE == 1
        && $opcoConfig->reviewCart->showCoupons)
    {
        echo '<div id="couponsection">';
        require_once('coupon_ajax.php');
        echo '</div>'; //couponsection
    }

    $styleHelper->endSection();
}


// ACCOUNT & BILLING SECTION

// always shows the login/billing section right now
// should this be an option??
if (1 || $opcoConfig->billingShipping->showShippingSection >= 0)
{
   $styleHelper->startSection($opcoConfig->billingShipping);
}

// show login, registration and billing
if (1) 
{
	$open_to_stretcher = 0;

    //////////////////////
    // Login section    //
    //////////////////////

	$showRegistrationOption = true;
	$showGuestOption = true;
	$showLoginOption = true;

	switch(VM_REGISTRATION_TYPE) {
	case 'NO_REGISTRATION':
		$showRegistrationOption = false;
		$showGuestOption = false;
		$showLoginOption = false;
		break;

	case 'OPTIONAL_REGISTRATION':
		// Everything on
		break;

	case 'SILENT_REGISTRATION':
		$showRegistrationOption = false;
		break;

	case 'NORMAL_REGISTRATION':
		$showGuestOption = false;
		break;

	}
	

	if($showLoginOption) 
	{
    	if ($opcoConfig->billingShipping->useJanrain)
    	{
    		if (file_exists('.'.DS.'plugins'.DS.'system'.DS.'pg_janrain_helper.php'))
    		{
                require_once('.'.DS.'plugins'.DS.'system'.DS.'pg_janrain_helper.php');
    		    $janrainHelper = new JanrainHelper();
    		    
            	echo '<div style="clear:both;"></div>';
    		    echo '<div id="loginFormJanrain" class="loginFormJanrain" style="display:none;"><h4 class="loginFormJanrainLabel">'.JText::_("JANRAIN_LOGIN_WITH").'</h4>';
    		    echo $janrainHelper->getLoginLink('
    		    	<div><img src="' . JURI::root(true).'/plugins/system/pg_janrain/facebook.png" border="0" width="32" height="32" />
    		    	<img src="' . JURI::root(true).'/plugins/system/pg_janrain/google.png" border="0" width="32" height="32" />
    		    	<img src="' . JURI::root(true).'/plugins/system/pg_janrain/twitter.png" border="0" width="32" height="32" />
    		    	<img src="' . JURI::root(true).'/plugins/system/pg_janrain/linkedin.png" border="0" width="32" height="32" /></div>', '');
    		    echo $janrainHelper->includeRpxAuthScript();
    		    echo '</div>'; //loginFormJanrain
    
    		    if($auth['user_id'] > 60) {}
        		else 
        		{
        		    echo "
        			<script LANGUAGE=javascript>
        			document.getElementById('loginFormJanrain').style.visibility = 'visible';
        			document.getElementById('loginFormJanrain').style.display = 'block';
        		    </script>
        		    ";
        		}
    		}
    	}
	}
	
	$isChecked = ($open_to_stretcher==1)? 'checked="checked"': '';
	if($showLoginOption) 
	{
	    echo '<div id="logintoggle"><h4><input type="radio" name="togglecheck" id="login_radio" class=toggler" value="login" '.$isChecked.' onclick="javascript:showlogin();" onfocus="javascript:showlogin();">'.JText::_('Show Login').'</input>'; 
	}
	$isChecked = ($open_to_stretcher==0)? 'checked="checked"': '';
	if($showGuestOption) 
	{
		echo '<input type="radio" name="togglecheck" id="register_radio" class="toggler" value="register" '.$isChecked.' onclick="javascript:showregister();" onfocus="javascript:showregister();">'.JText::_('Guest checkout').'</input>';
		$isChecked = '';
	}
	else
	{ 
	    $isChecked = ($open_to_stretcher==0)? 'checked="checked"': ''; 
	}
	
	if($showRegistrationOption) 
	{
	    echo '<input type="radio" name="togglecheck" id="createaccount_radio" class="toggler" value="createaccount" '.$isChecked.' onclick="javascript:showCreateAccount();" onfocus="javascript:showCreateAccount();">'.JText::_('Create Account').'</input></h4>'; 
	}
	
	if($showLoginOption) 
	{
	    echo '</div>'; //logintoggle
	}

//    echo '<div>';

    // handle side-by-side layout    
    if ($opcoConfig->billingShipping->showShippingSection >= 0
        && $opcoConfig->billingShipping->sideBySideLayout)
    {
        echo '<div id="account_section" style="float:left;width:47%; padding:12px 0 0 12px;">';
    }
    else
    {
        echo '<div id="account_section" class="clearfix">';
    }
    
	echo '<div class="stretcher" id="loginsection" style="display: inline">';

	if($showLoginOption) {

		if($auth['user_id'] > 60) {
		    
			echo JText::_("Hello").", ".$user->name;

			echo "<script LANGUAGE=javascript>
			document.getElementById('logintoggle').style.visibility = 'hidden';
			document.getElementById('logintoggle').style.display = 'none';
			</script>";
		} else {
			echo '<div class="loginSection">';
            echo '<div class="loginForm">';
			echo '<form id=loginfrm name=loginfrm method=POST action="index.php?option=com_user&task=login">';
			echo '<label for="opcologinusername">'.JText::_('Username').'</label><br/><input type="text" class="inputbox" id="opcologinusername" required></input><br/>';
			echo '<label for="opcologinpassword">'.JText::_('Password').'</label><br/><input type="password" class="inputbox" id="opcologinpassword" required></input><br/>';
			echo '<input type="hidden" name="'.JUtility::getToken().'" value="1" id="logintoken" />';
			echo '<input type="hidden" name="return" value="'.base64_encode(JURI::root(true).'/modules/mod_virtuemart_onepagecheckout/login_return_ajax.php').'" id="return" />';  // modules/mod_virtuemart_onepagecheckout/login_return_ajax.php
			echo '<input type="hidden" name="remember" value="yes" id="remember" />';
			echo '<input type="hidden" name="op2" value="login" id="op2"/>';
			echo '<input type="button" id="loginbutton" name="login" class="button" value="'.JText::_('Login').'" onclick="javascript:loginsubmit();"/><br/>';
            echo '<img src="' . JURI::root(true).'/modules/mod_virtuemart_onepagecheckout/assets/images/spinner.gif" class="ajaxprogress" style="display: none;" />';
			echo '</form>';
			echo '</div>';  //loginForm
			if(isset($_GET['loginErrorMsg'])) {
				$loginErrorMsg = $_GET['loginErrorMsg'];
			} else {
				$loginErrorMsg = '';
			}
			echo '<div id="loginerror">' . $styleHelper->showError(JText::_($loginErrorMsg), '') . '</div>';
			echo '</div>'; //loginSection
		}
	}

	echo '</div>'; //loginsection

//////////////////////
// Register section //
//////////////////////

	echo '<form action="index.php" method="post" name="adminForm">';
	echo '<div class="stretcher" id="registersection">';
	require_once('checkout_register_form.php');
	echo '<input type="hidden" name="address_type_name" id="address_type_name_field" value="-default-" />';
	echo '</div>';

	if(isset($_GET['registrationErrorMsg'])) {
		$registrationErrorMsg = $_GET['registrationErrorMsg'];
	} else {
		$registrationErrorMsg = '';
	}
	echo '<div id="registrationError">' . $styleHelper->showError(JText::_($registrationErrorMsg), '') . '</div>';
	echo '<div style="clear:both;"></div>';

    echo '<input type="hidden" name="joomla_root" value="'.JURI::root(true).'/" id="joomla_root" /><br/>';

}

    echo '</div>';  //account_section

if($opcoConfig->billingShipping->showShippingSection == '0') {
    echo '<div id="opco_hide_shipping_address"></div>';
}

if($opcoConfig->billingShipping->showShippingSection >= 0) 
{

    // handle side-by-side layout    
    if ($opcoConfig->billingShipping->showShippingSection >= 0
        && $opcoConfig->billingShipping->sideBySideLayout)
    {
        echo '<div id="shipping_section" style="float:left;width:47%; padding:12px 0 0 12px;">';
    }
    else
    {
        echo '<div id="shipping_section" class="clearfix">';
    }

	echo '<br/><font size="3">'.JText::_('Please select a shipping address').':</font> <br/><br/>';
	echo '<div id="existingshippingaddress">';
	$ps_checkout->ship_to_addresses_radio($auth["user_id"], "ship_to_info_id", $ship_to_info_id);
	echo '</div>';

	echo '<table border="0" width="100%" cellpadding="2" cellspacing="0">';
	echo '<tr class="sectiontableentry1"><td class="radiooption">';

	echo '<input type="radio" name="ship_to_info_id" id="NEWSHIPADDRESS" value="NEWSHIPADDRESS" />';
	echo '</td><td class="radiolabel"><label for="NEWSHIPADDRESS">'.JText::_('New Shipping Address').'</label></td></tr></table>';


	echo '<div id="newshippingaddress">';
		$fields = ps_userfield::getUserFields( 'shipping' );
		ps_userfield::listUserFields( $fields, array(), $db, false );
    echo '<input type="hidden" name="checkout_this_step[]" value="CHECK_OUT_GET_SHIPPING_ADDR" />';


    $requiredShippingFields = Array(); 
    $allShippingFields = Array();
    foreach( $fields as $field ) {
    	if( $field->required == 1 ) {
    		$requiredShippingFields[$field->name] = $field->type;
    	}
    	$allShippingFields[$field->name] = $field->name;
    }

    $field_list = implode( "','", array_keys( $requiredShippingFields ) );
    $field_list = str_replace( "'email',", '', $field_list );
    $field_list = str_replace( "'username',", '', $field_list );
    $field_list = str_replace( "'password',", '', $field_list );
    $field_list = str_replace( "'password2',", '', $field_list );

    echo '<script language="javascript">opcoRequiredShippingFields = new Array(\'' . $field_list . '\');</script>';
    
    echo '</div>'; //newshippingaddress
	echo '<br style="clear:both;" />';
  
    echo '</div>';  //shipping_section
}
/* else {
	echo '<input type="hidden" name="ship_to_info_id" id="ship_to_info_id" value="'.$ship_to_info_id.'" CHECKED="CHECKED"/>';
}
 */

if ($opcoConfig->billingShipping->showShippingSection >= 0)
{
//    echo '</div>';

   $styleHelper->endSection();
}


if($opcoConfig->shippingInstr->showSection == '0') {
    echo '<div id="opco_hide_shipping_instructions"></div>';
}



// SHIPPING INSTRUCTIONS SECTION
if($opcoConfig->shippingInstr->showSection >= 0) {

    $styleHelper->startSection($opcoConfig->shippingInstr);

	echo '<div id="shippingtype">';
    if($opcoConfig->shippingInstr->hideNonDefaultMethods)
    {
	    echo '<div id="hide_non_default_shipping_div"></div>';
    }
	if(!$opcoConfig->shippingInstr->showCalcShippingButton) {
	        $totals = $ps_checkout->calc_order_totals($d);
       		extract($totals);
        	$order_total = $totals['order_total'];

        	$_SESSION['onepagetotal'] = $totals['order_subtotal'];
        	$_SESSION['onepagetax_total'] = $totals['order_tax'];
		//		require(JModuleHelper::getLayoutPath('mod_virtuemart_onepagecheckout', 'get_shipping_method.tpl'));
		require_once('tmpl/list_shipping_methods.tpl.php');
	}
	echo '</div>';

	if($opcoConfig->shippingInstr->showCalcShippingButton)
	{
	 	echo '<div id="calcshippinginstruction">'.JText::_('Please fill in your billing and shipping information then please calculate your shipping before continuing').'.</div>';
	 	echo '<div id="calcshippingerror"></div>';
		echo '<div>';
		echo '<input type="button" id="calculateshippingbutton" class="button" value="'.JText::_('Calculate Shipping').'" onclick="javascript:calculateshipping(true, false);" />';
        echo '<img src="' . JURI::root(true).'/modules/mod_virtuemart_onepagecheckout/assets/images/spinner.gif" class="ajaxprogress" style="display: none;" />';
        echo '<div style="clear:both;"></div>';
        echo '</div>';
	}

	echo '<input type="hidden" id="hidden_shipping_rate_id" name="hidden_shipping_rate_id" value="" />';
	echo '<input type="hidden" name="checkout_this_step[]" value="CHECK_OUT_GET_SHIPPING_METHOD" />';
	
	if(!$opcoConfig->shippingInstr->showCalcShippingButton)
	{
        echo '<div class="opcoShippingMethodUpdate"><img src="' . JURI::root(true).'/modules/mod_virtuemart_onepagecheckout/assets/images/spinner.gif" class="ajaxprogress" style="display: none;" /></div>';
	}
	
    $styleHelper->endSection();
}


// PAYMENT METHOD SECTION
if($opcoConfig->paymentMethod->showSection == '0') {
    echo '<div id="opco_hide_payment_section"></div>';
}
    echo '<input type="hidden" name="hidden_payment_method_name" id="hidden_payment_method" value="-1" />';

    echo '<div id="paymentmethodsection">';

    $styleHelper->startSection($opcoConfig->paymentMethod);

    require(JModuleHelper::getLayoutpath('mod_virtuemart_onepagecheckout', 'get_payment_method.tpl'));
    echo '<input type="hidden" name="checkout_this_step[]" value="CHECK_OUT_GET_PAYMENT_METHOD" />';

    $styleHelper->endSection();
    echo '</div>';


// CONFIRM AND PLACE ORDER SECTION
$styleHelper->startSection($opcoConfig->confirmPlaceOrder);

echo '<div id="opcoConfirmPlaceOrder">';

echo '<div id="opcoConfirmPlaceTotals">';

if ($opcoConfig->confirmPlaceOrder->showTotals) 
{
    echo '<div id="checkoutTotals"></div>';
}

if ($opcoConfig->confirmPlaceOrder->showUpdateTotalsButton) 
{
    echo '<div id="opcoConfirmPlaceTotalsButton">';
    echo '<input type="button" class="button" id="updatetotalsbutton" value="'.JText::_('Update Totals').'" onclick="javascript:updatetotals();" />';
    echo '<img src="' . JURI::root(true).'/modules/mod_virtuemart_onepagecheckout/assets/images/spinner.gif" class="ajaxprogress" style="display: none;" />';
    echo '</div>';
}
echo '<div style="clear:both"></div>';
echo '</div>';  //opcoConfirmPlaceTotals

echo '<div id="opcoConfirmPlaceFinal">';
echo '<div id="finalconfirmationsection">';

require(JModuleHelper::getLayoutpath('mod_virtuemart_onepagecheckout', 'get_final_confirmation.tpl'));

echo '</div>';
echo '<div style="clear:both"></div>';
echo '</div>';

echo '<div style="clear:both"></div>';
echo '</div>'; //opcoConfirmPlaceOrder


echo '<div id="checkoutProcessingError">';
if($opcoConfig->shippingInstr->showCalcShippingButton) {
	echo '<script LANGUAGE=javascript> disableProcessOrder(); requireShippingMethod = true; </script>';
	echo JText::_('You will need to click the "Calculate Shipping" button and have a shipping method selected before you can proceed with placing your order');
}
echo '</div>';

//echo '<input type="hidden" name="order_total" value="'.$order_total.'" />';

$styleHelper->endSection();


// CLOSING HIDDEN PARAMS
echo '<input type="hidden" id="hidden_ship_to_info_id" name="hidden_ship_to_info_id" value="" />';

echo '<input type="hidden" name="onepagecheckout" value="1" />';
echo '<input type="hidden" name="option" value="com_virtuemart" />';
echo '<input type="hidden" name="func" value="checkoutProcess" />';
echo '<input type="hidden" name="page" value="' . $opcoConfig->general->customThankyou .'" />';

echo '<input type="hidden" name="checkout_this_step[]" value="CHECK_OUT_GET_FINAL_CONFIRMATION" />';
echo '<input type="hidden" name="checkout_last_step" value="1" />';


if($opcoConfig->billingShipping->usernameIsEmail)
{
	echo '<div id="usernameisemail_div"></div>';
}




// temporary button text
// login button="loginbutton" id
// calc shipping button="calculateshippingbutton" id
// update totals button="updatetotalsbutton" id
// tax button="calculatetax_button" id
// process order button="formSubmitButton" id
//

if(!isset($please_wait_message)) {
	$please_wait_message = '';
}
echo '<input type="hidden" name="opco_module_title" value="'.$module->title.'" id="opco_module_title" />';
echo '<input type="hidden" name="please_wait_button_text" value="'.$please_wait_message.'" id="please_wait_button_text" />';
echo '<input type="hidden" name="loginbutton_tmp_text" value="tmp text" id="loginbutton_tmp_text" />';
echo '<input type="hidden" name="calculateshippingbutton_tmp_text" value="tmp text" id="calculateshippingbutton_tmp_text" />';
echo '<input type="hidden" name="updatetotalsbutton_tmp_text" value="tmp text" id="updatetotalsbutton_tmp_text" />';
echo '<input type="hidden" name="calculatetax_button_tmp_text" value="tmp text" id="calculatetax_button_tmp_text" />';
echo '<input type="hidden" name="formSubmitButton_tmp_text" value="tmp text" id="formSubmitButton_tmp_text" />';

// Janrain support:  prepopulate user information upon social login 
if ($opcoConfig->billingShipping->useJanrain)
{
    require_once('.'.DS.'plugins'.DS.'system'.DS.'pg_janrain_helper.php');
    $janrainHelper = new JanrainHelper();
			    
    $socialUserParams = $janrainHelper->getSocialUserParams();
    
    if (isset($socialUserParams))
    {
        $tempEmail = '';
        if (!isset($socialUserParams['joomla_email_temporary'])) 
        {
            $tempEmail = $socialUserParams['joomla_email'];
            echo '<input type="hidden" id="joomla_email" name="joomla_email" value="'.$tempEmail.'" />';
        }
        
        echo '<script language="javascript">';
    
        if (!isset($socialUserParams['joomla_email_temporary']) && !empty($tempEmail)) 
        {
            echo 'presetUserInfo(\'registersection\', \'email\', $(\'joomla_email\').value);';
            echo 'presetUserInfo(\'newshippingaddress\', \'email\', $(\'joomla_email\').value);';
        }
        
        if (isset($socialUserParams['profile']['name']['familyName'])) 
        {
            echo 'presetUserInfo(\'registersection\', \'last_name\', \'' . $socialUserParams['profile']['name']['familyName'] . '\');';
            echo 'presetUserInfo(\'newshippingaddress\', \'last_name\', \'' . $socialUserParams['profile']['name']['familyName'] . '\');';
        }
    
        if (isset($socialUserParams['profile']['name']['givenName'])) 
        {
            echo 'presetUserInfo(\'registersection\', \'first_name\', \'' . $socialUserParams['profile']['name']['givenName'] . '\');';
            echo 'presetUserInfo(\'newshippingaddress\', \'first_name\', \'' . $socialUserParams['profile']['name']['givenName'] . '\');';
        }
        
        if (isset($socialUserParams['profile']['gender'])) 
        {
            echo 'presetUserInfo(\'registersection\', \'vm_gender[]\', \'' . ucfirst($socialUserParams['profile']['gender']) . '\');';
        }
        
        echo '</script>';
    }
}

echo '</form>';
;
echo '</div>';

?>

