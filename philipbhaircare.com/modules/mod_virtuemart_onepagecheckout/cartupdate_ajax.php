<?php
/* Joomla! 1.5 One Page Checkout Module for VirtueMart
*
* @version $Id: cartupdate_ajax.php 05-24-2011
 * author: Polished Geek
 * Website: PolishedGeek.com
 * Email: info@PolishedGeek.com
 * Phone: 919-374-2425
 *
 * license http://www.gnu.org/copyleft/gpl.html GNU/GPL
 *
*/

defined('_JEXEC') or define( '_JEXEC', 1 );

$myPath = dirname(__FILE__).DS;

global $_SESSION, $_REQUEST, $CURRENCY_DISPLAY, $ps_cart, $my, $GLOBALS, $cache_id, $tax_display;

defined( 'DS') or define( 'DS', DIRECTORY_SEPARATOR );
defined( 'JPATH_BASE') or define('JPATH_BASE', dirname('..'.DS.'..'.DS.'configuration.php'));


require_once ( JPATH_BASE .DS.'includes'.DS.'defines.php' );
require_once ( JPATH_BASE .DS.'includes'.DS.'framework.php' );


$mainframe =& JFactory::getApplication('site');
$mainframe->initialise();

$session =& JFactory::getSession();
$user = JFactory::getUser();
$username = $user->name;

$_POST = $_REQUEST;

if($user->id > 0) {
	$_SESSION['auth']['user_id'] = $user->id;
	$auth = $_SESSION['auth'];
}

$vmCompatFile = JPATH_ADMINISTRATOR.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_virtuemart'.DIRECTORY_SEPARATOR.'compat.joomla1.5.php';
if(file_exists($vmCompatFile)) {
	require_once($vmCompatFile);
	$my = $GLOBALS['my'];
}

$vmCfgFile = JPATH_ADMINISTRATOR.DS.'components'.DS.'com_virtuemart'.DS.'virtuemart.cfg.php';
	if(file_exists($vmCfgFile))
		defined('ADMINPATH') or require_once($vmCfgFile);

$vmGlobalFile = JPATH_ADMINISTRATOR.DS.'components'.DS.'com_virtuemart'.DS.'global.php';
if(file_exists($vmGlobalFile)) {
	require_once($vmGlobalFile);
}

$vmDBFile = JPATH_ADMINISTRATOR.DS.'components'.DS.'com_virtuemart'.DS.'classes'.DS.'ps_database.php';

$vmShopperFile = JPATH_SITE.DS.'modules'.DS.'mod_virtuemart_onepagecheckout'.DS.'ps_shopper_onepage.php';
if( file_exists($vmShopperFile)  ) {
	require_once($vmShopperFile);
} else {
	JError::raiseNotice('errorMessage', 'Module not installed');
}

$language =& JFactory::getLanguage();
$language->load('mod_virtuemart_onepagecheckout', JPATH_BASE, $language->getTag());

// now must get params from the opco module
require_once ( 'mod_load_ajax.php' );

require_once(CLASSPATH.'ps_cart.php');
$ps_cart = new ps_cart;

$e = $_REQUEST;

// based on the type of user action
if ($_REQUEST["action"] == "update")
{
    if (CHECK_STOCK)
    {
        $product_in_stock = ps_product::get_field( $e["prod_id"], 'product_in_stock');

        if (empty($product_in_stock)) {
    		$product_in_stock = 0;
    	}
    	
    	if ($product_in_stock > 0 && (int)$e["quantity"] > $product_in_stock)
        {
            $e["quantity"] = $product_in_stock;
        }
    }
    
    list($minOrderSize,$maxOrderSize) = ps_product::product_order_levels($e["prod_id"]);
    
	if ($minOrderSize > 0 && (int)$e["quantity"] < $minOrderSize) {
        $e["quantity"] = $minOrderSize; 
	}

	if ($maxOrderSize > 0 && (int)$e["quantity"] > $maxOrderSize) {
        $e["quantity"] = $maxOrderSize; 
	}
    
    // update the cart item
    $ps_cart->update($e);
}
else if ($_REQUEST["action"] == "delete")
{
    // delete the cart item
    $ps_cart->delete($e);
}
else
{
    // do nothing
}

// clear message queue
$session =& JFactory::getSession();
$session->set('application.queue', null);

// return new shopping cart                                                                                                                                                                                                                                                          
//echo '<div class="vmcartmodule">';
$_session['vmusegreybox'] = '0';
$_session['vmcartdirection'] = '1';
$_session['vmminicart'] = '0';

$mm_action_url = JURI::root(true);
include('shop.basket_short.php');
$cart = $_SESSION['cart'];

//$vmCartFile = JPATH_ADMINISTRATOR.DS.'components'.DS.'com_virtuemart'.DS.'html'.DS.'ro_basket.php';
//if(file_exists($vmCartFile)) {
//	require_once($vmCartFile);
//}

$_REQUEST["page"] = 'checkout.index';
include_once( PAGEPATH . 'ro_basket.php' );

//echo $basket_html;

//$_SESSION['opco'] = '1';
//$_SESSION['opco']['weight_total'] = $weight_total;

$_SESSION['opco']['weight_total'] = $weight_total;

$basket_html = '';

//echo '</div>';

?>
