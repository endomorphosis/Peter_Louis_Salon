<?php
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' ); 
/* Joomla! 1.5 One Page Checkout Module for VirtueMart
*
* @version $Id: get_payment_method.tpl.php 05-24-2011
 * author: Polished Geek
 * Website: PolishedGeek.com
 * Email: info@PolishedGeek.com
 * Phone: 919-374-2425
 *
 * license http://www.gnu.org/copyleft/gpl.html GNU/GPL
 *
*/


		global $order_total, $sess, $VM_CHECKOUT_MODULES;
		$ps_vendor_id = $_SESSION['ps_vendor_id'];
		$auth = $_SESSION['auth'];
		
		$ship_to_info_id = vmGet( $_REQUEST, 'ship_to_info_id' );
		$shipping_rate_id = vmGet( $_REQUEST, 'shipping_rate_id' );
		
        require_once(CLASSPATH . 'ps_payment_method.php');
        $ps_payment_method = new ps_payment_method;
		require_once( CLASSPATH. 'ps_creditcard.php' );
	    $ps_creditcard = new ps_creditcard();
	    $count = 0;
		// Do we have Credit Card Payments?
		$db_cc  = new ps_DB;
		$q = "SELECT * from #__{vm}_payment_method,#__{vm}_shopper_group WHERE ";
		$q .= "#__{vm}_payment_method.shopper_group_id=#__{vm}_shopper_group.shopper_group_id ";
		$q .= "AND (#__{vm}_payment_method.shopper_group_id='".$auth['shopper_group_id']."' ";
		$q .= "OR #__{vm}_shopper_group.default='1') ";
		$q .= "AND (enable_processor='' OR enable_processor='Y') ";
		$q .= "AND payment_enabled='Y' ";
		$q .= "AND #__{vm}_payment_method.vendor_id='$ps_vendor_id' ";
		$q .= " ORDER BY list_order";
		$db_cc->query($q);
		
		if ($db_cc->num_rows()) {
			$first_payment_method_id = $db_cc->f("payment_method_id");
			$count += $db_cc->num_rows();
		    $cc_payments=true;
		}
		else {
		    $cc_payments=false;
		}
		
		$db_nocc  = new ps_DB;
		$q = "SELECT * from #__{vm}_payment_method,#__{vm}_shopper_group WHERE ";
		$q .= "#__{vm}_payment_method.shopper_group_id=#__{vm}_shopper_group.shopper_group_id ";
		$q .= "AND (#__{vm}_payment_method.shopper_group_id='".$auth['shopper_group_id']."' ";
		$q .= "OR #__{vm}_shopper_group.default='1') ";
		$q .= "AND (enable_processor='B' OR enable_processor='N' OR enable_processor='P') ";
		$q .= "AND payment_enabled='Y' ";
		$q .= "AND #__{vm}_payment_method.vendor_id='$ps_vendor_id' ";
		$q .= " ORDER BY list_order";
		$db_nocc->query($q);
		if ($db_nocc->next_record()) {
		    $nocc_payments=true;
		    $first_payment_method_id = $db_nocc->f("payment_method_id");
		    $count += $db_nocc->num_rows();
		    $db_nocc->reset();
		}
		else {
		    $nocc_payments=false;
		}

		/* Added by Chill Creations for iDEAL payment method */
		$db_ideal  = new ps_DB;
		$q = "SELECT * from #__{vm}_payment_method,#__{vm}_shopper_group WHERE ";
		$q .= "#__{vm}_payment_method.shopper_group_id=#__{vm}_shopper_group.shopper_group_id ";
		$q .= "AND (#__{vm}_payment_method.shopper_group_id='".$auth['shopper_group_id']."' ";
		$q .= "OR #__{vm}_shopper_group.default='1') ";
		$q .= "AND ((#__{vm}_payment_method.payment_class='ps_ccideal') ";
		$q .= "OR (#__{vm}_payment_method.payment_class='ps_ccideal_post_basic') ";
		$q .= "OR (#__{vm}_payment_method.payment_class='ps_ccideal_rabo_lite') ";
		$q .= "OR (#__{vm}_payment_method.payment_class='ps_ccideal_rabo_prof') ";
		$q .= "OR (#__{vm}_payment_method.payment_class='ps_ccideal_rabo_kassa') ";
		$q .= "OR (#__{vm}_payment_method.payment_class='ps_ccideal_rabo_inter') ";
		$q .= "OR (#__{vm}_payment_method.payment_class='ps_ccideal_abnamro_easy') ";
		$q .= "OR (#__{vm}_payment_method.payment_class='ps_ccideal_abnamro_zelf') ";
		$q .= "OR (#__{vm}_payment_method.payment_class='ps_ccideal_abnamro_only') ";
		$q .= "OR (#__{vm}_payment_method.payment_class='ps_ccideal_abnamro_kassa') ";
		$q .= "OR (#__{vm}_payment_method.payment_class='ps_ccideal_ogone_inter') ";
		$q .= "OR (#__{vm}_payment_method.payment_class='ps_ccideal_ing_twyp') ";
		$q .= "OR (#__{vm}_payment_method.payment_class='ps_ccideal_fortis_inter')) ";
		$q .= "AND payment_enabled='Y' ";
		$q .= "AND #__{vm}_payment_method.vendor_id='$ps_vendor_id'";
		$db_ideal->query($q);

		if ($db_ideal->num_rows()) {
			$ideal_payments=true;
			//$count = $db_ideal->num_rows();
			$ideal_payment_method_id = $db_ideal->f("payment_method_id");
		}
		else {
			$ideal_payments=false;
			$ideal_payment_method_id = 0;
		}
		/* End of iDEAL code */


if( $nocc_payments &&  $cc_payments ) {
	echo '<table><tr valign="top"><td width="50%">';
}
        
if ($cc_payments==true) { 
  	?>
	<fieldset><legend><strong><?php echo $VM_LANG->_('PHPSHOP_CHECKOUT_PAYMENT_CC') ?></strong></legend>
		<table border="0" cellspacing="0" cellpadding="2" width="100%">
		    <tr>
		        <td colspan="2">
		        	<?php $ps_payment_method->list_cc($payment_method_id, false) ?>
		        </td>
		    </tr>
		    <tr>
		        <td colspan="2"><strong>&nbsp;</strong></td>
		    </tr>
		    <tr>
		        <td nowrap width="10%" align="right"><?php echo $VM_LANG->_('VM_CREDIT_CARD_TYPE'); ?>:</td>
		        <td>
		        <?php echo $ps_creditcard->creditcard_lists( $db_cc ); ?>
		        <script language="Javascript" type="text/javascript"><!--
				writeDynaList( 'class="inputbox" name="creditcard_code" size="1"',
				orders, originalPos, originalPos, originalOrder );
				//-->
				</script>
		<?php 
		            $db_cc->reset();
		            $payment_class = $db_cc->f("payment_class");
		            $require_cvv_code = "YES";
		            if(file_exists(CLASSPATH."payment/$payment_class.php") && file_exists(CLASSPATH."payment/$payment_class.cfg.php")) {
		                require_once(CLASSPATH."payment/$payment_class.php");
		                require_once(CLASSPATH."payment/$payment_class.cfg.php");
		                $_PAYMENT = new $payment_class();
		                if( defined( $_PAYMENT->payment_code.'_CHECK_CARD_CODE' ) ) {
		                	$require_cvv_code = strtoupper( constant($_PAYMENT->payment_code.'_CHECK_CARD_CODE') );
		                }
		            }
		?>      </td>
		    </tr>
		    <tr valign="top">
		        <td nowrap width="10%" align="right">
		        	<label for="order_payment_name"><?php echo $VM_LANG->_('PHPSHOP_CHECKOUT_CONF_PAYINFO_NAMECARD') ?>:</label>
		        </td>
		        <td>
		        <input type="text" class="inputbox" id="order_payment_name" name="order_payment_name" value="<?php if(!empty($_SESSION['ccdata']['order_payment_name'])) echo $_SESSION['ccdata']['order_payment_name'] ?>" autocomplete="off" />
		        </td>
		    </tr>
		    <tr valign="top">
		        <td nowrap width="10%" align="right">
		        	<label for="order_payment_number"><?php echo $VM_LANG->_('PHPSHOP_CHECKOUT_CONF_PAYINFO_CCNUM') ?>:</label>
		        </td>
		        <td>
		        <input type="text" class="inputbox" id="order_payment_number" name="order_payment_number" value="<?php if(!empty($_SESSION['ccdata']['order_payment_number'])) echo $_SESSION['ccdata']['order_payment_number'] ?>" autocomplete="off" />
		        </td>
		    </tr>
		<?php if( $require_cvv_code == "YES" ) { 
					$_SESSION['ccdata']['need_card_code'] = 1;	
			?>
		    <tr valign="top">
		        <td nowrap width="10%" align="right">
		        	<label for="credit_card_code">
		        		<?php echo vmToolTip( $VM_LANG->_('PHPSHOP_CUSTOMER_CVV2_TOOLTIP'), '', '', '', $VM_LANG->_('PHPSHOP_CUSTOMER_CVV2_TOOLTIP_TITLE') ) ?>:
		        	</label>
		        </td>		        		
		        <td>
		            <input type="text" class="inputbox" id="credit_card_code" name="credit_card_code" value="<?php if(!empty($_SESSION['ccdata']['credit_card_code'])) echo $_SESSION['ccdata']['credit_card_code'] ?>" autocomplete="off" />
		        
		        </td>
		    </tr>
		<?php } ?>
		    <tr>
		        <td nowrap width="10%" align="right"><?php echo $VM_LANG->_('PHPSHOP_CHECKOUT_CONF_PAYINFO_EXDATE') ?>:</td>
		        <td><?php 
		        $ps_html->list_month("order_payment_expire_month", @$_SESSION['ccdata']['order_payment_expire_month']);
		        echo "/";
		        $ps_html->list_year("order_payment_expire_year", @$_SESSION['ccdata']['order_payment_expire_year']) ?>
		       </td>
		    </tr>
    	</table>
    </fieldset>
  <?php  
}

/* Added By Sundaram */
if($ideal_payments==true) {
	$GLOBALS['payment_selected'] = true;
?>

<fieldset><legend><strong><?php echo JText::_( 'ID_DIRECT_ONLINE'); ?></strong></legend>
<table border="0" cellspacing="0" cellpadding="2" width="100%">
	<tr>
		<td  valign="top" style="padding-top:20px;">
			<input type="radio" name="payment_method_id" id="Ideal" checked="checked" value="<?php echo $ideal_payment_method_id; ?>">
		</td>
<?php

	$db = new ps_DB;
	$q  = "SELECT payment_class FROM #__{vm}_payment_method WHERE payment_method_code='ID' LIMIT 0,1 ";
	$db->query( $q );
	$db->next_record();
	$cc_payment_class = $db->f("payment_class");
	require_once(CLASSPATH."payment/$cc_payment_class.cfg.php");

	if($conf_data['IDEAL_enable_image']) {

		$config =& JFactory::getConfig();
		$site_langauge = $config->getValue('config.language');
		if($site_langauge == "nl-NL") {
			$image_path = $mosConfig_live_site . '/administrator/components/com_ccideal/assets/nl-NL_ccideal.jpg';
		}
		else {
			$image_path = $mosConfig_live_site . '/administrator/components/com_ccideal/assets/en-GB_ccideal.jpg';
		}
	if($conf_data['IDEAL_enable_tips']=='1')
	{

 	     if($conf_data['IDEAL_checkout_lable'] != '') {
		JHTML::script ('mootools.js', JURI::base(). '/media/system/js');
		$doc 		=& JFactory::getDocument();
		$js = "
			window.addEvent('domready', function(){
				var Tips1 = new Tips($$('.Tips1'));
			});
		";
		$doc->addScriptDeclaration($js);
		$tips = 'title="'.$conf_data['IDEAL_checkout_tips'].'"';
   	    }
  	    else {
		$tips = "";
	    }
	}else{
	       $tips = "";
	     }


?>
<style type="text/css">
.tool-tip {
	color: #000;
	width: 200px;
	z-index: 13000;
}

.tool-text {
	font-size: 12px;
	/*font-weight: bold;*/
	padding: 2px;
}

</style>
		<td valign="middle">
		<img class="Tips1" src="<?php echo $image_path; ?>" width="304" height="84" alt="iDeal" <?php echo $tips; ?> />
<?php
	}
	else if($conf_data['IDEAL_checkout_lable'] != ''){
				echo '<td valign="middle" style="padding-top:20px;">';
				echo $conf_data['IDEAL_checkout_lable'];
	}
	else{
				echo '<td valign="middle" style="padding-top:20px;">';
				echo 'iDeal';
	}
?>
		</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
</table>
</fieldset>
<?php
}
/* Ended */



if( $nocc_payments &&  $cc_payments ) {
	echo '</td><td width="50%">';
}

if ($nocc_payments==true) {
    if ($cc_payments==true) { 
    	$title = $VM_LANG->_('PHPSHOP_CHECKOUT_PAYMENT_OTHER');
    }
    else {
    	$title = $VM_LANG->_('PHPSHOP_ORDER_PRINT_PAYMENT_LBL');
    }
    	
   ?>
    <fieldset><legend><strong><?php echo $title ?></strong></legend>
		<table border="0" cellspacing="0" cellpadding="2" width="100%">
		    <tr>
		        <td colspan="2"><?php 
		            $ps_payment_method->list_nocheck($payment_method_id,  false); 
		            $ps_payment_method->list_bank($payment_method_id,  false);
		            $ps_payment_method->list_paypalrelated($payment_method_id,  false); ?>
		        </td>
		    </tr>
		 </table>
	</fieldset>
	<?php
}

if( $nocc_payments &&  $cc_payments ) {
	echo '</td></tr></table>';
}
?>
