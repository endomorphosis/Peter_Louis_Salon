<?php 
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' );
/* Joomla! 1.5 One Page Checkout Module for VirtueMart
*
* @version $Id: checkout_register_form.php 05-24-2011
 * author: Polished Geek
 * Website: PolishedGeek.com
 * Email: info@PolishedGeek.com
 * Phone: 919-374-2425
 *
 * license http://www.gnu.org/copyleft/gpl.html GNU/GPL
 *
*/

mm_showMyFileName( __FILE__ );
global $mosConfig_allowUserRegistration, $mosConfig_useractivation, $default, $_REQUEST, $missing, $user;

require_once( CLASSPATH . "ps_userfield.php" );
require_once( CLASSPATH . "htmlTools.class.php" );

$missing = vmGet( $_REQUEST, "missing", "" );

if (!empty( $missing )) {
	echo "<script type=\"text/javascript\">alert('".$VM_LANG->_('CONTACT_FORM_NC',false)."'); </script>\n";
}

// If not using NO_REGISTRATION, redirect with a warning when Joomla doesn't allow user registration
if ($mosConfig_allowUserRegistration == "0" && VM_REGISTRATION_TYPE != 'NO_REGISTRATION' ) {
	$msg = $VM_LANG->_('USER_REGISTRATION_DISABLED');
	vmRedirect( $sess->url( 'index.php?page='.HOMEPAGE, true, false ), $msg );
	return;
}

if( vmIsJoomla( '1.5' ) ) {
	// Set the validation value
	$validate = JUtility::getToken();
} else {
	$validate =  function_exists( 'josspoofvalue' ) ? josSpoofValue(1) : vmSpoofValue(1);
}

$fields = ps_userfield::getUserFields('registration', false, '', true );
// Read-only fields on registration don't make sense.
foreach( $fields as $field ) $field->readonly = 0;
$skip_fields = array();

if (  $my->id > 0 || (VM_REGISTRATION_TYPE != 'NORMAL_REGISTRATION' && VM_REGISTRATION_TYPE != 'OPTIONAL_REGISTRATION' 
	&& ( $page == 'checkout.index' || $page == 'shop.registration' ) )  ) {
	// A listing of fields that are NOT shown
	$skip_fields = array( 'username', 'password', 'password2' );
	if(/* $my->id*/ 0 ) {
		$skip_fields[] = 'email';
	}
}

$skip_fields[] = 'agreed';

if($db != null) {
	$default['email'] = $db->f('user_email');
}

// This is the part that prints out ALL registration fields!
ps_userfield::listUserFields( $fields, $skip_fields, $db , false);



$requiredRegistrationFields = Array(); 
$allRegistrationFields = Array();
foreach( $fields as $field ) {
	if( $field->required == 1 ) {
		$requiredRegistrationFields[$field->name] = $field->type;
	}
	$allfields[$field->name] = $field->name;
}
foreach( $skip_fields as $skip ) {			
	unset($requiredRegistrationFields[$skip]); 
}


$field_list = implode( "','", array_keys( $requiredRegistrationFields ) );
$field_list = str_replace( "'email',", '', $field_list );
$field_list = str_replace( "'username',", '', $field_list );
$field_list = str_replace( "'password',", '', $field_list );
$field_list = str_replace( "'password2',", '', $field_list );

echo '<script language="javascript">opcoRequiredRegistrationFields = new Array(\'' . $field_list . '\');</script>';


echo '

<div align="center">';
    
	if( !$mosConfig_useractivation && VM_SHOW_REMEMBER_ME_BOX && VM_REGISTRATION_TYPE == 'NORMAL_REGISTRATION' ) {
		echo '<input type="checkbox" name="remember" value="yes" id="remember_login2" checked="checked" />
		<label for="remember_login2">'. $VM_LANG->_('REMEMBER_ME') .'</label><br /><br />';
	}
	else {
		if( VM_REGISTRATION_TYPE == 'NO_REGISTRATION' ) {
			$rmbr = '';
		} else {
			$rmbr = 'yes';
		}
		echo '<input type="hidden" name="remember" value="'.$rmbr.'" />';
	}


	echo '
	</div>
	<input type="hidden" name="Itemid" value="'. $sess->getShopItemid() .'" />
	<input type="hidden" name="gid" value="'. $my->gid .'" />
	<input type="hidden" name="id" value="'. $my->id .'" />
	<input type="hidden" name="user_id" value="'. $my->id .'" />
	<input type="hidden" name="option" value="com_virtuemart" />
	<input type="hidden" name="' . $validate . '" value="1" />
	<input type="hidden" name="useractivation" value="'. $mosConfig_useractivation .'" />
	<input type="hidden" name="func" value="shopperadd" />
	<input type="hidden" name="page" value="checkout.index" />
	';



/*
	echo '
		<input type="submit" value="'. $VM_LANG->_('BUTTON_SEND_REG') . '" class="button" onclick="return( submitregistration());" />
	</div>
	<input type="hidden" name="Itemid" value="'. $sess->getShopItemid() .'" />
	<input type="hidden" name="gid" value="'. $my->gid .'" />
	<input type="hidden" name="id" value="'. $my->id .'" />
	<input type="hidden" name="user_id" value="'. $my->id .'" />
	<input type="hidden" name="option" value="com_virtuemart" />
	<input type="hidden" name="' . $validate . '" value="1" />
	<input type="hidden" name="useractivation" value="'. $mosConfig_useractivation .'" />
	<input type="hidden" name="func" value="shopperadd" />
	<input type="hidden" name="page" value="checkout.index" />
	</form>';
 */
?>
