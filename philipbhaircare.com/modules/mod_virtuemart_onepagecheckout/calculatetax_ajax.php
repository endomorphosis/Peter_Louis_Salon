<?php

/* Joomla! 1.5 One Page Checkout Module for VirtueMart
*
* @version $Id: calculatetax_ajax.php 05-24-2011
 * author: Polished Geek
 * Website: PolishedGeek.com
 * Email: info@PolishedGeek.com
 * Phone: 919-374-2425
 *
 * license http://www.gnu.org/copyleft/gpl.html GNU/GPL
 *
*/

global $_SESSION, $_REQUEST, $order_total, $_POST, $VM_LANG, $vm_mainframe, $sess, $mm_action_url, $db, $GLOBALS, $ps_html, $my, $mainframe, $ship_to_info_id, $shipping_rate_id, $weight_total, $vmLogger, $auth, $mosConfig_live_site;

define( '_JEXEC', 1 );
define( 'DS', DIRECTORY_SEPARATOR );
define('JPATH_BASE', dirname('..'.DS.'..'.DS.'configuration.php'));

require_once ( JPATH_BASE .DS.'includes'.DS.'defines.php' );
require_once ( JPATH_BASE .DS.'includes'.DS.'framework.php' );

jimport( 'joomla.error.error' );


if(isset($_REQUEST['shipping_rate_id'])) {
	$shipping_rate_id = $_REQUEST['shipping_rate_id'];
} else {
	$shipping_rate_id = '';
}

if( !defined( '_VALID_MOS' )) {define('_VALID_MOS', 1);}

$mainframe =& JFactory::getApplication('site');
$mainframe->initialise();

$session =& JFactory::getSession();

$user = JFactory::getUser();

$username = $user->name;

$_POST = $_REQUEST;


if($user->id > 0) {
	$_SESSION['auth']['user_id'] = $user->id;
	$auth = $_SESSION['auth'];
}


$vmCompatFile = JPATH_ADMINISTRATOR.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_virtuemart'.DIRECTORY_SEPARATOR.'compat.joomla1.5.php';
if(file_exists($vmCompatFile)) {
	require_once($vmCompatFile);
	$my = $GLOBALS['my'];
}



$vmCfgFile = JPATH_ADMINISTRATOR.DS.'components'.DS.'com_virtuemart'.DS.'virtuemart.cfg.php';
	if(file_exists($vmCfgFile))
		defined('ADMINPATH') or require_once($vmCfgFile);

$vmGlobalFile = JPATH_ADMINISTRATOR.DS.'components'.DS.'com_virtuemart'.DS.'global.php';
if(file_exists($vmGlobalFile)) {
	require_once($vmGlobalFile);
}





$vmDBFile = JPATH_ADMINISTRATOR.DS.'components'.DS.'com_virtuemart'.DS.'classes'.DS.'ps_database.php';


$vmShopperFile = JPATH_SITE.DS.'modules'.DS.'mod_virtuemart_onepagecheckout'.DS.'ps_shopper_onepage.php';
if( file_exists($vmShopperFile)  ) {
	require_once($vmShopperFile);
} else {
	JError::raiseNotice('errorMessage', 'Module not installed');
}

//require_once(CLASSPATH. 'ps_shipping_method.php' );
//require_once(CLASSPATH. 'ps_checkout.php' );
//$ps_checkout = new ps_checkout();

$language =& JFactory::getLanguage();
$language->load('mod_virtuemart_onepagecheckout', JPATH_BASE, $language->getTag());

require_once ( 'mod_load_ajax.php' );

$d = $_REQUEST;

$totals = $ps_checkout->calc_order_totals($d);
extract($totals);

$_SESSION['onepagetotal'] = $totals['order_subtotal'];
$_SESSION['onepagetax_total'] = $totals['order_tax'];


$basket_html = '';

$myReturn = true;
$myErrorMessage = "";
$_SESSION['VMOnepageCheckout'] = 1;





	$differentShipping = false;
	if($_POST['ship_to_info_id'] == 'NEWSHIPADDRESS') {
		$differentShipping = true;
		$_POST['ship_to_info_id'] = '';
		$_REQUEST['ship_to_info_id'] = '';
		$ship_to_info_id = '';

	}


	if(isset($_REQUEST['ship_to_info_id'])) {
		$ship_to_info_id = $_REQUEST['ship_to_info_id'];
	}

	$mosConfig_live_site = SECUREURL;	
    require_once('includes'.DS.'account_ajax.php');

/*	if(isset($_SESSION['checkout_user_info']) && strlen($_SESSION['checkout_user_info']) > 0) {
		$_POST['ship_to_info_id'] = $_SESSION['checkout_user_info'];
		$ship_to_info_id = $_POST['ship_to_info_id'];
		$d['ship_to_info_id'] = $_POST['ship_to_info_id'];
		$_REQUEST['ship_to_info_id'] = $_POST['ship_to_info_id'];

		JError::raiseNotice('errorMessage', 'Info set from registering user: '.$_POST['ship_to_info_id']);
		$auth['user_id'] = $_SESSION['auth']['user_id'];
					
} else { */
		if($myReturn && ((strlen($_REQUEST['ship_to_info_id']) < 15) || ($_REQUEST['ship_to_info_id'] == "NEWSHIPADDRESS")))
		if(isset($_SESSION['auth']['user_id']) ) {
			$db =& JFactory::getDBO();
			$query = 'SELECT user_info_id FROM #__vm_user_info WHERE user_id="'.$_SESSION['auth']['user_id'].'" AND address_type="BT"';
                	$db->setQuery($query);
                      	$_POST['ship_to_info_id'] = $db->loadResult();
			$ship_to_info_id = $_POST['ship_to_info_id'];
			$d['ship_to_info_id'] = $_POST['ship_to_info_id'];
			$_REQUEST['ship_to_info_id'] = $_POST['ship_to_info_id'];
//			JError::raiseNotice('errorMessage', 'Info set from DB: '.$_POST['ship_to_info_id']);
		} else {
			$joomlaErrors = JError::getErrors();

			//$myErrorMessage .= count($joomlaErrors) > 0 ? $joomlaErrors[0]->getMessage() : "";
			JError::raiseNotice('errorMessage', "Failed to login or add user.");
			$myReturn = false;
			
		}
//	}

	if($myReturn)	
	if($differentShipping) {
		$tmpREQUEST = array();

		foreach($_REQUEST as $key => $value) {
			if(strpos($key, '_shipto')) {
				$key = str_replace('_shipto', '', $key);
				$tmpREQUEST[$key] = $value;
			}
	
		}
		$tmpREQUEST['address_type'] = 'ST';
		$tmpREQUEST['vmtoken'] = $_POST['vmtoken'];
		$tmpREQUEST['user_id'] = $_SESSION['auth']['user_id'];


		$db =& JFactory::getDBO();
		$query = 'SELECT user_info_id FROM #__vm_user_info WHERE user_id="'.$_SESSION['auth']['user_id'].'" AND address_type="BT"';
               	$db->setQuery($query);
		$_POST['user_info_id'] = $db->loadResult();
		$tmpREQUEST['user_info_id'] = $_POST['user_info_id'];

		$vmUserAddressFile = JPATH_ADMINISTRATOR.DS.'components'.DS.'com_virtuemart'.DS.'classes'.DS.'ps_user_address.php';
		if(file_exists($vmUserAddressFile)) {
			require_once($vmUserAddressFile);

			$userAddressClass = new ps_user_address();
			if(!$userAddressClass->add($tmpREQUEST)) {
				JError::raiseNotice('errorMessage', 'Failed to add Ship to Address'.print_r($tmpREQUEST, true));
				JError::raiseNotice('errorMessage', print_r($_REQUEST, true));
				$myReturn = false;
				$myErrorMessage = "Failed to add Ship to Address: ".print_r($tmpREQUEST, true);
			} else {
				$ship_to_info_id = $_REQUEST['ship_to_info_id'];
				$d['ship_to_info_id'] = $_REQUEST['ship_to_info_id'];
 
			}
							

		} else {
			JError::raiseNotice('errorMessage', 'Virtuemart not installed');
			$myReturn = false;
			$myErrorMessage = "Virtuemart not installed";
		}
					


	}




	$weight_total = $_SESSION['cart']['weight_total'];


echo "<div id=checkoutProcessingError>";

if($myReturn) {
	echo '<input type="hidden" id="new_ship_info" value="'.print_r($_REQUEST['ship_to_info_id'], true).'" />';
	echo "</div><br>";
	echo "<script LANGUAGE=javascript>taxCalculated();</script><br>";
} else {
	$joomlaErrors = JError::getErrors();
	$myErrorMessage = '';
	for($i=0; $i < count($joomlaErrors); $i++) {
		$myErrorMessage .= $joomlaErrors[$i]->getMessage() .'<br/>';
	}

	if (isset($errorInRegistration))
	{
	    echo $styleHelper->showError(JText::_('There was an error processing your registration.'), $myErrorMessage);
	}
	else
	{
    	echo $styleHelper->showError(JText::_('There was an error calculating your tax.'), $myErrorMessage);
	}
		
	echo "</div><br>";
}
 


?>

