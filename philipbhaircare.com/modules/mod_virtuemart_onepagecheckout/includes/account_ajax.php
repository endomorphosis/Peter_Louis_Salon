<?php
/* Joomla! 1.5 One Page Checkout Module for VirtueMart
*
* @version $Id: account_ajax.php 05-24-2011
* author: Polished Geek
* Website: PolishedGeek.com
* Email: info@PolishedGeek.com
* Phone: 919-374-2425
*
* license http://www.gnu.org/copyleft/gpl.html GNU/GPL
*
*/

defined('_JEXEC') or define( '_JEXEC', 1 );

$auth = $_SESSION['auth'];
$vmLogger->debug('auth: '.$auth['user_id']);

$mosConfig_live_site = SECUREURL;	

if(!isset($_SESSION['auth']['user_id']) || $_SESSION['auth']['user_id'] == 0 || !isset($_SESSION['auth']['first_name']) || $_SESSION['auth']['first_name'] == 'guest') {

	$shopperClassOnePage = new vm_ps_shopper_onepage();
	if(!$shopperClassOnePage->addshopper($d)) {
		JError::raiseNotice('errorMessage', 'Failed to add user info.');
		$myReturn = false;
	}

	if(isset($d['checkout_user_info'])) {
		$_SESSION['checkout_user_info'] = $d['checkout_user_info'];
	}
		
} else {
	$d["user_id"] = $auth["user_id"];
	$shopperClassOnePage = new vm_ps_shopper_onepage();
	if(!$shopperClassOnePage->update($d)) {
		JError::raiseNotice('errorMessage', 'Failed to update user info.');
		$myReturn = false;
	}
} 

?>
