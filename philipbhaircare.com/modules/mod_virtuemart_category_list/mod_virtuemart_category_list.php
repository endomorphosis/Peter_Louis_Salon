<?php
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' );
/**
* NOTE: THIS MODULE REQUIRES AN INSTALLED VirtueMart Component!
*
* @module Phoca - VirtueMart Category List
* @copyright Copyright (C) Jan Pavelka www.phoca.cz
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
* @component - VirtueMart
* @copyright (C) 2004-2008 soeren - All Rights Reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
*
* VirtueMart is Free Software.
* VirtueMart comes with absolute no warranty.
*
* www.virtuemart.net
*/

// Load the virtuemart main parse code
if( file_exists(dirname(__FILE__).'/../../components/com_virtuemart/virtuemart_parser.php' )) {
	require_once( dirname(__FILE__).'/../../components/com_virtuemart/virtuemart_parser.php' );
	$mosConfig_absolute_path = realpath( dirname(__FILE__).'/../..' );
} else {
	require_once( dirname(__FILE__).'/../components/com_virtuemart/virtuemart_parser.php' );
}

//$category_id = vmGet( $_REQUEST, 'category_id');

global $VM_LANG, $sess;
if( vmIsJoomla('1.5' )) {
	$vm_path = $mosConfig_absolute_path.'/modules/mod_virtuemart';
} else {
	$vm_path = $mosConfig_absolute_path.'/modules';
}

require_once(CLASSPATH.'ps_product.php');
// PARAMS
$numColumns 	= $params->get('columns_num', 3);
$displayImg 	= $params->get('display_img', 1);
$displayAllCategories 	= $params->get('display_all_categories', 0);
		
$categories 	= array();
$ip 			= 0;
$categoriesAll 	=	getPhocaCategoryTreeArray( );
foreach ($categoriesAll as $key => $value){
	if ($displayAllCategories == 0) {
		if ($value['category_parent_id'] == 0) {
			$categories[$ip]['category_name'] 			= $value['category_name'];
			$categories[$ip]['category_child_id'] 		= $value['category_child_id'];
			$categories[$ip]['category_thumb_image'] 	= $value['category_thumb_image'];
			$ip++;
		}
	} else {
		$categories[$ip]['category_name'] 			= $value['category_name'];
		$categories[$ip]['category_child_id'] 		= $value['category_child_id'];
		$categories[$ip]['category_thumb_image'] 	= $value['category_thumb_image'];
		$ip++;
	}
	
}

$columns 			= (int)$numColumns;
$countCategories 	= count($categories);
$begin				= array();
$end				= array();
$begin[0]			= 0;// first
$begin[1]			=  ceil($countCategories / $columns);
$end[0]				= $begin[1] -1;

for ( $jp = 2; $jp < $columns; $jp++ ) {
	$begin[$jp]	= ceil(($countCategories / $columns) * $jp);
	$end[$jp-1]	= $begin[$jp] - 1;
}

$end[$jp-1]		= $countCategories - 1;// last
$endFloat		= $countCategories - 1;
echo '<div style="width: 100%;margin: 8px 0 0 0;padding: 0;float: left;"><table align="center" ><tr>';
for ($kp = 0; $kp < $countCategories; $kp++) {
$span = '<span style="display: block;width: 7px;height: 27px;float: left;margin: 0 10px;">&nbsp;&nbsp;&nbsp;</span>';
if($kp == 0 OR $kp == 6)
{
	$span = "";
}
if($kp == 6)
{
	echo '</tr></table><table align="center"><tr>';
}
	echo '<td class="menu_table" align="left" valign="middle" >'.$span.'<a class="peace_href" style="" href="'. $sess->url( URL .'index.php?page=shop.browse&amp;category_id=' . $categories[$kp]['category_child_id']). '">'.$categories[$kp]['category_name'].'</a></td>';
	
}
echo '</tr></table></div>';
function getPhocaCategoryTreeArray( $only_published=true, $keyword = "" ) {

	$db = new ps_DB;

	// Get published categories
	$query  = "SELECT category_id, category_description, category_name,category_child_id as cid, category_parent_id as pid,list_order, category_publish, category_thumb_image
				FROM #__{vm}_category, #__{vm}_category_xref WHERE ";
	if( $only_published ) {
		$query .= " #__{vm}_category.category_publish='Y' AND ";
	}
	$query .= " #__{vm}_category.category_id=#__{vm}_category_xref.category_child_id ";
	if( !empty( $keyword )) {
		$query .= " AND ( category_name LIKE '%$keyword%' ";
		$query .= " OR category_description LIKE '%$keyword%' ";
		$query .= " ) ";
	}
	$query .= " ORDER BY #__{vm}_category.list_order ASC, #__{vm}_category.category_name ASC";

	// initialise the query in the $database connector
	// this translates the '#__' prefix into the real database prefix
	$db->query( $query );

	$categories = Array();
	// Transfer the Result into a searchable Array

	while( $db->next_record() ) {
		$categories[$db->f("cid")]["category_child_id"] = $db->f("cid");
		$categories[$db->f("cid")]["category_parent_id"] = $db->f("pid");
		$categories[$db->f("cid")]["category_name"] = $db->f("category_name");
		$categories[$db->f("cid")]["category_description"] = $db->f("category_description");
		$categories[$db->f("cid")]["list_order"] = $db->f("list_order");
		$categories[$db->f("cid")]["category_publish"] = $db->f("category_publish");
		$categories[$db->f("cid")]["category_thumb_image"] = $db->f("category_thumb_image");
	}
		
	return $categories;
}
?>