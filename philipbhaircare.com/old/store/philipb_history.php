<? include($template_include_file);?>
<table valign: top; width="720" cellspacing="0" style="vertical-align: top; padding: 0px; margin: 0px; width: 720px; border-width: 0px; font-size: 11px; color: #ffffff;">
<tr style="background-repeat: no-repeat;">
<td bgcolor="#bab8b8" height="744" style="  background-image: url('./images/gradient.png'); background-color: #bab8b8; background-repeat: repeat-x; ">
<p style="color: #ffffff; font-size: 11px; margin-left: 1em; margin-right: 1em;">
As Hollywood's preeminent hair-treatment expert, Philip B. has garnered international praise for his visionary approach to hair, scalp and body-care treatments. His award-winning blends, all based on pure botanicals and essential oils at potent, unheard-of-in-the-industry concentrations, have dramatically changed the way the world perceives luxury care for hair and skin. American Vogue said Philip's Four Step Hair & Scalp Treatment "gives you the hair of a heroine in a romance novel – the hair you always dreamed of" Forbes Magazine declared it one of "the Top 100 things that are worth every penny."
</p>
<p style="color: #ffffff; font-size: 11px; margin-left: 1em; margin-right: 1em;">
Philip was propelled into the treatment business as a fledgling stylist in Boston and later, Los Angeles. Concerned about clients who came to him with fried, over-processed hair that wouldn't respond to regular conditioning treatments, he began whipping up his own natural remedies at home. Within a few years, he was working as a stylist and colorist to some of Hollywood's most prominent actors and industry players. As his career took him on photo shoots and film sets around the globe, he began a quest for his Holy Grail: the finest hair-care treatments in the world.
</p>
<p style="color: #ffffff; font-size: 11px; margin-left: 1em; margin-right: 1em;">
What he found along the way shocked him: Most shampoos -- even in luxury lines -- were predictable formulations of detergent, water, and fragrance. Conditioners too waxy and heavy! Conspicuously missing were the quality ingredients; i.e., proven botanicals at active levels high enough to ensure performance and results.
</p>
<p style="color: #ffffff; font-size: 11px; margin-left: 1em; margin-right: 1em;">
Philip decided to spark a change in an industry with infinite room for improvement. Back in L.A., he huddled with laboratory chemists, gathered an arsenal of natural ingredients (meticulously sourced from all over the world) and began formulating his own concoctions. His friends, family, and clients volunteered as guinea pigs: Upon arrival at his house/lab, they were often handed an unmarked bottle from the kitchen and pointed to the shower.
</p>
<p style="color: #ffffff; font-size: 11px; margin-left: 1em; margin-right: 1em;">
Philip took on the most challenging hurdle of all first: to develop a hair-conditioning oil gossamer-light enough to enter the hair shaft, without leaving it sticky, heavy, or limp. The result - Philip B Rejuvenating Oil - became an instant word-of-mouth phenomenon. Stars called Philip in the middle of the night for damaged-hair emergencies; his treatments were booked weeks in advance; and -- once beauty editors caught on in 1991 -- the product sold out across the country.
</p>
<p style="color: #ffffff; font-size: 11px; margin-left: 1em; margin-right: 1em;">
Philip gave up sleeping. Meanwhile, his fledgling collection launched internationally to equal acclaim: Philip's in-store appearances drew women by the hundreds, and royal families from around the world called for private appointments.
</p>
<p style="color: #ffffff; font-size: 11px; margin-left: 1em; margin-right: 1em;">
Today, Rejuvenating Oil remains the flagship treatment in the Philip B Botanical Products portfolio of luxury hair, scalp and body-care products. Other best-sellers include his cooling and invigorating Peppermint & Avocado Volumizing & Clarifying Shampoo (part of his core, Four Step Hair & Scalp Treatment); his warming, honey, cinnamon, cardamom, black tea and ginger-infused Chai Latte Soul & Body Wash; and pH Restorative Detangling Toning Mist, which works like a skin toner to give hair a perfect finish, for maximum body and shine. A high concentration of botanicals infuses Philip B. products with natural fragrance; all are available paraben-free. The line is now sold in thousands of upscale department stores, boutiques, salons and spas around the world. Products are also available at philipb.com - a great site for hair care tips, as well.
</p>
<p style="color: #ffffff; font-size: 11px; margin-left: 1em; margin-right: 1em;">
Celebrated as a pioneer in the hair care industry, Philip remains at the forefront of hair and scalp science. Each of his products is designed to evoke a feeling – warming, cooling or refreshing – while giving skin a radiant glow or making hair glossy and manageable, the natural way.
</p>
<p style="color: #ffffff; font-size: 11px; margin-left: 1em; margin-right: 1em;">
Fully involved in every aspect of his business, Philip continues to formulate all products, traveling extensively to track down new ingredients and stay in touch with the needs of his customers and fans. His company is marked by a personal touch that is often lost in modern beauty corporations. For those fortunate enough to receive his best-of-the-best treatments in person, Philip B. is a revelation: a professional with worlds of knowledge to impart and a larger-than-life character with an extravagant sense of fun.
</p>
</td>
<td width="200" style="background-color: #000000; background-image: url('./images/philipb00.png'); background-repeat: no-repeat;">
</td>
</tr>
</table>