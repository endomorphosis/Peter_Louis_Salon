    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/jquery.lightbox-0.5.js"></script>
    <link rel="stylesheet" type="text/css" href="css/jquery.lightbox-0.5.css" media="screen" />

  <script type="text/javascript">
    $(function() {
        $('#gallery a').lightBox();
    });
    </script>
   	<style type="text/css">
	/* jQuery lightBox plugin - Gallery style */
	#gallery {
		background-color:#FFFFFF;
		padding:0px;
		width: 520px;
	}
	#gallery ul { list-style: none; }
	#gallery ul li { display: inline; }
	#gallery ul img {
		border: 5px solid #3e3e3e;
		border-width: 5px 5px 20px;
	}
	#gallery ul a:hover img {
		border: 5px solid #fff;
		border-width: 5px 5px 20px;
		color: #fff;
	}
	#gallery ul a:hover { color: #fff; }
	</style>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td width="40">&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td width="40" height="617">&nbsp;</td>
<td>
<p><span style="font-size: 12pt;">
<table class="ae_noborder" border="0" cellpadding="0" cellspacing="0" width="100%">
<tbody>
<tr>
<td colspan="2" valign="top" width="100%">
<span style="font-size: 12pt;"><img alt="Hair Services" src="hair_services_147x27.gif" border="0" height="27" width="147"></span></td>
</tr>
<tr>
<td valign="top" width="15%"><span style="font-size: 12pt;"><font class="title"><b>&nbsp; Eyelash</b></font></span></td>
<td valign="top" width="85%">&nbsp;</td></tr>
<tr>
<td width="15%" rowspan="2" valign="top"><span style="font-size: 12pt;">
<table class="ae_noborder" id="table1" border="0" cellpadding="4" width="100%">
<tbody>
<tr>
<td><span style="font-size: 12pt;"><img src="Image1.gif" border="0" height="88" width="117"></span></td>
</tr>
<tr>
<td>
<span style="font-size: 12pt;"><img src="Image2.gif" border="0" height="88" width="117"></span></td>
</tr>
<tr>
<td>
<span style="font-size: 12pt;"><img src="Image3.gif" border="0" height="88" width="117"></span></td>
</tr>
</tbody>
</table>
</span></td>
<td valign="top" width="85%"><span style="font-size: 12pt;">
<p class="bodytext"><br>Eyelash Extensions are a totally new way to extend the length and thickness of your eyelashes. They are applied on a hair-by-hair basis to your own lashes for a totally natural look. When properly applied, Lashes can last up to two months or more. Touch-ups are recommended after initial full set application to replace any lashes that might have fallen off. Properly applied lashes will last for the length of the natural growth cycle of each eyelash hair. With a full set of eyelash extensions there is no need to wear mascara. Finally one less thing to worry about in the morning. Beautiful Batting eyes as soon as you wake up! Using custom eyelash placements we can accentuate the shape of your eyes!</p>
<p class="bodytext">The cost of a full set of lashes may range from $250 to $450. Call for a complimentary consultation. 212.319.0019</p></span>&nbsp;</td>
</tr>
<tr>
  <td align="center" valign="top">
  <div id="gallery">
  <table border="0" cellpadding="5" style="border:1px solid #000000;">
  <tr>
    <td align="left" valign="top"><a href="images/!cid_004301c914ac$e4b2bfe0$3e02a8c0@zahir.jpg" title="Before"><img src="images/!cid_004301c914ac$e4b2bfe0$3e02a8c0@zahir.jpg" width="280" border="0" /></a></td>
    <td align="left" valign="top"><a href="images/!cid_005001c914ac$ed8497b0$3e02a8c0@zahir.jpg" title="After"><img src="images/!cid_005001c914ac$ed8497b0$3e02a8c0@zahir.jpg" width="280" border="0" /></a></td>
  </tr>
  <tr>
    <td align="center" valign="top">Before</td>
    <td align="center" valign="top">After</td>
  </tr>  
  <tr>
  	<td  align="left" valign="top"><a href="images/IMG_2552.jpg" title="Before"><img src="images/IMG_2552.jpg" width="280"  border="0" /></a></td>
    <td  align="left" valign="top"><a href="images/IMG_2557.jpg" title="After"><img src="images/IMG_2557.jpg" width="280" border="0"  /></a></td>
  <tr/>
  <tr>
    <td align="center" valign="top">Before</td>
    <td align="center" valign="top">After</td>
  </tr>  
<tr>  
  	<td  align="left" valign="top"><a href="images/IMG_2553.jpg" title="Before"><img src="images/IMG_2553.jpg" width="280" border="0"  /></a></td>
    <td  align="left" valign="top"><a href="images/IMG_2556.jpg" title="After"><img src="images/IMG_2556.jpg" width="280" border="0"  /></a></td>

  </tr>
  <tr>
    <td align="center" valign="top">Before</td>
    <td align="center" valign="top">After</td>
  </tr>
</table>
</div>  
  
  </td>
</tr>
<tr>
<td></td>
</tr>
<tr>
<td width="100%" height="112" colspan="2" align="left" valign="top">
<p>&nbsp;</p>
<p></p></td>
</tr>
</tbody>
</table>
</span></p>
</td>
</tr>
</tbody>
</table>
