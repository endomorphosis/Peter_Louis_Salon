<?php
/*------------------------------------------------------------------------
# plg_vm_sef - Search Engine Friendly URL's for Virtuemart
# ------------------------------------------------------------------------
# author    Jeremy Magne
# copyright Copyright (C) 2010 Daycounts.com. All Rights Reserved.
# Websites: http://www.daycounts.com
# Technical Support: http://www.daycounts.com/en/contact/
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# See http://daycounts.com/fr/component/content/article/7 for details
-------------------------------------------------------------------------*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.plugin.plugin');
jimport ('joomla.application.component.helper');
jimport( 'joomla.filesystem.file');

/**
* Joomla! SEF Plugin
*
* @package 		Joomla
* @subpackage	System
*/
class plgSystemVm_Sef extends JPlugin
{
	/**
	 * Constructor
	 *
	 * For php4 compatability we must not use the __constructor as a constructor for plugins
	 * because func_get_args ( void ) returns a copy of all passed arguments NOT references.
	 * This causes problems with cross-referencing necessary for the observer design pattern.
	 *
	 * @param	object		$subject The object to observe
	  * @param 	array  		$config  An array that holds the plugin configuration
	 * @since	1.0
	 */
	function plgSystemVm_Sef(&$subject, $config)  {
		
		// Check if Virtuemart component is installed/enabled
		if (! JComponentHelper::isEnabled ( 'com_virtuemart', true )) {
			return false;
		}
		// Check if Virtuemart config exists
		$vm_config = JPATH_ADMINISTRATOR . '/components/com_virtuemart/virtuemart.cfg.php';
		if (! is_file ( $vm_config ))
			return false;

		parent::__construct($subject, $config);
	}

	function onAfterInitialise() {
		//die('titi');
	}

    function onAfterDispatch() {

		global $option;
		
		// if Admin side, just exit
		$application = JFactory::getApplication();
		if ($application->isAdmin()) { 
			return;
		}

		if($option != 'com_virtuemart') {
			return;
		}

		//Disable rewrite for store administrators
		require_once (JPATH_ADMINISTRATOR.DS."components".DS."com_vm_sef".DS.'classes'.DS.'helper.php');
		$helper = new VmSefHelper();
		$config = $helper->VmSefConfig;
		$enable_redirect = true;
		if ($config->disable_admin_rewrite) {
			global $perm;
			if (isset($perm)) {
				if ($perm->check("admin,storeadmin")) {
					 $enable_redirect = false;
				}
			}
		}

		//Redirect non-sef urls to sef urls
		$redirect_code = $this->params->get('redirect_code', '301 Moved Permanently');
		if ($enable_redirect && $redirect_code && $_SERVER['REQUEST_METHOD'] == 'GET') {
			$uri = JURI::getInstance();
			global $sess;
			$url = $sess->url('index.php'.$uri->toString(array('query')));
			$newuri = JURI::getInstance($url);

			//Replace index2.php with index.php?tmpl=component
			$i=strpos($uri->getPath(),'index2.php');
			if (isset($i) && is_numeric($i)) {
				$newuri->setVar('tmpl','component');
			}
			//Remove the output lite and replace by tmpl=component
			if ($uri->getVar('output')=='lite') {
				$newuri->delVar('output');
				$newuri->setVar('tmpl','component');
			}

			$newuri->setScheme($uri->getScheme());
			$newuri->setHost($uri->getHost());
			if ($uri->getVar('option')=='com_virtuemart' && $newuri->getVar('option')!='com_virtuemart') {
				$newurl = $newuri->toString(array('scheme','host','path','query'));
	
				if (headers_sent()) {
					echo '<script type="text/javascript">document.location.href=\''.$newurl.'\';</script>';
				} else {
					@ob_end_clean(); // clear output buffer
					//header( 'HTTP/1.1 301 Moved Permanently' );
					header( 'HTTP/1.1 '.$redirect_code );
					header( "Location: ". $newurl );
				}
			}
		}

		//Set Meta Keywords
		if ($this->params->get('metakey', '1') || $this->params->get('metadesc', '1')) {
			require_once(CLASSPATH . 'ps_product.php' );
			$document	=& JFactory::getDocument();
			$metakeywords = array();
			$metadesc = array();
			$product_id = 0;
			$category_id = 0;
			$manufacturer_id = 0;
			$vendor_id = 0;
			$metakey_combine_joomla = $this->params->get('metakey_combine_joomla', 'append');
			if ($metakey_combine_joomla == 'prepend') {
				$metakeywords[] = $document->getMetadata('keywords');
			}
			if ($product_id = JRequest::getVar('product_id')) {
				$dbsef = JFactory::getDBO();
				$q  = "SELECT metakey, metadesc FROM #__vm_sef_products WHERE published=1 AND product_id = '".$product_id."'";
				$dbsef->setQuery($q);
				$meta = $dbsef->loadObject();
				if (is_object($meta)) {
					if ($meta->metakey) {
						$metakeywords[] = $meta->metakey;
					}
					if ($meta->metadesc) {
						$metadesc['product'] = $meta->metadesc;
					}
				}
				$manufacturer_id = ps_product::get_manufacturer_id($product_id);
				$vendor_id = ps_product::get_vendor_id($product_id);
			}
			if ($category_id = JRequest::getVar('category_id')) {
				$dbsef = JFactory::getDBO();
				$q  = "SELECT metakey, metadesc FROM #__vm_sef_categories WHERE published=1 AND category_id = '".$category_id."'";
				$dbsef->setQuery($q);
				$meta = $dbsef->loadObject();
				if (is_object($meta)) {
					if ($meta->metakey) {
						$metakeywords[] = $meta->metakey;
					}
					if ($meta->metadesc) {
						$metadesc['category'] = $meta->metadesc;
					}
				}
			}
			if ($manufacturer_id = (JRequest::getVar('manufacturer_id')) ? JRequest::getVar('manufacturer_id') : $manufacturer_id) {
				$dbsef = JFactory::getDBO();
				$q  = "SELECT metakey, metadesc FROM #__vm_sef_manufacturers WHERE published=1 AND manufacturer_id = '".$manufacturer_id."'";
				$dbsef->setQuery($q);
				$meta = $dbsef->loadObject();
				if (is_object($meta)) {
					if ($meta->metakey) {
						$metakeywords[] = $meta->metakey;
					}
					if ($meta->metadesc) {
						$metadesc['manufacturer'] = $meta->metadesc;
					}
				}
			}
			if ($vendor_id = (JRequest::getVar('vendor_id')) ? JRequest::getVar('vendor_id') : $vendor_id) {
				$dbsef = JFactory::getDBO();
				$q  = "SELECT metakey, metadesc FROM #__vm_sef_vendors WHERE published=1 AND vendor_id = '".$vendor_id."'";
				$dbsef->setQuery($q);
				$meta = $dbsef->loadObject();
				if (is_object($meta)) {
					if ($meta->metakey) {
						$metakeywords[] = $meta->metakey;
					}
					if ($meta->metadesc) {
						$metadesc['vendor'] = $meta->metadesc;
					}
				}
			}
			if ($metakey_combine_joomla == 'append') {
				$metakeywords[] = $document->getMetadata('keywords');
			}

			$document->setMetadata('keywords', implode(',',$metakeywords));
			$page = JRequest::getVar('page');
			switch ($page) {
				case 'shop.browse':
					if ($metadesc['category']) {
						$document->setMetadata('description', $metadesc['category']);
					} else if ($metadesc['manufacturer']) {
						$document->setMetadata('description', $metadesc['manufacturer']);
					}
					break;
				case 'shop.product_details':
					if ($metadesc['product']) {
						$document->setMetadata('description', $metadesc['product']);
					}
					break;
				case 'shop.manufacturer_page':
					if ($metadesc['manufacturer']) {
						$document->setMetadata('description', $metadesc['manufacturer']);
					}
					break;
				case 'shop.infopage':
					if ($metadesc['vendor']) {
						$document->setMetadata('description', $metadesc['vendor']);
					}
					break;
			}
		}
		
		//Transform search post into get
		$keyword = JRequest::getVar('keyword',null,'post');
		if ($keyword) {
			$uri = JURI::getInstance();
			$uri->setVar('keyword',$keyword);
			$uri->setVar('page','shop.browse');
			$href = $uri->toString();
			$application->redirect($href);
		}

	}

	/**
     * Converting the site URL to fit to the HTTP request
     */
	function onAfterRender() {
		$app =& JFactory::getApplication();
		if($app->getName() != 'site') {
			return true;
		}
		
		//Prevent fail if Virtuemart is not installed
		if (!JFile::exists(JPATH_SITE.DS.'components'.DS.'com_virtuemart'.DS.'virtuemart_parser.php')) {
			return true;
		}
		
		$parse_content = intval($this->params->get('parse_content', 0));
		if(!$parse_content) {
			return true;
		}

		//require_once(JPATH_SITE.DS.'components'.DS.'com_virtuemart'.DS.'virtuemart_parser.php' );

		//Replace src links
      	$base   = JURI::base(true).'/';
		$buffer = JResponse::getBody();
		
//		// pull out contents of editor to prevent URL changes inside edit area
//		$editor =& JFactory::getEditor();
//		$regex = '#'.$editor->_tagForSEF['start'].'(.*)'.$editor->_tagForSEF['end'].'#Us';
//		preg_match_all($regex, $buffer, $editContents, PREG_PATTERN_ORDER);
//
//		// create an array to hold the placeholder text (in case there are more than one editor areas)
//		$placeholders = array();
//		for ($i = 0; $i < count($editContents[0]); $i++) {
//			$placeholders[] = $editor->_tagForSEF['start'].$i.$editor->_tagForSEF['end'];
//		}
//		
//		// replace editor contents with placeholder text
//		$buffer 	= str_replace($editContents[0], $placeholders, $buffer);
		
		global $mosConfig_live_site;
	
		$regex = "@((?:[-A-Z0-9+&/%~_.|:])*)(index(?:2?).php)([-A-Z0-9+&/%?=~_|:,.;]*)(option=com_virtuemart)([-A-Z0-9+&#/%=~_.|;]*)@si";
      	$buffer = preg_replace_callback($regex, array('plgSystemVm_Sef', 'vm_route'), $buffer );

		// restore the editor contents
		$buffer 	= str_replace($placeholders, $editContents[0], $buffer);
		
		JResponse::setBody($buffer);
		return true;
	}

	
	function vm_route(&$match) {
		$uri = JURI::getInstance($match[0]);
		//Verify again that we are not rewriting anything else than virtuemart.
		if ($uri->getVar('option')=='com_virtuemart') {
			$skipRewrite = false;
		
			//Replace index2.php with index.php?tmpl=component
			$i=strpos($uri->getPath(),'index2.php');
			if (isset($i) && is_numeric($i)) {
				$uri->setPath(str_replace('index2.php','index.php',$uri->getPath()));
				$uri->setVar('tmpl','component');
			}
			//Remove the output lite and replace by tmpl=component
			if ($uri->getVar('output')=='lite') {
				$uri->delVar('output');
				$uri->setVar('tmpl','component');
			}
			//Remove the output lite and replace by tmpl=component
			//if ($uri->getVar('pop')=='1') {
			//	$uri->delVar('pop');
			//	$uri->setVar('tmpl','component');
			//}
			//Skip if the url is not complete (product item dropdown) where the product id is added by javascript
			if ($uri->getVar('page')=='shop.product_details' && $uri->getVar('product_id')=='') {
				$skipRewrite=true;
			}
			
			//Remove the base path from the uri
			//echo 'Root:'.$uri->root(true);
			//if ($uri->root(true)) {
			//	$uri->setScheme($uri->getScheme().$uri->root(true));
			//	$uri->setPath(str_replace($uri->root(true),'',$uri->getPath()));
			//}
			//Get a compatible url to parse route.
			$url  = $uri->toString(array('path', 'query'));
			if (!$skipRewrite) {
				$sef = JRoute::_($url);
				$newUri = JURI::getInstance($sef);
				//Copy some values from old to new uri;
				$newUri->setScheme($uri->getScheme());
				$newUri->setHost($uri->getHost());
				$newUri->setPort($uri->getPort());
				$newUri->setFragment($uri->getFragment());
				$newurl = $newUri->toString();
				
				return $newUri->toString();
			} else {
				return $uri->toString();
			}
		}
		//return the default url if not a virtuemart one
		return $uri->toString();
	}
	
}
