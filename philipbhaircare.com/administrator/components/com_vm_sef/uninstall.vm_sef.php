<?php
/*------------------------------------------------------------------------
# vm_sef - Search Engine Friendly URL's for Virtuemart PRO
# ------------------------------------------------------------------------
# author    Jeremy Magne
# copyright Copyright (C) 2010 Daycounts.com. All Rights Reserved.
# Websites: http://www.daycounts.com
# Technical Support: http://www.daycounts.com/en/contact/
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# See http://daycounts.com/fr/component/content/article/7 for details
-------------------------------------------------------------------------*/

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.helper');
jimport('joomla.filesystem.file');
jimport('joomla.installer.installer');

	
	$vm_prefix = 'vm';
	if (JFile::exists(JPATH_SITE.DS.'components'.DS.'com_virtuemart'.DS.'virtuemart_parser.php')) {
		require_once(JPATH_SITE.DS.'components'.DS.'com_virtuemart'.DS.'virtuemart_parser.php');
		$vm_prefix = VM_TABLEPREFIX;
	}
	
	$db	=& JFactory::getDBO();
	$q = "DELETE FROM `#__{vm}_function` WHERE function_class='ps_sef'";
	$db->setQuery( str_replace("{vm}",$vm_prefix,$q) );
    $db->query();

//	$q = "DELETE FROM `#__{vm}_module` WHERE module_name='sef'";
//	$db->setQuery( str_replace("{vm}",$vm_prefix,$q) );
//    $db->query();

	$db->setQuery(  "DROP TABLE `#__vm_sef_pages` " );
    $db->query();
	$db->setQuery(  "DROP TABLE `#__vm_sef_products` " );
    $db->query();
	$db->setQuery(  "DROP TABLE `#__vm_sef_categories` " );
    $db->query();
	$db->setQuery(  "DROP TABLE `#__vm_sef_manufacturers` " );
    $db->query();
	$db->setQuery(  "DROP TABLE `#__vm_sef_vendors` " );
    $db->query();
	$db->setQuery(  "DROP TABLE `#__vm_sef_urls` " );
    $db->query();

	if (JFile::exists(JPATH_ADMINISTRATOR.DS."components".DS."com_vm_sef".DS."com_vm_sef.cfg.xml")) {
		JFile::delete(JPATH_ADMINISTRATOR.DS."components".DS."com_vm_sef".DS."com_vm_sef.cfg.xml");
	}
	if (JFile::exists(JPATH_SITE.DS."components".DS."com_virtuemart".DS."router.php")) {
		JFile::delete(JPATH_SITE.DS."components".DS."com_virtuemart".DS."router.php");
	}
	
	$lang =& JFactory::getLanguage();
	$lang->load('com_installer',JPATH_ADMINISTRATOR);		

	$installer = new JInstaller;
	$query = "SELECT `id` FROM `#__modules` WHERE `module` = 'mod_vm_sef_qicon'";
	$db->setQuery($query);
	$modID = $db->loadResult();
	$result = $installer->uninstall('module', $modID, 1 );

	$query = "SELECT `id` FROM `#__plugins` WHERE `folder`='system' AND `element`='vm_sef'";
	$db->setQuery($query);
	$plgID = $db->loadResult();
	$result = $installer->uninstall('plugin', $plgID );
	

$out = '<div style="margin:5px; padding 5px;"><div>'
. $msg
. '<br />Search Engine Friendly URL\'s for Virtuemart was uninstalled'
. '</div></div>' . "\n";

echo $out;
