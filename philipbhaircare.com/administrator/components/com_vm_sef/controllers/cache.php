<?php
/*------------------------------------------------------------------------
# vm_sef - Search Engine Friendly URL's for Virtuemart
# ------------------------------------------------------------------------
# author    Jeremy Magne
# copyright Copyright (C) 2010 Daycounts.com. All Rights Reserved.
# Websites: http://www.daycounts.com
# Technical Support: http://www.daycounts.com/en/contact/
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
-------------------------------------------------------------------------*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport('joomla.application.component.controller');
jimport('joomla.filesystem.file');
jimport('joomla.application.helper');

require_once (JPATH_COMPONENT.DS.'classes'.DS.'config.php');
require_once (JPATH_COMPONENT.DS.'classes'.DS.'helper.php');
require_once(JPATH_SITE.DS.'components'.DS.'com_virtuemart'.DS.'virtuemart_parser.php' );

/**
 * @package		Joomla
 * @subpackage	Config
 */
class VmSefCacheController extends JController
{
	/**
	 * Custom Constructor
	 */
	function __construct( $default = array())
	{
		parent::__construct( $default );

		$this->registerTask('apply','save');
		$this->registerTask('unpublish','publish');
		$this->registerTask('unlock','lock');
	}

	function display( )	{
		JRequest::setVar( 'hidemainmenu', 0 );
		//JRequest::setVar( 'layout', 'form'  );
		JRequest::setVar( 'edit', false  );
		global $option;

		$view = JRequest::getCmd('view');
		if(empty($view)) {
			JRequest::setVar('view', 'cache');
			$view='rules';
		};

		$this->getConflicts();
		parent::display();
	}

	function publish() {
		$mainframe = & JFactory::getApplication('site');
		global $option;

		// Check for request forgeries
		JRequest::checkToken() or jexit( 'Invalid Token' );

		$db 	=& JFactory::getDBO();

		$cid		= JRequest::getVar( 'cid', array(), '', 'array' );
		$publish	= ( $this->getTask() == 'publish' ? 1 : 0 );

		if (count( $cid ) < 1)
		{
			$action = $publish ? 'publish' : 'unpublish';
			JError::raiseError(500, JText::_( 'Select an item to' .$action, true ) );
		}

		JArrayHelper::toInteger($cid);
		$cids = implode( ',', $cid );
		$query = 'UPDATE #__vm_sef_urls SET published = '. (int) $publish. ' WHERE id IN ( '. $cids .' )';
		$db->setQuery( $query );
		if (!$db->query())
		{
			JError::raiseError(500, $db->getErrorMsg() );
		}

		$mainframe->redirect( 'index.php?option='.$option.'&view=cache' ,$action);
	}
	
	function purge() {
		$mainframe = & JFactory::getApplication('site');
		global $option;
		// Check for request forgeries
		JRequest::checkToken() or jexit( 'Invalid Token' );
		$db 	=& JFactory::getDBO();

		//$query = 'DELETE FROM #__vm_sef_urls WHERE locked = 0';
		$query = 'TRUNCATE TABLE #__vm_sef_urls';
		$db->setQuery( $query );
		if (!$db->query())
		{
			JError::raiseError(500, $db->getErrorMsg() );
		}
		$_SESSION['sef_modified'] = 0;

		$query = 'OPTIMIZE TABLE  `jos_vm_sef_urls`';
		$db->setQuery( $query );
		$db->query();
		
		//jimport('joomla.cache.cache');
		//$cache = & JFactory::getCache('com_vm_sef');
		//$cache->clean('com_vm_sef');

		$mainframe->redirect( 'index.php?option='.$option.'&view=cache');
	}

	function lock() {
		$mainframe = & JFactory::getApplication('site');
		global $option;

		// Check for request forgeries
		JRequest::checkToken() or jexit( 'Invalid Token' );

		$db 	=& JFactory::getDBO();

		$cid		= JRequest::getVar( 'cid', array(), '', 'array' );
		$lock		= ( $this->getTask() == 'lock' ? 1 : 0 );

		if (count( $cid ) < 1)
		{
			$action = $lock ? 'lock' : 'unlock';
			JError::raiseError(500, JText::_( 'Select an item to' .$action, true ) );
		}

		JArrayHelper::toInteger($cid);
		$cids = implode( ',', $cid );
		$query = 'UPDATE #__vm_sef_urls SET `locked` = '. (int) $lock. ' WHERE id IN ( '. $cids .' )';
		$db->setQuery( $query );
		if (!$db->query())
		{
			JError::raiseError(500, $db->getErrorMsg() );
		}

		$mainframe->redirect( 'index.php?option='.$option.'&view=cache' ,$action);
	}

	function remove() {
		$mainframe = & JFactory::getApplication('site');
		global $option;
		// Check for request forgeries
		JRequest::checkToken() or jexit( 'Invalid Token' );

		$db		=& JFactory::getDBO();
		$cid	= JRequest::getVar( 'cid', array(), '', 'array' );

		JArrayHelper::toInteger($cid);
		$msg = '';

		$cids = implode( ',', $cid );

		$query = 'DELETE FROM #__vm_sef_urls WHERE id IN ( '. $cids .' )';
		$db->setQuery( $query );
		if (!$db->query())
		{
			JError::raiseError(500, $db->getErrorMsg() );
		}
	
		$mainframe->redirect( 'index.php?option='.$option.'&view=cache' , JText::_('Urls deleted'));
	}
	

	function getConflicts() {

		$db	=& JFactory::getDBO();
		$q = 'SELECT count(id) as nb, rewrited as alias 
				FROM #__vm_sef_urls p 
				WHERE published=1 
				GROUP BY rewrited
				HAVING count(id) > 1 ';
		$q = str_replace("{vm}",VM_TABLEPREFIX,$q);
		$db->setQuery( $q );
		$conflicts = $db->loadObjectList();
		$wrongAlias = '';
		foreach ($conflicts as $conflict) {
			$wrongAlias .= sprintf(JText::_('PHPSHOP_SEF_DUPLICATE_ALIAS'), $conflict->alias, $conflict->nb);
			$q = "SELECT sef.id, sef.rewrited, sef.published
				FROM #__vm_sef_urls sef
				AND alias = '".$conflict->alias."' ";
			$q = str_replace("{vm}",VM_TABLEPREFIX,$q);
			$db->setQuery( $q );
			$conflict_items = $db->loadObjectList();
			$wrongAlias .= '<div style="padding-left:40px; text-indent:0;">';
			foreach ($conflict_items as $conflict_item) {
				$link 		= JRoute::_( 'index.php?tmpl=component&option=com_vm_sef&view=alias&task=edit&type=page&id='.$conflict_item->id );
				$wrongAlias .= "<a class=\"modal\" rel=\"{handler: 'iframe', size: {x: 450, y: 180}}\" href=\"" . $link ."\">".$conflict_item->rewrited."</a>";
				$wrongAlias .= "<br/>";
			}
			$wrongAlias .= "</div>";
		}
		
		if ($wrongAlias) {
			JError::raiseWarning( 100, $wrongAlias );
		}
	}

}