<?php
/*------------------------------------------------------------------------
# vm_sef - Search Engine Friendly URL's for Virtuemart
# ------------------------------------------------------------------------
# author    Jeremy Magne
# copyright Copyright (C) 2010 Daycounts.com. All Rights Reserved.
# Websites: http://www.daycounts.com
# Technical Support: http://www.daycounts.com/en/contact/
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
-------------------------------------------------------------------------*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport('joomla.application.component.controller');
jimport('joomla.filesystem.file');
jimport('joomla.application.helper');

require_once (JPATH_COMPONENT.DS.'classes'.DS.'config.php');
require_once (JPATH_COMPONENT.DS.'classes'.DS.'helper.php');
require_once(JPATH_SITE.DS.'components'.DS.'com_virtuemart'.DS.'virtuemart_parser.php' );

/**
 * @package		Joomla
 * @subpackage	Config
 */
class VmSefManufacturersController extends JController
{
	/**
	 * Custom Constructor
	 */
	function __construct( $default = array())
	{
		parent::__construct( $default );

		$this->registerTask('apply','save');
		$this->registerTask('unpublish','publish');
	}

	function display( )	{
		JRequest::setVar( 'hidemainmenu', 0 );
		//JRequest::setVar( 'layout', 'form'  );
		JRequest::setVar( 'edit', false  );
		global $option;

		$view = JRequest::getCmd('view');
		if(empty($view)) {
			JRequest::setVar('view', 'manufacturers');
			$view='rules';
		};

		$this->getOrphans();
		$this->getConflicts();
		parent::display();
	}

	function publish() {
		$mainframe = & JFactory::getApplication('site');
		global $option;

		// Check for request forgeries
		JRequest::checkToken() or jexit( 'Invalid Token' );

		$db 	=& JFactory::getDBO();

		$cid		= JRequest::getVar( 'cid', array(), '', 'array' );
		$publish	= ( $this->getTask() == 'publish' ? 1 : 0 );

		if (count( $cid ) < 1)
		{
			$action = $publish ? 'publish' : 'unpublish';
			JError::raiseError(500, JText::_( 'Select an item to' .$action, true ) );
		}

		JArrayHelper::toInteger($cid);
		$cids = implode( ',', $cid );
		$query = 'UPDATE #__vm_sef_manufacturers SET published = '. (int) $publish. ' WHERE manufacturer_id IN ( '. $cids .' )';
		$db->setQuery( $query );
		if (!$db->query())
		{
			JError::raiseError(500, $db->getErrorMsg() );
		} else {
			$_SESSION['sef_modified'] = 1;
		}

		$mainframe->redirect( 'index.php?option='.$option.'&view=manufacturers' ,$action);
	}

	function generate() {
		$mainframe = & JFactory::getApplication('site');
		global $option;

		// Check for request forgeries
		JRequest::checkToken() or jexit( 'Invalid Token' );

		$db 	=& JFactory::getDBO();

		$cid		= JRequest::getVar( 'cid', array(), '', 'array' );
		if (count( $cid ) < 1)
		{
			$action = $publish ? 'publish' : 'unpublish';
			JError::raiseError(500, JText::_( 'Select an item to generate', true ) );
		}

		JArrayHelper::toInteger($cid);
		$cids = implode( ',', $cid );
		$query = 'DELETE FROM #__vm_sef_manufacturers WHERE manufacturer_id IN ( '. $cids .' )';
		$db->setQuery( $query );
		$db->query();

		$query = 'SELECT src.manufacturer_id as srcid, src.mf_name as name'
		. ' FROM #__{vm}_manufacturer src'
		. ' WHERE src.manufacturer_id IN ( '. $cids .' )';
		$query = str_replace("{vm}",VM_TABLEPREFIX,$query);
		$db->setQuery( $query );
		$rows = $db->loadObjectList();
		$SefHelper = new VmSefHelper();
		foreach ($rows as $row) {
			
			$alias_row =& JTable::getInstance('manufacturer', 'Table');
			$alias_row->manufacturer_id = $row->srcid;
			$alias_row->alias = $SefHelper->generateAlias($row->name);
			$alias_row->published = 1;
			$alias_row->store();
		}

		$_SESSION['sef_modified'] = 1;

		$mainframe->redirect( 'index.php?option='.$option.'&view=manufacturers' ,$action);

	}

	function remove() {
		$mainframe = & JFactory::getApplication('site');
		global $option;
		// Check for request forgeries
		JRequest::checkToken() or jexit( 'Invalid Token' );

		$db		=& JFactory::getDBO();
		$cid	= JRequest::getVar( 'cid', array(), '', 'array' );

		JArrayHelper::toInteger($cid);
		$msg = '';

		$cids = implode( ',', $cid );

		$query = 'DELETE FROM #__vm_sef_manufacturers WHERE manufacturer_id IN ( '. $cids .' )';
		$db->setQuery( $query );
		if (!$db->query())
		{
			JError::raiseError(500, $db->getErrorMsg() );
		} else {
			$_SESSION['sef_modified'] = 1;
		}
	
		$mainframe->redirect( 'index.php?option='.$option.'&view=manufacturers' , JText::_('Alias deleted'));
	}

	
	function removeorphans() {
		$mainframe = & JFactory::getApplication('site');
		global $option;
		
		$db	=& JFactory::getDBO();
		$q = 'SELECT sef.id
				FROM #__vm_sef_manufacturers sef
				LEFT JOIN #__{vm}_manufacturer src
				ON sef.manufacturer_id = src.manufacturer_id
				WHERE src.manufacturer_id IS NULL';
		$q = str_replace("{vm}",VM_TABLEPREFIX,$q);
		$db->setQuery( $q );
		$orphans = $db->loadResultArray();
		$cids = implode( ',', $orphans );
		$query = 'DELETE FROM #__vm_sef_manufacturers WHERE id IN ( '. $cids .' )';
		$db->setQuery( $query );
		if (!$db->query())
		{
			JError::raiseError(500, $db->getErrorMsg() );
		}
		$mainframe->redirect( 'index.php?option='.$option.'&view=manufacturers' , JText::_('Alias deleted'));
	}

	function getOrphans() {
		$db	=& JFactory::getDBO();
		$q = 'SELECT count(sef.id) as nb
				FROM #__vm_sef_manufacturers sef
				LEFT JOIN #__{vm}_manufacturer src
				ON sef.manufacturer_id = src.manufacturer_id
				WHERE src.manufacturer_id IS NULL';
		$q = str_replace("{vm}",VM_TABLEPREFIX,$q);
		$db->setQuery( $q );
		$orphan = $db->loadObject();
		$msg='';
		if ($orphan->nb > 0) {
			$msg = sprintf(JText::_('You have %s orphan alias'),$orphan->nb);
			$msg .= '&nbsp;<input type="button" value="'.JText::_('remove').'" onclick="submitbutton(\'removeorphans\')">';
		}
		if ($msg) {
			JError::raiseWarning( 100, $msg );
		}
	}
	
	function getConflicts() {

		$db	=& JFactory::getDBO();
		$q = 'SELECT count(id) as nb, alias 
				FROM #__vm_sef_manufacturers p 
				INNER JOIN #__{vm}_manufacturer x
				ON p.manufacturer_id = x.manufacturer_id
				WHERE published=1 
				GROUP BY alias
				HAVING count(id) > 1 ';
		$q = str_replace("{vm}",VM_TABLEPREFIX,$q);
		$db->setQuery( $q );
		$conflicts = $db->loadObjectList();
		$wrongAlias = '';
		foreach ($conflicts as $conflict) {
			$wrongAlias .= sprintf(JText::_('PHPSHOP_SEF_DUPLICATE_ALIAS'), $conflict->alias, $conflict->nb);
			$q = "SELECT src.manufacturer_id, sef.id, src.mf_name, sef.alias, sef.published
				FROM #__{vm}_manufacturer src
				INNER JOIN #__vm_sef_manufacturers sef ON sef.product_id = src.manufacturer_id
				AND alias = '".$conflict->alias."' ";
			$q = str_replace("{vm}",VM_TABLEPREFIX,$q);
			$db->setQuery( $q );
			$conflict_items = $db->loadObjectList();
			$wrongAlias .= '<div style="padding-left:40px; text-indent:0;">';
			foreach ($conflict_items as $conflict_item) {
				$link 		= JRoute::_( 'index.php?tmpl=component&option=com_vm_sef&view=alias&task=edit&type=manufacturer&id='.$conflict_item->manufacturer_id );
				$wrongAlias .= "<a class=\"modal\" rel=\"{handler: 'iframe', size: {x: 450, y: 180}}\" href=\"" . $link ."\">".$conflict_item->mf_name."</a>";
				$wrongAlias .= "<br/>";
			}
			$wrongAlias .= "</div>";
		}
		
		if ($wrongAlias) {
			JError::raiseWarning( 100, $wrongAlias );
		}
	}

}