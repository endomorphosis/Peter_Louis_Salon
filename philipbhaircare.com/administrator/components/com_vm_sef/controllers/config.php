<?php
/*------------------------------------------------------------------------
# vm_sef - Search Engine Friendly URL's for Virtuemart
# ------------------------------------------------------------------------
# author    Jeremy Magne
# copyright Copyright (C) 2010 Daycounts.com. All Rights Reserved.
# Websites: http://www.daycounts.com
# Technical Support: http://www.daycounts.com/en/contact/
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# See http://daycounts.com/fr/component/content/article/7 for details
-------------------------------------------------------------------------*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport('joomla.application.component.controller');
jimport('joomla.filesystem.file');
jimport('joomla.application.helper');

require_once (JPATH_COMPONENT.DS.'classes'.DS.'config.php');
require_once (JPATH_COMPONENT.DS.'classes'.DS.'helper.php');
require_once (JPATH_COMPONENT.DS.'classes'.DS.'dbhelper.php');

class VmSefConfigController extends JController
{
	/**
	 * Custom Constructor
	 */
	function __construct( $default = array())
	{
		parent::__construct( $default );

		$this->registerTask('apply','save');
		$this->registerTask('copytheme','copyThemeFiles');
	}

	function display( )	{
		JRequest::setVar( 'hidemainmenu', 0 );
		JRequest::setVar( 'edit', false  );
		global $option;


		JRequest::setVar('view', 'config');

		parent::display();
	}

	function save() {
		$mainframe = & JFactory::getApplication('site');
		global $option;
		// Check for request forgeries
		JRequest::checkToken() or jexit( 'Invalid Token' );
		
		$config = new VmSefConfig();
		$config->save($_POST);

		$_SESSION['sef_modified'] = 1;

		$msg = JText::_( 'Config saved' );
		$link = 'index.php?option='.$option.'&view=config';
		$mainframe->redirect($link, $msg);
	}
	
	function install() {
		JRequest::setVar( 'hidemainmenu', 1 );
		$this->createTable();
		$this->copyRouterFile();
		$this->copyThemeFiles(false);
		$this->copyjoomfish(false);
		$this->installModule();
		$this->installPlugin();
		
		$this->CleanupOldVersionFiles();
		$this->cleanCache();
		
		$link = 'index.php?option=com_vm_sef&view=config';
		echo '<br><br><a href="'.$link.'">OK</a>';
		
	}
	
	function upgrade() {

		JRequest::setVar( 'hidemainmenu', 1 );
		$this->upgradeTable();
		$this->copyRouterFile();
		$this->copyThemeFiles(false);
		$this->copyjoomfish(false);
		$this->installModule();
		$this->installPlugin();
		$this->CleanupOldVersionFiles();
		$this->cleanCache();
		
		$link = JRoute::_( 'index.php?option=com_vm_sef&view=config');
		echo '<br><br><a href="'.$link.'">OK</a>';
	}
	
	function createTable() {
		
		$this->createProductsTable();
		$this->createCategoriesTable();
		$this->createManufacturersTable();
		$this->createVendorsTable();
		$this->createUrlsTable();
		$this->createPagesTable();
		$this->insertPagesRecords();
		return true;
	}

	function createProductsTable() {
		$db	= &JFactory::getDBO();
		$msgSQL 	= $msgFile = $msgError = '';	
			
		$query =' DROP TABLE IF EXISTS '.$db->nameQuote('#__vm_sef_products').';';
		$db->setQuery( $query );
		if (!$db->query())
		{
			JError::raiseError(500, $db->getErrorMsg() );
		}
	
		$charset = ($db->hasUTF()) ? 'DEFAULT CHARSET=utf8' : '';
		$query = "CREATE TABLE `#__vm_sef_products` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `product_id` int(11) NOT NULL,
		  `alias` varchar(255) NOT NULL,
		  `published` tinyint(1) NOT NULL DEFAULT '1',
		  `metakey` text NOT NULL,
		  `metadesc` text NOT NULL,
		  PRIMARY KEY (`id`)
		) ENGINE=MyISAM ".$charset.";";
		$db->setQuery( $query );
		echo "<br>".JText::_('Creating products table')."....";
		if ($db->query()) {
			echo "<span style='color:green'>".JText::_('OK')."</span>";
		} else {
			echo "<span style='color:red'>".JText::_('ERROR')."</span>";
		}
		
		return true;
		
	}
	
	function createCategoriesTable() {
		$db	= &JFactory::getDBO();
		$msgSQL 	= $msgFile = $msgError = '';	
			
		$query =' DROP TABLE IF EXISTS '.$db->nameQuote('#__vm_sef_categories').';';
		$db->setQuery( $query );
		if (!$db->query())
		{
			JError::raiseError(500, $db->getErrorMsg() );
		}
	
		$charset = ($db->hasUTF()) ? 'DEFAULT CHARSET=utf8' : '';
		$query = "CREATE TABLE `#__vm_sef_categories` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `category_id` int(11) NOT NULL,
		  `alias` varchar(255) NOT NULL,
		  `published` tinyint(1) NOT NULL DEFAULT '1',
		  `metakey` text NOT NULL,
		  `metadesc` text NOT NULL,
		  PRIMARY KEY (`id`)
		) ENGINE=MyISAM ".$charset.";";
		$db->setQuery( $query );
		echo "<br>".JText::_('Creating categories table')."....";
		if ($db->query()) {
			echo "<span style='color:green'>".JText::_('OK')."</span>";
		} else {
			echo "<span style='color:red'>".JText::_('ERROR')."</span>";
		}
		
		return true;
		
	}
	
	function createManufacturersTable() {
		$db	= &JFactory::getDBO();
		$msgSQL 	= $msgFile = $msgError = '';	
			
		$query =' DROP TABLE IF EXISTS '.$db->nameQuote('#__vm_sef_manufacturers').';';
		$db->setQuery( $query );
		if (!$db->query())
		{
			JError::raiseError(500, $db->getErrorMsg() );
		}
	
		$charset = ($db->hasUTF()) ? 'DEFAULT CHARSET=utf8' : '';
		$query = "CREATE TABLE `#__vm_sef_manufacturers` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `manufacturer_id` int(11) NOT NULL,
		  `alias` varchar(255) NOT NULL,
		  `published` tinyint(1) NOT NULL DEFAULT '1',
		  `metakey` text NOT NULL,
		  `metadesc` text NOT NULL,
		  PRIMARY KEY (`id`)
		) ENGINE=MyISAM ".$charset.";";
		$db->setQuery( $query );
		echo "<br>".JText::_('Creating manufacturers table')."....";
		if ($db->query()) {
			echo "<span style='color:green'>".JText::_('OK')."</span>";
		} else {
			echo "<span style='color:red'>".JText::_('ERROR')."</span>";
		}
		
		return true;
		
	}
	
	function createVendorsTable() {
		$db	= &JFactory::getDBO();
		$msgSQL 	= $msgFile = $msgError = '';	
			
		$query =' DROP TABLE IF EXISTS '.$db->nameQuote('#__vm_sef_vendors').';';
		$db->setQuery( $query );
		if (!$db->query())
		{
			JError::raiseError(500, $db->getErrorMsg() );
		}
	
		$charset = ($db->hasUTF()) ? 'DEFAULT CHARSET=utf8' : '';
		$query = "CREATE TABLE `#__vm_sef_vendors` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `vendor_id` int(11) NOT NULL,
		  `alias` varchar(255) NOT NULL,
		  `published` tinyint(1) NOT NULL DEFAULT '1',
		  `metakey` text NOT NULL,
		  `metadesc` text NOT NULL,
		  PRIMARY KEY (`id`)
		) ENGINE=MyISAM ".$charset.";";
		$db->setQuery( $query );
		echo "<br>".JText::_('Creating vendors table')."....";
		if ($db->query()) {
			echo "<span style='color:green'>".JText::_('OK')."</span>";
		} else {
			echo "<span style='color:red'>".JText::_('ERROR')."</span>";
		}
		
		return true;
		
	}
	
	function createUrlsTable() {
		$db	= &JFactory::getDBO();
		$msgSQL 	= $msgFile = $msgError = '';	
			
		$query =' DROP TABLE IF EXISTS '.$db->nameQuote('#__vm_sef_urls').';';
		$db->setQuery( $query );
		if (!$db->query())
		{
			JError::raiseError(500, $db->getErrorMsg() );
		}
	
		$charset = ($db->hasUTF()) ? 'DEFAULT CHARSET=utf8' : '';
		$query = "CREATE TABLE `#__vm_sef_urls` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `rewrited` varchar(255) NOT NULL,
		  `original` varchar(255) NOT NULL,
		  `published` tinyint(1) NOT NULL DEFAULT '1',
		  `locked` tinyint(1) NOT NULL DEFAULT '0',
		  `sef` varchar(20) NOT NULL,
		  PRIMARY KEY (`id`),
		  UNIQUE KEY `original` (`original`),
		  UNIQUE KEY `rewrited` (`rewrited`)
		) ENGINE=MyISAM ".$charset.";";
		$db->setQuery( $query );
		echo "<br>".JText::_('Creating urls table')."....";
		if ($db->query()) {
			echo "<span style='color:green'>".JText::_('OK')."</span>";
		} else {
			echo "<span style='color:red'>".JText::_('ERROR')."</span>";
		}
		
		return true;
		
	}
	
	function createPagesTable() {
		$db	= &JFactory::getDBO();
		$msgSQL 	= $msgFile = $msgError = '';	
			
		$query =' DROP TABLE IF EXISTS '.$db->nameQuote('#__vm_sef_pages').';';
		$db->setQuery( $query );
		if (!$db->query())
		{
			JError::raiseError(500, $db->getErrorMsg() );
		}
	
		$charset = ($db->hasUTF()) ? 'DEFAULT CHARSET=utf8' : '';
		$query = "CREATE TABLE `#__vm_sef_pages` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `code` varchar(50) NOT NULL,
		  `alias` varchar(50) NOT NULL,
		  `published` tinyint(1) NOT NULL DEFAULT '1',
		  PRIMARY KEY (`id`)
		) ENGINE=MyISAM ".$charset.";";
		$db->setQuery( $query );
		echo "<br>".JText::_('Creating pages table')."....";
		if ($db->query()) {
			echo "<span style='color:green'>".JText::_('OK')."</span>";
		} else {
			echo "<span style='color:red'>".JText::_('ERROR')."</span>";
		}
		
		return true;
		
	}
	
	function insertPagesRecords() {
		$db	= &JFactory::getDBO();
		$fail2 = false;
		$db->setQuery( "INSERT IGNORE INTO `#__vm_sef_pages` VALUES(1, 'shop.browse', 'browse', 1)" );   if( !$db->query() ) { $fail2 = true; } 
		$db->setQuery( "INSERT IGNORE INTO `#__vm_sef_pages` VALUES(2, 'shop.product_details', 'detail', 1)" );   if( !$db->query() ) { $fail2 = true; } 
		$db->setQuery( "INSERT IGNORE INTO `#__vm_sef_pages` VALUES(3, 'account.index', 'account', 1)" );   if( !$db->query() ) { $fail2 = true; } 
		$db->setQuery( "INSERT IGNORE INTO `#__vm_sef_pages` VALUES(4, 'account.billing', 'billing', 1)" );   if( !$db->query() ) { $fail2 = true; } 
		$db->setQuery( "INSERT IGNORE INTO `#__vm_sef_pages` VALUES(5, 'account.orders', 'orders', 1)" );   if( !$db->query() ) { $fail2 = true; } 
		$db->setQuery( "INSERT IGNORE INTO `#__vm_sef_pages` VALUES(6, 'account.shipping', 'shipping', 1)" );   if( !$db->query() ) { $fail2 = true; } 
		$db->setQuery( "INSERT IGNORE INTO `#__vm_sef_pages` VALUES(7, 'account.shipto', 'shipto', 1)" );   if( !$db->query() ) { $fail2 = true; } 
		$db->setQuery( "INSERT IGNORE INTO `#__vm_sef_pages` VALUES(8, 'shop.cart', 'cart', 1)" );   if( !$db->query() ) { $fail2 = true; } 
		$db->setQuery( "INSERT IGNORE INTO `#__vm_sef_pages` VALUES(9, 'shop.ask', 'ask', 1)" );   if( !$db->query() ) { $fail2 = true; } 
		$db->setQuery( "INSERT IGNORE INTO `#__vm_sef_pages` VALUES(10, 'shop.registration', 'registration', 1)" );   if( !$db->query() ) { $fail2 = true; } 
		$db->setQuery( "INSERT IGNORE INTO `#__vm_sef_pages` VALUES(11, 'checkout.index', 'checkout', 0)" );   if( !$db->query() ) { $fail2 = true; } 
		$db->setQuery( "INSERT IGNORE INTO `#__vm_sef_pages` VALUES(12, 'checkout.confirm', 'confirm', 1)" );   if( !$db->query() ) { $fail2 = true; } 
		$db->setQuery( "INSERT IGNORE INTO `#__vm_sef_pages` VALUES(13, 'checkout.thankyou', 'thankyou', 1)" );   if( !$db->query() ) { $fail2 = true; } 
		$db->setQuery( "INSERT IGNORE INTO `#__vm_sef_pages` VALUES(14, 'account.order_details', 'order_details', 1)" );   if( !$db->query() ) { $fail2 = true; } 
		$db->setQuery( "INSERT IGNORE INTO `#__vm_sef_pages` VALUES(15, 'shop.getfile', 'getfile', 1)" );   if( !$db->query() ) { $fail2 = true; } 
		$db->setQuery( "INSERT IGNORE INTO `#__vm_sef_pages` VALUES(16, 'shop.downloads', 'downloads', 1)" );   if( !$db->query() ) { $fail2 = true; } 
		$db->setQuery( "INSERT IGNORE INTO `#__vm_sef_pages` VALUES(17, 'shop.search', 'search', 1)" );   if( !$db->query() ) { $fail2 = true; } 
		$db->setQuery( "INSERT IGNORE INTO `#__vm_sef_pages` VALUES(18, 'shop.parameter_search', 'parameter_search', 1)" );   if( !$db->query() ) { $fail2 = true; } 
		$db->setQuery( "INSERT IGNORE INTO `#__vm_sef_pages` VALUES(19, 'shop.manufacturer_page', 'manufacturers', 1)" );  if( !$db->query() ) { $fail2 = true; }  
		$db->setQuery( "INSERT IGNORE INTO `#__vm_sef_pages` VALUES(20, 'shop.infopage', 'vendor', 1)" );  if( !$db->query() ) { $fail2 = true; }  
		$db->setQuery( "INSERT IGNORE INTO `#__vm_sef_pages` VALUES(21, 'shop.pdf_output', 'pdf', 1)" );  if( !$db->query() ) { $fail2 = true; }  
		$db->setQuery( "INSERT IGNORE INTO `#__vm_sef_pages` VALUES(22, 'shop.recommend', 'recommend', 1)" );  if( !$db->query() ) { $fail2 = true; }  
		$db->setQuery( "INSERT IGNORE INTO `#__vm_sef_pages` VALUES(23, 'shop.parameter_search_form', 'parameter_search_form', 1)" );  if( !$db->query() ) { $fail2 = true; }  
		echo "<br>".JText::_('Inserting pages records')."....";
		if ($fail2) {
			echo "<span style='color:red'>".JText::_('ERROR')."</span>"; 
			$fail = true;
		} else {
			echo "<span style='color:green'>".JText::_('OK')."</span>"; 
		}
	}
	
	function upgradeTable() {
		
		$this->upgradeProductsTable();
		$this->upgradeCategoriesTable();
		$this->upgradeManufacturersTable();
		$this->upgradeVendorsTable();
		$this->upgradeUrlsTable();
		$this->upgradePagesTable();
		$this->insertPagesRecords();
		return true;
	}
	
	function findTable($tableName) {
		$db = & JFactory::getDBO();
        $query = "SHOW TABLES LIKE '".$db->getPrefix().$tableName."'";
        $db->setQuery($query);
        $tableexists = $db->loadResult();
		if ($tableexists === null) {
			return false;
		} else {
			return true;
		}
	}

	function upgradeProductsTable() {
		
		if (!$this->findTable('vm_sef_products')) {
			echo "<br>".JText::_('Table not found')."....";
			$this->createProductsTable();
			return true;
		}

		$DbHelper = new SefDbHelper();
		
		$errorMsg = '';
		$DbHelper->AddColumnIfNotExists( $errorMsg, "#__vm_sef_products", "id", "int(11) NOT NULL AUTO_INCREMENT ''", "" );
		$DbHelper->AddColumnIfNotExists( $errorMsg, "#__vm_sef_products", "product_id", "int(11) NOT NULL", "id" );
		$DbHelper->AddColumnIfNotExists( $errorMsg, "#__vm_sef_products", "alias", "varchar(255) NOT NULL", "product_id" );
		$DbHelper->AddColumnIfNotExists( $errorMsg, "#__vm_sef_products", "published", "tinyint(1) NULL DEFAULT 1", "alias" );
		$DbHelper->AddColumnIfNotExists( $errorMsg, "#__vm_sef_products", "metakey", "text NOT NULL", "published" );
		$DbHelper->AddColumnIfNotExists( $errorMsg, "#__vm_sef_products", "metadesc", "text NOT NULL", "metakey" );

		echo "<br>".JText::_('Upgrading products tables')."....<span style='color:green'>".JText::_('OK')."</span>";
		return true;
	}

	function upgradeCategoriesTable() {
		
		if (!$this->findTable('vm_sef_categories')) {
			echo "<br>".JText::_('Table not found')."....";
			$this->createCategoriesTable();
			return true;
		}

		$DbHelper = new SefDbHelper();
		
		$errorMsg = '';
		$DbHelper->AddColumnIfNotExists( $errorMsg, "#__vm_sef_categories", "id", "int(11) NOT NULL AUTO_INCREMENT ''", "" );
		$DbHelper->AddColumnIfNotExists( $errorMsg, "#__vm_sef_categories", "category_id", "int(11) NOT NULL", "id" );
		$DbHelper->AddColumnIfNotExists( $errorMsg, "#__vm_sef_categories", "alias", "varchar(255) NOT NULL", "category_id" );
		$DbHelper->AddColumnIfNotExists( $errorMsg, "#__vm_sef_categories", "published", "tinyint(1) NULL DEFAULT 1", "alias" );
		$DbHelper->AddColumnIfNotExists( $errorMsg, "#__vm_sef_categories", "metakey", "text NOT NULL", "published" );
		$DbHelper->AddColumnIfNotExists( $errorMsg, "#__vm_sef_categories", "metadesc", "text NOT NULL", "metakey" );

		echo "<br>".JText::_('Upgrading categories tables')."....<span style='color:green'>".JText::_('OK')."</span>";
		return true;
	}

	function upgradeManufacturersTable() {
		
		if (!$this->findTable('vm_sef_manufacturers')) {
			echo "<br>".JText::_('Table not found')."....";
			$this->createManufacturersTable();
			return true;
		}

		$DbHelper = new SefDbHelper();
		
		$errorMsg = '';
		$DbHelper->AddColumnIfNotExists( $errorMsg, "#__vm_sef_manufacturers", "id", "int(11) NOT NULL AUTO_INCREMENT ''", "" );
		$DbHelper->AddColumnIfNotExists( $errorMsg, "#__vm_sef_manufacturers", "manufacturer_id", "int(11) NOT NULL", "id" );
		$DbHelper->AddColumnIfNotExists( $errorMsg, "#__vm_sef_manufacturers", "alias", "varchar(255) NOT NULL", "manufacturer_id" );
		$DbHelper->AddColumnIfNotExists( $errorMsg, "#__vm_sef_manufacturers", "published", "tinyint(1) NULL DEFAULT 1", "alias" );
		$DbHelper->AddColumnIfNotExists( $errorMsg, "#__vm_sef_manufacturers", "metakey", "text NOT NULL", "published" );
		$DbHelper->AddColumnIfNotExists( $errorMsg, "#__vm_sef_manufacturers", "metadesc", "text NOT NULL", "metakey" );

		echo "<br>".JText::_('Upgrading manufacturers tables')."....<span style='color:green'>".JText::_('OK')."</span>";
		return true;
	}

	function upgradeVendorsTable() {
		
		if (!$this->findTable('vm_sef_vendors')) {
			echo "<br>".JText::_('Table not found')."....";
			$this->createVendorsTable();
			return true;
		}

		$DbHelper = new SefDbHelper();
		
		$errorMsg = '';
		$DbHelper->AddColumnIfNotExists( $errorMsg, "#__vm_sef_vendors", "id", "int(11) NOT NULL AUTO_INCREMENT ''", "" );
		$DbHelper->AddColumnIfNotExists( $errorMsg, "#__vm_sef_vendors", "vendor_id", "int(11) NOT NULL", "id" );
		$DbHelper->AddColumnIfNotExists( $errorMsg, "#__vm_sef_vendors", "alias", "varchar(255) NOT NULL", "vendor_id" );
		$DbHelper->AddColumnIfNotExists( $errorMsg, "#__vm_sef_vendors", "published", "tinyint(1) NULL DEFAULT 1", "alias" );
		$DbHelper->AddColumnIfNotExists( $errorMsg, "#__vm_sef_vendors", "metakey", "text NOT NULL", "published" );
		$DbHelper->AddColumnIfNotExists( $errorMsg, "#__vm_sef_vendors", "metadesc", "text NOT NULL", "metakey" );

		echo "<br>".JText::_('Upgrading vendors tables')."....<span style='color:green'>".JText::_('OK')."</span>";
		return true;
	}

	function upgradeUrlsTable() {
		
		if (!$this->findTable('vm_sef_urls')) {
			echo "<br>".JText::_('Table not found')."....";
			$this->createUrlsTable();
			return true;
		}

		$DbHelper = new SefDbHelper();
		
		$errorMsg = '';
		$DbHelper->AddColumnIfNotExists( $errorMsg, "#__vm_sef_urls", "id", "int(11) NOT NULL AUTO_INCREMENT ''", "" );
		$DbHelper->AddColumnIfNotExists( $errorMsg, "#__vm_sef_urls", "rewrited", "varchar(255) NOT NULL", "id" );
		$DbHelper->AddColumnIfNotExists( $errorMsg, "#__vm_sef_urls", "original", "varchar(255) NOT NULL", "rewrited" );
		$DbHelper->AddColumnIfNotExists( $errorMsg, "#__vm_sef_urls", "published", "tinyint(1) NULL DEFAULT 1", "original" );
		$DbHelper->AddColumnIfNotExists( $errorMsg, "#__vm_sef_urls", "locked", "tinyint(1) NULL DEFAULT 0", "published" );
		$DbHelper->AddColumnIfNotExists( $errorMsg, "#__vm_sef_urls", "sef", "varchar(20) NOT NULL", "locked" );

		echo "<br>".JText::_('Upgrading urls tables')."....<span style='color:green'>".JText::_('OK')."</span>";
		return true;
	}

	function upgradePagesTable() {
		
		if (!$this->findTable('vm_sef_pages')) {
			echo "<br>".JText::_('Table not found')."....";
			$this->createPagesTable();
			return true;
		}

		$DbHelper = new SefDbHelper();
		
		$errorMsg = '';
		$DbHelper->AddColumnIfNotExists( $errorMsg, "#__vm_sef_pages", "id", "int(11) NOT NULL AUTO_INCREMENT ''", "" );
		$DbHelper->AddColumnIfNotExists( $errorMsg, "#__vm_sef_pages", "code", "varchar(50) NOT NULL", "id" );
		$DbHelper->AddColumnIfNotExists( $errorMsg, "#__vm_sef_pages", "alias", "varchar(50) NOT NULL", "code" );
		$DbHelper->AddColumnIfNotExists( $errorMsg, "#__vm_sef_pages", "published", "tinyint(1) NULL DEFAULT 1", "alias" );

		echo "<br>".JText::_('Upgrading pages tables')."....<span style='color:green'>".JText::_('OK')."</span>";
		return true;
	}

	function CleanupOldVersionFiles() {

		if (JFolder::exists(JPATH_ADMINISTRATOR.DS."components".DS."com_virtuemart".DS."languages".DS."sef"))
			JFolder::delete(JPATH_ADMINISTRATOR.DS."components".DS."com_virtuemart".DS."languages".DS."sef");
		if (JFile::exists(JPATH_ADMINISTRATOR.DS."components".DS."com_virtuemart".DS."classes".DS."ps_sef.php"))
			JFile::delete(JPATH_ADMINISTRATOR.DS."components".DS."com_virtuemart".DS."classes".DS."ps_sef.php");
		if (JFile::exists(JPATH_ADMINISTRATOR.DS."components".DS."com_virtuemart".DS."html".DS."sef.category_form.php"))
			JFile::delete(JPATH_ADMINISTRATOR.DS."components".DS."com_virtuemart".DS."html".DS."sef.category_form.php");
		if (JFile::exists(JPATH_ADMINISTRATOR.DS."components".DS."com_virtuemart".DS."html".DS."sef.category_list.php"))
			JFile::delete(JPATH_ADMINISTRATOR.DS."components".DS."com_virtuemart".DS."html".DS."sef.category_list.php");
		if (JFile::exists(JPATH_ADMINISTRATOR.DS."components".DS."com_virtuemart".DS."html".DS."sef.index.php"))
			JFile::delete(JPATH_ADMINISTRATOR.DS."components".DS."com_virtuemart".DS."html".DS."sef.index.php");
		if (JFile::exists(JPATH_ADMINISTRATOR.DS."components".DS."com_virtuemart".DS."html".DS."sef.manufacturer_form.php"))
			JFile::delete(JPATH_ADMINISTRATOR.DS."components".DS."com_virtuemart".DS."html".DS."sef.manufacturer_form.php");
		if (JFile::exists(JPATH_ADMINISTRATOR.DS."components".DS."com_virtuemart".DS."html".DS."sef.manufacturer_list.php"))
			JFile::delete(JPATH_ADMINISTRATOR.DS."components".DS."com_virtuemart".DS."html".DS."sef.manufacturer_list.php");
		if (JFile::exists(JPATH_ADMINISTRATOR.DS."components".DS."com_virtuemart".DS."html".DS."sef.page_form.php"))
			JFile::delete(JPATH_ADMINISTRATOR.DS."components".DS."com_virtuemart".DS."html".DS."sef.page_form.php");
		if (JFile::exists(JPATH_ADMINISTRATOR.DS."components".DS."com_virtuemart".DS."html".DS."sef.page_list.php"))
			JFile::delete(JPATH_ADMINISTRATOR.DS."components".DS."com_virtuemart".DS."html".DS."sef.page_list.php");
		if (JFile::exists(JPATH_ADMINISTRATOR.DS."components".DS."com_virtuemart".DS."html".DS."sef.product_form.php"))
			JFile::delete(JPATH_ADMINISTRATOR.DS."components".DS."com_virtuemart".DS."html".DS."sef.product_form.php");
		if (JFile::exists(JPATH_ADMINISTRATOR.DS."components".DS."com_virtuemart".DS."html".DS."sef.product_list.php"))
			JFile::delete(JPATH_ADMINISTRATOR.DS."components".DS."com_virtuemart".DS."html".DS."sef.product_list.php");
		if (JFile::exists(JPATH_ADMINISTRATOR.DS."components".DS."com_virtuemart".DS."html".DS."sef.urls_list.php"))
			JFile::delete(JPATH_ADMINISTRATOR.DS."components".DS."com_virtuemart".DS."html".DS."sef.urls_list.php");
		if (JFile::exists(JPATH_ADMINISTRATOR.DS."components".DS."com_virtuemart".DS."html".DS."sef.vendor_form.php"))
			JFile::delete(JPATH_ADMINISTRATOR.DS."components".DS."com_virtuemart".DS."html".DS."sef.vendor_form.php");
		if (JFile::exists(JPATH_ADMINISTRATOR.DS."components".DS."com_virtuemart".DS."html".DS."sef.vendor_list.php"))
			JFile::delete(JPATH_ADMINISTRATOR.DS."components".DS."com_virtuemart".DS."html".DS."sef.vendor_list.php");
		if (JFile::exists(JPATH_ADMINISTRATOR.DS."components".DS."com_vm_sef".DS."vm_sef.cfg.xml"))
			JFile::delete(JPATH_ADMINISTRATOR.DS."components".DS."com_vm_sef".DS."vm_sef.cfg.xml");
		if (JFolder::exists(JPATH_ADMINISTRATOR.DS."components".DS."com_vm_sef".DS."adm_virtuemart"))
			JFolder::delete(JPATH_ADMINISTRATOR.DS."components".DS."com_vm_sef".DS."adm_virtuemart");
		if (JFolder::exists(JPATH_ADMINISTRATOR.DS."components".DS."com_vm_sef".DS."site_virtuemart"))
			JFolder::delete(JPATH_ADMINISTRATOR.DS."components".DS."com_vm_sef".DS."site_virtuemart");
		if (JFile::exists(JPATH_ADMINISTRATOR.DS."components".DS."com_vm_sef".DS."assets".DS."daycounts.png"))
			JFile::delete(JPATH_ADMINISTRATOR.DS."components".DS."com_vm_sef".DS."assets".DS."daycounts.png");
		if (JFile::exists(JPATH_ADMINISTRATOR.DS."components".DS."com_vm_sef".DS."assets".DS."icon-16-editadd.png"))
			JFile::delete(JPATH_ADMINISTRATOR.DS."components".DS."com_vm_sef".DS."assets".DS."icon-16-editadd.png");
		if (JFile::exists(JPATH_ADMINISTRATOR.DS."components".DS."com_vm_sef".DS."assets".DS."icon-16-gear.png"))
			JFile::delete(JPATH_ADMINISTRATOR.DS."components".DS."com_vm_sef".DS."assets".DS."icon-16-gear.png");
		if (JFile::exists(JPATH_ADMINISTRATOR.DS."components".DS."com_vm_sef".DS."assets".DS."icon-16-lock.png"))
			JFile::delete(JPATH_ADMINISTRATOR.DS."components".DS."com_vm_sef".DS."assets".DS."icon-16-lock.png");
		if (JFile::exists(JPATH_ADMINISTRATOR.DS."components".DS."com_vm_sef".DS."assets".DS."icon-16-new.png"))
			JFile::delete(JPATH_ADMINISTRATOR.DS."components".DS."com_vm_sef".DS."assets".DS."icon-16-new.png");
		if (JFile::exists(JPATH_ADMINISTRATOR.DS."components".DS."com_vm_sef".DS."assets".DS."icon-16-publish.png"))
			JFile::delete(JPATH_ADMINISTRATOR.DS."components".DS."com_vm_sef".DS."assets".DS."icon-16-publish.png");
		if (JFile::exists(JPATH_ADMINISTRATOR.DS."components".DS."com_vm_sef".DS."assets".DS."icon-16-purge.png"))
			JFile::delete(JPATH_ADMINISTRATOR.DS."components".DS."com_vm_sef".DS."assets".DS."icon-16-purge.png");
		if (JFile::exists(JPATH_ADMINISTRATOR.DS."components".DS."com_vm_sef".DS."assets".DS."icon-16-reload.png"))
			JFile::delete(JPATH_ADMINISTRATOR.DS."components".DS."com_vm_sef".DS."assets".DS."icon-16-reload.png");
		if (JFile::exists(JPATH_ADMINISTRATOR.DS."components".DS."com_vm_sef".DS."assets".DS."icon-16-remove.png"))
			JFile::delete(JPATH_ADMINISTRATOR.DS."components".DS."com_vm_sef".DS."assets".DS."icon-16-remove.png");
		if (JFile::exists(JPATH_ADMINISTRATOR.DS."components".DS."com_vm_sef".DS."assets".DS."icon-16-save.png"))
			JFile::delete(JPATH_ADMINISTRATOR.DS."components".DS."com_vm_sef".DS."assets".DS."icon-16-save.png");
		if (JFile::exists(JPATH_ADMINISTRATOR.DS."components".DS."com_vm_sef".DS."assets".DS."icon-16-unlock.png"))
			JFile::delete(JPATH_ADMINISTRATOR.DS."components".DS."com_vm_sef".DS."assets".DS."icon-16-unlock.png");
		if (JFile::exists(JPATH_ADMINISTRATOR.DS."components".DS."com_vm_sef".DS."assets".DS."icon-16-unpublish.png"))
			JFile::delete(JPATH_ADMINISTRATOR.DS."components".DS."com_vm_sef".DS."assets".DS."icon-16-unpublish.png");
		if (JFile::exists(JPATH_ADMINISTRATOR.DS."components".DS."com_vm_sef".DS."assets".DS."lightbulb.png"))
			JFile::delete(JPATH_ADMINISTRATOR.DS."components".DS."com_vm_sef".DS."assets".DS."lightbulb.png");
		if (JFile::exists(JPATH_ADMINISTRATOR.DS."components".DS."com_vm_sef".DS."assets".DS."sef_pro.png"))
			JFile::delete(JPATH_ADMINISTRATOR.DS."components".DS."com_vm_sef".DS."assets".DS."sef_pro.png");
		if (JFile::exists(JPATH_ADMINISTRATOR.DS."components".DS."com_vm_sef".DS."assets".DS."sef-16.png"))
			JFile::delete(JPATH_ADMINISTRATOR.DS."components".DS."com_vm_sef".DS."assets".DS."sef-16.png");
		if (JFile::exists(JPATH_ADMINISTRATOR.DS."components".DS."com_vm_sef".DS."assets".DS."sef-48.png"))
			JFile::delete(JPATH_ADMINISTRATOR.DS."components".DS."com_vm_sef".DS."assets".DS."sef-48.png");
		if (JFile::exists(JPATH_ADMINISTRATOR.DS."components".DS."com_vm_sef".DS."assets".DS."sef.png"))
			JFile::delete(JPATH_ADMINISTRATOR.DS."components".DS."com_vm_sef".DS."assets".DS."sef.png");
		if (JFile::exists(JPATH_ADMINISTRATOR.DS."components".DS."com_vm_sef".DS."assets".DS."wand.png"))
			JFile::delete(JPATH_ADMINISTRATOR.DS."components".DS."com_vm_sef".DS."assets".DS."wand.png");


		require_once(JPATH_SITE.DS.'components'.DS.'com_virtuemart'.DS.'virtuemart_parser.php' );
		$db	=& JFactory::getDBO();
		$q = "DELETE FROM `#__{vm}_function` WHERE function_class='ps_sef'";
		$db->setQuery( str_replace("{vm}",VM_TABLEPREFIX,$q) );
		$db->query();
	
		$q = "DELETE FROM `#__{vm}_module` WHERE module_name='sef'";
		$db->setQuery( str_replace("{vm}",VM_TABLEPREFIX,$q) );
		$db->query();

		//$q = "DELETE FROM `#__vm_sef_pages` WHERE code='shop.pdf_output'";
		//$db->setQuery($q);
		//$db->query();

		echo "<br>".JText::_('Installation files cleanup')."....<span style='color:green'>".JText::_('OK')."</span>";
	}
	
	function copyRouterFile() {
		jimport('joomla.filesystem.path');

		$mainframe = & JFactory::getApplication('site');
		global $option;

		$src = JPATH_ADMINISTRATOR.DS.'components'.DS.'com_vm_sef'.DS.'assets'.DS.'vmfiles'.DS;
		$dest = JPATH_SITE.DS.'components'.DS.'com_virtuemart'.DS;
		
		echo "<br>".JText::_('Copying router file')."....";
		if (!JPath::canChmod($dest)) {
			JPath::setPermissions($dest);
		}
		if (JFile::copy($src.'router.php', $dest.'router.php')) {
			echo "<span style='color:green'>".JText::_('OK')."</span>";
		} else {
			echo "<span style='color:red'>".JText::_('ERROR')."</span>";
		}
		
		return true;
	}
	
	function installModule() {
		jimport('joomla.installer.installer');
		jimport('joomla.installer.helper');
		jimport('joomla.filesystem.file');
		jimport('joomla.filesystem.folder');
		jimport('joomla.filesystem.archive');
		jimport('joomla.filesystem.path');

		$lang =& JFactory::getLanguage();
		$lang->load('com_installer',JPATH_ADMINISTRATOR);		

		$installer = new JInstaller;
		$jconfig =& JFactory::getConfig();
		
		/* Search */
		$module_name = 'mod_vm_sef_search.zip';
		$tmp_dest 	= $jconfig->getValue('config.tmp_path').DS.$module_name;
		$src = JPATH_ADMINISTRATOR.DS.'components'.DS.'com_vm_sef'.DS.'assets'.DS.'modules'.DS.$module_name;
		JFile::copy($src,$tmp_dest);
		$package = JInstallerHelper::unpack($tmp_dest);
		$result = $installer->install($package['dir']);
		JFile::delete($package['packagefile']);
		JFolder::delete($package['dir']);

		echo "<br>".JText::_('Installing search module')."....";
		if ($result) {
			echo "<span style='color:green'>".JText::_('OK')."</span>";
		} else {
			echo "<span style='color:red'>".JText::_('ERROR')."</span>";
		}
		
		/* Quick Icon */
		$module_name = 'mod_vm_sef_qicon.zip';
		$tmp_dest 	= $jconfig->getValue('config.tmp_path').DS.$module_name;
		$src = JPATH_ADMINISTRATOR.DS.'components'.DS.'com_vm_sef'.DS.'assets'.DS.'modules'.DS.$module_name;
		JFile::copy($src,$tmp_dest);
		$package = JInstallerHelper::unpack($tmp_dest);
		$result = $installer->install($package['dir']);
		JFile::delete($package['packagefile']);
		JFolder::delete($package['dir']);

		echo "<br>".JText::_('Installing QuickIcon module')."....";
		if ($result) {
			echo "<span style='color:green'>".JText::_('OK')."</span>";
		} else {
			echo "<span style='color:red'>".JText::_('ERROR')."</span>";
		}
		
		return true;
	}

	function activatePlugin() {
		$mainframe = & JFactory::getApplication('site');
		global $option;
		$db	=& JFactory::getDBO();
		$query = "UPDATE `#__plugins` SET `published`=1 WHERE `folder`='system' AND `element`='vm_sef'";
		$db->SetQuery($query);
		$db->query();

		$msg = JText::_( 'Plugin activated' );
		$link = 'index.php?option='.$option.'&view=config';
		$mainframe->redirect($link, $msg);
	}
	
	function installPlugin() {
		jimport('joomla.installer.installer');
		jimport('joomla.installer.helper');
		jimport('joomla.filesystem.file');
		jimport('joomla.filesystem.folder');
		jimport('joomla.filesystem.archive');
		jimport('joomla.filesystem.path');
		jimport('joomla.plugin.helper');
		
		$lang =& JFactory::getLanguage();
		$lang->load('com_installer',JPATH_ADMINISTRATOR);		


		$installer = new JInstaller;
		$jconfig =& JFactory::getConfig();
		$plugin_name = 'plg_vm_sef.zip';

		$tmp_dest 	= $jconfig->getValue('config.tmp_path').DS.$plugin_name;
		$src = JPATH_ADMINISTRATOR.DS.'components'.DS.'com_vm_sef'.DS.'assets'.DS.'plugins'.DS.$plugin_name;
		JFile::copy($src,$tmp_dest);
		$package = JInstallerHelper::unpack($tmp_dest);
		$result = $installer->install($package['dir']);
		JFile::delete($package['packagefile']);
		JFolder::delete($package['dir']);

		$db	=& JFactory::getDBO();
		$query = "UPDATE #__plugins SET published='1', ordering='-5' WHERE `folder`='system' AND `element`='vm_sef'";
		$db->setQuery($query);
		$db->query();
		
		echo "<br>".JText::_('Installing plugin')."....";
		if ($result) {
			echo "<span style='color:green'>".JText::_('OK')."</span>";
		} else {
			echo "<span style='color:red'>".JText::_('ERROR')."</span>";
		}

		return true;
	}
	
	function copyThemeFiles($redirect=true) {

		$mainframe = & JFactory::getApplication('site');
		global $option;

		require_once(JPATH_SITE.DS.'components'.DS.'com_virtuemart'.DS.'virtuemart_parser.php' );

		$src = JPATH_ADMINISTRATOR.DS.'components'.DS.'com_vm_sef'.DS.'assets'.DS.'vmfiles'.DS;
		
		//User classes
		$dest = VM_THEMEPATH.'user_class'.DS;
		if (!JFolder::exists(VM_THEMEPATH.'user_class')) {
			JFolder::create(VM_THEMEPATH.'user_class');
			JFile::copy($src.'index.html', $dest.'index.html');
		}
		if (!JPath::canChmod($dest)) {
			JPath::setPermissions($dest);
		}
		if (JFile::exists($dest.'ps_session.php')) {
			$config = new VmSefConfig();
			JFile::copy($dest.'ps_session.php', $dest.'ps_session.php.before_vm_sef'.$config->version);
		} 
		if (JFile::copy($src.'ps_session.php', $dest.'ps_session.php')) {
			$msg = JText::_( 'Theme files copied' );
			$msgType = 'message';
		} else {
			$msg = JText::_( 'Error copying files' );
			$msgType = 'Error';
		}
		
		//User pages
		$dest = VM_THEMEPATH.'user_pages'.DS;
		if (!JFolder::exists(VM_THEMEPATH.'user_pages')) {
			JFolder::create(VM_THEMEPATH.'user_pages');
			JFile::copy($src.'index.html', $dest.'index.html');
		}
		if (!JPath::canChmod($dest)) {
			JPath::setPermissions($dest);
		}
		if (JFile::exists($dest.'shop.product_details.php')) {
			$config = new VmSefConfig();
			JFile::copy($dest.'shop.product_details.php', $dest.'shop.product_details.php.before_vm_sef'.$config->version);
		} 
		if (JFile::copy($src.'shop.product_details.php', $dest.'shop.product_details.php')) {
			$msg = JText::_( 'Theme files copied' );
			$msgType = 'message';
		} else {
			$msg = JText::_( 'Error copying files' );
			$msgType = 'Error';
		}

		if ($redirect) {
			$link = 'index.php?option='.$option.'&view=config';
			$mainframe->redirect($link, $msg, $msgType);
		} else {
			echo "<br>".JText::_('Copying theme files')."....";
			if ($msgType=='message') {
				echo "<span style='color:green'>".JText::_('OK')."</span>";
			} else {
				echo "<span style='color:red'>".$msg."</span>";
			}
			return true;
		}
	}
	
	function copyjoomfish($redirect=true) {
		$joomfish = &JComponentHelper::getComponent('com_joomfish', true);
		if (!$joomfish->enabled) {
			return true;
		}
		$mainframe = & JFactory::getApplication('site');
		global $option;

		$src = JPATH_ADMINISTRATOR.DS.'components'.DS.'com_vm_sef'.DS.'assets'.DS.'joomfish'.DS;
		$dest = JPATH_ADMINISTRATOR.DS.'components'.DS.'com_joomfish'.DS.'contentelements'.DS;
		
		if (JFile::copy($src.'vm_sef_products.xml', $dest.'vm_sef_products.xml')
		&& JFile::copy($src.'vm_sef_categories.xml', $dest.'vm_sef_categories.xml')
		&& JFile::copy($src.'vm_sef_pages.xml', $dest.'vm_sef_pages.xml')) {
			$msg = JText::_( 'Joomfish files copied' );
			$msgType = 'message';
		} else {
			$msg = JText::_( 'Error copying files' );
			$msgType = 'Error';
		}

		if ($redirect) {
			$link = 'index.php?option='.$option.'&view=config';
			$mainframe->redirect($link, $msg, $msgType);
		} else {
			echo "<br>".JText::_('Copying Joomfish files')."....";
			if ($msgType=='message') {
				echo "<span style='color:green'>".JText::_('OK')."</span>";
			} else {
				echo "<span style='color:red'>".JText::_('ERROR')."</span>";
			}
			return true;
		}
	}
	
	function cleanCache() {
		jimport('joomla.cache.cache');
		$cache = & JFactory::getCache();
		$result = $cache->clean('com_vm_sef:versioncheck');
		echo "<br>".JText::_('Cleaning cache')."....";
		if ($result) {
			echo "<span style='color:green'>".JText::_('OK')."</span>";
		} else {
			echo "<span style='color:red'>".JText::_('ERROR')."</span>";
		}
		
	}
	
}