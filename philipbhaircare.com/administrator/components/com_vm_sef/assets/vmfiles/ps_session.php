<?php 
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' );
/**
*
* @version $Id: ps_session.php 2286 2010-02-01 15:28:00Z soeren_nb $
* @package VirtueMart
* @subpackage classes
* @copyright Copyright (C) 2004-2010 soeren - All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See /administrator/components/com_virtuemart/COPYRIGHT.php for copyright notices and details.
*
* http://virtuemart.net
*/


/**
 * This class handles the session initialization, restart
 * and the re-init of a session after redirection to a Shared SSL domain
 *
 */
class ps_session extends vm_ps_session {

	function getShopItemid() {

		if( empty( $_REQUEST['shopItemid'] )) {

			$com_vm_sef = &JComponentHelper::getComponent('com_vm_sef', true);
			if ($com_vm_sef->enabled && file_exists(JPATH_ADMINISTRATOR.DS."components".DS."com_vm_sef".DS.'classes'.DS.'config.php')) {
				require_once (JPATH_ADMINISTRATOR.DS."components".DS."com_vm_sef".DS.'classes'.DS.'config.php');
				$config = new VmSefConfig();
				if ($config->root_menu) {
					$_REQUEST['shopItemid'] = intval($config->root_menu);
				} else {
					$_REQUEST['shopItemid'] = parent::getShopItemid();
				}
			} else {
				$_REQUEST['shopItemid'] = parent::getShopItemid();
			}
		}

		return intval($_REQUEST['shopItemid']);

	}

	function getFullQueryString() {
		$uri = clone(JURI::getInstance());
		$app = & JFactory::getApplication('site');
		$router = $app->getRouter();
		$vars = $router->getVars();
		$query_string = $uri->buildQuery( $vars );
		return $query_string;
	}

	/**
	 * Checks if the user-agent accepts cookies
	 * @since VirtueMart 1.0.7
	 * @author soeren
	 */
	function doCookieCheck() {
		global $mm_action_url, $VM_LANG;
		
		$doCheck = vmGet( $_REQUEST, 'vmcchk', 0 );
		$isOK = vmGet( $_SESSION, 'VMCHECK' );
		
		if( $doCheck && $isOK != 'OK' ) {
			$GLOBALS['vmLogger']->info( $VM_LANG->_('VM_SESSION_COOKIES_NOT_ACCEPTED_TIP',false) );
		}
		elseif( empty( $isOK )) {
			$_SESSION['VMCHECK'] = 'OK';
			//$query_string = vmGet($_SERVER,'QUERY_STRING');
			$query_string = $this->getFullQueryString();
			if( !empty($query_string) && empty( $_POST )) {
				vmRedirect( $this->url( $_SERVER['PHP_SELF'] . '?' .$query_string .'&vmcchk=1', false, false ));
			}
		}
	}
		
	function saveSessionAndRedirect( $toSecure = true ) {
		$martID = $this->getMartId();
				
		$sessionFile = IMAGEPATH. md5( $martID ).'.sess';
		$session_contents = session_encode();
		
		if( file_exists( ADMINPATH.'install.copy.php')) {
			require_once( ADMINPATH.'install.copy.php');
		}
		
		file_put_contents( $sessionFile, $session_contents );
		
		//$url = $toSecure ? SECUREURL : URL;
		
		$query_string = $this->getFullQueryString();
		$url = $this->url(  "index.php?".$query_string.'&martID='.$martID.'&redirected=1', false, false, false ) ;
		$server = vmIsHttpsMode() ? SECUREURL : URL;

		$serveruri = JURI::getInstance($server);
		$uri = JURI::getInstance($url);
		$uri->setScheme($serveruri->getScheme());
		$uri->setHost($serveruri->getHost());
		$uri->setPort($serveruri->getPort());
		$uri->setUser($serveruri->getUser());
		$uri->setPass($serveruri->getPass());
		vmRedirect($uri->toString() );
	}


	function prepare_SSL_Session() {
		global $mainframe, $my, $database, $mosConfig_secret, $page, $VM_MODULES_FORCE_HTTPS;
		if( vmIsAdminMode() && vmIsJoomla('1.0')) {
			return;
		}
		$ssl_redirect = vmGet( $_GET, "ssl_redirect", 0 );
		$redirected = vmGet( $_GET, "redirected", 0 );
		$martID = vmGet( $_GET, 'martID', '' );
		$ssl_domain = "";
		
		if (!empty( $VM_MODULES_FORCE_HTTPS )) {
			$pagearr = explode( '.', $page );
			$module = $pagearr[0];
			// When NOT in https mode, but the called page is part of a shop module that is
			// forced to use https, we prepare the redirection to https here
			if( array_search( $module, $VM_MODULES_FORCE_HTTPS ) !== false 
				&& !vmIsHttpsMode()
				&& $this->check_Shared_SSL( $ssl_domain ) 
				) {
					
				$ssl_redirect = 1;
			}
		}
		// Generally redirect to HTTP (from HTTPS) when it is not necessary? (speed up the pageload)
		if( VM_GENERALLY_PREVENT_HTTPS == '1' 
			&& vmIsHttpsMode() && $redirected != 1
			&& $ssl_redirect == 0 && !vmIsAdminMode()
			&& URL != SECUREURL
			&& @$_REQUEST['option']=='com_virtuemart') {
				
			$pagearr = explode( '.', $page );
			$module = $pagearr[0];
			
			// When it is not necessary to stay in https mode, we leave it here
			if( array_search( $module, $VM_MODULES_FORCE_HTTPS ) === false ) {
				if( $this->check_Shared_SSL($ssl_domain)) {
					$this->saveSessionAndRedirect( false );
				}
				//$query_string = vmGet($_SERVER,'QUERY_STRING');
				$query_string = $this->getFullQueryString();
				if( !empty($query_string) && empty( $_POST )) {
					$url = $this->url(  "index.php?".$query_string.'&redirected=1', false, false, false ) ;

					$serveruri = JURI::getInstance(URL);
					$uri = JURI::getInstance($url);
					$uri->setScheme($serveruri->getScheme());
					$uri->setHost($serveruri->getHost());
					$uri->setPort($serveruri->getPort());
					$uri->setUser($serveruri->getUser());
					$uri->setPass($serveruri->getPass());
					vmRedirect($uri->toString() );
				}
			}
		}
		
		/**
        * This is the first part of the Function:
        * We check if the function must be called at all
        * Usually this is only called once: Before we go to the checkout.
        * The variable ssl_redirect=1 is appended to the URL, just for this function knows
        * is must be active! This has nothing to do with SSL / Shared SSL or whatever
        */
		if( $ssl_redirect == 1 ) {
			
			$query_string = $this->getFullQueryString();
			$_SERVER['QUERY_STRING'] = str_replace('&ssl_redirect=1', '', $query_string );
			// check_Shared_SSL compares the normal http domain name
			// and the https Domain Name. If both do not match, we move on
			// else we leave this function.
			if( $this->check_Shared_SSL( $ssl_domain ) && !vmIsHttpsMode() && $redirected == 0) {
				
				$this->saveSessionAndRedirect( true );
			}
			// do nothing but redirect
			elseif( !vmIsHttpsMode() && $redirected == 0 ) {
				
				$query_string = $this->getFullQueryString();
				$url = $this->url(  "index.php?".$query_string.'&redirected=1', false, false, false ) ;

				$serveruri = JURI::getInstance(SECUREURL);
				$uri = JURI::getInstance($url);
				$uri->setScheme($serveruri->getScheme());
				$uri->setHost($serveruri->getHost());
				$uri->setPort($serveruri->getPort());
				$uri->setUser($serveruri->getUser());
				$uri->setPass($serveruri->getPass());
				vmRedirect($uri->toString() );
			}
		}
		/**
        * This is part two of the function
        * If the redirect (see 4/5 lines above) was successful
        * and the Store uses Shared SSL, we have the variable martID
        * So let's copy the Session contents ton the new domain and start the session again
        * othwerwise: do nothing.
        */
		if( !empty( $martID ) ) {
			
			if( $this->check_Shared_SSL( $ssl_domain ) ) {
	
				// We now need to copy the Session Data to the SSL Domain
				if( $martID ) {					
					
					require_once( ADMINPATH.'install.copy.php');
					
					$sessionFile = IMAGEPATH. md5( $martID ).'.sess';
					
					// Read the contents of the session file
					$session_data = file_get_contents( $sessionFile );
					
					// Delete it for security and disk space reasons
					unlink( $sessionFile );
					
					// Read the session data into $_SESSION
					// From now on, we can use all the data in $_SESSION
					session_decode( $session_data );
					
					$check = base64_decode( $martID );
					$checkValArr = explode( "|", $check );
					
					if( defined('_JEXEC') ) {
						//TODO
					}
					elseif( class_exists('mambocore')) {
						//TODO
					}
					elseif( $GLOBALS['_VERSION']->RELEASE == '1.0' && (int)$GLOBALS['_VERSION']->DEV_LEVEL >= 13) {
						if( !empty( $GLOBALS['real_mosConfig_live_site'] ) && empty( $_REQUEST['real_mosConfig_live_site'])) {
							$GLOBALS['mosConfig_live_site'] = $GLOBALS['real_mosConfig_live_site'];
						}
						if( !empty( $checkValArr[2] )) {
							// Joomla! >= 1.0.13 can be cheated to log in a user who has previsously logged in and checked the "Remember me" box
							setcookie( mosmainframe::remCookieName_User(), $checkValArr[2], false, '/' );
							// there's no need to call "$mainframe->login"
						}
					} else {
						// Check if the user was logged in in the http domain
						// and is not yet logged in at the Shared SSL domain
						if( isset( $checkValArr[1] ) && !$my->id ) {
							// user should expect to be logged in,
							// we can use the values from $_SESSION['auth'] now
							$username = $database->getEscaped( trim( $_SESSION['auth']['user_name'] ) );
							if( !empty( $username )) {
								$database->setQuery('SELECT username, password FROM `#__users` WHERE `username` = \''.$username.'\';');
								$database->loadObject( $user );
								if( is_object( $user )) {
									// a last security check using the transmitted md5 hash and the rebuilt hash
									$check = md5( $user->username . $user->password . $mosConfig_secret );
									if( $check === $checkValArr[1] ) {
										// Log the user in with his username
										$mainframe->login( $user->username, $user->password );
									}
								}
								
							}
						}
					}
									
					
					session_write_close();
					
					// Prevent the martID from being displayed in the URL
					if( !empty( $_GET['martID'] )) {
						$query_string = $this->getFullQueryString();
						$query_string = substr_replace( $query_string, '', strpos( $query_string, '&martID'));
						$url = $this->url(  "index.php?".$query_string.'&cartReset=N&redirected=1', false, false, false ) ;
						$server = vmIsHttpsMode() ? SECUREURL : URL;

						$serveruri = JURI::getInstance($server);
						$uri = JURI::getInstance($url);
						$uri->setScheme($serveruri->getScheme());
						$uri->setHost($serveruri->getHost());
						$uri->setPort($serveruri->getPort());
						$uri->setUser($serveruri->getUser());
						$uri->setPass($serveruri->getPass());
						vmRedirect($uri->toString() );
		
						//vmRedirect( $this->url( $url . "index.php?$query_string&cartReset=N&redirected=1", true, false, true) );
					}
	
				}
	
			}
		}
	}

} // end of class session


?>
