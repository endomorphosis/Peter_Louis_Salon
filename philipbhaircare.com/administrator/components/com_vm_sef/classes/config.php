<?php
/*------------------------------------------------------------------------
# vm_sef - Search Engine Friendly URL's for Virtuemart
# ------------------------------------------------------------------------
# author    Jeremy Magne
# copyright Copyright (C) 2010 Daycounts.com. All Rights Reserved.
# Websites: http://www.daycounts.com
# Technical Support: http://www.daycounts.com/en/contact/
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# See http://daycounts.com/ for details
-------------------------------------------------------------------------*/

defined( '_JEXEC' ) or die( 'Restricted access - vm_sef free v1.5.0' );

jimport('joomla.application.helper');
jimport('joomla.filesystem.file');
jimport('joomla.utilities.simplexml');

class VmSefConfig {
	var $version_cat 	= 1;
	var $is_active 		= 1;
	var $root_menu		= -1;
	var $rewrite_mode 	= 'alias';
	var $category_mode 	= 'full';
	var $vm_prefix		= 'vm';
	var $component_name	= 'com_vm_sef';
	var $flypage		= 'flypage.tpl';
	var $exlcude_post_keys = array('option','task','view');
	var $disable_admin_rewrite = 0;
	var $params = array('category_id'=>'c','product_id'=>'p','flypage'=>'f','page'=>'h','vendor_id'=>'v','manufacturer_id'=>'m','order_id'=>'o');
	var $vmparams = array('category_id','product_id','flypage','page','vendor_id','manufacturer_id','order_id');
	var $transliterate_search = 'α,β,Γ,γ,Δ,δ,ε,ζ,η,Θ,θ,ι,κ,Λ,λ,μ,νΞ,ξ,ο,Ο,Π,π,ρ,Σ,σς,τ,υ,Φ,φ,χ,Ψ,ψ,Ω,ω';
	var $transliterate_replace = 'a,v,G,g,TH,th,e,z,ee,TH,th,y,k,L,l,m,n,KS,ks,o,O,P,p,r,S,s,t,y,F,f,kh,PS,ps,O,o';
	
	function __construct() {
		$this->load();
	}
	
	function load() {
		if (file_exists(JPATH_ADMINISTRATOR.DS."components".DS.$this->component_name.DS.$this->component_name.".cfg.xml")) {
			$xml = new JSimpleXML;
			$xml->loadFile(JPATH_ADMINISTRATOR.DS."components".DS.$this->component_name.DS.$this->component_name.".cfg.xml");
			foreach( $xml->document->children() as $child ) {
				$key = $child->name();
				$val = $child->data();
				$this->{$key} = $val;
			}
		}
		//Load the version number from the install XML file.
		$comp = JApplicationHelper::parseXMLInstallFile(JPATH_ADMINISTRATOR.DS.'components'.DS.$this->component_name.DS.$this->component_name.'.xml');
		$this->version = $comp['version'];
	}
	
	function save($post) {
		$xml = "<".$this->component_name.">\n";
		$xml .= "\t<version>".$this->version."</version>\n";
		foreach ($post as $key=>$val) {
			if (in_array($key,$this->exlcude_post_keys) || $key == JUtility::getToken()) continue;
			$xml .= "\t<".$key.">".$val."</".$key.">\n";
			$this->{$key} = $val;
		}
		$xml .= "</".$this->component_name.">";
		JFile::write(JPATH_ADMINISTRATOR.DS."components".DS.$this->component_name.DS.$this->component_name.".cfg.xml",$xml);
	}

}