<?php
/*------------------------------------------------------------------------
# vm_sef - Search Engine Friendly URL's for Virtuemart
# ------------------------------------------------------------------------
# author    Jeremy Magne
# copyright Copyright (C) 2010 Daycounts.com. All Rights Reserved.
# Websites: http://www.daycounts.com
# Technical Support: http://www.daycounts.com/en/contact/
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# See http://daycounts.com/fr/component/content/article/7 for details
-------------------------------------------------------------------------*/

defined( '_JEXEC' ) or die( 'Restricted access' );

jimport('joomla.application.component.view');
//jimport('joomla.filesystem.file');

class VmSefAliasViewAlias extends JView
{
	function display($tpl = null)
	{
		$config = new VmSefConfig();
		$helper = new VmSefHelper();
		$this->assignRef('config',	$config);
		$this->assignRef('helper',	$helper);

		global $option;
		$mainframe = & JFactory::getApplication('site');
		
		$id = JRequest::getVar( 'id', -1 );
		$type = JRequest::getVar( 'type', -1 );
		$db	=& JFactory::getDBO();
		switch ($type) {
			case 'product':
				$query = 'SELECT src.product_id as srcid, src.product_name as name, sef.*'
				. ' FROM #__{vm}_product src'
				. '	LEFT JOIN #__vm_sef_products sef ON sef.product_id = src.product_id'
				. ' WHERE src.product_id = '. $id;
				$opener	= JRoute::_( 'index.php?option=com_vm_sef&view=products' );
				break;
			case 'category':
				$query = 'SELECT src.category_id as srcid, src.category_name as name, sef.*'
				. ' FROM #__{vm}_category src'
				. '	LEFT JOIN #__vm_sef_categories sef ON sef.category_id = src.category_id'
				. ' WHERE src.category_id = '. $id;
				$opener	= JRoute::_( 'index.php?option=com_vm_sef&view=categories' );
				break;
			case 'manufacturer':
				$query = 'SELECT src.manufacturer_id as srcid, src.mf_name as name, sef.*'
				. ' FROM #__{vm}_manufacturer src'
				. '	LEFT JOIN #__vm_sef_manufacturers sef ON sef.manufacturer_id = src.manufacturer_id'
				. ' WHERE src.manufacturer_id = '. $id;
				$opener	= JRoute::_( 'index.php?option=com_vm_sef&view=manufacturers' );
				break;
			case 'vendor':
				$query = 'SELECT src.vendor_id as srcid, src.vendor_name as name, sef.*'
				. ' FROM #__{vm}_vendor src'
				. '	LEFT JOIN #__vm_sef_vendors sef ON sef.vendor_id = src.vendor_id'
				. ' WHERE src.vendor_id = '. $id;
				$opener	= JRoute::_( 'index.php?option=com_vm_sef&view=vendors' );
				break;
			case 'page':
				$query = 'SELECT sef.code as srcid, sef.code as name, sef.*'
				. ' FROM #__vm_sef_pages sef'
				. ' WHERE sef.id = '. $id;
				$opener	= JRoute::_( 'index.php?option=com_vm_sef&view=pages' );
				break;
		}
		
		if ($query) {
			$query = str_replace("{vm}",VM_TABLEPREFIX,$query);
			$db->setQuery( $query );
			$row = $db->loadObject();
			$this->assignRef('row',	$row);
		}
		$this->assignRef('type', $type);
		$this->assignRef('opener', $opener);
		
		

		$document = & JFactory::getDocument();
		$document->setTitle( JText::_('Edit Alias') );

		parent::display($tpl);

	}
}