<?php 
/*------------------------------------------------------------------------
# vm_sef - Search Engine Friendly URL's for Virtuemart
# ------------------------------------------------------------------------
# author    Jeremy Magne
# copyright Copyright (C) 2010 Daycounts.com. All Rights Reserved.
# Websites: http://www.daycounts.com
# Technical Support: http://www.daycounts.com/en/contact/
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# See http://daycounts.com/fr/component/content/article/7 for details
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access'); 


if ($this->row) {
?>
<div class="clr"></div>

<form action="index.php" method="post" name="adminForm">
    <input type="hidden" name="task" value="save" />
	<input type="hidden" name="tmpl" value="component" />
    <input type="hidden" name="view" value="alias" />
    <input type="hidden" name="type" value="<?php echo $this->type; ?>" />
    <input type="hidden" name="option" value="com_vm_sef" />
    <input type="hidden" name="id" value="<?php echo $this->row->id; ?>" />
    <input type="hidden" name="srcid" value="<?php echo $this->row->srcid; ?>" />
    <?php echo JHTML::_( 'form.token' ); ?>
    <div>
		<fieldset>
			<div style="float: right">
				<button type="button" onclick="submitbutton('save');window.top.setTimeout('parent.location=\'<?php echo $this->opener; ?>\'', 2000);">
					<?php echo JText::_( 'Save' );?></button>
				<button type="button" onclick="window.parent.document.getElementById('sbox-window').close();">
					<?php echo JText::_( 'Cancel' );?></button>
                <?php if ($this->row->id && $this->type != 'page') { ?>
				<button type="button" onclick="submitbutton('remove');window.top.setTimeout('parent.location=\'<?php echo $this->opener; ?>\'', 2000);">
					<?php echo JText::_( 'Delete' );?></button>
                <?php } ?>    
			</div>
			<div class="configuration" >
				<?php echo ($this->row->id) ? JText::_('Edit alias') : JText::_('Create alias') ?>
			</div>
		</fieldset>
        <br />
        <fieldset>
          <table class="admintable" width="95%">
                <tr> 
                    <td class="key"><label><?php echo JText::_('Name') ?></label></td>
                    <td><?php echo $this->row->name ?><input type="hidden" name="original" id="original" value="<?php echo $this->row->name ?>" /></td>
                </tr>
                <tr> 
                    <td class="key"><label for="alias"><?php echo JText::_('Alias') ?></label></td>
                    <td>
                        <input type="text" class="inputbox" name="alias" id="alias" size="50" value="<?php echo $this->row->alias; ?>" />
                        <?php if ($this->type != 'page') { ?>
                        <button type="button" onclick="generate()"><?php echo JText::_( 'Generate' );?></button>
                        <?php } ?>
                    </td>
                </tr>
                <tr> 
                    <td class="key"><label for="published"><?php echo JText::_('Published') ?></label></td>
                    <td>
                        <?php echo JHTML::_( 'select.booleanlist',  'published', 'class="inputbox"', ($this->row->id) ? $this->row->published : 1 ); ?>
                    </td>
                </tr>
                <?php if (in_array($this->type,array('product','category','vendor','manufacturer'))) { ?>
                <tr> 
                    <td class="key" valign="top"><label for="metakey"><?php echo JText::_('Keywords') ?></label></td>
                    <td>
                    	<textarea rows="8" cols="40" name="metakey" id="metakey" style="width:100%;"><?php echo $this->escape($this->row->metakey); ?></textarea>
                    </td>
                </tr>
                <tr> 
                    <td class="key" valign="top"><label for="metadesc"><?php echo JText::_('Description') ?></label></td>
                    <td>
                    	<textarea rows="8" cols="40" name="metadesc" id="metadesc" style="width:100%;"><?php echo $this->escape($this->row->metadesc); ?></textarea>
                    </td>
                </tr>
                <?php } ?>
            </table>
        </fieldset>
    </div>
</form>
<?php } ?>
<div class="clr"></div>
