<?php defined('_JEXEC') or die('Restricted access'); ?>



<div class="col width-50">
<form action="index.php" method="post" name="adminForm">
	<fieldset class="adminform">
	<legend><?php echo JText::_( 'Config' ); ?></legend>
	<table class="admintable">
		<tr>
			<td width="120" class="key">
				<?php echo JText::_( 'Activate Virtuemart rewrite?' ); ?>:
			</td>
			<td>
				<?php echo JHTML::_( 'select.booleanlist',  'is_active', 'class="inputbox"', $this->config->is_active ); ?>
			</td>
		</tr>
		<tr>
			<td class="key">
				<label for="root_menu">
					<?php echo JText::_( 'Virtuemart root menu' ); ?>:
				</label>
			</td>
			<td>
				<?php echo JHTML::_('select.genericlist',   $this->vm_menus, 'root_menu', 'class="inputbox" size="1"', 'value', 'text', $this->config->root_menu );?>
			</td>
		</tr>
		<tr>
			<td class="key">
				<label for="alias">
					<?php echo JText::_( 'Rewrite type' ); ?>:
				</label>
			</td>
			<td>
				<?php echo JHTML::_('select.genericlist',   $this->rewrite_modes, 'rewrite_mode', 'class="inputbox" size="1"', 'value', 'text', $this->config->rewrite_mode );?>
			</td>
		</tr>
		<tr>
			<td class="key">
				<label for="flypage">
					<?php echo JText::_( 'Flypage to hide' ); ?>:
				</label>
			</td>
			<td>
				<?php echo JHTML::_('select.genericlist',   $this->flypages, 'flypage', 'class="inputbox" size="1"', 'value', 'text', $this->config->flypage );?>
			</td>
		</tr>
		<tr>
			<td class="key">
				<label for="disable_admin_rewrite">
					<?php echo JText::_( 'Disable rewrite for strore administrators' ); ?>:
				</label>
			</td>
			<td>
				<?php echo JHTML::_( 'select.booleanlist',  'disable_admin_rewrite', 'class="inputbox"', $this->config->disable_admin_rewrite ); ?>
			</td>
		</tr>
		<tr>
			<td class="key" valign="top">
				<label for="transliterate_search" class="hasTip" title="<?php echo JText::_('TRANSLITERATE_SEARCH_EXPLAIN') ?>">
					<?php echo JText::_( 'Transliterate search' ); ?>:
				</label>
			</td>
			<td>
            	<textarea name="transliterate_search" cols="60" rows="3"><?php echo $this->config->transliterate_search; ?></textarea>
			</td>
		</tr>
		<tr>
			<td class="key" valign="top">
				<label for="transliterate_replace" class="hasTip" title="<?php echo JText::_('TRANSLITERATE_REPLACE_EXPLAIN') ?>">
					<?php echo JText::_( 'Transliterate replace' ); ?>:
				</label>
			</td>
			<td>
            	<textarea name="transliterate_replace" cols="60" rows="3"><?php echo $this->config->transliterate_replace; ?></textarea>
			</td>
		</tr>
	</table>
	</fieldset>
	<input type="hidden" name="task" value="save" />
	<input type="hidden" name="view" value="config" />
	<input type="hidden" name="option" value="com_vm_sef" />
	<?php echo JHTML::_( 'form.token' ); ?>
</form>
	<fieldset class="adminform">
	<legend><?php echo JText::_( 'Info' ); ?></legend>
    <table class="admintable">
        <tr>
        	<td class="key">Joomla SEF</td>
            <td><?php echo ($this->sef) ? '<font color="green">'.JTEXT::_('YES').'</font>' : '<font color="red">'.JTEXT::_('NO').'</font>' ?></td>
        </tr>
        <tr>
            <td class="key"><label class="hasTip" title="<?php echo JText::_('PLUGIN_EXPLAIN') ?>"><?php echo JText::_( 'System - VM Sef Plugin' ); ?></label></td>
            <td><?php echo $this->plugin_check; ?></td>
        </tr>
    	<tr>
        	<td class="key">Virtuemart</td>
            <td><?php echo ($this->virtuemart) ? '<font color="green">'.JTEXT::_('Ok').'</font>' : '<font color="red">'.JTEXT::_('ERROR').'</font>' ?></td>
        </tr>
    	<tr>
        	<td class="key"><?php echo JText::_('Virtuemart Version'); ?></td>
            <td><?php echo $this->virtuemart_version; ?></td>
        </tr>
        <tr>
            <td class="key"><label class="hasTip" title="<?php echo JText::_('EXTENDED_CLASSES_EXPLAIN') ?>"><?php echo JText::_( 'Extended classes enabled' ); ?></label></td>
            <td><?php echo ($this->virtuemart_user_class) ? '<font color="green">'.JTEXT::_('Ok').'</font>' : '<font color="red">'.JTEXT::_('ERROR').'</font> '.JText::_('EXTENDED_CLASSES_EXPLAIN') ?></td>
        </tr>
        <tr>
            <td class="key"><?php echo JText::_( 'Theme' ); ?></td>
            <td><?php echo $this->virtuemart_theme; ?></td>
        </tr>
        <tr>
            <td class="key"><?php echo JText::_( 'Theme files' ); ?></td>
            <td><?php echo $this->virtuemart_theme_check; ?></td>
        </tr>
        <!--tr>
        	<td class="key"><label class="hasTip" title="<?php echo JText::_('COOKIE_CHECK_EXPLAIN') ?>"><?php echo JText::_('Cookie check'); ?></label></td>
            <td><?php echo ($this->virtuemart_cookie_check) ? '<font color="green">'.JTEXT::_('OK').'</font>' : '<font color="red">'.JTEXT::_('ERROR').'</font>' ?></td>
        </tr-->
        <tr>
        	<td class="key"><?php echo JText::_('Version'); ?></td>
            <td><?php echo $this->config->version; ?></td>
        </tr>
        <?php if ($this->joomfish) { ?>
        <tr>
            <td class="key"><?php echo JText::_( 'Joomfish content elements' ); ?></td>
            <td><?php echo $this->joomfish_check; ?></td>
        </tr>
        <?php } ?>
    </table>
	</fieldset>
</div>
<div class="col width-50">

    <fieldset class="adminform">
        <legend><?php echo JText::_( 'Version Info' ); ?></legend>
        <br/><a href="http://www.daycounts.com/" target="_blank" title="DayCounts.com"><img src="components/com_vm_sef/assets/images/daycounts.png"  alt="DayCounts.com" border="0" height="40" /></a>
        <br/><br/>
        <iframe frameborder="0" width="100%" height="350" src="http://daycounts.com/en/component/versions/?catid=<?php echo $this->config->version_cat; ?>&tmpl=component&myVersion=<?php echo $this->config->version; ?>"></iframe>

    
        <br /><br />
        <form method="post" action="https://www.paypal.com/cgi-bin/webscr"> 
            <input value="_s-xclick" name="cmd" type="hidden" /> 
            <input value="XJY7APQB5UE2U" name="hosted_button_id" type="hidden" /> 
            <input alt="PayPal - The safer, easier way to pay online!" name="submit" border="0" src="https://www.paypalobjects.com/WEBSCR-640-20110306-1/en_US/i/btn/btn_donate_LG.gif" type="image" style="border:none;" /> 
            <img height="1" width="1" src="https://www.paypalobjects.com/WEBSCR-640-20110306-1/fr_CA/i/scr/pixel.gif" border="0" /> 
        </form>
    </fieldset>
</div>
<div class="clr"></div>

