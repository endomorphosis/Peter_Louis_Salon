<?php 
/*------------------------------------------------------------------------
# vm_sef - Search Engine Friendly URL's for Virtuemart
# ------------------------------------------------------------------------
# author    Jeremy Magne
# copyright Copyright (C) 2010 Daycounts.com. All Rights Reserved.
# Websites: http://www.daycounts.com
# Technical Support: http://www.daycounts.com/en/contact/
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access'); 

?>
<div class="clr"></div>

<form action="index.php" method="post" name="adminForm">
<table>
	<tr>
		<td align="left" width="100%">
			<?php echo JText::_( 'Filter' ); ?>:
			<input type="text" name="search" id="search" value="<?php echo htmlspecialchars($this->lists['search']);?>" class="text_area" onchange="document.adminForm.submit();" />
			<button onclick="this.form.submit();"><?php echo JText::_( 'Go' ); ?></button>
			<button onclick="document.getElementById('search').value='';this.form.getElementById('filter_state').value='';this.form.submit();"><?php echo JText::_( 'Reset' ); ?></button>
		</td>
		<td nowrap="nowrap">
            <?php echo $this->lists['state']; ?>
		</td>
	</tr>
</table>
<div id="tablecell">
	<table class="adminlist">
	<thead>
		<tr>
			<th width="5">
				<?php echo JText::_( 'NUM' ); ?>
			</th>
			<th width="20">
				<input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count( $this->items ); ?>);" />
			</th>
			<th  class="title">
				<?php echo JHTML::_('grid.sort',   'SEF', 'sef.rewrited', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
			</th>
            <th  class="title">
            	<?php echo JText::_('Params'); ?>
            </th>
			<!--th width="1%"  class="title">
				<?php echo JHTML::_('grid.sort',   'Locked', 'sef.locked', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
			</th-->
			<!--th width="1%"  class="title">
				<?php echo JHTML::_('grid.sort',   'Published', 'sef.published', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
			</th-->
			<th width="1%" nowrap="nowrap">
				<?php echo JHTML::_('grid.sort',   'ID', 'sef.id', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
			</th>
		</tr>
	</thead>
	<tfoot>
		<tr>
			<td colspan="10">
				<?php echo $this->pagination->getListFooter(); ?>
			</td>
		</tr>
	</tfoot>
	<tbody>
	<?php
	$k = 0;
	for ($i=0, $n=count( $this->items ); $i < $n; $i++)
	{
		$row = &$this->items[$i];
		$lockaction 	= ($row->locked) ? 'unlock' : 'lock';
		$lockbutton		= ($row->locked) ? '' : '';

		$params = unserialize($row->original);
		$strParam = "";
		if ($params) {
			foreach($params as $key=>$val) {
				$strParam .= $key . " = <b>" . $val . "</b><br/>";
			}
		}
	?>
		<tr class="<?php echo "row$k"; ?>">
			<td>
				<?php echo $this->pagination->getRowOffset( $i ); ?>
			</td>
			<td>
				<?php echo JHTML::_('grid.checkedout',   $row, $i, 'id' ); ?>
			</td>
			<td>
				<?php echo htmlspecialchars($row->rewrited); ?>
			</td>
            <td align="center">
            	<span class="hasTip" title="<?php echo $strParam; ?>">
                	<img src="components/com_vm_sef/assets/images/gear.png" />
                </span>
            </td>
			<!--td align="center">
            	<a title="" onclick="return listItemTask('cb<?php echo $i ?>','<?php echo $lockaction ?>')" href="javascript:void(0);">
                	<img src="images/<?php echo ($row->locked ? 'checked_out.png' : 'tick.png');?>"/>
                </a>
			</td-->
			<!--td align="center">
            	<?php echo ($row->id) ? JHTML::_('grid.published', $row, $i ) : '-'; ?>
			</td-->
			<td align="center">
				<?php echo $row->id; ?>
			</td>
		</tr>
		<?php
			$k = 1 - $k;
		}
		?>
	</tbody>
	</table>
</div>

	<input type="hidden" name="option" value="com_vm_sef" />
	<input type="hidden" name="view" value="cache" />
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="boxchecked" value="0" />
	<input type="hidden" name="filter_order" value="<?php echo $this->lists['order']; ?>" />
	<input type="hidden" name="filter_order_Dir" value="<?php echo $this->lists['order_Dir']; ?>" />
	<?php echo JHTML::_( 'form.token' ); ?>
</form>
<div class="clr"></div>
