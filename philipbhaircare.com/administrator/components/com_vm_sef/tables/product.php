<?php
/*------------------------------------------------------------------------
# vm_sef - Search Engine Friendly URL's for Virtuemart
# ------------------------------------------------------------------------
# author    Jeremy Magne
# copyright Copyright (C) 2010 Daycounts.com. All Rights Reserved.
# Websites: http://www.daycounts.com
# Technical Support: http://www.daycounts.com/en/contact/
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
-------------------------------------------------------------------------*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

class TableProduct extends JTable
{
	var $id					= 0;
	var $product_id			= 0;
	var $alias				= '';
	var $published			= 0;
	var $metakey			= '';
	var $metadesc			= '';

	/**
	* @param database A database connector object
	*/
	function __construct( &$db )
	{
		parent::__construct( '#__vm_sef_products', 'id', $db );
	}

	function bind( $array, $ignore='' )
	{
		$result = parent::bind( $array );
		// cast properties
		$this->id	= (int) $this->id;

		return $result;
	}

	function check()
	{
		// check for valid name
		if (trim( $this->alias ) == '')
		{
			$this->setError(JText::_( 'You must define an alias.' ));
			return false;
		}

		return true;
	}

	
}
?>