<?php
/*------------------------------------------------------------------------
# vm_sef - Search Engine Friendly URL's for Virtuemart
# ------------------------------------------------------------------------
# author    Jeremy Magne
# copyright Copyright (C) 2010 Daycounts.com. All Rights Reserved.
# Websites: http://www.daycounts.com
# Technical Support: http://www.daycounts.com/en/contact/
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# See http://daycounts.com/fr/component/content/article/7 for details
-------------------------------------------------------------------------*/

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport( 'joomla.application.component.controller' );
jimport('joomla.application.helper');
jimport('joomla.filesystem.file');

$lang =& JFactory::getLanguage();
$lang->load('com_vm_sef',JPATH_ADMINISTRATOR);		


?>
	<h1><img src="components/com_vm_sef/assets/images/sef-48.png" />Search Engine Friendly URL's for Virtuemart installer</h1>
	<br /><br />
<?php

//Check if Virtuemart is installed
if (!file_exists(JPATH_ADMINISTRATOR.DS."components".DS."com_virtuemart".DS."virtuemart.cfg.php")) {
	echo "<h3 style='color:red;'>Virtuemart does not seem to be installed. Please install Virtuemart first<h3>";
} else {

	JRequest::setVar( 'hidemainmenu', 1 );
 	$install_link = 'index.php?option=com_vm_sef&view=config&task=install';
 	$upgrade_link = 'index.php?option=com_vm_sef&view=config&task=upgrade';
	?>
    <img src="components/com_vm_sef/assets/images/stop.png" width="150" height="150" style="float:left; margin-right:20px;" />
    <?php
	echo '<br>'.JText::_('Install is not complete yet. Click on one of the 2 links below to finish install/upgrade');
	echo '<br>';
	echo '<a style="font-size:24px;" href="'.$install_link.'">'.JText::_('Install').'</a> ';
	echo '<br>';
	echo JText::_('Will do a fresh install. All existing data will be lost');
	echo '<br><br><br>';
	echo '<a style="font-size:24px;" href="'.$upgrade_link.'">'.JText::_('Upgrade').'</a> ';
	echo '<br>';
	echo JText::_('Your data will be updraded to the new version');

	
	

} //End check Virtuemart is installed
?>

