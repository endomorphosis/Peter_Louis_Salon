<?php
/*------------------------------------------------------------------------
# vm_bonus_product - Bonus products for Virtuemart QuickIcon
# ------------------------------------------------------------------------
# author    Jeremy Magne
# copyright Copyright (C) 2010 Daycounts.com. All Rights Reserved.
# Websites: http://www.daycounts.com
# Technical Support: http://www.daycounts.com/en/contact/
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# See http://daycounts.com/fr/component/content/article/7 for details
-------------------------------------------------------------------------*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport('joomla.cache.cache');
jimport('joomla.application.helper');
jimport('joomla.filesystem.file');

if (!defined( '_JOS_QUICKICONVMSEF_MODULE' )) {

class vmSefVersionChecker {
	function getVersionInfo() {
		$comp = JApplicationHelper::parseXMLInstallFile(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_vm_sef'.DS.'com_vm_sef.xml');
		$version = $comp['version'];
		$versioncat = 1;

		$url = 'http://daycounts.com/en/component/versions/?catid='.$versioncat.'&myVersion='.$version.'&task=checkjson';
		if(function_exists('curl_exec')) {
			// Use cURL
			$curl_options = array(
				CURLOPT_AUTOREFERER		=> true,
				CURLOPT_FAILONERROR		=> true,
				CURLOPT_FOLLOWLOCATION	=> true,
				CURLOPT_HEADER			=> false,
				CURLOPT_RETURNTRANSFER	=> true,
				CURLOPT_SSL_VERIFYPEER	=> false,
				CURLOPT_CONNECTTIMEOUT	=> 5,
				CURLOPT_MAXREDIRS		=> 20
			);
			$ch = curl_init($url);
			foreach($curl_options as $option => $value)	{
				@curl_setopt($ch, $option, $value);
			}
			$data = curl_exec($ch);
		} elseif( ini_get('allow_url_fopen') ) {
			// Use fopen() wrappers
			$options = array( 'http' => array(
				'max_redirects' => 10,          // stop after 10 redirects
				'timeout'       => 20         // timeout on response
			) );
			$context = stream_context_create( $options );
			$data = @file_get_contents( $url, false, $context );
		} else {
			$data = false;
		}

		$json = @json_decode($data, true);
		return $json;
	}
}

    /** ensure that functions are declared only once */
    define( '_JOS_QUICKICONVMSEF_MODULE', 1 );
	
	$com_vm_sef = JComponentHelper::getComponent('com_vm_sef', true);
	if (!$com_vm_sef->enabled) {
		return null;
	}
	
	$cache = & JFactory::getCache('com_vm_sef:versioncheck');
	$cache->setCaching(true);	
	$cache->setLifeTime(86400);	
	$checkversion = $params->get('checkversion', 1);

	if ($checkversion) {
		$versionCheck = $cache->call( array( 'vmSefVersionChecker', 'getVersionInfo' ));
	}
	

    function quickiconButtonVmSef( $link, $image, $text, $warning=false, $info='' )
    {
        global $mainframe;
        $lang =& JFactory::getLanguage();
		$style1 = ($lang->isRTL()) ? "float:right;" : "float:left;";
		$style2 = ($warning) ? "background-color:#FF9308;" : "";
		$class = ($warning && $info) ? "hasTip" : "";
		
        ?>
        <div style=" <?php echo $style1; ?>">
            <div class="icon <?php echo $class; ?>" title="<?php echo $info; ?>">
                <a href="<?php echo $link; ?>">
                    <?php echo JHTML::_('image.site',  $image, '/components/com_vm_sef/assets/images/', NULL, NULL, $text ); ?>
                    <span><?php echo $text; ?></span></a>
            </div>
        </div>
        <?php
    }

	JHTML::_('behavior.tooltip');
    ?>
    <div id="cpanel">
        <?php
			$link = 'index.php?option=com_vm_sef';
			if ($versionCheck['valid']===-1) {
				quickiconButtonVmSef( $link, 'sef-48-warning.png', JText::_('Virtuemart SEF').'<br/><font color=\'red\'>'.JText::_( 'Unkown version').'</font>'   , true );
			} elseif ($versionCheck['valid']===0) {
				$msg = '<b>'.JText::_('Your version').'</b>: '.$versionCheck['current'];
				$msg .= '<br/><b>'.JText::_('New version').'</b>: '.$versionCheck['latest'];
				$msg .= '<br/><b>'.JText::_('Published').'</b>: '.$versionCheck['date'];
				$msg .= '<br/><b>'.JText::_('Changes').'</b>:<br/>'.$versionCheck['description'];
				quickiconButtonVmSef( $link, 'sef-48-warning.png', JText::_('Virtuemart SEF').'<br/><font color=\'red\'>'.JText::_( 'New version available').'</font>' , true, $msg );
			} else {
				quickiconButtonVmSef( $link, 'sef-48.png', JText::_( 'Virtuemart SEF' ));
			}
        ?>
    </div>
    <?php
}
?>