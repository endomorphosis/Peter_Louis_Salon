<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?>

		<div class="row rwbdy">
        	<div class="footer">
            	<div class="container">
                	<div class="row rwbdy">
                    	<div class="col-md-6">
                        	<h2 class="copy_text"> © 2014 ADVANCED NUTRITION NETWORK. All rights reserved.</h2>
                        </div>
                        <div class="col-md-6">
                        	<div class="bottom_box">
                            	<!--<ul class="bott_menu">
                                	<li><a href="#"> Home </a></li>
                                    <li><a href="#"> About </a></li>
                                    <li><a href="#"> FAQ </a></li>
                                    <li><a href="#"> VIDEO </a></li>
                                    <li><a href="#"> SHOP</a></li>
                                      <li><a href="#"> CONTACT</a></li>
                                </ul>-->
                                <?php
								      wp_nav_menu( array(
								        'menu'       => 'footermenu',
								        'depth'      => 4,
								        'container'  => false,
								        'menu_class' => 'bott_menu',
								        'fallback_cb' => 'wp_page_menu',
								        //Process nav menu using our custom nav walker
								        'walker' => new wp_bootstrap_navwalker()) 
								    );
								 ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


	<?php wp_footer(); ?>
	
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<?php echo get_template_directory_uri();?>/js/jquery.min.js"></script>
    <script src="<?php echo get_template_directory_uri();?>/js/bootstrap.min.js"></script>
    <script src="<?php echo get_template_directory_uri();?>/js/docs.min.js"></script>
  

</body></html>