<?php

/**

 * The Template for displaying all single posts

 *

 * @package WordPress

 * @subpackage Twenty_Fourteen

 * @since Twenty Fourteen 1.0

 */



get_header(); ?>

<?php

	if ( is_front_page() && twentyfourteen_has_featured_posts() ) {

		// Include the featured content template.

		get_template_part( 'featured-content' );

	}

?>

	 <div class="row rwbdy">

        	<div class="container">

				 <div class="row rwbdy">

			<?php

				// Start the Loop.

				while ( have_posts() ) : the_post();



					/*

					 * Include the post format-specific template for the content. If you want to

					 * use this in a child theme, then include a file called called content-___.php

					 * (where ___ is the post format) and that will be used instead.

					 */

					get_template_part( 'content', 'post' );



				endwhile;

			?>

		</div><!-- #content -->

	</div><!-- #primary -->

	</div>

<?php


get_footer();

