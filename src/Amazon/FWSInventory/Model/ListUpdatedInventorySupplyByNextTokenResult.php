<?php
/** 
 *  PHP Version 5
 *
 *  @category    Amazon
 *  @package     Amazon_FWSInventory
 *  @copyright   Copyright 2008 Amazon Technologies, Inc.
 *  @link        http://aws.amazon.com
 *  @license     http://aws.amazon.com/apache2.0  Apache License, Version 2.0
 *  @version     2009-07-31
 */
/******************************************************************************* 
 *    __  _    _  ___ 
 *   (  )( \/\/ )/ __)
 *   /__\ \    / \__ \
 *  (_)(_) \/\/  (___/
 * 
 *  Amazon FWS Inventory PHP5 Library
 *  Generated: Fri Nov 20 09:59:57 PST 2009
 * 
 */

/**
 *  @see Amazon_FWSInventory_Model
 */
require_once ('Amazon/FWSInventory/Model.php');  

    

/**
 * Amazon_FWSInventory_Model_ListUpdatedInventorySupplyByNextTokenResult
 * 
 * Properties:
 * <ul>
 * 
 * <li>NextToken: string</li>
 * <li>HasNext: bool</li>
 * <li>MerchantSKUSupply: Amazon_FWSInventory_Model_MerchantSKUSupply</li>
 *
 * </ul>
 */ 
class Amazon_FWSInventory_Model_ListUpdatedInventorySupplyByNextTokenResult extends Amazon_FWSInventory_Model
{


    /**
     * Construct new Amazon_FWSInventory_Model_ListUpdatedInventorySupplyByNextTokenResult
     * 
     * @param mixed $data DOMElement or Associative Array to construct from. 
     * 
     * Valid properties:
     * <ul>
     * 
     * <li>NextToken: string</li>
     * <li>HasNext: bool</li>
     * <li>MerchantSKUSupply: Amazon_FWSInventory_Model_MerchantSKUSupply</li>
     *
     * </ul>
     */
    public function __construct($data = null)
    {
        $this->_fields = array (
        'NextToken' => array('FieldValue' => null, 'FieldType' => 'string'),
        'HasNext' => array('FieldValue' => null, 'FieldType' => 'bool'),
        'MerchantSKUSupply' => array('FieldValue' => array(), 'FieldType' => array('Amazon_FWSInventory_Model_MerchantSKUSupply')),
        );
        parent::__construct($data);
    }

        /**
     * Gets the value of the NextToken property.
     * 
     * @return string NextToken
     */
    public function getNextToken() 
    {
        return $this->_fields['NextToken']['FieldValue'];
    }

    /**
     * Sets the value of the NextToken property.
     * 
     * @param string NextToken
     * @return this instance
     */
    public function setNextToken($value) 
    {
        $this->_fields['NextToken']['FieldValue'] = $value;
        return $this;
    }

    /**
     * Sets the value of the NextToken and returns this instance
     * 
     * @param string $value NextToken
     * @return Amazon_FWSInventory_Model_ListUpdatedInventorySupplyByNextTokenResult instance
     */
    public function withNextToken($value)
    {
        $this->setNextToken($value);
        return $this;
    }


    /**
     * Checks if NextToken is set
     * 
     * @return bool true if NextToken  is set
     */
    public function isSetNextToken()
    {
        return !is_null($this->_fields['NextToken']['FieldValue']);
    }

    /**
     * Gets the value of the HasNext property.
     * 
     * @return bool HasNext
     */
    public function getHasNext() 
    {
        return $this->_fields['HasNext']['FieldValue'];
    }

    /**
     * Sets the value of the HasNext property.
     * 
     * @param bool HasNext
     * @return this instance
     */
    public function setHasNext($value) 
    {
        $this->_fields['HasNext']['FieldValue'] = $value;
        return $this;
    }

    /**
     * Sets the value of the HasNext and returns this instance
     * 
     * @param bool $value HasNext
     * @return Amazon_FWSInventory_Model_ListUpdatedInventorySupplyByNextTokenResult instance
     */
    public function withHasNext($value)
    {
        $this->setHasNext($value);
        return $this;
    }


    /**
     * Checks if HasNext is set
     * 
     * @return bool true if HasNext  is set
     */
    public function isSetHasNext()
    {
        return !is_null($this->_fields['HasNext']['FieldValue']);
    }

    /**
     * Gets the value of the MerchantSKUSupply.
     * 
     * @return array of MerchantSKUSupply MerchantSKUSupply
     */
    public function getMerchantSKUSupply() 
    {
        return $this->_fields['MerchantSKUSupply']['FieldValue'];
    }

    /**
     * Sets the value of the MerchantSKUSupply.
     * 
     * @param mixed MerchantSKUSupply or an array of MerchantSKUSupply MerchantSKUSupply
     * @return this instance
     */
    public function setMerchantSKUSupply($merchantSKUSupply) 
    {
        if (!$this->_isNumericArray($merchantSKUSupply)) {
            $merchantSKUSupply =  array ($merchantSKUSupply);    
        }
        $this->_fields['MerchantSKUSupply']['FieldValue'] = $merchantSKUSupply;
        return $this;
    }


    /**
     * Sets single or multiple values of MerchantSKUSupply list via variable number of arguments. 
     * For example, to set the list with two elements, simply pass two values as arguments to this function
     * <code>withMerchantSKUSupply($merchantSKUSupply1, $merchantSKUSupply2)</code>
     * 
     * @param MerchantSKUSupply  $merchantSKUSupplyArgs one or more MerchantSKUSupply
     * @return Amazon_FWSInventory_Model_ListUpdatedInventorySupplyByNextTokenResult  instance
     */
    public function withMerchantSKUSupply($merchantSKUSupplyArgs)
    {
        foreach (func_get_args() as $merchantSKUSupply) {
            $this->_fields['MerchantSKUSupply']['FieldValue'][] = $merchantSKUSupply;
        }
        return $this;
    }   



    /**
     * Checks if MerchantSKUSupply list is non-empty
     * 
     * @return bool true if MerchantSKUSupply list is non-empty
     */
    public function isSetMerchantSKUSupply()
    {
        return count ($this->_fields['MerchantSKUSupply']['FieldValue']) > 0;
    }




}