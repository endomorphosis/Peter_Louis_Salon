<?php
/** 
 *  PHP Version 5
 *
 *  @category    Amazon
 *  @package     Amazon_FWSInventory
 *  @copyright   Copyright 2008 Amazon Technologies, Inc.
 *  @link        http://aws.amazon.com
 *  @license     http://aws.amazon.com/apache2.0  Apache License, Version 2.0
 *  @version     2009-07-31
 */
/******************************************************************************* 
 *    __  _    _  ___ 
 *   (  )( \/\/ )/ __)
 *   /__\ \    / \__ \
 *  (_)(_) \/\/  (___/
 * 
 *  Amazon FWS Inventory PHP5 Library
 *  Generated: Fri Nov 20 09:59:57 PST 2009
 * 
 */

using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Text;


namespace Amazon.FWSInventory.Model
{
    [XmlTypeAttribute(Namespace = "http://fba-inventory.amazonaws.com/doc/2009-07-31/")]
    [XmlRootAttribute(Namespace = "http://fba-inventory.amazonaws.com/doc/2009-07-31/", IsNullable = false)]
    public enum InventorySupplyType
    {
    InStock,
            
            Inbound,
            
            Transfer,
            
        

    }

}