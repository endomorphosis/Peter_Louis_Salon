<?php
/** 
 *  PHP Version 5
 *
 *  @category    Amazon
 *  @package     Amazon_FWSInventory
 *  @copyright   Copyright 2008 Amazon Technologies, Inc.
 *  @link        http://aws.amazon.com
 *  @license     http://aws.amazon.com/apache2.0  Apache License, Version 2.0
 *  @version     2009-07-31
 */
/******************************************************************************* 
 *    __  _    _  ___ 
 *   (  )( \/\/ )/ __)
 *   /__\ \    / \__ \
 *  (_)(_) \/\/  (___/
 * 
 *  Amazon FWS Inventory PHP5 Library
 *  Generated: Fri Nov 20 09:59:57 PST 2009
 * 
 */

/**
 * List Updated Inventory Supply By Next Token  Sample
 */

include_once ('.config.inc.php'); 

/************************************************************************
 * Instantiate Implementation of Amazon FWSInventory
 * 
 * AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY constants 
 * are defined in the .config.inc.php located in the same 
 * directory as this sample
 ***********************************************************************/
 $service = new Amazon_FWSInventory_Client(AWS_ACCESS_KEY_ID, 
                                       AWS_SECRET_ACCESS_KEY);
 
/************************************************************************
 * Uncomment to try out Mock Service that simulates Amazon_FWSInventory
 * responses without calling Amazon_FWSInventory service.
 *
 * Responses are loaded from local XML files. You can tweak XML files to
 * experiment with various outputs during development
 *
 * XML files available under Amazon/FWSInventory/Mock tree
 *
 ***********************************************************************/
 // $service = new Amazon_FWSInventory_Mock();

/************************************************************************
 * Setup request parameters and uncomment invoke to try out 
 * sample for List Updated Inventory Supply By Next Token Action
 ***********************************************************************/
 // @TODO: set request. Action can be passed as Amazon_FWSInventory_Model_ListUpdatedInventorySupplyByNextTokenRequest
 // object or array of parameters
 // invokeListUpdatedInventorySupplyByNextToken($service, $request);

                                    
/**
  * List Updated Inventory Supply By Next Token Action Sample
  * Continues pagination over a resultset of inventory data for inventory
  * items that may have had recent changes in inventory levels.
  * 
  * This operation is used in conjunction with ListUpdatedInventorySupply.
  * Please refer to documentation for that operation for further details.
  *   
  * @param Amazon_FWSInventory_Interface $service instance of Amazon_FWSInventory_Interface
  * @param mixed $request Amazon_FWSInventory_Model_ListUpdatedInventorySupplyByNextToken or array of parameters
  */
  function invokeListUpdatedInventorySupplyByNextToken(Amazon_FWSInventory_Interface $service, $request) 
  {
      try {
              $response = $service->listUpdatedInventorySupplyByNextToken($request);
              
                echo ("Service Response\n");
                echo ("=============================================================================\n");

                echo("        ListUpdatedInventorySupplyByNextTokenResponse\n");
                if ($response->isSetListUpdatedInventorySupplyByNextTokenResult()) { 
                    echo("            ListUpdatedInventorySupplyByNextTokenResult\n");
                    $listUpdatedInventorySupplyByNextTokenResult = $response->getListUpdatedInventorySupplyByNextTokenResult();
                    if ($listUpdatedInventorySupplyByNextTokenResult->isSetNextToken()) 
                    {
                        echo("                NextToken\n");
                        echo("                    " . $listUpdatedInventorySupplyByNextTokenResult->getNextToken() . "\n");
                    }
                    if ($listUpdatedInventorySupplyByNextTokenResult->isSetHasNext()) 
                    {
                        echo("                HasNext\n");
                        echo("                    " . $listUpdatedInventorySupplyByNextTokenResult->getHasNext() . "\n");
                    }
                    $merchantSKUSupplyList = $listUpdatedInventorySupplyByNextTokenResult->getMerchantSKUSupply();
                    foreach ($merchantSKUSupplyList as $merchantSKUSupply) {
                        echo("                MerchantSKUSupply\n");
                        if ($merchantSKUSupply->isSetMerchantSKU()) 
                        {
                            echo("                    MerchantSKU\n");
                            echo("                        " . $merchantSKUSupply->getMerchantSKU() . "\n");
                        }
                        if ($merchantSKUSupply->isSetFulfillmentNetworkSKU()) 
                        {
                            echo("                    FulfillmentNetworkSKU\n");
                            echo("                        " . $merchantSKUSupply->getFulfillmentNetworkSKU() . "\n");
                        }
                        if ($merchantSKUSupply->isSetASIN()) 
                        {
                            echo("                    ASIN\n");
                            echo("                        " . $merchantSKUSupply->getASIN() . "\n");
                        }
                        if ($merchantSKUSupply->isSetCondition()) 
                        {
                            echo("                    Condition\n");
                            echo("                        " . $merchantSKUSupply->getCondition() . "\n");
                        }
                        if ($merchantSKUSupply->isSetTotalSupplyQuantity()) 
                        {
                            echo("                    TotalSupplyQuantity\n");
                            echo("                        " . $merchantSKUSupply->getTotalSupplyQuantity() . "\n");
                        }
                        if ($merchantSKUSupply->isSetInStockSupplyQuantity()) 
                        {
                            echo("                    InStockSupplyQuantity\n");
                            echo("                        " . $merchantSKUSupply->getInStockSupplyQuantity() . "\n");
                        }
                        if ($merchantSKUSupply->isSetEarliestAvailability()) { 
                            echo("                    EarliestAvailability\n");
                            $earliestAvailability = $merchantSKUSupply->getEarliestAvailability();
                            if ($earliestAvailability->isSetTimepointType()) 
                            {
                                echo("                        TimepointType\n");
                                echo("                            " . $earliestAvailability->getTimepointType() . "\n");
                            }
                            if ($earliestAvailability->isSetDateTime()) 
                            {
                                echo("                        DateTime\n");
                                echo("                            " . $earliestAvailability->getDateTime() . "\n");
                            }
                        } 
                        $supplyDetailList = $merchantSKUSupply->getSupplyDetail();
                        foreach ($supplyDetailList as $supplyDetail) {
                            echo("                    SupplyDetail\n");
                            if ($supplyDetail->isSetQuantity()) 
                            {
                                echo("                        Quantity\n");
                                echo("                            " . $supplyDetail->getQuantity() . "\n");
                            }
                            if ($supplyDetail->isSetSupplyType()) 
                            {
                                echo("                        SupplyType\n");
                                echo("                            " . $supplyDetail->getSupplyType() . "\n");
                            }
                            if ($supplyDetail->isSetEarliestAvailableToPickDateTime()) { 
                                echo("                        EarliestAvailableToPickDateTime\n");
                                $earliestAvailableToPickDateTime = $supplyDetail->getEarliestAvailableToPickDateTime();
                                if ($earliestAvailableToPickDateTime->isSetTimepointType()) 
                                {
                                    echo("                            TimepointType\n");
                                    echo("                                " . $earliestAvailableToPickDateTime->getTimepointType() . "\n");
                                }
                                if ($earliestAvailableToPickDateTime->isSetDateTime()) 
                                {
                                    echo("                            DateTime\n");
                                    echo("                                " . $earliestAvailableToPickDateTime->getDateTime() . "\n");
                                }
                            } 
                            if ($supplyDetail->isSetLatestAvailableToPickDateTime()) { 
                                echo("                        LatestAvailableToPickDateTime\n");
                                $latestAvailableToPickDateTime = $supplyDetail->getLatestAvailableToPickDateTime();
                                if ($latestAvailableToPickDateTime->isSetTimepointType()) 
                                {
                                    echo("                            TimepointType\n");
                                    echo("                                " . $latestAvailableToPickDateTime->getTimepointType() . "\n");
                                }
                                if ($latestAvailableToPickDateTime->isSetDateTime()) 
                                {
                                    echo("                            DateTime\n");
                                    echo("                                " . $latestAvailableToPickDateTime->getDateTime() . "\n");
                                }
                            } 
                        }
                    }
                } 
                if ($response->isSetResponseMetadata()) { 
                    echo("            ResponseMetadata\n");
                    $responseMetadata = $response->getResponseMetadata();
                    if ($responseMetadata->isSetRequestId()) 
                    {
                        echo("                RequestId\n");
                        echo("                    " . $responseMetadata->getRequestId() . "\n");
                    }
                } 

     } catch (Amazon_FWSInventory_Exception $ex) {
         echo("Caught Exception: " . $ex->getMessage() . "\n");
         echo("Response Status Code: " . $ex->getStatusCode() . "\n");
         echo("Error Code: " . $ex->getErrorCode() . "\n");
         echo("Error Type: " . $ex->getErrorType() . "\n");
         echo("Request ID: " . $ex->getRequestId() . "\n");
         echo("XML: " . $ex->getXML() . "\n");
     }
 }
    