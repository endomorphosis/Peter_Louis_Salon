<?php
/** 
 *  PHP Version 5
 *
 *  @category    Amazon
 *  @package     Amazon_FWSInventory
 *  @copyright   Copyright 2008 Amazon Technologies, Inc.
 *  @link        http://aws.amazon.com
 *  @license     http://aws.amazon.com/apache2.0  Apache License, Version 2.0
 *  @version     2009-07-31
 */
/******************************************************************************* 
 *    __  _    _  ___ 
 *   (  )( \/\/ )/ __)
 *   /__\ \    / \__ \
 *  (_)(_) \/\/  (___/
 * 
 *  Amazon FWS Inventory PHP5 Library
 *  Generated: Fri Nov 20 09:59:57 PST 2009
 * 
 */

/**
 * Get Inventory Supply  Sample
 */

include_once ('.config.inc.php'); 

/************************************************************************
 * Instantiate Implementation of Amazon FWSInventory
 * 
 * AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY constants 
 * are defined in the .config.inc.php located in the same 
 * directory as this sample
 ***********************************************************************/
 $service = new Amazon_FWSInventory_Client(AWS_ACCESS_KEY_ID, 
                                       AWS_SECRET_ACCESS_KEY);
 
/************************************************************************
 * Uncomment to try out Mock Service that simulates Amazon_FWSInventory
 * responses without calling Amazon_FWSInventory service.
 *
 * Responses are loaded from local XML files. You can tweak XML files to
 * experiment with various outputs during development
 *
 * XML files available under Amazon/FWSInventory/Mock tree
 *
 ***********************************************************************/
 // $service = new Amazon_FWSInventory_Mock();

/************************************************************************
 * Setup request parameters and uncomment invoke to try out 
 * sample for Get Inventory Supply Action
 ***********************************************************************/
 // @TODO: set request. Action can be passed as Amazon_FWSInventory_Model_GetInventorySupplyRequest
 // object or array of parameters
 // invokeGetInventorySupply($service, $request);

                            
/**
  * Get Inventory Supply Action Sample
  * Get information about the supply of merchant-owned inventory in
  * Amazon's fulfillment network. "Supply" is inventory that is available
  * for fulfilling (a.k.a. Multi-Channel Fulfillment) orders. In general
  * this includes all sellable inventory that has been received by Amazon,
  * that is not reserved for existing orders or for internal FC processes,
  * and also inventory expected to be received from inbound shipments.
  *   
  * @param Amazon_FWSInventory_Interface $service instance of Amazon_FWSInventory_Interface
  * @param mixed $request Amazon_FWSInventory_Model_GetInventorySupply or array of parameters
  */
  function invokeGetInventorySupply(Amazon_FWSInventory_Interface $service, $request) 
  {
      try {
              $response = $service->getInventorySupply($request);
              
                echo ("Service Response\n");
                echo ("=============================================================================\n");

                echo("        GetInventorySupplyResponse\n");
                if ($response->isSetGetInventorySupplyResult()) { 
                    echo("            GetInventorySupplyResult\n");
                    $getInventorySupplyResult = $response->getGetInventorySupplyResult();
                    $merchantSKUSupplyList = $getInventorySupplyResult->getMerchantSKUSupply();
                    foreach ($merchantSKUSupplyList as $merchantSKUSupply) {
                        echo("                MerchantSKUSupply\n");
                        if ($merchantSKUSupply->isSetMerchantSKU()) 
                        {
                            echo("                    MerchantSKU\n");
                            echo("                        " . $merchantSKUSupply->getMerchantSKU() . "\n");
                        }
                        if ($merchantSKUSupply->isSetFulfillmentNetworkSKU()) 
                        {
                            echo("                    FulfillmentNetworkSKU\n");
                            echo("                        " . $merchantSKUSupply->getFulfillmentNetworkSKU() . "\n");
                        }
                        if ($merchantSKUSupply->isSetASIN()) 
                        {
                            echo("                    ASIN\n");
                            echo("                        " . $merchantSKUSupply->getASIN() . "\n");
                        }
                        if ($merchantSKUSupply->isSetCondition()) 
                        {
                            echo("                    Condition\n");
                            echo("                        " . $merchantSKUSupply->getCondition() . "\n");
                        }
                        if ($merchantSKUSupply->isSetTotalSupplyQuantity()) 
                        {
                            echo("                    TotalSupplyQuantity\n");
                            echo("                        " . $merchantSKUSupply->getTotalSupplyQuantity() . "\n");
                        }
                        if ($merchantSKUSupply->isSetInStockSupplyQuantity()) 
                        {
                            echo("                    InStockSupplyQuantity\n");
                            echo("                        " . $merchantSKUSupply->getInStockSupplyQuantity() . "\n");
                        }
                        if ($merchantSKUSupply->isSetEarliestAvailability()) { 
                            echo("                    EarliestAvailability\n");
                            $earliestAvailability = $merchantSKUSupply->getEarliestAvailability();
                            if ($earliestAvailability->isSetTimepointType()) 
                            {
                                echo("                        TimepointType\n");
                                echo("                            " . $earliestAvailability->getTimepointType() . "\n");
                            }
                            if ($earliestAvailability->isSetDateTime()) 
                            {
                                echo("                        DateTime\n");
                                echo("                            " . $earliestAvailability->getDateTime() . "\n");
                            }
                        } 
                        $supplyDetailList = $merchantSKUSupply->getSupplyDetail();
                        foreach ($supplyDetailList as $supplyDetail) {
                            echo("                    SupplyDetail\n");
                            if ($supplyDetail->isSetQuantity()) 
                            {
                                echo("                        Quantity\n");
                                echo("                            " . $supplyDetail->getQuantity() . "\n");
                            }
                            if ($supplyDetail->isSetSupplyType()) 
                            {
                                echo("                        SupplyType\n");
                                echo("                            " . $supplyDetail->getSupplyType() . "\n");
                            }
                            if ($supplyDetail->isSetEarliestAvailableToPickDateTime()) { 
                                echo("                        EarliestAvailableToPickDateTime\n");
                                $earliestAvailableToPickDateTime = $supplyDetail->getEarliestAvailableToPickDateTime();
                                if ($earliestAvailableToPickDateTime->isSetTimepointType()) 
                                {
                                    echo("                            TimepointType\n");
                                    echo("                                " . $earliestAvailableToPickDateTime->getTimepointType() . "\n");
                                }
                                if ($earliestAvailableToPickDateTime->isSetDateTime()) 
                                {
                                    echo("                            DateTime\n");
                                    echo("                                " . $earliestAvailableToPickDateTime->getDateTime() . "\n");
                                }
                            } 
                            if ($supplyDetail->isSetLatestAvailableToPickDateTime()) { 
                                echo("                        LatestAvailableToPickDateTime\n");
                                $latestAvailableToPickDateTime = $supplyDetail->getLatestAvailableToPickDateTime();
                                if ($latestAvailableToPickDateTime->isSetTimepointType()) 
                                {
                                    echo("                            TimepointType\n");
                                    echo("                                " . $latestAvailableToPickDateTime->getTimepointType() . "\n");
                                }
                                if ($latestAvailableToPickDateTime->isSetDateTime()) 
                                {
                                    echo("                            DateTime\n");
                                    echo("                                " . $latestAvailableToPickDateTime->getDateTime() . "\n");
                                }
                            } 
                        }
                    }
                } 
                if ($response->isSetResponseMetadata()) { 
                    echo("            ResponseMetadata\n");
                    $responseMetadata = $response->getResponseMetadata();
                    if ($responseMetadata->isSetRequestId()) 
                    {
                        echo("                RequestId\n");
                        echo("                    " . $responseMetadata->getRequestId() . "\n");
                    }
                } 

     } catch (Amazon_FWSInventory_Exception $ex) {
         echo("Caught Exception: " . $ex->getMessage() . "\n");
         echo("Response Status Code: " . $ex->getStatusCode() . "\n");
         echo("Error Code: " . $ex->getErrorCode() . "\n");
         echo("Error Type: " . $ex->getErrorType() . "\n");
         echo("Request ID: " . $ex->getRequestId() . "\n");
         echo("XML: " . $ex->getXML() . "\n");
     }
 }
            