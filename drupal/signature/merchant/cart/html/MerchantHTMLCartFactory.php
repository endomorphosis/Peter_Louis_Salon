<?php
require_once("/home/peterlou/public_html/drupal/signature/common/cart/html/HTMLCartFactory.php");
/**
 * Returns a simple static cart to generate a signature from.
 *
 * Copyright 2008-2008 Amazon.com, Inc., or its affiliates. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the “License”).
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 *    http://aws.amazon.com/apache2.0/
 *
 * or in the “license” file accompanying this file.
 * This file is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */
class MerchantHTMLCartFactory extends HTMLCartFactory {

   /**
    * Instantiate an instance of the cart factory.
    */
   public function MerchantHTMLCartFactory()
   {
   }

   public function getCart($merchantID, $awsAccessKeyID, $data_array) {
       $parameterMap = $this->getCartMap($merchantID, $awsAccessKeyID, $data_array);
       return $this->getCartFromMap($merchantID, $awsAccessKeyID, $parameterMap);
   }


   /**
    * Get map representation of the cart.
    * Replace with your own cart here to try out
    * different promotions, tax, shipping, etc. 
    */
   protected function getCartMap($merchantID, $awsAccessKeyID, $data_array) {
//$new_array = $data_array;
//ob_start();
// require_once('~/drupal/Amazon/FWSInventory/Samples/GetInventorySupplySample.php');
      foreach($data_array as $key => $value){
	  if(isset($value['sku'])){
	  if($value['ptype'] == 'amazon') { $noship = true; }
   else{
//$request = new Amazon_FWSInventory_Model_GetInventorySupplyRequest();
//$values = invokeGetInventorySupply($service, $request->withMerchantSKU($value['sku']));  
$ch = curl_init("http://www.peaceandlovesalon.com/~peterlou/Amazon/FWSInventory/Samples/GetInventorySupplySample.php?sku=".urlencode($value['sku']));
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
$output = curl_exec($ch); 
//echo $output;
$values = $output;
if(!$values || $values == 0){ $noship = true ;}
unset($values);
}
}}
//ob_end_clean();

      foreach($data_array as $key => $value){
	  if(isset($value['sku'])){
	  $cart["item_merchant_id_".$key] = $merchantID;
          $cart["item_title_".$key] =  $value['title'];
          $cart["item_sku_".$key] =  $value['sku'];
         // $cart["item_description_".$key] = $value['description'];
          $cart["item_price_".$key] = $value['price'];
          $cart["item_quantity_".$key] = $value['qty'];
          $cart["currency_code"] = "USD";
          $cart["aws_access_key_id"] = $awsAccessKeyID;
	  if($value['ptype'] == 'amazon' && $noship == false) { $cart["item_fulfillment_network_".$key] = "AMAZON_NA";}
     //     if($total <= 49){$cart['shipping_method_price_per_shipment_amount'] = "7";} 
}}

//print_r($cart);
      // sort cart by key
      ksort($cart);

      return $cart;
   }

   /**
    * Construct a very basic cart with one item.
    */
   public function getCartHTML($merchantID, $awsAccessKeyID, $signature, $data_array) {
       $cartInput = $this->getCart($merchantID, $awsAccessKeyID, $data_array);
       return $this->getCartHTMLFromCartInput($merchantID, $awsAccessKeyID, $signature,
                                 $cartInput);
   }


   /**
    * Get the input to generate the hmac-sha1 signature.
    */
   public function getSignatureInput($merchantID, $awsAccessKeyID, $data_array) {
      $parameterMap = $this->getCartMap($merchantID, $awsAccessKeyID, $data_array);
      return $this->getSignatureInputFromMap($parameterMap);
   }
}
?>
