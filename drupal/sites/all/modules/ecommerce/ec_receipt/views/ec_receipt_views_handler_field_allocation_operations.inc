<?php

/**
 * @file
 * Handles list of operation links for a receipt.
 */

class ec_receipt_views_handler_field_allocation_operations extends views_handler_field {

  function render($values) {
    if ($receipt = db_fetch_object(db_query('SELECT * FROM {ec_receipt_allocation} WHERE eaid = %d', $values->{$this->field_alias}))) {
      $links = module_invoke_all('link', 'ec_receipt_allocation', $receipt);
      if (!empty($links)) {
        return theme('ctools_dropdown', t('Operations'), $links);
      }
    }
  }
}
