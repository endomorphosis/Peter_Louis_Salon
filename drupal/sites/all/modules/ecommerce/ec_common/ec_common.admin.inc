<?php

/**
 * @file
 * This module contains generic functions for ecommerce which are typically
 * available to the user with 'store admin manage' permissions.
 */

/**
 * Create an overview of the Settings Page (based on system_settings_overview).
 */
function ec_common_settings_overview($path) {
  $content = system_admin_menu_block(menu_get_item($path));
  return theme('admin_block_content', $content);
}

/**
 * Theme the form for the table style plugin
 */
function theme_ec_common_style_plugin_table($form) {
  $output = drupal_render($form['description_markup']);

  $header = array(
    t('Field'),
    t('Column'),
    t('Separator'),
    t('Row'),
    t('Row span'),
    t('Col span'),
    array(
      'data' => t('Sortable'),
      'align' => 'center',
    ),
    array(
      'data' => t('Default sort'),
      'align' => 'center',
    ),
  );
  $rows = array();
  foreach (element_children($form['columns']) as $id) {
    $row = array();
    $row[] = drupal_render($form['info'][$id]['name']);
    $row[] = drupal_render($form['columns'][$id]);
    $row[] = drupal_render($form['info'][$id]['separator']);
    $row[] = drupal_render($form['info'][$id]['row']);
    $row[] = drupal_render($form['info'][$id]['rowspan']);
    $row[] = drupal_render($form['info'][$id]['colspan']);
    if (!empty($form['info'][$id]['sortable'])) {
      $row[] = array(
        'data' => drupal_render($form['info'][$id]['sortable']),
        'align' => 'center',
      );
      $row[] = array(
        'data' => drupal_render($form['default'][$id]),
        'align' => 'center',
      );
    }
    else {
      $row[] = '';
      $row[] = '';
    }
    $rows[] = $row;
  }

  // Add the special 'None' row.
  $rows[] = array(t('None'), '', '', '', '', '', '', array('align' => 'center', 'data' => drupal_render($form['default'][-1])));

  $output .= theme('table', $header, $rows);
  $output .= drupal_render($form);
  return $output;
}