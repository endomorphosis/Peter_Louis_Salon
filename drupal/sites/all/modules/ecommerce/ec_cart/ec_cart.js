// $Id: ec_cart.js,v 1.1.2.5 2009/04/27 04:50:54 gordon Exp $

Drupal.ec_cart_load = function (cart) {
  $('#block-ec_cart-0')
    .each(
      function () {
        $(this).before(cart.full_cart)
        $(this).remove();
      }
    );
}

Drupal.ec_cart_success = function (json) {
  if (json.errors == undefined) {
    if (json.full_cart) {
      Drupal.ec_cart_load(json);
    }
    else {
      var block_item = $(json.block_item);
      var total = json.total;
      var items = json.items;
      var block_id = $('.cart-item-wrapper', '<div>' + json.block_item + '</div>').attr('id');
      var item = $('#'+ block_id);

      if ($('#' + item).size()) {
        $(item)
          .fadeOut("slow",
            function () {
              $(this)
                .replaceWith(block_item)
                .fadeIn("slow");
            }
          );
      }
      else {
        $('#block-ec_cart-0 .items')
          .append(block_item)
          .find('#block-ec_cart-0 .cart-item:last')
          .fadeIn("slow");
      }
      $('#block-ec_cart-0 .total')
        .fadeOut("slow", 
          function () {
            $(this)
              .html(total)
              .fadeIn("slow");
          }
        );
      $('#block-ec_cart-0 .item-count em')
        .each(
          function () {
            $(this)
              .html(items+' items');
          }
        );
      $('#block-ec_cart-0 .checkout:hidden')
        .each(
          function () {
            $(this)
              .fadeIn("slow");
          }
        );
    }
  }
  else {
    alert(json.errors.join('\n'));
  }
}

Drupal.ec_cart_add = function (href) {
  jQuery.getJSON(href, Drupal.ec_cart_success);
  return false;
}

Drupal.ec_cart_convert_link = function () {
  var e = this;
  var href = Drupal.settings.ec_cart.add_path+'/'+$(this).attr('id').replace(/cart-item-/i, '');
  var q = $(this).attr('href').split('?');
  if (q[1] != undefined) {
    href+= '?'+q[1];
  }
  Drupal.ec_cart_add(href);
  return false;
}

Drupal.ecCartSubmit = function (response) {
  Drupal.ec_cart_success(response);
  return $('html');
}

$(document).ready(
  function () {
    if (Drupal.settings.ec_cart.empty) {
      $('#block-ec_cart-0').hide();
    }
    if (Drupal.settings.ec_cart.load_cart) {
      $.getJSON(Drupal.settings.ec_cart.load_path, Drupal.ec_cart_load)
    }
    $('a.cart-link').click(Drupal.ec_cart_convert_link);
  }
);
