<?php

/**
 * @file
 * Charge functions for ec_store.
 */

function ec_store_misc_group_filter_form(&$form_state, $settings = array()) {
  $form = array();
  $settings+= array('group' => '');

  $form['group'] = array(
    '#type' => 'textfield',
    '#title' => t('Charge group'),
    '#default_value' => $settings['group'],
    '#required' => TRUE,
  );

  return $form;
}

function ec_store_misc_group_filter_settings() {
  return array('group');
}

function ec_store_misc_group_filter_process($type, $settings, $object, $charges = array()) {
  $$type =& $object;

  switch ($type) {
    case 'txn':
      if (!empty($txn->misc)) {
        $charges = $txn->misc;
      }
      break;
  }

  ec_store_filter_misc(array('misc_group' => $settings['group']), 'init');

  $charges = array_filter($charges, 'ec_store_filter_misc');

  return !empty($charges);
}

function ec_store_txn_type_filter_form($form_state, $settings = array()) {
  $settings += array(
    'txn_type' => array(),
  );

  $options = ec_store_transaction_types();

  $form['txn_type'] = array(
    '#type' => 'select',
    '#title' => t('Transaction type'),
    '#default_value' => $settings['txn_type'],
    '#options' => $options,
    '#description' => t('Select type of transactions to allow.'),
    '#required' => TRUE,
    '#multiple' => TRUE,
  );

  return $form;
}

function ec_store_txn_type_filter_settings() {
  return array('txn_type');
}

function ec_store_txn_type_filter_process($type, $settings, $object, $charges = array()) {
  $$type =& $object;

  switch ($type) {
    case 'txn':
      if (in_array($txn->type, $settings['txn_type'])) {
        return TRUE;
      }
      break;
  }

  return FALSE;
}

function ec_store_misc_group_variable_form(&$form_state, $settings = array()) {
  return ec_store_misc_group_filter_form($form_state, $settings);
}

function ec_store_misc_group_variable_settings() {
  return array('group');
}

function ec_store_misc_group_variable_process($type, $settings, $object, $charges = array()) {
  $$type =& $object;

  switch ($type) {
    case 'txn':
      if (!empty($txn->misc)) {
        $charges = array_merge($charges, $txn->misc);
      }
      break;
  }

  ec_store_filter_misc(array('misc_group' => $settings['group']), 'init');

  $charges = array_filter($charges, 'ec_store_filter_misc');

  return array_sum(array_map('ec_store_map_price', $charges));
}

function ec_store_misc_group_variable_description($chg, $type, $object, $variables) {
  return t('The summation of all the additional charges with the tax group %group', array('%group' => $settings['group']));
}

function ec_store_fields_filter_form(&$form_state, $settings) {
  ctools_include('export');
    
  $fields = ctools_export_crud_load_all('ec_fields');
  $options = array();
  foreach ($fields as $name => $field) {
    $options[$name] = $field->field_title;
  }
  
  $form = array();
  
  $form['name'] = array(
    '#type' => 'select',
    '#title' => t('Field'),
    '#default_value' => $settings['name'],
    '#options' => array_merge(array('' => t('- Select -')), $options),
    '#required' => TRUE,
  );
  
  $form['operation'] = array(
    '#value' => '='
  );
  
  $form['value'] = array(
    '#type' => 'textfield',
    '#title' => t('Value'),
    '#default_value' => $settings['value'],
  );
  
  return $form;
}

function ec_store_fields_filter_settings() {
  return array('name', 'operation', 'value');
}

function ec_store_fields_filter_process($type, $settings, $object, $charges, $chg) {
  $$type =& $object;
  
  if ($type == 'txn') {
    $txn->fields_accessed[] = $settings['name'];
    if (isset($txn->fields[$settings['name']]) && $txn->fields[$settings['name']] == $settings['value']) {
      return TRUE;
    }
  }
  
  return FALSE;
}