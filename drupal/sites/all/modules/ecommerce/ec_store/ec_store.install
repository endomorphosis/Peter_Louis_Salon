<?php

/**
 * @file
 * Install and uninstall routines, incremental database updates and database
 * schema.
 */

/**
 * Implementation of hook_requirements().
 */
function ec_store_requirements($phase) {
  $requirements = array();
  if ($phase == 'runtime') {
    $balance = (float)db_result(db_query('SELECT SUM(gross - (allocated + balance)) FROM {ec_transaction}'));
    $requirements['ec_txn_balance'] = array(
      'title' => t('Transaction balance'),
      'value' => $balance ? t('Transactions are not in balance!') : t('Transactions are in balance'),
      'severity' => $balance ? REQUIREMENT_ERROR : REQUIREMENT_OK,
      'description' => t('Checks to make sure all the transactions are internally in balance (gross - (allocated + balance) == %balance). This should be $0.00', array('%balance' => format_currency($balance))),
    );
    if (module_exists('ec_receipt')) {
      $t_allocated = db_result(db_query('SELECT SUM(allocated) FROM {ec_transaction}'));
      $a_allocated = db_result(db_query("SELECT SUM(amount) FROM {ec_receipt_allocation} WHERE type ='transaction'"));
      $balance = $t_allocated - $a_allocated;
      $requirements['ec_txn_allocations'] = array(
        'title' => t('Transaction allocation'),
        'value' => $balance ? t('Transaction allocations are not in balance!') : t('Transactions allocations are in balance'),
        'severity' => $balance ? REQUIREMENT_ERROR : REQUIREMENT_OK,
        'description' => t('Checks that the allocationed amounts are equal. ie (ec_transaction.allocated - ec_receipt_allocation.amount == %balance). This should be $0.00', array('%balance' => format_currency($balance))),
      );
    }
  }
  return $requirements;
}

/**
 * Implementation of hook_install().
 */
function ec_store_install() {
  $versions = drupal_get_installed_schema_version('store', FALSE, TRUE);
  if (!isset($versions['store']) || $versions['store'] == SCHEMA_UNINSTALLED) {
    drupal_install_schema('ec_store');

    $workflow = array(
      array('workflow' => 1, 'description' => 'received', 'type' => 0, 'weight' => 1),
      array('workflow' => 2, 'description' => 'invoiced', 'type' => 0, 'weight' => 2),
      array('workflow' => 10, 'description' => 'in picking', 'type' => 0, 'weight' => 3),
      array('workflow' => 8, 'description' => 'packaged', 'type' => 0, 'weight' => 4),
      array('workflow' => 3, 'description' => 'shipped', 'type' => 0, 'weight' => 5),
      array('workflow' => 4, 'description' => 'awaiting customer response', 'type' => 1, 'weight' => 6),
      array('workflow' => 5, 'description' => 'canceled', 'type' => 2, 'weight' => 7),
      array('workflow' => 6, 'description' => 'completed', 'type' => 1, 'weight' => 8),
      array('workflow' => 7, 'description' => 'security violation', 'type' => 0, 'weight' => 9),
    );

    foreach ($workflow as $item) {
      // Instead of using drupal_write_record() this items are inserted manually
      // because drupal_write_record() doesn't support setting the primary key.
      db_query("INSERT INTO {ec_workflow_statuses} (workflow, description, type, weight) VALUES (%d, '%s', %d, %d)", $item['workflow'], $item['description'], $item['type'], $item['weight']);
    }

    actions_save('ec_store_action_set_workflow', 'transaction', array('workflow' => '3'), 'Ship Orders');
  }

  drupal_set_message(st('e-Commerce: All core modules have been installed! You still need install at least one payment gateway in order to use the e-Commerce. Good sales!'));
}

/**
 * Implementation of hook_enable().
 */
function ec_store_enable() {
  $versions = drupal_get_installed_schema_version('store', FALSE, TRUE);
  if (isset($versions['store']) && $versions['store'] != SCHEMA_UNINSTALLED) {
    // Upgrade from e-Commerce 3 store module.
    drupal_set_installed_schema_version('ec_store', $versions['store']);
    drupal_set_installed_schema_version('store', SCHEMA_UNINSTALLED);
  }
}

/**
 * Implementation of hook_uninstall().
 */
function ec_store_uninstall() {
  drupal_uninstall_schema('ec_store');

  variable_del('ec_store_email_customer_invoice_subject');
  variable_del('ec_store_email_customer_invoice_body');
  variable_del('ec_store_email_processing_error_subject');
  variable_del('ec_store_email_processing_error_body');

  variable_del('ec_measure_weight');
  variable_del('payment_notices');
  variable_del('ec_order_overview');
  variable_del('customer_invoice');
  variable_del('processing_error');
  variable_del('ask_customer_template');
  variable_del('cancel_transaction');
  variable_del('product_cart_is_destination');
  variable_del('ec_goto_cart_empty');

  variable_del('ec_country');
  variable_del('ec_measure_weight');
  variable_del('ec_measure_length');
  variable_del('ec_measure_area');
  variable_del('ec_measure_volume');
}

/**
 * Implementation of hook_schema().
 */
function ec_store_schema() {
  $schema = array();

  $schema['ec_workflow_statuses'] = array(
    'description' => 'Transaction workflow statuses table.',
    'fields' => array(
      'workflow' => array(
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'disp-width' => '10',
        'description' => 'Workflow status code.',
      ),
      'description' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'description' => 'Workflow status description.',
      ),
      'help' => array(
        'type' => 'text',
        'description' => 'Workflow status help text.',
      ),
      'type' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'disp-width' => '10',
        'description' => 'Workflow status type.',
      ),
      'weight' => array(
        'type' => 'int',
        'not null' => TRUE,
        'disp-width' => '11',
        'description' => 'Workflow status weight.',
      ),
    ),
    'primary key' => array('workflow'),
  );

  $schema['ec_fields'] = array(
    'description' => 'Configuration of fields which will appear on checkout',
    'fields' => array(
      'name' => array(
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
        'description' => t('Unique identifier for each charge'),
      ),
      'field_type' => array(
        'type' => 'varchar',
        'length' => 16,
        'not null' => TRUE,
        'default' => '',
        'description' => 'Type of field to be created.',
      ),
      'field_title' => array(
        'type' => 'varchar',
        'length' => 128,
        'not null' => TRUE,
        'default' => '',
        'description' => 'Title which is shown on the checkout.',
      ),
      'field_description' => array(
        'type' => 'text',
        'size' => 'medium',
        'default' => '',
        'not null' => TRUE,
        'description' => 'description to display on the field created.',
      ),
      'field_error' => array(
        'type' => 'text',
        'size' => 'medium',
        'default' => '',
        'not null' => TRUE,
        'description' => 'Error message to display when the field is not used.',
      ), 
      'field_settings' => array(
        'type' => 'text',
        'serialize' => TRUE,
      ),
      'weight' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'description' => t('Weight of the charge to determine the order'),
      ),
      'enabled' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'size' => 'tiny',
        'description' => t('Set the charged as enabled.'),
      ),
    ),
    'primary key' => array('name'),
    'export' => array(
      'key' => 'name',
      'key name' => 'Name',
      'can disable' => TRUE,
      'status' => 'enabled',
    ),
  );

  $schema['ec_transaction'] = array(
    'description' => 'Transaction table.',
    'fields' => array(
      'txnid' => array(
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => 'Transaction identifier.',
      ),
      'ecid' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => 'The {ec_customer}.ecid that owns this transaction.',
      ),
      'type' => array(
        'type' => 'varchar',
        'length' => '16',
        'not null' => TRUE,
        'default' => '',
        'description' => 'Transaction type.',
      ),
      'mail' => array(
        'type' => 'varchar',
        'length' => '64',
        'not null' => TRUE,
        'default' => '',
        'description' => 'Transaction customer email.',
      ),
      // Should I be removing this field?
      'payment_method' => array(
        'type' => 'varchar',
        'length' => '32',
        'not null' => TRUE,
        'default' => '',
        'description' => 'Payment method.',
      ),
      'allocation' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'description' => 'Receipt allocation status.',
      ),
      'workflow' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => 'Workflow status.',
      ),
      'shippable' => array(
        'type' => 'int',
        'size' => 'tiny',
        'default' => 0,
        'not null' => FALSE,
        'description' => 'Boolean indicating whether the transaction is shippable.',
      ),
      'currency' => array(
        'type' => 'varchar',
        'length' => '3',
        'not null' => TRUE,
        'description' => 'ISO 4217 currency code.',
      ),
      'gross' => array(
        'type' => 'numeric',
        'not null' => TRUE,
        'default' => 0,
        'precision' => '10',
        'scale' => '2',
        'description' => 'Transaction gross amount.',
      ),
      'allocated' => array(
        'type' => 'numeric',
        'not null' => TRUE,
        'default' => 0,
        'precision' => '10',
        'scale' => '2',
        'description' => 'Total amount allocated to this transaction.',
      ),
      'balance' => array(
        'type' => 'numeric',
        'not null' => TRUE,
        'default' => 0,
        'precision' => '10',
        'scale' => '2',
        'description' => 'Total amount allocated to this transaction.',
      ),
      'created' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'description' => 'The Unix timestamp when the transaction was created.',
      ),
      'changed' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'description' => 'The Unix timestamp when the transaction was most recently changed.',
      ),
      'duedate' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'description' => 'The Unix timestamp the transaction is due to.',
      ),
      'token' => array(
        'type' => 'varchar',
        'length' => '42',
        'not null' => TRUE,
        'default' => '',
        'description' => '32 bit unique transaction token.',
      ),
      'additional' => array(
        'type' => 'text',
        'serialize' => TRUE,
        'description' => 'Additional serialized data.',
      ),
    ),
    'primary key' => array('txnid'),
    'indexes' => array(
      'ecid' => array('ecid'),
    ),
  );

  // Note: If this table is being altered by hook_schema_alter() be sure to
  // change the ec_address table as well.
  $schema['ec_transaction_address'] = array(
    'description' => 'Transaction address table.',
    'fields' => array(
      'txnid' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'description' => 'The {ec_transaction}.txnid this address is belongs to.',
      ),
      'type' => array(
        'type' => 'varchar',
        'length' => '16',
        'not null' => TRUE,
        'default' => '',
        'description' => 'Address type.',
      ),
      'fullname' => array(
        'type' => 'varchar',
        'length' => '32',
        'not null' => TRUE,
        'default' => '',
        'description' => 'Full name.',
      ),
      'firstname' => array(
        'type' => 'varchar',
        'length' => '32',
        'not null' => TRUE,
        'default' => '',
        'description' => 'First name.',
      ),
      'lastname' => array(
        'type' => 'varchar',
        'length' => '32',
        'not null' => TRUE,
        'default' => '',
        'description' => 'Last name.',
      ),
      'street1' => array(
        'type' => 'varchar',
        'length' => '64',
        'not null' => TRUE,
        'default' => '',
        'description' => 'Street address line 1.',
      ),
      'street2' => array(
        'type' => 'varchar',
        'length' => '64',
        'not null' => TRUE,
        'default' => '',
        'description' => 'Street address line 2.',
      ),
      'zip' => array(
        'type' => 'varchar',
        'length' => '10',
        'not null' => TRUE,
        'default' => '',
        'description' => 'Zip/postal code.',
      ),
      'city' => array(
        'type' => 'varchar',
        'length' => '32',
        'not null' => TRUE,
        'default' => '',
        'description' => 'City.',
      ),
      'state' => array(
        'type' => 'varchar',
        'length' => '32',
        'not null' => TRUE,
        'default' => '',
        'description' => 'State/province.',
      ),
      'country' => array(
        'type' => 'varchar',
        'length' => '2',
        'not null' => TRUE,
        'default' => '',
        'description' => 'Country code.',
      ),
    ),
    'primary key' => array('txnid', 'type'),
  );

  $schema['ec_transaction_product'] = array(
    'description' => 'Transaction product table.',
    'fields' => array(
      'txnid' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'description' => 'The {ec_transaction}.txnid this product belongs to.',
      ),
      'nid' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'description' => 'The {node}.nid of this product.',
      ),
      'vid' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'description' => 'The {node_revisions}.vid of this product.',
      ),
      'title' => array(
        'type' => 'varchar',
        'length' => '128',
        'not null' => FALSE,
        'description' => 'The product title/name.',
      ),
      'price' => array(
        'type' => 'numeric',
        'not null' => TRUE,
        'default' => 0,
        'precision' => '10',
        'scale' => '2',
        'description' => 'The price the product was sold.',
      ),
      'qty' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'description' => 'How many products of this kind.',
      ),
      'shippable' => array(
        'type' => 'int',
        'size' => 'tiny',
        'default' => 0,
        'description' => 'Boolean indicating whether the product is shippable.',
      ),
      'data' => array(
        'type' => 'text',
        'not null' => FALSE,
        'serialize' => TRUE,
        'description' => 'Additional serialized data.',
      ),
    ),
    'unique keys' => array(
      'txnid' => array('txnid', 'nid'),
    ),
    'indexes' => array(
      'txnid_2' => array('txnid'),
    ),
  );

  $schema['ec_transaction_misc'] = array(
    'description' => 'Miscellaneous transaction table.',
    'fields' => array(
      'txnid' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'description' => 'The {ec_transaction}.txnid this miscellaneous transaction belongs to.',
      ),
      'type' => array(
        'type' => 'varchar',
        'length' => '64',
        'not null' => TRUE,
        'default' => '',
        'description' => 'The type of this miscellaneous transaction.',
      ),
      'vid' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'description' => 'The {node_revisions}.vid of the product this miscellaneous transaction links to if any.',
      ),
      'misc_group' => array(
        'type' => 'varchar',
        'length' => 16,
        'not null' => TRUE,
        'default' => '',
        'description' => 'The group this miscellaneous transaction belongs to.',
      ),
      'description' => array(
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
        'default' => '',
        'description' => 'Miscellaneous transaction description.'
      ),
      'invisible' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'description' => 'Boolean indicating whether this miscellaneous transaction is shippable.',
      ),
      'displayonly' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'description' => 'Boolean indicating whether this miscellaneous transaction is display only.',
      ),
      'price' => array(
        'type' => 'numeric',
        'default' => 0,
        'precision' => '10',
        'scale' => '2',
        'description' => 'The price/amount of this miscellaneous transaction.',
      ),
      'qty' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 1,
        'description' => 'How many miscellaneous transaction of this kind.',
      ),
      'weight' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'description' => 'The weight of this miscellaneous transaction.',
      )
    ),
    'primary key' => array('txnid', 'type', 'vid'),
  );
  
  $schema['ec_transaction_fields'] = array(
    'fields' => array(
      'txnid' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => 'Transaction identifier.',
      ),
      'name' => array(
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
        'description' => t('Unique identifier for each charge'),
      ),
      'value' => array(
        'type' => 'text',
        'not null' => TRUE,
        'default' => '',
      ),
    ),
    'primary key' => array('txnid', 'name'),
  );

  return $schema;
}

/**
 * @file
 * All incremental database updates performed between Drupal releases.
 */

/**
 * Check that the ec_anon updates have past 6400
 */
function ec_store_update_5400() {
  $ret = array();
  $version = drupal_get_installed_schema_version('ec_anon', TRUE);
  
  if ($version < 6400) {
    $ret['#abort'] = array('success' => FALSE, 'query' => t('Waiting until eC Anonymous has completed the initial upgrade'));
    return $ret;
  }  
  return $ret;
}

function ec_store_update_5401() {
  $ret = array();
  
  module_rebuild_cache();
  drupal_install_modules(array('ec_customer'));
  drupal_get_schema(NULL, TRUE);
  drupal_load('module', 'ec_customer');
  $ret[] = update_sql('ALTER TABLE {ec_transaction} ADD ecid INT UNSIGNED NOT NULL AFTER uid');
  $ret[] = update_sql('ALTER TABLE {ec_transaction} ADD INDEX (ecid)');

  return $ret;
}

function ec_store_update_5402(&$sandbox) {
  $ret = array();
  
  if (!isset($sandbox['count'])) {
    $sandbox['count'] = 0;
    $sandbox['total'] =  db_result(db_query('SELECT COUNT(*) FROM {ec_transaction} WHERE uid > 0 AND ecid = 0 GROUP BY uid'));
  }

  // Do work.
  $count = 0;
  $result = db_query('SELECT uid FROM {ec_transaction} WHERE uid > 0 AND ecid = 0 GROUP BY uid');
  while (($txn = db_fetch_object($result))) {
    $customer = ec_customer_get_customer(array('type' => 'user', 'exid' => $txn->uid));
    $ret[] = array('success' => TRUE, 'query' => print_r($customer, TRUE));
    if (empty($customer->ecid)) {
      $customer = ec_customer_insert($customer);
      $ret[] = array('success' => TRUE, 'query' => print_r($customer, TRUE));
    }
    $ret[] = update_sql('UPDATE {ec_transaction} SET ecid = ' . $customer->ecid . ' WHERE uid = '. $txn->uid);
    ++$count;
    if (timer_read('page') > 4000) {
      break;
    }
  }
  $sandbox['count'] += $count;

  // See if we are done.
  if ($sandbox['count'] < $sandbox['total']) {
    // Not done yet. Return the progress.
    return array('#finished' => $sandbox['count'] / $sandbox['total']);
  }
  else {
    // Done. Clean up and indicate we're finished.
    $ret['#finished'] = 1;
    return $ret;
  }
}

function ec_store_update_5403() {
  if (!isset($sandbox['count'])) {
    $sandbox['count'] = 0;
    $sandbox['total'] = db_result(db_query('SELECT COUNT(*) FROM {ec_transaction} WHERE uid > 0 AND ecid = 0 GROUP BY uid'));
  }

  // Do work.
  $count = 0;
  $result = db_query('SELECT txnid, mail FROM {ec_transaction} WHERE uid = 0 AND ecid = 0 GROUP BY mail');
  while (($txn = db_fetch_object($result))) {
    $customer = ec_customer_get_customer(array('type' => 'anonymous'));
    if (empty($customer->ecid)) {
      $customer = ec_customer_insert($customer);
    }
    $ret[] = update_sql('UPDATE {ec_transaction} SET ecid = ' . $customer->ecid . ' WHERE uid = 0 AND mail = \'' . $txn->mail . '\'');
    unset($_SESSION['ec_customer_anonymous']);
    ++$count;
    if (timer_read('page') > 4000) {
      break;
    }
  }
  $sandbox['count'] += $count;

  // See if we are done.
  if ($sandbox['count'] < $sandbox['total'] && $count > 0) {
    // Not done yet. Return the progress.
    return array('#finished' => $sandbox['count'] / $sandbox['total']);
  }
  else {
    // Done. Clean up and indicate we're finished.
    $ret['#finished'] = 1;
    return $ret;
  }
}

function ec_store_update_5404() {
  $ret = array();

  db_add_field($ret, 'ec_transaction_address', 'fullname', array(
    'type' => 'varchar',
    'length' => '32',
    'not null' => TRUE,
    'default' => ''
  ));

  return $ret;
}

function ec_store_update_5405() {
  $ret = array();

  db_add_field($ret, 'ec_transaction', 'currency', array(
    'type' => 'varchar',
    'length' => '3',
    'not null' => TRUE,
    'default' => ''
  ));

  module_rebuild_cache();
  drupal_install_modules(array('ec_receipt'));
  return $ret;
}

function ec_store_update_5406() {
  $dc = variable_get('ec_default_currency', NULL);
  if (empty($dc)) {
    $ret['#abort'] = array('success' => FALSE, 'query' => t('Default currency needs to be set before the update can continue. Use the <a href="!link">Receipt type settings</a> to set the default currenecy', array('!link' => url('admin/ecsettings/rtypes/settings'))));
  }
  else {
    $ret[] = update_sql("UPDATE {ec_transaction} SET currency = '{$dc}' WHERE currency = ''");
  }
  return $ret;
}

function ec_store_update_5407() {
  // Destination after adding a product to the cart can now be explicitly set
  variable_del('product_cart_is_destination');
  return array();
}

function ec_store_update_5408() {
  $ret = array();
  db_add_field($ret, 'ec_transaction', 'allocation', array(
    'type' => 'int',
    'unsigned' => TRUE,
    'not null' => TRUE,
    'default' => 0
  ));

  return $ret;
}

/**
 * Enable the eC Common. It holds all main function to e-Commerce
 *
 * @note Skip 5409 as this was a duplicate
 */
function ec_store_update_5410() {
  module_rebuild_cache();
  drupal_install_modules(array('ec_common'));
  return array();
}

/**
 * Enable the eC Charge module to all that use the old Flexicharge module.
 */
function ec_store_update_5411() {
  $updates = array();
  if (module_exists('flexicharge')) {
    $updates[] = update_sql('ALTER TABLE {ec_flexicharge} RENAME TO {ec_charge}');
    $updates[] = update_sql('ALTER TABLE {ec_flexicharge_product} RENAME TO {ec_charge_product}');
    module_rebuild_cache();
    module_enable(array('ec_charge'));
    module_disable(array('flexicharge'));
    module_rebuild_cache();
  }
  if (variable_get('product_catalog', 'product') != 'product') {
    variable_set('ec_goto_cart_empty', variable_get('product_catalog', 'product'));
    variable_del('product_catalog');
  }
  return $updates;
}

/**
 * Enable the eC File module to all that use the old File module.
 */
function ec_store_update_5412() {
  $updates = array();
  if (module_exists('file')) {
    $updates[] = update_sql('ALTER TABLE {ec_product_file} RENAME TO {ec_file}');
    module_rebuild_cache();
    module_enable(array('ec_file'));
    module_disable(array('file'));
    module_rebuild_cache();
  }
  return $updates;
}

/**
 * United Kingdom should use the ISO country code GB (not UK).
 */
function ec_store_update_5413() {
  $updates = array();
  $updates[] = update_sql("UPDATE {ec_transaction_address} SET country = 'gb' WHERE country = 'uk'");
  return $updates;
}

/**
 * All country codes should be upper case
 */
function ec_store_update_5414() {
  $updates = array();
  $updates[] = update_sql("UPDATE {ec_transaction_address} SET country = upper(country)");
  return $updates;
}

/**
 * Remove uid from ec_transaction
 */
function ec_store_update_5415() {
  $ret[] = update_sql('ALTER TABLE {ec_transaction} DROP uid');
  return $ret;
}

/**
 * Move all workflow statuses to a new table.
 */
function ec_store_update_6401() {
  $ret = array();
  $schema['ec_workflow_statuses'] = array(
    'fields' => array(
      'workflow' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'description' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE
      ),
      'help' => array(
        'type' => 'text'
      ),
      'type' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'weight' => array(
        'type' => 'int',
        'not null' => TRUE,
      ),
    ),
  );

  db_create_table($ret, 'ec_workflow_statuses', $schema['ec_workflow_statuses']);

  $workflow = array(
    array('workflow' => 1, 'description' => 'received', 'type' => 0, 'weight' => 1),
    array('workflow' => 2, 'description' => 'invoiced', 'type' => 0, 'weight' => 2),
    array('workflow' => 10, 'description' => 'in picking', 'type' => 0, 'weight' => 3),
    array('workflow' => 8, 'description' => 'packaged', 'type' => 0, 'weight' => 4),
    array('workflow' => 3, 'description' => 'shipped', 'type' => 0, 'weight' => 5),
    array('workflow' => 4, 'description' => 'awaiting customer response', 'type' => 1, 'weight' => 6),
    array('workflow' => 5, 'description' => 'canceled', 'type' => 2, 'weight' => 7),
    array('workflow' => 6, 'description' => 'completed', 'type' => 1, 'weight' => 8),
    array('workflow' => 7, 'description' => 'security violation', 'type' => 0, 'weight' => 9),
  );

  foreach ($workflow as $item) {
    $ret[] = update_sql("INSERT INTO {ec_workflow_statuses} (workflow, description, type, weight) VALUES (". $item['workflow'] .", '". $item['description'] ."', ". $item['type'] .", ". $item['weight'] .")");
  }

  db_change_field($ret, 'ec_workflow_statuses', 'workflow', 'workflow', array(
    'type' => 'serial',
    'unsigned' => TRUE,
    'not null' => TRUE,
    'disp-width' => '10'
  ),
  array(
    'primary key' => array('workflow')
  ));

  return $ret;
}

/**
 * Create new shippable fields on ec_transaction and ec_transaction_product().
 */
function ec_store_update_6402() {
  $ret = array();

  db_add_field($ret, 'ec_transaction', 'shippable', array(
    'type' => 'int',
    'size' => 'tiny',
    'default' => 0
  ));
  db_add_field($ret, 'ec_transaction_product', 'shippable', array(
    'type' => 'int',
    'size' => 'tiny',
    'default' => 0
  ));

  return $ret;
}

/**
 * Update all current transactions with the new shippable fields.
 */
function ec_store_update_6403(&$sandbox) {
  if (empty($sandbox)) {
    $sandbox['max_txn'] = db_result(db_query('SELECT MAX(txnid) FROM {ec_transaction}'));
    $sandbox['last_txn'] = 0;
  }

  $result = db_query('SELECT txnid from {ec_transaction} WHERE txnid > %d ORDER BY txnid', $sandbox['last_txn']);

  while ($data = db_fetch_object($result)) {
    if ($txn = ec_store_transaction_load($data->txnid)) {
      $txn->shippable = 0;
      foreach ($txn->items as $nid => $product) {
        if (ec_product_is_shippable($product->vid)) {
          $txn->shippable = 1;
          $txn->items[$nid]->shippable = 1;
        }
      }
      ec_store_transaction_save($txn);
      $sandbox['last_txn'] = $txn->txnid;
    }


    if (timer_read('batch_processing') > 1000) {
      break;
    }
  }
  if ($sandbox['last_txn'] == $sandbox['max_txn']) {
    $ret['#finished'] = 1;
  }
  else {
    $ret['#finished'] = $sandbox['last_txn']/$sandbox['max_txn'];
  }

  return $ret;
}

/**
 * Add action for shipping orders
 */
function ec_store_update_6404() {
  actions_save('ec_store_action_set_workflow', 'transaction', array('workflow' => '3'), 'Ship Orders');
  return array();
}

/**
 * extend the ec_transaction_misc.type field to 64 chars
 */
function ec_store_update_6405() {
  $ret = array();

  db_change_field($ret, 'ec_transaction_misc', 'type', 'type', array(
    'type' => 'varchar',
    'length' => '64',
    'not null' => TRUE,
    'default' => ''
  ));

  return $ret;
}

function ec_store_update_6406() {
  $ret = array();

  db_add_field($ret, 'ec_transaction_misc', 'displayonly', array(
    'type' => 'int',
    'not null' => TRUE,
    'default' => 0,
  ));
  db_change_field($ret, 'ec_transaction_misc', 'price', 'price', array(
    'type' => 'numeric',
    'precision' => '10',
    'scale' => '2'
  ));

  return $ret;
}

function ec_store_update_6407() {
  $ret = array();

  db_add_field($ret, 'ec_transaction', 'additional', array(
    'type' => 'text',
    'serialize' => TRUE
  ));

  return $ret;
}

function ec_store_update_6408() {
  $ret = array();

  db_change_field($ret, 'ec_transaction', 'allocation', 'allocation', array(
    'type' => 'int',
    'unsigned' => TRUE,
    'not null' => TRUE,
    'default' => 0
  ));

  return $ret;
}

/**
 * Add group field to ec_transaction_misc
 */
function ec_store_update_6409() {
  $ret = array();

  db_add_field($ret, 'ec_transaction_misc', 'misc_group',
    array(
      'type' => 'varchar',
      'length' => 16,
      'not null' => TRUE,
      'default' => '',
    )
  );

  return $ret;
}

/**
 * Remove ec_goto_cart_change variable which is not being used anymore.
 */
function ec_store_update_6410() {
  variable_del('ec_goto_cart_change');

  return array();
}

/**
 * Populate the allocation column from the old payment_status column.
 */
function ec_store_update_6411() {
  $ret[] = update_sql("UPDATE {ec_transaction} SET allocation = payment_status");
  db_drop_field($ret, 'ec_transaction', 'payment_status');
  return $ret;
}

/**
 * Add Allocated and balance coloumns
 */
function ec_store_update_6412() {
  $ret = array();

  db_add_field($ret, 'ec_transaction', 'allocated', array(
    'type' => 'numeric',
    'not null' => TRUE,
    'default' => 0,
    'precision' => '10',
    'scale' => '2'
  ));

  db_add_field($ret, 'ec_transaction', 'balance', array(
    'type' => 'numeric',
    'not null' => TRUE,
    'default' => 0,
    'precision' => '10',
    'scale' => '2'
  ));

  $ret[] = update_sql("UPDATE {ec_transaction} t SET t.allocated = (SELECT SUM(a.amount) FROM {ec_receipt_allocation} a WHERE a.type = 'transaction' AND CAST(a.etid AS UNSIGNED) = t.txnid)");
  $ret[] = update_sql('UPDATE {ec_transaction} SET balance = (gross - allocated)');
  return $ret;
}

/**
 * Create tables required for fields
 */
function ec_store_update_6413() {
  $ret = array();
  
  db_create_table($ret, 'ec_fields', array(
    'description' => 'Configuration of fields which will appear on checkout',
    'fields' => array(
      'name' => array(
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
        'description' => t('Unique identifier for each charge'),
      ),
      'field_type' => array(
        'type' => 'varchar',
        'length' => 16,
        'not null' => TRUE,
        'default' => '',
        'description' => 'Type of field to be created.',
      ),
      'field_title' => array(
        'type' => 'varchar',
        'length' => 128,
        'not null' => TRUE,
        'default' => '',
        'description' => 'Title which is shown on the checkout.',
      ),
      'field_description' => array(
        'type' => 'text',
        'size' => 'medium',
        'default' => '',
        'not null' => TRUE,
        'description' => 'description to display on the field created.',
      ),
      'field_error' => array(
        'type' => 'text',
        'size' => 'medium',
        'default' => '',
        'not null' => TRUE,
        'description' => 'Error message to display when the field is not used.',
      ), 
      'field_settings' => array(
        'type' => 'text',
        'serialize' => TRUE,
      ),
      'weight' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'description' => t('Weight of the charge to determine the order'),
      ),
      'enabled' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'size' => 'tiny',
        'description' => t('Set the charged as enabled.'),
      ),
    ),
    'primary key' => array('name'),
    'export' => array(
      'key' => 'name',
      'key name' => 'Name',
      'can disable' => TRUE,
      'status' => 'enabled',
    ),
  ));
  
  db_create_table($ret, 'ec_transaction_fields', array(
    'fields' => array(
      'txnid' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => 'Transaction identifier.',
      ),
      'name' => array(
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
        'description' => t('Unique identifier for each charge'),
      ),
      'value' => array(
        'type' => 'text',
        'not null' => TRUE,
        'default' => '',
      ),
    ),
    'primary key' => array('txnid', 'name'),
  ));
  return $ret;
}