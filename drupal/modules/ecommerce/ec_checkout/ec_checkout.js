// $Id: ec_checkout.js,v 1.1.2.4 2009/10/04 02:48:46 gordon Exp $

Drupal.behaviors.ecCheckout = function() {
  $('#ec-checkout-form input, #ec-checkout-form select').not('.ignore-update').change(function() {
    if (!$('#edit-order').attr('disabled')) {
      $('#edit-order').attr('disabled', true);
      $('div.tabs').after('<p class="warning">* '+ Drupal.t('Details have been changed. Please submit your order below.') + '</p>');

      }
      $('.update').attr('style', 'display: none;');
      $('#edit-update').attr('style', 'display: inline;');
      $('#amazon-frame').attr('style', 'display: none;');
      $.cookie('update', null);
  });


};
