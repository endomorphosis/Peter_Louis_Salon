// $Id: ec_customer.checkout.js,v 1.1.2.2 2009/06/26 21:45:01 darrenoh Exp $

Drupal.hideShippingAddress = function(hide) {
  if (hide) {
    $('.customer-addresses').removeClass('shippable');
    $('#customer-address-shipping').hide();
  }
  else {
    $('.customer-addresses').addClass('shippable');
    $('#customer-address-shipping').show();
  }
};

Drupal.behaviors.ecCustomer = function() {
  if ($('#ec-customer-use-for-shipping:checked').size()) {
    Drupal.hideShippingAddress(true);
  }
  else {
    Drupal.hideShippingAddress(false);
  }
  $('#ec-customer-use-for-shipping').click(function() {
    if (this.checked) {
      Drupal.hideShippingAddress(true);
    }
    else {
      Drupal.hideShippingAddress(false);
    }
  });
};
