<?php

/**
 * @file
 * Install and uninstall routines, incremental database updates and database
 * schema.
 */

/**
 * e-Commerce cart module schema
 */
function ec_cart_install() {
  $versions = drupal_get_installed_schema_version('cart', FALSE, TRUE);
  if (!isset($versions['cart']) || $versions['cart'] == SCHEMA_UNINSTALLED) {
    drupal_install_schema('ec_cart');

    if (!db_column_exists('ec_product', 'hide_cart_link')) {
      $ret = array();
      db_add_field($ret, 'ec_product', 'hide_cart_link', array('type' => 'int', 'unsigned' => TRUE, 'not null' => TRUE, 'default' => 0, 'disp-width' => '2'));
    }
  }
}

/**
 * Implementation of hook_enable().
 */
function ec_cart_enable() {
  $versions = drupal_get_installed_schema_version('cart', FALSE, TRUE);
  if (isset($versions['cart']) && $versions['cart'] != SCHEMA_UNINSTALLED) {
    // Upgrade from e-Commerce 3 cart module.
    drupal_set_installed_schema_version('ec_cart', $versions['cart']);
    drupal_set_installed_schema_version('cart', SCHEMA_UNINSTALLED);
  }

  // Reset the the ptypes cache so it loads up the new ptypes correctly
  ec_product_get_types('types', NULL, TRUE);
}

/**
 * Implementation of hook_uninstall().
 */
function ec_cart_uninstall() {
  drupal_uninstall_schema('ec_cart');
  variable_del('ec_cart_empty_hide');

  if (db_column_exists('ec_product', 'hide_cart_link')) {
    $ret = array();
    db_drop_field($ret, 'ec_product', 'hide_cart_link');
  }
}

/**
 * Implementation of hook_schema().
 */
function ec_cart_schema() {
  $schema = array();

  $schema['ec_cart'] = array(
    'fields' => array(
      'cookie_id' => array('type' => 'varchar', 'length' => '40', 'not null' => FALSE),
      'nid' => array('type' => 'int', 'not null' => TRUE, 'default' => 0, 'disp-width' => '10'),
      'qty' => array('type' => 'int', 'not null' => TRUE, 'default' => 0, 'disp-width' => '10'),
      'changed' => array('type' => 'int', 'not null' => TRUE, 'default' => 0, 'disp-width' => '11'),
      'data' => array('type' => 'text', 'not null' => FALSE, 'serialize' => TRUE)),
    'indexes' => array(
      'cookie_id' => array('cookie_id')),
  );

  return $schema;
}

/**
 * Implementation of hook_schema_alter().
 */
function ec_cart_schema_alter(&$schema) {
  $schema['ec_product']['fields']['hide_cart_link'] = array(
    'type' => 'int',
    'unsigned' => TRUE,
    'not null' => TRUE,
    'default' => 0,
    'disp-width' => '2'
  );
}

/**
 * Check that the ec_anon updates have past 6400
 */
function ec_cart_update_5400() {
  $ret = array();
  $version = drupal_get_installed_schema_version('ec_anon', TRUE);
  
  if ($version < 6400) {
    $ret['#abort'] = array('success' => FALSE, 'query' => t('Waiting until eC Anonymous has completed the initial upgrade'));
    return $ret;
  }
}

/**
 * Implementation of hook_update().
 *
 * Change the the
 */
function ec_cart_update_5401() {
  // There is no ANSI solution for this...
  if ($GLOBALS['db_type'] == 'mysql' or $GLOBALS['db_type'] == 'mysqli') {
    $update[] = update_sql('ALTER TABLE {ec_cart} MODIFY COLUMN cookie_id VARCHAR(40)');
  }
  else {
    $update[] = update_sql('ALTER TABLE {ec_cart} ALTER COLUMN cookie_id TYPE VARCHAR(40)');
  }
  return $update;
}

/**
 * Implementation of hook_update().
 *
 * Enable the new ec_checkout module which is required by cart
 */
function ec_cart_update_5402() {
  module_rebuild_cache();
  module_enable(array('ec_checkout'));

  return array();
}

/**
 * Implementation of hook_update_n().
 *
 * Insert hide_cart_link into ec_product and update cart block.
 */
function ec_cart_update_6400() {
  $ret = array();

  if (!db_column_exists('ec_product', 'hide_cart_link')) {
    db_add_field($ret, 'ec_product', 'hide_cart_link', array('type' => 'int', 'unsigned' => TRUE, 'not null' => TRUE, 'default' => 0, 'disp-width' => '2'));
  }
  $ret[] = update_sql("UPDATE {blocks} SET module = 'ec_cart' WHERE module = 'cart'");

  return $ret;
}
