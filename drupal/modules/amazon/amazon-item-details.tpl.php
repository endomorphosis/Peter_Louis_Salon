<div class="<?php print $classes; ?>">
<?php print $smallimage; ?>
<div><strong><?php print l($title, $detailpageurl); ?></strong></div>
<div><strong><?php print t('Manufacturer'); ?>:</strong> <?php print $manufacturer; ?></div>
<div><strong><?php print t('Part Number'); ?>:</strong> <?php print $mpn; ?></div>
<div><strong><?php print t('Price'); ?>:</strong> <?php print $listpriceformattedprice; ?></div>
</div>
