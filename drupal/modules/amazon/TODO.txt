$Id: TODO.txt,v 1.2.2.1 2008/05/31 04:22:58 eaton Exp $

API
* Handle update/delete cases cleanly (currently inefficient)

Search
* Paged searching
* Advanced search options
* Properly themed search results

Theming
* Document variables

Functionality
* Wish lists (separate module?
* Related Books block for nodes (separate module?)
* Reading List module (separate module?)
