<?php
// $Id: amazon-views-view-row-item.tpl.php,v 1.1.2.1 2008/06/13 13:51:10 eaton Exp $
/**
 * @file amazon-views-view-row-item.tpl.php
 * Default simple view template to display a single Amazon item.
 *
 *
 * @ingroup views_templates
 */
?>
<?php print $content; ?>
