<div class="<?php print $classes; ?>">
<?php print $smallimage; ?>
<div><strong><?php print l($title, $detailpageurl); ?></strong> (<?php print $hardwareplatform; ?>)</div>
<div><strong><?php print t('Genre'); ?>:</strong> <?php print $genre; ?></div>
<div><strong><?php print t('Age rating'); ?>:</strong> <?php print $esrbagerating; ?></div>
<div><strong><?php print t('Price'); ?>:</strong> <?php print $listpriceformattedprice; ?></div>
</div>
