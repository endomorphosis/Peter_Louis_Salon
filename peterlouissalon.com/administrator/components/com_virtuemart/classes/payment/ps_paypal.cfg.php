<?php
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' ); 

define ('PAYPAL_DEBUG', '1');
define ('PAYPAL_EMAIL', 'websitepayment-pro@sandy.com');
define ('PAYPAL_VERIFIED_ONLY', '0');
define ('PAYPAL_VERIFIED_STATUS', 'C');
define ('PAYPAL_PENDING_STATUS', 'P');
define ('PAYPAL_INVALID_STATUS', 'X');
?>