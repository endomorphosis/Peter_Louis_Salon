<?php
/*------------------------------------------------------------------------
# vm_sef - Search Engine Friendly URL's for Virtuemart
# ------------------------------------------------------------------------
# author    Jeremy Magne
# copyright Copyright (C) 2010 Daycounts.com. All Rights Reserved.
# Websites: http://www.daycounts.com
# Technical Support: http://www.daycounts.com/en/contact/
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
-------------------------------------------------------------------------*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport('joomla.application.component.controller');
jimport('joomla.application.helper');

require_once (JPATH_COMPONENT.DS.'classes'.DS.'config.php');
require_once (JPATH_COMPONENT.DS.'classes'.DS.'helper.php');
require_once(JPATH_SITE.DS.'components'.DS.'com_virtuemart'.DS.'virtuemart_parser.php' );

/**
 * @package		Joomla
 * @subpackage	Config
 */
class VmSefAliasController extends JController
{
	/**
	 * Custom Constructor
	 */
	function __construct( $default = array())
	{
		parent::__construct( $default );
	}

	function display( )	{
		JRequest::setVar( 'hidemainmenu',1 );
		JRequest::setVar( 'edit', false  );
		global $option;

		$view = JRequest::getCmd('view');
		if(empty($view)) {
			JRequest::setVar('view', 'alias');
			$view='alias';
		};

		parent::display();
	}

	function save()	{
		$mainframe = & JFactory::getApplication('site');
		// Check for request forgeries
		JRequest::checkToken() or jexit( 'Invalid Token' );

		$db		=& JFactory::getDBO();
		$type = JRequest::getVar( 'type', -1 );
		$row	=& JTable::getInstance($type, 'Table');
		$post	= JRequest::get( 'post' );
		switch ($type) {
			case 'product':
				$post['product_id'] = $post['srcid'];
				break;
			case 'category':
				$post['category_id'] = $post['srcid'];
				break;
			case 'manufacturer':
				$post['manufacturer_id'] = $post['srcid'];
				break;
			case 'vendor':
				$post['vendor_id'] = $post['srcid'];
				break;
			case 'page':
				$post['code'] = $post['srcid'];
				break;
		}
		if (!$row->bind( $post )) {
			JError::raiseError(500, $row->getError() );
		}
		$isNew = ($row->id == 0);

		if (!$row->check())	{
			JError::raiseError(500, $row->getError() );
		}

		if (!$row->store())	{
			JError::raiseError(500, $row->getError() );
		} else {
			$_SESSION['sef_modified'] = 1;
		}
		
		global $option;
		$msg = JText::_( 'Alias saved' );
		$link = 'index.php?option='.$option.'&tmpl=component&view=alias&task=edit&type='.$type.'&id='. $row->id .'';
		$mainframe->redirect($link, $msg);
	}
	
	function remove() {
		$mainframe = & JFactory::getApplication('site');
		// Check for request forgeries
		JRequest::checkToken() or jexit( 'Invalid Token' );

		$db		=& JFactory::getDBO();
		$type = JRequest::getVar( 'type', -1 );
		$row	=& JTable::getInstance($type, 'Table');
		$post	= JRequest::get( 'post' );
		switch ($type) {
			case 'product':
				$post['product_id'] = $post['srcid'];
				break;
			case 'category':
				$post['category_id'] = $post['srcid'];
				break;
			case 'manufacturer':
				$post['manufacturer_id'] = $post['srcid'];
				break;
			case 'vendor':
				$post['vendor_id'] = $post['srcid'];
				break;
			case 'page':
				$post['code'] = $post['srcid'];
				break;
		}
		if (!$row->bind( $post )) {
			JError::raiseError(500, $row->getError() );
		}

		if (!$row->delete())	{
			JError::raiseError(500, $row->getError() );
		} else {
			$_SESSION['sef_modified'] = 1;
		}
		
		global $option;
		$msg = JText::_( 'Alias removed' );
		$link = 'index.php?option='.$option.'&tmpl=component&view=alias&task=edit&type='.$type.'&id='. $row->id .'';
		$mainframe->redirect($link, $msg);
	}

	function generate() {
		$original = JRequest::getVar( 'original');
		$SefHelper = new VmSefHelper();
		echo $SefHelper->generateAlias($original);
		die();
	}
	

}