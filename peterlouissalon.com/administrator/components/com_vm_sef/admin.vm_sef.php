<?php
/*------------------------------------------------------------------------
# vm_sef - Search Engine Friendly URL's for Virtuemart
# ------------------------------------------------------------------------
# author    Jeremy Magne
# copyright Copyright (C) 2010 Daycounts.com. All Rights Reserved.
# Websites: http://www.daycounts.com
# Technical Support: http://www.daycounts.com/en/contact/
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# See http://daycounts.com/fr/component/content/article/7 for details
-------------------------------------------------------------------------*/

// no direct access
defined('_JEXEC') or die('Restricted access');

require_once(JPATH_SITE.DS.'components'.DS.'com_virtuemart'.DS.'virtuemart_parser.php' );

// Set the table directory
JTable::addIncludePath( JPATH_COMPONENT.DS.'tables' );
//Load the appropriate controller
$view = JRequest::getCmd('view');
$task = JRequest::getCmd('task');

global $option;
JHTML::stylesheet( 'admin.css', 'administrator/components/'.$option.'/assets/css/');
JHTML::script( 'vm_sef.js', 'administrator/components/'.$option.'/assets/js/' );
JHTML::_('behavior.tooltip');

//Check if Joomla SEF is activated
$JConfig =& JFactory::getConfig();
if ($JConfig->getValue('sef')==0 && $view != 'alias') {
	JError::raiseNotice( 100, JText::_('Joomla SEF is not activated. URL will not be rewrote until you activate it in Joomla Configuration') );
}

if (isset($_SESSION['sef_modified']) && $_SESSION['sef_modified'] == 1 && $view !='alias') {
	$db 	=& JFactory::getDBO();
	$query = 'SELECT COUNT(*) FROM #__vm_sef_urls';
	$db->setQuery( $query );
	$total = $db->loadResult();
	if ($total>0) {
		$text = JText::_('SEF_CHANGED');
		$text .= '&nbsp;&nbsp;<input type="button" onclick="submitbutton(\'purge\')" value="'.JText::_('Purge cache').'">';
		JError::raiseNotice( 100, $text );
	} else {
		$_SESSION['sef_modified'] = 0;
	}
}

if ($task == 'purge') {
	$view = 'cache';
}

switch ($view) {
	case 'config':
		require_once( JPATH_COMPONENT.DS.'controllers'.DS.'config.php' );
		$controller = new VmSefConfigController();
		break;
	case 'categories':
		require_once( JPATH_COMPONENT.DS.'controllers'.DS.'categories.php' );
		$controller = new VmSefCategoriesController();
		break;
	case 'manufacturers':
		require_once( JPATH_COMPONENT.DS.'controllers'.DS.'manufacturers.php' );
		$controller = new VmSefManufacturersController();
		break;
	case 'vendors':
		require_once( JPATH_COMPONENT.DS.'controllers'.DS.'vendors.php' );
		$controller = new VmSefVendorsController();
		break;
	case 'pages':
		require_once( JPATH_COMPONENT.DS.'controllers'.DS.'pages.php' );
		$controller = new VmSefPagesController();
		break;
	case 'alias':
		require_once( JPATH_COMPONENT.DS.'controllers'.DS.'alias.php' );
		$controller = new VmSefAliasController();
		break;
	case 'cache':
		require_once( JPATH_COMPONENT.DS.'controllers'.DS.'cache.php' );
		$controller = new VmSefCacheController();
		break;
	case 'products':
	default:
		require_once( JPATH_COMPONENT.DS.'controllers'.DS.'products.php' );
		$controller = new VmSefProductsController();
}

$controller->execute( JRequest::getWord( 'task' ) );


?>