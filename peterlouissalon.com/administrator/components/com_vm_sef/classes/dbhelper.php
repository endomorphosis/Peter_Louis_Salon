<?php
/*------------------------------------------------------------------------
# vm_containers - products containers for Virtuemart
# ------------------------------------------------------------------------
# author    Jeremy Magne
# copyright Copyright (C) 2010 Daycounts.com. All Rights Reserved.
# Websites: http://www.daycounts.com
# Technical Support: http://www.daycounts.com/en/contact/
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
-------------------------------------------------------------------------*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

class SefDbHelper {

	function ConvertToBoolean(&$errorMsg, $table, $column) {
		$db				=& JFactory::getDBO();
		$query = "UPDATE ".$table. " SET ".$db->nameQuote($column)."='1' WHERE ".$db->nameQuote($column)."='Y'";
		$db->setQuery( $query );
		if (!$result = $db->query()){return false;}
		$result = $this->AlterColumnIfExists($errorMsg, $table, $column, $column, $attributes = "tinyint(1) NOT NULL DEFAULT '0'" );
		return $result;
	}

	function AlterColumnIfExists(&$errorMsg, $table, $old_column, $new_column, $attributes = "INT( 11 ) NOT NULL DEFAULT '0'" ) {
		$db				=& JFactory::getDBO();
		$columnExists 	= false;

		$query = 'SHOW COLUMNS FROM '.$table;
		$db->setQuery( $query );
		if (!$result = $db->query()){return false;}
		$columnData = $db->loadObjectList();
		
		foreach ($columnData as $valueColumn) {
			if ($valueColumn->Field == $old_column) {
				$columnExists = true;
				break;
			}
		}

		if ($columnExists) {
			$query = 'ALTER TABLE '.$db->nameQuote($table).' CHANGE '.$db->nameQuote($old_column).' '.$new_column.' '.$attributes.';';
			$db->setQuery( $query );
			if (!$result = $db->query()){return false;}
			return true;
		}
		
		return false;
	}

	function DropColumnIfExists(&$errorMsg, $table, $column ) {
		$db				=& JFactory::getDBO();
		$columnExists 	= false;

		$query = 'SHOW COLUMNS FROM '.$table;
		$db->setQuery( $query );
		if (!$result = $db->query()){return false;}
		$columnData = $db->loadObjectList();
		
		foreach ($columnData as $valueColumn) {
			if ($valueColumn->Field == $column) {
				$columnExists = true;
				break;
			}
		}

		if ($columnExists) {
			$query = 'ALTER TABLE '.$db->nameQuote($table).' DROP '.$db->nameQuote($column).';';
			$db->setQuery( $query );
			if (!$result = $db->query()){return false;}
			return true;
		}
		
		return false;
	}

	function AddColumnIfNotExists(&$errorMsg, $table, $column, $attributes = "INT( 11 ) NOT NULL DEFAULT '0'", $after = '' ) {
		
		$db				=& JFactory::getDBO();
		$columnExists 	= false;

		$query = 'SHOW COLUMNS FROM '.$table;
		$db->setQuery( $query );
		if (!$result = $db->query()){return false;}
		$columnData = $db->loadObjectList();
		
		foreach ($columnData as $valueColumn) {
			if ($valueColumn->Field == $column) {
				$columnExists = true;
				break;
			}
		}
		
		if (!$columnExists) {
			if ($after != '') {
				$query = 'ALTER TABLE '.$db->nameQuote($table).' ADD '.$db->nameQuote($column).' '.$attributes.' AFTER '.$db->nameQuote($after).';';
			} else {
				$query = 'ALTER TABLE '.$db->nameQuote($table).' ADD '.$db->nameQuote($column).' '.$attributes.';';
			}
			$db->setQuery( $query );
			if (!$result = $db->query()){return false;}
			$errorMsg = 'notexistcreated';
		}
		
		return true;
	}



}

?>