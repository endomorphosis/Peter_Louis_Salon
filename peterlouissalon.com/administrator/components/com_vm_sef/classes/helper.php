<?php
/*------------------------------------------------------------------------
# vm_sef - Search Engine Friendly URL's for Virtuemart
# ------------------------------------------------------------------------
# author    Jeremy Magne
# copyright Copyright (C) 2010 Daycounts.com. All Rights Reserved.
# Websites: http://www.daycounts.com
# Technical Support: http://www.daycounts.com/en/contact/
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
-------------------------------------------------------------------------*/

defined( '_JEXEC' ) or die( 'Restricted access' );

require_once (JPATH_ADMINISTRATOR.DS."components".DS."com_vm_sef".DS.'classes'.DS.'config.php');

class VmSefHelper {

	var $VmSefConfig;

	function __construct() {
		$this->VmSefConfig = new VmSefConfig();
	}

	function generateAlias($original) {
		jimport('joomla.filter.output');

		//$string = htmlentities(utf8_decode($string));
		if ($this->VmSefConfig->transliterate_search && $this->VmSefConfig->transliterate_replace) {
			$translit_search = explode(',',$this->VmSefConfig->transliterate_search);
			for ($i=0; $i < count($translit_search); $i++) {
				$translit_search[$i] = '/'.$translit_search[$i].'/';
			}
			$translit_replace = explode(',',$this->VmSefConfig->transliterate_replace);
			$original = preg_replace($translit_search,$translit_replace,$original);
		}
			
		$alias = JFilterOutput::stringURLSafe($original);
		return $alias;
	}
	
	function getParentCategories($vm_prefix,$category_id,&$category_tree) {
		$db = JFactory::getDBO();
		$q  = "SELECT c.category_name, x.category_parent_id FROM #__".$vm_prefix."_category c
		INNER JOIN #__".$vm_prefix."_category_xref x
		ON c.category_id = x.category_parent_id
		WHERE x.category_child_id = '".$category_id."'";
		//echo '<br/>'.$q;
		$db->setQuery($q);
		$category_parent = $db->loadObject();
		if ($category_parent && $category_parent->category_parent_id > 0) {
			$category_tree[] = $category_parent->category_name;
			$category_tree = $this->getParentCategories($vm_prefix,$category_parent_id,$category_tree);
		}
		return $category_tree;
	}

	
}

?>