<?php
/*------------------------------------------------------------------------
# vm_sef - Search Engine Friendly URL's for Virtuemart
# ------------------------------------------------------------------------
# author    Jeremy Magne
# copyright Copyright (C) 2010 Daycounts.com. All Rights Reserved.
# Websites: http://www.daycounts.com
# Technical Support: http://www.daycounts.com/en/contact/
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
-------------------------------------------------------------------------*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

$client	=& JApplicationHelper::getClientInfo(JRequest::getVar('client', '0', '', 'int'));
$view = JRequest::getCmd('view');
$bar = & JToolBar::getInstance('toolbar');
$title = '';

switch ($view) {
	case 'config':
		$title = ' - '.JText::_('Settings');
		switch ($task) {
			case 'install':
			case 'upgrade':
				break;
			default:
				JToolBarHelper::save();
				JToolBarHelper::divider();
				$bar->appendButton( 'Popup', 'help', JText::_('help'), 'http://www.daycounts.com/en/help/virtuemart-sef-pro/?tmpl=component#settings', 670, 500 );
				//JToolBarHelper::save( 'save_config' );
		}
		break;
	case 'products':
		$title = ' - '.JText::_('Products');
		JToolBarHelper::publishList();
		JToolBarHelper::unpublishList();
		JToolBarHelper::deleteList();
		JToolBarHelper::custom('generate','default','default',JText::_('Generate'));
		JToolBarHelper::divider();
		$bar->appendButton( 'Popup', 'help', JText::_('help'), 'http://www.daycounts.com/en/help/virtuemart-sef-pro/?tmpl=component#products', 670, 500 );
		break;
	case 'categories':
		$title = ' - '.JText::_('Categories');
		JToolBarHelper::publishList();
		JToolBarHelper::unpublishList();
		JToolBarHelper::deleteList();
		JToolBarHelper::custom('generate','default','default',JText::_('Generate'));
		JToolBarHelper::divider();
		$bar->appendButton( 'Popup', 'help', JText::_('help'), 'http://www.daycounts.com/en/help/virtuemart-sef-pro/?tmpl=component#categories', 670, 500 );
		break;
	case 'manufacturers':
		$title = ' - '.JText::_('Manufacturers');
		JToolBarHelper::publishList();
		JToolBarHelper::unpublishList();
		JToolBarHelper::deleteList();
		JToolBarHelper::custom('generate','default','default',JText::_('Generate'));
		JToolBarHelper::divider();
		$bar->appendButton( 'Popup', 'help', JText::_('help'), 'http://www.daycounts.com/en/help/virtuemart-sef-pro/?tmpl=component#manufacturers', 670, 500 );
		break;
	case 'vendors':
		$title = ' - '.JText::_('Vendors');
		JToolBarHelper::publishList();
		JToolBarHelper::unpublishList();
		JToolBarHelper::deleteList();
		JToolBarHelper::custom('generate','default','default',JText::_('Generate'));
		JToolBarHelper::divider();
		$bar->appendButton( 'Popup', 'help', JText::_('help'), 'http://www.daycounts.com/en/help/virtuemart-sef-pro/?tmpl=component#vendors', 670, 500 );
		break;
	case 'pages':
		$title = ' - '.JText::_('Pages');
		JToolBarHelper::publishList();
		JToolBarHelper::unpublishList();
		JToolBarHelper::divider();
		$bar->appendButton( 'Popup', 'help', JText::_('help'), 'http://www.daycounts.com/en/help/virtuemart-sef-pro/?tmpl=component#pages', 670, 500 );
		break;
	case 'cache':
		$title = ' - '.JText::_('Urls cache');
		//JToolBarHelper::custom('lock','lock','lock',JText::_('Lock'));
		//JToolBarHelper::custom('unlock','unlock','unlock',JText::_('Unlock'));
		JToolBarHelper::divider();
		//JToolBarHelper::publishList();
		//JToolBarHelper::unpublishList();
		JToolBarHelper::divider();
		JToolBarHelper::custom('purge','default','default',JText::_('Purge'),false);
		JToolBarHelper::deleteList();
		JToolBarHelper::divider();
		$bar->appendButton( 'Popup', 'help', JText::_('help'), 'http://www.daycounts.com/en/help/virtuemart-sef-pro/?tmpl=component#cache', 670, 500 );
		break;
	default:
		switch ($task) {
			case 'edit':
				JToolBarHelper::title( JText::_( 'Virtuemart Sef' ), 'sef' );
				break;
			default:
				JToolBarHelper::title( JText::_( 'Virtuemart Sef' ), 'sef' );
				JToolBarHelper::publishList();
				JToolBarHelper::unpublishList();
				JToolBarHelper::deleteList();
				JToolBarHelper::addNewX();
				//JToolBarHelper::help( 'screen.templates' );
		}
		break;

}

JToolBarHelper::title( JText::_( 'Virtuemart Sef' ).$title, 'sef' );


//Load the sub menu
if ($view != 'product') {
	JSubMenuHelper::addEntry(JText::_('Products'), 'index.php?option='.$option.'&view=products',($view=='products'));
	JSubMenuHelper::addEntry(JText::_('Categories'), 'index.php?option='.$option.'&view=categories',($view=='categories'));
	JSubMenuHelper::addEntry(JText::_('Manufacturers'), 'index.php?option='.$option.'&view=manufacturers',($view=='manufacturers'));
	JSubMenuHelper::addEntry(JText::_('Vendors'), 'index.php?option='.$option.'&view=vendors',($view=='vendors'));
	JSubMenuHelper::addEntry(JText::_('Pages'), 'index.php?option='.$option.'&view=pages',($view=='pages'));
	JSubMenuHelper::addEntry(JText::_('Cache'), 'index.php?option='.$option.'&view=cache',($view=='cache'));
	JSubMenuHelper::addEntry(JText::_('Settings'), 'index.php?option='.$option.'&view=config',($view=='config'));
}

?>
