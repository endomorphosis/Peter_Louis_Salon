<?php
/**
* @version		$Id: view.html.php 14401 2010-01-26 14:10:00Z louis $
* @package		Joomla
* @subpackage	Config
* @copyright	Copyright (C) 2005 - 2010 Open Source Matters. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport('joomla.application.component.view');
jimport('joomla.plugin.helper');
//jimport('joomla.filesystem.file');
require_once (JPATH_ADMINISTRATOR.DS.'components'.DS.'com_vm_sef'.DS.'classes'.DS.'config.php');

/**
 * HTML View class 
 *
 * @static
 * @package		Joomla
 * @subpackage	Poll
 * @since 1.0
 */
class VmSefConfigViewConfig extends JView
{
	function display($tpl = null)
	{
		$config = new VmSefConfig();
		$this->assignRef('config',	$config);
		
		$rewrite_modes = array();
		$rewrite_modes[] = JHTML::_('select.option', 'alias', JText::_('Alias'));
		$rewrite_modes[] = JHTML::_('select.option', 'alias-safe', JText::_('ID-Alias'));
		$rewrite_modes[] = JHTML::_('select.option', 'id', JText::_('ID'));
		$this->assignRef('rewrite_modes',	$rewrite_modes);
		
		$category_modes = array();
		$category_modes[] = JHTML::_('select.option', 'full', JText::_('Parent category/Category'));
		$category_modes[] = JHTML::_('select.option', 'single', JText::_('Category'));
		$this->assignRef('category_modes',	$category_modes);
		
		//Flypages
		$handle = opendir( VM_THEMEPATH . "templates/product_details/" );
		$flypages = array();
		$files = JFolder::files(VM_THEMEPATH . "templates/product_details/",'.tpl.php');
		$flypages[] = JHTML::_('select.option', '', JText::_('- Select -'));
		foreach ($files as $flypage) {
			$flypage = str_replace(".php","",$flypage);
			$flypages[] = JHTML::_('select.option', $flypage, $flypage);
		}
		$this->assignRef('flypages',	$flypages);
		
		//Virtuemart menus
		$db	=& JFactory::getDBO();
		$q = "SELECT * FROM #__menu WHERE link='index.php?option=com_virtuemart' AND published=1";
		$db->setQuery( $q );
		$menus = $db->loadObjectList();
		$vm_menus = array();
		//$vm_menus[] = JHTML::_('select.option', '', JText::_('- Select -'));
		foreach ($menus as $menu) {
			$vm_menus[] = JHTML::_('select.option', $menu->id, $menu->name.' ('.$menu->alias.')');
		}
		$this->assignRef('vm_menus',	$vm_menus);
		
		//Is Virtuemart active?
		$com_virtuemart = &JComponentHelper::getComponent('com_virtuemart', true);
		if ($com_virtuemart->enabled) {
			$virtuemart = 1;
			require_once(JPATH_SITE.DS.'components'.DS.'com_virtuemart'.DS.'virtuemart_parser.php' );
			$virtuemart_cookie_check = (VM_ENABLE_COOKIE_CHECK) ? 0 : 1;

			include_once( ADMINPATH. 'version.php' );
			if( !isset( $VMVERSION ) || !is_object( $VMVERSION ) ) {
				$VMVERSION = new vmVersion();
			}
			$virtuemart_version = $VMVERSION->RELEASE;
			$virtuemart_user_class = VM_ALLOW_EXTENDED_CLASSES;
			$virtuemart_theme = basename(VM_THEMEURL);

			$vm_theme_path = JPATH_SITE.DS.'components'.DS.'com_virtuemart'.DS.'themes'.DS.$virtuemart_theme.DS;
			if (file_exists($vm_theme_path.'user_class'.DS.'ps_session.php')) {
				$virtuemart_theme_check = '<font color="green">'.JTEXT::_('Ok').'</font>';
			} else {
				$virtuemart_theme_check = '<font color="red">'.JTEXT::_('user class files not found').'</font>';
				$virtuemart_theme_check .= '&nbsp;<input type="button" onclick="submitbutton(\'copytheme\')" value="'.JTEXT::_('copy files').'" />';
			}

		}
		
		//Is Joomfish active?
		$com_joomfish = &JComponentHelper::getComponent('com_joomfish', true);
		if ($com_joomfish->enabled) {
			$joomfish = 1;
			$content_elts_path = JPATH_ADMINISTRATOR.DS.'components'.DS.'com_joomfish'.DS.'contentelements'.DS;
			if (file_exists($content_elts_path.'vm_sef_products.xml')
			&& file_exists($content_elts_path.'vm_sef_categories.xml')
			&& file_exists($content_elts_path.'vm_sef_pages.xml')) {
				$joomfish_check = '<font color="green">'.JTEXT::_('Ok').'</font>';
			} else {
				$joomfish_check = '<font color="red">'.JTEXT::_('content elements not found').'</font>';
				$joomfish_check .= '&nbsp;<input type="button" onclick="submitbutton(\'copyjoomfish\')" value="'.JTEXT::_('copy files').'" />';
			}
			$this->assignRef('joomfish_check',	$joomfish_check);
		} else {
			$joomfish = 0;
		}
		$this->assignRef('joomfish',	$joomfish);
		
		$plugin_installed = false;
		$plugin_enabled = false;
		//check if plugin is installed and published
		$db->setQuery("SELECT `published` FROM `#__plugins` WHERE `folder`='system' AND `element`='vm_sef' LIMIT 1");
		$plugins = $db->loadObjectList();					
		foreach($plugins as $plugin){	
			$plugin_installed = true;
			$plugin_enabled = $plugin->published;
		}
		if ($plugin_installed) {
			if ($plugin_enabled) {
				$plugin_check = '<font color="green">'.JTEXT::_('Ok').'</font>';
			} else {
				$plugin_check = '<font color="red">'.JTEXT::_('Plugin not activated').'</font>';
				$plugin_check .= '&nbsp;<input type="button" onclick="submitbutton(\'activateplugin\')" value="'.JTEXT::_('Activate plugin').'" />';
			}
		} else {
			$plugin_check = '<font color="red">'.JTEXT::_('Plugin not installed').'</font>';
			$plugin_check .= '&nbsp;<input type="button" onclick="submitbutton(\'installplugin\')" value="'.JTEXT::_('Install plugin').'" />';
		}


		$JConfig =& JFactory::getConfig();
		$sef = $JConfig->getValue('sef');

		
		//Export variables
		$this->assignRef('virtuemart',	$virtuemart);
		$this->assignRef('virtuemart_cookie_check',	$virtuemart_cookie_check);
		$this->assignRef('virtuemart_version',	$virtuemart_version);
		$this->assignRef('virtuemart_user_class',	$virtuemart_user_class);
		$this->assignRef('virtuemart_theme',	$virtuemart_theme);
		$this->assignRef('virtuemart_theme_check',	$virtuemart_theme_check);
		$this->assignRef('plugin_check',	$plugin_check);
		$this->assignRef('sef',	$sef);
		
		parent::display($tpl);

	}
}