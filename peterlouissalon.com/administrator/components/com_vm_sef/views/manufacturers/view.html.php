<?php
/*------------------------------------------------------------------------
# vm_sef - Search Engine Friendly URL's for Virtuemart
# ------------------------------------------------------------------------
# author    Jeremy Magne
# copyright Copyright (C) 2010 Daycounts.com. All Rights Reserved.
# Websites: http://www.daycounts.com
# Technical Support: http://www.daycounts.com/en/contact/
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# See http://daycounts.com/fr/component/content/article/7 for details
-------------------------------------------------------------------------*/

defined( '_JEXEC' ) or die( 'Restricted access' );

jimport('joomla.application.component.view');
//jimport('joomla.filesystem.file');

class VmSefManufacturersViewManufacturers extends JView
{
	function display($tpl = null)
	{
		$SefConfig = new VmSefConfig();
		$SefHelper = new VmSefHelper();
		$this->assignRef('config',	$SefConfig);
		$this->assignRef('helper',	$SefHelper);

		global $option;
		$mainframe = & JFactory::getApplication('site');

		$db					=& JFactory::getDBO();
		$filter_order		= $mainframe->getUserStateFromRequest( $option.".filter_order",		'filter_order',		'sef.id',	'cmd' );
		$filter_order_Dir	= $mainframe->getUserStateFromRequest( $option.".filter_order_Dir",	'filter_order_Dir',	'desc',		'word' );
		$filter_state		= $mainframe->getUserStateFromRequest( $option.".filter_state",		'filter_state',		'',		'word' );
		$filter_status		= $mainframe->getUserStateFromRequest( $option.".filter_status",	'filter_status',	'',		'word' );
		$search				= $mainframe->getUserStateFromRequest( $option.".search",			'search',			'',		'string' );
		if (strpos($search, '"') !== false) {
			$search = str_replace(array('=', '<'), '', $search);
		}
		$search = JString::strtolower($search);

		$limit		= $mainframe->getUserStateFromRequest( 'global.list.limit', 'limit', $mainframe->getCfg('list_limit'), 'int' );
		$limitstart	= $mainframe->getUserStateFromRequest( $option.'.limitstart', 'limitstart', 0, 'int' );

		$tables = '	FROM #__{vm}_manufacturer src'
		. '	LEFT JOIN #__vm_sef_manufacturers sef ON sef.manufacturer_id = src.manufacturer_id';

		$where = array();
		if ($filter_status) {
			if ( $filter_status == 'E' )	{
				$where[] = 'sef.id IS NOT NULL';
			} else if ($filter_status == 'M' )	{
				$where[] = 'sef.id IS NULL';
			}
		}
		if ( $filter_state ) {
			if ( $filter_state == 'P' )	{
				$where[] = 'sef.published = 1';
			} else if ($filter_state == 'U' )	{
				$where[] = 'sef.published = 0';
			}
		}
		if ($search) {
			$where[] = '('
			. ' LOWER(sef.alias) LIKE '.$db->Quote( '%'.$db->getEscaped( $search, true ).'%', false ) 
			. ' OR LOWER(src.mf_name) LIKE '.$db->Quote( '%'.$db->getEscaped( $search, true ).'%', false )
			. ')';
		}
		$where 		= ( count( $where ) ? ' WHERE ' . implode( ' AND ', $where ) : '' );

		// sanitize $filter_order
		if (!in_array($filter_order, array('src.mf_name', 'sef.alias', 'sef.id'))) {
			$filter_order = 'sef.id';
		}

		if (!in_array(strtoupper($filter_order_Dir), array('ASC', 'DESC'))) {
			$filter_order_Dir = '';
		}
		$orderby 	= ' ORDER BY '. $filter_order .' '. $filter_order_Dir;

		$query = 'SELECT COUNT(src.manufacturer_id)'
		. $tables
		. $where
		;
		$query = str_replace("{vm}",VM_TABLEPREFIX,$query);
		$db->setQuery( $query );
		$total = $db->loadResult();

		jimport('joomla.html.pagination');
		$pagination = new JPagination( $total, $limitstart, $limit );

		$query  = 'SELECT src.manufacturer_id, sef.id, src.mf_name, sef.alias, sef.published, 0 as checked_out, sef.metakey, sef.metadesc'
		. $tables
		. $where
		. $orderby
		;
		$query = str_replace("{vm}",VM_TABLEPREFIX,$query);
		$db->setQuery( $query, $pagination->limitstart, $pagination->limit );
		$rows = $db->loadObjectList();

		if ($db->getErrorNum())
		{
			echo $db->stderr();
			return false;
		}

		$status[] = JHTML::_('select.option', '', JText::_('- Select status'));
		$status[] = JHTML::_('select.option', 'E', JText::_('Exists'));
		$status[] = JHTML::_('select.option', 'M', JText::_('Missing'));
	
		$lists['status']	=  JHTML::_('select.genericlist',   $status, 'filter_status', 'class="inputbox" onchange="submitform( );" size="1"', 'value', 'text', $filter_status );
		$lists['state']	= JHTML::_('grid.state',  $filter_state );

		// table ordering
		$lists['order_Dir']	= $filter_order_Dir;
		$lists['order']		= $filter_order;

		// search filter
		$lists['search']= $search;
		

		$this->assignRef('user',		JFactory::getUser());
		$this->assignRef('lists',		$lists);
		$this->assignRef('items',		$rows);
		$this->assignRef('pagination',	$pagination);

		parent::display($tpl);

	}
}