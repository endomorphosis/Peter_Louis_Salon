<?php 
/*------------------------------------------------------------------------
# vm_sef - Search Engine Friendly URL's for Virtuemart
# ------------------------------------------------------------------------
# author    Jeremy Magne
# copyright Copyright (C) 2010 Daycounts.com. All Rights Reserved.
# Websites: http://www.daycounts.com
# Technical Support: http://www.daycounts.com/en/contact/
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access'); 

?>
<div class="clr"></div>

<form action="index.php" method="post" name="adminForm">
<table>
	<tr>
		<td align="left" width="100%">
			<?php echo JText::_( 'Filter' ); ?>:
			<input type="text" name="search" id="search" value="<?php echo htmlspecialchars($this->lists['search']);?>" class="text_area" onchange="document.adminForm.submit();" />
			<button onclick="this.form.submit();"><?php echo JText::_( 'Go' ); ?></button>
			<button onclick="document.getElementById('search').value='';this.form.getElementById('filter_state').value='';this.form.getElementById('filter_status').value='';this.form.submit();"><?php echo JText::_( 'Reset' ); ?></button>
		</td>
		<td nowrap="nowrap">
			<?php echo $this->lists['status']; ?>
            <?php echo $this->lists['state']; ?>
		</td>
	</tr>
</table>
<div id="tablecell">
	<table class="adminlist">
	<thead>
		<tr>
			<th width="5">
				<?php echo JText::_( 'NUM' ); ?>
			</th>
			<th width="20">
				<input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count( $this->items ); ?>);" />
			</th>
			<th  class="title">
				<?php echo JHTML::_('grid.sort',   'Name', 'src.mf_name', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
			</th>
			<th  class="title">
				<?php echo JHTML::_('grid.sort',   'Alias', 'sef.alias', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
			</th>
            <th  class="title">
            	<?php echo JText::_('Keywords'); ?>
            </th>
            <th  class="title">
            	<?php echo JText::_('Description'); ?>
            </th>
			<th  class="title">
				<?php echo JHTML::_('grid.sort',   'Published', 'sef.published', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
			</th>
			<th width="1%" nowrap="nowrap">
				<?php echo JHTML::_('grid.sort',   'Manufacturer ID', 'src.manufacturer_id', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
			</th>
			<th width="1%" nowrap="nowrap">
				<?php echo JHTML::_('grid.sort',   'ID', 'sef.id', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
			</th>
		</tr>
	</thead>
	<tfoot>
		<tr>
			<td colspan="10">
				<?php echo $this->pagination->getListFooter(); ?>
			</td>
		</tr>
	</tfoot>
	<tbody>
	<?php
	$k = 0;
	for ($i=0, $n=count( $this->items ); $i < $n; $i++)
	{
		$row = &$this->items[$i];
		$editlink 		= JRoute::_( 'index.php?tmpl=component&option=com_vm_sef&view=alias&task=edit&type=manufacturer&id='.$row->manufacturer_id );
		$virtuemartlink = JRoute::_( 'index.php?&option=com_virtuemart&page=manufacturer.manufacturer_form&manufacturer_id='.$row->manufacturer_id );
	?>
		<tr class="<?php echo "row$k"; ?>">
			<td>
				<?php echo $this->pagination->getRowOffset( $i ); ?>
			</td>
			<td>
				<?php echo JHTML::_('grid.checkedout',   $row, $i, 'manufacturer_id' ); ?>
			</td>
			<td>
				<span class="editlinktip hasTip" title="<?php echo JText::_( 'Edit Alias' );?>::<?php echo htmlspecialchars($row->mf_name); ?>">
				<a class="modal" rel="{handler: 'iframe', size: {x: 450, y: 420}}" href="<?php echo $editlink  ?>"><?php echo htmlspecialchars($row->mf_name); ?></a></span>
			</td>
			<td>
				<?php echo ($row->id) ? $row->alias : '<font color="red">'.JText::_('Missing').'</font>'; ?>
			</td>
            <td align="center">
            	<?php if ($row->metakey) { ?>
                    <span class="hasTip" title="<?php echo $row->metakey; ?>">
                        <img src="components/com_vm_sef/assets/images/gear.png" />
                    </span>
                <?php } else { ?>
                	&nbsp;
                <?php } ?>
            </td>
            <td align="center">
            	<?php if ($row->metadesc) { ?>
                    <span class="hasTip" title="<?php echo $row->metadesc; ?>">
                        <img src="components/com_vm_sef/assets/images/gear.png" />
                    </span>
                <?php } else { ?>
                	&nbsp;
                <?php } ?>
            </td>
			<td align="center">
            	<?php echo ($row->id) ? JHTML::_('grid.published', $row, $i ) : '-'; ?>
			</td>
			<td align="center">
				<a href="<?php echo $virtuemartlink  ?>"><?php echo $row->manufacturer_id; ?></a>
			</td>
			<td align="center">
				<?php echo $row->id; ?>
			</td>
		</tr>
		<?php
			$k = 1 - $k;
		}
		?>
	</tbody>
	</table>
</div>

	<input type="hidden" name="option" value="com_vm_sef" />
	<input type="hidden" name="view" value="manufacturers" />
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="boxchecked" value="0" />
	<input type="hidden" name="filter_order" value="<?php echo $this->lists['order']; ?>" />
	<input type="hidden" name="filter_order_Dir" value="<?php echo $this->lists['order_Dir']; ?>" />
	<?php echo JHTML::_( 'form.token' ); ?>
</form>
<div class="clr"></div>
