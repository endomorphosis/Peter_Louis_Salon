<?php
/**
* @subpackage OSE Update Manager
*
* @author Open Source Excellence, Jays.Soho {@link  http://www.opensource-excellence.com}
* @author Created on 01-Apr-2010
* @copyright Copyright (C) 2010 Open Source Excellence. All rights reserved.
* @license GNU/GPL v2  http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined( '_JEXEC' ) or die( ';)' );

jimport( 'joomla.application.component.view');

/**
 * upman Controller
 *
 * @package    upman
 * @subpackage Controllers
 */
class upmanViewupman extends JView
{
	/**
	 * constructor (registers additional tasks to methods)
	 * @return void
	 */
	function __construct()
	{
		parent::__construct();
	}// function

	function display($tpl = null)
	{
		JHTML::stylesheet( 'style.css', 'administrator/components/com_upman/assets/css/' );
		JToolBarHelper::title( JText::_( 'OSE Update Manager V2.2' ).'&nbsp;&nbsp;<img src="components/com_upman/assets/images/compat_15_native.png"/>&nbsp;<img src="components/com_upman/assets/images/compat_16_native.png"/>', 'updater');
		JToolBarHelper::trash('remove','Delete');
		// Get data from the model
	 	$items =& $this->get('Items');
	 	$unzipFiles =& $this->get('UnzipFiles');
	 	$pagination =& $this->get('Pagination');
		$lists =& $this->get('ViewLists');

		// push data into the template
		$this->assignRef('items', $items);
		$this->assignRef('unzipFiles', $unzipFiles);
		$this->assignRef('pagination', $pagination);
		$this->assignRef('lists', $lists);
		parent::display($tpl);
	}
}// class
