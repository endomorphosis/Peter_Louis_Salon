<?php
/**
* @subpackage OSE Update Manager
*
* @author Open Source Excellence, Jays.Soho {@link  http://www.opensource-excellence.com}
* @author Created on 01-Apr-2010
* @copyright Copyright (C) 2010 Open Source Excellence. All rights reserved.
* @license GNU/GPL v2  http://www.gnu.org/copyleft/gpl.html
*/ // no direct access
defined('_JEXEC') or die(';)');
JHTML::_('behavior.modal');
?>
<script language="javascript" type="text/javascript">
 Ext.onReady(function(){
 	Ext.get('loadingindicator').hide();
 })
 function extract(filename)
 {
 	Ext.get('loadingindicator').show();
 	Ext.Ajax.request({
			url : 'index.php' ,
			params : {
				option : 'com_upman',
				task:'extractFile',
				controller:'upman',
				file: filename
				},
				method: 'POST',
				success: function ( result, request ) {
				msg = Ext.decode(result.responseText);
				if (msg.result=='SUCCESS')
				{
					Ext.get('loadingindicator').hide();
					alert(msg.text);
					window.location = 'index.php?option=com_upman';
				}
				else
				{
					Ext.get('loadingindicator').hide();
					alert(msg.text);
				}
			 }
		});
 }

</script>
<?php

echo "<div class='setting-msg'>Your server has the following PHP setting at the moment: Maximum Execution Time is ".ini_get("max_execution_time")." seconds";
echo ", Memory Limit is ".ini_get("memory_limit").", Upload Maximum Filesize is: ".ini_get("upload_max_filesize").
	 ". <br />If your upload_max_filesize is lower than 3M, and or the maximum_execution_time is lower than 60, please access <a href = 'http://wiki.opensource-excellence.com/index.php?title=Changing_PHP_configuration_setting' class=\"modal\" rel=\"{handler: 'iframe', size: {x: 980, y: 500}}\">OSE WIKI </a> for instructions on increasing these values. </div>";

?>

<form action="index.php?option=com_upman" enctype="multipart/form-data" method="post" name="adminForm">
    <fieldset>
    <legend><?php echo JText::_( 'Update Package Installation' ); ?></legend>
		<table>
		<tr><td>
		<img src="components/com_upman/assets/images/globe_up.png"></img>
		</td>
		<td class="description">
		<?php echo JText::_( 'Please Upload your Update Package here.' ); ?>
		</td>
		</tr>
		</table>
		<table class="adminform">
			<tr>
				<th colspan="2"><?php echo JText::_( 'Method 1: Upload Package File' ); ?></th>
			</tr>
			<tr>
				<td width="120">
					<label for="install_package"><?php echo JText::_( 'Package File' ); ?>:</label>
				</td>
				<td>
					<input class="input_box" id="install_package" name="install_package" type="file" size="57" />
					<input class="button" type="button" value="<?php echo JText::_( 'Upload File' ); ?> &amp; <?php echo JText::_( 'Setup' ); ?>" onclick="submitbutton('upload')" />
				</td>
			</tr>
		</table>
		<table class="adminform">
			<tr>
				<th colspan="2"><?php echo JText::_( 'Method 2: Install from URL' ); ?></th>
			</tr>
			<tr>
				<td width="120">
					<label for="install_url"><?php echo JText::_( 'Install URL' ); ?>:</label>
				</td>
				<td>
					<input type="text" id="install_url" name="install_url" class="input_box" size="70" value="http://" />
					<input type="button" class="button" value="<?php echo JText::_( 'Setup' ); ?>" onclick="submitbutton4('upload')" />
				</td>
			</tr>
		</table>
    </fieldset>


<?php
if (!empty($this->unzipFiles))
{	?>
<fieldset>
<table >
			<tr>
				<th colspan="2"><?php echo JText::_( 'Detected Zip Files' ); ?></th>
			</tr>
			<tr>
				<td width="100%">
					<?php echo JText::_( 'There are un-extracted zip files detected in the UPMan folders' ); ?>
				</td>
			</tr>
			<?php
			foreach ($this->unzipFiles as $unzipFile)
			{
				echo "<tr class='items'><td class='filename'>".$unzipFile."</td>";
				echo '<td><input type="button" class="button" value="Extract Now" onClick = "extract('."'{$unzipFile}'".')"/></td></tr>';
			}
			?>
			<tr><td colspan="2"><div id="loadingindicator"></div></td></tr>
		</table>

</fieldset>
<?php } ?>
    <fieldset>
	    <legend><?php echo JText::_( 'Update Package List' ); ?></legend>
<table>
		<tr><td>
		<img src="components/com_upman/assets/images/process_add.png"></img>

		</td>
		<td class="description">
		<?php echo JText::_( 'Please click the installation link under the operation section to run the update robot.' ); ?>
		</td>
		</tr>
		</table>

	<table  width="100%" align="left" class="adminForm" id="packListToolbar">
		<tr>
			<td align="left">
				<input type="text" name="search" id="search" value="<?php if (isset($this->viewLists['search'])) {echo $this->viewLists['search'];}?>" class="text_area" onchange="document.adminForm.submit();" />
				<button onclick="this.form.submit();"><?php echo JText::_( 'Filter' ); ?></button>
				<button onclick="document.getElementById('search').value='';this.form.getElementById('filter_type').value='0';this.form.submit();"><?php echo JText::_( 'Reset' ); ?></button>
			</td>
			<td nowrap="nowrap" align="right">
				<?php echo $this->lists['type'];?>
			</td>
		</tr>
	</table>

	<div class="clr"></div>

	<table class="adminlist" cellpadding="1"  width="100%" align="left">
		<thead>
			<tr>
				<th width="3%">
					<?php echo JText::_( 'ID' ); ?>
				</th>

				<th width="5%">
					<input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count( $this->items ); ?>);" />
				</th>

		        <th class="title">
					<?php echo JHTML::_('grid.sort',   'Update Package Folder Name', 'u.folder', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
				</th>

				<th class="title" width="20%">
					<?php echo JHTML::_('grid.sort',   'Type', 'u.type', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
				</th>

				<th class="title" width="20%">
					<?php echo JHTML::_('grid.sort',   'Date Added', 'u.date_add', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
				</th>

			</tr>
		</thead>
	<?php

$k= 0;
for($i= 0, $n= count($this->items); $i < $n; $i++) {
	$row= & $this->items[$i];
	$checked= JHTML :: _('grid.id', $i, $row->id);
	$link= 'index.php?option=com_upman&view=installer&parent='.$row->id.'&'.JUtility :: getToken().'=1';
	$type=($row->type == 'js.updater') ? ucfirst('original update pack') : ucfirst($row->type);
?>
		<tr class="<?php echo "row$k"; ?>">
			<td>
				<?php echo $i+1+$this->pagination->limitstart;?>
			</td>

			<td align="center"><?php echo $checked; ?></td>

			<td align="left"><a href='<?php echo  $link; ?>'><?php echo  (!empty($row->name))?$row->name:"" ; ?><?php echo JText::_(' (Click In to Install)');?></a></td>

			<td align="center"><?php echo  $type ; ?></td>

			<td align="center"><?php echo  $row->date_add ; ?></td>

		</tr>
		<?php

	$k= 1 - $k;
}
?>
		 <tfoot>
		    <tr>
		      <td colspan="5">
		      	<?php echo $this->pagination->getListFooter(); ?>
		      </td>
		    </tr>
		  </tfoot>
	  </table>
</fieldset>
<input type="hidden" name="option" id="option" value="com_upman" />
<input type="hidden" name="boxchecked" value="0" />
<input type="hidden" name="installtype" value="upload" />
<input type="hidden" name="controller" value="upman" />
<input type="hidden" name="task" value="" />
<input type="hidden" name="filter_order" value="<?php echo $this->lists['order']; ?>" />
<input type="hidden" name="filter_order_Dir" value="<?php echo $this->lists['order_Dir']; ?>" />
<?php echo JHTML::_( 'form.token' ); ?>
</form>
