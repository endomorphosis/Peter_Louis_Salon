<?php
/**
* @subpackage OSE Update Manager
*
* @author Open Source Excellence, Jays.Soho {@link  http://www.opensource-excellence.com}
* @author Created on 01-Apr-2010
* @copyright Copyright (C) 2010 Open Source Excellence. All rights reserved.
* @license GNU/GPL v2  http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined( '_JEXEC' ) or die( ';)' );

jimport( 'joomla.application.component.view');

/**
 * upman Controller
 *
 * @package    upman
 * @subpackage Controllers
 */
class upmanViewInstaller extends JView
{
	/**
	 * constructor (registers additional tasks to methods)
	 * @return void
	 */
	function __construct()
	{
		parent::__construct();
	}// function

	function display($tpl = null)
	{
		// Get data from the model
	 	$items =& $this->get('Items');
	 	$parent =& JRequest::getInt('parent',null);

	 	// ToolBar
	 	JHTML::stylesheet( 'style.css', 'administrator/components/com_upman/assets/css/' );
        JToolBarHelper::title( JText::_( 'OSE Update Manager' ), 'upman');
		JToolBarHelper::custom( 'install', 'apply.png', 'apply.png', "Install", true, false );

		JToolBarHelper::deleteList(JText::_(''),'uninstall',JText::_('uninstall'));
		JToolBarHelper::cancel('cancel',JText::_('Cancel & Return'));

		// push data into the template
		$this->assignRef('items', $items);
		$this->assignRef('parent', $parent);
		parent::display($tpl);
	}
}// class