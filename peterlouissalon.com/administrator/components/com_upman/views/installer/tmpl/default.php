<?php
/**
* @subpackage OSE Update Manager
*
* @author Open Source Excellence, Jays.Soho {@link  http://www.opensource-excellence.com}
* @author Created on 01-Apr-2010
* @copyright Copyright (C) 2010 Open Source Excellence. All rights reserved.
* @license GNU/GPL v2  http://www.gnu.org/copyleft/gpl.html
 */
// no direct access
defined('_JEXEC') or die(';)');
?>
<form action="index.php?option=com_upman&view=install&parent=<?php echo $this->info->id; ?>" enctype="multipart/form-data" method="post" name="adminForm">
	<table class="adminlist" cellpadding="1"  width="100%" align="left">
		<thead>
			<tr>
				<th width="3%">
					<?php echo JText::_( 'ID' ); ?>
				</th>
				<th width="5%">
					<input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count( $this->items ); ?>);" />
				</th>
		        <th class="title">
					<?php echo JText::_( 'Package: ' ); ?>
				</th>

				<th class="title">
					<?php echo JText::_( 'Description: ' ); ?>
				</th>
				<th class="title" width="5%">
					<?php echo JText::_('Version'); ?>
				</th>
				<th class="title" width="15%">
					<?php echo JText::_( 'Folder Name ' ); ?>
				</th>

				<th class="title" width="15%">
					<?php echo JText::_('Type'); ?>
				</th>

				<th class="title" width="10%">
					<?php echo JText::_('Status'); ?>
				</th>
			</tr>
		</thead>
	<?php

$k= 0;
for($i= 0, $n= count($this->items); $i < $n; $i++) {
	$row= & $this->items[$i];
	$checked= JHTML :: _('grid.id', $i, $row->id);
?>
		<tr class="<?php echo "row$k"; ?>">
			<td align="center">
				<?php echo $i+1;?>
			</td>

			<td align="center"><?php echo $checked; ?></td>

			<td align="left"><?php echo  $row->name ; ?></td>

			<td align="left">
				<span class="editlinktip hasTip" title="<?php echo JText::_( $row->description );?>">
				<?php echo  $row->description ; ?>
				</span>
			</td>
			<td align="center"><?php

	$parent= '';
	if($row->parent > 0) {
		$db= & JFactory :: getDBO();
		$query= "SELECT folder FROM #__upman WHERE id = {$row->parent}";
		$db->setQuery($query);
		$parent= $db->loadResult().DS;
	}
	$folder= JPATH_COMPONENT.DS.'up_files'.DS.$parent.$row->folder;
	$xmlFilesInDir= JFolder :: files($folder, '.xml$');
	if(!empty($xmlFilesInDir)) {
		foreach($xmlFilesInDir as $xmlfile) {
			if($data= JApplicationHelper :: parseXMLInstallFile($folder.DS.$xmlfile)) {
				foreach($data as $key => $value) {
					$comp-> $key= $value;
				}
			}
		}
	}
	if(isset($comp->version)) {
		echo $comp->version;
	} else {
		echo "Upman cannot detect the version.";
	}
?></td>
			<td align="center"><?php echo  $row->folder ; ?></td>

			<td align="center"><?php echo  $row->type ; ?></td>

			<td align="center"><?php echo  $row->status ; ?></td>

		</tr>
		<?php

	$k= 1 - $k;
}
?>
		 <tfoot>
		    <tr>
		      <td colspan="9">

		      </td>
		    </tr>
		  </tfoot>
	  </table>
<input type="hidden" name="option" value="com_upman" />
<input type="hidden" name="task" value="" />
<input type="hidden" name="boxchecked" value="0" />
<input type="hidden" name="controller" value="installer" />
<input type="hidden" name="parent" value="<?php echo $this->parent; ?>" />
<?php echo JHTML::_( 'form.token' ); ?>
</form>
