<?php
/**
 * @version $Id: header.php 248 2008-05-23 10:40:56Z elkuku $
 * @package		upman
 * @subpackage	
 * @author		Jays.Soho {@link http://jays.survey-partner.com}
 * @author		Created on 11-Mar-2009
 */

// no direct access
defined( '_JEXEC' ) or die( ';)' );

?>
<form action="index.php?option=com_upman&view=install&parent=<?php echo $this->info->id; ?>" enctype="multipart/form-data" method="post" name="adminForm">
	<table class="adminlist" cellpadding="1"  width="100%" align="left">
		<thead>
			<tr>
				<th width="3%">
					<?php echo JText::_( 'ID' ); ?>
				</th>
				<th width="5%">
					<input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count( $this->items ); ?>);" />
				</th>			
		        <th class="title">
					<?php echo JText::_( 'Package: '.$this->info->folder ); ?>
				</th>
				
				<th class="title" width="15%">
					<?php echo JText::_('Type'); ?>
				</th>  
				
				<th class="title" width="10%">
					<?php echo JText::_('Status'); ?>
				</th>                    
			</tr>			
		</thead>
	<?php
	$k = 0;
	for ($i=0, $n=count( $this->items ); $i < $n; $i++)
	{
		$row = &$this->items[$i];
		
		$checked 	= JHTML::_('grid.id',   $i, $row->id );
		$type = ($row->type=='js.updater')?ucfirst('original update pack'):ucfirst($row->type);
		$status = ($row->status==1)?'installed':'';
		?>
		<tr class="<?php echo "row$k"; ?>">
			<td align="center">
				<?php echo $i+1;?>
			</td>
            
			<td align="center"><?php echo $checked; ?></td>
            
			<td align="left"><?php echo  $row->folder ; ?></td>
			
			<td align="center"><?php echo  $type ; ?></td>
			
			<td align="center"><?php echo  $status ; ?></td>
                                             
		</tr>
		<?php
		$k = 1 - $k;
	}
	?>
	

		 <tfoot>
		    <tr>
		      <td colspan="5">
		      	
		      </td>
		    </tr>
		  </tfoot>
	  </table>


<input type="hidden" name="option" value="com_upman" />
<input type="hidden" name="task" value="" />
<input type="hidden" name="boxchecked" value="0" />
<input type="hidden" name="controller" value="install" />
<input type="hidden" name="parent" value="<?php echo $this->info->id; ?>" />
<input type="hidden" name="type" value="<?php echo $this->info->type; ?>" />
<?php echo JHTML::_( 'form.token' ); ?>
</form>
