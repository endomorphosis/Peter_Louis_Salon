<?php
/**
 * @version $Id: header.php 248 2008-05-23 10:40:56Z elkuku $
 * @package		upman
 * @subpackage	
 * @author		Jays.Soho {@link http://jays.survey-partner.com}
 * @author		Created on 11-Mar-2009
 */

// no direct access
defined( '_JEXEC' ) or die( ';)' );

jimport( 'joomla.application.component.view');

/**
 * upman Controller
 *
 * @package    upman
 * @subpackage Controllers
 */
class upmanViewInstall extends JView
{
	/**
	 * constructor (registers additional tasks to methods)
	 * @return void
	 */
	function __construct()
	{
		parent::__construct();
	}// function
	
	function display($tpl = null)
	{
		// Get data from the model
	 	$items =& $this->get('Items');
	 	$info =& $this->get('Info');	
	 	
	 	// ToolBar
	 	JHTML::stylesheet( 'style.css', 'administrator/components/com_upman/assets/css/' );
        JToolBarHelper::title( JText::_( 'OSE Update Manager -- ' )."<small><small>".JText::_( 'Are you up-to-date, man?' )."</small></small>" , 'upman');		
		JToolBarHelper::custom( 'install', 'apply.png', 'apply.png', "Install", true, false );
		
		if($info->type == 'js.updater')
		{
			JToolBarHelper::custom( 'restore', 'help.png', 'help.png', "Restore", false, false );
		}
		
		
		
		JToolBarHelper::deleteList(JText::_(''),'uninstall',JText::_('uninstall'));
		JToolBarHelper::cancel();
	
		// push data into the template
		$this->assignRef('items', $items);
		$this->assignRef('info', $info);
		parent::display($tpl);
	}
}// class