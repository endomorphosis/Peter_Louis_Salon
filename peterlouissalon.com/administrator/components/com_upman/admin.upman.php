<?php
/**
* @subpackage OSE Update Manager
*
* @author Open Source Excellence, Jays.Soho {@link  http://www.opensource-excellence.com}
* @author Created on 01-Apr-2010
* @copyright Copyright (C) 2010 Open Source Excellence. All rights reserved.
* @license GNU/GPL v2  http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined( '_JEXEC' ) or die( ';)' );

// Require the base controller
require_once( JPATH_COMPONENT.DS.'controller.php' );

jimport( 'joomla.installer.installer' );
jimport('joomla.installer.helper');
jimport('joomla.filesystem.file');
jimport('joomla.filesystem.folder');
jimport('joomla.filesystem.archive');
jimport('joomla.filesystem.path');

jimport( 'joomla.version' );
$version = new JVersion();
$version = substr($version->getShortVersion(),0,3);
$jsfiles[0] = 'administrator/components/com_upman/assets/js/adapter/ext/ext-base.js';
$jsfiles[1] = 'administrator/components/com_upman/assets/js/ext-all.js';

foreach ($jsfiles as $jsfile )
{
	if($version == '1.6')
	{
		JHTML::script($jsfile,$framework = false, $relative = false, $path_only = false);
		if (!defined("OSEJVERSION"))
		{
		define("OSEJVERSION", 'J16');
		// Update Menu;
		$db = &JFactory::getDBO();
		$query = "SELECT * FROM `#__menu` WHERE `alias` =  'OSE Upman™'";
		$db->setQuery($query);
		$results = $db->loadResult();
		if (empty($results))
		{
		$query= "UPDATE `#__menu` SET `alias` =  'OSE Upman™', `path` =  'OSE Upman™', `published`=1, `img` = '\"components/com_upman/favicon.ico\"'  WHERE `component_id` = ( SELECT extension_id FROM `#__extensions` WHERE element ='com_upman' and name ='upman' ) ";
		$db->setQuery($query);
		$db->query();
		}
		}
	}
	else
	{
		$fPath = dirname($jsfile).'/';
		$fName = basename($jsfile);
		JHTML::script($fName,$fPath);
		if (!defined("OSEJVERSION"))
		{
		define("OSEJVERSION", 'J15');
		}
	}
}
// Require specific controller if requested
$controller = JRequest::getVar('controller');

$path = JPATH_COMPONENT.DS.'controllers'.DS.$controller.'.php';

if( file_exists($path))
{
	require_once($path);
} else	{
	$controller = '';
}

if (function_exists('ini_set')) {
ini_set("max_execution_time", "1800");
ini_set("memory_limit", "512M");
}

// Create the controller
$classname    = 'upmanController'.$controller;
$controller   = new $classname();

// Perform the Request task
$controller->execute( JRequest::getVar( 'task' ) );

// Redirect if set by the controller
$controller->redirect();