<?php
/**
* @subpackage OSE Update Manager
*
* @author Open Source Excellence, Jays.Soho {@link  http://www.opensource-excellence.com}
* @author Created on 01-Apr-2010
* @copyright Copyright (C) 2010 Open Source Excellence. All rights reserved.
* @license GNU/GPL v2  http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined( '_JEXEC' ) or die( ';)' );

class UpmanWearComponent Extends Upman
{
	var $ext_id ;

	protected
	$com = null,
	$name = null,
	$link = null,
	$path = null;

	function __construct()
	{
		$this->db =& JFactory::getDBO();
	}

	function setup($p_dir,$parent = 0)
	{
		$this->set('link','index.php?option=com_upman&view=upman');

		// Get the Name
		$this->getName($p_dir);

		// Rename the Folder
		$this->renameFolder($p_dir);

		$p_dir = $this->get('path');
		$folder 	= $this->get('folder');
		$name 		= $this->get('name');
		$desc 		= $this->get('description');
		$desc		= $this->db->quote($desc);
		// Mark down the info. of component.
		// -> check whether exists or not

		$query = " SELECT count(*) FROM `#__upman` WHERE folder = '{$folder}' AND parent='{$parent}'";

		$this->db->setQuery($query);

		if($this->db->loadResult() > 0)
		{
			$query = " UPDATE `#__upman` SET name = '{$name}', description = {$desc},date_add = CURDATE()"
					." WHERE folder = '{$folder}' ";
					;
		}
		else
		{
			$query = " INSERT INTO `#__upman` (name,folder,parent,type,description,date_add) "
					." VALUES ('{$name}','{$folder}','{$parent}','component',{$desc},CURDATE()) "
					;
		}
		//echo $query;exit;
		$this->db->setQuery($query);

		if(!$this->db->query())
		{
			//JFolder::delete($p_dir);
			$this->errorReturn(JText::_('Can Not Setup the Component:'.$name));
		}
		else
		{
			return true;
		}
	}

	function getName($path)
	{
		$menu = null;
		$name = null;

		//$installer =& new JInstaller;
		$installer =& JInstaller::getInstance();
		$installer->setPath('source', $path);
		$manifest = $installer->getManifest();

		jimport( 'joomla.version' );
		$version = new JVersion();
		$version = substr($version->getShortVersion(),0,3);

		if ($version =='1.6')
		{
			$name = strtolower(JFilterInput::getInstance()->clean((string)$manifest->name, 'cmd'));
			$description = JFilterInput::getInstance()->clean((string)$manifest->description, 'string');
			$menu = JFilterInput::getInstance()->clean((string)$manifest->administration->menu, 'string');
			if (empty($menu))
			{
				$menu=$name;
			}
		}
		else
		{
			$document =& $manifest->document;
			$name =& $document->getElementByPath('name');
			$name = JFilterInput::clean($name->data(), 'cmd');

			$adminElement		=& $document->getElementByPath('administration');
			$menu = $adminElement->getElementByPath('menu');
			if(is_a($menu,'JSimpleXMLElement'))
			{
				$menu = $menu->data();
			}
			else
			{
				$menu = $name;
			}

			$description = $document->getElementByPath('description');
			$description = JFilterInput::clean($description->data(), 'string');;
		}
		$this->set('description', $description);
		$this->set('folder', 'com_'.strtolower($name));
		$this->set('name', $menu );
	}

	function renameFolder($path)
	{
		$folder = $this->get('folder');
		$r_path = str_replace(basename($path),$folder,$path);

		if(JFolder::exists( $r_path ))
		{
			JFolder::delete( $r_path );
		}

		if(JFolder::move($path,$r_path))
		{
			$this->set('path',$r_path);
			return true;
		}
		else
		{
			JFolder::delete($path);
			$this->errorReturn(JText::_('Can Not Rename the Component Folder '.basename($path).' To '.$folder));
		}
	}

	function getItems($obj)
	{
		return $this->resetType($obj);
	}

	function install($path,$obj)
	{
		$installer =& JInstaller::getInstance();

		if($obj->status == 1)
		{
			$installer->setOverwrite(true);
		}

		if(!$installer->install($path))
		{
			return false;
		}

		// Update the Status...
		//if ($obj->folder=="com_ose_cpu")
		//{
		//	$query = " UPDATE `#__upman` SET ext_id = '0', status = '1' "
		//		." WHERE id = '{$obj->id}' ";
		//}
		//else
		//{
			if (OSEJVERSION=='J16')
			{
				$query = " UPDATE `#__upman` SET ext_id = (SELECT extension_id FROM `#__extensions` WHERE `element` = '{$obj->folder}'), status = '1' "
				." WHERE id = '{$obj->id}' ";
			}
			else
			{
				$query = " UPDATE `#__upman` SET ext_id = (SELECT id FROM `#__components` WHERE `admin_menu_link` = 'option={$obj->folder}'), status = '1' "
				." WHERE id = '{$obj->id}' ";
			}
		//}
		$this->db->setQuery($query);
		if(!$this->db->query())
		{
			return false;
		}

		return true;
	}

	function uninstall($obj)
	{
		$installer =& JInstaller::getInstance();

		if(empty($obj->ext_id))
		{
			// Report

			return false;
		}
		//echo $obj->ext_id;exit;
		if(!$installer->uninstall($obj->type ,$obj->ext_id , 0))
		{
			return false;
		}

		// Update the Status...
		$query = " UPDATE `#__upman` SET ext_id = '0', status = '0' WHERE id = '{$obj->id}' ";
		$this->db->setQuery($query);
		if(!$this->db->query())
		{
			return false;
		}

		return true;
	}

	function resetType($obj)
	{

		if (OSEJVERSION=='J16')
		{
			$query = " SELECT extension_id FROM `#__extensions` WHERE element = '{$obj->folder}'";
		}
		else
		{
		$query = " SELECT id FROM `#__components` WHERE link = 'option={$obj->folder}'";
		}

		$this->db->setQuery($query);
		$id = $this->db->loadResult();

		if(empty($id))
		{
			$obj->ext_id = 0;
		}
		else
		{
			if($id != $obj->ext_id)
			{
				$obj->ext_id = $id;
			}
		}

		if(!empty($obj->ext_id))
		{
			$obj->status = 1;
			//$obj->type = 'component&patch';
		}
		else{
			$obj->status = 0;
			//$obj->type = 'component';
		}

		$query = " UPDATE `#__upman` SET ext_id = '{$obj->ext_id}', type = '{$obj->type}', status = '{$obj->status}' WHERE folder = '{$obj->folder}' ";
		$this->db->setQuery($query);
		if(!$this->db->query())
		{
			return false;
		}

		switch($obj->status)
		{
			case(0):
				$obj->status = 'Not Install Yet';
				break;

			case(1):
				$obj->status = 'Installed';
				break;
		}

		return $obj;
	}
}
?>