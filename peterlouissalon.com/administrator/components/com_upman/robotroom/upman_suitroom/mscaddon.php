<?php
/**
* @subpackage OSE Update Manager
* 
* @author Open Source Excellence, Jays.Soho {@link  http://www.opensource-excellence.com}
* @author Created on 01-Apr-2010
* @copyright Copyright (C) 2010 Open Source Excellence. All rights reserved. 
* @license GNU/GPL v2  http://www.gnu.org/copyleft/gpl.html
 */
 
// no direct access
defined( '_JEXEC' ) or die( ';)' );

class UpmanWearMscaddon extends upman
{
	var $ext_id ;
	
	protected 
	$com = null, 
	$name = null, 
	$link = null,
	$path = null;

	function __construct()
	{
		$this->db =& JFactory::getDBO();
	}
	
	function setup($p_dir,$parent = 0)
	{
		$this->set('link','index.php?option=com_upman&view=upman');
		
		// Get the Name
		$this->getName($p_dir);
		
		// Rename the Folder
		$this->renameFolder($p_dir);
		
		$p_dir = $this->get('path');
		$folder 	= $this->get('folder');
		$name 	= $this->get('name');
		$desc 		= $this->get('description');
		$desc		= $this->db->quote($desc);
		// Mark down the info. of component.
		// -> 
		$query = " SELECT count(*) FROM `#__upman` WHERE folder = '{$folder}' AND parent='{$parent}'";
		$this->db->setQuery($query);
		
		if($this->db->loadResult() > 0)
		{
			$query = " UPDATE `#__upman` SET name = '{$name}', description = {$desc},date_add = CURDATE()"
					." WHERE folder = '{$folder}' ";
					;
		}
		else
		{
			$query = " INSERT INTO `#__upman` (name,folder,parent,type,description,date_add) "
				." VALUES ('{$name}','{$folder}','{$parent}','mscaddon',{$desc},CURDATE()) "
				;
		}
		
		$this->db->setQuery($query);
		if(!$this->db->query())
		{
			JFolder::delete($p_dir);
			$this->errorReturn(JText::_('Can Not Setup the Msc Addon Pack:'.$name));
		}
		else
		{
			return true;
		}
	}
	
	function getName($path)
	{
		$osemsc = array();
		
		require_once($path.DS.'info.php');
		
		if(count($osemsc) == 1)
        {
	        foreach ($osemsc as $key => $value)
		    {
	            $name = $value->title;
	            $folder = $value->name;
	            $description = $value->description;
	        }
	    
        }
        
        
		
		$this->set('description', $description);
        
        $this->set('name',$name);
        $this->set('folder','mscaddon_'.$folder);
		
	}
	
	function renameFolder($path)
	{
		$folder = $this->get('folder');
		
		$r_path = str_replace(basename($path),$folder,$path);
		
		if(JFolder::exists( $r_path ))
		{
			JFolder::delete( $r_path );
		}
		
		if(JFolder::move($path,$r_path))
		{
			$this->set('path',$r_path);
			return true;
		}
		else
		{
			JFolder::delete($path);
			$this->errorReturn(JText::_('Can Not Rename the Msc add-on Folder '.basename($path).' To '.$folder));
		}
	}
	
	function install($path,$obj)
	{
		
		$folder = str_replace('mscaddon_','',basename($path));
		
		
		$des = JPATH_ADMINISTRATOR.DS.'components'.DS.'com_osemsc'.DS.'warehouse'.DS.$folder;
		//echo $des;exit;
		
		if(!JFolder::copy($path,$des,null,true))
		{
			return false;
		}	
		
		$osemsc = array(); $addon = null;
		require_once($path.DS.'info.php');
		
		if(count($osemsc) ==  1)
		{
			foreach( $osemsc as $value)
			{
				$addon = $value;
			}
		}
		
		$query = " SELECT id FROM `#__osemsc_addons` WHERE name = '{$addon->name}' ";
        $this->db->setQuery($query);
        $result = $this->db->loadResult();
        
        if(empty($result))
        {
        	$panel = ($addon->panel)?1:0;
        	$config = ($addon->config)?1:0;
        	$member = ($addon->member)?1:0;
        	$query = " INSERT INTO `#__osemsc_addons` "
        			." (name,title,type,panel,config,member) VALUES "
        			." ('{$addon->name}','{$addon->title}','{$addon->type}','{$panel}','{$config}','{$member}')"
        			;
        	$this->db->setQuery($query);
        	if(!$this->db->query())
        	{
        		return false;
        	}
        }
		
		
		
		// Update the Status...
		$query = " UPDATE `#__upman` SET ext_id = (SELECT id FROM `#__osemsc_addons` WHERE name = '{$folder}'), status = '1' "
				." WHERE id = '{$obj->id}' ";
		$this->db->setQuery($query);
		if(!$this->db->query())
		{
			return false;
		}

		return true;
	}

	function uninstall($obj)
	{	
		$addon_name = str_replace('mscaddon_','',$obj->folder);
		$folder = JPATH_ADMINISTRATOR.DS.'components'.DS.'com_osemsc'.DS. 'warehouse' . DS .$addon_name;
		
		if(JFolder::delete($folder))
        {
        	$query = " DELETE FROM `#__osemsc_addons` WHERE name = '{$addon_name}'";
        	$this->db->setQuery($query);
        	if(!$this->db->query())
        	{
        		return false;
        	}
        }
        
        return true;
		
	}
	
	
	function getItems($obj)
	{
		return $this->resetType($obj);
	}
	
	function resetType($obj)
	{
		$query = " SELECT id FROM `#__osemsc_addons` WHERE name = '".str_replace('mscaddon_','',$obj->folder)."'";
		$this->db->setQuery($query);
		$id = $this->db->loadResult();

		if(empty($id))
		{
			$obj->ext_id = 0;
		}
		else
		{
			if($id != $obj->ext_id)
			{
				$obj->ext_id = $id;
			}
		}

		if(!empty($obj->ext_id))
		{
			$obj->status = 1;
			//$obj->type = 'component&patch';
		}
		else{
			$obj->status = 0;
			//$obj->type = 'component';
		}

		$query = " UPDATE `#__upman` SET ext_id = '{$obj->ext_id}', type = '{$obj->type}', status = '{$obj->status}' WHERE folder = '{$obj->folder}' ";
		$this->db->setQuery($query);
		if(!$this->db->query())
		{
			return false;
		}
		
		switch($obj->status)
		{
			case(0):
				$obj->status = 'Not Install Yet';
				break;
				
			case(1):
				$obj->status = 'Installed';
				break;
		}

		return $obj;
	}
}


?>