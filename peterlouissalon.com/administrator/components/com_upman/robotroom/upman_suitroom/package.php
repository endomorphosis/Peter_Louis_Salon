<?php
/**
* @subpackage OSE Update Manager
*
* @author Open Source Excellence, Jays.Soho {@link  http://www.opensource-excellence.com}
* @author Created on 01-Apr-2010
* @copyright Copyright (C) 2010 Open Source Excellence. All rights reserved.
* @license GNU/GPL v2  http://www.gnu.org/copyleft/gpl.html
 */
// no direct access
defined('_JEXEC') or die(';)');
class upmanWearPackage extends upman {
	var $ext_id;
	protected $com= null, $name= null, $link= null, $path= null;
	function __construct() {
		$this->db= & JFactory :: getDBO();
	}
	function setup($p_dir, $parent= 0) {
		$this->set('link', 'index.php?option=com_upman&view=upman');
		$this->renameFolder($p_dir);
		$p_dir= $this->get('path');
		$this->getName($p_dir);
		$name= $this->get('name');
		$folder= $this->get('folder');
		$files= array();
		$return = true;
		//echo dirname($p_dir).DS.$folder;
		$files= $this->getZips(dirname($p_dir).DS.$folder);
		if(count($files) < 1) {
			JFolder :: delete(dirname($p_dir).DS.$folder);
		} else {
			$query= " SELECT id FROM `#__upman` WHERE folder = '{$folder}' AND parent='{$parent}'";
			$this->db->setQuery($query);
			$id= $this->db->loadResult();
			if($id > 0) {
				$query= " UPDATE `#__upman` SET name = '{$name}',date_add = CURDATE() ".
						" WHERE folder = '{$folder}' ";
				;
				$this->db->setQuery($query);
				$this->db->query();
				$parent= $id;
			} else {
				$query= " INSERT INTO `#__upman` (name,folder,parent,type,date_add) ".
						" VALUES ('{$name}','{$folder}','{$parent}','package',CURDATE()) ";
				$this->db->setQuery($query);
				$this->db->query();
				$parent= $this->db->insertid();
			}

			foreach($files as $file) {
				$response = parent :: setup($file, $parent, true) ;
				if ($response ==true)
				{
					$return = true;
				}
				else
				{
					$return=false;
				}
			}
		}
		return $return;
	}
	function getZips($p_dir) {
		$files= JFolder :: files($p_dir, '\.zip$', true, true);
		if(empty($files)) {
			$files= JFolder :: files($p_dir, '\.tar.gz$', true, true);
		}
		return $files;
		/*
		foreach($files as $file)
		{
			$zipname = basename($file,'.zip');

			$prefix = substr($zipname,0,3);

			switch($prefix)
			{
				case('com_'):
					$zip['com'] = $file;
					break;

				case('mod_'):
					$zip['mod'] = $file;
					break;

				case('plg_'):
					$zip['plg'] = $file;
					break;

				case('oup_'): // oup = original updater pack
					$zip['oup'] = $file;
					break;
			}
		}

		return $zip;
		*/
	}
	function getName($path) {
		$name= basename($path);
		$this->set('folder', $name);
		$this->set('name', str_replace('package_', '', $name));
		$this->set('description', '');
	}
	function renameFolder($path) {
		$name= basename($path);
		$name= strtolower($name);
		if(JFolder :: exists(dirname($path).DS.'package_'.$name)) {
			JFolder :: delete(dirname($path).DS.'package_'.$name);
		}
		if(JFolder :: move($path, dirname($path).DS.'package_'.$name)) {
			$this->set('path', dirname($path).DS.'package_'.$name);
			return true;
		} else {
			JFolder :: delete($path);
			$this->errorReturn(JText :: _('Can Not Rename the Package Folder:'.$name));
		}
	}
	function getItems($obj) {
		$items= array();
		$query= " SELECT * FROM `#__upman` WHERE parent = '{$obj->id}'";
		$this->db->setQuery($query);
		$subobjs= $this->db->loadObjectList();
		foreach($subobjs as $subobj) {
			$suit= $this->getSuit($subobj->type);
			$items[]= $suit->getItems($subobj);
		}
		return $items;
	}
	function install($path, $obj) {
		$installer= & JInstaller :: getInstance();
		if($obj->status == 1) {
			$installer->setOverwrite(true);
		}
		if(!$installer->install($path)) {
			return false;
		}
		jimport( 'joomla.version' );
		$version = new JVersion();
		$version = substr($version->getShortVersion(),0,3);
		if ($version =='1.5')
		{
			$query= " UPDATE `#__upman` SET ext_id = (SELECT id FROM `#__components` WHERE link = 'option={$obj->folder}'), status = '1' ".		" WHERE id = '{$obj->id}' ";
			$this->db->setQuery($query);
			if(!$this->db->query()) {
				return false;
			}
		}
		return true;
	}
	function uninstall($obj) {
		$installer= & JInstaller :: getInstance();
		if(empty($obj->ext_id)) {
			// Report
			return false;
		}
		//echo $obj->ext_id;exit;
		if(!$installer->uninstall($obj->type, $obj->ext_id, 0)) {
			return false;
		}
		// Update the Status...
		$query= " UPDATE `#__upman` SET ext_id = '0', status = '0' WHERE id = '{$obj->id}' ";
		$this->db->setQuery($query);
		if(!$this->db->query()) {
			return false;
		}
		return true;
	}
	function resetType($obj) {
		$query= " SELECT id FROM `#__components` WHERE link = 'option={$obj->folder}'";
		$this->db->setQuery($query);
		$id= $this->db->loadResult();
		if(empty($id)) {
			$obj->ext_id= 0;
		} else {
			if($id != $obj->ext_id) {
				$obj->ext_id= $id;
			}
		}
		if(!empty($obj->ext_id)) {
			$obj->status= 1;
			//$obj->type = 'component&patch';
		} else {
			$obj->status= 0;
			//$obj->type = 'component';
		}
		$query= " UPDATE `#__upman` SET ext_id = '{$obj->ext_id}', type = '{$obj->type}', status = '{$obj->status}' WHERE folder = '{$obj->folder}' ";
		$this->db->setQuery($query);
		if(!$this->db->query()) {
			return false;
		}
		switch($obj->status) {
			case(0) :
				$obj->status= 'Not Install Yet';
				break;
			case(1) :
				$obj->status= 'Installed';
				break;
		}
		return $obj;
	}
}
?>