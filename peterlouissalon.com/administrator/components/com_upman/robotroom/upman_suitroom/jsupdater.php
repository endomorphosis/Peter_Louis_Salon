<?php
/**
* @subpackage OSE Update Manager
* 
* @author Open Source Excellence, Jays.Soho {@link  http://www.opensource-excellence.com}
* @author Created on 01-Apr-2010
* @copyright Copyright (C) 2010 Open Source Excellence. All rights reserved. 
* @license GNU/GPL v2  http://www.gnu.org/copyleft/gpl.html
 */
 
// no direct access
defined( '_JEXEC' ) or die( ';)' );

class UpmanWearJsupdater extends upman
{
	var $ext_id ;
	
	protected 
	$com = null, 
	$name = null, 
	$link = null,
	$path = null;

	function __construct()
	{
		$this->db =& JFactory::getDBO();
	}
	
	function setup($p_dir,$parent = 0)
	{
		$this->set('link','index.php?option=com_upman&view=upman');
		
		// Get the Name
		$this->getName($p_dir);
		
		// Rename the Folder
		$this->renameFolder($p_dir);
		
		$p_dir = $this->get('path');
		$folder 	= $this->get('folder');
		$name 	= $this->get('name');
		// Mark down the info. of component.
		// -> 
		$query = " SELECT count(*) FROM `#__upman` WHERE folder = '{$folder}'";
		$this->db->setQuery($query);
		
		if($this->db->loadResult() > 0)
		{
			$query = " UPDATE `#__upman` SET name = '{$name}' ,date_add=CURDATE()"
					." WHERE folder = '{$folder}' ";
					;
		}
		else
		{
			$query = " INSERT INTO `#__upman` (name,folder,parent,type,status,description,date_add) "
					." VALUES ('{$name}','{$folder}','{$parent}','jsupdater','0','',CURDATE()) "
					;
		}
		
		$this->db->setQuery($query);
		if(!$this->db->query())
		{
			JFolder::delete($p_dir);
			$this->errorReturn(JText::_('Can Not Setup the Jsupdater:'.$name));
		}
		else
		{
			return true;
		}
	}
	
	function getItems($obj)
	{
		if($obj->status == 1)
		{
			$obj->status = 'Have Been Upgraded';
		}
		else
		{
			$obj->status = 'Joomla Original Files';
		}
		
		return $obj;
	}
	
	
	function getName($path)
	{
		$name = null;
		$installer = null;
		$installer =& new JInstaller;
		//$installer =& JInstaller::getInstance();
		$installer->setPath('source', $path);
		$manifest = $installer->getManifest();

		$document =& $manifest->document;
			
		$name =& $document->getElementByPath('name');		
		$name = JFilterInput::clean(str_replace(' ','_',$name->data()), 'cmd');
		
		$description = $document->getElementByPath('description');
		$description = $description->data();
		
		$this->set('description', $description);	

		$this->set('name',$name);
		$this->set('folder', 'jsupdater_'.strtolower($name));
	}
	
	function renameFolder($path)
	{
		$folder = $this->get('folder');
		
		$r_path = str_replace(basename($path),$folder,$path);
		
		if(JFolder::exists( $r_path ))
		{
			JFolder::delete( $r_path );
		}
		
		if(JFolder::move($path,$r_path))
		{
			$this->set('path',$r_path);
			return true;
		}
		else
		{
			JFolder::delete($path);
			$this->errorReturn(JText::_('Can Not Rename the Jsupdater Folder'.basename($path).' To '.$folder));
		}
	}
	
	function install($path,$obj)
	{
		$this->set('link','index.php?option=com_upman&view=installer&parent='.$this->parent.'&'. JUtility::getToken() .'=1');
		// set Restore for the original files
		$this->set('path',$path);
		//echo $this->path;exit;
		$upgrade = $path . DS . 'upgrade';
		
		$manage = $this->manageInstance();
		
		if(!JFolder::exists($path.DS.'restore'))
		{
			if(!$this->setRestore($manage))
			{
				$this->errorReturn(JText::_('Fail to Create backup folders!'));
			}
		}

		// load the upgrade files info
		
		$error = 0 ;
		
		if( count($manage->upg_sql) > 0)
		{
			foreach($manage->upg_sql as $query)
			{
				$this->db->setQuery($query);
				if(!$this->db->query())
				{
					JError::raiseWarning('SOME_ERROR_CODE', JText::_( 'SQL query upgrade fails'));
					$error++;
				}
			}
		}

		if( count($manage->cover_files) > 0 )
		{
			foreach ( $manage->cover_files as $file )
			{
				$src = $upgrade . DS . $file;
				$des = $manage->path . DS . $file;
					
				if(!JFile::exists($des))
				{
					JError::raiseWarning('SOME_ERROR_CODE', JText::_( 'Can not find the upgrade files to be covered'));
					$error++;
				}
					
				if(!JFile::copy($src,$des))
				{
					JError::raiseWarning('SOME_ERROR_CODE', JText::_( "File: $src, upgrade fails"));
					$error++;
				}
			}
		}

		if( count($manage->add_files) > 0 )
		{
			foreach ( $manage->add_files as $file )
			{
				$src = $upgrade . DS . $file;
				$des = $manage->path . DS . $file;

				$tmpl = explode(DS,$file);
				$arr = array();
				// check whether the directory exists
				if(count($tmpl) >= 2)
				{
					for($i=0; $i < (count($tmpl)-1); $i++)
					{
						$arr[] = $tmpl[$i];
						$path = implode(DS,$arr);
							
						if(!JFolder::exists($manage->path .DS.$path))
						{
							JFolder::create($manage->path .DS. $path);
						}
					}

				}

				if(!JFile::copy($src,$des))
				{
					JError::raiseWarning('SOME_ERROR_CODE', JText::_( "File: $src, upgrade fails"));
					$error++;
				}
			}
		}
		
		if($error > 0)
		{
			$this->errorReturn();
		}
		
		// Update the Status...
		$query = " UPDATE `#__upman` SET ext_id = 0, status = '1' "
				." WHERE id = '{$obj->id}' ";
		$this->db->setQuery($query);
		if(!$this->db->query())
		{
			return false;
		}

		return true;
	}

	function setRestore($manage)
	{
		//echo $this->path.DS.'manage.php';exit;
		//require_once($this->get('path').DS.'manage.php');
		
		//$manage = $this->manageInstance();

		$restore = $this->get('path') .DS. 'restore';

		JFolder::create($restore);
		//echo count($manage->cover_files);exit;
		foreach ($manage->cover_files as $file)
		{
			$arr = array();
				
			$src = $manage->path.DS.$file;
			$des = $restore .DS.$file;
				
			$tmpl = explode(DS,$file);
				
			// check whether the directory exists
			if(count($tmpl) >= 2)
			{
				for($i=0; $i < (count($tmpl)-1); $i++)
				{
					$arr[] = $tmpl[$i];
					$path = implode(DS,$arr);
						
					if(!JFolder::exists($restore .DS.$path))
					{
						JFolder::create($restore .DS. $path);
					}
				}
			}
				
			if(!JFile::copy($src,$des))
			{
				JFolder::delete($restore);
				return false;
			}
		}

		return true;
	}
	
	function uninstall($obj)
	{
		$parent = JRequest::getVar('parent',null);
		
		if($obj->id == $parent)
		{
			$path = JPATH_COMPONENT.DS.'up_files'.DS.$obj->folder;
		}
		else
		{
			$path = JPATH_COMPONENT.DS.'up_files'.DS.$this->getPath(0,$obj->id);
		}
		
		if($this->restore($path))
		{
			// Update the Status...
			$query = " UPDATE `#__upman` SET ext_id = 0, status = '0' "
					." WHERE id = '{$obj->id}' ";
			$this->db->setQuery($query);
			if(!$this->db->query())
			{
				return false;
			}
			
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function restore($path)
	{
		$this->set('path',$path);
		//echo $this->path;exit;
		$restore = $path . DS . 'restore';

		//require_once($path.DS.'manage.php');

		$manage = $this->manageInstance();
		
		$error = 0;
		
		if(!empty($manage->res_sql))
		{
			foreach($manage->res_sql as $query)
			{
				$this->db->setQuery($query);
				if($this->db->query())
				{
					JError::raiseWarning('SOME_ERROR_CODE', JText::_( 'SQL query restoration fails'));
					$error++;
				}
			}
		}

		if(!empty($manage->cover_files))
		{
			foreach ($manage->cover_files as $file)
			{
				$src = $restore . DS . $file;
				$des = $manage->path . DS . $file;
					
				if(!JFile::copy($src,$des))
				{
					JError::raiseWarning('SOME_ERROR_CODE', JText::_( "File : {$src} fails to restore"));
					$error++;
				}
			}
		}

		if(!empty($manage->add_files))
		{
			foreach ($manage->add_files as $file )
			{
				$des = $manage->path . DS . $file;
				JFile::delete($des);
			}
				
		}
		
		if($error > 0 )
		{
			$this->errorReturn();
		}
		
		return true;
	}
	
	function manageInstance()
	{
		if(class_exists('manage'))
		{
			$manage = new stdClass();
			
			include_once($this->get('path').DS.'manage2.php');
			
			return $manage;
		}
		else
		{
			include_once($this->get('path').DS.'manage.php');
			$manage = new manage();
			$manage->set_files();
			return $manage;
		}
		
	}
}


?>