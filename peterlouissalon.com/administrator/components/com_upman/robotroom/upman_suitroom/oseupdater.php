<?php
/**
* @subpackage OSE Update Manager
*
* @author Open Source Excellence, Jays.Soho {@link  http://www.opensource-excellence.com}
* @author Created on 01-Apr-2010
* @copyright Copyright (C) 2010 Open Source Excellence. All rights reserved.
* @license GNU/GPL v2  http://www.gnu.org/copyleft/gpl.html
 */


// no direct access
defined( '_JEXEC' ) or die( ';)' );

class UpmanWearOseupdater extends upman
{
	var $ext_id ;

	protected
	$com = null,
	$name = null,
	$link = null,
	$path = null;

	function __construct()
	{
		$this->db =& JFactory::getDBO();
	}

	function setup($p_dir,$parent = 0)
	{
		$this->set('link','index.php?option=com_upman&view=upman');

		// Get the Name
		$this->getName($p_dir);

		// Rename the Folder
		$this->renameFolder($p_dir);

		$p_dir = $this->get('path');
		$folder 	= $this->get('folder');
		$name 	= $this->get('name');
		$desc 		= $this->get('description');
		$desc		= $this->db->quote($desc);
		// Mark down the info. of component.
		// ->
		$query = " SELECT count(*) FROM `#__upman` WHERE folder = '{$folder}' AND parent='{$parent}'";
		$this->db->setQuery($query);

		if($this->db->loadResult() > 0)
		{
			$query = " UPDATE `#__upman` SET name = '{$name}',description = {$desc},date_add = CURDATE() "
					." WHERE folder = '{$folder}' ";
					;
		}
		else
		{
			$query = " INSERT INTO `#__upman` (name,folder,parent,type,status,description,date_add) "
					." VALUES ('{$name}','{$folder}','{$parent}','oseupdater','0',{$desc},CURDATE()) "
					;
		}

		$this->db->setQuery($query);
		if(!$this->db->query())
		{
			JFolder::delete($p_dir);
			return false;
		}
		else
		{
			return true;
		}
	}

	function getItems($obj)
	{
		if($obj->status == 1)
		{
			$obj->status = 'Have Been Upgraded';
		}
		else
		{
			$obj->status = 'Joomla Original Files';
		}

		return $obj;
	}


	function getName($path)
	{
		$name = null;
		$installer = null;
		$installer =& new JInstaller;
		//$installer =& JInstaller::getInstance();
		$installer->setPath('source', $path);
		$manifest = $installer->getManifest();

		jimport( 'joomla.version' );
		$version = new JVersion();
		$version = substr($version->getShortVersion(),0,3);

		if ($version =='1.6')
		{
			$name = strtolower(JFilterInput::getInstance()->clean((string)$manifest->name, 'cmd'));
			$description = JFilterInput::getInstance()->clean((string)$manifest->description, 'string');
			$menu = JFilterInput::getInstance()->clean((string)$manifest->administration->menu, 'string');
			if (empty($menu))
			{
				$menu=$name;
			}
		}
		else
		{
			$document =& $manifest->document;
			$name =& $document->getElementByPath('name');
			$name = JFilterInput::clean($name->data(), 'cmd');

			$adminElement		=& $document->getElementByPath('administration');
			$menu = $adminElement->getElementByPath('menu');
			if(is_a($menu,'JSimpleXMLElement'))
			{
				$menu = $menu->data();
			}
			else
			{
				$menu = $name;
			}

			$description = $document->getElementByPath('description');
			$description = JFilterInput::clean($description->data(), 'string');;
		}

		$this->set('description', $description);

		$this->set('name',$name);
		$this->set('folder', 'oseupdater_'.strtolower($name));
	}

	function renameFolder($path)
	{
		$folder = $this->get('folder');

		$r_path = str_replace(basename($path),$folder,$path);

		if(JFolder::exists( $r_path ))
		{
			JFolder::delete( $r_path );
		}

		if(JFolder::move($path,$r_path))
		{
			$this->set('path',$r_path);
			return true;
		}
		else
		{
			JFolder::delete($path);
			$this->errorReturn(JText::_('Can Not Rename the Oseupdater Folder '.basename($path).' To '.$folder));
		}
	}

	function install($path,$obj)
	{
		//$this->set('link','index.php?option=com_upman&view=installer&parent='.$this->parent.'&'. JUtility::getToken() .'=1');

		// set Restore for the original files
		$this->set('path',$path);
		$upgrade = $path . DS . 'upgrade';
		$manage = $this->manageInstance();
		// Check whether have authority to create folder
		if(!JFolder::exists($path.DS.'restore'))
		{
			if(!$this->setRestore($manage))
			{
				$this->errorReturn(JText::_('Fail to Create backup folders!'));
			}
		}
		// load the upgrade files info

		$error = 0 ;

		if( count($manage->upg_sql) > 0)
		{
			foreach($manage->upg_sql as $query)
			{
				$this->db->setQuery($query);
				if(!$this->db->query())
				{
					JError::raiseWarning('SOME_ERROR_CODE', JText::_( 'SQL query upgrade fails'));
					$this->errorReturn();
				}
			}
		}

		if( count($manage->cover_files) > 0 )
		{
			foreach ( $manage->cover_files as $file )
			{
				$src = $upgrade . DS . $file;
				$des = $manage->path . DS . $file;

				if(!$this->buildFile($src,$des))
				{
					$this->errorReturn();
				}

				/*
				if(!JFile::exists($des))
				{
					JError::raiseWarning('SOME_ERROR_CODE', JText::_( 'Can not find the files need upgraded'));
					$error++;
				}

				if(!$this->checkAuth($des))
				{
					$this->errorReturn(JText::_('You do not Have Authority to Overwrite the File!'));
				}

				if(!JFile::copy($src,$des))
				{
					JError::raiseWarning('SOME_ERROR_CODE', JText::_( "File: $src, upgrade fails"));
					$error++;
				}
				*/
			}
		}

		if( count($manage->add_files) > 0 )
		{
			foreach ( $manage->add_files as $file )
			{
				$src = $upgrade . DS . $file;
				$des = $manage->path . DS . $file;

				$tmpl = explode(DS,$file);

				if(!JFolder::create(dirname($des)))
				{
					JError::raiseWarning('SOME_ERROR_CODE', JText::_( "Can Not Create the Folder"));

					$this->errorReturn();
				}

				if(!$this->buildFile($src,$des))
				{
					$this->errorReturn();
				}

			}
		}



		// Update the Status...
		$query = " UPDATE `#__upman` SET ext_id = 0, status = '1' "
				." WHERE id = '{$obj->id}' ";
		$this->db->setQuery($query);

		if(!$this->db->query())
		{
			return false;
		}

		return true;
	}

	function setRestore($manage)
	{
		$restore = $this->get('path') .DS. 'restore';

		if(!JFolder::create($restore))
		{
			return false;
		}

		//echo count($manage->cover_files);exit;
		foreach ($manage->cover_files as $file)
		{
			$src = $manage->path.DS.$file;

			// Build the Folder in Folder 'Restore'
			$des = $restore .DS.$file;
			if(!JFolder::create(dirname($des)))
			{
				return false;
			}

			if(!$this->buildFile($src,$des))
			{
				JFolder::delete($restore);
				return false;
			}


		}

		return true;
	}

	function uninstall($obj)
	{
		$parent = JRequest::getVar('parent',null);

		if($obj->id == $parent)
		{
			$path = JPATH_COMPONENT.DS.'up_files'.DS.$obj->folder;
		}
		else
		{
			$path = JPATH_COMPONENT.DS.'up_files'.DS.$this->getPath(0,$obj->id);
		}

		if(!JFolder::exists($path.DS.'restore'))
		{
			return false;
		}

		if($this->restore($path))
		{
			// Update the Status...
			$query = " UPDATE `#__upman` SET ext_id = 0, status = '0' "
					." WHERE id = '{$obj->id}' ";
			$this->db->setQuery($query);
			if(!$this->db->query())
			{
				return false;
			}

			return true;
		}
		else
		{
			return false;
		}
	}

	function restore($path)
	{
		$this->set('path',$path);
		//echo $this->path;exit;
		$restore = $path . DS . 'restore';

		//require_once($path.DS.'manage.php');

		$manage = $this->manageInstance();

		$error = 0;

		if(!empty($manage->res_sql))
		{
			foreach($manage->res_sql as $query)
			{
				$this->db->setQuery($query);
				if($this->db->query())
				{
					JError::raiseWarning('SOME_ERROR_CODE', JText::_( 'SQL query restoration fails'));
					$this->errorReturn();
				}
			}
		}

		if(!empty($manage->cover_files))
		{
			foreach ($manage->cover_files as $file)
			{
				$src = $restore . DS . $file;
				$des = $manage->path . DS . $file;

				if(!$this->buildFile($src,$des))
				{
					JError::raiseWarning('SOME_ERROR_CODE', JText::_( "File : {$src} fails to restore"));
					$this->errorReturn();
				}
			}
		}

		if(!empty($manage->add_files))
		{
			foreach ($manage->add_files as $file )
			{
				$des = $manage->path . DS . $file;
				JFile::delete($des);
			}

		}

		return true;
	}

	function manageInstance()
	{
		if(class_exists('manage'))
		{
			$manage = new stdClass();

			include_once($this->get('path').DS.'manage2.php');

			return $manage;
		}
		else
		{
			include_once($this->get('path').DS.'manage.php');
			$manage = new manage();
			$manage->set_files();
			return $manage;
		}

	}

	function checkAuth($fname)
	{
		if(JFile::exists($fname))
		{
			if (JPath::isOwner($fname) && !JPath::setPermissions($fname, '0644'))
			{
				JError::raiseWarning('SOME_ERROR_CODE', 'Could not make '.$fname.' writable');
				return false;
			}
			else
			{
				return true;
			}
		}

		return true;
	}

	function buildFile($src,$des)
	{
		$result  = true;

		// check whether own the authority to create a file or overwrite it
		if(!JFile::copy($src,$des))
		{
			$result = false;
		}

		if(!$result)
		{
			if($this->checkAuth($des))
			{
				$content = JFile::read($src);
				if(!JFile::write($des,$content))
				{
					$result = false;
				}
			}
		}

		if(!$result)
		{
			JError::raiseWarning('SOME_ERROR_CODE', 'Can Not Upgrade the File, Maybe You Do Not Have Authority!');
			return false;
		}

		return true;
	}
}


?>