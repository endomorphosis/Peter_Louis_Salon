<?php
/**
* @subpackage OSE Update Manager
*
* @author Open Source Excellence, Jays.Soho {@link  http://www.opensource-excellence.com}
* @author Created on 01-Apr-2010
* @copyright Copyright (C) 2010 Open Source Excellence. All rights reserved.
* @license GNU/GPL v2  http://www.gnu.org/copyleft/gpl.html
 */
// no direct access
defined( '_JEXEC' ) or die( ';)' );

class UpmanWearPlugin extends upman
{
	var $ext_id ;

	protected
	$com = null,
	$name = null,
	$link = null,
	$path = null;

	function __construct()
	{
		$this->db =& JFactory::getDBO();
	}

	function setup($p_dir,$parent = 0)
	{

		$this->set('link','index.php?option=com_upman&view=upman');

		// Get the Name
		$this->getName($p_dir);

		// Rename the Folder
		$this->renameFolder($p_dir);
		$p_dir = $this->get('path');
		$folder 	= $this->get('folder');
		$name 	= $this->get('name');
		$desc 		= $this->get('description');
		$desc		= $this->db->quote($desc);
		// Mark down the info. of component.
		// ->
		$query = " SELECT count(*) FROM `#__upman` WHERE folder = '{$folder}' AND parent='{$parent}'";
		$this->db->setQuery($query);

		if($this->db->loadResult() > 0)
		{
			$query = " UPDATE `#__upman` SET name = '{$name}',description = {$desc},date_add = CURDATE() "
					." WHERE folder = '{$folder}' "
					;
		}
		else
		{
			$query = " INSERT INTO `#__upman` (name,folder,parent,type,description,date_add) "
					." VALUES ('{$name}','{$folder}','{$parent}','plugin',{$desc},CURDATE()) "
					;
		}
		$this->db->setQuery($query);
		if(!$this->db->query())
		{
			JFolder::delete($p_dir);
			$this->errorReturn(JText::_('Can Not Setup the Plugin:'.$name));
		}
		else
		{
			return true;
		}
	}

	function getName($path)
	{
		$name = null;
		$installer = null;
		$installer =& new JInstaller;
		//$installer =& JInstaller::getInstance();
		$installer->setPath('source', $path);
		$manifest = $installer->getManifest();

		jimport( 'joomla.version' );
		$version = new JVersion();
		$version = substr($version->getShortVersion(),0,3);

		if ($version =='1.6')
		{
			$name = strtolower(JFilterInput::getInstance()->clean((string)$manifest->name, 'cmd'));
			$name= explode("-", $name);
			$name = $name[1];
			$description = JFilterInput::getInstance()->clean((string)$manifest->description, 'string');
			$menu = JFilterInput::getInstance()->clean((string)$manifest->administration->menu, 'string');
			if (empty($menu))
			{
				$menu=$name;
			}
		}
		else
		{
			$document =& $manifest->document;
			$name =& $document->getElementByPath('name');
			$name = JFilterInput::clean($name->data(), 'cmd');
			$element =& $document->getElementByPath('files');
			if (is_a($element, 'JSimpleXMLElement') && count($element->children()))
			{
				$files = $element->children();
				foreach ($files as $file)
				{
					if ($file->attributes('plugin'))
					{
						$name = $file->attributes('plugin');
						break;
					}
				}
			}
			$description = $document->getElementByPath('description');
			$description = $description->data();
		}
		$this->set('name',$name);
		$this->set('description', $description);
		$this->set('folder', 'plg_'.strtolower($name));
		//$this->set('folder', 'plg_'.uniqid());
	}

	function renameFolder($path)
	{
		$folder = $this->get('folder');

		$r_path = str_replace(basename($path),$folder,$path);

		if(JFolder::exists( $r_path ))
		{
			JFolder::delete( $r_path );
		}

		if(JFolder::move($path,$r_path))
		{
			$this->set('path',$r_path);
			return true;
		}
		else
		{
			JFolder::delete($path);
			$this->errorReturn(JText::_('Can Not Rename the Plugin Folder '.basename($path).' To '.$folder));
		}
	}

	function getItems($obj)
	{
		return $this->resetType($obj);
	}

	function resetType($obj)
	{
		$parent = JRequest::getInt('parent',null);
		//echo $parent.$obj->id;
		if($parent == $obj->id)
		{
			$path =  JPATH_COMPONENT.DS.'up_files'.DS.$obj->folder;
		}
		else
		{
			$path = JPATH_COMPONENT.DS.'up_files'.DS.$this->getPath(0,$obj->id);
		}

		$installer =& new JInstaller;
		$installer->setPath('source', $path);
		$manifest = $installer->getManifest();
		$document =& $manifest->document;
		$group = $document->attributes('group');
		$element = str_replace('plg_','',$obj->folder);

		if (OSEJVERSION=='J16')
		{
			$query = " SELECT extension_id FROM `#__extensions` WHERE folder = '{$group}' AND element = '{$element}'";
		}
		else
		{
			$query = " SELECT id FROM `#__plugins` WHERE folder = '{$group}' AND element = '{$element}'";
		}

		$this->db->setQuery($query);
		$id = $this->db->loadResult();

		if(empty($id))
		{
			$obj->ext_id = 0;
		}
		else
		{
			if($id != $obj->ext_id)
			{
				$obj->ext_id = $id;
			}
		}

		if(!empty($obj->ext_id))
		{
			$obj->status = 1;
			//$obj->type = 'module&patch';

		}
		else{
			$obj->status = 0;
			//$obj->type = 'module';
		}

		$query = " UPDATE `#__upman` SET ext_id = '{$obj->ext_id}', type = '{$obj->type}' , status = '{$obj->status}' WHERE folder = '{$obj->folder}' ";
		$this->db->setQuery($query);
		if(!$this->db->query())
		{
			return false;
		}

		switch($obj->status)
		{
			case(0):
				$obj->status = 'Not Install Yet';
				break;

			case(1):
				$obj->status = 'Installed';
				break;
		}

		return $obj;
	}

	function install($path,$obj)
	{
		$installer =& JInstaller::getInstance();

		if($obj->status == 1)
		{
			$installer->setOverwrite(true);
		}

		if(!$installer->install($path))
		{
			return false;
		}
		$installer =& new JInstaller;
		$installer->setPath('source', $path);
		$manifest = $installer->getManifest();

		jimport( 'joomla.version' );
		$version = new JVersion();
		$version = substr($version->getShortVersion(),0,3);
		if ($version =='1.6')
		{
			$group = strtolower(JFilterInput::getInstance()->clean((string)$manifest->attributes()->group[0], 'string'));
			$name = JFilterInput::getInstance()->clean((string)$manifest->name, 'string');

			$query = " UPDATE `#__upman` SET "
				." ext_id = (SELECT extension_id FROM `#__extensions` WHERE folder = '{$group}' AND name = '{$name}'), status = '1' "
				." WHERE id = '{$obj->id}' ";
		}
		else
		{
		$document =& $manifest->document;
		$group = $document->attributes('group');
		$element = str_replace('plg_','',$obj->folder);
		$query = " UPDATE `#__upman` SET "
				." ext_id = (SELECT id FROM `#__plugins` WHERE folder = '{$group}' AND element = '{$element}'), status = '1' "
				." WHERE id = '{$obj->id}' ";
		}

		$this->db->setQuery($query);
		if(!$this->db->query())
		{
			return false;
		}

		return true;
	}

	function uninstall($obj)
	{
		$installer =& JInstaller::getInstance();

		if(empty($obj->ext_id))
		{
			// Report
			return false;
		}

		if(!$installer->uninstall($obj->type ,$obj->ext_id , 0))
		{
			return false;
		}

		// Update the Status...
		$query = " UPDATE `#__upman` SET ext_id = '0', status = '0' WHERE id = '{$obj->id}' ";
		$this->db->setQuery($query);
		if(!$this->db->query())
		{
			return false;
		}
		return true;
	}
}
?>
