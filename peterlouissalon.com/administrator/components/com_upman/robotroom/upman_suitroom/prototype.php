<?php
/**
* @subpackage OSE Update Manager
* 
* @author Open Source Excellence, Jays.Soho {@link  http://www.opensource-excellence.com}
* @author Created on 01-Apr-2010
* @copyright Copyright (C) 2010 Open Source Excellence. All rights reserved. 
* @license GNU/GPL v2  http://www.gnu.org/copyleft/gpl.html
 */
 
 
// no direct access
defined( '_JEXEC' ) or die( ';)' );

class UpmanWearPrototype extends upman
{
	var $ext_id ;
	
	protected 
	$com = null, 
	$name = null, 
	$link = null,
	$path = null;

	function __construct()
	{
		$this->db =& JFactory::getDBO();
	}
	
	function setup($p_dir,$parent = 0)
	{
		$this->set('link','index.php?option=com_upman&view=upman');
		
		// Get the Name
		$this->getName($p_dir);
		
		// Rename the Folder
		$this->renameFolder($p_dir);
		
		$p_dir = $this->get('path');
		$folder 	= $this->get('folder');
		$name 	= $this->get('name');
		$desc 		= $this->get('description');
		$desc		= $this->db->quote($desc);
		// Mark down the info. of component.
		// -> 
		$query = " SELECT count(*) FROM `#__upman` WHERE folder = '{$folder}' AND parent='{$parent}'";
		$this->db->setQuery($query);
		
		if($this->db->loadResult() > 0)
		{
			$query = " UPDATE `#__upman` SET name = '{$name}', description = {$desc},date_add = CURDATE()"
					." WHERE folder = '{$folder}' ";
					;
		}
		else
		{
			$query = " INSERT INTO `#__upman` (name,folder,parent,type,status,description,date_add) "
					." VALUES ('{$name}','{$folder}','{$parent}','upman','0',{$desc},CURDATE()) "
					;
		}
		
		
		$this->db->setQuery($query);
		if(!$this->db->query())
		{
			JFolder::delete($p_dir);
			$this->errorReturn(JText::_('Can Not Setup the Upman Pack:'.$name));
		}
		else
		{
			return true;
		}
	}
	
	function getName($path)
	{
		$name = null;
		$installer = null;
		$installer =& new JInstaller;
		//$installer =& JInstaller::getInstance();
		$installer->setPath('source', $path);
		$manifest = $installer->getManifest();

		$document =& $manifest->document;
			
		$name =& $document->getElementByPath('name');
		$name = JFilterInput::clean($name->data(), 'cmd');

		$this->set('name',$name);
		
		$description = $document->getElementByPath('description');
		$description = $description->data();
		
		$this->set('description', $description);


		$this->set('folder', 'upman_'.strtolower($name));
	}
	
	function renameFolder($path)
	{
		$folder = $this->get('folder');
		
		$r_path = str_replace(basename($path),$folder,$path);
		
		if(JFolder::exists( $r_path ))
		{
			JFolder::delete( $r_path );
		}
		
		if(JFolder::move($path,$r_path))
		{
			$this->set('path',$r_path);
			return true;
		}
		else
		{
			JFolder::delete($path);
			$this->errorReturn(JText::_('Can Not Rename the Prototype Folder '.basename($path).' To '.$folder));
		}
	}
}


?>