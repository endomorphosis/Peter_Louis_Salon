<?php
// no direct access
defined( '_JEXEC' ) or die( ';)' );

jimport('joomla.filesystem.file');
jimport('joomla.filesystem.folder');

class P_Team_Recordman
{


	var $db = null;

	function __construct()
	{
		$this->db	=& JFactory::getDBO();
	}

	function recordPackage($name,$folder,$type)
	{
		if(empty($type))
		{
			$type = 'package';
		}

		$query = "SELECT id FROM `#__upman` WHERE folder = '{$folder}' AND parent = '0'";
		$this->db->setQuery($query);
		$parent = $this->db->loadResult();
			
		if(!empty($parent))
		{
			$query = " DELETE FROM `#__upman` WHERE parent = '{$parent}' ";
			$this->db->setQuery($query);
			if(!$this->db->query())
			{
				JFolder::delete($path);
				// Error
				return false;
			}
			else
			{
				return true;
			}
		}

		$query = " INSERT INTO `#__upman` (name,folder,parent,type) VALUES ('{$name}','{$folder}','0','{$type}') ";
		$this->db->setQuery($query);
		if(!$this->db->query())
		{
			return false;
		}

		return true;
	}

	function record($name,$folder,$sub_folder,$type)
	{
		if(empty($type))
		{
			return false;
		}

		$query = "SELECT id FROM `#__upman` WHERE folder = '{$folder}'";
		$this->db->setQuery($query);
		$parent = $this->db->loadResult();
			

		$query = " INSERT INTO `#__upman` (name,folder,parent,type) VALUES ('{$name}','{$sub_folder}','{$parent}','{$type}') ";
		$this->db->setQuery($query);
		if(!$this->db->query())
		{
			return false;
		}

		return true;
	}

}
?>