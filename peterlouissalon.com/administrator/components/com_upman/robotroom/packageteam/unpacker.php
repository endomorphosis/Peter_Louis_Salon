<?php
// no direct access
defined( '_JEXEC' ) or die( ';)' );

class P_Team_Unpacker extends P_Team
{
	var $folder 	= null;

	var $package 	= null;

	var $type		= null;

	var $menu		= null;



	function __construct($package)
	{
		$this->db	=& JFactory::getDBO();

		if(!$this->unpackage($package))
		{
			return false;
		}
	}

	function get($name)
	{
		if(!isset($this->{$name}))
		{
			return false;
		}

		return $this->{$name};
	}

	private function set($name,$value)
	{
		$this->{$name} = $value;
	}

	function unpackage($package)
	{
		if(!JFolder::exists(JPATH_COMPONENT.DS.'up_files'))
		{
			JFolder::create(JPATH_COMPONENT.DS.'up_files');
		}

		$dir = dirname($package['z_path']);
		$extractdir = JPath::clean($dir.DS.uniqid('tmpl_'));

		$archivename = JPath::clean($package['z_path']);
			
		$result = JArchive::extract( $archivename, $extractdir);

		if ( $result === false )
		{
			return false;
		}
		// delete the zip file
		JFile::delete($archivename);

		$type = $this->detectType($extractdir);

		if($type)
		{
			$this->type = $type;
			$name = $this->getname($extractdir) ;
				
		}
		else
		{
			$this->type = 'package';
			$name = $package['folder'];
		}

		$path = str_replace($package['zip'],$name,$package['z_path']);

		if(JFolder::exists($path))
		{
			JFolder::delete($path);
		}

		if(!rename($extractdir,$path))
		{
			JFolder::delete($extractdir);
			// Error Report
				
			return false;
		}

		$this->folder = JPath::clean($name);
		return true;
	}

	function unpack($package)
	{
		$dir = dirname($package['z_path']);
		$extractdir = JPath::clean($dir.DS.uniqid('tmpl_'));
		$archivename = JPath::clean($package['z_path']);
			
		$result = JArchive::extract( $archivename, $extractdir);

		if ( $result === false )
		{
			return false;
		}

		// delete the zip file
		JFile::delete($archivename);

		// Detect the extension type
		$type = $this->detectType($extractdir);

		if($type)
		{
				
			$this->type = $type;
			$name = $this->getName($extractdir) ;
				
		}
		else
		{
			JFolder::delete($extractdir);
			return false;
		}

		$path = str_replace($package['zip'],$name,$package['z_path']);

		if(!rename($extractdir,$path))
		{
			JFolder::delete($extractdir);
			// Error Report
				
			return false;
		}

		$this->folder = JPath::clean($name);
		return true;
	}

	function detectType($p_dir,$recurse = false)
	{
		// Search the install dir for an xml file
		$files = JFolder::files($p_dir, '\.xml$', $recurse, true);

		if (count($files) > 0)
		{

			foreach ($files as $file)
			{
				$xmlDoc = & JFactory::getXMLParser();
				$xmlDoc->resolveErrors(true);

				if (!$xmlDoc->loadXML($file, false, true))
				{
					// Free up memory from DOMIT parser
					unset ($xmlDoc);
					continue;
				}
				$root = & $xmlDoc->documentElement;
				if (!is_object($root) || ($root->getTagName() != "install" && $root->getTagName() != 'mosinstall'))
				{
					unset($xmlDoc);
					continue;
				}

				$type = $root->getAttribute('type');
				// Free up memory from DOMIT parser
				unset ($xmlDoc);
				return $type;
			}

				
			// Free up memory from DOMIT parser
			unset ($xmlDoc);
			return false;
		}
		else
		{
			return false;
		}
	}

	function getName($path)
	{
		$name = null;
		$installer = null;
		$installer =& new JInstaller;
		//$installer =& JInstaller::getInstance();
		$installer->setPath('source', $path);
		$manifest = $installer->getManifest();

		$document =& $manifest->document;
			
		switch($this->type)
		{
			case('component'):

				$name =& $document->getElementByPath('name');
				$name = JFilterInput::clean($name->data(), 'cmd');
				$name = 'com_'.strtolower($name);
				$menu = $document->getElementByPath('menu');
				$this->set('menu',$menu);
				break;

			case('module'):
				$name =& $document->getElementByPath('name');
				$name = JFilterInput::clean($name->data(), 'cmd');

				$this->set('menu',$name);

				$element =& $document->getElementByPath('files');

				if (is_a($element, 'JSimpleXMLElement') && count($element->children())) {
					$files = $element->children();
					foreach ($files as $file) {
						if ($file->attributes('module')) {
							$name = $file->attributes('module');
							break;
						}
					}
				}

				break;

			case('plugin'):
				// Set the installation path
				$name =& $document->getElementByPath('name');
				$name = JFilterInput::clean($name->data(), 'cmd');
				$this->set('menu',$name);

				$element =& $document->getElementByPath('files');
				if (is_a($element, 'JSimpleXMLElement') && count($element->children())) {
					$files = $element->children();
					foreach ($files as $file) {
						if ($file->attributes($this->type)) {
							$name = $file->attributes($this->type);
							break;
						}
					}
				}

				$name = 'plg_'.$name;
				break;

			default:

				break;
		}

		return $name;
	}


}

?>