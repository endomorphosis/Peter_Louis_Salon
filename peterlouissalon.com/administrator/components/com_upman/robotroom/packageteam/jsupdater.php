<?php
// no direct access
defined( '_JEXEC' ) or die( ';)' );

jimport('joomla.filesystem.file');
jimport('joomla.filesystem.folder');


class P_Team_Jsupdater
{
	function __construct()
	{

	}

	function check($path)
	{
		if(JFile::exists($path.DS.'manage.php') && JFolder::exists($path.DS.'upgrade'))
		{

			return true;
		}

		return false;
	}

	function renameFolder($folder)
	{
		if(str_replace('com_','',$folder))
		{
			return str_replace('com_','jsupdater_',$folder);
		}
		else
		{
			return $folder;
		}
	}
}

?>