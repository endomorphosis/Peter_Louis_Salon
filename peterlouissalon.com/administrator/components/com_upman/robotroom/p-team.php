<?php
// no direct access
defined( '_JEXEC' ) or die( ';)' );

class P_Team
{
	var $jsupdater = null;

	var $package = array();

	var $recordman = null;

	var $unpacker = null;

	function __construct($package)
	{
		$this->receive($package);
	}

	function receive($package)
	{
		$this->package = array();

		$info = pathinfo($package);
		if ($info['extension']!='zip' && $info['extension']!='tar' && $info['extension']!='gz')
		{
			return false;
		}
		if ($info['extension']=='gz')
		{
			$info['extension'] = 'tar.gz';
		}

		$this->package['folder'] 	= basename($package,'.'.$info['extension']);
		$this->package['f_path']	= str_replace('.'.$info['extension'],'',$package);
		$this->package['z_path'] 	= $package;
		$this->package['zip']		= basename($package);

		return $this->package;
	}


	function callRecordman()
	{
		if(is_a($this->recordman,'P_Team_Recordman'))
		{
			return $this->recordman;
		}

		require_once(dirname(__FILE__).DS.'packageteam'.DS.'record.php');
		$class = 'P_Team_Recordman';
		$this->recordman = new $class();

		return $this->recordman;
	}

	function callUnpacker()
	{
		if(is_a($this->unpacker,'P_Team_Unpacker'))
		{
			return $this->unpacker;
		}

		require_once(dirname(__FILE__).DS.'packageteam'.DS.'unpacker.php');
		$class = 'P_Team_Unpacker';
		$this->unpacker = new $class($this->package);

		return $this->unpacker;
	}

	function callJsupdater()
	{
		if(is_a($this->jsupdater,'P_Team_Jsupdater'))
		{
			return $this->jsupdater;
		}

		require_once(dirname(__FILE__).DS.'packageteam'.DS.'jsupdater.php');
		$class = 'P_Team_Jsupdater';
		$this->jsupdater = new $class();

		return $this->jsupdater;
	}




}

?>