<?php
/**
* @subpackage OSE Update Manager
*
* @author Open Source Excellence, Jays.Soho {@link  http://www.opensource-excellence.com}
* @author Created on 01-Apr-2010
* @copyright Copyright (C) 2010 Open Source Excellence. All rights reserved.
* @license GNU/GPL v2  http://www.gnu.org/copyleft/gpl.html
*/
defined('_JEXEC') or die(';)');
class upman {
	var $package= array();
	var $folder= null;
	var $type= null;
	protected $mainframe;
	protected $link;
	function __construct() {
		$this->checkFolder();
		$this->set('link', 'index.php?option=com_upman&view=upman');
	}
	// Unpack the biggest zip & Analyse the type of Zip
	function setup($package, $parent= 0, $return= true) {
		if(!$this->unpack($package, $parent)) {
			$return['result']= 'FAILED';
			$return['text']= 'Cannot extract the Zip file';
			echo json_encode($return);
			exit;
		}
		$folder= $this->get('folder');
		$pack= $this->get('package');
		$p_dir= $folder;
		$type= $this->detectType($p_dir, 1);
		if($type) {
			if($type == 'component') {
				if(JFile :: exists($p_dir.DS.'manage.php') && JFolder :: exists($p_dir.DS.'upgrade')) {
					$type= 'oseupdater';
				}
			}
			$this->set('type', $type);
		} else {
			if(JFile :: exists($p_dir.DS.'info.php') && JFile :: exists($p_dir.DS.'action.php')) {
				$type= 'mscaddon';
			} else {
				if(!JFolder :: exists($pack['f_path'])) {
					JFolder :: move($folder, $pack['f_path']);
				}
				$p_dir= $pack['f_path'];
				$type= 'package';
			}
			$this->set('type', $type);
		}
		$suit= $this->getSuit();

		if($suit->setup($p_dir, $parent)) {
			if($return) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	function analyse() {
		$this->setup();
	}
	function getSuit($type= '') {
		if(empty($type)) {
			$type= $this->get('type');
		}
		$instance= null;
		$result= $this->fetchFile(dirname(__FILE__).DS.'upman_suitroom'.DS.$type.'.php');
		$class= 'UpmanWear'.ucfirst($type);
		$instance= new $class();
		return $instance;
	}
	function checkFolder() {
		if(!JFolder :: exists(JPATH_COMPONENT.DS.'up_files')) {
			if(!JFolder :: create(JPATH_COMPONENT.DS.'up_files')) {
				$this->errorReturn('The folder cannot be created!');
			}
		}
		return true;
	}
	function fetchFile($path) {
		if(JFile :: exists($path)) {
			require_once($path);
		} else {
			$this->errorReturn('Cannot find this type!');
		}
	}
	function unpack($package, $parent) {
		$pack= array();
		$info= pathinfo($package);
		if($info['extension'] != 'zip' && $info['extension'] != 'tar' && $info['extension'] != 'gz') {
			return false;
		}
		if($info['extension'] == 'gz') {
			$info['extension']= 'tar.gz';
		}
		$pack['folder']= basename($package, '.'.$info['extension']);
		$pack['f_path']= str_replace('.'.$info['extension'], '', $package);
		$pack['z_path']= $package;
		$pack['zip']= basename($package);
		$this->set('package', $pack);
		$archivename= JPath :: clean($pack['z_path']);
		if($parent == 0) {
			$extractdir= JPath :: clean(JPATH_COMPONENT.DS.'up_files'.DS.uniqid('tmpl_'));
		} else {
			$extractdir= JPath :: clean(JPATH_COMPONENT.DS.'up_files'.DS.$this->getPath(0, $parent).DS.uniqid('tmpl_'));
		}
		$result= JArchive :: extract($archivename, $extractdir);
		if($result == false) {
			return false;
		} else {
			JFile :: delete($archivename);
			$this->set('folder', $extractdir);
			return true;
		}
	}
	function detectType($p_dir, $recurse= false) {
		if(OSEJVERSION == 'J16') {
			$type= self :: detectTypeJ16($p_dir);
		} else {
			$type= self :: detectTypeJ15($p_dir, $recurse);
		}
		return $type;
	}
	function detectTypeJ15($p_dir, $recurse= false) {
		// Search the install dir for an xml file
		$files= JFolder :: files($p_dir, '\.xml$', $recurse, true);
		if(count($files) > 0) {
			foreach($files as $file) {
				$xmlDoc= & JFactory :: getXMLParser();
				$xmlDoc->resolveErrors(true);
				if(!$xmlDoc->loadXML($file, false, true)) {
					// Free up memory from DOMIT parser
					unset($xmlDoc);
					continue;
				}
				$root= & $xmlDoc->documentElement;
				if(!is_object($root) ||($root->getTagName() != "install" && $root->getTagName() != 'mosinstall')) {
					unset($xmlDoc);
					continue;
				}
				$type= $root->getAttribute('type');
				// Free up memory from DOMIT parser
				unset($xmlDoc);
				return $type;
			}
			// Free up memory from DOMIT parser
			unset($xmlDoc);
			return false;
		} else {
			return false;
		}
	}
	function detectTypeJ16($p_dir) {
		// Search the install dir for an xml file
		$files= JFolder :: files($p_dir, '\.xml$', 1, true);
		if(!count($files)) {
			JError :: raiseWarning(1, JText :: _('ERRORNOTFINDXMLSETUPFILE'));
			return false;
		}
		foreach($files as $file) {
			if(!$xml= JFactory :: getXML($file)) {
				continue;
			}
			if($xml->getName() != 'install' && $xml->getName() != 'extension') {
				unset($xml);
				continue;
			}
			$type= (string) $xml->attributes()->type;
			// Free up memory
			unset($xml);
			return $type;
		}
		JError :: raiseWarning(1, JText :: _('ERRORNOTFINDJOOMLAXMLSETUPFILE'));
		// Free up memory.
		unset($xml);
		return false;
	}
	function get($name) {
		return $this-> {
			$name };
	}
	protected function set($name, $value) {
		$this-> {
			$name }
		= $value;
	}
	function errorReturn($msg, $type= null) {
		global $mainframe;
		$mainframe->redirect($this->link, $msg, $type);
	}
	function getPath($top_id, $end_id, $path= '') {
		$db= & JFactory :: getDBO();
		//static $section;
		$query= " SELECT * FROM `#__upman` WHERE id = '{$end_id}' ";
		$db->setQuery($query);
		$obj= $db->loadObject();
		if($path) {
			$path= $obj->folder.DS.$path;
		} else {
			$path= $obj->folder;
		}
		if($obj->parent != $top_id) {
			$path= $this->getPath($top_id, $obj->parent, $path);
		}
		return $path;
	}
}
?>