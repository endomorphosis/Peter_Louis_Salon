<?php
/**
* @subpackage OSE Update Manager
*
* @author Open Source Excellence, Jays.Soho {@link  http://www.opensource-excellence.com}
* @author Created on 01-Apr-2010
* @copyright Copyright (C) 2010 Open Source Excellence. All rights reserved.
* @license GNU/GPL v2  http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined( '_JEXEC' ) or die( ';)' );

/**
 * Main installer
 */
function com_install()
{
	$errors = FALSE;

	//-- common images
	$img_OK = '<img src="images/publish_g.png" />';
	$img_WARN = '<img src="images/publish_y.png" />';
	$img_ERROR = '<img src="images/publish_r.png" />';
	$BR = '<br />';

	//--install...
	$db = & JFactory::getDBO();

	$query = "CREATE TABLE IF NOT EXISTS `#__upman` (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `ext_id` int(5) NOT NULL,
			  `name` varchar(50) NOT NULL,
			  `folder` varchar(50) NOT NULL,
			  `type` varchar(30) NOT NULL,
			  `parent` int(5) NOT NULL,
			  `status` varchar(15) NOT NULL DEFAULT '0',
`description` text NOT NULL,
  `date_add` date NOT NULL,
			  PRIMARY KEY (`id`),
			  UNIQUE KEY `folder` (`folder`,`parent`)
			) ENGINE=MyISAM AUTO_INCREMENT=1 ;";
	$db->setQuery($query);
	if( ! $db->query() )
	{
		echo $img_ERROR.JText::_('Unable to create table').$BR;
		echo $db->getErrorMsg();
		return FALSE;
	}

	if( $errors )
	{
		return FALSE;
	}

	return TRUE;
}// function
