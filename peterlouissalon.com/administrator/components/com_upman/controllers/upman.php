<?php
/**
* @subpackage OSE Update Manager
*
* @author Open Source Excellence, Jays.Soho {@link  http://www.opensource-excellence.com}
* @author Created on 01-Apr-2010
* @copyright Copyright (C) 2010 Open Source Excellence. All rights reserved.
* @license GNU/GPL v2  http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined( '_JEXEC' ) or die( ';)' );

jimport('joomla.application.component.controller');

/**
 * upman Controller
 *
 * @package    upman
 * @subpackage Controllers
 */
class upmanControllerupman extends upmanController
{
	/**
	 * constructor (registers additional tasks to methods)
	 * @return void
	 */
	function __construct()
	{
		parent::__construct();
	}// function
	function upload()
	{
		JRequest::checkToken() or jexit( 'Invalid Token' );
		$model = $this->getModel('upman');
		$result = $model->setup();
		$msg = null;
		if($result)
		{
			$msg = JText::_('Upload Finishes!');
		}
		$this->setRedirect( 'index.php?option=com_upman', $msg );
	}

	// Remove the Folder and delete the data in db
	function remove()
	{
		JRequest::checkToken() or jexit( 'Invalid Token' );

		$cids = JRequest::getVar('cid',array(0),'post','array');

		$model = $this->getModel('upman');

		$msg = null;

		foreach($cids as $cid)
		{
			$result = $model->remove($cid);

			if(!result)
			{
				JError::raiseWarning('SOME_ERROR_CODE', JText::_('Can\'t Remove'));
			}
		}

		$msg = 'Remove Finish';

		$this->setRedirect( 'index.php?option=com_upman',$msg );

	}


	// Remove folder and uninstall
	function fullremove()
	{
		JRequest::checkToken() or jexit( 'Invalid Token' );

		$cids = JRequest::getVar('cid',array(0),'post','array');

		$msg = null;

		$model  = $this->getModel('upman');
		$model1 = $this->getModel('install');
		foreach($cids as $cid)
		{
			JRequest::setVar('parent',$cid);
			$items = $model1->_getItems();

			if(count($items) < 1)
			{
				// Error
				JError::raiseWarning('SOME_ERROR_CODE', JText::_('No Extension Remove'));
			}

			$objs  = array();
			foreach ($items as $item)
			{
				$objs[] = $item->id;
			}

			JRequest::setVar('cid',$objs);

			if(!$model1->uninstall())
			{
				continue;
			}

			$result = $model->remove($cid);

			if(result)
			{
				$msg = 'Remove Completed';
			}
		}

		$this->setRedirect( 'index.php?option=com_upman', $msg );
	}

	function fullinstall()
	{
		JRequest::checkToken() or jexit( 'Invalid Token' );

		$cids = JRequest::getVar('cid',array(0),'post','array');

		$msg = null;

		$model = $this->getModel('install');

		foreach($cids as $cid)
		{

			JRequest::setVar('parent',$cid);
			$items = $model->_getItems();

			if(count($items) < 1)
			{
				// Error
				JError::raiseWarning('SOME_ERROR_CODE', JText::_('No Extension Remove'));
				continue;
			}

			$objs  = array();
			foreach ($items as $item)
			{
				$objs[] = $item->id;
			}

			JRequest::setVar('cid',$objs);

			if($model->install())
			{
				$msg = 'Installation Completed!';
			}
		}

		$this->setRedirect( 'index.php?option=com_upman', $msg );
	}


	/**
	 * cancel editing a record
	 * @return void
	 */
	function cancel()
	{
		$msg = JText::_( 'Operation Cancelled' );
		$this->setRedirect( 'index.php?option=com_upman', $msg );
	}// function

	function extractFile()
	{
		$file = JRequest::getString('file');
		$model = $this->getModel('upman');
		$model-> extractFile($file);
	}
}// class