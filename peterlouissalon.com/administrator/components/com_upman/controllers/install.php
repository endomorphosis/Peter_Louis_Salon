<?php
/**
 * @version $Id: header.php 248 2008-05-23 10:40:56Z elkuku $
 * @package		upman
 * @subpackage
 * @author		Jays.Soho {@link http://jays.survey-partner.com}
 * @author		Created on 11-Mar-2009
 */
// no direct access
defined('_JEXEC') or die(';)');
jimport('joomla.application.component.controller');
/**
 * upman Controller
 *
 * @package    upman
 * @subpackage Controllers
 */
class upmanControllerInstall extends upmanController {
	var $parent= null;
	function __construct() {
		parent :: __construct();
		$this->parent= JRequest :: getVar('parent', null);
	}
	function install() {
		// check token
		JRequest :: checkToken() or jexit('Invalid Token');
		$model= $this->getModel('install');
		$result= $model->install();

		$msg= null;
		if($result) {
			// Stay at this page
			$msg= 'Installation have been completed!';
		}
		$this->setRedirect('index.php?option=com_upman&view=install&parent='.$this->parent.'&'.JUtility :: getToken().'=1', $msg);
	}
	function cancel() {
		$msg= 'Action Cancelled';
		$this->setRedirect('index.php?option=com_upman&view=upman',$msg);
	}
	function remove() {
		$msg= 'Item Removed';
		$this->setRedirect('index.php?option=com_upman&view=upman',$msg);
	}
	function uninstall() {
		JRequest :: checkToken() or jexit('Invalid Token');
		$model= $this->getModel('install');
		$result= $model->uninstall();
		$msg= null;
		if($result) {
			// Stay at this page
			$msg= 'Uninstallation have been completed!';
		}
		$this->setRedirect('index.php?option=com_upman&view=install&parent='.$this->parent.'&'.JUtility :: getToken().'=1', $msg);
	}
}
?>