<?php
/**
* @subpackage OSE Update Manager
* 
* @author Open Source Excellence, Jays.Soho {@link  http://www.opensource-excellence.com}
* @author Created on 01-Apr-2010
* @copyright Copyright (C) 2010 Open Source Excellence. All rights reserved. 
* @license GNU/GPL v2  http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined( '_JEXEC' ) or die( ';)' );

jimport('joomla.application.component.controller');

/**
 * upman Controller
 *
 * @package    upman
 * @subpackage Controllers
 */
class upmanControllerInstaller extends upmanController
{
	var $parent = null;
	
	function __construct()
	{
		parent::__construct();
		$this->parent = JRequest::getInt('parent',null);
	}

	function install()
	{
		// check token
		JRequest::checkToken() or jexit( 'Invalid Token' );
		
		$cids = JRequest::getVar('cid',array(),'post','array');
		$msg  = null;
		
		if(count($cids) > 0)
		{
			$model = $this->getModel('installer');
	
			$result = $model->install();
	
			if($result)
			{
				// Stay at this page
				$msg = JText::_('Installation have been completed!');
			}
		}
		else
		{
			$msg = JText::_('Please Select One Item to Install');
		}
		
		$this->setRedirect( 'index.php?option=com_upman&view=installer&parent='.$this->parent.'&'. JUtility::getToken() .'=1', $msg );
	}

	function cancel($msg = null)
	{
		$this->setRedirect( 'index.php?option=com_upman&view=upman', $msg );
	}

	function remove($msg = null)
	{
		$this->setRedirect( 'index.php?option=com_upman&view=upman', $msg );
	}

	function uninstall()
	{
		JRequest::checkToken() or jexit( 'Invalid Token' );
		
		$cids = JRequest::getVar('cid',array(),'post','array');
		$msg  = null;
		
		if(count($cids) > 0)
		{
			$model = $this->getModel('installer');
	
			$result = $model->uninstall();
	
			if($result)
			{
				// Stay at this page
				$msg = JText::_('Uninstallation have been completed!');
			}
		}
		else
		{
			$msg = JText::_('Please Select One Item to Uninstall');
			
		}
		$this->setRedirect( 'index.php?option=com_upman&view=installer&parent='.$this->parent.'&'. JUtility::getToken() .'=1', $msg );
	}
}
?>