<?php
/**
 * @version $Id: header.php 248 2008-05-23 10:40:56Z elkuku $
 * @package		upman
 * @subpackage
 * @author		Jays.Soho {@link http://jays.survey-partner.com}
 * @author		Created on 11-Mar-2009
 */

// no direct access
defined( '_JEXEC' ) or die( ';)' );

require_once(JPATH_COMPONENT.DS.'robotroom'.DS.'upman.php');
/**
 * upman Controller
 *
 * @package    upman
 * @subpackage Controllers
 */
class upmanModelInstall extends JModel
{
	var $info = null;
	var $up = null;

	function __construct()
	{
		parent::__construct();

		$this->up = JPATH_COMPONENT.DS.'up_files';
	}

	function getItems()
	{
		JRequest::checkToken('get') or jexit( 'Invalid Token' );

		$rows = $this->_getItems();

		return $rows;
	}

	function getInfo()
	{
		if ($this->info == null)
		{
			$this->getInfo();
		}
		return $this->info;
	}

	function install()
	{
		$cids = JRequest::getVar('cid',array(0),'post','array');

		$parent = JRequest::getVar('parent',null);

		// Get an upman instance
		// Hi, I am upman!!
		$upman = Upman::getInstance();

		// Get the parent folder info
		$query = " SELECT folder FROM `#__upman` WHERE id = '{$parent}' AND parent = '0'";
		$this->_db->setQuery($query);
		$p_folder = $this->_db->loadResult();

		foreach($cids as $cid)
		{
			// Get the subfolder install message
			$query = " SELECT * FROM `#__upman` WHERE id = '{$cid}' ";
			$this->_db->setQuery($query);
			$row = $this->_db->loadObject();

			if($row->id == $parent)
			{
				$path = $this->up.DS.$p_folder;
			}
			else
			{
				$path = $this->up.DS.$p_folder.DS.$row->folder;
			}

			// Let me dispatch the type to the right  installaton
			// get the type...

			//OK! Prepared everything, get the path and install...
			if(!$upman->install($path,$row))
			{
				return false;
			}
		}

		return true;
	}

	function uninstall()
	{
		$cids = JRequest::getVar('cid',array(0),'post','array');

		$parent = JRequest::getVar('parent',null);

		// Get an upman instance
		// Hi, I am upman!!
		$upman = Upman::getInstance();

		// Get the parent folder info
		$query = " SELECT folder FROM `#__upman` WHERE id = '{$parent}' AND parent = '0' ";
		$this->_db->setQuery($query);
		$p_folder = $this->_db->loadResult();

		foreach($cids as $cid)
		{
			// Get the subfolder install message
			$query = " SELECT * FROM `#__upman` WHERE id = '{$cid}' ";
			$this->_db->setQuery($query);
			$row = $this->_db->loadObject();

			if($row->status == 0)
			{
				continue;
			}

			if($row->id == $parent)
			{
				$path = $this->up.DS.$p_folder;
			}
			else
			{
				$path = $this->up.DS.$p_folder.DS.$row->folder;
			}

			// Let me dispatch the type to the right  installaton
			// get the type...

			//OK! Prepared everything, get the path and install...
			if(!$upman->uninstall( $path,$row ))
			{
				return false;
			}

		}

		return true;

	}

	function _getItems()
	{

		global $mainframe;

		$rows = array();

		$upman = Upman::getInstance();

		$parent = JRequest::getInt('parent',null);

		if(empty($parent))
		{
			$link = 'index.php?option=com_upman&view=upman';
			$msg = 'Please select one item';
			$mainframe->redirect($link, $msg);
		}

		$query = " SELECT * FROM `#__upman` "
		." WHERE id = '{$parent}' ";
		$this->_db->setQuery($query);
		$this->info = $this->_db->loadObject();

		$items = array();

		switch($this->info->type)
		{
			case('package'):
				$query = "SELECT * FROM `#__upman` WHERE parent = '{$parent}' ";
				$this->_db->setQuery($query);
				$rows = $this->_db->loadObjectList();

				foreach($rows as $row)
				{
					$type = str_replace('&patch','',$row->type);
					$installer = $upman->getSuit($type);
					$items[] = $installer->resetType($this->info->folder,$row);
				}
				$rows = $items;
				break;

			case('jsupdater'):
				$rows[] = $this->info;
				break;

			case('component&patch'):
			case('module&patch'):
			case('plugin&patch'):
				$type = str_replace('&patch','',$this->info->type);
				$installer = $upman->getSuit($type);
				$this->info = $installer->resetType('',$this->info);
				$rows[] = $this->info;
				break;

			default:
				$installer = $upman->getSuit($this->info->type);
				$this->info = $installer->resetType('',$this->info);
				$rows[] = $this->info;
				break;
		}

		return $rows;
	}
}

?>