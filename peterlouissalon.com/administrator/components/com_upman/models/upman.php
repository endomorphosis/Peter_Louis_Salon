<?php
/**
* @subpackage OSE Update Manager V2
*
* @author Open Source Excellence, Jays.Soho {@link  http://www.opensource-excellence.com}
* @author Created on 01-Apr-2010
* @copyright Copyright (C) 2010 Open Source Excellence. All rights reserved.
* @license GNU/GPL v2  http://www.gnu.org/copyleft/gpl.html
 */
// no direct access
defined('_JEXEC') or die(';)');
require_once(JPATH_COMPONENT.DS.'robotroom'.DS.'upman.php');
/**
 * upman Controller
 *
 * @package    upman
 * @subpackage Controllers
 */
class upmanModelUpman extends JModel {
	var $pagination= null;
	var $lists= null;
	function __construct() {
		parent :: __construct();
	} //function
	function setup() {
		$upman= new upman();
		switch(JRequest :: getWord('installtype')) {
			case 'upload' :
				$package= $this->_getPackageFromUpload();
				break;
			case 'url' :
				$package= $this->_getPackageFromUrl();
				break;
			default :
				JError :: raiseWarning('SOME_ERROR_CODE', JText :: _('No Install Type Found'));
				return false;
				break;
		}
		if($package == false) {
			JError :: raiseWarning('SOME_ERROR_CODE', JText :: _('Upload Failed!'));
			return false;
		}
		if(!$upman->setup($package))
		{
			JError::raiseWarning('SOME_ERROR_CODE', JText::_('Upload Failed!'));
			return false;
		}
		else
		{
			return true;
		}
	}
	function extractFile($file)
	{
		$upman = new upman();
		$path = JPATH_COMPONENT.DS.'up_files'.DS;
		$result = $upman ->setup($path.$file,$parent = 0,$return = true);
		$return = array();
		if ($result==true)
		{
			$return['result']='SUCCESS';
			$return['text']='The file has been extracted successfully';
		}
		else
		{
			$return['result']='FAILED';
			$return['text']='The file was extracted unsuccessfully';
		}
		echo json_encode($return); exit;
	}
	function _getPackageFromUpload() { //Should be Capitalized
		// Checking...
		$userfile= JRequest :: getVar('install_package', null, 'files', 'array');
		// Make sure that file uploads are enabled in php
		if(!(bool) ini_get('file_uploads')) {
			JError :: raiseWarning('SOME_ERROR_CODE', JText :: _('WARNINSTALLFILE'));
			return false;
		}
		// Make sure that zlib is loaded so that the package can be unpacked
		if(!extension_loaded('zlib')) {
			JError :: raiseWarning('SOME_ERROR_CODE', JText :: _('WARNINSTALLZLIB'));
			return false;
		}
		// If there is no uploaded file, we have a problem...
		if(!is_array($userfile)) {
			JError :: raiseWarning('SOME_ERROR_CODE', JText :: _('No file selected'));
			return false;
		}
		// Check if there was a problem uploading the file.
		if($userfile['error'] || $userfile['size'] < 1) {
			JError :: raiseWarning('SOME_ERROR_CODE', JText :: _('WARNINSTALLUPLOADERROR'));
			return false;
		}
		// Build the appropriate paths
		$tmp_dest= JPATH_COMPONENT.DS.'up_files'.DS.$userfile['name'];
		$tmp_src= $userfile['tmp_name'];
		$basedir = dirname($tmp_dest);
		// Begin to Deliver the Package...

		$uploaded= JFile :: upload($tmp_src, $tmp_dest);
		if(!$uploaded) {
			JError :: raiseWarning('SOME_ERROR_CODE', JText :: _('Fail to Upload file'));
			return false;
		} else {
			return $tmp_dest;
		}
	}
	function _getPackageFromUrl() {
		// Get the URL of the package to install
		$url= JRequest :: getString('install_url');
		// Did you give us a URL?
		if(!$url) {
			JError :: raiseWarning('SOME_ERROR_CODE', JText :: _('Please enter a URL'));
			return false;
		}
		// Download the package at the URL given
		$p_file= $this->downloadPackage($url);
		// Was the package downloaded?
		if(!$p_file) {
			JError :: raiseWarning('SOME_ERROR_CODE', JText :: _('Invalid URL'));
			return false;
		}
		$tmp_dest= JPATH_COMPONENT.DS.'up_files'.DS.$p_file;
		return $tmp_dest;
	}
	function getUnzipFiles()
	{
		jimport( 'joomla.filesystem.folder' );
		$files = JFolder::files (JPATH_ADMINISTRATOR.DS.'components'.DS.'com_upman'.DS.'up_files', '.zip|.tar.gz');
		return $files;
	}
	function downloadPackage($url, $target= false) {
		$config= & JFactory :: getConfig();
		// Capture PHP errors
		$php_errormsg= 'Error Unknown';
		ini_set('track_errors', true);
		// Set user agent
		ini_set('user_agent', "Joomla! 1.5 Installer");
		// Open the remote server socket for reading
		$inputHandle= @ fopen($url, "r");
		$error= strstr($php_errormsg, 'failed to open stream:');
		if(!$inputHandle) {
			JError :: raiseWarning(42, JText :: _('SERVER_CONNECT_FAILED').', '.$error);
			return false;
		}
		$meta_data= stream_get_meta_data($inputHandle);
		foreach($meta_data['wrapper_data'] as $wrapper_data) {
			if(substr($wrapper_data, 0, strlen("Content-Disposition")) == "Content-Disposition") {
				$contentfilename= explode("\"", $wrapper_data);
				$target= $contentfilename[1];
			}
		}
		// Set the target path if not given
		if(!$target) {
			$target= JPATH_COMPONENT.DS.'up_files'.DS.$this->getFilenameFromURL($url);
		} else {
			$target= JPATH_COMPONENT.DS.'up_files'.DS.$this->getFilenameFromURL($url);
		}
		// Initialize contents buffer
		$contents= null;
		while(!feof($inputHandle)) {
			$contents .= fread($inputHandle, 4096);
			if($contents == false) {
				JError :: raiseWarning(44, 'Failed reading network resource: '.$php_errormsg);
				return false;
			}
		}
		// Write buffer to file
		JFile :: write($target, $contents);
		// Close file pointer resource
		fclose($inputHandle);
		// Return the name of the downloaded package
		return basename($target);
	}
	function getFilenameFromURL($url) {
		if(is_string($url)) {
			$url= str_replace(array('/', '\\'), DS, $url);
			$parts= explode(DS, $url);
			return $parts[count($parts) - 1];
		}
		return false;
	}
	function getItems() {
		$mainframe = &JFactory::getApplication();
		static $items;
		if(isset($items)) {
			return $items;
		}
		$option= 'com_upman';
		$filter_order= $mainframe->getUserStateFromRequest("$option.filter_order", 'filter_order', 'u.folder', 'cmd');
		$filter_order_Dir= $mainframe->getUserStateFromRequest("$option.filter_order_Dir", 'filter_order_Dir', '', 'word');
		$filter_type= $mainframe->getUserStateFromRequest("$option.filter_type", 'filter_type', 0, 'string');
		$search= $mainframe->getUserStateFromRequest("$option.search", 'search', '', 'string');
		$search= JString :: strtolower($search);
		$limit= $mainframe->getUserStateFromRequest('global.list.limit', 'limit', $mainframe->getCfg('list_limit'), 'int');
		$limitstart= $mainframe->getUserStateFromRequest($option.'.limitstart', 'limitstart', 0, 'int');
		$where= array();
		$orderby= ' ORDER BY '.$filter_order.' '.$filter_order_Dir;
		if(isset($search) && $search != '') {
			$searchEscaped= $this->_db->Quote('%'.$this->_db->getEscaped($search, true).'%', false);
			$where[]= ' u.folder LIKE '.$searchEscaped.' ';
		}
		if($filter_type) {
			if($filter_type > 0) {
				$where[]= " u.type = '{$filter_type}' ";
			}
		}
		$where[]= " (u.parent = '0') ";
		$where=(count($where) ? ' WHERE ('.implode(') AND (', $where).')' : '');
		$query= 'SELECT COUNT(u.id)'.		' FROM `#__upman` AS u '.		$where;
		$this->_db->setQuery($query);
		$total= $this->_db->loadResult();
		jimport('joomla.html.pagination');
		$this->pagination= new JPagination($total, $limitstart, $limit);
		$query= "SELECT * FROM `#__upman` AS u ".$where.$orderby;
		$this->_db->setQuery($query, $this->pagination->limitstart, $this->pagination->limit);
		$rows= $this->_db->loadObjectList();
		// view lists
		$lists= array();
		$types[]= JHTML :: _('select.option', '0', '- '.JText :: _('Select Type').' -');
		$types[]= JHTML :: _('select.option', 'js.updater', JText :: _('OSE Update Pack'));
		$types[]= JHTML :: _('select.option', 'package', JText :: _('Package'));
		$types[]= JHTML :: _('select.option', '-1', JText :: _('----------'));
		$types[]= JHTML :: _('select.option', 'component', JText :: _('Component'));
		$types[]= JHTML :: _('select.option', 'module', JText :: _('Module'));
		$types[]= JHTML :: _('select.option', 'plugin', JText :: _('Plugin'));
		$lists['type']= JHTML :: _('select.genericlist', $types, 'filter_type', 'class="inputbox" size="1" onchange="document.adminForm.submit( );"', 'value', 'text', "$filter_type");
		// table ordering
		$lists['order_Dir']= $filter_order_Dir;
		$lists['order']= $filter_order;
		// search filter
		$lists['search']= $search;
		$this->lists= $lists;
		return $rows;
	}
	function & getPagination() {
		if($this->pagination == null) {
			$this->getItems();
		}
		return $this->pagination;
	}
	function & getViewLists() {
		if($this->lists == null) {
			$this->getItems();
		}
		return $this->lists;
	}
	function remove($id) {
		$query= " SELECT * FROM `#__upman` WHERE id = '{$id}'";
		$this->_db->setQuery($query);
		$obj= $this->_db->loadObject();
		if(!JFolder :: delete(JPATH_COMPONENT.DS.'up_files'.DS.$obj->folder)) {
			return false;
		}
		$query= " DELETE FROM `#__upman` WHERE id = '{$id}' OR parent = '{$id}'";
		$this->_db->setQuery($query);
		if(!$this->_db->query()) {
			return false;
		}
		return true;
	}
} // class