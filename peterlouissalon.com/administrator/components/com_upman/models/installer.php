<?php
/**
* @subpackage OSE Update Manager
*
* @author Open Source Excellence, Jays.Soho {@link  http://www.opensource-excellence.com}
* @author Created on 01-Apr-2010
* @copyright Copyright (C) 2010 Open Source Excellence. All rights reserved.
* @license GNU/GPL v2  http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined( '_JEXEC' ) or die( ';)' );

require_once(JPATH_COMPONENT.DS.'robotroom'.DS.'upman.php');
/**
 * upman Controller
 *
 * @package    upman
 * @subpackage Controllers
 */
class upmanModelInstaller extends JModel
{
	var $info = null;
	var $up = null;

	function __construct()
	{
		parent::__construct();

		$this->up = JPATH_COMPONENT.DS.'up_files';
	}

	function getItems()
	{
		JRequest::checkToken('get') or jexit( 'Invalid Token' );

		$rows = $this->_getItems();

		return $rows;
	}

	function getInfo()
	{
		if ($this->info == null)
		{
			$this->_getItems();
		}
		return $this->info;
	}

	function install()
	{
		$cids = JRequest::getVar('cid',array(),'post','array');

		$parent = JRequest::getVar('parent',null);


		// Get the parent folder info
		/*
		$query = " SELECT * FROM `#__upman` WHERE id = '{$parent}' AND parent = 0";
		$this->_db->setQuery($query);
		$info = $this->_db->loadResult();
		*/

		if(count($cids) < 1 )
		{
			return false;
		}
		foreach($cids as $cid)
		{
			// Get the subfolder install message
			$query = " SELECT * FROM `#__upman` WHERE id = '{$cid}' ";
			$this->_db->setQuery($query);
			$obj = $this->_db->loadObject();

			$upman = new Upman();
			$suit  = $upman->getSuit($obj->type);

			if($obj->id == $parent)
			{
				$path = $this->up.DS.$obj->folder;
			}
			else
			{
				$path = $this->up.DS.$upman->getPath(0,$obj->id);
				//echo $path.$obj->type;exit;
			}
			//OK! Prepared everything, get the path and install...
			if(!$suit->install($path,$obj))
			{
				//JError::raiseWarning('SOME_ERROR_CODE', JText::_('No Install Type Found'));
				return false;
			}
		}

		return true;
	}

	function uninstall()
	{
		$cids = JRequest::getVar('cid',array(),'post','array');

		$parent = JRequest::getInt('parent',null);


		if(count($cids) < 1 )
		{
			return false;
		}

		foreach($cids as $cid)
		{
			// Get the subfolder install message
			$query = " SELECT * FROM `#__upman` WHERE id = '{$cid}' ";
			$this->_db->setQuery($query);
			$obj = $this->_db->loadObject();

			$upman = new Upman();
			$suit  = $upman->getSuit($obj->type);

			if($obj->status == 0)
			{
				continue;
			}


			if(!$suit->uninstall($obj))
			{
				//JError::raiseWarning('SOME_ERROR_CODE', JText::_('No Install Type Found'));
				return false;
			}
		}

		return true;
	}

	function _getItems()
	{
		$parent = JRequest::getInt('parent',null);
		$upman = new upman();
		if(empty($parent))
		{
			$upman->errorReturn(JText::_('Please select one item'));
		}

		$items = array();
		$rows  = array();


		// Get the type of this require
		$query = " SELECT * FROM `#__upman` "
				." WHERE id = '{$parent}' ";
		$this->_db->setQuery($query);
		$info = $this->_db->loadObject();

		// Get Instance
		$suit = $upman->getSuit($info->type);

		$property = $suit->getItems($info);

		if(is_array($property))
		{
			$items = $property;
		}
		else
		{
			$items[] = $property;
		}

		return $items;
			/*
		switch($this->info->type)
		{
			case('package'):
				$query = "SELECT * FROM `#__upman` WHERE parent = '{$parent}' ";
				$this->_db->setQuery($query);
				$rows = $this->_db->loadObjectList();

				foreach($rows as $row)
				{
					$type = str_replace('&patch','',$row->type);
					$installer = $upman->getSuit($type);
					$items[] = $installer->resetType($this->info->folder,$row);
				}
				$rows = $items;
				break;

			case('jsupdater'):
				$rows[] = $this->info;
				break;

			case('component&patch'):
			case('module&patch'):
			case('plugin&patch'):
				$type = str_replace('&patch','',$this->info->type);
				$installer = $upman->getSuit($type);
				$this->info = $installer->resetType('',$this->info);
				$rows[] = $this->info;
				break;

			default:
				$installer = $upman->getSuit($this->info->type);
				$this->info = $installer->resetType('',$this->info);
				$rows[] = $this->info;
				break;
		}

		return $rows;
		*/
	}
}

?>