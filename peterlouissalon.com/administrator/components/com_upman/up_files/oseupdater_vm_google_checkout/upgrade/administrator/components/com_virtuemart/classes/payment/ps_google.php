<?php
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' );
class ps_google {
    var $classname = "ps_google";
    var $payment_code = "GOOGLE";
    function show_configuration() {
        global $VM_LANG;
        $db = new ps_DB();
        /** Read current Configuration ***/
        include_once(CLASSPATH ."payment/".$this->classname.".cfg.php");
    ?>
    <table class="adminform">

 <tr class="row0">
            <td><strong><?php echo 'Sandbox mode' ?></strong></td>
            <td>
                <select name="GOOGLE_SANDBOX" class="inputbox" >
                <option value="1" <?php
                if (GOOGLE_SANDBOX == true)
                {
                	echo 'selected="selected"';
                }
                ?>>Yes</option>

                <option value="0"
                <?php
                if (GOOGLE_SANDBOX == false)
                {
                	echo 'selected="selected"';
                }
                ?>
                >No</option>
                </select>
            </td>
            <td><?php echo 'If Sandbox Mode is chosen, please enter a Sandbox Merchant ID and Merchant Key in the following box. Please check this with <a href="https://sandbox.google.com/checkout/" target="_blank">Google Sandbox</a>. '; ?>
            </td>
        </tr>


        <tr class="row0">
        <td><strong><?php echo 'Google Merchant ID:'; ?></strong></td>
            <td>
                <input type="text" name="GOOGLE_MERCHANT_ID" class="inputbox" value="<?php  echo GOOGLE_MERCHANT_ID ?>" />
            </td>
            <td><?php echo 'Your Google Merchant ID'; ?>
            </td>
        </tr>
        <tr class="row1">
        <td><strong><?php echo 'Google Merchant KEY:'; ?></strong></td>
            <td>
                <input type="text" name="GOOGLE_MERCHANT_KEY" class="inputbox" value="<?php  echo GOOGLE_MERCHANT_KEY ?>" />
            </td>
            <td><?php echo 'Your Google Merchant KEY (Tips: 1. Sign in to Google Checkout; 2. Click the Settings tab; 3. Click Integration; 4. Your Merchant Key and Merchant ID will appear under "Account information.")'; ?>
            </td>
        </tr>

        <tr class="row0">
            <td><strong><?php echo $VM_LANG->_('PHPSHOP_ADMIN_CFG_PAYPAL_STATUS_SUCCESS') ?></strong></td>
            <td>
                <select name="GOOGLE_VERIFIED_STATUS" class="inputbox" >
                <?php
                    $q = "SELECT order_status_name,order_status_code FROM #__{vm}_order_status ORDER BY list_order";
                    $db->query($q);
                    $order_status_code = Array();
                    $order_status_name = Array();

                    while ($db->next_record()) {
                    $order_status_code[] = $db->f("order_status_code");
                    $order_status_name[] =  $db->f("order_status_name");
                    }
                    for ($i = 0; $i < sizeof($order_status_code); $i++) {
                      echo "<option value=\"" . $order_status_code[$i];
                      if (GOOGLE_VERIFIED_STATUS == $order_status_code[$i])
                         echo "\" selected=\"selected\">";
                      else
                         echo "\">";
                      echo $order_status_name[$i] . "</option>\n";
                    }?>
                    </select>
            </td>
            <td><?php echo 'Select the order status to which the actual order is set, if the Google Notification was successful. If using download selling options: select the status which enables the download (then the customer is instantly notified about the download via e-mail). '; ?>
            </td>
        </tr>
        <tr class="row1">
            <td><strong><?php echo $VM_LANG->_('VM_ADMIN_CFG_PAYPAL_STATUS_PENDING') ?></strong></td>
            <td>
                <select name="GOOGLE_PENDING_STATUS" class="inputbox" >
                <?php
                    for ($i = 0; $i < sizeof($order_status_code); $i++) {
                      echo "<option value=\"" . $order_status_code[$i];
                      if (GOOGLE_PENDING_STATUS == $order_status_code[$i])
                         echo "\" selected=\"selected\">";
                      else
	                     echo "\">";
                      echo $order_status_name[$i] . "</option>\n";
                    } ?>
                    </select>
            </td>
            <td><?php echo $VM_LANG->_('VM_ADMIN_CFG_PAYPAL_STATUS_PENDING_EXPLAIN') ?></td>
        </tr>
        <tr class="row1">
            <td><strong><?php echo $VM_LANG->_('PHPSHOP_ADMIN_CFG_PAYPAL_STATUS_FAILED') ?></strong></td>
            <td>
                <select name="GOOGLE_INVALID_STATUS" class="inputbox" >
                <?php
                    for ($i = 0; $i < sizeof($order_status_code); $i++) {
                      echo "<option value=\"" . $order_status_code[$i];
                      if (GOOGLE_INVALID_STATUS == $order_status_code[$i])
                         echo "\" selected=\"selected\">";
                      else
                         echo "\">";
                      echo $order_status_name[$i] . "</option>\n";
                    } ?>
                    </select>
            </td>
            <td><?php echo $VM_LANG->_('PHPSHOP_ADMIN_CFG_PAYPAL_STATUS_FAILED_EXPLAIN') ?>
            </td>
        </tr>
      </table>
    <?php
    }

    function has_configuration() {
      // return false if there's no configuration
      return true;
   }


  /**

	* Returns the "is_writeable" status of the configuration file

	* @param void

	* @returns boolean True when the configuration file is writeable, false when not

	*/

   function configfile_writeable() {

      return is_writeable( CLASSPATH."payment/".$this->classname.".cfg.php" );

   }



  /**

	* Returns the "is_readable" status of the configuration file

	* @param void

	* @returns boolean True when the configuration file is writeable, false when not

	*/

   function configfile_readable() {

      return is_readable( CLASSPATH."payment/".$this->classname.".cfg.php" );

   }



  /**

	* Writes the configuration file for this payment method

	* @param array An array of objects

	* @returns boolean True when writing was successful

	*/

   function write_configuration( &$d ) {
      $my_config_array = array(
                              "GOOGLE_SANDBOX" => $d['GOOGLE_SANDBOX'],
                              "GOOGLE_MERCHANT_ID" => $d['GOOGLE_MERCHANT_ID'],
                              "GOOGLE_MERCHANT_KEY" => $d['GOOGLE_MERCHANT_KEY'],
                              "GOOGLE_VERIFIED_STATUS" => $d['GOOGLE_VERIFIED_STATUS'],
                              "GOOGLE_PENDING_STATUS" => $d['GOOGLE_PENDING_STATUS'],
                              "GOOGLE_INVALID_STATUS" => $d['GOOGLE_INVALID_STATUS']
                            );
      $config = "<?php\n";
      $config .= "if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' ); \n\n";
      foreach( $my_config_array as $key => $value ) {
        $config .= "define ('$key', '$value');\n";
      }
      $config .= "?>";
      if ($fp = fopen(CLASSPATH ."payment/".$this->classname.".cfg.php", "w")) {
          fputs($fp, $config, strlen($config));
          fclose ($fp);
          return true;
     }
     else
        return false;
   }

  /**************************************************************************

  ** name: process_payment()

  ** returns:

  ***************************************************************************/

   function process_payment($order_number, $order_total, &$d) {

        return true;

    }
}


