<?php
/*------------------------------------------------------------------------
# vm_sef_pro - Search Engine Friendly URL's for Virtuemart (Pro)
# ------------------------------------------------------------------------
# author    Jeremy Magne
# copyright Copyright (C) 2010 Daycounts.com. All Rights Reserved.
# Websites: http://www.daycounts.com
# Technical Support: http://www.daycounts.com/en/contact/
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# See http://daycounts.com/ for details
# @version 1.5
-------------------------------------------------------------------------*/

defined( '_JEXEC' ) or die( 'Restricted access - VM Sef Pro v2.0.0' );


require_once (JPATH_ADMINISTRATOR.DS."components".DS."com_vm_sef".DS.'classes'.DS.'helper.php');

if (!function_exists('print_a')) { 
	function print_a($subject){
		echo str_replace("=>","&#8658;",str_replace("Array","<font color=\"red\"><b>Array</b></font>",nl2br(str_replace(" "," &nbsp; ",print_r($subject,true)))));
	}
} 


function virtuemartBuildRoute(&$query)
{
	
	$helper = new VmSefHelper();
	$config = $helper->VmSefConfig;
	//$config = new VmSefConfig();
	$segments = array();
	$debug = JRequest::getVar('debug',false);
	$com_vm_sef = &JComponentHelper::getComponent('com_vm_sef', true);
	if ((!$com_vm_sef->enabled || $config->is_active==0) && !$debug) {
		 return $segments;
	}
	
	//print_a($config);

	$origQuery = $query;

    jimport('joomla.filter.output');
	
	require_once(JPATH_SITE.DS.'components'.DS.'com_virtuemart'.DS.'virtuemart_parser.php' );
	$vm_prefix = VM_TABLEPREFIX;
	
	//Disable rewrite for store administrators
	if ($config->disable_admin_rewrite) {
		global $perm;
		if ($perm->check("admin,storeadmin")) {
			 return $segments;
		}
	}
	
	//Get the value from cache
	ksort($origQuery);
    $dbsef = JFactory::getDBO();
	$q  = "SELECT * FROM #__vm_sef_urls WHERE original = '".serialize($origQuery)."'";
	$dbsef->setQuery($q);
	$knownUrl = $dbsef->loadObject();
	if ($knownUrl) {
		foreach($origQuery as $key => $val) {
			if (in_array($key,$config->vmparams)) {
				unset($query[$key]);
			}
		}
		return explode('/',$knownUrl->rewrited);
	}
	
	//OK, not found in the cache, let's generate it and store it.
	$sef = "";
	
	//Check if we already have some parameters known from the menu item.
	if (isset($query['Itemid'])) {
		jimport( 'joomla.application.menu' );
		$menu =& JMenu::getInstance('site');
		$menu_params = $menu->getParams($query['Itemid']);
	}
	
	//Replace the 'output=lite' querystring by the standard 'tmpl=component'
	if (isset($query['output'])) {
		if ($query['output'] == 'lite') {
			unset($query['output']);
			$query['tmpl']='component';
		}
	}
	
	//Remove pop=0 from url
	if (isset($query['pop'])) {
		if ($query['pop'] == '0') {
			unset($query['pop']);
		}
	}
	
	foreach($config->params as $key => $code) {
		$param = new stdClass();
		$param->task = $key;
		$param->code = $code;
		
		if (isset($query[$param->task]))	{

			//Check if the parameter is already known from the url
			if (isset($menu_params)) {
				if ($menu_params->get($param->task)) {
					if ($param->task == 'page' && (isset($query['keyword']) || isset($_POST['keyword']) ) && $query['page'] == 'shop.browse') {
						$no_unset=1;
					} else {
						$no_unset=0;
					}
					if (!$no_unset) {
						unset($query[$param->task]);
						continue;
					}
				}
			}

			switch($param->task) {
				
				case "page":
					if (isset($query['keyword']) && $query['page'] == 'shop.browse') {
						$no_unset=1;
					} else {
						$no_unset=0;
					}
					switch($query['page']) {
						case 'shop.infopage':
						case 'shop.manufacturer_page':
						case 'shop.recommend':
							unset($query['tmpl']);
							unset($query['pop']);
							break;
						case 'shop.pdf_output':
							unset($query['tmpl']);
							unset($query['pop']);
							unset($query['output']);
							break;
						case 'account.shipto':
							if (!isset($query['user_info_id'])) {
								$query['user_info_id']='';
								$no_unset=1;
							}
							break;
					}
					if ($config->rewrite_mode == 'alias' && isset($query['page']) && ($query['page'] == 'shop.browse' || $query['page'] == 'shop.product_details')) {
						if (!$no_unset)
							unset($query['page']);
					} else {
						$q = 'SELECT * from #__vm_sef_pages WHERE published=1 AND code = \''.$query['page'] . '\'';
						$dbsef->setQuery($q);
						$data = $dbsef->loadAssoc();
						$pageAlias = $data['alias'];
						if ($pageAlias) {
							$segments[]= $pageAlias;	
							unset($query['page']);
							$sef .= $param->code;
						}
					}
					break;
					
				case "product_id":
					if ($config->rewrite_mode == 'id') {
						$segments[]=$query['product_id'];
						unset($query['product_id']);
						$sef .= $param->code;
						break;
					}
					$q = 'SELECT * from #__vm_sef_products WHERE published=1 AND product_id = "'.$query['product_id'] . '"';
					$dbsef->setQuery($q);
					$data = $dbsef->loadAssoc();
					$productAlias = $data['alias'];
					if ($productAlias) {
						if ($config->rewrite_mode == 'alias-safe') {
							$productAlias = $query['product_id']."-".$productAlias;
						}
						$segments[] = $productAlias;
						unset($query['product_id']);
						$sef .= $param->code;
					} else {
						$q = "SELECT * from #__".$vm_prefix."_product WHERE product_id = '".(int)$query['product_id']."'";
						$dbsef->setQuery($q);
						$data = $dbsef->loadAssoc();
						$productAlias = $data['product_name'];
						$productAlias = $helper->generateAlias($productAlias);
						if ($productAlias) {
							$productAlias = $query['product_id']."-".$productAlias;
							$segments[] = $productAlias;
							unset($query['product_id']);
							$sef .= $param->code;
						}
					}
					break;
					
				case "manufacturer_id":
					if ($config->rewrite_mode == 'id') {
						$segments[]=$query['manufacturer_id'];
						unset($query['manufacturer_id']);
						$sef .= $param->code;
						break;
					}
					$q = 'SELECT * from #__vm_sef_manufacturers WHERE published=1 AND manufacturer_id = "'.$query['manufacturer_id'] . '"';
					$dbsef->setQuery($q);
					$data = $dbsef->loadAssoc();
					$manufacturerAlias = $data['alias'];
					if ($manufacturerAlias) {
						if ($config->rewrite_mode == 'alias-safe') {
							$manufacturerAlias = $query['manufacturer_id']."-".$manufacturerAlias;
						}
						$segments[] = $manufacturerAlias;
						unset($query['manufacturer_id']);
						$sef .= $param->code;
					} else {
						$q = "SELECT * from #__".$vm_prefix."_manufacturer WHERE manufacturer_id = '".(int)$query['manufacturer_id']."'";
						$dbsef->setQuery($q);
						$data = $dbsef->loadAssoc();
						$manufacturerAlias = $data['mf_name'];
						$manufacturerAlias = $helper->generateAlias($manufacturerAlias);
						if ($manufacturerAlias) {
							$manufacturerAlias = $query['manufacturer_id']."-".$manufacturerAlias;
							$segments[] = $manufacturerAlias;
							unset($query['manufacturer_id']);
							$sef .= $param->code;
						}
					}
					break;
					
				case "vendor_id":
					if ($config->rewrite_mode == 'id') {
						$segments[]=$query['vendor_id'];
						unset($query['vendor_id']);
						$sef .= $param->code;
						break;
					}
					$q = 'SELECT * from #__vm_sef_vendors WHERE published=1 AND vendor_id = "'.$query['vendor_id'] . '"';
					$dbsef->setQuery($q);
					$data = $dbsef->loadAssoc();
					$vendorAlias = $data['alias'];
					if ($vendorAlias) {
						if ($config->rewrite_mode == 'alias-safe') {
							$vendorAlias = $query['vendor_id']."-".$vendorAlias;
						}
						$segments[] = $vendorAlias;
						unset($query['vendor_id']);
						$sef .= $param->code;
					} else {
						$q = "SELECT * from #__".$vm_prefix."_vendor WHERE vendor_id = '".(int)$query['vendor_id']."'";
						$dbsef->setQuery($q);
						$data = $dbsef->loadAssoc();
						$vendorAlias = $data['vendor_name'];
						$vendorAlias = $helper->generateAlias($vendorAlias);
						if ($vendorAlias) {
							$vendorAlias = $query['vendor_id']."-".$vendorAlias;
							$segments[] = $vendorAlias;
							unset($query['vendor_id']);
							$sef .= $param->code;
						}
					}
					break;
					
				case "category_id":
					if ($config->rewrite_mode == 'id') {
						$segments[]=$query['category_id'];
						unset($query['category_id']);
						$sef .= $param->code;
						break;
					}
					$category_tree = array();
					$category_id = $query['category_id'];
					$category_tree[] = $category_id;
					$category_tree = getCategorytree($vm_prefix,$category_id,$category_tree);
					//Return the tree to get parents first
					krsort($category_tree);
					//echo "<pre>";
					//print_r($category_tree);
					//echo "</pre>";
				
					foreach ($category_tree as $category_id) {
						$q = 'SELECT * from #__vm_sef_categories WHERE category_id = "'.$category_id . '"';
						$dbsef->setQuery($q);
						$data = $dbsef->loadAssoc();
						$catAlias = $data['alias'];
						if ($catAlias) {
							if ($config->rewrite_mode == 'alias-safe') {
								$catAlias = $category_id."-".$catAlias;
							}
							$segments[] = $catAlias;
							unset($query['category_id']);
							$sef .= $param->code;
						} else {
							$q = "SELECT * from #__".$vm_prefix."_category WHERE category_id = '".$category_id."'";
							$dbsef->setQuery($q);
							$data = $dbsef->loadAssoc();
							$catAlias = $data['category_name'];
							if ($catAlias) {
								$catAlias = $category_id."-".$helper->generateAlias($catAlias);
								$segments[] = $catAlias;
								unset($query['category_id']);
								$sef .= $param->code;
							} 
						}
					}
					break;
				
				case "flypage":
					if ($config->flypage) {
						if ($config->flypage == $query['flypage'] || $config->rewrite_mode == 'alias') {
							unset($query['flypage']);
						} else {
							$alias = str_replace(".tpl","",$query['flypage']);
							$alias = str_replace("-","|",$alias);
							$segments[] = str_replace(".tpl","",$query['flypage']);
							unset($query['flypage']);
							$sef .= $param->code;
						}
					} else {
						$alias = str_replace(".tpl","",$query['flypage']);
						$alias = str_replace("-","|",$alias);
						$segments[] = str_replace(".tpl","",$query['flypage']);
						unset($query['flypage']);
						$sef .= $param->code;
					}
					break;
					
//				case "file_id":
//					if ($config->rewrite_mode == 'id') {
//						$segments[]=$query['file_id'];
//						unset($query['file_id']);
//						$sef .= $param->code;
//						break;
//					}
//					$q = "SELECT * from #__".$vm_prefix."_product_files WHERE file_is_image=0 AND file_id = '".(int)$query['file_id']."'";
//					$dbsef->setQuery($q);
//					$data = $dbsef->loadAssoc();
//					$fileAlias = $data['file_title'];
//					//$fileAlias = basename($data['file_name']);
//					
//					if ($fileAlias) {
//						if ($config->rewrite_mode == 'alias-safe') {
//							$fileAlias = $query['file_id']."-".$fileAlias;
//						}
//						$segments[] = $fileAlias;
//						unset($query['file_id']);
//						$sef .= $param->code;
//					}
//					break;
					
				default:
					$segments[] = $query[$param->task];
					unset($query[$param->task]);
					$sef .= $param->code;
			}
		}
	}

	if ($sef) {
		$query['sef']=$sef;
	}
	

	//*************
	// URL Caching
	//*************	
	if ($config->rewrite_mode == 'alias') {
		$cleanQuery = $query;
		unset($cleanQuery['option']);
		unset($cleanQuery['Itemid']);
		unset($cleanQuery['sef']);
		
		unset($query['sef']);
		
		//echo serialize($origQuery);
		
		$cleanQueryStr = JURI::buildQuery($cleanQuery);
		

		$rewrited = implode('/',$segments);
		//if ($cleanQueryStr)
		//	$rewrited .= '?'.$cleanQueryStr;
	
		//We store the rewited page and associated parameters
		if ($rewrited) {
			$q  = "SELECT * FROM #__vm_sef_urls WHERE rewrited = '".$rewrited."'";
			$dbsef->setQuery($q);
			$dbsef->query();
			$data = $dbsef->loadAssoc();
			if (sizeof($data)==0) {
				$q  = "INSERT IGNORE INTO #__vm_sef_urls VALUES (null,'".$rewrited."','".serialize($origQuery)."',1,0,'".$sef."')";
				//echo $q;
				$dbsef->setQuery($q);
				$dbsef->query();
			}
			//print_a($dbsef);
		}
	}
	
	return $segments;
}

function getCategorytree($vm_prefix,$category_id,&$category_tree) {
    $dbsef = JFactory::getDBO();
	$q  = "SELECT category_parent_id FROM #__".$vm_prefix."_category_xref WHERE category_child_id = '".$category_id."'";
	//echo '<br/>'.$q;
	$dbsef->setQuery($q);
	$category_parent_id = $dbsef->loadResult();
	if ($category_parent_id > 0) {
		$category_tree[] = $category_parent_id;
		$category_tree = getCategorytree($vm_prefix,$category_parent_id,$category_tree);
	}
	return $category_tree;
}

function virtuemartParseRoute($segments)
{

	$vars = array();
	$helper = new VmSefHelper();
	$config = $helper->VmSefConfig;

	$debug = JRequest::getVar('debug',false);
	if (!JComponentHelper::isEnabled('com_vm_sef', true) && !$debug) {
		return $vars;
	}

	$dbsef = JFactory::getDBO();

	if (JComponentHelper::isEnabled('com_joomfish', true)) {
		$jfm = JoomFishManager::getInstance();
		$lang =& JFactory::getLanguage();
		$languageid = $jfm->getLanguageID($lang->getTag());
		$joomfish=true;
	}

	if ($config->rewrite_mode == 'alias') {
		$rewrited = implode('/',$segments);
		//echo $rewrited.'<br>';
		$rewrited = str_replace(":","-",$rewrited);
		
		$uri = &JFactory::getURI();
		$vars = $uri->getQuery(true);
		unset($vars['lang']);
		$i=0;
		$queryStrElts = array();
		foreach($vars as $key=>$val) {
			$queryStrElts[$key] = $val;
		}
	
		//We look for the page rewrited (only url segments) and then we append querystrings
		$q  = "SELECT * FROM #__vm_sef_urls WHERE published=1 AND rewrited = '".$rewrited."'";
		$dbsef->setQuery($q);
		$data = $dbsef->loadAssoc();
		//echo ($q);
		//die();
		if ($data['original']) {
			$vars = unserialize($data['original']);
			//print_a($vars);
			//Append the existing query strings
			foreach ($queryStrElts as $key => $val) {
				$vars[$key] = $val;
			}
			//print_a($vars);
			
			return $vars;
		}
	}
	
	if (isset($_REQUEST['sef'])) {
		$sef = $_REQUEST['sef'];
	} else {
		$sef = '';
	}
	
	//Task
	$i = strpos($sef,'t');
	if (isset($i) && is_numeric($i)) {
		$vars['task'] = $segments[$i];
	}
	
	//Pages
	$i = strpos($sef,'h');
	if (isset($i) && is_numeric($i)) {
		$alias = str_replace(":","-",$segments[$i]);
		$q = 'SELECT * from #__vm_sef_pages WHERE published=1 AND alias = \''.$alias . '\'';
		$dbsef->setQuery($q);
		$data = $dbsef->loadAssoc();
		$pageCode = $data['code'];
		if ($pageCode) {
			$vars['page'] = $pageCode;
		} else if ($joomfish) {
			$q = 'SELECT * from #__jf_content WHERE published=1 AND reference_table=\'__vm_sef_pages\' AND reference_field=\'alias\' AND language_id="'.$languageid.'" AND value = "'. $alias . '"';
			$dbsef->setQuery($q);
			$data = $dbsef->loadAssoc();
			$pageCode = $data['reference_id'];
			if ($pageCode) {
				$vars['page'] = $pageCode;
			} else {
				$vars['page'] = $segments[$i];
			}
		}
		//Cleanup constant parameters
		switch ($pageCode) {
			case 'shop.infopage':
			case 'shop.manufacturer_page':
			case 'shop.recommend':
				$vars['tmpl'] = 'component';
				$vars['pop'] = '1';
				break;
			case 'shop.pdf_output':
				$vars['tmpl'] = 'component';
				$vars['pop'] = '1';
				$vars['output'] = 'pdf';
				break;
		}
	}

	//Product
	$i = strpos($sef,'p');
	if (isset($i) && is_numeric($i)) {
		$alias = str_replace(":","-",$segments[$i]);
		$q = 'SELECT * from #__vm_sef_products WHERE published=1 AND alias = "'. $alias . '"';
		$dbsef->setQuery($q);
		$data = $dbsef->loadAssoc();
		$product_id = $data['product_id'];
		if ($product_id) {
			$vars['product_id'] = $product_id;
		} else {
			if ($joomfish) {
				$q = 'SELECT * from #__jf_content WHERE published=1 AND reference_table=\'vm_sef_products\' AND reference_field=\'alias\' AND language_id="'.$languageid.'" AND value = "'. $alias . '"';
				$dbsef->setQuery($q);
				$data = $dbsef->loadAssoc();
				$product_id = $data['reference_id'];
			}
			if ($product_id) {
				$vars['product_id'] = $product_id;
			} else {
				$vars['product_id'] 	= $segments[$i];
			}
		}
	}
	
	//Manufacturer
	$i = strpos($sef,'m');
	if (isset($i) && is_numeric($i)) {
		$alias = str_replace(":","-",$segments[$i]);
		$q = 'SELECT * from #__vm_sef_manufacturers WHERE published=1 AND alias = "'. $alias . '"';
		$dbsef->setQuery($q);
		$data = $dbsef->loadAssoc();
		$manufacturer_id = $data['manufacturer_id'];
		if ($manufacturer_id) {
			$vars['manufacturer_id'] = $manufacturer_id;
		} else {
			$vars['manufacturer_id'] 	= $segments[$i];
		}
	}
	
	//Vendor
	$i = strpos($sef,'v');
	if (isset($i) && is_numeric($i)) {
		$alias = str_replace(":","-",$segments[$i]);
		$q = 'SELECT * from #__vm_sef_vendors WHERE published=1 AND alias = "'. $alias . '"';
		$dbsef->setQuery($q);
		$data = $dbsef->loadAssoc();
		$vendor_id = $data['vendor_id'];
		if ($vendor_id) {
			$vars['vendor_id'] = $vendor_id;
		} else {
			$vars['vendor_id'] 	= $segments[$i];
		}
	}
	
	//SSL
	$i = strpos($sef,'s');
	if (isset($i) && is_numeric($i)) {
		$vars['ssl_redirect'] 	= $segments[$i];
	}
	
	//Category
	$i = strrpos($sef,'c'); //Get the last occurence of category
	if (isset($i) && is_numeric($i)) {
		$alias = str_replace(":","-",$segments[$i]);
		$q = 'SELECT * from #__vm_sef_categories WHERE published=1 AND alias = "'.$alias . '"';
		$dbsef->setQuery($q);
		$data = $dbsef->loadAssoc();
		$category_id = $data['category_id'];
		if ($category_id) {
			$vars['category_id'] = $category_id;
		} else {
			if ($joomfish) {
				$q = 'SELECT * from #__jf_content WHERE published=1 AND reference_table=\'vm_sef_categories\' AND reference_field=\'alias\' AND language_id="'.$languageid.'" AND value = "'. $alias . '"';
				$dbsef->setQuery($q);
				$data = $dbsef->loadAssoc();
				$category_id = $data['reference_id'];
			}
			if ($category_id) {
				$vars['category_id'] = $category_id;
			} else {
				$vars['category_id'] 	= $segments[$i];
			}
		}
	}
	
	//Order
	$i = strpos($sef,'o');
	if (isset($i) && is_numeric($i)) {
		$vars['order_id'] 	= $segments[$i];
	}
	
	//Flypage
	if ($config->flypage) {
		$i = strpos($sef,'f');
		if (isset($i) && is_numeric($i)) {
			$vars['flypage'] 	= $segments[$i].".tpl";
		} else {
			$forcedFlypage = ($data['code']) ? $data['code'] : 'flypage.tpl';
			$vars['flypage'] = $config->flypage;
		}
	} else {
		$i = strpos($sef,'f');
		if (isset($i) && is_numeric($i)) {
			$flypage = str_replace("|","-",$segments[$i]);
			$vars['flypage'] 	= $flypage.".tpl";
		}
	}


	
	//Files (downloads)
//	$i = strpos($sef,'d');
//	if (isset($i) && is_numeric($i)) {
//		$alias = str_replace(":","-",$segments[$i]);
//		$q = 'SELECT * from #__'.VM_TABLEPREFIX.'_product_files WHERE file_published=1 AND file_is_image=0 AND file_title = "'. $alias . '"';
//		$dbsef->setQuery($q);
//		$data = $dbsef->loadAssoc();
//		$file_id = $data['file_id'];
//		if ($vendor_id) {
//			$vars['file_id'] = $file_id;
//		} else {
//			$vars['file_id'] 	= $segments[$i];
//		}
//	}

	return $vars;
}


?>