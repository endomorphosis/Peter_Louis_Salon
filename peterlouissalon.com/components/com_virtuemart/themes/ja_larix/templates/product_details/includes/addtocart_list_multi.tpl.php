<?php if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' ); ?>

<?php
mm_showMyFileName(__FILE__);

// Start Ouputing the Child Detail
?>
<div class="vmCartDetails<?php echo $cls_suffix ?>">
<!-- Output The heading -->

    <div class="vmCartChildHeading">
        <span style="float: left;width: 15%;padding-left: 32px;">Image</span>
        <span style="float: left;width: 10%;">SKU</span>
	    <span style="float: left;width: 22%;margin-left: 43px;">Title</span>
	    <span style="float: left;width: 13%;">Quantity</span>
	    <span style="float: left;width: 12%;">Price</span>
	    </div><br/>
	<?php
// Loop through each row and build the table
foreach( $products as $product ) { 		

    foreach( $product as $attr => $val ) {
			// Using this we make all the variables available in the template
			// translated example: $this->set( 'product_name', $product_name );
			$this->set( $attr, $val );
    }
    
    if( CHECK_STOCK == '1' && !$product['product_in_stock'] ) {
     	$notify = true;
    } else {
    	$notify = false;
    }
    
    ?>

    <div class="vmCartChild " style="width:680px;">
        <form action="<?php echo $mm_action_url ?>index.php" method="post" name="addtocart" id="addtocart<?php echo $product['product_id'] ?>" class="addtocart_form" <?php if( $this->get_cfg( 'useAjaxCartActions', 1 ) && !$notify ) { echo 'onsubmit="handleAddToCart( this.id );return false;"'; } ?>>
            <div class="vmCartChildElement<?php echo $cls_suffix ?>">
                <input type="hidden" name="prod_id[]" value="<?php echo $product['product_id'] ?>" />
                <input type="hidden" name="product_id" value="<?php echo $product['parent_id'] ?>" />
				<!-- child Image-->
				<style type="text/css">
				#child_image img{
					margin-left: auto;margin-right: auto;align:center;color: #78797A;
				}
				#child_image a{
					display: block;text-align: center;color: #78797A;
				}
				#product_title a{
					font-size:16px;color: #78797A;
				}
				</style>
				 <span id="child_image" class="vmChildDetail<?php echo $cls_suffix ?>" style="float: left;width :17%;" />
                <?php echo $product['product_image'] ?></span>
				
				<!-- SKU-->
				 <span id="child_image" class="vmChildDetail<?php echo $cls_suffix ?>" style="color: #78797A;float: left;width: 13%;margin-top: 45px;" />
                <?php echo $product['product_sku'] ?></span>
				
				
				<span id="product_title" class="vmChildDetail<?php echo $cls_suffix ?>" style="color: #78797A;float: left;width :28%;margin-top: 45px;font-weight: bold;" />
                <?php echo $product['product_title'] ?></span>
				<?php
				
				 if (USE_AS_CATALOGUE != '1' ) { ?>
                    <span  class="vmChildDetail<?php echo $cls_suffix ?>" style="float: left;width :12%;margin-top: 40px;"><?php echo $product['quantity_box'] ?></span>
                <?php } 
				
				
				
				if( $_SESSION['auth']['show_prices'] && _SHOW_PRICES) {  ?>           
                    <span  class="vmChildDetail<?php echo $cls_suffix ?>" style="color: #78797A;float: left;width :13%;margin-top: 45px;" >
                    <?php
                    if( $product['price'] != $product['actual_price'] ) { ?>
                        <span class="product-Old-Price"><?php echo $product['price'] ?>&nbsp;</span>
                    <?php } 
                    echo $product['actual_price'] ?></span>
                <?php } ?>
				
				
				
                <?php // Ouput Each Attribute
               /* if( !empty( $product['attrib_value'] )) {
	                foreach($product['attrib_value'] as $attribute) { ?>
	                    <span class="vmChildDetail<?php echo $cls_suffix ?>" style="float: left;width :<?php echo $attrib_width ?>;" />
		                <?php echo " ".$attribute ?></span>
	                <?php 
					}
				} */
                if (USE_AS_CATALOGUE != '1'  && $product_price != "" && !stristr( $product_price, $VM_LANG->_('PHPSHOP_PRODUCT_CALL'))) { 

					$button_lbl = $VM_LANG->_('PHPSHOP_CART_ADD_TO');
					$button_cls = 'addtocart_button';
					if( CHECK_STOCK == '1' && !$product['product_in_stock'] ) {
						$button_lbl = $VM_LANG->_('VM_CART_NOTIFY');
						$button_cls = 'notify_button';
					}

                	?>
                   <span class="vmChildDetail<?php echo $cls_suffix ?>" style="width :20%;">
                    <input style="font-size:0px;width: 96px;margin-top: 36px;" type="submit" class="<?php echo $button_cls ?>" value="<?php echo $button_lbl ?>" title="<?php echo $button_lbl ?>" /></span>
                <?php } ?>
                
                
            </div>
            <br style="clear: both;">
            <input type="hidden" name="flypage" value="shop.<?php echo $product['flypage'] ?>" />
            <input type="hidden" name="category_id" value="<?php echo $product['category_id'] ?>" />
            <input type="hidden" name="page" value="shop.cart" />
            <input type="hidden" name="func" value="cartAdd" />
            <input type="hidden" name="option" value="com_virtuemart" />
            <input type="hidden" name="Itemid" value="<?php echo $product['Itemid'] ?>" />
            <input type="hidden" name="set_price[]" value="" />
            <input type="hidden" name="adjust_price[]" value="" />
            <input type="hidden" name="master_product[]" value="" />    
            <?php
            // Out Put Product Type 
            if ($display_product_type == "Y" && $product['product_type'] != "") { ?>  
                <div class="vmChildType<?php echo $cls_suffix ?>">
                <?php echo $product['product_type'] ?>
                </div>
            <?php } 
            // Output Advanced & Custom Attributes
            if(USE_AS_CATALOGUE != '1' && ($product['advanced_attribute'] != "" || $product['custom_attribute'] != "")) { ?>
                <div class="vmCartAttributes<?php echo $cls_suffix ?>">
                    <?php if($product['advanced_attribute']) {
                        echo $product['advanced_attribute'];
                    }
                    if($product['custom_attribute']) {
                        echo $product['custom_attribute'];
                    }
                ?>
                </div>
            <?php } ?>
        </form>
            </div>
    
    <?php } ?>
    </div >