/* Joomla! 1.5 One Page Checkout Module for VirtueMart
*
* @version $Id: onepage.js 05-24-2011
 * author: Polished Geek
 * Website: PolishedGeek.com
 * Email: info@PolishedGeek.com
 * Phone: 919-374-2425
 *
 * license http://www.gnu.org/copyleft/gpl.html GNU/GPL
 *
*/


/* This is a the javascript/AJAX file that goes with the VirtueMart One Page checkout module */



var selected = function() { customShippingDateUpdated(); }; 

bAMode = true;
var opcoDebug = false;
window.onload = onepageinit;
if(window.addEvent) {
	window.addEvent('load', onepageinit);
} 
else if (window.addEventListener) // W3C standard
{
	window.addEventListener('load', onepageinit, false); // NB **not** 'onload'
} 
else if (window.attachEvent) // Microsoft
{
	window.attachEvent('onload', onepageinit);
}



var requireShippingMethod = false;

var OpcoHelper = new Class({
	hideById: function(id) {
	    this.hideElement($(id));
	},
	
	showById: function (id) {
		this.showById(id, 'block');
	},
	
	showById: function(id, displayType) {
		this.showElement($(id), displayType);
	},
	
	hideElement: function(elem) {
	    if (elem == null || elem.style == undefined) return;
	    elem.style.visibility = 'hidden';
	    elem.style.display = 'none';
	},

	showElement: function(elem, displayType) {
	    if (elem == null || elem.style == undefined) return;
	    elem.style.visibility = 'visible';
	    elem.style.display = displayType;
	},

	enableElement: function(elem, status) {
	    if (elem != null || elem != undefined) elem.disabled = !status;
	},
	
	emptyById: function(id) {
		this.emptyElement($(id));
	},
	
	emptyElement: function(elem) {
		if (elem != null || elem != undefined) elem.empty();
	},
	
	isBrowserIE: function () {
		return (navigator.appVersion.indexOf("MSIE") != -1);
	}, 
	
	stringToElement: function(s) {
        var div = new Element('div');
        div.innerHTML = s;
        return div.getFirst();
    } 
});

var opcoHelper = new OpcoHelper();

function debug_opco_msg(msg) {
	if (opcoDebug) alert(msg);
}


function showlogin() {
	opcoHelper.showById('loginsection', 'inline');
	$('register_account').checked = false;

	if(registerCheckoutEnabled()) {
		//showaccountcreation();
	}
	
	opcoHelper.hideById('registersection');	
}

function showregister() {
	opcoHelper.hideById('loginsection');
	if($('register_account') != null) {$('register_account').checked = false;}
	
	nocreateaccount();	
}

function nocreateaccount() {
    /*	if(guestCheckoutEnabled() == false) {
    return;
    }	
    */

    /*	if(document.getElementById('username_div').previousSibling != null) {
    document.getElementById('username_div').previousSibling.innerHTML = '';
    }
    */
	
	if ($$("input[name=user_id]") == null 
			|| $$("input[name=user_id]")[0].value <= 60)
	{
	    opcoHelper.hideById('loginsection');
	}

    if ($('register_account') != null) {
        $('register_account').checked = false;
    }

    opcoHelper.showById('registersection', '');

    if (document.getElementById('username_input') != null) {
        opcoHelper.hideById('username_input');
        opcoHelper.hideById('username_div');
        opcoHelper.hideById('password_input');
        opcoHelper.hideById('password_div');
        opcoHelper.hideById('password2_input');
        opcoHelper.hideById('password2_div');
    }
}


function showCreateAccount() {

/*	if(document.getElementById('username_div').previousSibling != null) {
		document.getElementById('username_div').previousSibling.innerHTML = '';
	}	
*/
	
	if ($$("input[name=user_id]") == null
			|| $$("input[name=user_id]")[0].value <= 60)
	{
		opcoHelper.hideById('loginsection');
	}	
	
	showaccountcreation();
}

function showaccountcreation() {
    if ($('register_account') == null) {
        return;
    }

    $('register_account').checked = true;
    $('register_account').value = 'Yes';
	
	opcoHelper.showById('registersection', '');	

    opcoHelper.showById('username_input', '');
    opcoHelper.showById('username_div', '');
    opcoHelper.showById('password_input', '');
    opcoHelper.showById('password_div', '');
    opcoHelper.showById('password2_input', '');
    opcoHelper.showById('password2_div', '');
}

function shippingEnabled() {
//	var isShippingEnabled = document.getElementById('newshippingaddress');
    //	if(isShippingEnabled.length == 0) {
	return ($('newshippingaddress') != null);
}




function showNewAddress() {
    opcoHelper.showById('newshippingaddress', 'inline');
}


function hideNewAddress() {
    opcoHelper.hideById('newshippingaddress');
}

function hidePaymentSection() {
    opcoHelper.hideById('paymentmethodsection');

	var checkoutSteps = document.getElementsByName('checkout_this_step[]');

	for(var j=0; j<checkoutSteps.length; j++) {
		if(checkoutSteps[j].value == 'CHECK_OUT_GET_PAYMENT_METHOD') {
//			paymentmethodsection_div.removeChild(checkoutSteps[j]);
			checkoutSteps[j].name = 'checkout_old';
//			checkoutSteps[j].execCommand("Delete");
		}
	}
	
	var paymentMethods = document.getElementsByName('payment_method_id');

	for(var k=0; k<paymentMethods.length; k++) {
			paymentMethods[k].name = 'payment_method_id_save';
	//	paymentMethods[k].value = 0 - paymentMethods[k].value;
	//			checkoutSteps[j].execCommand("Delete");
	}

//	document.getElementById('order_total').value = "0.00";

/*	var rng = document.body.createTextRange();
	rng.findText('<input type="hidden" name="checkout_this_step[]" value="CHECK_OUT_GET_PAYMENT_METHOD" />');
	rng.select();
	rng.execCommand("Delete");
*/
}

function initPaymentMethodArea() {
	var payment_methods = $$("input[name=payment_method_id]");
	
	if (payment_methods != null && payment_methods != undefined 
	    && payment_methods.length != undefined && payment_methods.length > 0) {
		for(var j=0; j<payment_methods.length; j++) {
			updatePaymentMethodHandler($$("input[name=payment_method_id]")[j], handlePaymentMethodChange);
		}
	}
	else if (payment_methods != null && payment_methods != undefined) {
		updatePaymentMethodHandler($$("input[name=payment_method_id]"), handlePaymentMethodChange);
	}
}

function showPaymentSection() {

	initPaymentMethodArea();

	// populate the initial list
	handlePaymentMethodChange();
	
	opcoHelper.showById('paymentmethodsection', 'inline');

//	var newhidden = document.createElement('<input type="hidden" name="checkout_this_step[]" value="CHECK_OUT_GET_PAYMENT_METHOD" />');
//	paymentmethodsection_div.appendChild(newhidden);
//	var checkoutSteps = document.getElementsByName('checkout_this_step[]');

//	for(var j=0; j<checkoutSteps.length; j++) {
//		if(checkoutSteps[j].value == '') {
//			checkoutSteps[j].value = 'CHECK_OUT_GET_PAYMENT_METHOD';
//		}
//	}

}


function alreadyLoggedIn() {
    var togglecheck = document.getElementsByName('togglecheck');

    var myReturn = true;
    for (var j = 0; j < togglecheck.length; j++) {
        if (togglecheck[j].value == 'login') {
            myReturn = false;
        }
    }
    return myReturn;
}	

function guestCheckoutEnabled() {
/*	var togglecheck = document.getElementsByName('togglecheck');

	var myReturn = false;
	for(var j=0; j<togglecheck.length; j++) {
		if(togglecheck.value == 'register') {
			myReturn = true;
		}

	}
*/
	return ($('register_radio') != null);

/*	var togglecheck = document.getElementById('toggler1').value;

	if(togglecheck == 'register') {
		myReturn = true;
	} else {
		myReturn = false;
	}
	return myReturn;
*/	
}	

function registerCheckoutEnabled() {
	var togglecheck = document.getElementsByName('togglecheck');

	var myReturn = false;
	for(var j=0; j<togglecheck.length; j++) {
		if(togglecheck.value == 'createaccount') {
			myReturn = true;
		}
	}
	return myReturn;
}

function removeBRTags(sElementId) {
	var myElement = $(sElementId);

	if(myElement != null && myElement != undefined && myElement.getElementsByTagName != undefined) {
		var elms = myElement.getElementsByTagName("br");
		for(var i=0, maxI = elms.length; i < maxI; i++) {
			opcoHelper.hideElement(elms[i]);
		}
	}
}


function taxCalculated() {
	setShipToInfoWithoutSubmit();
//	updatetotals();
	opcoHelper.showById('formSubmitButton', 'inline');
	opcoHelper.hideById('calculatetax_button');
}

function calculatetax_ajax() {
	var bAjaxMode=bAMode;
	//	bAjaxMode = false;

	if (!validateAll(false)) return false;

	var poststr = "";
	poststr = poststr + "Div_ID=checkoutProcessingError";

	var shipping_value = 0;


	var registerSectionDiv = document.getElementById('registersection');

	var elms = registerSectionDiv.getElementsByTagName("*");

	for(var i=0, maxI = elms.length; i < maxI; i++) {
		var elm = elms[i];
		switch(elm.type) {
			case "radio":
			case "checkbox":
			if(!elm.checked) continue;
			case "text":
			case "textarea":
			case "button":
			case "reset":
			case "submit":
			case "file":
			case "hidden":
			case "password":
			case "image":
			case "select-one":
			case "select-multiple":
			poststr = poststr + "&" + elm.name + "=" + encodeURIComponent(elm.value);
		}
	}
			


	if(shippingEnabled() == true) {

		var ship_to_info_id = document.getElementsByName('ship_to_info_id');

		var ship_to_id = getCheckedId(ship_to_info_id);
		var ship_to_value = getCheckedValue(ship_to_info_id);

		if(ship_to_id == "NEWSHIPADDRESS") {

			var newAddressDiv = document.getElementById('newshippingaddress');

			var elms2 = newAddressDiv.getElementsByTagName("*");

			for(var i=0, maxI = elms2.length; i < maxI; i++) {
				var elm = elms2[i];
				switch(elm.type) {
					case "radio":
					case "checkbox":
					if(!elm.checked) continue;
					case "text":
					case "textarea":
					case "button":
					case "reset":
					case "submit":
					case "file":
					case "hidden":
					case "password":
					case "image":
					case "select-one":
					case "select-multiple":
					poststr = poststr + "&" + elm.name + "=" + encodeURIComponent(elm.value);
				}
			}	

		}

		var radios = document.getElementsByName("shipping_rate_id");

		for(var i=0; i< radios.length; i++) {
			if(radios[i].checked) {
				shipping_value= i;
			}
		}	


		poststr = poststr + "&" + "ship_to_info_id" + "=" + encodeURIComponent(ship_to_value);

		if(radios.length > 0) {
			poststr = poststr + "&shipping_rate_id=" + encodeURIComponent(radios[shipping_value].value);
		}
	}	

	var myagreed = document.getElementsByName("agreed")[0];

	var agreedval = '1';

//	if(myagreed.checked) {
//		agreedval = myagreed.value;
//	}

	poststr = poststr + "&agreed=" + agreedval;
	poststr = poststr + "&opco_title=" + encodeURIComponent(getOpcoModuleName());

//	alert(poststr);
	
	makePOSTRequest('modules/mod_virtuemart_onepagecheckout/calculatetax_ajax.php', poststr, bAjaxMode);
	return false;

}

function handleRegistrationCountryChange() {
	handleCountryChange('registersection');
}

function handleShippingCountryChange() {
	handleCountryChange('newshippingaddress');
}

function handleCountryChange(section) {
	// find the country element
	var country_elem = $$('div#' + section + ' select#country_field');
	var state_elem = $$('div#' + section + ' select#state');
	
	// if either state or country not found
	if (country_elem == null || state_elem == null) return;
	
	// get the selected country value
	selected_country = '';
	for (var i = 0; i < country_elem[0].length; i++) {
		if (country_elem[0][i].selected) {
			selected_country = country_elem[0][i].value;
			break;
		}
	}
	
	// get state list
	var list = state_elem[0];
	var source = states;
	var key = selected_country;
	var orig_key = originalPos;
	var orig_val = originalOrder;
	
	// empty the list
    for (i in list.options.length) {
        list.options[i] = null;
    }
    
    i = 0;
    for (x in source) {
        if (source[x][0] == key) {
            opt = new Option();
            opt.value = source[x][1];
            opt.text = source[x][2];

            if ((orig_key == key && orig_val == opt.value) || i == 0) {
                opt.selected = true;
            }
            list.options[i++] = opt;
        }
    }
    list.length = i;
}

function updateCountryHandler(c, f) {
	if (c == null) return;
	if (c.length == 0) return;

    c[0].onchange = '';
    if (opcoHelper.isBrowserIE()) {
    	c[0].detachEvent('change', changeStateList);
    }
    c.addEvent("change", f );
    c.addEvent("click", f);
}

function initShippingAddressArea() {
    // process country and state inside the registration address area
	updateCountryHandler($$("div#registersection select#country_field"), handleRegistrationCountryChange);
	
    var state_labels = $$('label[for="state_field"]');
    if (state_labels != null && state_labels.length != undefined) {
        for (var i = 0; i < state_labels.length; i++) {
            state_labels[i].innerHTML = state_labels[i].innerHTML.replace(/\//g, ' / ');
        }
    }

    // process country and state inside the shipping address area
    updateCountryHandler($$("div#newshippingaddress select#country_field"), handleShippingCountryChange);

    // add click handler for shipping addresses
	var ship_to_info_ids = document.getElementsByName('ship_to_info_id');
	var isShipInfoSelected = false;
	for (var i = 0; i < ship_to_info_ids.length; i++) {
	    ship_to_info_ids[i].onclick = (ship_to_info_ids[i].id == "NEWSHIPADDRESS") 
	    								? shippingstatusNew : shippingstatusExisting;
	    
	    // prevents multiple ship addresses from being selected
	    if (ship_to_info_ids[i].checked) {
	    	if (!isShipInfoSelected) isShipInfoSelected = true;
	    	else ship_to_info_ids[i].checked = false;
	    }
	}

	// hide the new address
    if (shippingEnabled() == true) {
        hideNewAddress();
    }

    // remove any BR tags
    removeBRTags('newshippingaddress');
}

function updatePaymentMethodHandler(c, f) {
	if (c == null) return;

    c.onchange = '';
    if (opcoHelper.isBrowserIE()) {
    	c.detachEvent('change', changeCreditCardList);
    }
    c.addEvent("change", f );
    c.addEvent("click", f);
}

function handlePaymentMethodChange() {
	var selected_payment = null;
	
	var payments = document.getElementsByName("payment_method_id");

	for(var j=0; j < payments.length; j++) {
		if(payments[j].checked) {
			selected_payment = payments[j].id;
		}
	}
	
	// get orders list
	if(typeof(window['orders']) == "undefined")
		return;
	var listname = 'creditcard_code';
	var list = document.getElementsByName(listname)[0]
	var source = orders;
	var key = selected_payment;
	var orig_key = originalPos;
	var orig_val = originalOrder;
	
	// empty the list
    for (i in list.options.length) {
        list.options[i] = null;
    }
    
    i = 0;
    for (x in source) {
        if (source[x][0] == key) {
            opt = new Option();
            opt.value = source[x][1];
            opt.text = source[x][2];

            if ((orig_key == key && orig_val == opt.value) || i == 0) {
                opt.selected = true;
            }
            list.options[i++] = opt;
        }
    }
    list.length = i;
} 

function onepageinit() {
	updatetotals();
	
	initShippingAddressArea();
	
	setInnerHtmlbyForHtml('label', 'register_account', '');
	if(document.getElementById('register_account') != null) {
		document.getElementById('register_account').checked = false;
		document.getElementById('register_account').value = '';
		document.getElementById('register_account').style.visibility = 'hidden';
		document.getElementById('register_account').style.display = 'none';
		
	}

//	displaycoupon();

	if(registerCheckoutEnabled() != true) {
		nocreateaccount();
	}
    
	if(document.getElementById('createaccount_radio') != null) {
		if(document.getElementById('createaccount_radio').checked) {
			showCreateAccount();
		}
	}

	removeBRTags('registersection');

	var password2 = document.getElementById('password2_field');

	if(password2 != null) {
		password2.onblur = validatePassword;
	}

	if(document.getElementById('email_field') != null) {
		document.getElementById('email_field').onblur = fixUsername;
		fixUsername();
	}

	// initial login form
	var elem = $("opcologinusername");
    if (elem != null && elem.addEvent != undefined) elem.addEvent("keypress", handleLoginKeyPress);
	elem = $("opcologinusername");
    if (elem != null && elem.addEvent != undefined) elem.addEvent("keypress", handleLoginKeyPress);
	
	hideNonDefaultShipping();
	usernameMimicEmail();
	hideShipping();
	hidePaymentSection_div();
	
	if (document.getElementById("calculateshippingbutton") != null) {
		calculateshipping(false, true);
	}
}

function usernameMimicEmail() {
	var emailField = $('email_field');
	var usernameField = $('username_field');
	var usernameisemailDiv = $('usernameisemail_div');

	if (emailField != null && usernameField != null && usernameisemailDiv != null) {
	    $('username_div').disabled = true;
	    usernameField.disabled = true;
	    // TODO: Need a param to hide username
	    //new Fx.Slide('username_div').hide()
	    //new Fx.Slide('username_input').hide()
	    emailField.onchange = usernameCopyEmail;
	    emailField.onkeyup = usernameCopyEmail;
	    emailField.onblur = usernameCopyEmail;
	}
}

function usernameCopyEmail() {
	var emailField = document.getElementById('email_field');
	var usernameField = document.getElementById('username_field');
	usernameField.value = emailField.value;
}


function hideShipping() {
	if(document.getElementById('opco_hide_shipping_address') != null) {
		document.getElementById('shipping_section').style.visibility = 'hidden';
		document.getElementById('shipping_section').style.display = 'none';
	}
	if(document.getElementById('opco_hide_shipping_instructions') != null) {
		document.getElementById('ship_instr').style.visibility = 'hidden';
		document.getElementById('ship_instr').style.display = 'none';
	}
}


function hidePaymentSection_div() {
	if(document.getElementById('opco_hide_payment_section') != null) {
		document.getElementById('payment').style.visibility = 'hidden';
		document.getElementById('payment').style.display = 'none';
	}
}


function hideNonDefaultShipping() {
	var hiddingSwitch = document.getElementById('hide_non_default_shipping_div');
	var shippingTypeId = document.getElementById('shippingtype');

	if (hiddingSwitch != null)
	{
		var shippingRateId = document.getElementsByName('shipping_rate_id');

		for (var j = 0; j < shippingRateId.length; j++)
		{
		    if (!shippingRateId[j].checked)
		    {
		        // if this item is in a table, then hide the whole row
		        if (shippingRateId[j].parentNode.parentNode.nodeName.toLowerCase() == 'tr') 
		        {
		            shippingRateId[j].parentNode.parentNode.style.visibility = 'hidden';
		            shippingRateId[j].parentNode.parentNode.style.display = 'none';
		        }                                                         
		        else
		        {
		            shippingRateId[j].style.visibility = 'hidden';
		            shippingRateId[j].style.display = 'none';

		            var labels = shippingTypeId.getElementsByTagName('label');
		            for (i = 0; i < labels.length; i++)
		            {
		                if (labels[i].htmlFor == shippingRateId[j].id)
		                {
		                    labels[i].style.visibility = 'hidden';
		                    labels[i].style.display = 'none';
		                }
		            }
		        }
			}
        }

        removeBRTags(hiddingSwitch.parentNode.id);
	}
}




function fixUsername() {
	var myUsername = document.getElementById('username_field');
	if(myUsername != null) {
		if(myUsername.value == '' && document.getElementById('email_field') != null) {
			myUsername.value = document.getElementById('email_field').value;
		}
	}
}

function mustselectpaymentmethod() {
	var payment_value = -1;

	var payments = document.getElementsByName("payment_method_id");

	for(var j=0; j < payments.length; j++) {
		if(payments[j].checked) {
			payment_value = j;
		}
	}


	if(payments.length > 0 && 
			document.getElementById('paymentmethodsection').style.visibility != 'hidden' &&
			payment_value == -1) {
		return false;
	} else if(payments.length > 0 && document.getElementById('paymentmethodsection').style.visibility == 'hidden' && payment_value == -1) {
		payments[0].checked = true;
	}


	return true;

}


function updatetotals() {
	var bAjaxMode=bAMode;
//	bAjaxMode = false;
	
	var shipping_value = 0;
	var radios = document.getElementsByName("shipping_rate_id");

//	var radios = document.adminForm.shipping_rate_id;
	for(var i=0; i< radios.length; i++) {
		if(radios[i].checked) {
			shipping_value= i;
		}
	}


	var payment_value = 0;

	var payments = document.getElementsByName("payment_method_id");

	for(var j=0; j < payments.length; j++) {
		if(payments[j].checked) {
			payment_value = j;
		}
	}



	var poststr = "";
	poststr = poststr + "Div_ID=checkoutTotals";
	if(shippingEnabled() && radios.length > 0) {
		poststr = poststr + "&shipping_rate_id=" + encodeURIComponent(radios[shipping_value].value);
//		alert(radios[shipping_value].value);
	} else { 
		/*alert('No shipping ID found'); */ 
	}

	if(shippingEnabled()) {
		var ship_to_info_id_elm = document.getElementsByName('ship_to_info_id');

		var ship_to_value = getCheckedValue(ship_to_info_id_elm);

		poststr = poststr + "&" + "ship_to_info_id" + "=" + encodeURIComponent(ship_to_value);
		

	}
	if(document.getElementById("coupon_code") != null) {
		poststr = poststr + "&coupon_code=" + encodeURIComponent(document.getElementById("coupon_code").value);
	}
	
	poststr = poststr + "&payment_method_id=" + encodeURIComponent(payments[payment_value].value);
	poststr = poststr + "&opco_title=" + encodeURIComponent(getOpcoModuleName());

	makePOSTRequest('modules/mod_virtuemart_onepagecheckout/checkoutTotals_ajax.php', poststr, bAjaxMode);
	

}


function displaycoupon() {
	var bAjaxMode=bAMode;
//	bAjaxMode = false;
	
	var poststr = "";
	poststr = poststr + "Div_ID=couponsection";
	poststr = poststr + "&opco_title=" + encodeURIComponent(getOpcoModuleName());

	makePOSTRequest('modules/mod_virtuemart_onepagecheckout/coupon_ajax.php', poststr, bAjaxMode);

	updatetotals();

}

function setInnerHtmlbyForHtml(sTagName, sHTMLFor, sHTML) {
	var labels=document.getElementsByTagName(sTagName);
	for( i=0; i<labels.length;i++ ){
		if(labels[i].htmlFor==sHTMLFor){
			labels[i].innerHTML = sHTML;
		}
	}
}

var opcoRequiredRegistrationFields;
var opcoRequiredShippingFields;

// clear missing field flag, if already set
// this way it is more reliable than using the other code
function clearMissingClass(section) {
    var toClear = $$("div#" + section + " .missing");
    if (toClear != null && toClear.length > 0) {
        for (i = 0; i < toClear.length; i++) {
            toClear[i].removeClass("missing");
        }
    }
}

function validateAccountFields(silent) {
	if (!silent) clearMissingClass("registersection");
	var rv = validateOpcoForm("registersection", opcoRequiredRegistrationFields, silent);
	if (silent && rv) clearMissingClass("registersection");
	return rv;
}

function validateShippingFields(silent) {
	if (!silent) clearMissingClass("newshippingaddress");
	var rv = validateOpcoForm("newshippingaddress", opcoRequiredShippingFields, silent);
	if (silent && rv) clearMissingClass("newshippingaddress");
	return rv;
}

function createAccount() {
	var bAjaxMode=bAMode;
//	bAjaxMode = false;

	var poststr = "";
	poststr = poststr + "Div_ID=registersection";



	var registerSectionDiv = document.getElementById('registersection');

	var elms = registerSectionDiv.getElementsByTagName("*");




	for(var i=0, maxI = elms.length; i < maxI; i++) {
		var elm = elms[i];
		switch(elm.type) {
			case "radio":
			case "checkbox":
			if(!elm.checked) continue;
			case "text":
			case "textarea":
			case "button":
			case "reset":
			case "submit":
			case "file":
			case "hidden":
			case "password":
			case "image":
			case "select-one":
			case "select-multiple":
			poststr = poststr + "&" + elm.name + "=" + encodeURIComponent(elm.value);
		}
	}
			

	if(shippingEnabled() == true) {

		var ship_to_info_id_elm = document.getElementsByName('ship_to_info_id');

		var ship_to_id = getCheckedId(ship_to_info_id_elm);
		var ship_to_value = getCheckedValue(ship_to_info_id_elm);

		if(ship_to_id == "NEWSHIPADDRESS") {

			var newAddressDiv = document.getElementById('newshippingaddress');

			var elms2 = newAddressDiv.getElementsByTagName("*");

			for(var i=0, maxI = elms2.length; i < maxI; i++) {
				var elm = elms2[i];
				switch(elm.type) {
					case "radio":
					case "checkbox":
					if(!elm.checked) continue;
					case "text":
					case "textarea":
					case "button":
					case "reset":
					case "submit":
					case "file":
					case "hidden":
					case "password":
					case "image":
					case "select-one":
					case "select-multiple":
					poststr = poststr + "&" + elm.name + "_shipto" + "=" + encodeURIComponent(elm.value);
				}
			}
		}
	} 


	poststr = poststr + "&" + "ship_to_info_id" + "=" + encodeURIComponent(ship_to_value);



	poststr = poststr + "&agreed=" + document.getElementsByName("agreed")[0].value;
	poststr = poststr + "&opco_title=" + encodeURIComponent(getOpcoModuleName());
	
	makePOSTRequest('modules/mod_virtuemart_onepagecheckout/createaccount_ajax.php', poststr, bAjaxMode);


}


function enableProcessOrder($resetErrorDiv) {
    if (requireShippingMethod) {
        opcoHelper.enableElement($('formSubmitButton'), false);
        
        // determine if shipping method was set
        // if set, then can enable submit
        var shipRates = document.getElementsByName('shipping_rate_id');

        for (var j = 0; j < shipRates.length; j++) { 
            if (shipRates[j].checked || (shipRates[j].value.indexOf("free_shipping") == 0)) {
                opcoHelper.enableElement($('formSubmitButton'), true);
                if ($resetErrorDiv)
                {
                	document.getElementById("checkoutProcessingError").innerHTML = "";
                }
                break;
            }
        }
    }
    else {
        opcoHelper.enableElement($('formSubmitButton'), true);
        if ($resetErrorDiv)
        {
        	document.getElementById("checkoutProcessingError").innerHTML = "";
        }
    }
}

function disableProcessOrder() {
    opcoHelper.enableElement($('formSubmitButton'), false);
}

function validateAll(silent) {
	var valid = validateAccountFields(silent);

	if (shippingEnabled() == true) {
		var ship_to_info_id = document.getElementsByName('ship_to_info_id');
		var ship_to_id = ship_to_info_id ? getCheckedId(ship_to_info_id) : null;
		
		if (ship_to_id == "NEWSHIPADDRESS") {
			valid = validateShippingFields(silent) && valid;
		}
	}
	
	if (!valid) {
        if (!silent) alert("Please make sure the form is complete and valid.");
        return false;
	}

	return true;
}

function calculateshipping(enforceValidation, forceUpdateWhenNotValid) {
	var bAjaxMode=bAMode;
	var validInfo = true;






	selected = function(cal,mdate) { cal.sel.value = mdate; 
		var slotsToSkip = document.getElementById('slots_to_skip').value;
		var deliveryDate = document.getElementById('delivery_date').value;
		var currentDate = document.getElementById('current_date').value;

		var dt = new Date();
		var currentHour = dt.getHours();


		if(deliveryDate == currentDate)
		{
			var cutOffTime = (currentHour*1) + (slotsToSkip*1);

			var deliveryTime = document.getElementById('delivery_time');

			var goThrough = 0;


			var currentTimeSlot;
			var currentSlot = 0;
			for(goThrough=0; currentSlot < cutOffTime && deliveryTime.options[goThrough]; goThrough++) {
				currentTimeSlot = deliveryTime.options[goThrough].value.split(escape('|'));
				currentSlot = (currentTimeSlot[0].split(escape('_'))[1]*1) ;

				deliveryTime.options[goThrough].disabled = true;

			}

		}
			
		
		customShippingDateUpdated(); closeHandler(cal);};





var dateFormat = '%m-%d-%Y';

var cal = new Calendar(true, null, selected, closeHandler);
calendar = cal; // remember the calendar in the global
cal.setRange(1900, 2070); // min/max year allowed
if ( dateFormat ) // optional date format
{
cal.setDateFormat(dateFormat);
}
calendar.create(); // create a popup calendar

/*calendar.setDateStatusHandler(

    function(dt)
    {

var today = new Date();
if (dt <= today) {
  return true; // true says "disabled"
} else {
  return false; // leave other dates "enabled"
}



    });
*/





	if (enforceValidation) {
		if (!validateAll(false)) return false;
	}
	else {
		validInfo = validateAll(true);
		
		// if calculate shipping button is available and if not valid, 
		// then can not proceed
		if (!validInfo && document.getElementById("calculateshippingbutton") != null) {
			
			// show instructions again and exit
		    opcoHelper.showById('calcshippinginstruction', '');
		    opcoHelper.emptyById('calcshippingerror');

		    // HOWEVER, only reset if not already reset
			// if resetting must recalculate totals
			// because shipping method affects total costs!
			var shouldUpdateTotals = (document.getElementsByName("shipping_rate_id").length > 0 
										|| forceUpdateWhenNotValid);
			
			opcoHelper.emptyById('shippingtype');

		    // because shipping was reset without recalculating
		    // must update totals
		    if (shouldUpdateTotals) updatetotals();

			return false;
		}
		else if (!validInfo) {
		    // reset shipping method area
		    opcoHelper.showById('calcshippinginstruction', '');
		    opcoHelper.emptyById('shippingtype');
		    opcoHelper.emptyById('calcshippingerror');
		}
	}
	
	if (shippingEnabled() == false && document.getElementById("calcshippingerror") != null) {
		document.getElementById("calcshippingerror").innerHTML = "Shipping not enabled";
    }
    
	var poststr = "";
	poststr = poststr + "Div_ID=calcshippingerror";

	var shipping_value = 0;

	var ship_to_info_id = document.getElementsByName('ship_to_info_id');
	
	var ship_to_id = getCheckedId(ship_to_info_id);
	var ship_to_value = getCheckedValue(ship_to_info_id);

	if (validInfo) {
		var registerSectionDiv = document.getElementById('registersection');
	
		var elms = registerSectionDiv.getElementsByTagName("*");
	
		for(var i=0, maxI = elms.length; i < maxI; i++) {
			var elm = elms[i];
			switch(elm.type) {
				case "radio":
				case "checkbox":
				if(!elm.checked) continue;
				case "text":
				case "textarea":
				case "button":
				case "reset":
				case "submit":
				case "file":
				case "hidden":
				case "password":
				case "image":
				case "select-one":
				case "select-multiple":
				poststr = poststr + "&" + elm.name + "=" + encodeURIComponent(elm.value);
			}
		}
	
		if(ship_to_id == "NEWSHIPADDRESS") {
	
			var newAddressDiv = document.getElementById('newshippingaddress');
	
			var elms2 = newAddressDiv.getElementsByTagName("*");
	
			for(var i=0, maxI = elms2.length; i < maxI; i++) {
				var elm = elms2[i];
				switch(elm.type) {
					case "radio":
					case "checkbox":
					if(!elm.checked) continue;
					case "text":
					case "textarea":
					case "button":
					case "reset":
					case "submit":
					case "file":
					case "hidden":
					case "password":
					case "image":
					case "select-one":
					case "select-multiple":
					poststr = poststr + "&" + elm.name + "_shipto" + "=" + encodeURIComponent(elm.value);
				}
			}
	
		}
	}

	poststr = poststr + "&" + "ship_to_info_id" + "=" + encodeURIComponent(ship_to_value);

	
	var radios = document.getElementsByName("shipping_rate_id");

	for(var i=0; i< radios.length; i++) {
		if(radios[i].checked) {
			shipping_value= i;
		}
	}
	
	if(radios.length > 0) {
		poststr = poststr + "&shipping_rate_id=" + encodeURIComponent(radios[shipping_value].value);
	} 

	poststr = poststr + "&agreed=" + document.getElementsByName("agreed")[0].value;

	if (!enforceValidation && !validInfo) {
		poststr = poststr + "&ignoreAccountUpdate=1";
	}

    opcoHelper.hideById('calcshippinginstruction');
    
	poststr = poststr + "&opco_title=" + encodeURIComponent(getOpcoModuleName());

	makePOSTRequest('modules/mod_virtuemart_onepagecheckout/calculateshipping_ajax.php', poststr, bAjaxMode);
}


function setShipToInfoWithoutSubmit() {


	var deliveryCalendar = document.getElementById("delivery_calendar");



	var newShipInfoElem = document.getElementById("new_ship_info");
	if (newShipInfoElem != null) {
	    var shipToInfo = newShipInfoElem.value;
		
		var radios = document.getElementsByName("ship_to_info_id");
	
		var found = false;
		for(var i=0; i< radios.length; i++) {
			if(radios[i].checked) {
			    radios[i].value = shipToInfo;
				found = true;
				break;
			}
		}
		if (!found) {
			var shipToInfoElem = document.getElementById("ship_to_info_id");
			if (shipToInfoElem != null) shipToInfoElem.value = shipToInfo;
		}
	}
	
	var shipRates = document.getElementsByName("shipping_rate_id");
	if (shipRates.length > 0) {
	    for (var i = 0; i < shipRates.length; i++) {
	        shipRates[i].onclick = updatetotals;
	    }
	    opcoHelper.hideById('calcshippinginstruction');
	}
	else {
	    opcoHelper.showById('calcshippinginstruction', '');
	}

	hideNonDefaultShipping();
	updatetotals();
	enableProcessOrder(true);


	if(deliveryCalendar != null) {
		eval(deliveryCalendar.onclick);
	}


}


function setPleaseWaitText() 
{
    // disable these controls
    opcoHelper.enableElement($('loginbutton'), false);
    opcoHelper.enableElement($('calculateshippingbutton'), false);
    opcoHelper.enableElement($('updatetotalsbutton'), false);
    opcoHelper.enableElement($('calculatetax_button'), false);
    opcoHelper.enableElement($('formSubmitButton'), false);

    // show correspondign progress notifications
    var progressElems = $$('.ajaxprogress');
    for (i = 0; i < progressElems.length; i++) {
        opcoHelper.showElement(progressElems[i], 'inline');
    }
}

function unsetPleaseWaitText() {

    // disable these controls
    opcoHelper.enableElement($('loginbutton'), true);
    opcoHelper.enableElement($('calculateshippingbutton'), true);
    opcoHelper.enableElement($('updatetotalsbutton'), true);
    opcoHelper.enableElement($('calculatetax_button'), true);

    enableProcessOrder(false);
    // opcoHelper.enableElement($('formSubmitButton'), true);

    // show correspondign progress notifications
    var progressElems = $$('.ajaxprogress');
    for (i = 0; i < progressElems.length; i++) {
        opcoHelper.hideElement(progressElems[i]);
    }
}


function setButtonText(button_name, new_button_text) {
	var button_text1 = document.getElementById(button_name);
	if(button_text1 != null) {
		button_text1.value = new_button_text;
	}	

}



function saveButtonText(button_name) {
	var button_text1 = document.getElementById(button_name);
	var button_text2 = document.getElementById(button_name+'_tmp_text')	
	if(button_text1 != null && button_text2 != null) {
		button_text2.value = button_text1.value;
	}	

}

function restoreButtonText(button_name) {
	var button_text1 = document.getElementById(button_name);
	var button_text2 = document.getElementById(button_name+'_tmp_text')	
	if(button_text1 != null && button_text2 != null) {
		button_text1.value = button_text2.value;
	}	

}






function validatePassword() {

	var password1 = document.getElementById('password_field');
	var password2 = document.getElementById('password2_field');


	if(password1 != null && password2 != null) {

		if(password1.value != password2.value) {
			alert("Passwords do not match");
		}
	}
}

function setShipToInfo() {

	if(mustselectpaymentmethod() == false) {
		alert("You must select a payment method");
		return;
	}

	var newShipInfoElem = document.getElementById("new_ship_info");
	
	if (newShipInfoElem != null) {
		var shipToInfo = newShipInfoElem.value;
	
		var radios = document.getElementsByName("ship_to_info_id");

		var found = false;
		for(var i=0; i< radios.length; i++) {
			if(radios[i].checked) {
				radios[i].value = shipToInfo;
				found = true;
				break;
			}
		}
		if (!found) {
			var shipToInfoElem = document.getElementById("ship_to_info_id");
			if (shipToInfoElem != null) shipToInfoElem.value = shipToInfo;
		}
	}
	
	var my_hidden_shipping_rate_id = document.getElementById("hidden_shipping_rate_id");
	var shipping_value = 0;
	var my_shipping_rate = document.getElementsByName("shipping_rate_id");

//	var radios = document.adminForm.shipping_rate_id;
	for(var j=0; j< my_shipping_rate.length; j++) {
		if(my_shipping_rate[j].checked) {
			shipping_value= j;
		}
	}

	if(j > 0 && my_hidden_shipping_rate_id != null) {
		my_hidden_shipping_rate_id.name = "shipping_rate_id";
		my_hidden_shipping_rate_id.id = my_shipping_rate[shipping_value].id;
		my_hidden_shipping_rate_id.value = my_shipping_rate[shipping_value].value;

		//alert(my_shipping_rate[shipping_value].value);
	}

	// if admin form is missing ship_to_info_id
	var shipToInfoMissing = false;
	if (document.adminForm.ship_to_info_id == null)
	{
		shipToInfoMissing = true;
	}
	// if multiple ship to values on the form
	else if (document.adminForm.ship_to_info_id.length != undefined)
	{
		// check if the ship to that DOESN'T belong to the adminForm is selected
		var checkedShipToFound = false;
		for(var i=0; i< document.adminForm.ship_to_info_id.length; i++) {
			if (document.adminForm.ship_to_info_id[i].checked) { 
				checkedShipToFound = true;
			}
		}
		shipToInfoMissing = !checkedShipToFound;
	}
	// if just one ship to value on the form
	else if (!document.adminForm.ship_to_info_id.checked)
	{
		shipToInfoMissing = true;
	}
	
	if (shipToInfoMissing) 
	{
		// rename hiddent_ship_to_info_id
		var my_hidden_ship_to_info_id = document.getElementById("hidden_ship_to_info_id");
		my_hidden_ship_to_info_id.name = "ship_to_info_id";
		my_hidden_ship_to_info_id.value = shipToInfo;
	}

	var payment_value = -1;

	var payments = document.getElementsByName("payment_method_id");

	for(var j=0; j < payments.length; j++) {
		if(payments[j].checked) {
			payment_value = payments[j].value;
		}
	}

	if(payment_value != -1) {
		var hidden_payment = document.getElementById('hidden_payment_method');
		hidden_payment.name = "payment_method_id";
		hidden_payment.value = payment_value;
	}


	
	setPleaseWaitText();
	document.adminForm.submit();
}




function submit_allinone() {
	if (!validateAll(false)) return false;

	createAccount();

	return false;
}



function couponsubmit() {
	var bAjaxMode=bAMode;
//	bAjaxMode = false;

	
	var poststr = "";
	poststr = poststr + "Div_ID=couponsection";
/*	poststr = poststr + "&coupon_code=" + encodeURIComponent(document.getElementById("coupon_code").value);
	poststr = poststr + "&opco_title=" + encodeURIComponent(getOpcoModuleName());
*/
	var shipping_value = 0;
	var radios = document.getElementsByName("shipping_rate_id");

//	var radios = document.adminForm.shipping_rate_id;
	for(var i=0; i< radios.length; i++) {
		if(radios[i].checked) {
			shipping_value= i;
		}
	}


	var payment_value = 0;

	var payments = document.getElementsByName("payment_method_id");

	for(var j=0; j < payments.length; j++) {
		if(payments[j].checked) {
			payment_value = j;
		}
	}



	var poststr = "";
	poststr = poststr + "Div_ID=checkoutTotals";
	if(shippingEnabled() && radios.length > 0) {
		poststr = poststr + "&shipping_rate_id=" + encodeURIComponent(radios[shipping_value].value);
//		alert(radios[shipping_value].value);
	} else { 
		/*alert('No shipping ID found'); */ 
	}

	if(shippingEnabled()) {
		var ship_to_info_id_elm = document.getElementsByName('ship_to_info_id');

		var ship_to_value = getCheckedValue(ship_to_info_id_elm);

		poststr = poststr + "&" + "ship_to_info_id" + "=" + encodeURIComponent(ship_to_value);
		

	}
	if(document.getElementById("coupon_code") != null) {
		poststr = poststr + "&coupon_code=" + encodeURIComponent(document.getElementById("coupon_code").value);
	}
	
	poststr = poststr + "&payment_method_id=" + encodeURIComponent(payments[payment_value].value);
	poststr = poststr + "&opco_title=" + encodeURIComponent(getOpcoModuleName());


	makePOSTRequest('modules/mod_virtuemart_onepagecheckout/coupon_process_ajax.php', poststr, bAjaxMode);

}

function checkCouponField(form) {
	if(form.coupon_code.value == '') {
		new Effect.Highlight('coupon_code');
		return false;
	}
	return true;
}

var changingShippingStatus = false;
function shippingstatusNew() {
	if (changingShippingStatus) return;
	
	changingShippingStatus = true;
	
	// deselect all other shipping elements
	var ship_to_info_ids = document.getElementsByName('ship_to_info_id');
	for (var i = 0; i < ship_to_info_ids.length; i++) {
		if (ship_to_info_ids[i].id != "NEWSHIPADDRESS") {
			ship_to_info_ids[i].checked = false;
		}
	}
	showNewAddress();
	
	changingShippingStatus = false;

	shippingstatusCommon();
}

function shippingstatusExisting() {
	if (changingShippingStatus) return;
	
	changingShippingStatus = true;
	
	// deselect new shipping address
	document.getElementById("NEWSHIPADDRESS").checked = false;
	hideNewAddress();
	
	changingShippingStatus = false;

	shippingstatusCommon();
}

function shippingstatusCommon() {
    calculateshipping(false, false);
}	


function getCheckedValue(radioObj) {
	if(!radioObj)
		return "";
	var radioLength = radioObj.length;
	if(radioLength == undefined)
		if(radioObj.checked)
			return radioObj.value;
		else
			return "";
	for(var i = 0; i < radioLength; i++) {
		if(radioObj[i].checked) {
			return radioObj[i].value;
		}
	}
	return "";
}

function getCheckedId(radioObj) {
	if(!radioObj)
		return "";
	var radioLength = radioObj.length;
	if(radioLength == undefined)
		if(radioObj.checked)
			return radioObj.id;
		else
			return "";
	for(var i = 0; i < radioLength; i++) {
		if(radioObj[i].checked) {
			return radioObj[i].id;
		}
	}
	return "";
}

/*--------------------------
	 Ajax Code
--------------------------*/
function getFullJoomlaUrl(url)
{
	var joomla_root = document.getElementById('joomla_root');
	return joomla_root.value + url;
}

var http_request;// = false;
function makePOSTRequest(url, parameters, bAjaxMode) {

	url = getFullJoomlaUrl(url);
	//alert(parameters);

	if (bAjaxMode == true) {
	//alert(bAjaxMode );
	//	ShowScreenLoading(true);	//Show the loading Screen
	setPleaseWaitText();
		http_request = false;
		if (window.XMLHttpRequest) { // Mozilla, Safari,...
			http_request = new XMLHttpRequest();
			if (http_request.overrideMimeType) {
				// set type accordingly to anticipated content type
				//http_request.overrideMimeType('text/xml');
				http_request.overrideMimeType('text/html');
			}
		}
		else if (window.ActiveXObject)
		{ // IE
			try {
				http_request = new ActiveXObject("Msxml2.XMLHTTP");
				alert('Msxml2.XMLHTTP');
			}
			catch (e) 
			{
				try {
					http_request = new ActiveXObject("Microsoft.XMLHTTP");
					alert('Microsoft.XMLHTTP');
				} catch (e) {}
			}
		}

		if (!http_request) {
			alert('Cannot create XMLHTTP instance');
			return false;
		}

		//alert("ajax post");
		http_request.onreadystatechange = onePageSetContents;
		//alert(url);
		http_request.open('POST', url, true);
		http_request.setRequestHeader("Content-type", "application/x-www-form-urlencoded; charset=ISO-8859-1");
		//http_request.setRequestHeader("Content-type", "multipart/form-data");
//		http_request.setRequestHeader("Content-length", parameters.length);
//		http_request.setRequestHeader("Connection", "close");
		//alert("bAjaxMode=" + bAjaxMode + "&" + parameters);
		http_request.send("bAjaxMode=" + bAjaxMode + "&" + parameters);
	}
	else
	{
		document.adminForm.action = url + "?bAjaxMode=" + bAjaxMode + "&" + parameters.substring(0,10000)
	//	alert("classic post.  Action: "+ document.adminForm.action);
		document.adminForm.submit();
	}
}


function onePageSetContents() {
    if (http_request.readyState == 4) {
	    if (http_request.status == 200) {

//			ShowScreenLoading(false);	//Hide the loading Screen
			//alert(http_request.responseText.length);
			result = http_request.responseText;
			debug_opco_msg("response="+http_request.responseText)
			
			var crlf = "<br>";
			var sBlock=http_request.responseText.split(crlf);


			for (var blocksIndex = 0; blocksIndex < sBlock.length; blocksIndex++) {
				debug_opco_msg('processing ' + (blocksIndex + 1) + ' of ' + sBlock.length + ': ' + sBlock[blocksIndex]);
				ThisBlock = sBlock[blocksIndex];
				ThisTag = ThisBlock.slice(ThisBlock.indexOf("<"),ThisBlock.indexOf(">"))
				
//alert('ID = ' + ThisTag);
//alert('Index = ' + ThisTag.toLowerCase().indexOf("<div "));

				if (ThisTag.toLowerCase().indexOf("<div ") == 0) {
					//Div Tag so replace object on page
					
					//Get the ID of the Div Tag
					aID = ThisTag.split("=")
					ID = aID[1]

					if(ID == null)
						continue;
//alert('ID = ' + ID);
					
					//Get the Content of the Div Tag
					EndTag = '</div>'
					Contents = ThisBlock.slice(ThisBlock.indexOf(">") + 1, ThisBlock.length)
					Contents = Contents.substring(0,Contents.length - EndTag.length);

					//Replace the Object contents on the document
					debug_opco_msg(ID + "\n" + Contents)
	
					//$(ID).set('html', Contents);
					setHtmlAndExecute(ID, Contents);
					
					//document.getElementById(ID).innerHTML = Contents;
					
/*					var ob = document.getElementById(ID).getElementsByTagName("script");
					for(var s=0; s < ob.length; s++){
						if(ob[s].innerHTML!=null) eval(ob[s].innerHTML);
					}					
*/

				} else if (ThisTag.trim().toLowerCase().indexOf("<scr" + "ipt ") == 0) {
					//script so execute the script
					
					//Get the Script Content of the Script Tag
					EndTag = '</scr' + 'ipt>'
					
					Script = ThisBlock.slice(ThisBlock.indexOf(">") + 1, ThisBlock.length)
					Script = Script.substring(0,Script.lastIndexOf("<"));

					//Execute the script
					debug_opco_msg('executing script: ' + Script);
					eval(Script);
				}
				
			}
			
			

		} else {
			alert('There was a problem with the request.  Error Code: ' + http_request.status);
			debug_opco_msg('error response: ' + http_request.responseText);
//			ShowScreenLoading(false);	//Hide the loading Screen
		}
		
		unsetPleaseWaitText();
    }
}

/*--------------------------
	 / Ajax Code
--------------------------*/	

function opChangeDynaList( listname, source, key, orig_key, orig_val ) {
	var s_i = document.getElementById('newshippingaddress');
	if(s_i == null) {
		return;
	}

	var elms = s_i.getElementsByTagName("select");
	var s_i_tmp = null;
	for(var i=0, maxI = elms.length; i < maxI; i++) {
		var elm = elms[i];
		if(elm.name == listname) {
			s_i_tmp = elm;


		}
	}

	if(s_i_tmp != null) {
		s_i = s_i_tmp;
	} else {
		return;
	}
	var list = s_i;

//	var list = eval( 'document.adminForm.' + listname );

	// empty the list
	for (i in list.options.length) {
		list.options[i] = null;
	}
	i = 0;
	for (x in source) {
		if (source[x][0] == key) {
			opt = new Option();
			opt.value = source[x][1];
			opt.text = source[x][2];

			if ((orig_key == key && orig_val == opt.value) || i == 0) {
				opt.selected = true;
			}
			list.options[i++] = opt;
		}
	}
	list.length = i;
}




function loginsubmit() {

	var bAjaxMode=bAMode;
//	bAjaxMode = false;
	
	var poststr = "";
	poststr = poststr + "Div_ID=registersection";
	poststr = poststr + "&username=" + encodeURIComponent(document.getElementById("opcologinusername").value);
	poststr = poststr + "&passwd=" + encodeURIComponent(document.getElementById("opcologinpassword").value);
	poststr = poststr + "&" + encodeURIComponent(document.getElementById("logintoken").name) + "=1";
	poststr = poststr + "&return=" + document.getElementById("return").value;
	poststr = poststr + "&remember=" + document.getElementById("remember").value;
	poststr = poststr + "&op2=" + document.getElementById("op2").value;
	poststr = poststr + "&opco_title=" + encodeURIComponent(getOpcoModuleName());

	opcoHelper.emptyById('loginerror');
    
	makePOSTRequest('index.php?option=com_user&task=login', poststr, bAjaxMode);
}


function logoutsubmit() {
	var bAjaxMode=bAMode;
//	bAjaxMode = false;

	var poststr = "";
	poststr = poststr + "Div_ID=loginsection";
	poststr = poststr + "&option=" + encodeURIComponent(document.getElementsByName("option")[0].value);
	poststr = poststr + "&task=" + encodeURIComponent(document.getElementsByName("task")[0].value);
	poststr = poststr + "&return=" + encodeURIComponent(document.getElementsByName("return")[0].value);
	poststr = poststr + "&opco_title=" + encodeURIComponent(getOpcoModuleName());


	makePOSTRequest(document.adminForm, poststr, bAjaxMode);

}












function ShowDarkFilm(bState) {


	var x,y;
	if (self.innerHeight) // all except Explorer
	{
		x = self.innerWidth;
		y = self.innerHeight;
	}
	else if (document.documentElement && document.documentElement.clientHeight)
		// Explorer 6 Strict Mode
	{
		x = document.documentElement.clientWidth;
		y = document.documentElement.clientHeight;
	}
	else if (document.body) // other Explorers
	{
		x = document.body.clientWidth;
		y = document.body.clientHeight;
	}
	
	if (window.innerHeight && window.scrollMaxY) {// Firefox
		yWithScroll = window.innerHeight + window.scrollMaxY;
		xWithScroll = window.innerWidth + window.scrollMaxX;
	}
	else if (document.body.scrollHeight > document.body.offsetHeight){ // all but Explorer Mac
		yWithScroll = document.body.scrollHeight;
		xWithScroll = document.body.scrollWidth;
	} else { // works in Explorer 6 Strict, Mozilla (not FF) and Safari
		yWithScroll = document.body.offsetHeight;
		xWithScroll = document.body.offsetWidth;
	} 



	if (bState == true) {
		document.getElementById("ifmClientDarkFilm").style.visibility = "visible";
		document.getElementById("panClientFilm").style.visibility = "visible";
		//document.getElementById("ifmClientDarkFilm").style.height = y * 2 + "px";
		document.getElementById("ifmClientDarkFilm").style.height = yWithScroll;
		document.getElementById("ifmClientDarkFilm").style.width = x;
		document.getElementById("panClientFilm").style.height = yWithScroll;
		document.getElementById("panClientFilm").style.width = x;
	}
	else {
		document.getElementById("ifmClientDarkFilm").style.visibility = "hidden";
		document.getElementById("panClientFilm").style.visibility = "hidden";
	}

}


function ShowScreenLoading(bState) {


	ShowDarkFilm(bState);	//turn on or off darkfilm
	
	if (bState == true) {
		document.getElementById("panLoading").style.visibility = "visible";
		//Set cursor to hourglass
		document.getElementById("panClientFilm").style.cursor = "wait";
		document.getElementById("panLoading").style.cursor = "wait";
	}
	else {
		document.getElementById("panLoading").style.visibility = "hidden";
		//Turn hourglass off
		document.getElementById("panClientFilm").style.cursor = "default";
		document.getElementById("panLoading").style.cursor = "default";
		DisableRightClick(false);
	}
}

/*
Add the following to you body tag...
onmousedown="DisableRightClick();"
*/

function DisableRightClick(bDisableSwitch)
{
    if(bDisableSwitch==true)
    {
        if(window.event.button==2 ||window.event.button==3)
        {
            document.oncontextmenu = function(){return false}
            return false;
        }
        else
        {
            return true;
        }
    }
    else
    {
        document.oncontextmenu = function(){return true};
    }
}

function opWriteDynaList(insertAfter, selectParams, source, key, orig_key, orig_val ) {
	var html = '\n <select ' + selectParams + '>';
	var i = 0;
	for (x in source) {
		if (source[x][0] == key) {
			var selected = '';
			if ((orig_key == key && orig_val == source[x][1]) || (i == 0 && orig_key != key)) {
				selected = 'selected="selected"';
			}
			html += '\n <option value="'+source[x][1]+'" '+selected+'>'+source[x][2]+'</option>';
		}
		i++;
	}
//	alert(insertAfter);
//	var div = insertAfter;
//	var myDiv = document.getElementById(insertAfter);
//	var x = myDiv.getElementsByTagName("script");   

	html += '\n </select>';
//	alert(html);
		
	// execute the new script to draw the states dropdown
//	opcoHelper.stringToElement(html).inject(/*x[0]*/this, 'after');
	return html;
}

function setHtmlAndExecute(divId, innerHTML) {
	var div = document.getElementById(divId);
	if (div == null) return;
	
	// fix innerHTML for payment method issues -- need to revisit
	innerHTML = innerHTML.replace(/document\.adminForm\.payment_method_id/g, 'document.getElementsByName(\'payment_method_id\')');
	
	//opcoHelper.emptyElement(div);
	
	div.innerHTML = innerHTML;  
	var x = div.getElementsByTagName("script");   
	
	for( var i=0; i < x.length; i++) {
		var scriptToRun = x[i].text;
		if (scriptToRun.indexOf("<!--") >= 0) {
			scriptToRun = scriptToRun.substring(scriptToRun.indexOf("<!--")+4, scriptToRun.indexOf("//-->")); 
		}

		// hack for state dropdown generation
		if (scriptToRun.indexOf('writeDynaList') >= 0) {
			// fix the script to exclude writeDynaList line
			var replacementText = 'opWriteDynaList('+i+', ';

			scriptToRun = scriptToRun.replace('writeDynaList(', replacementText);

			debug_opco_msg(scriptToRun);
			
 			// run the script
 			var tmp_return = '';

			tmp_return = eval(scriptToRun);

			var elms = div.getElementsByTagName("div");
			var s_i_tmp = null;
			for(var j=0, maxI = elms.length; j < maxI; j++) {
				var elm = elms[j];
				if(elm.id == "state_input") {
					s_i_tmp = elm;


				}
			}

			if(scriptToRun.indexOf('creditcard_code') >= 0) {
				x[i].parentNode.innerHTML += tmp_return;
			}

			if(s_i_tmp != null) {

				s_i_tmp.innerHTML += tmp_return;
			}
		}
		else {
			eval(scriptToRun);
		}
	}  
}  

function presetUserInfo(section, fieldName, value)
{
    var sectionElem = $(section);
    if (sectionElem == null) return;

    var allFormElements = sectionElem.getFormElements();

    var formelement = null;
    for (var j = 0; j < allFormElements.length; j++) {
    	if (allFormElements[j].name == fieldName) {
    		formelement = allFormElements[j];
    		break;
    	}
    }
    
    // try to find [] elements
    if (!formelement) {
        for (var j = 0; j < allFormElements.length; j++) {
        	if (allFormElements[j].name == fieldName + "[]") {
        		formelement = allFormElements[j];
        		break;
        	}
        }
    }
    
    if (!formelement) return;

    if (formelement.type.toLowerCase() == 'radio') {
    	formelement.set('value', value);
    }
    else if (formelement.options) {
    	for (var i = 0; i < formelement.options.length; i++) {
    		if (formelement.options[i].value == value) {
    			formelement.options[i].selected = 'selected';
			}
    	}
    }
    else {
    	if (formelement.value == null || trim(formelement.value).length == 0)
    	{
            formelement.value = value;
    	}
    }
}

//var form = document.adminForm;
//var required_fields = new Array('address_type_name', 'first_name', 'last_name', 'address_1', 'city', 'zip', 'country', 'state', 'phone_1');

function validateOpcoForm(formElementId, required_fields, silent) {
    var r = new RegExp("[\<|\>|\"|'|\%|\;|\(|\)|\&|\+|\-]", "i");
    var sectionElem = $(formElementId);
    var allFormElements = sectionElem.getFormElements();
    var allDivElements = sectionElem.getElements("div");
    var isvalid = true;
    for (var i = 0; i < required_fields.length; i++) {
        var formelement = null;
        for (var j = 0; j < allFormElements.length; j++) {
        	if (allFormElements[j].name == required_fields[i]) {
        		formelement = allFormElements[j];
        		break;
        	}
        }

        if (!formelement) {
            formelement = sectionElem.getElementById(required_fields[i] + "_field0"); 
            var loopIds = true;
        }
        
        // try to find [] elements
        if (!formelement) {
	        for (var j = 0; j < allFormElements.length; j++) {
	        	if (allFormElements[j].name == required_fields[i] + "[]") {
	        		formelement = allFormElements[j];
	        		break;
	        	}
	        }
        }

        if (!formelement) continue;

        // try to find div element
        var divElement = null;
        for (var j = 0; j < allDivElements.length; j++) {
        	if (allDivElements[j].id == required_fields[i] + '_div') {
        		divElement = allDivElements[j];
        		break;
        	}
        }
        
        // if invalid form element, then the whole thing is invalid
        if (!formelement.type) {
            if (silent) return false;
            if (divElement != null) divElement.className += ' missing';
            // this is for self-preservation.  either we couldn't find the right field, i.e. a bug, 
            // or the field didn't render right.  either way, this shouldn't prevent user from submitting.
            // it could force the server to reject the registration/shipping.
            //isvalid = false;  
            continue;
        }
        
        
        if (formelement.type.toLowerCase() == 'radio' || formelement.type.toLowerCase() == 'checkbox') {
            if (loopIds) {
                var rOptions = new Array();
                for (var j = 0; j < 30; j++) {
                    rOptions[j] = sectionElem.getElementById(required_fields[i] + '_field' + j);
                    if (!rOptions[j]) { break; }
                }
            } else {
            	var rOptions = sectionElem.getElement("input[name=" + formelement.getAttribute('name') + "]");
            }
            var rChecked = 0;
            if (rOptions.length > 1) {
                for (var r = 0; r < rOptions.length; r++) {
                    if (!rOptions[r]) { continue; }
                    if (rOptions[r].checked) { rChecked = 1; }
                }
            } else {
                if (formelement.checked) {
                    rChecked = 1;
                }
            }
            if (rChecked == 0) {
            	if (divElement != null) divElement.className += ' missing';
                isvalid = false;
            }
            else if (divElement != null && divElement.className == 'formLabel missing') {
            	divElement.className = 'formLabel';
            }
        }
        else if (formelement.options) {
            if (formelement.options[formelement.options.selectedIndex].value == '') {
            	if (silent) return false;
            	if (divElement != null) divElement.className += ' missing';
                isvalid = false;
            }
            else if (divElement != null && divElement.className == 'formLabel missing') {
            	divElement.className = 'formLabel';
            }
        }
        else {
            if (formelement.value == '') {
                if (silent) return false;
                if (divElement != null) divElement.className += ' missing';
                isvalid = false;
            }
            else if (divElement != null && divElement.className == 'formLabel missing') {
            	if (divElement != null) divElement.className = 'formLabel';
            }
        }
    }

    return isvalid;
}

function opcoHandleCartUpdate(f)
{
	var url = getFullJoomlaUrl('modules/mod_virtuemart_onepagecheckout/cartupdate_ajax.php');
	var prodId = f.product_id;
	var desc = f.description;
	var qty = f.quantity;
	
	// if quantity is 0, then it's a delete
	if (qty.value == '0')
	{
		opcoHandleCartDelete(f);
	}
	else
	{
		// gather form params
		new Ajax(url, {
			method: 'get', 
			data: { 
				'action': 'update', 
				'prod_id' : prodId.value,
				'description' : desc.value,
				'quantity' : qty.value,
				'opco_title' : getOpcoModuleName()
			}, 
			onRequest: function() {
				// we may need to disable controls
			}, 
			onFailure: function(e) {
				alert(e.statusText);
			},
			onComplete: function(response) {
				cartUpdateCompleted(response);
			}
		}).request();
	}
	
	return false;
}

function opcoHandleCartDelete(f)
{
	var url = getFullJoomlaUrl('modules/mod_virtuemart_onepagecheckout/cartupdate_ajax.php');
	var prodId = f.product_id;
	var desc = f.description;
	
	// gather form params
	new Ajax(url, {
		method: 'get', 
		data: { 
			'action': 'delete', 
			'prod_id' : prodId.value,
			'description' : desc.value,
			'opco_title' : getOpcoModuleName()
		}, 
		onRequest: function() {
			// we may need to disable controls
		}, 
		onFailure: function(e) {
			alert(e.statusText);
		},
		onComplete: function(response) {
			cartUpdateCompleted(response);
		}
	}).request();
	
	return false;
}

function cartUpdateCompleted(response)
{
	$$('div#review_cart .vmcartmodule').setHTML(response);

	calculateshipping(false, true);
}

function handleLoginKeyPress(e)
{
	var keycode;
	if(!e) e = window.event;
	key = e.keyCode ? e.keyCode : e.which;
	
	if (key == 13) {
	   loginsubmit();
	   return false;
	}
	else {
		return true;
	}
}

function getOpcoModuleName()
{
	return $('opco_module_title').value;
}



function customShippingDateUpdated() {
		var availableTimeSlots = new Array();
			availableTimeSlots['TIMESLOT_1'] = '12AM to 1AM';
			availableTimeSlots['TIMESLOT_2'] = '1AM to 2AM';
			availableTimeSlots['TIMESLOT_3'] = '2AM to 3AM';
			availableTimeSlots['TIMESLOT_4'] = '3AM to 4AM';
			availableTimeSlots['TIMESLOT_5'] = '4AM to 5AM';
			availableTimeSlots['TIMESLOT_6'] = '5AM to 6AM';
			availableTimeSlots['TIMESLOT_7'] = '6AM to 7AM';
			availableTimeSlots['TIMESLOT_8'] = '7AM to 8AM';
			availableTimeSlots['TIMESLOT_9'] = '8AM to 9AM';
			availableTimeSlots['TIMESLOT_10'] = '9AM to 10AM';
			availableTimeSlots['TIMESLOT_11'] = '10AM to 11AM';
			availableTimeSlots['TIMESLOT_12'] = '11AM to 12PM';
			availableTimeSlots['TIMESLOT_13'] = '12PM to 1PM';
			availableTimeSlots['TIMESLOT_14'] = '1PM to 2PM';
			availableTimeSlots['TIMESLOT_15'] = '2PM to 3PM';
			availableTimeSlots['TIMESLOT_16'] = '3PM to 4PM';
			availableTimeSlots['TIMESLOT_17'] = '4PM to 5PM';
			availableTimeSlots['TIMESLOT_18'] = '5PM to 6PM';
			availableTimeSlots['TIMESLOT_19'] = '6PM to 7PM';
			availableTimeSlots['TIMESLOT_20'] = '7PM to 8PM';
			availableTimeSlots['TIMESLOT_21'] = '8PM to 9PM';
			availableTimeSlots['TIMESLOT_22'] = '9PM to 10PM';
			availableTimeSlots['TIMESLOT_23'] = '10PM to 11PM';
			availableTimeSlots['TIMESLOT_24'] = '11PM to 12AM';



	var deliveryPrice = document.getElementById('shipping_delivery_price');
	var deliveryDate = document.getElementById('delivery_date');
	var deliveryTime = document.getElementById('delivery_time');
	var deliveryZonePrice = document.getElementById('delivery_zone_price').value*1;

	if(deliveryPrice != null && deliveryDate != null && deliveryTime != null && deliveryZonePrice != null) {
		var deliveryTimeArray = deliveryTime.value.split(escape('|'));
		var deliveryTimePrice = deliveryTimeArray[1]*1;


		var shippingTypeId = document.getElementById('shippingtype');


		var my_rate_ids = document.getElementsByName('shipping_rate_id');

		var myRate = -1;
		for(var j = 0;j < my_rate_ids.length; j++) {
			if(my_rate_ids[j].value.split(escape('|'))[0] == 'delivery') {
				var newValue = escape('delivery|delivery|'+deliveryDate.value+':'+availableTimeSlots[deliveryTimeArray[0]]+'|'+(deliveryZonePrice+deliveryTimePrice));
				var newLabel = deliveryDate.value+' ('+availableTimeSlots[deliveryTimeArray[0]]+')  Total: $'+(deliveryZonePrice+deliveryTimePrice);
				myRate = j;
			}

		}
		var labels = shippingTypeId.getElementsByTagName('label');

		if(myRate >= 0) 
		for (i = 0; i < labels.length; i++)
		{
			if (labels[i].htmlFor == my_rate_ids[myRate].id)
			{
				labels[i].style.visibility = 'hidden';
				labels[i].style.display = 'none';
				labels[i].innerHTML = newLabel;
				labels[i].htmlFor = newValue;
				my_rate_ids[myRate].id = newValue;
				my_rate_ids[myRate].value = newValue;
				i = labels.length;

			}
		}		



		updatetotals();


	}


}
