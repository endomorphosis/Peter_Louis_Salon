<?php
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' ); 
/* Joomla! 1.5 One Page Checkout Module for VirtueMart
*
* @version $Id: list_shipping_methods.tpl.php 05-24-2011
 * author: Polished Geek
 * Website: PolishedGeek.com
 * Email: info@PolishedGeek.com
 * Phone: 919-374-2425
 *
 * license http://www.gnu.org/copyleft/gpl.html GNU/GPL
 *
*/


global $PSHOP_SHIPPING_MODULES, $vmLogger, $auth, $weight_total, $ship_to_info_id, $shipping_rate_id, $_SESSION, $total, $tax_total, $shipping_method_id, $page, $cache_id, $_REQUEST;
		
if( empty( $ship_to_info_id )) {
	// Get the Bill to user_info_id
	$database = new ps_DB();
	$database->setQuery( "SELECT user_info_id FROM #__{vm}_user_info WHERE user_id=".$auth['user_id']." AND address_type='BT'" );
	$vars["ship_to_info_id"] = $_REQUEST['ship_to_info_id'] = $database->loadResult();
} else {
	$vars['ship_to_info_id'] = $ship_to_info_id;
}
if(!isset($_REQUEST["page"])) {
	$_REQUEST["page"] = 'checkout.index';
}
$d["shipping_rate_id"] = $shipping_rate_id;
$_REQUEST["shipping_rate_id"] = $shipping_rate_id;
$_SESSION["shipping_rate_id"] = $shipping_rate_id;
$vars["shipping_rate_id"] = $shipping_rate_id;
include_once( PAGEPATH . 'ro_basket.php');
$vars["weight"] = $weight_total;
$vars['zone_qty'] = vmRequest::getInt( 'zone_qty', 0 );
$i = 0;



$total = $_SESSION['onepagetotal'];
$tax_total = $_SESSION['onepagetax_total'];


//print_r($vars);
//echo '<br/>Total:  '.$total.'    Tax Total: '.$tax_total;

$firstShipMethod = true;

foreach( $PSHOP_SHIPPING_MODULES as $shipping_module ) {
    $vmLogger->debug( 'Starting Shipping module: '.$shipping_module );
	
	if( file_exists( CLASSPATH. "shipping/".$shipping_module.".php" )) 
	{
		include_once( CLASSPATH. "shipping/".$shipping_module.".php" );
	}
	
	if ( class_exists( $shipping_module )) 
	{
		if (!$firstShipMethod) { echo "<br /><br />"; }
		$SHIPPING = new $shipping_module();
		$SHIPPING->list_rates( $vars );
    	$firstShipMethod = false;
	}
	
}

?>
