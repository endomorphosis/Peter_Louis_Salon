<?php if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' );
/* Joomla! 1.5 One Page Checkout Module for VirtueMart
*
* @version $Id: minicart.tpl.php 05-24-2011
 * author: Polished Geek
 * Website: PolishedGeek.com
 * Email: info@PolishedGeek.com
 * Phone: 919-374-2425
 *
 * license http://www.gnu.org/copyleft/gpl.html GNU/GPL
 *
*/

if($empty_cart) { ?>
    
    <div style="margin: 0 auto;">
    <?php if(!$vmMinicart) { ?>
        <a href="http://virtuemart.net/" target="_blank">
        <img src="<?php echo (strrpos($mm_action_url, '/') == (strlen($mm_action_url) - 1) ? $mm_action_url : $mm_action_url . '/') ?>components/com_virtuemart/shop_image/ps_image/menu_logo.gif" alt="VirtueMart" width="80" border="0" /></a>
        <br />
    <?php }
    echo $VM_LANG->_('PHPSHOP_EMPTY_CART') ?>
    </div>
<?php } 
else {
    // Loop through each row and build the table
    foreach( $minicart as $cart ) { 		

        echo '<div class="opcoCartProductLine">';
		foreach( $cart as $attr => $val ) {
			// Using this we make all the variables available in the template
			// translated example: $this->set( 'product_name', $product_name );
			$this->set( $attr, $val );
		}
        if(!$vmMinicart) { // Build Minicart
            ?>
            <div style="float: left;">
            <?php echo $cart['quantity'] ?>&nbsp;x&nbsp;<a href="<?php echo $cart['url'] ?>"><?php echo $cart['product_name'] ?></a>
            </div>
            <div style="float: right;">
            <?php echo $cart['price'] ?>
            </div>
            <br style="clear: both;" />
            <?php echo $cart['attributes'];
        }
        echo '<div style="clear:both"></div>';
        echo '</div>';
    }
}
if(!$vmMinicart) { ?>
    <hr style="clear: both;" />
<?php } ?>
<div style="float: left;" >
<?php echo $total_products ?>
</div>
<div style="float: right;">
<?php echo $total_price ?>
</div>
<div style="clear: both;"></div>
<?php if (!$empty_cart && !$vmMinicart && $opcoConfig->reviewCart->showEditCartLink) { ?>
    <br/><br style="clear:both" /><div style="text-align:right;">
    <?php echo $show_cart ?>
    </div><br/>

<?php } 
echo $saved_cart;
?>
