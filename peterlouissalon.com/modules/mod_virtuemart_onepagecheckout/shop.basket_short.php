<?php
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' );
/* Joomla! 1.5 One Page Checkout Module for VirtueMart
*
* @version $Id: shop.basket_short.php 05-24-2011 
 * author: Polished Geek
 * Website: PolishedGeek.com
 * Email: info@PolishedGeek.com
 * Phone: 919-374-2425
 *
 * license http://www.gnu.org/copyleft/gpl.html GNU/GPL
 *
*/
mm_showMyFileName( __FILE__ );

$myPath = dirname(__FILE__).DS;

require_once(CLASSPATH. 'ps_product.php' );
$ps_product = new ps_product;
require_once(CLASSPATH. 'ps_shipping_method.php' );
require_once(CLASSPATH. 'ps_checkout.php' );
$ps_checkout = new ps_checkout;

global $CURRENCY_DISPLAY, $VM_LANG, $vars,$mosConfig_live_site, $sess, $mm_action_url, $_SESSION;

$catid = vmGet($_REQUEST, "category_id", null);
$prodid = vmGet($_REQUEST, "product_id", null);
$page = vmGet($_REQUEST, "page", null);
$flypage = vmGet($_REQUEST, "flypage", null);
$Itemid = vmGet($_REQUEST, "Itemid", null);
$option = vmGet($_REQUEST, "option", null);
$page =vmGet( $_REQUEST, 'page', null );
$tpl = new $GLOBALS['VM_THEMECLASS']();
$cart = $_SESSION['cart'];
$saved_cart = @$_SESSION['savedcart'];
$auth = $_SESSION['auth'];
$empty_cart = false;
$minicart = array();
if ($cart["idx"] == 0) {
	$empty_cart = true;
	$checkout = false;
	$total = 0;
}
else {
	$empty_cart = false;
	$checkout = True;
	$total = $order_taxable = $order_tax = 0;
	$amount = 0;
	$weight_total = 0;
	$html="";

	$_SESSION['vmCartDirection'] = 1;
	// Determiine the cart direction and set vars
	if (@$_SESSION['vmCartDirection']) {
		$i=0;
		$up_limit = $cart["idx"] ;
	}
	else {
		$i=$cart["idx"]-1;
		$up_limit = -1;
	}
	$ci = 0;

$_SESSION['vmMiniCart'] = 0;

	//Start loop through cart
	while ($i != $up_limit)
	{
		//If we are not showing the minicart start the styling of the individual products

		$price = $ps_product->get_adjusted_attribute_price($cart[$i]["product_id"],$cart[$i]["description"]);
		$price["product_price"] = $GLOBALS['CURRENCY']->convert( $price["product_price"], $price["product_currency"] );
		$amount += $cart[$i]["quantity"];
		$product_parent_id=$ps_product->get_field($cart[$i]["product_id"],"product_parent_id");
		if (@$auth["show_price_including_tax"] == 1) {
			$my_taxrate = $ps_product->get_product_taxrate($cart[$i]["product_id"] );
			$price["product_price"] *= ($my_taxrate+1);
		}
		$subtotal = round( $price["product_price"], 2 ) * $cart[$i]["quantity"];
		$total += $subtotal;
		$flypage_id = $product_parent_id;
		if($flypage_id == 0) {
			$flypage_id = $cart[$i]["product_id"];
		}
		$flypage = $ps_product->get_flypage($flypage_id);
		$category_id = vmGet( $cart[$i], 'category_id', 0 );
		if ($product_parent_id) {
			$url = $sess->url(URL . "index.php?page=shop.product_details&flypage=$flypage&product_id=$product_parent_id&category_id=$category_id");
		}
		else {
			$url = $sess->url(URL . "index.php?page=shop.product_details&flypage=$flypage&category_id=$category_id&product_id=" . $_SESSION['cart'][$i]["product_id"]);
		}
		$html = str_replace("_"," ",$ps_product->getDescriptionWithTax( $_SESSION['cart'][$i]["description"], $_SESSION['cart'][$i]["product_id"] ))." ";
		if ($product_parent_id) {
			$db_detail=$ps_product->attribute_sql($cart[$i]["product_id"],$product_parent_id);
			while ($db_detail->next_record()) {
				$html .= $db_detail->f("attribute_value") . " ";
			}
		}

		$minicart[$ci]['product_id'] = $cart[$i]["product_id"];
		$minicart[$ci]['url'] = $url;
		$minicart[$ci]['product_name'] = shopMakeHtmlSafe($ps_product->get_field($_SESSION['cart'][$i]["product_id"], "product_name"));
		$minicart[$ci]['sku'] = (!empty($cart[$i]["sku"])) ? $cart[$i]["sku"] : $ps_product->get_field($_SESSION['cart'][$i]["product_id"], "product_sku");
		$minicart[$ci]['quantity'] = $cart[$i]["quantity"];

		$minicart[$ci]['product_price'] = $CURRENCY_DISPLAY->getFullValue($price["product_price"]);
		$minicart[$ci]['price'] = $CURRENCY_DISPLAY->getFullValue( $subtotal );
		$minicart[$ci]['description'] = $cart[$i]["description"];
		$minicart[$ci]['attributes'] = $html;

        $action_url = 'unknown';

		$minicart[$ci]['update_form'] = '<form method="post" onsubmit="return opcoHandleCartUpdate(this);" style="display: inline;">
		<input type="hidden" name="option" value="com_virtuemart" />
		<input style="width: 64px;margin-left: 5px;text-align: center;" type="text" title="'. $VM_LANG->_('PHPSHOP_CART_UPDATE') .'" class="inputbox" size="4" maxlength="4" name="quantity" value="'.$minicart[$ci]["quantity"].'" />
		<input type="hidden" name="page" value="'. $page .'" />
    <input type="hidden" name="func" value="cartUpdate" />
    <input type="hidden" name="product_id" value="'. $minicart[$ci]['product_id'] .'" />
    <input type="hidden" name="prod_id" value="'. $minicart[$ci]['product_id'] .'" />
    <input type="hidden" name="Itemid" value="'. $sess->getShopItemid() .'" />
    <input type="hidden" name="description" value="'. stripslashes($minicart[$ci]['description']).'" />
    <input type="image" name="update" title="'. $VM_LANG->_('PHPSHOP_CART_UPDATE') .'" src="modules/mod_virtuemart_onepagecheckout/assets/images/update_quantity_cart.png" alt="'. $VM_LANG->_('PHPSHOP_UPDATE') .'" align="middle" onclick="return opcoHandleCartUpdate(this.parentNode);" />
  </form>';
  
		$minicart[$ci]['delete_form'] = '<form method="post" onsubmit="return opcoHandleCartDelete(this);" style="display: inline;">
    <input type="hidden" name="option" value="com_virtuemart" />
    <input type="hidden" name="page" value="'. $page .'" />
    <input type="hidden" name="Itemid" value="'. $sess->getShopItemid() .'" />
    <input type="hidden" name="func" value="cartDelete" />
    <input type="hidden" name="product_id" value="'. $minicart[$ci]['product_id'] .'" />
    <input type="hidden" name="description" value="'. $minicart[$ci]["description"].'" />
  	<input type="image" name="delete" title="'. $VM_LANG->_('PHPSHOP_CART_DELETE') .'" src="modules/mod_virtuemart_onepagecheckout/assets/images/remove_from_cart.png" alt="'. $VM_LANG->_('PHPSHOP_CART_DELETE') .'" align="middle" onclick="return opcoHandleCartDelete(this.parentNode);" />
  </form>';

		if(@$_SESSION['vmCartDirection']) {
			$i++;
		}
		else {
			$i--;
		}

		$ci++;
	} 
	//End loop through cart


}
if( !empty($_SESSION['coupon_discount']) ) {
	$total -= $_SESSION['coupon_discount'];
}
if(!$empty_cart) {
	if ($amount > 1) {
		$total_products = $amount ." ". $VM_LANG->_('PHPSHOP_PRODUCTS_LBL');
	}
	else {
		$total_products = $amount ." ". $VM_LANG->_('PHPSHOP_PRODUCT_LBL');
	}


	$total_price = $CURRENCY_DISPLAY->getFullValue( $total );
}
// Display clear cart
$delete_cart = '';
if(@$_SESSION['vmEnableEmptyCart'] && !@$_SESSION['vmMiniCart']) {
	// Output the empty cart button
	//echo vmCommonHTML::scriptTag( $mosConfig_live_site.'/components/'.$option.'/js/wz_tooltip.js' );
	$delete_cart = "<a href=\"".$_SERVER['SCRIPT_NAME'] . "?page=shop.cart_reset&amp;option=com_virtuemart&amp;option2=$option&amp;product_id=$prodid&amp;category_id=$catid&amp;return=$page&amp;flypage=$flypage&amp;Itemid=$Itemid\" title=\"". $VM_LANG->_('PHPSHOP_EMPTY_YOUR_CART') ." \">
					<img src=\"". $mosConfig_live_site ."/images/cancel_f2.png\" width=\"12\" border=\"0\" style=\"float: right;vertical-align: middle;\" alt=\"". $VM_LANG->_('PHPSHOP_EMPTY_YOUR_CART') ." \" />
      </a>"; 
	$html1 = vmToolTip($VM_LANG->_('VM_EMPTY_YOUR_CART_TIP'), $VM_LANG->_('PHPSHOP_EMPTY_YOUR_CART'),'','',$delete_cart,true);
	$delete_cart = $html1;

}

$href = $sess->url(URL."index.php?page=shop.cart");
$href2 = $sess->url(URL."index2.php?page=shop.cart", true);
$text = JText::_('Edit Cart');
if( @$_SESSION['vmUseGreyBox'] ) {
	$show_cart = vmCommonHTML::getGreyboxPopUpLink( $href2, $text, '', $text, '', 500, 600, $href );
}
else {
	$show_cart = vmCommonHTML::hyperlink( $href, $text, '', $text, '' );
}

$tpl->set('minicart',$minicart);
$tpl->set('empty_cart', $empty_cart);
$tpl->set('delete_cart', $delete_cart);
$tpl->set('vmMinicart', @$_SESSION['vmMiniCart']);
$tpl->set('total_products', @$total_products);
$tpl->set('total_price', @$total_price);
$tpl->set('show_cart', @$show_cart);
$tpl->set('opcoConfig', $opcoConfig);
$saved_cart_text = "";
if($saved_cart['idx'] != 0) {
	$saved_cart_text = "";
}
$tpl->set('saved_cart',$saved_cart_text);
$tpl->set_path($myPath);

if ($opcoConfig->reviewCart->allowCartUpdates)
{
	echo $tpl->fetch( 'tmpl/basket.tpl.php');
}
else
{
	echo $tpl->fetch( 'tmpl/minicart.tpl.php');
}

?>
