<?php
/* Joomla! 1.5 One Page Checkout Module for VirtueMart
*
* @version $Id: checkoutTotals_ajax.php 05-24-2011
 * author: Polished Geek
 * Website: PolishedGeek.com
 * Email: info@PolishedGeek.com
 * Phone: 919-374-2425
 *
 * license http://www.gnu.org/copyleft/gpl.html GNU/GPL
 *
*/

global $_SESSION, $_REQUEST, $order_total, $total, $tax_total, $db, $VM_CHECKOUT_MODULES;

define( '_JEXEC', 1 );
define( 'DS', DIRECTORY_SEPARATOR );
define('JPATH_BASE', dirname('..'.DS.'..'.DS.'configuration.php'));


require_once ( JPATH_BASE .DS.'includes'.DS.'defines.php' );
require_once ( JPATH_BASE .DS.'includes'.DS.'framework.php' );
require_once ( JPATH_BASE .DS.'libraries'.DS.'joomla'.DS.'language'.DS.'language.php');
require_once ( JPATH_BASE .DS.'libraries'.DS.'joomla'.DS.'language'.DS.'helper.php');
require_once ( JPATH_BASE .DS.'libraries'.DS.'joomla'.DS.'application'.DS.'component'.DS.'helper.php');



$mainframe =& JFactory::getApplication('site');
$mainframe->initialise();


$session =& JFactory::getSession();


$user = JFactory::getUser();

$username = $user->name;

$language =& JFactory::getLanguage();
$language->load('mod_virtuemart_onepagecheckout', JPATH_BASE, $language->getTag());

global $VM_LANG, $vm_mainframe, $sess, $mm_action_url, $_SESSION, $db, $_REQUEST, $GLOBALS, $ps_html, $VM_LANG, $my, $vm_mainframe, $mainframe, $auth;



if(isset($_SESSION['combine_tax'])) {
	$combine_tax = $_SESSION['combine_tax'];
} else {
	$combine_tax = 0;
}


if($user->id > 0) {
	$_SESSION['auth']['user_id'] = $user->id;
	$auth = $_SESSION['auth'];
}


$vmCompatFile = JPATH_ADMINISTRATOR.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_virtuemart'.DIRECTORY_SEPARATOR.'compat.joomla1.5.php';
if(file_exists($vmCompatFile)) {
	require_once($vmCompatFile);
	$my = $GLOBALS['my'];
}

$vmCfgFile = JPATH_ADMINISTRATOR.DS.'components'.DS.'com_virtuemart'.DS.'virtuemart.cfg.php';
	if(file_exists($vmCfgFile))
		defined('ADMINPATH') or require_once($vmCfgFile);

$vmGlobalFile = JPATH_ADMINISTRATOR.DS.'components'.DS.'com_virtuemart'.DS.'global.php';
if(file_exists($vmGlobalFile)) {
	require_once($vmGlobalFile);
}



$vmParserFile = JPATH_SITE.DS.'components'.DS.'com_virtuemart'.DS.'virtuemart_parser.php';
if( file_exists($vmParserFile)) {
	require_once( $vmParserFile);
}



$vmDBFile = JPATH_ADMINISTRATOR.DS.'components'.DS.'com_virtuemart'.DS.'classes'.DS.'ps_database.php';

$basket_html = '';

////require_once(CLASSPATH. 'ps_product.php' );
////$ps_product = new ps_product;
////require_once(CLASSPATH. 'ps_shipping_method.php' );
////require_once(CLASSPATH. 'ps_checkout.php' );
////$ps_checkout = new ps_checkout();

require_once ( 'mod_load_ajax.php' );

if(!isset($d)) $d = $_REQUEST;

if (!isset($ps_checkout)) $ps_checkout = new ps_checkout();

$totals = $ps_checkout->calc_order_totals( $d );

extract($totals);

$order_total = $CURRENCY_DISPLAY->getFullValue($totals['order_total']);


if(PSHOP_COUPONS_ENABLE == 1) {
	$coupon_discount = $totals['coupon_discount'];

	$coupon_discount = abs($coupon_discount);
}

if(PAYMENT_DISCOUNT_BEFORE == '1' ) {
	$d['total'] = $totals['order_subtotal'];
}

/* awocoupon code start */
$totals['order_total'] += $coupon_discount;
require_once(CLASSPATH. 'ps_coupon.php');
$ps_coupon = new ps_coupon();
$ps_coupon->process_coupon_code($d);

$coupon_shipping_discount = 0;
if(isset($_SESSION['coupon_awo_shipping_discount'])) {
	$coupon_shipping_discount = @$_SESSION['coupon_awo_shipping_discount'];
}

$coupon_disount = 0;

if(isset($_SESSION['coupon_discount'])) {
	$coupon_discount = $_SESSION['coupon_discount'];
}

$coupon_discount += $coupon_shipping_discount;
$totals['order_total'] -= $coupon_discount;
/* awocoupon code end */


$payment_discount = -$totals['payment_discount'];

$show_shipping_rate = $opcoConfig->shippingInstr->showSection;
$show_payment_charge = $opcoConfig->confirmPlaceOrder->showPaymentCharge;

$order_subtotal = $CURRENCY_DISPLAY->getFullValue($totals['order_subtotal']);
$shipping_amount = $CURRENCY_DISPLAY->getFullValue($totals['order_shipping']);

if($combine_tax) {

	$order_subtotal = $CURRENCY_DISPLAY->getFullValue($totals['order_tax'] + $totals['order_subtotal']);
	$shipping_amount = $CURRENCY_DISPLAY->getFullValue($totals['order_shipping_tax'] + $totals['order_shipping']);
}


echo '<br><div id=checkoutTotals>';
echo '<table>';
echo '<tr><td width="120px">'.JText::_('Order Subtotal').': </td><td align="right">'.$order_subtotal.'</td></tr>';
if(!$combine_tax) {
    echo '<tr><td width="120px">'.JText::_('Order Tax').': </td><td align="right">'.$CURRENCY_DISPLAY->getFullValue($totals['order_tax']).'</td></tr>';
}
if ($show_shipping_rate) {
    echo '<tr><td width="120px">'.JText::_('Shipping amount').': </td><td align="right">'.$shipping_amount.'</td></tr>';
}
if(!$combine_tax && $show_shipping_rate) {
    echo '<tr><td width="120px">'.JText::_('Shipping tax').': </td><td align="right">'.$CURRENCY_DISPLAY->getFullValue($totals['order_shipping_tax']).'</td></tr>';
}
if(PSHOP_COUPONS_ENABLE == 1) {
	echo '<tr><td width="120px">'.JText::_('Coupon discount'). ': </td><td align="right">'.$CURRENCY_DISPLAY->getFullValue($coupon_discount).'</td></tr>';
}
if ($show_payment_charge) {
    echo '<tr><td width="120px">'.JText::_('Payment charge').': </td><td align="right">'.$CURRENCY_DISPLAY->getFullValue($payment_discount).'</td></tr>';
}
echo '<tr><td width="120px">'.JText::_('Order Total').': </td><td align="right">'.$CURRENCY_DISPLAY->getFullValue($totals['order_total']).'</td></tr>';

echo '</table>';




echo '</div><br>';

if($totals['order_total'] <= 0.00) {
	if(isset($VM_CHECKOUT_MODULES['CHECK_OUT_GET_PAYMENT_METHOD'])) {
		unset($VM_CHECKOUT_MODULES['CHECK_OUT_GET_PAYMENT_METHOD']);
	}
	echo "<script LANGUAGE=javascript>hidePaymentSection();</script><br>";
} else {
	if(!isset($VM_CHECKOUT_MODULES['CHECK_OUT_GET_PAYMENT_METHOD'])) {
		$VM_CHECKOUT_MODULES['CHECK_OUT_GET_PAYMENT_METHOD'] = array('order' => 1, 'enabled' => 1);

	}
	echo "<script LANGUAGE=javascript>showPaymentSection();</script><br>"; 
}

 
 



?>
