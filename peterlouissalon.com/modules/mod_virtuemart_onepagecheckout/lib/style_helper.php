<?php

if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' );

/* Joomla! 1.5 One Page Checkout Module for VirtueMart
*
* @version $Id: style_helper.php 05-24-2011
 * author: Polished Geek
 * Website: PolishedGeek.com
 * Email: info@PolishedGeek.com
 * Phone: 919-374-2425
 *
 * license http://www.gnu.org/copyleft/gpl.html GNU/GPL
 *
*/

class StyleHelper extends JObject
{
    public static $opcoConfig;
    private static $_syncLock = false;

    function init($opcoConfigParam)
    {
        self::$opcoConfig = $opcoConfigParam;
    }
    
    function showError($message, $reason)
    {
        $s = $message;
        $s .= (strlen($reason) > 0) ? "<br />".$reason : '';
        
        return self::$opcoConfig->styles->error->styleText($s);
    }
    
    function showHeader($headerText)
    {
        return self::$opcoConfig->styles->sectionTitle->styleText($headerText);
    }
    
    function startSection($section)
    {
        echo('<div id="' . $section->sectionPrefix . '" class="opco_section">');
        echo($this->showHeader(JText::_($section->getTitle())));
    }
    
    function endSection()
    {
        echo('<div style="clear:both"></div>');
        echo('</div>');
    }
    
    function getClass($class)
    {
        return $class . self::$opcoConfig->styles->moduleclass_sfx;
    }
    
    
    function writeOpcoStyle()
    {
        $doc =& JFactory::getDocument();
        $doc->addStyleDeclaration(self::$opcoConfig->getCustomStyle());
    }
}


?>
