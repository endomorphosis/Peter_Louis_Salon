<?php
/**
  * @version     1.5
  * @package        Open Source Excellence Google Checkout - com_osemsc
  * @author        Open Source Excellence {@link http://www.opensource-excellence.co.uk}
  * @author        Created on 29-April-2011
  * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
  *
  *
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  *  @Copyright Copyright (C) 2010- ... author-name
*/
$messages= array();
function security_check() {
	$iplist = array();
	$iplist[]= gethostbynamel('checkout.google.com');
	$iplist[]= gethostbynamel('checkout.google.co.uk');
	$iplist[]= gethostbynamel('sandbox.google.com');
	$remote_hostname= $_SERVER['REMOTE_ADDR'];
	$valid_ip= false;

	$ips= "";
		// Loop through all allowed IPs and test if the remote IP connected here
		// is a valid IP address
	foreach($iplist as $ip) {
			$parts= explode(".", $ip);
			$first_two= $parts[0].".".$parts[1];
			if(preg_match("/^$first_two/", $_SERVER['REMOTE_ADDR'])) {
				$valid_ip= true;
				break;
			}
		}
	if(!$valid_ip) {
		debug_msg("Error code 506. Possible fraud. Error with REMOTE IP ADDRESS = ".$_SERVER['REMOTE_ADDR'].".
                   The remote address of the script posting to this notify script does not match a valid Google ip address\n");
		$mailsubject= "Google Notification Transaction on your site: Possible fraud";
		$mailbody= "Error code 506. Possible fraud. Error with REMOTE IP ADDRESS = ".$_SERVER['REMOTE_ADDR'].".
                    The remote address of the script posting to this notify script does not match a valid Google IP address\n
		            These are the valid IP Addresses: $ips
		            The Order ID received was: $google_order_number\n
					$post_msg";
		mail($mailfrom, $mailsubject, $mailbody);
		exit();
	}
}
if($_POST) {
	global $mosConfig_absolute_path, $mosConfig_live_site, $mosConfig_lang, $database, $mailfrom, $fromname;
	/*** access Joomla's configuration file ***/
	$my_path= dirname(__file__);
	if(file_exists($my_path."/../../../configuration.php")) {
		$absolute_path= dirname($my_path."/../../../configuration.php");
		require_once($my_path."/../../../configuration.php");
	}
	elseif(file_exists($my_path."/../../configuration.php")) {
		$absolute_path= dirname($my_path."/../../configuration.php");
		require_once($my_path."/../../configuration.php");
	}
	elseif(file_exists($my_path."/../configuration.php")) {
		$absolute_path= dirname($my_path."/../configuration.php");
		require_once($my_path."/../configuration.php");
	}
	elseif(file_exists($my_path."/configuration.php")) {
		$absolute_path= dirname($my_path."/configuration.php");
		require_once($my_path."/configuration.php");
	} else {
		die("Joomla Configuration File not found!");
	}
	$absolute_path= realpath($absolute_path);
	// Set up the appropriate CMS framework
	if(class_exists('JConfig')) {
		define('_JEXEC', 1);
		define('JPATH_BASE', $absolute_path);
		define('DS', DIRECTORY_SEPARATOR);
		// Load the framework
		require_once(JPATH_BASE.DS.'includes'.DS.'defines.php');
		require_once(JPATH_BASE.DS.'includes'.DS.'framework.php');
		// create the mainframe object
		$mainframe= & JFactory :: getApplication('site');
		// Initialize the framework
		//$mainframe->initialise();
		// load system plugin group
		//JPluginHelper :: importPlugin('system');
		// trigger the onBeforeStart events
		//$mainframe->triggerEvent('onBeforeStart');
		$lang= & JFactory :: getLanguage();
		$mosConfig_lang= $GLOBALS['mosConfig_lang']= strtolower($lang->getBackwardLang());
		// Adjust the live site path
		$u= & JURI :: getInstance();
		if($u->isSSL()) {
			$pro= "https://";
		} else {
			$pro= "http://";
		}
		$mosConfig_live_site= $pro.$u->getHost();
		$mosConfig_absolute_path= JPATH_BASE;
		$config= new JConfig();
		$mailfrom= $config->mailfrom;
		$fromname= $config->fromname;
	}
	//security_check($mailfrom);
	// load Joomla Language File
	if(file_exists($mosConfig_absolute_path.'/language/'.$mosConfig_lang.'.php')) {
		require_once($mosConfig_absolute_path.'/language/'.$mosConfig_lang.'.php');
	}
	elseif(file_exists($mosConfig_absolute_path.'/language/english.php')) {
		require_once($mosConfig_absolute_path.'/language/english.php');
	}
	/*** END of Joomla config ***/
	/*** OSE part ***/
	require_once($mosConfig_absolute_path.'/administrator/components/com_virtuemart/virtuemart.cfg.php');
	include_once(ADMINPATH.'/compat.joomla1.5.php');
	require_once(ADMINPATH.'global.php');
	require_once(CLASSPATH.'ps_main.php');
	/* @MWM1: Logging enhancements (file logging & composite logger). */
	$vmLogIdentifier= "notify.php";
	require_once(CLASSPATH."Log/LogInit.php");
	/* Load the Google Checkout Configuration File */
	require_once(CLASSPATH.'payment/ps_google.cfg.php');
	$google_merchant_id= GOOGLE_MERCHANT_ID;
	$google_merchant_key= GOOGLE_MERCHANT_KEY;
	$test_mode= GOOGLE_SANDBOX;
	$db= JFactory :: getDBO();
	/*** END OSE part ***/
	$serial_number= trim(stripslashes($_POST['serial-number']));
	$google_order_number= trim(stripslashes($_POST['google-order-number']));
	$fulfillment_order_state= trim(stripslashes($_POST['fulfillment-order-state']));
	$financial_order_state= trim(stripslashes($_POST['financial-order-state']));
	$new_financial_order_state= trim(stripslashes($_POST['new-financial-order-state']));
	$timestamp= trim(stripslashes($_POST['timestamp']));
	$payer_email= trim(stripslashes($_POST['buyer-billing-address_email']));
	$merchant_item_id= trim(stripslashes($_POST['shopping-cart_items_item-1_merchant-item-id']));
	$invoice= trim(stripslashes($_POST['shopping-cart_items_item-1_merchant-item-id']));
	$remote_hostname= $_SERVER['REMOTE_ADDR'];
	if($test_mode) {
		$hostname= 'sandbox.google.com/checkout';
	} else {
		$hostname= 'checkout.google.com';
	}
	$schema_url= $hostname."/api/checkout/v2/requestForm/Merchant/".$google_merchant_id;
	switch($_POST['_type']) {
		case("authorization-amount-notification"):
		case("new-order-notification") :
			$serialNumber= $db->Quote($serial_number);
			$invoice= trim(stripslashes($_POST['shopping-cart_items_item-1_merchant-item-id']));
			if(!empty($invoice)) {
				$query= " CREATE TABLE IF NOT EXISTS `#__ose_googlecheckout` (
										  `id` int(11) NOT NULL auto_increment,
										  `vm_ordernumber` text NOT NULL,
										  `google_ordernumber` text NOT NULL,
										  PRIMARY KEY  (`id`)
										) ENGINE=MyISAM; ";
				$db->setQuery($query);
				$db->query();
				$query= "SELECT `vm_ordernumber` FROM `#__ose_googlecheckout` WHERE `google_ordernumber`=".$db->Quote($google_order_number);
				$db->setQuery($query);
				$vm_ordernumber= $db->loadResult();
				if(empty($vm_ordernumber)) {
					$query= "INSERT INTO `#__ose_googlecheckout` (`id`, `vm_ordernumber`, `google_ordernumber`) VALUES
												  (NULL, ".$db->Quote($invoice).", ".$db->Quote($google_order_number).");";
					$db->setQuery($query);
					$db->query();
				}
			}
			$acknowledgment= "<?xml version=\"1.0\" encoding=\"UTF-8\"?>".			"<notification-acknowledgment xmlns=\"".			$schema_url."\"";
			if(isset($serial_number)) {
				$acknowledgment .= " serial-number=\"".$serial_number."\"";
			}
			$acknowledgment .= " />";
			echo $acknowledgment;
			break;
		case("order-state-change-notification") :
			switch($new_financial_order_state) {
				case('REVIEWING') :
					break;
				case('CHARGEABLE') :
					break;
				case('CHARGING') :
					break;
				case('CHARGED') :
					$query= "SELECT `vm_ordernumber` FROM `#__ose_googlecheckout` WHERE `google_ordernumber`=".$db->Quote($google_order_number);
					$db->setQuery($query);
					$vm_ordernumber= $db->loadResult();

					$qv= "SELECT order_id FROM `#__vm_orders` WHERE `order_number`=".$db->Quote($vm_ordernumber);
					$db->setQuery($qv);
					$order_id= $db->loadResult();

					$d['order_id']= $order_id;
					$d['notify_customer']= "Y";
					if(empty($order_id)) {
						$mailsubject= "Google Notification Transaction on your site: Order ID not found";
						$mailbody= "The right order_id wasn't found during a Google transaction on your website.
												The Order ID received was: $invoice";
						vmMail($mosConfig_mailfrom, $mosConfig_fromname, $debug_email_address, $mailsubject, $mailbody);
						exit();
					} else {
						$d['order_status']= GOOGLE_VERIFIED_STATUS;
						require_once(CLASSPATH.'ps_order.php');
						$ps_order= new ps_order;
						$ps_order->order_status_update($d);
						$mailsubject= "Google Notification on your site";
						$mailbody= "Hello Admin,\n\n";
						$mailbody .= "a Google transaction for you has been made on your website!\n";
						$mailbody .= "-----------------------------------------------------------\n";
						$mailbody .= "Transaction ID: $google_order_number\n";
						$mailbody .= "Buyer Email: $payer_email\n";
						$mailbody .= "Order ID: $order_id\n";
						if(empty($new_financial_order_state)) {
							$mailbody .= "Payment Status returned by Google: $financial_order_state\n";
						} else {
							$mailbody .= "Payment Status returned by Google: $new_financial_order_state\n";
						}
						$mailbody .= "Order Status Code: ".$d['order_status'];
						vmMail("$mosConfig_mailfrom", $mosConfig_fromname, $debug_email_address, $mailsubject, $mailbody);
					}
					break;
				case('PAYMENT_DECLINED') :
					break;
				case('CANCELLED') :
					$query= "SELECT `vm_ordernumber` FROM `#__ose_googlecheckout` WHERE `google_ordernumber`=".$db->Quote($google_order_number);
					$db->setQuery($query);
					$vm_ordernumber= $db->loadResult();

					$qv= "SELECT order_id FROM `#__vm_orders` WHERE `order_number`=".$db->Quote($vm_ordernumber);
					$db->setQuery($qv);
					$order_id= $db->loadResult();

					$d['order_id']= $order_id;
					$d['notify_customer']= "Y";
					if(empty($order_id)) {
						$mailsubject= "Google Notification Transaction on your site: Order ID not found";
						$mailbody= "The right order_id wasn't found during a Google transaction on your website.
												The Order ID received was: $invoice";
						vmMail($mosConfig_mailfrom, $mosConfig_fromname, $debug_email_address, $mailsubject, $mailbody);
						exit();
					} else {
						$d['order_status']= GOOGLE_PENDING_STATUS;
						require_once(CLASSPATH.'ps_order.php');
						$ps_order= new ps_order;
						$ps_order->order_status_update($d);
						$mailsubject= "Google Notification on your site";
						$mailbody= "Hello Admin,\n\n";
						$mailbody .= "a Google transaction for you has been made on your website!\n";
						$mailbody .= "-----------------------------------------------------------\n";
						$mailbody .= "Transaction ID: $google_order_number\n";
						$mailbody .= "Buyer Email: $payer_email\n";
						$mailbody .= "Order ID: $order_id\n";
						if(empty($new_financial_order_state)) {
							$mailbody .= "Payment Status returned by Google: $financial_order_state\n";
						} else {
							$mailbody .= "Payment Status returned by Google: $new_financial_order_state\n";
						}
						$mailbody .= "Order Status Code: ".$d['order_status'];
						vmMail($mosConfig_mailfrom, $mosConfig_fromname, $debug_email_address, $mailsubject, $mailbody);
					}
					break;
				case('CANCELLED_BY_GOOGLE') :
					//$Grequest->SendBuyerMessage($data[$root]['google-order-number']['VALUE'],
					//    "Sorry, your order is cancelled by Google", true);
					break;
				default :
					break;
			}
			$acknowledgment= "<?xml version=\"1.0\" encoding=\"UTF-8\"?>".			"<notification-acknowledgment xmlns=\"".			$schema_url."\"";
			if(isset($serial_number)) {
				$acknowledgment .= " serial-number=\"".$serial_number."\"";
			}
			$acknowledgment .= " />";
			echo $acknowledgment;
			break;
		case("charge-amount-notification") :
			$serialNumber= $db->Quote($serial_number);
			$acknowledgment= "<?xml version=\"1.0\" encoding=\"UTF-8\"?>".			"<notification-acknowledgment xmlns=\"".			$schema_url."\"";
			if(isset($serial_number)) {
				$acknowledgment .= " serial-number=\"".$serial_number."\"";
			}
			$acknowledgment .= " />";
			echo $acknowledgment;
			break;
		case("chargeback-amount-notification") :
		case("refund-amount-notification") :
		case("risk-information-notification") :
			$acknowledgment= "<?xml version=\"1.0\" encoding=\"UTF-8\"?>".			"<notification-acknowledgment xmlns=\"".			$schema_url."\"";
			if(isset($serial_number)) {
				$acknowledgment .= " serial-number=\"".$serial_number."\"";
			}
			$acknowledgment .= " />";
			echo $acknowledgment;
			break;
		default :
			$msg= '400 Bad Request';
			header('HTTP/1.0 400 Bad Request');
			echo $msg;
			break;
	}
}
?>
