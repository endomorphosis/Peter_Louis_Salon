<?php
/**
 * @version		1.5.2
 * @package		Joomla
 * @subpackage	Product Share
 * @author  	Giang Dinh Truong
 * @copyright	Copyright (C) 2012 Ossolution Team
 * @license		GNU/GPL, see LICENSE.php
 */
// No direct access
defined('_JEXEC') or die;
class plgSystemProductShare extends JPlugin
{
	/**
	 * 
	 * Function to add css, javascript to header
	 */
	function onAfterInitialise() {
		// Use this plugin only in site application
		if (JFactory::getApplication()->isSite()) {
			$document =& JFactory::getDocument();
			// Add style for Product Share buttons
			$document->addStyleSheet(JURI::base().'plugins/system/productshare/css/style.css');
			// Add script for Twitter
			if ($this->params->get('show_twitter_button', '1')) {
				$script = '!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");';
				$document->addScriptDeclaration($script);
			}
			//Add script for PinIt 
			if ($this->params->get('show_pinit_button', '1')) {
				$script = '(function() {
					    window.PinIt = window.PinIt || { loaded:false };
					    if (window.PinIt.loaded) return;
					    window.PinIt.loaded = true;
					    function async_load(){
					        var s = document.createElement("script");
					        s.type = "text/javascript";
					        s.async = true;
					        s.src = "http://assets.pinterest.com/js/pinit.js";
					        var x = document.getElementsByTagName("script")[0];
					        x.parentNode.insertBefore(s, x);
					    }
					    if (window.attachEvent)
					        window.attachEvent("onload", async_load);
					    else
					        window.addEventListener("load", async_load, false);
					})();
				';
				$document->addScriptDeclaration($script);
			}
			// Add script for LinkedIn
			if ($this->params->get('show_linkedin_button', '1')) {
				$document->addScript('//platform.linkedin.com/in.js');
			}
			// Add script for Google
			if ($this->params->get('show_google_button', '1')) {
				$script = '(function() {
								    var po = document.createElement("script"); po.type = "text/javascript"; po.async = true;
								    po.src = "https://apis.google.com/js/plusone.js";
								    var s = document.getElementsByTagName("script")[0]; s.parentNode.insertBefore(po, s);
								  })();';
				$document->addScriptDeclaration($script);
			}
			// Add script for Facebook
			if ($this->params->get('show_facebook_like', '1') || $this->params->get('show_facebook_send', '1') || $this->params->get('show_facebook_comment', '1')) {
				$script = '(function(d, s, id) {
								  var js, fjs = d.getElementsByTagName(s)[0];
								  if (d.getElementById(id)) return;
								  js = d.createElement(s); js.id = id;
								  js.src = "//connect.facebook.net/'.$this->params->get('lang', 'en_US').'/all.js#xfbml=1&appId=372958799407679";
								  fjs.parentNode.insertBefore(js, fjs);
								}(document, "script","facebook-jssdk"));';
				$document->addScriptDeclaration($script);	
			}	
		}
	}
	
	function onAfterDispatch() {
		if (JFactory::getApplication()->isSite() && JRequest::getVar('option') == 'com_virtuemart' && Jrequest::getVar('page') == 'shop.product_details') {
			if ($this->params->get('show_facebook_like', '1') || $this->params->get('show_facebook_send', '1') || $this->params->get('show_facebook_comment', '1')) {
				// Add Open Graph Tags for facebook
				$conf = JFactory::getConfig();
				$uri = JURI::getInstance();
				$document =& JFactory::getDocument();
				$virtuemart_product_id = JRequest::getInt('product_id');
				$product_info = $this->_getProductInfo($virtuemart_product_id);
				$document->addCustomTag('<meta property="og:title" content="'.$product_info->product_name.'"/>');
				$document->addCustomTag('<meta property="og:type" content="product"/>');
				$document->addCustomTag('<meta property="og:image" content="'.JURI::base()."components/com_virtuemart/shop_image/product/".$product_info->product_thumb_image.'"/>');
				$document->addCustomTag('<meta property="og:url" content="'.$uri->toString().'"/>');
				$document->addCustomTag('<meta property="og:description" content="'.$product_info->product_name.'"/>');
				$document->addCustomTag('<meta property="og:site_name" content="'.$conf->get('sitename').'"/>');
				$document->addCustomTag('<meta property="fb:admins" content="'.$this->params->get('app_id', '372958799407679').'"/>');
			}
		}
	}

	/**
	 * 
	 * Function to add Product Share area on top of page
	 */
	public function onAfterRender()
	{
		// Use this plugin only in site application
		if (JFactory::getApplication()->isSite() && JRequest::getVar('option') == 'com_virtuemart' && Jrequest::getVar('page') == 'shop.product_details') {
			// Add Facebook Like and Send Button
			$addedElements = '<div class="ps_area">';
			if ($this->params->get('show_facebook_like', '1')) {
				$addedElements .= '<div class="ps_facebook_like">'.$this->_getFacebookLikeButton($this->params).'</div>';
			}
			// Add Facebook Send Button
			if ($this->params->get('show_facebook_send', '1') && !$this->params->get('show_facebook_like', '1')) {
				$addedElements .= '<div class="ps_facebook_send">'.$this->_getFacebookSendButton($this->params).'</div>';
			}
			// Add Twitter Button
			if ($this->params->get('show_twitter_button', '1')) {
				$addedElements .= '<div class="ps_twitter">'.$this->_getTwitterButton($this->params).'</div>';
			}
			//Add Pinit Button
			if ($this->params->get('show_pinit_button', '1')) {
				$addedElements .= '<div class="ps_pinit">'.$this->_getPinitButton($this->params).'</div>';
			}
			// Add LinkedIn Button
			if ($this->params->get('show_linkedin_button', '1')) {
				$addedElements .= '<div class="ps_linkedin">'.$this->_getLinkedInButton($this->params).'</div>';
			}
			// Add Google Button
			if ($this->params->get('show_google_button', '1')) {
				$addedElements .= '<div class="ps_google">'.$this->_getGoogleButton($this->params).'</div>';
			}
			$addedElements .= '</div>';
			// Get the response body
			$body = JResponse::getBody();
			$body = str_replace('<div id="vmMainPage">', $addedElements.'<div id="vmMainPage">', $body);
			JResponse::setBody($body);
		}
	}
	
	/**
	 * Content Prepare for Joomla 1.6 or later
	 *
	 * @param	string	The context of the content being passed to the plugin.
	 * @param	object	The product object.
	 * @param	object	The params
	 * @param	int		The 'page' number
	 */
	function onContentPrepare($context, &$product, &$params, $page = 0) {
		// Add Facebook Comment
		if ($this->params->get('show_facebook_comment', '1') && $context == 'shop.product_details') {
			$facebookComment = $this->_getFacebookComment($this->params);
			$product->text = $product->text.'<br />'.$facebookComment;
		}
	}
	
	/**
	 * Content Prepare for Joomla 1.5
	 *
	 * @param	object	The product object.
	 * @param	object	The params
	 * @param	int		The 'page' number
	 */
	function onPrepareContent( &$product, &$params, $page = 0 ) {
		// Add Facebook Comment
		if ($this->params->get('show_facebook_comment', '1') && JRequest::getVar('option') == 'com_virtuemart') {
			$facebookComment = $this->_getFacebookComment($this->params);
			$product->text = $product->text.'<br />'.$facebookComment;
		}
	}

	/**
	 * 
	 * Private method to get Facebook Like Button
	 * @param object $params
	 */
	function _getFacebookLikeButton($params) {
		$facebookSendButton = '<div class="fb-like"'.($params->get('show_facebook_send', '1') ? ' data-send="true"' : '').' data-width="'.$params->get('width', '450').'" data-show-faces="'.$params->get('show_faces', '1').'" vdata-font="'.$params->get('button_font', 'arial').'" data-colorscheme="'.$params->get('button_theme', 'light').'" layout="button_count"></div>';
		return $facebookSendButton;
	}

	/**
	 * 
	 * Private method to get Facebook Send Button
	 * @param object $params
	 */
	function _getFacebookSendButton($params) {
		$facebookSendButton = '<div class="fb-send" vdata-font="'.$params->get('button_font', 'arial').'" data-colorscheme="'.$params->get('button_theme', 'light').'"></div>';
		return $facebookSendButton;
	}
	
	/**
	 * 
	 * Private method to get Facebook Comment
	 * @param object $params
	 */
	function _getFacebookComment($params) {
		$uri = JURI::getInstance();
		$facebookComment = '<div class="fb-comments" data-num-posts="'.$params->get('num_posts', '10').'" data-width="'.$params->get('comment_width', '400').'"  data-href="'.$uri->toString().'"></div>';
		return $facebookComment;
	}

	/**
	 * 
	 * Private method to get Twitter Button
	 * @param object $params
	 */
	function _getTwitterButton($params) {
		$uri = JURI::getInstance();
		$twitterButton = '<a href="https://twitter.com/share" class="twitter-share-button" data-url="'.$uri->toString().'" tw:via="ontwiik" data-lang="en" data-related="anywhereTheJavascriptAPI" data-count="horizontal">Tweet</a>';
		return $twitterButton;
	}
	/**
	 * Private method to get Pinit Button
	 */
	function _getPinitButton($params) {
		$uri = JURI::getInstance();
		define('URL_THUB_IMAGE',JURI::base().'components/com_virtuemart/shop_image/product/');
		$virtuemart_product_id = JRequest::getInt('product_id');
		$product_info = $this->_getProductInfo($virtuemart_product_id);
		$pinitButton = '<a href="http://pinterest.com/pin/create/button/?url='.urlencode($uri->toString()).'&media='.urlencode(URL_THUB_IMAGE.$product_info->product_thumb_image).'&description='.$product_info->product_name.'" count-layout="horizontal" class="pin-it-button">Pin It</a>';
		return $pinitButton;		
	}
	/**
	 * 
	 * Private method to get Google+ Button
	 * @param object $params
	 */
	function _getGoogleButton($params) {
		$googleButton = '<g:plusone annotation="vertical"></g:plusone>';
		return $googleButton;
	}
	
	/**
	 * 
	 * Private method to get LinkedIn Button
	 * @param object $params
	 */
	function _getLinkedInButton($params) {
		if ($params->get('linkedin_layout', 'right') == "no-count") {
			$linkedInButton = '<script type="IN/Share"></script>';
		} else {
			$linkedInButton = '<script type="IN/Share" data-counter="'.$params->get('linkedin_layout', 'right').'"></script>';
		}
		return $linkedInButton;
	}
	
	/**
	 * 
	 * Private method to get information for a specific product
	 * @param int $id
	 */
	function _getProductInfo($id) {
		$db = JFactory::getDBO();
		$sql = 'SELECT pi.*'.
			' FROM #__vm_product AS pi' .
			' WHERE product_id = ' . $id
			;
		$db->setQuery($sql);
		return $db->loadObject();
	}
}