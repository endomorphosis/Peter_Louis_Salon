<?php
/**
 * @version		1.5.2
 * @package		Joomla
 * @subpackage	Product Share
 * @author  	Giang Dinh Truong
 * @copyright	Copyright (C) 2012 Ossolution Team
 * @license		GNU/GPL, see LICENSE.php
 */
// No direct access
defined('_JEXEC') or die;
	jimport('joomla.form.formfield');
	class JElementLangfield extends JElement
	{
		/**
		* Element name
		*
		* @access	protected
		* @var		string
		*/
		var	$_name = 'Langfield';
	
		function fetchElement($name, $value, &$node, $control_name)
		{
			$options = array();
    		$options[] = JHTML::_('select.option', 'af_ZA', 'Afrikaans');
			$options[] = JHTML::_('select.option', 'ar_AR', 'Arabic');
			$options[] = JHTML::_('select.option', 'az_AZ', 'Azerbaijani');
			$options[] = JHTML::_('select.option', 'be_BY', 'Belarusian');
			$options[] = JHTML::_('select.option', 'bg_BG', 'Bulgarian');
			$options[] = JHTML::_('select.option', 'bn_IN', 'Bengali');
			$options[] = JHTML::_('select.option', 'bs_BA', 'Bosnian');
			$options[] = JHTML::_('select.option', 'ca_ES', 'Catalan');
			$options[] = JHTML::_('select.option', 'cs_CZ', 'Czech');
			$options[] = JHTML::_('select.option', 'cy_GB', 'Welsh');
			$options[] = JHTML::_('select.option', 'da_DK', 'Danish');
			$options[] = JHTML::_('select.option', 'de_DE', 'German');
			$options[] = JHTML::_('select.option', 'el_GR', 'Greek');
			$options[] = JHTML::_('select.option', 'en_GB', 'English (UK)');
			$options[] = JHTML::_('select.option', 'en_PI', 'English (Pirate)');
			$options[] = JHTML::_('select.option', 'en_UD', 'English (Upside Down)');
			$options[] = JHTML::_('select.option', 'en_US', 'English (US)');
			$options[] = JHTML::_('select.option', 'eo_EO', 'Esperanto');
			$options[] = JHTML::_('select.option', 'es_ES', 'Spanish (Spain)');
			$options[] = JHTML::_('select.option', 'es_LA', 'Spanish');
			$options[] = JHTML::_('select.option', 'et_EE', 'Estonian');
			$options[] = JHTML::_('select.option', 'eu_ES', 'Basque');
			$options[] = JHTML::_('select.option', 'fa_IR', 'Persian');
			$options[] = JHTML::_('select.option', 'fb_LT', 'Leet Speak');
			$options[] = JHTML::_('select.option', 'fi_FI', 'Finnish');
			$options[] = JHTML::_('select.option', 'fo_FO', 'Faroese');
			$options[] = JHTML::_('select.option', 'fr_CA', 'French (Canada)');
			$options[] = JHTML::_('select.option', 'fr_FR', 'French (France)');
			$options[] = JHTML::_('select.option', 'fy_NL', 'Frisian');
			$options[] = JHTML::_('select.option', 'ga_IE', 'Irish');
			$options[] = JHTML::_('select.option', 'gl_ES', 'Galician');
			$options[] = JHTML::_('select.option', 'he_IL', 'Hebrew');
			$options[] = JHTML::_('select.option', 'hi_IN', 'Hindi');
			$options[] = JHTML::_('select.option', 'hr_HR', 'Croatian');
			$options[] = JHTML::_('select.option', 'hu_HU', 'Hungarian');
			$options[] = JHTML::_('select.option', 'hy_AM', 'Armenian');
			$options[] = JHTML::_('select.option', 'id_ID', 'Indonesian');
			$options[] = JHTML::_('select.option', 'is_IS', 'Icelandic');
			$options[] = JHTML::_('select.option', 'it_IT', 'Italian');
			$options[] = JHTML::_('select.option', 'ja_JP', 'Japanese');
			$options[] = JHTML::_('select.option', 'ka_GE', 'Georgian');
			$options[] = JHTML::_('select.option', 'km_KH', 'Khmer');
			$options[] = JHTML::_('select.option', 'ko_KR', 'Korean');
			$options[] = JHTML::_('select.option', 'ku_TR', 'Kurdish');
			$options[] = JHTML::_('select.option', 'la_VA', 'Latin');
			$options[] = JHTML::_('select.option', 'lt_LT', 'Lithuanian');
			$options[] = JHTML::_('select.option', 'lv_LV', 'Latvian');
			$options[] = JHTML::_('select.option', 'mk_MK', 'Macedonian');
			$options[] = JHTML::_('select.option', 'ml_IN', 'Malayalam');
			$options[] = JHTML::_('select.option', 'ms_MY', 'Malay');
			$options[] = JHTML::_('select.option', 'nb_NO', 'Norwegian (bokmal)');
			$options[] = JHTML::_('select.option', 'ne_NP', 'Nepali');
			$options[] = JHTML::_('select.option', 'nl_NL', 'Dutch');
			$options[] = JHTML::_('select.option', 'nn_NO', 'Norwegian (nynorsk)');
			$options[] = JHTML::_('select.option', 'pa_IN', 'Punjabi');
			$options[] = JHTML::_('select.option', 'pl_PL', 'Polish');
			$options[] = JHTML::_('select.option', 'ps_AF', 'Pashto');
			$options[] = JHTML::_('select.option', 'pt_BR', 'Portuguese (Brazil)');
			$options[] = JHTML::_('select.option', 'pt_PT', 'Portuguese (Portugal)');
			$options[] = JHTML::_('select.option', 'ro_RO', 'Romanian');
			$options[] = JHTML::_('select.option', 'ru_RU', 'Russian');
			$options[] = JHTML::_('select.option', 'sk_SK', 'Slovak');
			$options[] = JHTML::_('select.option', 'sl_SI', 'Slovenian');
			$options[] = JHTML::_('select.option', 'sq_AL', 'Albanian');
			$options[] = JHTML::_('select.option', 'sr_RS', 'Serbian');
			$options[] = JHTML::_('select.option', 'sv_SE', 'Swedish');
			$options[] = JHTML::_('select.option', 'sw_KE', 'Swahili');
			$options[] = JHTML::_('select.option', 'ta_IN', 'Tamil');
			$options[] = JHTML::_('select.option', 'te_IN', 'Telugu');
			$options[] = JHTML::_('select.option', 'th_TH', 'Thai');
			$options[] = JHTML::_('select.option', 'tl_PH', 'Filipino');
			$options[] = JHTML::_('select.option', 'tr_TR', 'Turkish');
			$options[] = JHTML::_('select.option', 'uk_UA', 'Ukrainian');
			$options[] = JHTML::_('select.option', 'vi_VN', 'Vietnamese');
			$options[] = JHTML::_('select.option', 'zh_CN', 'Simplified Chinese (China)');
			$options[] = JHTML::_('select.option', 'zh_HK', 'Traditional Chinese (Hong Kong)');
			$options[] = JHTML::_('select.option', 'zh_TW', 'Traditional Chinese (Taiwan)');
			$langField = JHTML::_('select.genericlist', $options, $control_name .'['. $name .']', ' class="inputbox"', 'value', 'text', $value);
			return $langField;
		}
	}
?>