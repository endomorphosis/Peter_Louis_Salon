<?php defined( '_JEXEC' ) or die( 'Restricted access' );?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" >

<head>

<jdoc:include type="head" />

<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/system/css/system.css" type="text/css" />

<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/system/css/general.css" type="text/css" />

<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/template.css" type="text/css" />
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_GB/all.js#xfbml=1&appId=498431036836223";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
</head>  
<?php

$doc = JFactory::getDocument();
$page_title = $doc->getTitle();
$menu = & JSite::getMenu();
if ($menu->getActive() == $menu->getDefault() AND  $page_title != "Cart") {
	echo '<body>';
}
else
{
	echo '<body>';
}
?>





<div id="mainglobal">
<?php
if ($menu->getActive() == $menu->getDefault()) {
	echo '<div id="mainwrapper">';
}
else
{
	echo '<div id="mainwrapper" style="background: white">';
}
?>

<?php
if ($menu->getActive() == $menu->getDefault() AND  $page_title != "Cart") {
	echo '<div id="topbox">';
}
else
{
	echo '<div id="topbox" style="background: white">';
}
?>
	

    <div id="header">

    	  <div class="logo"><a href="index.php"><jdoc:include type="modules" name="logo" /></a></div>

          <div class="smallogos_box"><jdoc:include type="modules" name="logos" /></div>

          <div class="toprgtlnkwrpr"><jdoc:include type="modules" name="buttons" /></div>

    </div>

	

   <jdoc:include type="modules" name="slider" />

	<div id="menubox"><jdoc:include type="modules" name="mainmenu" /></div>

	</div>

      
<?php
if ($menu->getActive() == $menu->getDefault() AND  $page_title != "Cart") {
	echo '<div id="centercontent">';
}
else
{
	echo '<div id="centercontent" style="background: white;margin: 0 0 0 0;">';
}
?>
	
	<jdoc:include type="modules" name="maincomponent" />
	

	<jdoc:include type="component" />   
	<?php
if ($menu->getActive() == $menu->getDefault() AND  $page_title != "Cart") {
	echo '<div class="leftspecialbox"><jdoc:include type="modules" name="left" /></div><div class="rightspecialbox"><jdoc:include type="modules" name="right" /></div>';
}
else
{
	echo '';
}
?> 
	

    </div>

	      

    <div ><jdoc:include type="modules" name="footer" /></div>  

      



</div>

</div>

</body>

</html>