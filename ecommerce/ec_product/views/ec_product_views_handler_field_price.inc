<?php
// $Id: ec_product_views_handler_field_price.inc,v 1.1.2.2 2009/07/02 10:25:40 recidive Exp $

/**
 * @file
 * Handles the display of product price.
 */

class ec_product_views_handler_field_price extends views_handler_field {
  function construct() {
    parent::construct();
    $this->additional_fields['nid'] = array('table' => 'ec_product', 'field' => 'nid');
  }
  
  function render($value) {
    $node = node_load($value->{$this->aliases['nid']});
    $price = ec_product_get_final_price($node, 'product');
    return format_currency($price);
  }
}