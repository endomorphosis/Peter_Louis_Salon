<?php
// $Id: ec_product_handler_field_ptype.inc,v 1.1.2.3 2009/07/02 10:25:40 recidive Exp $

/**
 * @file
 * Implements display handler for product types.
 */

class ec_product_handler_field_ptype extends views_handler_field {
  function render($values) {
    if (isset($values->{$this->field_alias})) {
      $value = ec_product_ptypes_get('name', $values->{$this->field_alias});
      return check_plain($value);
    }
  }
}

