<?php
// $Id: ec_product.charge.inc,v 1.1.2.9 2009/07/02 10:25:39 recidive Exp $

/**
 * @file
 * Provides charge rules for ec_product module.
 */

/**
 * Implementation of hook_filter_process().
 */
function ec_product_shippable_filter_process($type, $settings, $object) {
  $$type =& $object;
  
  switch ($type) {
    case 'txn':
      return $txn->shippable ? TRUE : FALSE;
      break;
      
    case 'node':
      return ec_product_is_shippable($node->vid);
      break;
  }
}

/**
 * Implementation of hook_variable_process().
 */
function ec_product_shippable_variable_process($type, $settings, $object) {
  $$type =& $object;
  $qty = $value = 0;
  
  if (!$object)
    return;
  
  switch ($type) {
    case 'txn':
      $txn = drupal_clone($txn);

      $items = array_filter($txn->items, '_ec_product_shippable_filter');

      if (!empty($items)) {
        foreach ($items as $item) {
          $value+= ec_store_adjust_misc($txn, $item) * (ec_product_has_quantity($item) ? $item->qty : 1);
          $qty+= (ec_product_has_quantity($item) ? $item->qty : 1);
        }
      }
      break;
      
    case 'node':
      if (ec_product_is_shippable($node)) {
        $qty = ec_product_has_quantity($node) && isset($node->qty) ? $node->qty : 1;
        $value = ec_product_get_final_price($node, 'product') * $qty;
      }
      break;
  }

  return array('total' => $value, 'qty' => $qty);
}

function _ec_product_shippable_filter($a) {
  return $a->shippable;
}

function ec_product_sold_filter_form(&$form_state, $settings = array()) {
  $form = array();
  $settings+= array('operator' => NULL, 'value' => '');
  
  $form['operator'] = array(
    '#type' => 'select',
    '#title' => t('Operator'),
    '#default_value' => $settings['operator'],
    '#options' => array(
      '==' => t('Equal'),
      '<' => t('Less than'),
      '<=' => t('Less than equal'),
      '>' => t('Greater than'),
      '>=' => t('Greater than equal'),
    ),
  );
  
  $form['value'] = array(
    '#type' => 'textfield',
    '#title' => t('Number'),
    '#default_value' => $settings['value'],
    '#element_validate' => array('valid_number'),
    '#required' => TRUE,
  );
  
  return $form;
}

function ec_product_sold_filter_settings() {
  return array('operator', 'value');
}

function ec_product_sold_filter_process($type, $settings, $object) {
  static $nodes = array();
  
  $$type =& $object;
  
  switch ($type) {
    case 'node':
      if (!isset($nodes[$node->nid])) {
        $nodes[$node->nid] = db_result(db_query('SELECT SUM(qty) FROM {ec_transaction_product} WHERE nid = %d', $node->nid));
      }
      
      return @eval('return $nodes[$node->nid] '. $settings['operator'] .' '. $settings['value'] .';');
      break;
      
    case 'txn':
      // this will not work on transaction as which node do you choose.
      break;
  }
}

function ec_product_shippable_variable_description($type, $settings, $object, $variables) {
  return array(
    'total' => t('The value of all shippable products'),
    'qty' => t('The quantity of all the shippable products.'),
  );
}
