<?php
// $Id: ec_address.module,v 1.4.2.6 2009/08/07 13:03:50 gordon Exp $

/**
 * @file
 * Let customers to enter their address to receive their bills and shipping
 * items. Enable address books for users.
 */

/**
 * Implementation of menu_hook().
 */
function ec_address_menu() {
  $items = array();

  $items['ec_address/autocomplete'] = array(
    'page callback' => 'ec_address_autocomplete',
    'type' => MENU_CALLBACK,
    'access callback' => TRUE,
  );
  $items['admin/ecsettings/ctype/ec_address'] = array(
    'page callback' => 'drupal_get_form',
    'page arguments' => array('ec_address_admin'),
    'title' => 'Address',
    'type' => MENU_CALLBACK,
    'access callback' => 'user_access',
    'access arguments' => array(EC_PERM_SETTINGS),
    'file' => 'ec_address.admin.inc',
  );
  $items['user/%/ec_address/add'] = array(
    'title' => 'Add',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('ec_address_form', 1),
    'access callback' => 'ec_customer_check_access',
    'access arguments' => array('user', 1),
    'type' => MENU_LOCAL_TASK,
    'file' => 'ec_address.admin.inc',
  );
  $items['user/%/ec_address/%/edit'] = array(
    'title' => 'Edit',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('ec_address_form', 1, 3),
    'access callback' => 'ec_customer_check_access',
    'access arguments' => array('user', 1),
    'type' => MENU_CALLBACK,
    'file' => 'ec_address.admin.inc',
  );
  $items['user/%/ec_address/%/delete'] = array(
    'title' => 'Edit',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('ec_address_delete_confirm', 1, 3),
    'access callback' => 'ec_customer_check_access',
    'access arguments' => array('user', 1),
    'type' => MENU_CALLBACK,
    'file' => 'ec_address.admin.inc',
  );

  return $items;
}

/**
 * Get a given address.
 *
 * @param $aid
 *   Number, the address ID
 * @return
 *   Array, the address data
 */
function ec_address_address_load($aid) {
  return db_fetch_array(db_query('SELECT * FROM {ec_address}
    WHERE aid = %d', $aid));
}

/**
 * Get all addresses from a given user/customer.
 *
 * @param $uid
 *   Number (optional), the user ID
 * @return
 *   Array, the address data
 */
function ec_address_address_load_all($uid = NULL) {
  global $user;
  $address = array();

  $result = db_query('SELECT * FROM {ec_address}
    WHERE uid = %d', isset($uid) ? $uid : $user->uid);
  while ($data = db_fetch_object($result)) {
    $address[] = $data;
  }

  return $address;
}

/**
 * Create a list of states from a given country
 *
 * @param $country
 *   String, the country code
 * @param $string
 *   String (optional), the state name typed by user
 * @return
 *   Javascript array, list of states
 */
function ec_address_autocomplete($country, $string = '') {
  $string = drupal_strtolower($string);
  $string = '/^'. $string .'/';
  $matches = array();
  module_load_include('inc', 'ec_store', 'ec_store.localization');
  $states = _ec_store_location_states($country);

  $counter = 0;
  if (!empty($states)) {
    while (list($code, $name) = each($states)) {
      if ($counter < 5) {
        if (preg_match($string, drupal_strtolower($name))) {
          $matches[$code] = drupal_strtolower($name);
          ++$counter;
        }
      }
    }
  }
  echo drupal_to_js($matches);
  exit();
}

/**
 * Implementation of hook_customer_info().
 */
function ec_address_customer_info() {
  return array(
    'user' => array(
      'name' => t('User'),
      'description' => t('Attach addresses to Drupal users which can be used for transactions'),
      'module' => 'ec_address',
      'settings_path' => 'admin/ecsettings/ctype/ec_address',
      'weight' => 5,
      'file' => 'ec_address.customer.inc',
    ),
  );
}

/**
 * Implementation of hook_customer_search().
 */
function ec_address_customer_search($search_id, &$query, &$filter, &$details) {
  $query->ensure_table('ec_address', 'a', 'LEFT', "ec.type = 'user' AND ec.exid = a.uid");
  return "a.firstname LIKE '%%{$filter['data'][0]}%%' OR a.lastname LIKE '%%{$filter['data'][0]}%%'";
}

/**
 * The controller for managing addresses. Callback happens via menu().
 *
 * @param $action
 *   String (optional), the action to be done
 * @param $aid
 *   Number (optional), the Address ID
 */
function ec_address_page($action = NULL, $aid = NULL) {
  return views_embed_view('ec_address_list');
}

/**
 * Implementation of hook_user().
 */
function ec_address_user($op, &$edit, &$account, $category = NULL) {
  switch ($op) {
    case 'delete':
      db_query('DELETE FROM {ec_address} WHERE uid = %d', $account->uid);
      break;
  }
}

/**
 * Implementation of hook_views_api().
 */
function ec_address_views_api() {
  return array(
    'api' => 2,
    'path' => drupal_get_path('module', 'ec_address'),
  );
}
