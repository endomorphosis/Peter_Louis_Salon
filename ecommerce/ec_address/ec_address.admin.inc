<?php
// $Id: ec_address.admin.inc,v 1.4.2.5 2009/07/02 10:25:39 recidive Exp $

/**
 * @file
 * Address administration screens.
 */

/**
 * Module Settings
 *
 * @param $form
 *   Array, the Seetings form
 */
function ec_address_admin() {
  $form = array();

  // Get all countries
  module_load_include('inc', 'ec_store', 'ec_store.localization');
  $countries = _ec_store_location_countries();

  $form['ec_address_countries'] = array(
    '#default_value' => variable_get('ec_address_countries', array_keys($countries)),
    '#description'   => t('What countries your users can choose?'),
    '#multiple'      => TRUE,
    '#options'       => $countries,
    '#title'         => t('Countries'),
    '#type'          => 'select'
  );
  $form['ec_address_address2'] = array(
    '#default_value' => variable_get('ec_address_address2', TRUE),
    '#description'   => t('A second address line needed?'),
    '#title'         => t('Second Address Line'),
    '#type'          => 'checkbox'
  );
  $form['ec_address_zip'] = array(
    '#default_value' => variable_get('ec_address_zip', TRUE),
    '#description'   => t('The ZIP code is needed?'),
    '#title'         => t('ZIP code'),
    '#type'          => 'checkbox'
  );

  return system_settings_form($form);
}

/**
 * Insert the address fields to the form.
 *
 * @param $edit
 *   Array (optional), the current form array
 *
 * @ingroup form
 */
function ec_address_form($form_state, $uid = NULL, $aid = NULL) {
  global $user;

  $address = array('firstname' => '', 'lastname' => '', 'street1' => '', 'street2' => '', 'zip' => '', 'city' => '', 'state' => '', 'country' => variable_get('ec_country', 0));
  if (isset($aid)) {
    $address = db_fetch_array(db_query('SELECT * FROM {ec_address} WHERE aid = %d', $aid));
  }

  drupal_add_js(drupal_get_path('module', 'ec_address') .'/ec_address.js', 'module');

  module_load_include('inc', 'ec_store', 'ec_store.localization');
  $countries = _ec_store_location_countries();
  $countries_list = variable_get('ec_address_countries', $countries);
  foreach ($countries as $country_code => $country_name) {
    if (empty($countries_list[$country_code])) {
      unset($countries[$country_code]);
    }
  }
  array_unshift($countries, t('Please choose...'));
  $form['#address'] = $address;
  
  $form['firstname'] = array(
    '#type'           => 'textfield',
    '#title'          => t('First Name'),
    '#default_value'  => $address['firstname'],
    '#size'           => 50,
    '#maxlength'      => 75,
    '#required'       => TRUE
  );
  $form['lastname'] = array(
    '#type'           => 'textfield',
    '#title'          => t('Last Name'),
    '#default_value'  => $address['lastname'],
    '#size'           => 50,
    '#maxlength'      => 75,
    '#required'       => TRUE
  );
  $form['country'] = array(
    '#type'               => 'select',
    '#title'              => t('Country'),
    '#default_value'      => $address['country'],
    '#options'            => $countries,
    '#required'           => TRUE
  );
  // Use AJAX to autocomplete based on current country
  $form['state'] = array(
    '#autocomplete_path'  => 'ec_address/autocomplete',
    '#type'               => 'textfield',
    '#title'              => t('State / Province'),
    '#default_value'      => $address['state'],
    '#size'               => 30,
    '#maxlength'          => 64
  );
  $form['city'] = array(
    '#type'           => 'textfield',
    '#title'          => t('City'),
    '#default_value'  => $address['city'],
    '#size'           => 30,
    '#maxlength'      => 64,
    '#required'       => TRUE
  );
  $form['street1'] = array(
    '#type'           => 'textfield',
    '#title'          => t('Address Line 1'),
    '#default_value'  => $address['street1'],
    '#size'           => 50,
    '#maxlength'      => 75,
    '#required'       => TRUE
  );
  if (variable_get('ec_address_address2', TRUE)) {
    $form['street2'] = array(
      '#type'           => 'textfield',
      '#title'          => t('Address Line 2'),
      '#default_value'  => $address['street2'],
      '#size'           => 50,
      '#maxlength'      => 75
    );
  }
  if (variable_get('ec_address_zip', TRUE)) {
    $form['zip'] = array(
      '#type'           => 'textfield',
      '#title'          => t('Zip / Postal Code'),
      '#default_value'  => $address['zip'],
      '#size'           => 20,
      '#maxlength'      => 20
    );
  }

  if (isset($uid)) {
    $form['uid'] = array('#type' => 'value', '#value' => $uid);
  }

  if (!empty($address['aid'])) {
    $form['aid'] = array('#type' => 'value', '#value' => $address['aid']);
    $form['submit'] = array(
      '#type'   => 'submit',
      '#value'  => t('Update address'),
      '#weight' => 9,
      );
  }
  else {
    $form['submit'] = array(
      '#type'   => 'submit',
      '#value'  => t('Create new address'),
      '#weight' => 9,
    );
  }

  return $form;
}

/**
 * Validate an address being saved
 */
function ec_address_form_validate(&$form, &$form_state) {
  if ( variable_get('store_ignore_state', FALSE)) {
    return;
  }

  module_load_include('inc', 'ec_store', 'ec_store.localization');

  if (($states = _ec_store_location_states($form_state['values']['country'])) && empty($states[$form_state['values']['state']])) {
    form_set_error('state', t('Invalid state. Try to use the state code.'));
  }
}

/**
 * Submit an address being saved
 */
function ec_address_form_submit(&$form, &$form_state) {
  if (isset($form_state['values']['aid'])) {
    drupal_write_record('ec_address', $form_state['values'], 'aid');
  }
  else {
    drupal_write_record('ec_address', $form_state['values']);
  }

  drupal_set_message(t('The address has been saved.'));
  $form_state['redirect'] = "user/{$form_state['values']['uid']}/ec_address";
}

/**
 * Delete address from the system
 */
function ec_address_delete_confirm($form_state, $uid, $aid) {
  if ($address = db_fetch_array(db_query('SELECT * FROM {ec_address} WHERE uid = %d AND aid = %d', $uid, $aid))) {
    $form = array();
    $form['uid'] = array(
      '#type' => 'value',
      '#value' => $uid,
    );
    $form['aid'] = array(
      '#type' => 'value',
      '#value' => $aid,
    );

    return confirm_form($form, t('Do you really want to delete this address'), 'user/'. $uid .'/ec_address', theme('formatted_address', $address), t('Delete Address'));
  }
  else {
    drupal_not_found();
  }
}

function ec_address_delete_confirm_submit(&$form, &$form_state) {
  db_query('DELETE FROM {ec_address} WHERE aid = %d', $form_state['values']['aid']);
  drupal_set_message(t('Address deleted.'));
  $form_state['redirect'] = 'user/'. $form_state['values']['uid'] .'/ec_address';
}
