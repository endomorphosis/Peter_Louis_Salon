<div id="cart-view-page">
  <table class="sticky-enabled sticky-table">
    <thead>
      <th>Items</th>
      <th>Price</th>
      <th>Qty</th>
      <th>Total</th>
      <th><!-- Operations header --></th>
    </thead>
    <tbody>
      <?php $i = 0; ?>
      <?php foreach ($items as $nid => $item) { ?>
        <tr id="item-<?php echo $nid ?>" class="cart-item <?php echo (++$i/2 == intval($i/2)) ? 'even' : 'odd'; ?>">
          <td><?php echo $item['title']; ?></td>
          <td><?php echo $item['price']; ?></td>
          <td><?php echo $item['qty']; ?></td>
          <td><?php echo $item['total']; ?></td>
          <td><?php echo $item['ops']; ?></td>
        </tr>
      <?php } ?>
      <tr id="cart-final" class="<?php echo (++$i/2 == intval($i/2)) ? 'even' : 'odd'; ?>">
        <td colspan="3"></td>
        <td id="cart-total"><?php echo $total; ?></td>
        <td/>
      </tr>
    </tbody>
  </table>
  <?php echo $output; ?>
</div>