<?php
// $Id: ec_checkout.checkout.inc,v 1.1.2.9 2009/07/02 10:25:40 recidive Exp $

/**
 * @file
 * Provide checkout functionality to ec_checkout module.
 */

/**
 * Implementation of hook_checkout_form().
 */
function ec_checkout_checkout_form(&$form, &$form_state) {
  $txn =& $form_state['txn'];

  $txn->subtotal = 0;
  $txn->gross = $txn->balance = ec_store_transaction_calc_gross($txn);

  if (isset($txn->misc) && is_array($txn->misc)) {
    usort($txn->misc, 'ec_sort');
  }

  foreach ((array)$txn->items as $product) {
    $price = ec_store_adjust_misc($txn, $product);
    $node = node_load($product->nid);
    $subtotal = ec_product_has_quantity($node) ? ($price * $product->qty) : $price;

    $form['ec_checkout_review']['items'][$product->nid] = array(
      '#node' => array('#type' => 'value', '#value' => $product), // allow more info to be displayed by the theme
      'title' => array('#value' => $product->title),
      'qty' => array('#value' => ec_product_has_quantity($node) ? $product->qty : ''),
      'price' => array('#value' => format_currency($price)),
      'subtotal' => array('#value' => format_currency($subtotal)),
      'options' => array('#value' => $txn->type == 'ec_cart' ? l(t('Change'), 'cart/view') : '')
    );

    $txn->subtotal += $subtotal;
  }

  $form['ec_checkout_review']['totals'] = array();

  // Since we may remove items, keep a track of the current key.
  $line = 0;

  // Keep track of which lines are subtotals.
  $st = array('#row_type' => 'ST');

  // Create a subtotal line.
  // If the Total comes immediately afterwards, then it will be repressed later.
  $form['ec_checkout_review']['totals'][++$line] = array('#title' => t('Subtotal'), '#value' => $txn->subtotal, 'info' => $st);

  if (!empty($txn->misc)) {
    uasort($txn->misc, 'ec_sort');
    foreach ($txn->misc as $key => $misc) {
      if (empty($txn->misc[$key]->seen)) {
        // Only add the subtotal line if there are other items in misc.
        if (!empty($misc->subtotal_before) && ($form['ec_checkout_review']['totals'][$line-1]['info']['#row_type'] != $st['#row_type'])) {
          $form['ec_checkout_review']['totals'][++$line] = array('#title' => t('Subtotal'), '#value' => $txn->subtotal, 'info' => $st);
        }
      }

      // Here we calculate the misc item.
      if (!empty($misc->callback) and function_exists($misc->callback)) {
        $f = $misc->callback;
        $amount = $f($txn, $misc, $txn->subtotal);
        // Apply the total of this charge to the transaction object for saving
        // later.
        $txn->misc[$key]->price = $amount;
      }
      elseif (!empty($misc->qty)) {
        $amount = ($misc->price * $misc->qty);
      }
      else {
        $amount = $misc->price;
      }
      if (empty($misc->already_added)) {
        $txn->subtotal+= $amount;
      }


      if (empty($txn->misc[$key]->seen)) {
        // Misc Item goes here.
        $form['ec_checkout_review']['totals'][++$line] = array('#title' => $misc->description, '#value' => $amount);

        // Subtotal - check straight after the misc item.
        if (!empty($misc->subtotal_after)) {
          $form['ec_checkout_review']['totals'][++$line] = array('#title' => t('Subtotal'), '#value' => $txn->subtotal, 'info' => $st);
        }
      }
    }
  }

  // Remove any subtotal occurring before the total.
  if (!empty($form['ec_checkout_review']['totals'][$line-1]['info']['#row_type']) and $form['ec_checkout_review']['totals'][$line-1]['info']['#row_type'] == $st['#row_type']) {
    unset($form['ec_checkout_review']['totals'][$line-1]);
  }
  // Grand total
  $form['ec_checkout_review']['totals'][] = array('#title' => t('Total'), '#value' => $txn->gross);
  $form['ec_checkout_review']['#theme'] = 'ec_checkout_checkout_review_form';
}
