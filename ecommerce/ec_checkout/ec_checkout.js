// $Id: ec_checkout.js,v 1.1.2.4 2009/10/04 02:48:46 gordon Exp $

Drupal.behaviors.ecCheckout = function() {
  $('#ec-checkout-form input, #ec-checkout-form select').not('.ignore-update').change(function() {
    if (!$('#edit-order').attr('disabled')) {
      $('#edit-order').attr('disabled', true).parent().before('<div class="warning" style="display: block;"><span class="warning">*</span> '+ Drupal.t('Checkout has been changed. Please update the order to ensure the totals are correct.') +'</div>');
    }
  });
};
