<?php
// $Id: ec_checkout.theme.inc,v 1.2.2.5 2009/05/25 01:21:41 gordon Exp $

/**
 * @file
 * Handle all checkout theme items.
 */

function theme_ec_checkout_checkout_review_form(&$form) {
  $header = array(t('Item'), t('Qty'), t('Price'), t('Subtotal'), '');
  $rows = array();
  if (!empty($form['items'])) {
    foreach ($form['items'] as $nid => $line) {
      if (is_numeric($nid)) {
        $rows[] = array(
          drupal_render($form['items'][$nid]['title']),
          drupal_render($form['items'][$nid]['qty']),
          array('data' => drupal_render($form['items'][$nid]['price']), 'align' => 'right'),
          array('data' => drupal_render($form['items'][$nid]['subtotal']), 'align' => 'right'),
          drupal_render($form['items'][$nid]['options']),
        );
      }
    }
  }

  $rows[] = array('', '', '', '', '');
  foreach ($form['totals'] as $id => $line) {
    if (is_numeric($id)) {
      $rows[] = array(
        "<b>{$line['#title']}</b>",
        '',
        '',
        array('data' => isset($line['#value']) ? format_currency($line['#value']) : '', 'align' => 'right'),
        ''
      );
      drupal_render($form['totals'][$id]);
    }
  }

  $content = theme('table', $header, $rows);

  return theme('box', t('Order Summary'), $content);
}

/**
 * Themes the admin screen.
 * @ingroup themeable
 */
function theme_ec_checkout_admin_screen_form($form) {
  drupal_add_tabledrag('screen-table', 'order', 'sibling', 'screen-weight', 'screen-weight');
  $output = '';
  $header = array(t('Type'), t('Description'), t('Weight'));

  foreach (element_children($form) as $type) {
    $rows[] = array(
      'data' => array(
        drupal_render($form[$type]['name']),
        drupal_render($form[$type]['description']),
        drupal_render($form[$type]['weight']),
      ),
      'class' => 'draggable',
    );
  }

  $output .= theme('table', $header, $rows, array('id' => 'screen-table'));
  $output .= drupal_render($form);

  return $output;
}
