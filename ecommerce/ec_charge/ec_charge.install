<?php
// $Id: ec_charge.install,v 1.1.4.10 2009/07/02 10:25:40 recidive Exp $

/**
 * @file
 * Install and uninstall routines, incremental database updates and database
 * schema.
 */

/**
 * Implementation of hook_install().
 */
function ec_charge_install() {
  drupal_install_schema('ec_charge');
}

/**
 * Implementation of hook_install().
 */
function ec_charge_uninstall() {
  drupal_uninstall_schema('ec_charge');
}

/**
 * Implementation of hook_schema().
 */
function ec_charge_schema() {
  $schema['ec_charge'] = array(
    'description' => t('Store of miscellenous charges'),
    'fields' => array(
      'chgid' => array(
        'type' => 'serial',
        'not null' => TRUE,
        'disp-width' => '11',
        'description' => t('Primary Key: Unique ID for each charge'),
      ),
      'name' => array(
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
        'description' => t('Unique identifier for each charge'),
      ),
      'type' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'description' => t('Indicates the charge type, transaction/product'),
      ),
      'chg_group' => array(
        'type' => 'varchar',
        'not null' => TRUE,
        'length' => 16,
        'default' => '',
      ),
      'description' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
        'description' => t('Description of the charge that will appear on the transaction'),
      ),
      'weight' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'description' => t('Weight of the charge to determine the order'),
      ),
      'enabled' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'size' => 'tiny',
        'description' => t('Set the charged as enabled.'),
      ),
    ),
    'primary key' => array('chgid'),
  );

  $schema['ec_charge_components'] = array(
    'fields' => array(
      'id' => array(
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'chgid' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'type' => array(
        'type' => 'varchar',
        'length' => '16',
        'not null' => TRUE,
      ),
      'component' => array(
        'type' => 'varchar',
        'length' => '16',
        'not null' => TRUE,
      ),
      'name' => array(
        'type' => 'varchar',
        'length' => '64',
        'not null' => TRUE,
      ),
      'weight' => array(
        'type' => 'int',
        'not null' => TRUE,
      ),
      'parent' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'data' => array(
        'type' => 'text', 
        'size' => 'big',
        'not null' => TRUE,
        'serialize' => TRUE,
      ),
    ),
    'primary key' => array('id'),
    'indexes' => array(
      'chgid' => array('chgid')
    ),
  );

  return $schema;
}

/**
 * Rename componet to component in all tables and columns.
 */
function ec_charge_update_6401() {
  $ret = array();
  
  db_rename_table($ret, 'ec_charge_componets', 'ec_charge_components');
  db_change_field($ret, 'ec_charge_components', 'componet', 'component', array('type' => 'varchar', 'length' => '16', 'not null' => TRUE));

  return $ret;
}

/**
 * Add type field to ec_charge table
 */
function ec_charge_update_6402() {
  $ret = array();
  
  db_add_field($ret, 'ec_charge', 'type', 
    array(
      'type' => 'int',
      'not null' => TRUE,
      'length' => 1,
      'default' => 0,
      'description' => t('Indicates the charge type, transaction/product'),
    )
  );
  
  db_add_field($ret, 'ec_charge', 'chg_group',
    array(
      'type' => 'varchar',
      'not null' => TRUE,
      'length' => 16,
      'default' => '',
    )
  );

  return $ret;
}

function ec_charge_update_6403() {
  $ret = array();
  
  db_add_field($ret, 'ec_charge_components', 'parent', array(
    'type' => 'int',
    'unsigned' => TRUE,
    'not null' => TRUE,
    'default' => 0,
  ));
  
  return $ret;
}