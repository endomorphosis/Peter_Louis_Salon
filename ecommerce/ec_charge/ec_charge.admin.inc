<?php
// $Id: ec_charge.admin.inc,v 1.1.2.33 2009/07/02 10:25:40 recidive Exp $

/**
 * @file
 * Provide administration functions for ec_charge module.
 */

/**
 *
 */
function ec_charge_admin_list() {
  $form = array();

  $delta = db_result(db_query('SELECT COUNT(*) FROM {ec_charge}'));
  $delta = round($delta/2);
  $result = db_query('SELECT * FROM {ec_charge} ORDER BY weight ASC');

  if (!$result) {
    drupal_goto('admin/ecsettings/charge/add');
  }
  
  $form['chg'] = array(
    '#tree' => TRUE,
    '#theme' => 'ec_charge_admin_list_table',
  );
  while ($chg = db_fetch_array($result)) {
    $form['chg'][$chg['chgid']]['chgid'] = array(
      '#type' => 'value',
      '#value' => $chg['chgid'],
    );
    $form['chg'][$chg['chgid']]['name'] = array(
      '#value' => $chg['name'],
    );
    $form['chg'][$chg['chgid']]['description'] = array(
      '#value' => $chg['description'],
    );
    $form['chg'][$chg['chgid']]['type'] = array(
      '#value' => $chg['type'] ? t('Product') : t('Transaction'),
    );
    $form['chg'][$chg['chgid']]['enabled'] = array(
      '#value' => $chg['enabled'] ? t('Yes') : t('No'),
    );
    $form['chg'][$chg['chgid']]['weight'] = array(
      '#type' => 'weight',
      '#default_value' => $chg['weight'],
      '#delta' => $delta,
    );
    $form['chg'][$chg['chgid']]['operations'] = array(
      '#value' => l(t('edit'), 'admin/ecsettings/charge/'. $chg['chgid'] .'/edit') .' '. l(t('delete'), 'admin/ecsettings/charge/'. $chg['chgid'] .'/delete'),
    );
    $form['chg'][$chg['chgid']]['operations']['#value'].= ' '. l(t('export'), 'admin/ecsettings/charge/'. $chg['chgid'] .'/export');
    if ($chg['enabled']) {
      $form['chg'][$chg['chgid']]['operations']['#value'].= ' '. l(t('disable'), 'admin/ecsettings/charge/'. $chg['chgid'] .'/disable');
    }
    else {
      $form['chg'][$chg['chgid']]['operations']['#value'].= ' '. l(t('enable'), 'admin/ecsettings/charge/'. $chg['chgid'] .'/enable');
    }
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update'),
  );

  return $form;
}

function ec_charge_admin_list_submit(&$form, &$form_state) {
  foreach ($form_state['values']['chg'] as $record) {
    drupal_write_record('ec_charge', $record, 'chgid');
  }

  drupal_set_message(t('The configuration options have been saved.'));
}

/**
 *
 */
function ec_charge_admin_edit_controller(&$form_state, $chg = array(), $command = 'edit') {
  $form = array();

  if (isset($form_state['storage']['charge_type'])) {
    $info = ec_charge_get_info($form_state['storage']['charge_type'], 'type', $form_state['storage']['component']);
    drupal_set_title(t('@operation @name @charge_type', array('@charge_type' => $form_state['storage']['charge_type'], '@operation' => ($form_state['storage']['operation'] === 'add' ? t('Add') : t('Edit')), '@name' => $info['name'])));

    $settings =& $form_state['storage']['settings'];

    if ($function = ec_charge_get_callback_function($form_state['storage']['charge_type'], $form_state['storage']['component'], 'form')) {
      $form = $function($form_state, $settings);
    }

    $default = array();
    if (isset($settings['id'])) {
      $default['id'] = array(
        '#type' => 'value',
        '#value' => $settings['id']
      );
    }
    $default['component'] = array(
      '#type' => 'value',
      '#value' => $settings['component']
    );
    $default['name'] = array(
      '#type' => 'textfield',
      '#title' => t('Name'),
      '#default_value' => isset($settings['name']) ? $settings['name'] : $info['name'],
      '#weight' => -10,
    );
    $default['weight'] = array(
      '#type' => 'value',
      '#value' => isset($settings['weight']) ? $settings['weight'] : ec_charge_get_component_weight($form_state['storage']['chg'], $form_state['storage']['charge_type']),
    );
    if ($form_state['storage']['charge_type'] == 'filter') {
      $default['reverse'] = array(
        '#type' => 'checkbox',
        '#title' => t('Reverse the outcome of this filter.'),
        '#default_value' => isset($settings['reverse']) ? $settings['reverse'] : 0,
      );
    }
    $default['submit'] = array(
      '#type' => 'submit',
      '#value' => $form_state['storage']['operation'] === 'add' ? t('Add') : t('Update'),
      '#submit' => array('ec_charge_admin_edit_component_submit'),
    );
    $default['cancel'] = array(
      '#type' => 'submit',
      '#value' => t('Cancel'),
      '#submit' => array('ec_charge_admin_edit_component_cancel'),
    );
    $default['#validate'] = array('ec_charge_admin_edit_component_validate');

    $form+= $default;
  }
  else if ($command == 'import' && empty($form_state['storage'])) {
    $form = ec_charge_admin_import_form($form_state);
  }
  else {
    $form = ec_charge_admin_edit_form($form_state, $chg);
  }
  return $form;
}

function ec_charge_admin_edit_component_validate(&$form, &$form_state) {
  if ($function = ec_charge_get_callback_function($form_state['storage']['charge_type'], $form_state['storage']['component'], 'validate')) {
    $function($form, $form_state);
  }
}

function ec_charge_admin_edit_component_submit(&$form, &$form_state) {
  if ($function = ec_charge_get_callback_function($form_state['storage']['charge_type'], $form_state['storage']['component'], 'submit')) {
    $function($form, $form_state);
  }

  $type = $form_state['storage']['charge_type'];
  if ($form_state['storage']['operation'] === 'add') {
    $form_state['values']['new'] = 1;
    $form_state['storage']['chg'][$type .'s'][] = $form_state['values'];
  }
  else {
    $form_state['storage']['chg'][$type .'s'][$form_state['storage']['operation']] = $form_state['values'];
  }

  unset($form_state['storage']['charge_type'], $form_state['storage']['operation'], $form_state['storage']['component'], $form_state['storage']['settings']);
  $form_state['rebuild'] = TRUE;
}

function ec_charge_admin_edit_component_cancel(&$form, &$form_state) {
  unset($form_state['storage']['charge_type'], $form_state['storage']['operation'], $form_state['storage']['component'], $form_state['storage']['settings']);
  $form_state['rebuild'] = TRUE;
}

/**
 * Import charge from another system
 */
function ec_charge_admin_import_form(&$form_state) {
  $form = array();
  
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
  );
  
  $form['import'] = array(
    '#type' => 'textarea',
    '#title' => t('Import'),
    '#rows' => 20,
    '#required' => TRUE,
  );
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Import'),
    '#submit' => array('ec_charge_admin_import_form_submit'),
  );
  
  $form['#validate'] = array('ec_charge_admin_import_form_validate');
  
  return $form;
}

function ec_charge_admin_import_form_validate(&$form, &$form_state) {
  $import = $form_state['values']['import'];
  
  if (empty($import)) {
    return;
  }
  
  if (preg_replace('/[a-z0-9_]/i', '', $form_state['values']['name'])) {
    form_set_error('name', t('The name field can only contain alphanumeric characters and underscores'));
  }
  
  if (substr($import, 0, 7) != 'array (' || substr($import, -1) != ')') {
    form_set_error('', t('Invalid import data. Be sure to paste the data exactly as it was present in the export window.'));
    return;
  }
  $chg = eval('return '. $import .';');
  if (is_array($chg)) {
    if (!empty($form_state['values']['name'])) {
      $chg['name'] = $form_state['values']['name'];
    }
  
    if (ec_charge_load(NULL, $chg['name'])) {
      form_set_error('name', t('The name of the new charge already exists, please use a different name.'));
    }
    
    $errorno = 0;
    foreach (array('filters', 'variables', 'calculations', 'modifications') as $types) {
      foreach ($chg[$types] as $info) {
        $type = substr($types, 0, -1);
        if (!ec_charge_get_info($type, 'type', $info['component'])) {
          form_set_error('componet_'. $errorno++, t('@type %name cannot be found. Make sure the correct modules are installed.', array('@type' => drupal_ucfirst($type), '%name' => $info['component'])));
        }
      }
    }
  
    form_set_value($form['import'], $chg, $form_state);
  }
}

function ec_charge_admin_import_form_submit(&$form, &$form_state) {
  $form_state['storage']['chg'] = $form_state['values']['import'];
}

/**
 * Default main form for adding of charges.
 */
function ec_charge_admin_edit_form(&$form_state, $chg = array()) {
  drupal_add_css(drupal_get_path('module', 'ec_charge') .'/ec_charge.admin.css', 'module');
  $default = array('name' => '', 'description' => '', 'type' => 0, 'chg_group' => '',
    'variables' => array(), 'filters' => array(), 'calculations' => array(),
    'modifications' => array(), 'enabled' => 0,
  );
  
  $form = array();

  $chg+= $default;

  if (!isset($form_state['storage']['chg'])) {
    $form_state['storage']['chg'] = $chg;
  }
  $chg =& $form_state['storage']['chg'];

  if (!empty($chg->chgid)) {
    $form['chgid'] = array(
      '#type' => 'value',
      '#value' => $chg['chgid'],
    );
  }

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#default_value' => $chg['name'],
    '#description' => t('Enter a unique id which contains only letters, numbers and underscores "_", this name can be used to to reference the charge as a variable'),
    '#required' => TRUE,
    '#size' => 32,
    '#max_length' => 32,
  );

  $form['description'] = array(
    '#type' => 'textfield',
    '#title' => t('Description'),
    '#default_value' => $chg['description'],
    '#description' => t('Enter a description which will be displayed on the invoice'),
    '#required' => TRUE,
  );
  
  $form['type'] = array(
    '#type' => 'select',
    '#title' => t('Charge type'),
    '#default_value' => $chg['type'],
    '#options' => array(
      0 => t('Transaction'),
      1 => t('Product'),
    ),
    '#description' => t('Choose where this charge is going to be applied to the transaction. If you choose transaction the charge will be added as an additional line item on the invoice, or a product is where the price will be included in the total price of the product.'),
  );
  
  $form['chg_group'] = array(
    '#type' => 'textfield',
    '#title' => theme('advanced_help_topic', 'ec_charge', 'charge-group') . t('Transaction group'),
    '#default_value' => $chg['chg_group'],
  );

  $form['subform'] = array(
    '#value' => '<div id="sub-form"></div>',
  );

  $form['right'] = array(
    '#prefix' => '<div id="right-container">',
    '#suffix' => '</div>',
  );

  $form['left'] = array(
    '#prefix' => '<div id="left-container">',
    '#suffix' => '</div><span class="clear" />',
  );

  ec_charge_add_filter_form($form, $form['left'], $chg);
  ec_charge_add_variable_form($form, $form['left'], $chg);
  ec_charge_add_calc_form($form, $form['right'], $chg);
  ec_charge_add_modification_form($form, $form['right'], $chg);

  if (empty($chg['chgid'])) {
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Create'),
      '#submit' => array('ec_charge_admin_edit_form_submit'),
    );
  }
  else {
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Update'),
      '#submit' => array('ec_charge_admin_edit_form_submit'),
    );
    $form['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#submit' => array('ec_charge_admin_edit_form_delete'),
    );
  }
  $form['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
    '#submit' => array('ec_charge_admin_edit_form_cancel'),
  );

  $form['#validate'][] = 'ec_charge_admin_edit_form_validate';

  return $form;
}

function ec_charge_admin_edit_form_validate(&$form, &$form_state) {
  if (preg_replace('/[a-z0-9_]/i', '', $form_state['values']['name'])) {
    form_set_error('name', t('The name field can only contain alphanumeric characters and underscores'));
  }
}

function ec_charge_admin_edit_form_submit(&$form, &$form_state) {
  ec_charge_admin_update_charge_form($form_state);

  $chg =& $form_state['storage']['chg'];
  ec_charge_save_charge($chg);

  unset($form_state['storage']);
  $form_state['redirect'] = 'admin/ecsettings/charge';
}

function ec_charge_admin_edit_form_cancel(&$form, &$form_state) {
  unset($form_state['storage']);
  $form_state['redirect'] = 'admin/ecsettings/charge';
}

function ec_charge_admin_edit_form_delete(&$form, &$form_state) {
  $chg =& $form_state['storage']['chg'];
  $form_state['redirect'] = 'admin/ecsettings/charge/'. $chg['chgid'] .'/delete';
  unset($form_state['storage']);
}

/**
 * Build the form to create filters
 */
function ec_charge_add_filter_form(&$form, &$form_location, $chg) {
  $form_location['filters'] = array(
    '#tree' => TRUE,
  );
  ec_charge_add_form($form, $form_location['filters'], 'filter', $chg['filters'], t('Filters are used to determine if a charge is going to be added to transaction. Multiple filters can be added and in such a case when this is done the conditions need to be meet by all the filters to be a valid charge on this transaction.'));
}

function ec_charge_add_variable_form(&$form, &$form_location, $chg) {
  $form_location['variables'] = array(
    '#tree' => TRUE,
  );
  ec_charge_add_form($form, $form_location['variables'], 'variable', $chg['variables'], t('Variables are used to calculate initial figures for use with calculations. Variables can be made up of different cross sections of the transaction or any other basic parameters in the system.'));
}

function ec_charge_add_calc_form(&$form, &$form_location, $chg) {
  $form_location['calculations'] = array(
    '#tree' => TRUE,
  );
  ec_charge_add_form($form, $form_location['calculations'], 'calculation', $chg['calculations'], t('Calculations do all the heavy lifting in the creating of charges. These can as simple of complex as needed and the value of an individual transaction will flow onto the next.'));
}

function ec_charge_add_modification_form(&$form, &$form_location, $chg) {
  $form_location['modifications'] = array(
    '#tree' => TRUE,
  );
  ec_charge_add_form($form, $form_location['modifications'], 'modification', $chg['modifications'], t('Modifications are used to make changes to the charges that are calculated, to modify how they are going to be displayed on the transaction.'));
}

function ec_charge_add_form(&$form, &$form_location, $type, $params, $description) {
  static $parents = array();
  
  if (!isset($parents[$type])) {
    $parents[$type] = ec_charge_component_get_parents($type, $params);
  }
  
  $form_location['#theme'] = 'ec_charge_component_form';
  $form_location['#ctype'] = $type;
  foreach ($params as $key => $param) {
    if (!isset($param['delete']) || !$param['delete']) {
      $info = ec_charge_get_info($type, 'type', $param['component']);
      $form_location[$key]['#parent'] = isset($info['parent']) ? $info['parent'] : FALSE;
      $form_location[$key]['name'] = array(
        '#value' => (isset($param['parent']) && $param['parent'] ? theme('indentation') : '') . $param['name'],
      );
      if ($type != 'variable') {
        if ($parents[$type]) {
          $form_location[$key]['id'] = array(
            '#type' => 'hidden',
            '#value' => $key,
            '#attributes' => array(
              'class' => $type .'-id',
            ),
          );
          $form_location[$key]['parent'] = array(
            '#type' => 'hidden',
            '#default_value' => isset($param['parent']) ? $param['parent'] : '',
            '#attributes' => array(
              'class' => $type .'-parent',
            ),
          );
        }
      
        $form_location[$key]['weight'] = array(
          '#type' => 'weight',
          '#default_value' => $param['weight'],
        );
      }
      if (!isset($info['no form']) || !$info['no form']) {
        $form_location[$key]['operations']['edit'] = array(
          '#type' => 'submit',
          '#name' => 'edit-'. $key .'-'. $type,
          '#value' => 'edit',
          '#charge_type' => $type,
          '#operation' => $key,
          '#submit' => array('ec_charge_admin_edit_form_component'),
        );
      }
      $form_location[$key]['operations']['delete'] = array(
        '#type' => 'submit',
        '#name' => 'delete-'. $key .'-'. $type,
        '#value' => 'delete',
        '#charge_type' => $type,
        '#key' => $key,
        '#submit' => array('ec_charge_admin_delete_form_component'),
      );
    }
  }

  $options = ec_charge_get_info($type, 'names');
  $form_location['new'] = array(
    '#prefix' => '<div id="new-'. $type .'-param" class="new-param container-inline">',
    '#suffix' => '</div>',
    '#tree' => FALSE,
  );
  $form_location['new']['new_'. $type] = array(
    '#type' => 'select',
    '#options' => $options,
  );
  $form_location['new']['new_'. $type .'_submit'] = array(
    '#type' => 'submit',
    '#name' => 'add-'. $type,
    '#value' => t('Add'),
    '#charge_type' => $type,
    '#operation' => 'add',
    '#submit' => array('ec_charge_admin_edit_form_component'),
  );

  $form_location['description'] = array(
    '#prefix' => '<div class="description">',
    '#suffix' => '</div>',
    '#value' => $description,
  );

  ec_charge_load_js($form_location);
}

function ec_charge_component_form_render_row($type, &$form) {
  $row = array();
  $row[] = drupal_render($form['name']);
  if ($type != 'variable') {
    $row[] = drupal_render($form['weight']) . (isset($form['id']) ? drupal_render($form['id']) : '') . (isset($form['parent']) ? drupal_render($form['parent']) : '');
  }
  $row[] = drupal_render($form['operations']);

  return $row;
}

function ec_charge_load_js(&$form) {
  static $loaded = FALSE;
  if (empty($form['#ctype'])) return;

  $type = $form['#ctype'];
  $keys = _ec_charge_get_table_ids($form);
  $weight = ($type == 'variable' ? 0 : 1);
  $parent = NULL;

  $form['new']['new_'. $type]['#attributes']['class'] = $type .'-select';
  $form['new']['new_'. $type .'_submit']['#attributes']['class'] = $type .'-add';

  if (!empty($keys)) {
    foreach ($keys as $key) {
      $form[$key]['weight']['#attributes']['class'] = $type .'-weight';
      $parent = isset($parent) ? $parent : (isset($form[$key]['parent']) ? TRUE : FALSE); 
    }
  }

  if ($weight) {
    drupal_add_tabledrag($type .'-table', 'order', 'sibling', $type .'-weight');
    if ($parent) {
      drupal_add_tabledrag($type .'-table', 'match', 'parent', $type .'-parent', $type .'-parent', $type .'-id');
    }
  }
}

function _ec_charge_get_table_ids($form) {
  $ids = element_children($form);
  return array_filter($ids, '_ec_charge_table_filter');
}

function _ec_charge_table_filter($a) {
  return ($a !== 'new' && $a !== 'description') ? TRUE : FALSE;
}

function ec_charge_admin_edit_form_component(&$form, &$form_state) {
  ec_charge_admin_update_charge_form($form_state);

  $type = $form_state['storage']['charge_type'] = $form_state['clicked_button']['#charge_type'];
  $form_state['storage']['operation'] = $form_state['clicked_button']['#operation'];
  
  if ($form_state['storage']['operation'] === 'add') {
    $info = ec_charge_get_info($type, 'type', $form_state['values']['new_'. $form_state['storage']['charge_type']]);
    if ($info['no form']) { 
      $form_state['storage']['chg'][$type .'s'][] = array(
        'name' => $info['name'],
        'type' => $type,
        'component' => $form_state['values']['new_'. $type],
        'new' => 1,
      );
      unset($form_state['storage']['operation'], $form_state['storage']['charge_type']);
    }
    else {
      $form_state['storage']['component'] = $form_state['values']['new_'. $form_state['storage']['charge_type']];
      $form_state['storage']['settings'] = array('component' => $form_state['storage']['component']);
    }
  }
  else {
    $form_state['storage']['settings'] = $form_state['storage']['chg'][$type .'s'][$form_state['storage']['operation']];
    $form_state['storage']['component'] = $form_state['storage']['settings']['component'];
  }

  $form_state['rebuild'] = TRUE;
}

function ec_charge_admin_delete_form_component(&$form, &$form_state) {
  $type = $form_state['clicked_button']['#charge_type'];
  $key = $form_state['clicked_button']['#key'];
  $form_state['storage']['chg'][$type .'s'][$key]['delete'] = TRUE;
  $form_state['rebuild'] = TRUE;
}

function ec_charge_get_component_weight($chg, $type) {
  $weights = array_map('_ec_charge_get_component_weight', $chg[$type .'s']);
  return empty($weights) ? 0 : max($weights)+1;
}

function _ec_charge_get_component_weight($a) {
  return isset($a['weight']) ? $a['weight'] : 0;
}

function ec_charge_admin_update_charge_form(&$form_state) {
  $table = drupal_get_schema('ec_charge');
  
  foreach ($table['fields'] as $field => $info) {
    if (isset($form_state['values'][$field])) {
      $form_state['storage']['chg'][$field] = $form_state['values'][$field];
    }
  }

  foreach (array('filters', 'variables', 'calculation') as $type) {
    if (!empty($form_state['values'][$type])) {
      foreach ($form_state['values'][$type] as $key => $component) {
        $form_state['storage']['chg'][$type][$key] = array_merge($form_state['storage']['chg'][$type][$key], $component);
      }
    }
  }
}


function ec_charge_admin_delete_confirm(&$form_state, $chg) {
  $form = array();
  $form['chg'] = array(
    '#type' => 'value',
    '#value' => $chg,
  );

  return confirm_form(
    $form,
    t('Are you sure you want to delete charge %name', array('%name' => $chg['name'])),
    'admin/ecsettings/charge',
    t('This charge will be deleted and cannot be undone'),
    t('Delete'),
    t('Cancel')
  );
}

function ec_charge_admin_delete_confirm_submit(&$form, &$form_state) {
  $chg = $form_state['values']['chg'];

  ec_charge_delete($chg['chgid']);
  drupal_set_message(t('Charge %name has been deleted.', array('%name' => $chg['name'])));
  $form_state['redirect'] = 'admin/ecsettings/charge';
}

function ec_charge_admin_enable_confirm(&$form_state, $chg) {
  $form = array();
  $form['chg'] = array(
    '#type' => 'value',
    '#value' => $chg,
  );

  return confirm_form(
    $form,
    t('Are you sure you want to enable charge %name', array('%name' => $chg['name'])),
    'admin/ecsettings/charge',
    t('This charge will be applied to all transactions from now onwards'),
    t('Enable'),
    t('Cancel')
  );
}

function ec_charge_admin_enable_confirm_submit(&$form, &$form_state) {
  $chg =& $form_state['values']['chg'];
  $chg['enabled'] = 1;
  ec_charge_save_charge($chg);
  drupal_set_message(t('Charge %name has been enabled', array('%name' => $chg['name'])));

  $form_state['redirect'] = 'admin/ecsettings/charge';
}

function ec_charge_admin_disable_confirm(&$form_state, $chg) {
  $form = array();
  $form['chg'] = array(
    '#type' => 'value',
    '#value' => $chg,
  );

  return confirm_form(
    $form,
    t('Are you sure you want to disable charge %name', array('%name' => $chg['name'])),
    'admin/ecsettings/charge',
    t('This charge will not be applied to all transactions from now onwards'),
    t('Disable'),
    t('Cancel')
  );
}

function ec_charge_admin_disable_confirm_submit(&$form, &$form_state) {
  $chg =& $form_state['values']['chg'];
  $chg['enabled'] = 0;
  ec_charge_save_charge($chg);
  drupal_set_message(t('Charge %name has been disabled', array('%name' => $chg['name'])));

  $form_state['redirect'] = 'admin/ecsettings/charge';
}

function ec_charge_admin_export($form_state, $chg) {
  $form = array();
  
  unset($chg['chgid']);
  foreach (array('filters', 'variables', 'calculations', 'modifications') as $type) {
    $chg[$type] = array_map('_ec_charge_export_clean', $chg[$type]);
  }
  
  $form['export'] = array(
    '#type' => 'textarea',
    '#value' => var_export($chg, TRUE),
    '#rows' => 20, 
  );
  
  return $form;
}

function _ec_charge_export_clean($a) {
  unset($a['id'], $a['chgid']);
  return $a;
}
