<table id="table2" border="0" cellpadding="5" cellspacing="0" width="840">
<tbody>
<tr>
<td colspan="2">
<table id="table1" cellspacing="0" cellpadding="4" width="100%" border="0"> 
<tbody> 
<tr valign="top"> 
<td colspan="2">
			<?php include'sub-menue.php';?>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<table id="table2" bgcolor="#e9e9e9" border="0" cellpadding="5" cellspacing="0" width="840">
<tr>
<td colspan="2" align="right" bgcolor="#003366">  
<center>
    <font class="bodytext-alt"><b><font color="#FFFFFF">Where does all the hair for hair extensions really come from?</font></b>      </font>
</center>
  
</td>
</tr>
<tr>
<td width="250">
  <font style="font-size: 9pt;" face="Verdana">
  <img alt="" src="greatlengths/media/bionda.jpg" border="0" height="324" width="250">
  </font>
</td>
<td>
  <font class="bodytext" style="font-size: 9pt;" face="Verdana">So you want to change the way you look&nbsp;? </font>
  <p><font class="bodytext" style="font-size: 9pt;" face="Verdana">Do you have short or damaged hair and would like long or thicker, healthy hair with beautiful volume? </font></p>
  <p><font class="bodytext" style="font-size: 9pt;" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;<b>No problem!</b> </font></p>
  <p><font class="bodytext" style="font-size: 9pt;" face="Verdana">With new technology which is now available, a reputable and high quality hair extension system may be the answer to your needs. </font></p>
  <p><font class="bodytext" style="font-size: 9pt;" face="Verdana">To provide this service, high quality hair is vital in ensuring perfect and lasting results and enormous quantities of human hair are needed to satisfy the ever-increasing hair extension market. </font></p>
  <p><font class="bodytext" style="font-size: 9pt;" face="Verdana"><b>Yet, have you ever wondered where all this hair comes from?</b></font></p>
</td>
</tr>
<tr>
<td colspan="2" valign="top">
  <p align="justify"><font style="font-size: 9pt;" face="Verdana"><font class="bodytext">This booklet is intended to provide answers and facts to the often misleading information publicised about the sources of hair. It is designed to help you ask the right questions about your new hair extensions...<br>...so that you are given the correct answers.</font> </font></p>
  <p align="justify"><font style="font-size: 9pt;" face="Verdana"><font class="bodytext"><b>You have the right to know what makes the difference in making the correct choice for your hair extensions. Your new hairstyle must not only look, but more importantly must become part of your everyday lifestyle without causing the problems so often associated with this type of service!</b></font> </font></p></td></tr>
<tr>
<td colspan="2">
	<font style="font-size: 9pt;" face="Verdana">&nbsp;<font class="bodytext">The next few pages will really take you behind the scenes...</font><a href="template.php?f=history2"><img alt="next" src="greatlengths/media/next.gif" align="right" border="0" height="38" width="46"></a></font>
</td>
</tr>
</table>
<table id="table2" border="0" cellpadding="5" cellspacing="0" width="840">
<tr>
<td>
<table id="table1" cellspacing="0" cellpadding="4" width="100%" border="0"> 
<tbody> 
<tr valign="top"> 
<td colspan="2">
			<?php include'sub-menue.php';?>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</table>