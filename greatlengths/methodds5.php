<table cellspacing="0" cellpadding="0" width="839" border="0"> 
<tbody> 
<tr> 
<td> 
<table id="table1" cellspacing="0" cellpadding="4" width="100%" border="0"> 
<tbody> 
<tr valign="top"> 
<td colspan="2">
			<?php include'sub-menue.php';?>
</td>
</tr>
</tbody>
</table>
</td>
</tr> 
<tr> 
<td>&nbsp;</td>
</tr> 
<tr> 
<td> 

<table id="table2" align="center" border="0" cellpadding="5" cellspacing="2" width="100%">
<tbody>
<tr>
<td colspan="3">
<font class="bodytext">
  <a href="template.php?f=methodds4">
  <img alt="pagina precedente" src="greatlengths/media/prev.gif" border="0" height="38" width="46">
  </a>
</font>
</td>
<td>&nbsp;</td>
</tr>
<tr>
<td colspan="3">
<img alt="" src="greatlengths/media/prodotti.jpg" border="0" height="260" width="292">
</td>
<td>
	<font class="bodytext">As a Great Lengths customer your hairdresser will provide you with your personal cosmetic care ticket, which gives you helpful hints as to how you should treat and care for your hair!</font>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td>&nbsp;</td>
</tr>
<tr>
<td>
<table id="table3" align="center" border="0" cellpadding="5" cellspacing="0" width="100%">
<tbody>
<tr>
<td valign="top">
  <font class="bodytext"><b>Shampoo No. 1</b></font> 
  <font class="small">for dry hair</font><br>
  <font class="bodytext"><b>Shampoo No. 3</b></font> 
  <font class="small">for normal hair</font><br>
  <font class="bodytext"><b>Conditioner:</b></font> 
  <font class="small">balsam for rinsing out to make your hair shine and make it easy to comb</font><br>
  <font class="bodytext"><b>Energy Conditioner:</b></font> 
  <font class="small">provides blond and wavy hair with the moisture it needs to stay soft and to be combed easily.</font> 
</td>
<td valign="top">
	<img alt="" src="greatlengths/media/trans_1x1.gif" border="0" height="1" width="1"></td>
<td valign="top">
  <font class="bodytext"><b>Anti-Tap Water with portioning device:</b></font> 
  <font class="small">for the final treatment with proteins and cosmetic care substances to achieve fantastic shine and to close off the natural cuticle layer. The pH-value of water is set to the ideal value of 5,5.</font> 
  <p>
  <font class="bodytext"><b>De-Tangle Spray:</b></font> 
  <font class="small">this spray is a conditioner which remains in your hair giving it fantastic shine and preventing it from drying out.</font></p>
</td>
<td valign="top">
	<img alt="" src="greatlengths/media/trans_1x1.gif" border="0" height="1" width="1">
</td>
<td colspan="2" valign="top">
  <font class="bodytext">
  <b>Hair fluid:</b></font> 
  <font class="small">hair-end gel for wet or dry hair for the special care of hair ends.</font> 
  <p>
  <font class="small">Your hairdresser will be pleased to give you all the details on our high-quality care programme!</font></p>
</td>
</tr>
<tr>

<td colspan="6" align="middle">
  <hr align="center" color="#000080" noshade="noshade" size="2">
  <font class="bodytext">Naturally, long hair is exposed to more strains than short hair. That is why <b>easy and gentle treatment</b> is very important. Since the cuticle layer of Great Lengths strands is intact the strands will respond to any cosmetic care in the same way as your own hair would. Great Lengths care products have especially been developed for Great Lengths strands and therefore optimally meet their demands. Our strands are <b>100% composed of real hair</b>, which is why our care products can of course also be used for your own hair!</font> 
  <hr align="center" color="#000080" noshade="noshade" size="2">
</td>
</tr>
</tbody>
</table>
<tr> 
<td valign="top" colspan="3">
<font class="bodytext">
  <b>
  <a href="template.php?f=methodds4">
	  <img alt="" src="greatlengths/media/fprev.gif" align="absbottom" border="0" height="20" width="20"> 
  	back
  </a> 
  -
  Page:
  <a href="template.php?f=methodds1">
  	1
  </a>
  | 
  <a href="template.php?f=methodds2">
  	2
  </a>
  |
  <a href="template.php?f=methodds3">
  	3
  </a> 
  | 
  <a href="template.php?f=methodds4">
  	4
  </a> 
  | 
   	5
  - 
  <a href="template.php?f=methodds1">
  	next
  	<img height="20" alt="" src="greatlengths/media/fnext.gif" width="20" align="absBottom" border="0" />
  </a>
  </b>
</font> 
</td>
</tr>

</td>
</tr> 
<tr> 
<td>
			<?php include'sub-menue.php';?>
</td>
</tr> 
<tr> 
<td>&nbsp;</td>
</tr>
</tbody>
</table>