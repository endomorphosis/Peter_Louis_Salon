<table id="table2" border="0" cellpadding="5" cellspacing="0" width="840">
<tbody>
<tr>
<td colspan="2">
<table id="table1" cellspacing="0" cellpadding="4" width="100%" border="0"> 
<tbody> 
<tr valign="top"> 
<td colspan="2">
			<?php include'sub-menue.php';?>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>

<table id="table2" wdith="100%" bgcolor="#e9e9e9" border="0" cellpadding="2" cellspacing="2">
<tbody>
<tr>
<td colspan="2" bgcolor="#003366">
<center>
    <font class="bodytext-alt"><b><font color="#FFFFFF">Brushed (fallen) Indian Hair</font></b>      </font>
</center>
</td>
</tr>
<tr>
<td>
  <font style="font-size: 9pt;" face="Verdana"><font class="bodytext"><br>Because of its same genetic origin the basic structure of Indian hair is very similar to that of European hair. Indian hair is of exceptional quality and Indian women rarely go to salons for chemical treatment of any kind. This might lead you to the conclusion that all Indian hair is ideal for hair extensions...</font> </font>
  <p><font class="bodytext"></font></p>
  <center><font class="bodytext"><font style="font-size: 9pt;" face="Verdana"><b>...but this is generally not the case!</b> </font></font></center><font class="bodytext">&nbsp; 
  </font><p></p>
  <p><font class="bodytext">&nbsp;</font></p>
  <center><font class="bodytext"><font style="font-size: 9pt;" face="Verdana"><img alt="" src="greatlengths/media/c2.gif" border="2" height="125" width="250"> </font></font></center>
  <p align="justify"><font class="bodytext"><font style="font-size: 9pt;" face="Verdana"><font class="bodytext"><b>Believe it or not, hundreds of millions of women of all ages collect their hair every day after brushing it.</b> This includes dark hair, grey hair, and hair of all lengths and thicknesses. This hair is extremely cheap to purchase because the result of this method of collection is, of course, a mass of mixed up hair aligned in different directions.</font> </font></font></p>
  <p align="justify"><font class="bodytext"><font style="font-size: 9pt;" face="Verdana"><font class="bodytext">Because of the mixed direction of the cuticles, the hair has to be subjected to an aggressive acid process in order to remove the cuticle in an attempt to prevent subsequent tangling. Hair companies in Korea and Indonesia specialise in purchasing this type of hair and subjecting it to this acidising process.</font> </font></font></p>
  <p><font class="bodytext">&nbsp;</font></p>
  <center><font class="bodytext"><font style="font-size: 9pt;" face="Verdana"><img alt="" src="greatlengths/media/c3.gif" border="2" height="136" width="250"> </font></font></center>
</td>
</tr>
<tr>
<td>
  <p align="justify"><font style="font-size: 9pt;" face="Verdana"><img alt="" src="greatlengths/media/point.gif" border="0" height="22" width="20"><font class="bodytext">However, because of the mixture of different hairs in this mass and because some areas are particularly oily or dirty, it is very often impossible to fully remove all the cuticles.</font> </font></p>
  <p align="justify"><font style="font-size: 9pt;" face="Verdana"><img alt="" src="greatlengths/media/point.gif" border="0" height="22" width="20"><font class="bodytext">To give this acidised and now damaged hair a healthy appearance once again, the hair is dipped in silicon to make it shine and to cover up any cuticles that were not removed in the acid bath. The silicon also adds weight to the hair which makes it fetch a better price on the market since hair is generally sold by weight.</font><br>&nbsp;</font></p>
  <p align="justify"><font style="font-size: 9pt;" face="Verdana"><img alt="" src="greatlengths/media/point.gif" border="0" height="22" width="20"><font class="bodytext">The standard of this hair is acceptable for the use in wigs and toupets since these items are usually dry-cleaned and are separate to your own hair.</font> </font></p>
  <p align="justify"><font style="font-size: 9pt;" face="Verdana"><font class="bodytext">But what happens if hair extensions are made with this type of hair? (where this hair is directly <u>mixed in with your own hair</u>.)</font> </font></p>
  <p>&nbsp;</p>
<table id="table3" border="0" cellpadding="8" cellspacing="4" width="100%">
<tbody>
<tr>
  <td>
    <font style="font-size: 9pt;" face="Verdana"><img alt="" src="greatlengths/media/c8.gif" border="2" height="160" width="126"></font>
  </td>
  <td>
    <font style="font-size: 9pt;" face="Verdana"><img alt="" src="greatlengths/media/c06.gif" border="2" height="160" width="119"></font>
  </td>
  <td>
    <font style="font-size: 9pt;" face="Verdana"><img alt="" src="greatlengths/media/c5.gif" border="2" height="160" width="130"></font>
  </td>
</tr>
<tr>
  <td valign="top">
    <p align="justify"><font style="font-size: 9pt;" face="Verdana"><font class="bodytext">...to begin with the hair is beautiful, shiny and brushable...</font> </font></p>
    <p><font style="font-size: 9pt;" face="Verdana"><img alt="" src="greatlengths/media/timbro1.gif" align="left" border="0" height="77" width="80"></font></p></td>
  <td valign="top">
    <p align="justify"><font class="bodytext" style="font-size: 9pt;" face="Verdana">...after a few washes, however, the silicon layer starts to wash out...<br>...slowly the hair becomes duller and loses its sheen. It becomes brittle and starts to break...</font></p></td>
  <td valign="top">
    <p align="justify"><font class="bodytext" style="font-size: 9pt;" face="Verdana">...any cuticle originally covered by the silicon will become exposed and present a real threat of tangling and matting with your own hair...<br>...with every wash this condition will become worse...</font></p>
  </td>
</tr>
</tbody>
</table>
    &nbsp; 
    <p></p>
    <p>&nbsp;</p>
</td>
</tr>

<tr>
  <td align="right">
    <center><font face="Verdana"><font class="bodytext"><span style="font-size: 9pt;">This type of hair is </span><font style="font-size: 9pt;"><b>not the best solution</b></font><span style="font-size: 9pt;"> for use in <br>hair extensions !</span></font><span style="font-size: 9pt;"> </span></font></center><font style="font-size: 9pt;" face="Verdana"><a href="template.php?f=history5"><img alt="previous" src="greatlengths/media/prev.gif" border="0" height="38" width="46"></a>&nbsp;<a href="template.php?f=history7"><img alt="next" src="greatlengths/media/next.gif" border="0" height="38" width="46"></a></font>
  </td>
</tr>
</tbody>
</table>



<table id="table2" border="0" cellpadding="5" cellspacing="0" width="840">
<tr>
<td>
<table id="table1" cellspacing="0" cellpadding="4" width="100%" border="0"> 
<tbody> 
<tr valign="top"> 
<td colspan="2">
			<?php include'sub-menue.php';?>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</table>