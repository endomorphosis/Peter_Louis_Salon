<table width="720" cellspacing="0" cellpadding="0" style=" background-repeat:repeat-x; max-width: 720px;">
<tr>
<td colspan="3">
<span><? include($template_include_file);?></span>
</td>
</tr>
<tr>
<td colspan="2"  style="background-image: url('./images/hdny_gradientB.png') ;">
<p style="color: #000000; font-size: 16px; margin-left: 1em; margin-right: 1em;">
<strong>
STANDARD BONDINGS</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
Hairdreams Standard Bondings consist of a thermo-elastic synthetic material developed by Hairdreams.  This material features excellent application characteristics, outstanding durability and great wearing comfort.
</strong>
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
Application is done with the Hairdreams Selector System. Hairdreams Standard Bondings are available in 4 different strand sizes. This permits optimal customization for each part of the head and all the different structures in the client's own hair. 
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
<? include('store/hdny_nav_bondings.php'); ?>
</p>
</td>
<td align="right" style=" vertical-align: bottom; background-image: url('./images/2817.jpg') ;">
<img width="350" height="450" src="/images/2817.jpg"/>
</td>
</tr>
<tr>
<td colspan="3" style=" background-image: url('./images/hdny_bottom.png') ;">
<img src="./images/hdny_bottom.png"/>
</td>
</tr>
</table>
