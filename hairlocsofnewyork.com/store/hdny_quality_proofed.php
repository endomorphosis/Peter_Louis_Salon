<table width="720" cellspacing="0" cellpadding="0" style=" background-repeat:repeat-x; max-width: 720px;">
<tr>
<td colspan="3">
<span><? include($template_include_file);?></span>
</td>
</tr>
<tr>
<td colspan="2"  style="background-image: url('./images/hdny_gradientB.png') ;">
<p style="color: #000000; font-size: 16px; margin-left: 1em; margin-right: 1em;">
<strong>
SCIETIFICALLY PROVEN HAIRDREAMS QUALITY</strong>
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
An analysis at the University of Aachen proved the impressive and unparalleled quality of Hairdreams hair.

</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
Comparison with a lower-priced competitor's hair revealed that Hairdreams hair consists of 100% healthy "normal" hair with fully closed cuticle layers, which stands out with its perfect shine and natural elasticity. The competition's hair on the other hand is mostly composed of extremely damaged hair with etched or even burst surfaces as a result of chemical treatments. This is of great significance in terms of the quality of a hair extension since hair without a healthy surface has little shine and elasticity and tends to get matted easily.

</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
<? include('store/hdny_nav_qualities.php'); ?>
</p>
</td>
<td align="right" style=" vertical-align: bottom; background-image: url('./images/2790.jpg') ;">
<img width="350" height="450" src="/images/2790.jpg"/>
</td>
</tr>
<tr>
<td colspan="3" style=" background-image: url('./images/hdny_bottom.png') ;">
<img src="./images/hdny_bottom.png"/>
</td>
</tr>
</table>
