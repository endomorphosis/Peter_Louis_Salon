<span><? include($template_include_file);?></span>
<table width="720" cellspacing="0" cellpadding="0" style=" text-align: center; background-color: #cae7ee; max-width: 720px;">
<tr>
<td style="text-align: left;" colspan="2"><br/>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
Peter Louis Salon and Great Lengths offers you the opportunity of creating the look you've always wanted.
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
What's more, your new hair will behave and feel exactly as your own so you can treat them in the exact same way. 
</p>
</td>
</tr>
<tr style="font-size: 14px; font-weight: 700;">
<td style="font-size: 14px; font-weight: 700;" >
<br/>
<center>BEFORE:</center>
<br/>
</td>
<td style="font-size: 14px; font-weight: 700;">
<br/>
<center>AFTER:</center>

<br/></td>
</tr>

<tr>
<td><br/>
<img src="/images/primaILARIA.jpg"/>
<br/></td>
<td><br/>
<img src="/images/primaILARIA1.jpg"/>
<br/></td>
</tr>
<tr>
<td><br/>
<img src="/images/primaIRINA.jpg"/>
<br/></td>
<td><br/>
<img src="/images/primaIRINA1.jpg"/>
<br/></td>
</tr>
<tr>
<td><br/>
<img src="/images/primametis.jpg"/>
<br/></td>
<td><br/>
<img src="/images/primametis1.jpg"/>
<br/></td>
</tr>
<tr>
<td><br/>
<img src="/images/primaOLIVIA.jpg"/>
<br/></td>
<td><br/>
<img src="/images/primaOLIVIA1.jpg"/>
<br/></td>
</tr>
<tr>
<td><br/>
<img src="/images/primaPAULA.jpg"/>
<br/></td>
<td><br/>
<img src="/images/primaPAULA1.jpg"/>
<br/></td>
</tr>
</table>
