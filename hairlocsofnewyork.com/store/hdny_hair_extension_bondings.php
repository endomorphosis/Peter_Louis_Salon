<table width="720" cellspacing="0" cellpadding="0" style=" background-repeat:repeat-x; max-width: 720px;">
<tr>
<td colspan="3">
<span><? include($template_include_file);?></span>
</td>
</tr>
<tr>
<td colspan="2"  style="background-image: url('./images/hdny_gradientB.png') ;">
<p style="color: #000000; font-size: 16px; margin-left: 1em; margin-right: 1em;">
<strong>
THE HAIRDREAMS BONDINGS
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
Hairdreams hair extensions meet the highest professional standards with their innovative bondings and wide selection of lengths as well as strand sizes.</strong>
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
The Hairdreams standard of perfection and high quality is evident in the hair quality as well as in the seemingly endless combinations of bonding and strand types.
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
Any client can enjoy a perfectly customized hair extension using the combination that is just right for him or her.
Hairdreams is the ideal base for truly professional hair extensions, thickenings and trendy strand effects.
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
A total of four different bonding types are available for the careful attachment of Hairdreams strands to your own hair, ensuring an optimal match between the extension and your own hair.
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
<? include('store/hdny_nav_bondings.php'); ?>
</p>
</td>
<td align="right" style=" vertical-align: bottom; background-image: url('./images/151.jpg') ;">
<img width="300" height="450" src="/images/151.jpg"/>
</td>
</tr>
<tr>
<td colspan="3" style=" background-image: url('./images/hdny_bottom.png') ;">
<img src="./images/hdny_bottom.png"/>
</td>
</tr>
</table>
