<table width="720" cellspacing="0" cellpadding="0" style=" background-repeat:repeat-x; max-width: 720px;">
<tr>
<td colspan="3">
<span><? include($template_include_file);?></span>
</td>
</tr>
<tr>
<td colspan="2"  style="background-image: url('./images/hdny_gradientB.png') ;">
<p style="color: #000000; font-size: 16px; margin-left: 1em; margin-right: 1em;">
<strong>THE STANDARD HAIRDREAMS METHOD</strong>
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
With the standard technique, each strand is attached individually by hand. </p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
Through the manual process the hair extensions can be handled in a particularly precise way and perfectly adjusted to the individual characteristics. Durability of the strands using this method is around 5-6 months.</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;"><table border="0" cellpadding="3" cellspacing="3" width="100"><tbody><tr><td align="center"><a href="#" onmouseover="document.getElementById('gal41').src='/images/306.jpg';" onmouseout="document.getElementById('gal41').src='/images/310.jpg';"><img onclick="$('#image').attr('src', '/images/307.jpg');" src="/images/310.jpg" name="gal41" id="gal41" border="0"></a><br>step 1</td><td align="center"><a href="#" onmouseover="document.getElementById('gal30').src='/images/195.jpg';" onmouseout="document.getElementById('gal30').src='/images/279.jpg';"><img onclick="$('#image').attr('src', '/images/196.jpg');" src="/images/279.jpg" name="gal30" id="gal30" border="0"></a><br>step 2</td><td align="center"><a href="#" onmouseover="document.getElementById('gal31').src='/images/197.jpg';" onmouseout="document.getElementById('gal31').src='/images/280.jpg';"><img onclick="$('#image').attr('src', '/images/198.jpg');" src="/images/280.jpg" name="gal31" id="gal31" border="0"></a><br>step 3</td>

<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
<? include('store/hdny_nav_how_it_works.php'); ?>
</p>
</td>
<td align="right" style=" vertical-align: bottom; background-image: url('./images/198.jpg') ;">
<img width="350" height="450" id="image" src="/images/198.jpg"/>
</td>
</tr>
<tr>
<td colspan="3" style=" background-image: url('./images/hdny_bottom.png') ;">
<img src="./images/hdny_bottom.png"/>
</td>
</tr>
</table>
