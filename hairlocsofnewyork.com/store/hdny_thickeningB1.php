<span><? include($template_include_file);?></span>
<table width="720" cellspacing="0" cellpadding="0" style=" background-repeat:repeat-x; max-width: 720px;">
<tr>
<td colspan="2"  style="background-image: url('./images/hdny_gradientB.png') ;">
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
<strong>HAIR THICKENING WITH HAIRDREAMS STRANDS</strong>
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
Hair Thickening with Hairdreams strands is the ideal method to give fine hair lasting volume in lengths and ends.
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
The client's own hair is inconspicuously filled in with individual Hairdreams strands, which match in color, length and structure.
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
Thus volume can be added to lengths and ends according to the client's individual desire, with an absolutely natural and long-lasting result.
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
The hair is attached with tiny, patented Bondings, which are characterized by optimal wearing comfort and reliable durability. For long-lasting, pure satisfaction with one's new hairstyle.
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
HAIRDREAMS EXTENSIONS<br/>
<ul><li>HAIRDREAMS STRANDS<br/>
How It Works</li><li>

<li>HAIRDREAMS EXTENSIONS <br/>
Style Examples </li>

</td>
<td align="right" style=" vertical-align: bottom; background-image: url('./images/hdny_thickeningB1.png') ;">
<img  src="/images/hdny_thickeningB1.png"/>
</td>
</tr>
<tr>
<td colspan="3" style=" background-image: url('./images/hdny_bottom.png') ;">
<img src="./images/hdny_bottom.png"/>
</td>
</tr>
</table>
