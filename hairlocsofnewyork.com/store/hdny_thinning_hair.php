<table width="720" cellspacing="0" cellpadding="0" style=" background-repeat:repeat-x; max-width: 720px;">
<tr>
<td colspan="3">
<span><? include($template_include_file);?></span>
</td>
</tr>
<tr>
<td colspan="2"  style="background-image: url('./images/hdny_gradientB.png') ;">
<p style="color: #000000; font-size: 16px; margin-left: 1em; margin-right: 1em;">
<strong>THINNING HAIR</strong><br/>
Problem Hair Loss
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
It is considered hair loss when the approx. 100 individual hairs that one looses on a daily basis are not completely replaced by new and growing hairs.
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
<strong>The causes:</strong> The main cause of hair loss is genetic predisposition. With increasing age the hair becomes less plentiful, as well as more tired and frail. Add to that environmental factors such as stress, toxins, diseases, nutritional deficiencies and chemical treatments.
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
<strong>The consequence:</strong> As the amount of hair decreases the scalp becomes more visible, especially in the crown area. The hair looks very thin. Oftentimes bald spots will form. Styling products are not enough to cover up the problem anymore.
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
<strong>The solution:</strong> The client's existing hair is gently supplemented and replenished with high-grade, hand-selected human hair. This hair matches the client's own hair in color, length and structure, thus making for an absolutely natural look.
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
The integration process is completely gentle and non-damaging; wearing it feels just like one's own hair, without any noteworthy lifestyle limitations. The additional volume looks completely natural and can't be distinguished from the client's own hair.
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
<? include('store/hdny_nav_thinning_hair.php'); ?>
</p>
</td>
<td align="right" style=" vertical-align: bottom; background-image: url('./images/hdny_thickeningC.png') ;">
<img  src="/images/hdny_thickeningC.png"/>
</td>
</tr>
<tr>
<td colspan="3" style=" background-image: url('./images/hdny_bottom.png') ;">
<img src="./images/hdny_bottom.png"/>
</td>
</tr>
</table>
