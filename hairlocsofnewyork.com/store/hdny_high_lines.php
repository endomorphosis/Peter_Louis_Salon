<table width="720" cellspacing="0" cellpadding="0" style=" background-repeat:repeat-x; max-width: 720px;">
<tr>
<td colspan="3">
<span><? include($template_include_file);?></span>
</td>
</tr>
<tr>
<td colspan="2"  style="background-image: url('./images/hdny_gradientB.png') ;">
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
<strong>HAIRDREAMS HIGHLINES</strong>
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
Hair Thickening with amazing highlight effects.
The new Hairdreams HighLines provide considerably more volume on top of the head for very fine and slightly thinning hair. This is done with micro-fine, invisible special threads, which are integrated into the client’s own hair, and which have fine Hairdreams strands attached to them.
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
If desired, a different color can be chosen for those strands, so that the result is an attractive change in color, or strand effects such as highlights or lowlights.
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
The unique characteristics of the HighLines System make these strand effects especially natural. And all this is done in a gentle, chemical-free process!
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
<? include('store/hdny_nav_high_lines.php'); ?>
</p>
</td>
<td align="right" style=" vertical-align: bottom; background-image: url('./images/hdny_thickeningSUB3.png') ;">
<img  src="/images/hdny_thickeningSUB3.png"/>
</td>
</tr>
<tr>
<td colspan="3" style=" background-image: url('./images/hdny_bottom.png') ;">
<img src="./images/hdny_bottom.png"/>
</td>
</tr>
</table>
