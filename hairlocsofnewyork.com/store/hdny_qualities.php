<table width="720" cellspacing="0" cellpadding="0" style=" background-repeat:repeat-x; max-width: 720px;">
<tr>
<td colspan="3">
<span><? include($template_include_file);?></span>
</td>
</tr>
<tr>
<td colspan="2"  style="background-image: url('./images/hdny_gradientB.png') ;">
<p style="color: #000000; font-size: 16px; margin-left: 1em; margin-right: 1em;">
<strong>
ADVANTAGE THROUGH QUALITY!</strong>
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
 
The quality of Hairdreams hair extensions makes all the difference. We only work with the best and highest-grade hair, which is procured all over the world in adherence with strict ethical principles.
 </p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
Intricate and laborious sorting and refinement processes are used to produce hair extensions and other human hair products with the typical shine, elasticity and bounce, characterizing real Hairdreams hair.
 </p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
The two hair qualities "Basic" and "Special" live up to even the highest expectations of stylists and end clients. Especially our "Special" hair remains unsurpassed in quality and enjoys a legendary reputation among top-stylists all over the world. 
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
<? include('store/hdny_nav_qualities.php'); ?>
</p>
</td>
<td align="right" style=" vertical-align: bottom; background-image: url('./images/74.jpg') ;">
<img width="350" height="450" src="/images/74.jpg"/>
</td>
</tr>
<tr>
<td colspan="3" style=" background-image: url('./images/hdny_bottom.png') ;">
<img src="./images/hdny_bottom.png"/>
</td>
</tr>
</table>
