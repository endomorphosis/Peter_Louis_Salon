<table width="720" cellspacing="0" cellpadding="0" style=" background-repeat:repeat-x; max-width: 720px;">
<tr>
<td colspan="3">
<span><? include($template_include_file);?></span>
</td>
</tr>
<tr>
<td colspan="2"  style="background-image: url('./images/hdny_gradientB.png') ;">
<p style="color: #000000; font-size: 16px; margin-left: 1em; margin-right: 1em;">
<strong>HAIRDREAMS COLORS<br/>
Lustrous colors for any taste</strong>
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
Hairdreams hair is available in a vast variety of standard colors ranging from subtle natural browns all the way to brilliant red shades; from a wide range of blonde nuances to intense and strong trend colors.
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
Furthermore, we offer other custom blends, bi-color strands and special color orders.
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
All colors stand out with their distinguished fade resistance and brilliance.
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
<? include('store/hdny_nav_cwl.php'); ?>
</p>
</td>
<td align="right" style=" vertical-align: bottom; background-image: url('./images/2811.jpg') ;">
<img  src="/images/2811.jpg"/>
</td>
</tr>
<tr>
<td colspan="3" style=" background-image: url('./images/hdny_bottom.png') ;">
<img src="./images/hdny_bottom.png"/>
</td>
</tr>
</table>
