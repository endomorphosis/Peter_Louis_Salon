<table width="720" cellspacing="0" cellpadding="0" style=" background-repeat:repeat-x; max-width: 720px;">
<tr>
<td colspan="3">
<span><? include($template_include_file);?></span>
</td>
</tr>
<tr>
<td colspan="2"  style="background-image: url('./images/hdny_gradientB.png') ;">
</p>
<p style="color: #000000; font-size: 16px; margin-left: 1em; margin-right: 1em;">
<strong>Hairdreams MicroLines</strong>
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
The proven method for hair loss and thin hair. <br/>
The MicroLines System is ideally suited to replenish natural volume on top of the head for people who have thin hair or even advanced hair loss. 
</p>
<p style="color: #000000; font-size: 12px; margin-left: 1em; margin-right: 1em;">
Micro-fine, virtually invisible special threads, so-called "MicroLines", which have high-grade human hair attached to them, are worked into the client's existing hair. The hair is matched in terms of color, structure and length, and can be unobtrusively integrated into the client's own hair. The result is absolutely natural and voluminous hair, which blends into the clients own hair perfectly.
</p>
<p style="color: #000000; font-size: 12px; margin-left: 1em; margin-right: 1em;">
The MicroLines System is attached permanently and can be worn for many months. The additional hair can be worn like one's own – even when swimming, going to a sauna, riding in a convertible etc. As opposed to hair pieces, MicroLines don't require any limitations to one's normal lifestyle.
</p>
<p style="color: #000000; font-size: 12px; margin-left: 1em; margin-right: 1em;">
The MicroLines System is perfectly adapted to each client's individual hair situation. Custom solutions are available even for special hair problems such as bald spots. 
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
<? include('store/hdny_nav_microlines.php'); ?>
</p>
</td>
<td align="right" style=" vertical-align: bottom; background-image: url('./images/2911.jpg') ;">
<img width="275" height="466" src="/images/2911.jpg"/>
</td>
</tr>
<tr>
<td colspan="3" style=" background-image: url('./images/hdny_bottom.png') ;">
<img src="./images/hdny_bottom.png"/>
</td>
</tr>
</table>
