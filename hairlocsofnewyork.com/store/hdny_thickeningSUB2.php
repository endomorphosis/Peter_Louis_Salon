<span><? include($template_include_file);?></span>
<table width="720" cellspacing="0" cellpadding="0" style=" background-repeat:repeat-x; max-width: 720px;">
<tr>
<td colspan="2"  style="background-image: url('./images/hdny_gradientB.png') ;">
</p>
<p style="color: #000000; font-size: 16px; margin-left: 1em; margin-right: 1em;">
<strong>HAIRDREAMS TOP HAIR</strong>
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
Extra-fast, extra-flexible thickening for fine and thinning hair.
The innovative TopHair System by Hairdreams is especially fast, convenient and flexible to use. It is ideally suited for fast and easy thickening of fine and thinning hair on top of the head. Due to its unique construction it is especially suitable for hairstyles with longer hair on top and partings.
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
It consists of an innovative, breathable high-tech fabric, which has Hairdreams human hair attached to it. It is integrated into the client’s own hair on top of the head, making possible wonderfully soft and natural hairstyles with longer hair and natural bangs or partings.
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
Depending on preference it can be attached permanently or with the use of clips, so that the wearer can remove and reattach the hair at any time. This easy process is practiced in the salon under professional supervision.
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
<strong>TOPHAIR Hair thickening</strong><ul><li><strong>TOPHAIR</strong> - How It Works</li><li><strong>TOPHAIR</strong> - Style Examples</li></ul>
</p>
</td>
<td align="right" style=" vertical-align: bottom; background-image: url('./images/hdny_thickeningSUB2.png') ;">
<img  src="/images/hdny_thickeningSUB2.png"/>
</td>
</tr>
<tr>
<td colspan="3" style=" background-image: url('./images/hdny_bottom.png') ;">
<img src="./images/hdny_bottom.png"/>
</td>
</tr>
</table>
