<table width="720" cellspacing="0" cellpadding="0" style=" background-repeat:repeat-x; max-width: 720px;">
<tr>
<td colspan="3">
<span><? include($template_include_file);?></span>
</td>
</tr>
<tr style="background-image: url('./images/home-655.png') ;">
<td colspan="2" valign="top" >
<p>
</p>
<p style="color: #000000; font-size: 16px; margin-left: 1em; margin-right: 1em;">
<strong>
Frequently Asked Questions</strong>
</p>
<span class="collasped" name="#1"><a href="#">How does it work? </a><br/></span>
<div class="collasped" id="1">The Hairlocs Hair Extension System starts with 100% human hair from Europe. It is then applied sing an unique cold application by a certified stylist. Unlike other methods, the process uses no chemicals, sewing, heat or glue and is completely safe and painless to apply.</div>
<span class="collasped" name="#2"><a href="#">Will it damage my natural hair?</a><br/></span>
<div class="collasped" id="2">With Hairlocs there is no tension, stretching, chemicals or heat used during the application or take down process. Hairlocs are the safest extensions available today.</div>
<span class="collasped" name="#3"><a href="#">Can you see them?</a><br/></span>
<div class="collasped" id="3">The extensions are always applied behind the hairline and away from the natural partings. The unique shells are color matched so they blend naturally with your hair and are virtually undetectable!</div>
<span class="collasped" name="#4"><a href="#">Will the hair match my color and texture?</a><br/></span>
<div class="collasped" id="4">Absolutely. Your certified Hairlocs specialist will match your color and texture perfectly. Custom blending is our speciality</div>
<span class="collasped" name="#5"><a href="#">How long do they last?</a><br/></span>
<div class="collasped" id="5">With regular maintenance, the life of your extensions is up to you. Most people can go up to 3 months before needing any service on their hair.</div>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
Hairlocs is a "Cold Process" for applying hair extensions, wefts, integrations, that is safe for ALL hair types. Since our hair is re-useable over several applications, we offer "in-between" maintenance services so you never have to experience a full take down. We also guarantee the quality of our hair from Italy, Russia, and Spain insuring lasting results with your investment
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
All Hairlocs Certified specialists are trained and choached in the field of Hair Extensions. Hairlocs are a safe, reuseable cold application. Offering re-use-abilitiy of your purchase and maintenance services avoiding frequent take downs and re-application....</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
Extensions are an investment and you need to care for them. Take the time to learn about the world of hair enhancement, then you will have a very satisfying experience.
</p>
</td>
<td width="200" align="right" style=" vertical-align: bottom;">
</td>
</tr>
<tr>
<td colspan="3" style=" background-image: url('./images/hdny_bottom.png') ;">
<img src="./images/hdny_bottom.png"/>
</td>
</tr>
</table>
<style>

</style>
