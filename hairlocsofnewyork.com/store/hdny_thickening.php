<table width="720" cellspacing="0" cellpadding="0" style=" background-repeat:repeat-x; max-width: 720px;">
<tr>
<td colspan="3">
<span><? include($template_include_file);?></span>
</td>
</tr>
<td colspan="2"  style="background-image: url('./images/hdny_gradient.png') ;">
<p style="color: #000000; font-size: 16px; margin-left: 1em; margin-right: 1em;">
<strong>HAIRDREAMS VOLUME+ SYSTEM </strong>
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
<strong>Hair Thickening with Hairdreams </strong>
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
Many women suffer from having thinning hair because the hair breaks, gets thinner, or even falls out and leaves bald spots. In any case, this is a serious problem for affected women: they feel vulnerable and unattractive. Their biggest dream: a full and healthy head of hair!  Welcome to Peter Louis Salon and Hairdreams Hair Extensions.
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
Hair Thickening at <a href="http://www.peterlouissalon.com">Peter Louis Salon</a> with Hairdreams is the most natural and durable method to give fine and thinning hair body and volume. Several methods of Hair Thickening are available, depending on how fine or thin your own hair is.
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
The following pages will help you find a perfect custom solution for any case.
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
<? include('store/hdny_nav_thickening.php'); ?>
</p>
</td>
<td align="right" style=" background-image: url('./images/hdny_thickening.png') ;">
<img  src="/images/hdny_thickening.png"/>
</td>
</tr>
<tr>
<td colspan="3" style=" background-image: url('./images/hdny_bottom.png') ;">
<img src="./images/hdny_bottom.png"/>
</td>
</tr>
</table>
