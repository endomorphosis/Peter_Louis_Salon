<? include($template_include_file);?>
<table width="720" cellspacing="0" cellpadding="0" style=" background-repeat:repeat-x; max-width: 720px;">
<tr>
<td colspan="2"  style="background-image: url('./images/glbg1.png') ;">
<p style="color: #000000; font-weight: 700; font-size: 12px; margin-left: 1em; margin-right: 1em;">
</p>

<p style="color: #000000; font-size: 12px; margin-left: 1em; margin-right: 1em;">
While others use techniques such as welding, waxing, gluing or knotting, which strain and very often damage your hair to a high degree, Great Lengths is based on a much more gentle principle: modulating.
</p>
<p style="color: #000000; font-size: 12px; margin-left: 1em; margin-right: 1em;">
We have created an extremely high-tech method of bonding between the Great Lengths strand and your own hair. This point of attachment is composed of polymer chains whose molecular structure is very similar to that of human hair. It is activated by our Great Lengths applicator and bonds the Great Lengths strand with your own hair in this way. It can, however, be easily removed by an expert!
</p>
<p style="color: #000000; font-size: 12px; margin-left: 1em; margin-right: 1em;">
Most important of all: your own hair is not damaged. This patented attachment of Great Lengths to your own hair offers you perfect freedom in organizing your leisure time and in your freedom of action but in no way compromises the quality of your own hair!

</p>

</td>
<td  style=" max-width: 396px; text-align: right; background-image: url('./images/glbg1.png') ;">
<img src="/images/greatlengths11.png"/>
</td>
</tr>
<tr>
<td   style=" background-image: url('./images/glbg4.png') ;" >
<img src="/images/greatlengths10.png"/>
</td>
<td colspan="2"  style=" max-width: 417px; background-image: url('./images/glbg4.png') ;">
<p style="color: #000000; font-size: 12px; margin-left: 1em; margin-right: 1em;">
Great Lengths' philosophy is to remedy the lack of thick hair in a way which is as simple as dyeing it if you want to change its colour. Considering the fact that people who do not have a lot of hair generally have very fine hair at the same time, the Great Lengths' programme offers various thicknesses of natural hair strands for attaching to fine hair which can be attached in a practically invisible way.
</p>
<p style="color: #000000; font-size: 12px; margin-left: 1em; margin-right: 1em;">
If used properly and professionally, hair extensions may significantly change the way you look and give you the hair you've always dreamed of in no time. Our objective is to provide your hairstylist with the best possible tools in order to achieve this. You will never try another hair extension system after you've lived the Great Lengths experience.
</p>


</td>
</tr>
</table>
