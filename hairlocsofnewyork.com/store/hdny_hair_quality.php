<table width="720" cellspacing="0" cellpadding="0" style=" background-repeat:repeat-x; max-width: 720px;">
<tr>
<td colspan="3">
<span><? include($template_include_file);?></span>
</td>
</tr>
<tr>
<td colspan="2"  style="background-image: url('./images/hdny_gradient.png') ;">
<p style="color: #000000; font-size: 16px; margin-left: 1em; margin-right: 1em;">
<strong>
ADVANTAGE THROUGH QUALITY!</strong>
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
 
The quality of Hairdreams hair extensions makes all the difference. We only work with the best and highest-grade hair, which is procured all over the world in adherence with strict ethical principles.
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;"> 
Intricate and laborious sorting and refinement processes are used to produce hair extensions and other human hair products with the typical shine, elasticity and bounce, characterizing real Hairdreams hair.
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;"> 
The two hair qualities "Basic" and "Special" live up to even the highest expectations of stylists and end clients. Especially our "Special" hair remains unsurpassed in quality and enjoys a legendary reputation among top-stylists all over the world. 
</p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">

<table><tr><td><img src="/images/user/2631.jpg" name="navi" id="navi" border="0"></td><td style="padding-left: 5px;" valign="top" width="100%"><h1>HAIR QUALITIES</h1><table border="0" cellpadding="0" cellspacing="1" width="100%"><tbody><tr><td nowrap="nowrap" width="15"><a href="/en/hair_quality_guarantee"><img src="/images/pfeil.gif" border="0"></a>&nbsp;</td><td width="100%"><a href="/en/hair_quality_guarantee"  onmouseover="document.getElementById('navi').src='/images/user/2631.jpg';">HAIRDREAMS GUARANTEE</a></td></tr><tr><td nowrap="nowrap" width="15"><a href="/en/basic_quality"><img src="/images/pfeil.gif" border="0"></a>&nbsp;</td><td width="100%"><a href="/en/basic_quality"  onmouseover="document.getElementById('navi').src='/images/user/374.jpg';">HAIRDREAMS BASIC</a></td></tr><tr><td nowrap="nowrap" width="15"><a href="/en/special_quality"><img src="/images/pfeil.gif" border="0"></a>&nbsp;</td><td width="100%"><a href="/en/special_quality"  onmouseover="document.getElementById('navi').src='/images/user/375.jpg';">HAIRDREAMS SPECIAL</a></td></tr><tr><td nowrap="nowrap" width="15"><a href="/en/quality_proofed"><img src="/images/pfeil.gif" border="0"></a>&nbsp;</td><td width="100%"><a href="/en/quality_proofed"  onmouseover="document.getElementById('navi').src='/images/user/2632.jpg';">SCIENTIFICALLY PROVEN QUALITY</a></td></tr></table></td></tr></table>
</p>
</td>
<td align="right" style=" background-image: url('./images/3035.jpg') ;">
<img  src="/images/3035.jpg"/>
</td>
</tr>
<tr>
<td colspan="3" style=" background-image: url('./images/hdny_bottom.png') ;">
<img src="./images/hdny_bottom.png"/>
</td>
</tr>
</table>
