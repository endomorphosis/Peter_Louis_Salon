<table width="720" cellspacing="0" cellpadding="0" style=" background-repeat:repeat-x; max-width:  720px; background-image: url('./images/home-1094.png'); ">
<tr>
<td colspan="3">
<span><? include($template_include_file);?></span>
</td>
</tr>
<tr>
<td colspan="3">
<p style="text-align: center; color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;"> Hairlocs<sup>&reg;</sup> products found at<br/> <a style="text-align: center; text-decoration: underline; color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;" href="http://www.peterlouissalon.com"> Peter Louis Salon </a><br/> 143 E 57th Street<br/> New York, NY 10022 
212.319.0019</p>
</td>
</tr>
<tr>
<td colspan="2"  >

<p style="color: #000000; font-size: 16px; margin-left: 1em; margin-right: 1em;">
<strong>Why Hairlocs?</strong>
</p>
<p style="color: #000000; font-size: 12px; margin-left: 1em; margin-right: 1em;">
The Hairlocs Hair Extension System starts with the finest, 100% Russian and European Hair. The hair is applied using a simple, unique method by a Certified Hairlocs Stylist, using our patented "Hairloc." Unlike other methods, the process uses no braiding, sewing, heat or glue, making The Hairlocs Hair Extension System painless and the safest choice for hair enhancement.
</p>
<p style="color: #000000; font-size: 12px; margin-left: 1em; margin-right: 1em;">
Our product is internationally known for our quality and we take pride in our thousands of worldwide customers. From our Film and Television Celebrities preparing for a new shoot, to a wedding makeover, or just adding some spice and color for a fresh new look, The Hairlocs Hair Extension System's versatility is unmatched.
</p>
<p style="color: #000000; font-size: 12px; margin-left: 1em; margin-right: 1em;">
Only The Hairlocs Hair Extension System uses no machines, glue, heat, or complex weaves and braiding that can damage your real hair, hair follicles and scalp. Our Certified Hairlocs Stylists are highly trained professionals, located globally. Cannot find a stylist on our site in your area, give us a call or send an email and we will have one come to you.
</p>
<p style="color: #000000; font-size: 12px; margin-left: 1em; margin-right: 1em;">
The Hairlocs Hair Extension System is the safest method of hair enhancement. Have a fun color you would like try for the first time, want highlights without the chemicals, have special needs? Our production facility works diligently to ensure your order is perfectly cut, colored, and matched to your needs, every time. Our customers are repeat customers, because The Hairlocs Hair Extension System is easily removed for maintenance, can be further tailored by your stylists, and can be reused, extending the life of your investment, saving you money. Our customers enjoy very low maintenance and an enduring new look.
</p>

</td>
<td align="right" style=" ">
<img src="/images/home-1399.png"/><br/><img src="/images/home-1402.png"/>
</td>
</tr>
<tr>
<td colspan="3">
<br/>
<p style="color: #000000; font-size: 12px; margin-left: 1em; margin-right: 1em;">
A major advantage of The Hairlocs Hair Extension System is its compatibility with all hair types. This method can also be used to attach other hair pieces such as wefts and custom fitted pieces. The Hairlocs Hair Extension System even works for men and teens. Other methods of gluing and sewing may prove harmful to your own hair, which creates tension and stress, possibly hair loss. The Hairlocs Hair Extension System is the safest method for all types of hair, because it was initially designed for fine and fragile hair, and does not require solvents to remove, making our system the #1 Hair Extension System.
</p>
<p style="color: #000000; font-size: 14px; text-align: center; font-weight: 700; margin-left: 1em; margin-right: 1em;">

Hair Lengths, Hair Quality, Hair Enhancement, Hairlocs.
</p>
</td>
</tr>
<tr>
<td colspan="3" style=" background-image: url('./images/hdny_bottom.png') ;">
<img src="./images/hlny_bottom.png"/>
</td>
</tr>
</table>
