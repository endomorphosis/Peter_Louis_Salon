<?php
/** 
 *  PHP Version 5
 *
 *  @category    Amazon
 *  @package     Amazon_FWSInventory
 *  @copyright   Copyright 2008 Amazon Technologies, Inc.
 *  @link        http://aws.amazon.com
 *  @license     http://aws.amazon.com/apache2.0  Apache License, Version 2.0
 *  @version     2009-07-31
 */
/******************************************************************************* 
 *    __  _    _  ___ 
 *   (  )( \/\/ )/ __)
 *   /__\ \    / \__ \
 *  (_)(_) \/\/  (___/
 * 
 *  Amazon FWS Inventory PHP5 Library
 *  Generated: Fri Nov 20 09:59:57 PST 2009
 * 
 */

/**
 *  @see Amazon_FWSInventory_Model
 */
require_once ('Model.php');  

    

/**
 * Amazon_FWSInventory_Model_GetInventorySupplyRequest
 * 
 * Properties:
 * <ul>
 * 
 * <li>MerchantSKU: string</li>
 * <li>ResponseGroup: string</li>
 *
 * </ul>
 */ 
class Amazon_FWSInventory_Model_GetInventorySupplyRequest extends Amazon_FWSInventory_Model
{


    /**
     * Construct new Amazon_FWSInventory_Model_GetInventorySupplyRequest
     * 
     * @param mixed $data DOMElement or Associative Array to construct from. 
     * 
     * Valid properties:
     * <ul>
     * 
     * <li>MerchantSKU: string</li>
     * <li>ResponseGroup: string</li>
     *
     * </ul>
     */
    public function __construct($data = null)
    {
        $this->_fields = array (
        'MerchantSKU' => array('FieldValue' => array(), 'FieldType' => array('string')),
        'ResponseGroup' => array('FieldValue' => null, 'FieldType' => 'string'),
        );
        parent::__construct($data);
    }

        /**
     * Gets the value of the MerchantSKU .
     * 
     * @return array of string MerchantSKU
     */
    public function getMerchantSKU() 
    {
        return $this->_fields['MerchantSKU']['FieldValue'];
    }

    /**
     * Sets the value of the MerchantSKU.
     * 
     * @param string or an array of string MerchantSKU
     * @return this instance
     */
    public function setMerchantSKU($merchantSKU) 
    {
        if (!$this->_isNumericArray($merchantSKU)) {
            $merchantSKU =  array ($merchantSKU);    
        }
        $this->_fields['MerchantSKU']['FieldValue'] = $merchantSKU;
        return $this;
    }
  

    /**
     * Sets single or multiple values of MerchantSKU list via variable number of arguments. 
     * For example, to set the list with two elements, simply pass two values as arguments to this function
     * <code>withMerchantSKU($merchantSKU1, $merchantSKU2)</code>
     * 
     * @param string  $stringArgs one or more MerchantSKU
     * @return Amazon_FWSInventory_Model_GetInventorySupplyRequest  instance
     */
    public function withMerchantSKU($stringArgs)
    {
        foreach (func_get_args() as $merchantSKU) {
            $this->_fields['MerchantSKU']['FieldValue'][] = $merchantSKU;
        }
        return $this;
    }  
      

    /**
     * Checks if MerchantSKU list is non-empty
     * 
     * @return bool true if MerchantSKU list is non-empty
     */
    public function isSetMerchantSKU()
    {
        return count ($this->_fields['MerchantSKU']['FieldValue']) > 0;
    }

    /**
     * Gets the value of the ResponseGroup property.
     * 
     * @return string ResponseGroup
     */
    public function getResponseGroup() 
    {
        return $this->_fields['ResponseGroup']['FieldValue'];
    }

    /**
     * Sets the value of the ResponseGroup property.
     * 
     * @param string ResponseGroup
     * @return this instance
     */
    public function setResponseGroup($value) 
    {
        $this->_fields['ResponseGroup']['FieldValue'] = $value;
        return $this;
    }

    /**
     * Sets the value of the ResponseGroup and returns this instance
     * 
     * @param string $value ResponseGroup
     * @return Amazon_FWSInventory_Model_GetInventorySupplyRequest instance
     */
    public function withResponseGroup($value)
    {
        $this->setResponseGroup($value);
        return $this;
    }


    /**
     * Checks if ResponseGroup is set
     * 
     * @return bool true if ResponseGroup  is set
     */
    public function isSetResponseGroup()
    {
        return !is_null($this->_fields['ResponseGroup']['FieldValue']);
    }




}