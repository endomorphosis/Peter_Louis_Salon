<?php
/** 
 *  PHP Version 5
 *
 *  @category    Amazon
 *  @package     Amazon_FWSInventory
 *  @copyright   Copyright 2008 Amazon Technologies, Inc.
 *  @link        http://aws.amazon.com
 *  @license     http://aws.amazon.com/apache2.0  Apache License, Version 2.0
 *  @version     2009-07-31
 */
/******************************************************************************* 
 *    __  _    _  ___ 
 *   (  )( \/\/ )/ __)
 *   /__\ \    / \__ \
 *  (_)(_) \/\/  (___/
 * 
 *  Amazon FWS Inventory PHP5 Library
 *  Generated: Fri Nov 20 09:59:57 PST 2009
 * 
 */

/**
 *  @see Amazon_FWSInventory_Model
 */
require_once ('Model.php');  

    

/**
 * Amazon_FWSInventory_Model_SupplyDetail
 * 
 * Properties:
 * <ul>
 * 
 * <li>Quantity: int</li>
 * <li>SupplyType: string</li>
 * <li>EarliestAvailableToPickDateTime: Amazon_FWSInventory_Model_Timepoint</li>
 * <li>LatestAvailableToPickDateTime: Amazon_FWSInventory_Model_Timepoint</li>
 *
 * </ul>
 */ 
class Amazon_FWSInventory_Model_SupplyDetail extends Amazon_FWSInventory_Model
{


    /**
     * Construct new Amazon_FWSInventory_Model_SupplyDetail
     * 
     * @param mixed $data DOMElement or Associative Array to construct from. 
     * 
     * Valid properties:
     * <ul>
     * 
     * <li>Quantity: int</li>
     * <li>SupplyType: string</li>
     * <li>EarliestAvailableToPickDateTime: Amazon_FWSInventory_Model_Timepoint</li>
     * <li>LatestAvailableToPickDateTime: Amazon_FWSInventory_Model_Timepoint</li>
     *
     * </ul>
     */
    public function __construct($data = null)
    {
        $this->_fields = array (
        'Quantity' => array('FieldValue' => null, 'FieldType' => 'int'),
        'SupplyType' => array('FieldValue' => null, 'FieldType' => 'string'),
        'EarliestAvailableToPickDateTime' => array('FieldValue' => null, 'FieldType' => 'Amazon_FWSInventory_Model_Timepoint'),
        'LatestAvailableToPickDateTime' => array('FieldValue' => null, 'FieldType' => 'Amazon_FWSInventory_Model_Timepoint'),
        );
        parent::__construct($data);
    }

        /**
     * Gets the value of the Quantity property.
     * 
     * @return int Quantity
     */
    public function getQuantity() 
    {
        return $this->_fields['Quantity']['FieldValue'];
    }

    /**
     * Sets the value of the Quantity property.
     * 
     * @param int Quantity
     * @return this instance
     */
    public function setQuantity($value) 
    {
        $this->_fields['Quantity']['FieldValue'] = $value;
        return $this;
    }

    /**
     * Sets the value of the Quantity and returns this instance
     * 
     * @param int $value Quantity
     * @return Amazon_FWSInventory_Model_SupplyDetail instance
     */
    public function withQuantity($value)
    {
        $this->setQuantity($value);
        return $this;
    }


    /**
     * Checks if Quantity is set
     * 
     * @return bool true if Quantity  is set
     */
    public function isSetQuantity()
    {
        return !is_null($this->_fields['Quantity']['FieldValue']);
    }

    /**
     * Gets the value of the SupplyType property.
     * 
     * @return string SupplyType
     */
    public function getSupplyType() 
    {
        return $this->_fields['SupplyType']['FieldValue'];
    }

    /**
     * Sets the value of the SupplyType property.
     * 
     * @param string SupplyType
     * @return this instance
     */
    public function setSupplyType($value) 
    {
        $this->_fields['SupplyType']['FieldValue'] = $value;
        return $this;
    }

    /**
     * Sets the value of the SupplyType and returns this instance
     * 
     * @param string $value SupplyType
     * @return Amazon_FWSInventory_Model_SupplyDetail instance
     */
    public function withSupplyType($value)
    {
        $this->setSupplyType($value);
        return $this;
    }


    /**
     * Checks if SupplyType is set
     * 
     * @return bool true if SupplyType  is set
     */
    public function isSetSupplyType()
    {
        return !is_null($this->_fields['SupplyType']['FieldValue']);
    }

    /**
     * Gets the value of the EarliestAvailableToPickDateTime.
     * 
     * @return Timepoint EarliestAvailableToPickDateTime
     */
    public function getEarliestAvailableToPickDateTime() 
    {
        return $this->_fields['EarliestAvailableToPickDateTime']['FieldValue'];
    }

    /**
     * Sets the value of the EarliestAvailableToPickDateTime.
     * 
     * @param Timepoint EarliestAvailableToPickDateTime
     * @return void
     */
    public function setEarliestAvailableToPickDateTime($value) 
    {
        $this->_fields['EarliestAvailableToPickDateTime']['FieldValue'] = $value;
        return;
    }

    /**
     * Sets the value of the EarliestAvailableToPickDateTime  and returns this instance
     * 
     * @param Timepoint $value EarliestAvailableToPickDateTime
     * @return Amazon_FWSInventory_Model_SupplyDetail instance
     */
    public function withEarliestAvailableToPickDateTime($value)
    {
        $this->setEarliestAvailableToPickDateTime($value);
        return $this;
    }


    /**
     * Checks if EarliestAvailableToPickDateTime  is set
     * 
     * @return bool true if EarliestAvailableToPickDateTime property is set
     */
    public function isSetEarliestAvailableToPickDateTime()
    {
        return !is_null($this->_fields['EarliestAvailableToPickDateTime']['FieldValue']);

    }

    /**
     * Gets the value of the LatestAvailableToPickDateTime.
     * 
     * @return Timepoint LatestAvailableToPickDateTime
     */
    public function getLatestAvailableToPickDateTime() 
    {
        return $this->_fields['LatestAvailableToPickDateTime']['FieldValue'];
    }

    /**
     * Sets the value of the LatestAvailableToPickDateTime.
     * 
     * @param Timepoint LatestAvailableToPickDateTime
     * @return void
     */
    public function setLatestAvailableToPickDateTime($value) 
    {
        $this->_fields['LatestAvailableToPickDateTime']['FieldValue'] = $value;
        return;
    }

    /**
     * Sets the value of the LatestAvailableToPickDateTime  and returns this instance
     * 
     * @param Timepoint $value LatestAvailableToPickDateTime
     * @return Amazon_FWSInventory_Model_SupplyDetail instance
     */
    public function withLatestAvailableToPickDateTime($value)
    {
        $this->setLatestAvailableToPickDateTime($value);
        return $this;
    }


    /**
     * Checks if LatestAvailableToPickDateTime  is set
     * 
     * @return bool true if LatestAvailableToPickDateTime property is set
     */
    public function isSetLatestAvailableToPickDateTime()
    {
        return !is_null($this->_fields['LatestAvailableToPickDateTime']['FieldValue']);

    }




}