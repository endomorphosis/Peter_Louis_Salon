<table width="720"  style=" text-align: center; background-color: #b8b8b8;">
<tr>
<td>
<a href="template.php?f=jflazartigue">SHOP</a> 
</td>
<td>
<a href="template.php?f=jfl_history">HISTORY</a> 
</td>
<td>
<a href="template.php?f=jfl_news">NEWS</a> 
</td>
<td>
<a href="template.php?f=jfl_press">PRESS</a> 
</td>
</tr>
</table>
<table border="0" class="bgcolor2" cellpadding="0" cellspacing="0" width="716">
<tbody>
<tr>
<td width="40">&nbsp;</td>
<td>&nbsp;</td></tr>
<tr>
<td width="40">&nbsp;</td>
<td>
<table border="0" cellpadding="0" cellspacing="0" width="716">
<tbody>
<tr>
<td>
<img alt="Hair Services" src="hair_services_147x27.gif" border="0" height="27" width="147"> 
<p><font class="title"><b>Hair Loss and Preventive Hair Care</b></font></p>
<p><font class="bodytext">Have you ever noticed a clump of your hair in the drain after showering? Do you seem to "shed" on your clothing? If so relax, your not alone. In fact, it is normal to loose 50 to 100 hairs per day. A human scalp typically has between 90,000 to 140,000 hairs, 90% of which are constantly in a growing cycle that lasts between two and seven years. The remaining 10% of the hair is in a resting phase that lasts two to three months. At the end of the resting phase, the hair is shed. Each person has a finite number of hair follicles. When a hair sheds, it is replaced by a new hair from the same follicle, and the growing cycle starts again. Thus, a certain amount of hair loss is natural and inevitable and should not be cause for concern.<br>If you believe your hair loss to be in excess of 100 hairs per day, we <br>recommend that you seek the professional counsel of a physician, as abnormal hair loss can be an indication of other organic/internal health problems. Occasionally maladies such as thyroid disease, fungus infection, and influenza cause hair loss. (this type of hair loss is reversible). Often times, people who have under gone major surgery or experienced chronic illness will notice increased hair loss. Such hair loss is the result of the severe disruption in the body's systematic functioning; and, in most cases, the hair grows back. Certain Medications such as those frequently used to treat arthritis, depression, and heart problems, as well as birth control pills, can also cause hair to thin. Many women experience a large amount of hair "shedding" after childbirth. Due to an increase in estrogen, women do not lose as much hair as usual during pregnancy - hence causing the temporary hair thickening effect that a majority of women tend to note during these nine months. Upon giving birth, however, hormone levels normalize, and women lose the hair that they kept during pregnancy in addition to the hair that they would lose normally. To keep the hair and scalp in optimal condition and to avoid unnecessary hair loss, we recommend the following:</font></p>

<p><font class="bodytext">1. Make sure that your diet is sufficient in protein and iron. As hair itself is largely comprised of a form of protein, it is imperative to consume an adequate amount of protein in one's diet to maintain normal hair production. ( Protein is found in eggs, tofu, dried beans, nuts, grains, fish, poultry, red meat, yogurt and certain cheeses.) In addition, iron deficiency has been know to produce hair loss; proper iron intake is thus integral to basic hair production. (Sources of iron include lentils, brown rice, dates, millet, prunes and red meat.)<br><br>2. Limit performing potentially-damaging cosmetic procedures to the hair, without the advice of a true hair care professional.<br><br>3. Do not over-cleanse the hair. Superfluous washing or using shampoos that are too strong can strip the scalp and hair of necessary oils that help to maintain a proper hair and scalp condition. ( The amount that a shampoo lathers is often a good indication of is strength. In general, stronger shampoos are formulated with a greater ratio of surfactants - the ingredients that cause a product to foam to conditioning agents.) These low grade shampoos cause dry scalp, which looks like dandruff. Which in turn makes a certain mass market dandruff shampoo the #1 selling brand.<br><br>4. After shampooing, use a conditioner with an effective detangling agent in order to make the hair easier to comb and style. Hair is most fragile when wet and is therefore more susceptible to breakage at this time. In addition, vigorous towel-drying of the hair and rough combing (use a wooden comb) or brushing ( use a natural boar bristle brush) can cause breakage and further damage hair.</font></p>
<p align="left"><font class="bodytext">5. Avoid wearing hair in styles that can pull the hair, such as tight <br>ponytails and braids. If these styles are worn, they should be alternated with looser styles. In addition, we suggest using gentle cloth-covered fasteners instead of rubber bands to secure the hair. Rubber bands tear and rip the hair.</font></p>
<hr>

<p><font class="title"><b>The Peter Louis Hair Growth Juice</b></font></p>
<p><font class="bodytext"><a href="http://peterlouissalon.com/template.php?f=prod_detail&pid=13&ref=jfl"><img style="border-style: solid; border-color: rgb(255, 255, 255);" alt="" src="supplement.jpg" align="left" border="0" height="222" width="150"></a>Mix Equal Parts of Celery, Cucumber, Cabbage add other small amounts of all organic greens, Kale, Parsley, Dandelion, Swiss chard and 1-3 tablespoons of <b>Silica Mineral Supplement</b></font></p>
<p><font class="bodytext"><u>No Wheat No Dairy</u></font></p>

<p><font class="bodytext">Drink one to three 8oz glasses per day!</font></p>
<p><font class="bodytext">My clients have also had excellent hair re-growth drinking a shot of wheatgrass juice per day. I have seen at least 25% more hair in three months.</font></p>
<p><font class="bodytext"><a href="template.php?f=llht">What is laser light hair therapy?</a></font></p>
<p><font class="bodytext">Peter</font></p>
<p></p>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>