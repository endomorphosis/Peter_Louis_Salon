<? include($template_include_file);?>
<table width="720" cellspacing="0" cellpadding="0" style=" background-repeat:repeat-x; max-width: 720px;">
<tr>
<td  style="background-color: #d7ede0;">

<p style="color: #000000; font-size: 12px; margin-left: 1em; margin-right: 1em;">
Jean-Francois Lazartigue is one of the most renowned hair care specialists in France. He began his career as a hairdresser and opened his first salon in "Rue de Courcelles", Paris, in December 1963.
</p>
<p style="color: #000000; font-size: 12px; margin-left: 1em; margin-right: 1em;">
He has always been fascinated by hairdressing techniques and interested in the health of the hair. By 1972 his salon had been established as a prominent center in Paris for relaxing the coarsest textures of hair. Today it is still a favorite for French editors and for the most demanding clients.
</p>
<p style="color: #000000; font-size: 12px; margin-left: 1em; margin-right: 1em;">
Driven by a true passion for hair, Mr. Lazartigue came across many unaddressed hair issues such as overprocessed hair damages by oxydation color, perms, straightening, ironing or even daily blow-drying. With the purpose of solving these issues, he created his own products to repair, protect and embellish hair. 
In 1976: the j.f. lazartigue brand was born creating tremendous innovative products.
 </p>
<p style="color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;">
<ul style="color: #000000; font-size: 12px; margin-left: 1em; margin-right: 1em;">
<li>   the treatment cream shampoo with collagen </li>
<li>     the pre-shampoo cream with shea butter </li>
<li>     the vita-cream with milk proteins </li>
</ul> </p>
<p style="color: #000000; font-size: 12px; margin-left: 1em; margin-right: 1em;">
Facing an outstanding success, Mr Lazartigue developed a number of prominent hair treatment centers in France where he offers a hair & scalp analysis. This computerized microscope determines a personalized prescription of treatments for each costumer. He was also the first to incorporate, into hair care, the same ingredients as in skin care (AHA, collagen, vitamins).
 </p>
<p style="color: #000000; font-size: 12px; margin-left: 1em; margin-right: 1em;">
Mr. Lazartigue's philosophy has always been to select the best available ingredients and use them in high concentrations. All of them are tested in the J.F. Lazartigue laboratories in France as well as in his salons & treatment centers.
 </p>
<p style="color: #000000; font-size: 12px; margin-left: 1em; margin-right: 1em;">
He is a unique entrepreneur, focusing his strength and consciousness on the needs of the customers. His breakthrough products has his clients addicted to his treatments and, he has revolutionized the hair care industry with constant innovation. 
 </p>
<p style="color: #000000; font-size: 12px; margin-left: 1em; margin-right: 1em;">
In 1987, he brought his complete line to the USA. Shortly thereafter, Peter Louis Salon has been proud to distribute all of j.f. lazartigue's latest haircare innovations:
</p>

</td>
<td  style="">
<img src="/images/jflside.png"/>
</td>
</tr>
<tr>
<td colspan="2">
<img src="./images/jflbottom.png"/> 
</td>
</tr>
</table>
 
