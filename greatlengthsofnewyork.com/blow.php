<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td valign="top">&nbsp;</td>
<td valign="top">
<table class="ae_noborder" id="table2" border="0" cellpadding="0" cellspacing="0" width="100%">
<tbody>
<tr>
<td>
<table class="ae_noborder" id="table3" border="0" cellpadding="0" cellspacing="0" width="100%">
<tbody>
<tr>
<td valign="top">
<table class="ae_noborder" id="table4" border="0" cellpadding="0" cellspacing="0" width="100%">
<tbody>
<tr>
<td valign="top">
<table class="ae_noborder" id="table5" border="0" cellpadding="5" cellspacing="0" width="100%">
<tbody>

<tr valign="top">
<td>&nbsp;<img src="hair_services_147x27.gif" border="0" height="27" width="147"> 
<p><font class="title"><b>A Blow Dry Make Over</b></font></p>
<p><font class="bodytext">1. Gently towel dry just-washed hair.</font></p>
<p><font class="bodytext">2. Comb a Quarter size portion of Phytodefrisant through your hair with a large tooth comb. Then remove 80% of the moisture with your blow-dryer or less if you have very dry curly hair.</font></p>
<p><font class="bodytext">3. Place your hair into three sections, two at the sides and one at the back. Clip two sections up.</font></p>
<p><font class="bodytext">4. Select a one inch rectangular portion of hair from the unclipped section.</font></p>
<p><font class="bodytext">5. Use an appropriate round brush (small if very curly or large if wavy) and, beginning at the roots, gently pull the hair with the brush holding it straight while you blow it completely dry, section by section.</font></p>
<p><font class="bodytext">6. Hair is like plastic. if you heat it up, it becomes pliable and when it cools down becoming completely dry it will take the shape you have given it...</font></p>
<p><font class="bodytext">7. Always keep the tension consistent. Also make sure to distribute the heat from the dryer evenly over the section of hair, blowing down from the roots to the ends.</font></p>

<p><font class="bodytext">8. Completely dry hair then blow with some cool air. If your hair is very dry rub a dime size or smaller portion of Phyto 7 through the ends and blow your hair a little more.</font></p>
<p><font class="small">Always point the dryer down as you are blow-drying the hair; blowing hair upward can cause frizz.</font></p></td>
<td align="middle"><img src="images/MK-before.jpg" border="0" height="301" width="200"><font class="bodytext"><br>Before</font> 
<p><img src="blowdry.jpg" border="0" height="206" width="134"></p>
<p><img src="MK-after.jpg" border="0" height="301" width="200"><font class="bodytext"><br>After</font></p>
<p><font class="small">Special thanks to our model Meryl</font></p></td></tr>
<tr valign="top">
<td>&nbsp;</td>
<td align="middle">&nbsp;</td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table>
<p></p></td></tr></tbody></table></td>
<td valign="top">&nbsp;</td></tr></table>
