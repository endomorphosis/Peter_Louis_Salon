<style> a{ color: #000; text-decoration: underline; }</style>
<span><? include($template_include_file);?></span>
<table width="720" cellspacing="0" cellpadding="0" style=" background-repeat:repeat-x; max-width: 720px;">
<tr>
<td style="background-image: url('./images/glbg1.png') ;" style="background-image: url('./images/hdny_gradient.png');" colspan="3">
<p style="text-align: center; color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;"> Great Lengths<sup>&reg;</sup> products found at<br/> <a style="text-align: center; text-decoration: underline; color: #000000; font-size: 14px; margin-left: 1em; margin-right: 1em;" href="http://www.peterlouissalon.com"> Peter Louis Salon </a><br/> 143 E 57th Street<br/> New York, NY 10022 
212.319.0019</p>
</td>
</tr>
<tr>
<td colspan="2"  style="background-image: url('./images/glbg1.png') ;">
<p style="color: #000000; font-size: 11px; margin-left: 1em; margin-right: 1em;">
Great Lengths' birthplace is London, one of the world's greatest fashion centres. David Gold, founder of Great Lengths, had set himself the goal to develop a hair extension system as simple to use as cutting or drying. When in 1991 he had developed and patented the Great Lengths principle he had simultaneously discovered the first method of thickening thin and fine hair in a very simple way.
</p>
<p style="color: #000000; font-size: 11px; margin-left: 1em; margin-right: 1em;">
This invention set off a revolution in 1991.  Soon afterwards, <a href="http://www.peterlouissalon.com">Peter Louis Salon</a> was on of the first in New York City to offer the Great Lengths hair extensions and hair thickening system. Great Lengths has become a sign of our times: both men and women prefer the non aggressive technical solutions and natural products offered at <a href="http://www.peterlouissalon.com">Peter Louis Salon</a>.
</p>
<p style="color: #000000; font-size: 11px; margin-left: 1em; margin-right: 1em;">
Great Lengths and <a href="http://www.peterlouissalon.com">Peter Louis Salon</a> continue to stay one step ahead! So far no other system has developed a similarly non compromising technique of attaching additional strands to the customer's own hair. It goes without saying that only 100% natural human hair of the highest quality is used at <a href="http://www.peterlouissalon.com">Peter Louis Salon</a>.  It is no wonder that Great Lengths has established itself as a pioneer in the worldwide market of hair extensions and hair thickening.
</p>
<p style="color: #000000; font-size: 11px; margin-left: 1em; margin-right: 1em;">
<a href="http://www.peterlouissalon.com">Peter Louis Salon</a> is proud to offer you the unparalleled technique and quality that is Great Lengths - 100% Human Hair Extensions.
</p>
</td>
<td align="right" style=" background-image: url('./images/glbg1.png') ;">
<img  src="/images/greatlengths01.png"/>
</td>
</tr>
<tr>
<td  style="background-image: url('./images/glbg2.png') ;">
<img src="/images/grearlengths00.png"/>
</td>
<td colspan="2" style=" max-width: 417px; background-image: url('./images/glbg2.png') ;">
<p style="color: #000000; font-size: 11px; margin-left: 1em; margin-right: 1em;">
Today top hairdressers in Great Britain, Italy, France, Austria, Germany, Scandinavia, Holland, Belgium, Greece, Spain, Portugal, Switzerland, Luxembourg, the U. S. A., Canada, Brazil, Australia, New Zealand, Russia, Hong Kong, the United Arab Emirates, South Africa, the Caribbean, Japan, Mexico, Thailand and many more countries are offering Great Lengths with increasing success.
</p>
<p style="color: #000000; font-size: 11px; margin-left: 1em; margin-right: 1em;">
Great Lengths' philosophy is to remedy the lack of thick hair in a way which is as simple as dyeing it if you want to change its colour. Considering the fact that people who do not have a lot of hair generally have very fine hair at the same time, the Great Lengths' programme offers various thicknesses
of natural hair strands for attaching to fine hair which can be attached in a practically invisible way.
</p>
</td>
</tr>
</table>
