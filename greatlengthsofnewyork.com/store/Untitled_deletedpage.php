<table width="100%" cellpadding="4" cellspacing="0" border="0" class="outline">
<tr valign="top"><td valign=top class="stripe-one"><font class="small-alt"><b>Rene Furterer</b></font></td>
</tr>
      
      <tr valign=top>
<td valign=top align=left class="outline">
<font class="title">&nbsp;<!-- rec 131 -->
&nbsp;<BR>
<a name="131"></a>
<!-- Default Product Template -->
<img  src="http://peterlouissalon.com/shopsite_sc/store/html/media/ReneFurterersoftgel.jpg" alt="ReneFurterersoftgel.jpg" width="82" height="180" vspace="3" hspace="3" border="0" align=left>

<b>Rene Furterer Styling Gel - Strong Hold </b><br>
<b>$18.00
</b><br>
This gel is ideal when extra volume is required, leaving hair soft and supple. Easy restyling is achieved by simply brushing. Leaves no undesirable residuals.
<br>

<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;dbname=products&amp;itemnum=131&amp;function=add">[Add to Cart]</a>&nbsp;
<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;function=show">[View Cart]</a><BR>
</font>
        <br />
        </td>
      </tr>
      <tr valign=top>
<td valign=top align=left class="outline">
<font class="title">&nbsp;<!-- rec 133 -->
&nbsp;<BR>
<a name="133"></a>
<!-- Default Product Template -->
<img  src="http://peterlouissalon.com/shopsite_sc/store/html/media/ReneFurtererwax.jpg" alt="ReneFurtererwax.jpg" width="168" height="180" vspace="3" hspace="3" border="0" align=left>

<b>Rene Furterer Styling Wax Sheer Shine (Jar)</b><br>
<b>$18.00
</b><br>
This finishing wax gives your hair an exceptional gloss underlining the movements of your hair.<br>

<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;dbname=products&amp;itemnum=133&amp;function=add">[Add to Cart]</a>&nbsp;
<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;function=show">[View Cart]</a><BR>
</font>
        <br />
        </td>
      </tr>
      <tr valign=top>
<td valign=top align=left class="outline">
<font class="title">&nbsp;<!-- rec 134 -->
&nbsp;<BR>
<a name="134"></a>
<!-- Default Product Template -->
<img  src="http://peterlouissalon.com/shopsite_sc/store/html/media/RFsunoil.JPG" alt="RFsunoil.JPG" width="105" height="308" vspace="3" hspace="3" border="0" align=left>

<b>Rene Furterer Nutritive Oil Spray - Very high protection 2.5 fl.oz</b><br>
<b>$18.00
</b><br>
In order to effectively protect hair against UV radiation, sea and pool water throughout the day, this water resistant Oil offers Hair Protection Factor KPF 90. Guarantees 90% protection of hair keratin, that gives hair nourishment and shine. Apply only once.<br>

<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;dbname=products&amp;itemnum=134&amp;function=add">[Add to Cart]</a>&nbsp;
<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;function=show">[View Cart]</a><BR>
</font>
        <br />
        </td>
      </tr>
      <tr valign=top>
<td valign=top align=left class="outline">
<font class="title">&nbsp;<!-- rec 135 -->
&nbsp;<BR>
<a name="135"></a>
<!-- Default Product Template -->
<img  src="http://peterlouissalon.com/shopsite_sc/store/html/media/ReneFurterersunmousse.jpg" alt="ReneFurterersunmousse.jpg" width="200" height="260" vspace="3" hspace="3" border="0" align=left>

<b>Rene Furterer Anti-dryness Protective Sun Mousse </b><br>
<b>$18.00
</b><br>
The sun mousse protects your hair with a KPF (Keratin Protector Factor) of 30. High protection Light texture Natural effect 
  <br>

<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;dbname=products&amp;itemnum=135&amp;function=add">[Add to Cart]</a>&nbsp;
<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;function=show">[View Cart]</a><BR>
</font>
        <br />
        </td>
      </tr>
      <tr valign=top>
<td valign=top align=left class="outline">
<font class="title">&nbsp;<!-- rec 136 -->
&nbsp;<BR>
<a name="136"></a>
<!-- Default Product Template -->
<img  src="http://peterlouissalon.com/shopsite_sc/store/html/media/ReneFurterersunmask.jpg" alt="ReneFurterersunmask.jpg" width="200" height="260" vspace="3" hspace="3" border="0" align=left>

<b>Rene Furterer After-Sun Mask </b><br>
<b>$22.00
</b><br>
The after-sun repairing mask deeply restructures hair damaged after sun exposure. Nourishes and restructures the hair shaft and Detangles instantly, thanks to its nutrient substances Shea butter and Essential oil of sesame. 
<br>

<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;dbname=products&amp;itemnum=136&amp;function=add">[Add to Cart]</a>&nbsp;
<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;function=show">[View Cart]</a><BR>
</font>
        <br />
        </td>
      </tr>
      <tr valign=top>
<td valign=top align=left class="outline">
<font class="title">&nbsp;<!-- rec 137 -->
&nbsp;<BR>
<a name="137"></a>
<!-- Default Product Template -->
<img  src="http://peterlouissalon.com/shopsite_sc/store/html/media/ReneFurterersunbaum.jpg" alt="ReneFurterersunbaum.jpg" width="200" height="260" vspace="3" hspace="3" border="0" align=left>

<b>Rene Furterer After-Sun Hair Balm </b><br>
<b>$17.00
</b><br>
The after-sun hair balm nourishes dry ends damaged after sun exposure. Repairs dry ends and Detangles instantly.
 <br>

<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;dbname=products&amp;itemnum=137&amp;function=add">[Add to Cart]</a>&nbsp;
<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;function=show">[View Cart]</a><BR>
</font>
        <br />
        </td>
      </tr>
      <tr valign=top>
<td valign=top align=left class="outline">
<font class="title">&nbsp;<!-- rec 138 -->
&nbsp;<BR>
<a name="138"></a>
<!-- Default Product Template -->
<img  src="http://peterlouissalon.com/shopsite_sc/store/html/media/ReneFurterersunshowergel.jpg" alt="ReneFurterersunshowergel.jpg" width="200" height="260" vspace="3" hspace="3" border="0" align=left>

<b>Rene Furterer After-Sun Body and Hair Shower Gel </b><br>
<b>$22.00
</b><br>
<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;dbname=products&amp;itemnum=138&amp;function=add">[Add to Cart]</a>&nbsp;
<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;function=show">[View Cart]</a><BR>
</font>
        <br />
        </td>
      </tr>
      <tr valign=top>
<td valign=top align=left class="outline">
<font class="title">&nbsp;<!-- rec 139 -->
&nbsp;<BR>
<a name="139"></a>
<!-- Default Product Template -->
<img  src="http://peterlouissalon.com/shopsite_sc/store/html/media/ReneFurterersunshamp.jpg" alt="ReneFurterersunshamp.jpg" width="200" height="260" vspace="3" hspace="3" border="0" align=left>

<b>Rene Furterer After-Sun Shampoo </b><br>
<b>$17.00
</b><br>
The after-sun shampoo gently cleanses the hair, repairing damages caused by the sun. Nourishes the hair removes salt and chlorine. Formulated with Shea butter, Lecithin of egg and soy & Essential oil of lemon. <br>

<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;dbname=products&amp;itemnum=139&amp;function=add">[Add to Cart]</a>&nbsp;
<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;function=show">[View Cart]</a><BR>
</font>
        <br />
        </td>
      </tr>
      <tr valign=top>
<td valign=top align=left class="outline">
<font class="title">&nbsp;<!-- rec 142 -->
&nbsp;<BR>
<a name="142"></a>
<!-- Default Product Template -->
<img  src="http://peterlouissalon.com/shopsite_sc/store/html/media/ReneFurterervitalfanthinning.jpg" alt="ReneFurterervitalfanthinning.jpg" width="200" height="132" vspace="3" hspace="3" border="0" align=left>

<b>Rene Furterer Vitalfan for Thinning Hair</b><br>
<b>$25.00
</b><br>
A dietary supplement suitable for those suffering hair loss. Vital fan hair loss treatment, contains oil extracted from squash, methionine, cystine, vitaminC, zinc, beta-carotene and vitamin E. These essential substances are known to help stimulate hair growth, improve microcirculation of the scalp to give the hair volume, vigor and shine.
60 Capsules<br>

<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;dbname=products&amp;itemnum=142&amp;function=add">[Add to Cart]</a>&nbsp;
<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;function=show">[View Cart]</a><BR>
</font>
        <br />
        </td>
      </tr>
      <tr valign=top>
<td valign=top align=left class="outline">
<font class="title">&nbsp;<!-- rec 143 -->
&nbsp;<BR>
<a name="143"></a>
<!-- Default Product Template -->
<img  src="http://peterlouissalon.com/shopsite_sc/store/html/media/Rene_Furterer_naturia.jpg" alt="Rene_Furterer_naturia.jpg" width="131" height="253" vspace="3" hspace="3" border="0" align=left>

<b>Rene Furterer Naturia Shampoo 10.14 fl.oz</b><br>
<b>$28.00
</b><br>
New Larger Size!
Gel shampoo for frequent use. It is gentle enough to use as often as needed.
<br>

<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;dbname=products&amp;itemnum=143&amp;function=add">[Add to Cart]</a>&nbsp;
<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;function=show">[View Cart]</a><BR>
</font>
        <br />
        </td>
      </tr>
      <tr valign=top>
<td valign=top align=left class="outline">
<font class="title">&nbsp;<!-- rec 144 -->
&nbsp;<BR>
<a name="144"></a>
<!-- Default Product Template -->
<img  src="http://peterlouissalon.com/shopsite_sc/store/html/media/Rene FurtererAsteraSoothin.jpg" alt="Rene FurtererAsteraSoothin.jpg" width="122" height="180" vspace="3" hspace="3" border="0" align=left>

<b>Rene Furterer Astera Soothing Complex with Essential Oils</b><br>
<b>$40.00
</b><br>
Prepare the scalp with the soothing Astera Complex, with its cool essential oils. Avery gentle supplement, it gives an immediate and lasting relief which is calming and refreshing, thanks to its natural ingredients: Menthol, Eucalyptus,Camphor, vitamins A, E and F. 1.69 fl oz

<br>

<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;dbname=products&amp;itemnum=144&amp;function=add">[Add to Cart]</a>&nbsp;
<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;function=show">[View Cart]</a><BR>
</font>
        <br />
        </td>
      </tr>
      <tr valign=top>
<td valign=top align=left class="outline">
<font class="title">&nbsp;<!-- rec 145 -->
&nbsp;<BR>
<a name="145"></a>
<!-- Default Product Template -->
<img  src="http://peterlouissalon.com/shopsite_sc/store/html/media/Rene FurtererAsteraSoothishamp.jpg" alt="Rene FurtererAsteraSoothishamp.jpg" width="94" height="180" vspace="3" hspace="3" border="0" align=left>

<b>Rene Furterer Astera Soothing  Shampoo </b><br>
<b>$20.00
</b><br>
Thanks to its natural active ingredients: the extract of Aster, vitamin B5, and plant-based lipo-proteins , it gently cleanses and leases hair easy and pleasant to comb. The Astera Soothing Milk Shampoo durably softens and soothes the scalp. Perfectly tolerated (neutralpH), easy to rinse out, it can be used as often as desired. 5.07 fl oz
<br>

<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;dbname=products&amp;itemnum=145&amp;function=add">[Add to Cart]</a>&nbsp;
<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;function=show">[View Cart]</a><BR>
</font>
        <br />
        </td>
      </tr>
      <tr valign=top>
<td valign=top align=left class="outline">
<font class="title">&nbsp;<!-- rec 274 -->
&nbsp;<BR>
<a name="274"></a>
<!-- Default Product Template -->
<img  src="http://peterlouissalon.com/shopsite_sc/store/html/media/Renefurtereremulsion.jpg" alt="Renefurtereremulsion.jpg" width="250" height="250" vspace="3" hspace="3" border="0" align=left>

<b>Rene Furterer Emulsion Controle 8.5oz</b><br>
<b>$22.00
</b><br>
Emulsion Controle combats frizz by shielding the hair from humidity as it smoothes and softens hair, maintaining the curl while preventing frizz.

To use: Dispense a nickel-sized portion in the palm of your hand for average length hair, rub hands together to evenly disperse over palms, then work through hair from ends to roots.
<br>

<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;dbname=products&amp;itemnum=274&amp;function=add">[Add to Cart]</a>&nbsp;
<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;function=show">[View Cart]</a><BR>
</font>
        <br />
        </td>
      </tr>
      <tr valign=top>
<td valign=top align=left class="outline">
<font class="title">&nbsp;<!-- rec 432 -->
&nbsp;<BR>
<a name="432"></a>
<!-- Default Product Template -->
<img  src="http://peterlouissalon.com/shopsite_sc/store/html/media/ReneFurtererEmulsion507.jpg" alt="ReneFurtererEmulsion507.jpg" width="250" height="250" vspace="3" hspace="3" border="0" align=left>

<b>Rene Furterer Emulsion Controle 5.07oz</b><br>
<b>$17.00
</b><br>
Emulsion Controle combats frizz by shielding the hair from humidity as it smoothes and softens hair, maintaining the curl while preventing frizz. To use: Dispense a nickel-sized portion in the palm of your hand for average length hair, rub hands together to evenly disperse over palms, then work through hair from ends to roots. 
<br>

<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;dbname=products&amp;itemnum=432&amp;function=add">[Add to Cart]</a>&nbsp;
<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;function=show">[View Cart]</a><BR>
</font>
        <br />
        </td>
      </tr>
      <tr valign=top>
<td valign=top align=left class="outline">
<font class="title">&nbsp;<!-- rec 277 -->
&nbsp;<BR>
<a name="277"></a>
<!-- Default Product Template -->
<img  src="http://peterlouissalon.com/shopsite_sc/store/html/media/rfihfspray.jpg" alt="rfihfspray.jpg" width="250" height="250" vspace="3" hspace="3" border="0" align=left>

<b>Rene Furterer Laque De Finition 6.9oz</b><br>
<b>$24.00
</b><br>
Finishing Spray Holds hair instantly without stiffness. Easy restyling is achieved by simply brushing. Leaves no undesirable residuals. The vitamin B5 and the Anatide original molecule prevent dehydration and provide transparent shine. Aerosol 6.9oz<br>

<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;dbname=products&amp;itemnum=277&amp;function=add">[Add to Cart]</a>&nbsp;
<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;function=show">[View Cart]</a><BR>
</font>
        <br />
        </td>
      </tr>
      <tr valign=top>
<td valign=top align=left class="outline">
<font class="title">&nbsp;<!-- rec 275 -->
&nbsp;<BR>
<a name="275"></a>
<!-- Default Product Template -->
<img  src="http://peterlouissalon.com/shopsite_sc/store/html/media/Renefurterergloss" alt="Renefurterergloss" align=left>

<b>Rene Furterer Spray Gloss 3.38 oz</b><br>
<b>$18.00
</b><br>
When sprayed over dry hair, Spray Gloss creates a soft, "mirror-like" effect on each strand to enhance shine and hair texture. Infused with Anatide and vitamin B5 to protect and condition.
<br>

<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;dbname=products&amp;itemnum=275&amp;function=add">[Add to Cart]</a>&nbsp;
<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;function=show">[View Cart]</a><BR>
</font>
        <br />
        </td>
      </tr>
      <tr valign=top>
<td valign=top align=left class="outline">
<font class="title">&nbsp;<!-- rec 323 -->
&nbsp;<BR>
<a name="323"></a>
<!-- Default Product Template -->
<img  src="http://peterlouissalon.com/shopsite_sc/store/html/media/rfkaritenorinse.jpg" alt="rfkaritenorinse.jpg" width="250" height="250" vspace="3" hspace="3" border="0" align=left>

<b>Rene Furterer Karite No Rinse Nutritive concentrate 3.3oz</b><br>
<b>$20.00
</b><br>
Rich in Karite oil, highly nourishing and restructuring, and in vitamin B5, a natural moisturiser. Karite nutritive concentrate deeply acts to repair and protect the dryest hair and to make it exceptionally soft and shiny. Its conditioning agents straighten and softly control rebellious hair. The hair becomes easy to style, with exceptional shine and sheer.<br>

<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;dbname=products&amp;itemnum=323&amp;function=add">[Add to Cart]</a>&nbsp;
<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;function=show">[View Cart]</a><BR>
</font>
        <br />
        </td>
      </tr>
      <tr valign=top>
<td valign=top align=left class="outline">
<font class="title">&nbsp;<!-- rec 322 -->
&nbsp;<BR>
<a name="322"></a>
<!-- Default Product Template -->
<img  src="http://peterlouissalon.com/shopsite_sc/store/html/media/rfkaritecrm.jpg" alt="rfkaritecrm.jpg" width="250" height="250" vspace="3" hspace="3" border="0" align=left>

<b>Rene Furterer Karite Nourishing conditioning cream 3.4oz</b><br>
<b>$22.00
</b><br>
Karite Nourishing Conditioning Cream  

Suitable for dry and lifeless hair, this cream rebuilds the dry ends, while it nourishes and protects the hair. Contains shea butter and quince seed. Use weekly as an intensive nourishing treatment or as often as needed if hair is particularly dry.
<br>

<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;dbname=products&amp;itemnum=322&amp;function=add">[Add to Cart]</a>&nbsp;
<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;function=show">[View Cart]</a><BR>
</font>
        <br />
        </td>
      </tr>
      <tr valign=top>
<td valign=top align=left class="outline">
<font class="title">&nbsp;<!-- rec 321 -->
&nbsp;<BR>
<a name="321"></a>
<!-- Default Product Template -->
<img  src="http://peterlouissalon.com/shopsite_sc/store/html/media/RFKarite.jpg" alt="RFKarite.jpg" width="250" height="250" vspace="3" hspace="3" border="0" align=left>

<b>Rene Furterer Karite Nutritive Shampoo 5.07oz</b><br>
<b>$22.00
</b><br>
Karite Shampoo  

Specifically designed to treat extremely dry hair and scalp, nutritive Karite Shampoo is extremely hydrating and restructuring to the hair, and nourishing to the scalp. Infused with rich, moisturizing conditioners, your hair will quickly regain its natural sheen and bounce.
<br>

<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;dbname=products&amp;itemnum=321&amp;function=add">[Add to Cart]</a>&nbsp;
<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;function=show">[View Cart]</a><BR>
</font>
        <br />
        </td>
      </tr>
      <tr valign=top>
<td valign=top align=left class="outline">
<font class="title">&nbsp;<!-- rec 449 -->
&nbsp;<BR>
<a name="449"></a>
<!-- Default Product Template -->
<img  src="http://peterlouissalon.com/shopsite_sc/store/html/media/carthamenorinsecream.jpg" alt="carthamenorinsecream.jpg" width="250" height="250" vspace="3" hspace="3" border="0" align=left>

<b>Rene Furterer Carthame No Rinse Protective Cream 2.56 oz</b><br>
<b>$22.00
</b><br>
Carthame No Protective Cream has been specially formulated for dry hair with split ends. Rich in Safflower oil and coating agents, it helps protect and repair damaged ends. Oil free and easy to use it will leave the hair softer, silkier, and more manageable, Carthame No Rinse protective cream can be used as part of the Rene Furterer personalized treatment program for dry hair.<br>

<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;dbname=products&amp;itemnum=449&amp;function=add">[Add to Cart]</a>&nbsp;
<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;function=show">[View Cart]</a><BR>
</font>
        <br />
        </td>
      </tr>
      <tr valign=top>
<td valign=top align=left class="outline">
<font class="title">&nbsp;<!-- rec 448 -->
&nbsp;<BR>
<a name="448"></a>
<!-- Default Product Template -->
<img  src="http://peterlouissalon.com/shopsite_sc/store/html/media/carthamenorinsecream.jpg" alt="carthamenorinsecream.jpg" width="250" height="250" vspace="3" hspace="3" border="0" align=left>

<b>Rene Furterer Carthame No Rinse Protective Cream 8.5 Oz</b><br>
<b>$36.00
</b><br>
Carthame No Protective Cream has been specially formulated for dry hair with split ends. Rich in Safflower oil and coating agents, it helps protect and repair damaged ends. Oil free and easy to use it will leave the hair softer, silkier, and more manageable, Carthame No Rinse protective cream can be used as part of the Rene Furterer personalized treatment program for dry hair.<br>

<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;dbname=products&amp;itemnum=448&amp;function=add">[Add to Cart]</a>&nbsp;
<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;function=show">[View Cart]</a><BR>
</font>
        <br />
        </td>
      </tr>
      <tr valign=top>
<td valign=top align=left class="outline">
<font class="title">&nbsp;<!-- rec 229 -->
&nbsp;<BR>
<a name="229"></a>
<!-- Default Product Template -->
<img  src="http://peterlouissalon.com/shopsite_sc/store/html/media/RFpurifyingshp.JPG" alt="RFpurifyingshp.JPG" width="164" height="317" vspace="3" hspace="3" border="0" align=left>

<b>Rene Furterer  Curbicia Concentrated Purifying Shampoo 5.07 fl.oz</b><br>
<b>$22.00
</b><br>
High shampooing frequency means that your scalp is frequently oily and your hair looks dull and flat. Pleasantly scented with deep-cleansing essential oils, this shampoo made with a high concentration of Curbicia extract eliminates excess sebum from the scalp. For long term benefits and less frequent shampooing, we recommend that you combine this shampoo with CURBICIA Purifying argilla mask. Your hair will regain natural lightness and volume.
<br>

<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;dbname=products&amp;itemnum=229&amp;function=add">[Add to Cart]</a>&nbsp;
<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;function=show">[View Cart]</a><BR>
</font>
        <br />
        </td>
      </tr>
      <tr valign=top>
<td valign=top align=left class="outline">
<font class="title">&nbsp;<!-- rec 242 -->
&nbsp;<BR>
<a name="242"></a>
<!-- Default Product Template -->
<img  src="http://peterlouissalon.com/shopsite_sc/store/html/media/rfcarthameserum.jpg" alt="rfcarthameserum.jpg" width="250" height="250" vspace="3" hspace="3" border="0" align=left>

<b>Rene Furterer Carthame Pre-Shampoo hydro-nutritive serum (6 tubes)</b><br>
<b>$30.00
</b><br>
The CARTHAME Hydro-nutritive serum protects and replenishes moisture to dry hair and scalp, as well as provides comfort and suppleness, thanks to its pure oil complex (including safflower Carthame oil), which is rich in essential fatty acids and is combined with revitalizing vitamins (B5 and E). Its specific moisture-balance to the hair and scalp's moisture capital. Its fine and fluid texture leaves your hair feeling airy light and looking radiantly healthy. This pre-shampoo serum prepares your hair for subsequent treatments and  thus allows it to fully benefit from other CARTHAME products.
<br>

<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;dbname=products&amp;itemnum=242&amp;function=add">[Add to Cart]</a>&nbsp;
<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;function=show">[View Cart]</a><BR>
</font>
        <br />
        </td>
      </tr>
      <tr valign=top>
<td valign=top align=left class="outline">
<font class="title">&nbsp;<!-- rec 243 -->
&nbsp;<BR>
<a name="243"></a>
<!-- Default Product Template -->
<img  src="http://peterlouissalon.com/shopsite_sc/store/html/media/rfsilkeningshp.jpg" alt="rfsilkeningshp.jpg" width="250" height="250" vspace="3" hspace="3" border="0" align=left>

<b>Rene Furterer Fioravanti ANTI FRIZZ Silkening Shampoo 5.07 fl.oz</b><br>
<b>$22.00
</b><br>
Do you need to discipline unruly, unmanageable and frizzy hair? Thanks to its enveloping and smoothing agents, FIORAVANTI Silkening shampoo helps you control your hair, providing it with suppleness and shine for a perfectly smooth result while styling. Traditionally used to enhance the beauty of hair, Fioravanti Alcoholate, enriched in Acerola fruit acid, which is highly concentrated in vitamin C, seals and smoothes hair scales, for extreme shine.
<br>

<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;dbname=products&amp;itemnum=243&amp;function=add">[Add to Cart]</a>&nbsp;
<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;function=show">[View Cart]</a><BR>
</font>
        <br />
        </td>
      </tr>
      <tr valign=top>
<td valign=top align=left class="outline">
<font class="title">&nbsp;<!-- rec 244 -->
&nbsp;<BR>
<a name="244"></a>
<!-- Default Product Template -->
<img  src="http://peterlouissalon.com/shopsite_sc/store/html/media/rfsilkeningcondt.jpg" alt="rfsilkeningcondt.jpg" width="250" height="250" vspace="3" hspace="3" border="0" align=left>

<b>Rene Furterer Fioravanti ANTI FRIZZ Silkening Conditioner 5.07 fl.oz</b><br>
<b>$22.00
</b><br>
Ideal for controlling frizzy hair and taming unwanted curls, FIORAVANTI Silkening conditioner disciplines unmanageable hair. After washing, this conditioner detangles your hair for easy and optimal styling results, leaving your hair beautifully shiny. Traditionally used to enhance the beauty of hair, Fioravanti Alcoholate, enriched in Acerola fruit acid, which is highly concentrated in vitamin C, seals and smoothes hair scales, for extreme shine.
<br>

<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;dbname=products&amp;itemnum=244&amp;function=add">[Add to Cart]</a>&nbsp;
<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;function=show">[View Cart]</a><BR>
</font>
        <br />
        </td>
      </tr>
      <tr valign=top>
<td valign=top align=left class="outline">
<font class="title">&nbsp;<!-- rec 245 -->
&nbsp;<BR>
<a name="245"></a>
<!-- Default Product Template -->
<img  src="http://peterlouissalon.com/shopsite_sc/store/html/media/rfkariteoil.jpg" alt="rfkariteoil.jpg" width="250" height="250" vspace="3" hspace="3" border="0" align=left>

<b>Rene Furterer Karite Intense Nutrition Oil 3.3 fl.oz</b><br>
<b>$30.00
</b><br>
Rich in Shea oil, an essential revitalizing and nourishing ingredient, and vitamin E, with anti-free radical properties, KARITE Intense nutrition oil optimally nourishes and protects your extremely dry scalp and restores exceptional suppleness and silky softness to dry hair. Thanks to its unrivaled nourishing and revitalizing action, it restores the protective hydrolipidic film, reconstructs the intercellular cement and regenerates both hair and scalp. Its conditioning agents protectively envelop the keratin fibers, leaving your hair beautifully smooth and supple. Its self-emulsifying formula makes rinsing extremely easy. After the first application, your hair is increasingly supple, easy to style, bouncy and luminously radiant.<br>

<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;dbname=products&amp;itemnum=245&amp;function=add">[Add to Cart]</a>&nbsp;
<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;function=show">[View Cart]</a><BR>
</font>
        <br />
        </td>
      </tr>
      <tr valign=top>
<td valign=top align=left class="outline">
<font class="title">&nbsp;<!-- rec 436 -->
&nbsp;<BR>
<a name="436"></a>
<!-- Default Product Template -->
<img  src="http://peterlouissalon.com/shopsite_sc/store/html/media/Rene_Furterer_deep_cleansing_spray.jpg" alt="Rene_Furterer_deep_cleansing_spray.jpg" width="175" height="283" vspace="3" hspace="3" border="0" align=left>

<b>Rene Furterer  Deep Cleansing Spray 3.3 fl.oz</b><br>
<b>$24.00
</b><br>
For day-to-day comfort, the spray purifies and instantly soothes the scalp, maintaining its protective function to reduce recurrent flakes. It can be applied on the scalp daily.<br>

<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;dbname=products&amp;itemnum=436&amp;function=add">[Add to Cart]</a>&nbsp;
<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;function=show">[View Cart]</a><BR>
</font>
        <br />
        </td>
      </tr>
      <tr valign=top>
<td valign=top align=left class="outline">
<font class="title">&nbsp;<!-- rec 437 -->
&nbsp;<BR>
<a name="437"></a>
<!-- Default Product Template -->
<img  src="http://peterlouissalon.com/shopsite_sc/store/html/media/ss_size3/Rene_Furterer_Okara_Mild_Silver_shampoo.jpg" alt="Rene_Furterer_Okara_Mild_Silver_shampoo.jpg" width="52" height="84" vspace="3" hspace="3" border="0" align=left>

<b>Rene Furterer Okara Mild Silver Shampoo 5.07 fl.oz</b><br>
<b>$23.00
</b><br>
Okara Mild Silver Shampoo was specially formulated to restructure, protect and gently wash gray and white hair while neutralizing yellowing. Thanks to its corrective pigments, this product illuminates and revives silvery highlights. Hair is restructured by the Okara protein which reinforces the hair shaft. Vitamin B5 maintains hair moisture and vitamin E ensures protective anti-free radical activity. Hair regains suppleness and luster, shining with silvery brilliance.
Use 1 to 2 times a week.<br>

<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;dbname=products&amp;itemnum=437&amp;function=add">[Add to Cart]</a>&nbsp;
<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;function=show">[View Cart]</a><BR>
</font>
        <br />
        </td>
      </tr>
      <tr valign=top>
<td valign=top align=left class="outline">
<font class="title">&nbsp;<!-- rec 18 -->
&nbsp;<BR>
<a name="18"></a>
<!-- Default Product Template -->
<img  src="http://peterlouissalon.com/shopsite_sc/store/html/media/RFcarthamehydromask.gif" alt="RFcarthamehydromask.gif" width="70" height="121" vspace="3" hspace="3" border="0" align=left>

<b>Rene Furterer Carthame Gentle hydro-nutritive mask  Net Wet 3.4Oz.</b><br>
<b>$28.00
</b><br>
Thanks to carthame and nut oils and its specific moisturizing complex, CARTHAME Gentle hydro-nutritive mask deeply rehydrates and nourishes dry hair,while its natural coating agents smooth the keratin fiber and soften dried out hair.It protects the hair and provides it with exceptional suppleness and softness. Its creamy texture leaves the hair light and luminous, perfectly detangled.<br>

<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;dbname=products&amp;itemnum=18&amp;function=add">[Add to Cart]</a>&nbsp;
<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;function=show">[View Cart]</a><BR>
</font>
        <br />
        </td>
      </tr>
      <tr valign=top>
<td valign=top align=left class="outline">
<font class="title">&nbsp;<!-- rec 73 -->
&nbsp;<BR>
<a name="73"></a>
<!-- Default Product Template -->
<img  src="http://peterlouissalon.com/shopsite_sc/store/html/media/Rene_Furterer_tonucia_treatment.jpeg" alt="Rene_Furterer_tonucia_treatment.jpeg" vspace="3" hspace="3" border="0" align=left>

<b>Rene Furterer Tonucia No rinse fortifying and densifying treatment 5 fl.oz</b><br>
<b>$23.00
</b><br>
Thanks to its active foam rich in Cimentrio and wheat micro-protein TONUCIA Fortifying and densifying treatment works to restructure and densify hair, which gains in thickness without a weighing down effect. Hair regains resistance and body, as well as long-lasting density. For optimal effectiveness, combine this treatment with Tonucia Shampoo.<br>

<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;dbname=products&amp;itemnum=73&amp;function=add">[Add to Cart]</a>&nbsp;
<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;function=show">[View Cart]</a><BR>
</font>
        <br />
        </td>
      </tr>
      <tr valign=top>
<td valign=top align=left class="outline">
<font class="title">&nbsp;<!-- rec 177 -->
&nbsp;<BR>
<a name="177"></a>
<!-- Default Product Template -->
<img  src="http://peterlouissalon.com/shopsite_sc/store/html/media/okara_protct_shp.jpg" alt="okara_protct_shp.jpg" width="200" height="200" vspace="3" hspace="3" border="0" align=left>

<b>Rene Furterer Okara Protective Radiance Shampoo 5.07 fl.oz (+70% color PROTECTION)</b><br>
<b>$22.00
</b><br>
OKARA PROTECT COLOR Protective radiance shampoo preserves your hair color thanks to the color setting properties of Witch Hazel extract. This formula was specially developed to minimize the color washout.The Okara protein provides deep-down keratin repair action and maintains hair hydration. Your color is protected ; your hair is soft and luminous, longer.<br>

<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;dbname=products&amp;itemnum=177&amp;function=add">[Add to Cart]</a>&nbsp;
<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;function=show">[View Cart]</a><BR>
</font>
        <br />
        </td>
      </tr>
      <tr valign=top>
<td valign=top align=left class="outline">
<font class="title">&nbsp;<!-- rec 198 -->
&nbsp;<BR>
<a name="198"></a>
<!-- Default Product Template -->
<img  src="http://peterlouissalon.com/shopsite_sc/store/html/media/okara_protct_condt.jpg" alt="okara_protct_condt.jpg" width="200" height="369" vspace="3" hspace="3" border="0" align=left>

<b>Rene Furterer Okara Restructuring and protective radiance mask 5.07 oz (+80% color PROTECTION)</b><br>
<b>$22.00
</b><br>
OKARA PROTECT COLOR Restructuring and protective radiance mask preserves your hair color thanks to the color setting properties of Witch Hazel extract. Anti-free radical vitamin E and a UV filter work to further reinforce your color protection. Vitamin B5 maintains hair hydration and the Okara protein provides deep down keratin repair detangled, it regains long-lasting silkiness and incomparable luminosity.<br>

<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;dbname=products&amp;itemnum=198&amp;function=add">[Add to Cart]</a>&nbsp;
<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;function=show">[View Cart]</a><BR>
</font>
        <br />
        </td>
      </tr>
      <tr valign=top>
<td valign=top align=left class="outline">
<font class="title">&nbsp;<!-- rec 212 -->
&nbsp;<BR>
<a name="212"></a>
<!-- Default Product Template -->
<img  src="http://peterlouissalon.com/shopsite_sc/store/html/media/okara_2phases_protect.jpg" alt="okara_2phases_protect.jpg" width="200" height="200" vspace="3" hspace="3" border="0" align=left>

<b>Rene Furterer Okara No rinse detangling 2-phases conditioner protection & radiance (+80% color PROTECTION) 5.07 fl.oz</b><br>
<b>$22.00
</b><br>
OKARA protect color No rinse 2-phases conditioner gives instant coating and has detangling action.
Okara extract repairs and nourishes, Hamamelis extract maintains color intensity and double UV filter and Vitamin E protects the color. This is a frequent use leave-in conditioner.<br>

<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;dbname=products&amp;itemnum=212&amp;function=add">[Add to Cart]</a>&nbsp;
<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;function=show">[View Cart]</a><BR>
</font>
        <br />
        </td>
      </tr>
      <tr valign=top>
<td valign=top align=left class="outline">
<font class="title">&nbsp;<!-- rec 119 -->
&nbsp;<BR>
<a name="119"></a>
<!-- Default Product Template -->
<img  src="http://peterlouissalon.com/shopsite_sc/store/html/media/ReneFurtererfioravanticands.jpg" alt="ReneFurtererfioravanticands.jpg" width="200" height="260" vspace="3" hspace="3" border="0" align=left>

<b>Rene Furterer Fioravanti Clarify and Shine Rinse </b><br>
<b>$18.00
</b><br>
The Fioravanti clarify and shine rinse beautifies the hair, bringing shine and body.   
Provides exceptional gloss 
Removes lime scale 
Make detangling easy 
<br>

<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;dbname=products&amp;itemnum=119&amp;function=add">[Add to Cart]</a>&nbsp;
<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;function=show">[View Cart]</a><BR>
</font>
        <br />
        </td>
      </tr>
      <tr valign=top>
<td valign=top align=left class="outline">
<font class="title">&nbsp;<!-- rec 96 -->
&nbsp;<BR>
<a name="96"></a>
<!-- Default Product Template -->
<img  src="http://peterlouissalon.com/shopsite_sc/store/html/media/Rene_Furterer_dry_shampoo.gif" alt="Rene_Furterer_dry_shampoo.gif" width="70" height="200" vspace="3" hspace="3" border="0" align=left>

<b>Rene Furterer Naturia DRY Shampoo 3.2 oz</b><br>
<b>$23.00
</b><br>
NATURIA Dry Shampoo cleanses hair WITHOUT WATER.Its highly absorbent argilla powder restores volume and lightness to hair, making styling easy. It can be used as often as needed and is adapted to everyone's life style. Its fresh scent comes from essential oils for true sensory pleasure.

Recommendations for use: Shake well before use and spray all over hair from a distance of 15 inches. Leave in for 2 minutes. Pat gently with towel and comb. Do not inhale. Use in well ventilated areas.<br>

<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;dbname=products&amp;itemnum=96&amp;function=add">[Add to Cart]</a>&nbsp;
<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;function=show">[View Cart]</a><BR>
</font>
        <br />
        </td>
      </tr>
      <tr valign=top>
<td valign=top align=left class="outline">
<font class="title">&nbsp;<!-- rec 72 -->
&nbsp;<BR>
<a name="72"></a>
<!-- Default Product Template -->
<img  src="http://peterlouissalon.com/shopsite_sc/store/html/media/RF_tonu_shp" alt="RF_tonu_shp" width="87" height="179" align=left>

<b>Rene Furterer Tonucia Toning Shampoo 5.07 fl.oz</b><br>
<b>$22.00
</b><br>
TONUCIA Toning Shampoo tones the scalp and strengthens the hair, thanks to the active bio-spheres it contains. During massaging, the bio-spheres release essential oils in order to encourage the provision of energy-containing elements to hair the roots.Cimentrio and wheat micro-protein work to restructure and coat the hair shafts. Hair regains resistance and body. For long-lasting texturizing action, combine TONUCIA shampoo with TONUCIA No rinse fortifying and densifying treatment.<br>

<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;dbname=products&amp;itemnum=72&amp;function=add">[Add to Cart]</a>&nbsp;
<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;function=show">[View Cart]</a><BR>
</font>
        <br />
        </td>
      </tr>
      <tr valign=top>
<td valign=top align=left class="outline">
<font class="title">&nbsp;<!-- rec 146 -->
&nbsp;<BR>
<a name="146"></a>
<!-- Default Product Template -->
<img  src="http://peterlouissalon.com/shopsite_sc/store/html/media/t3_evolution.jpg" alt="t3_evolution.jpg" width="200" height="200" vspace="3" hspace="3" border="0" align=left>

<b>T3 Tourmaline Evolution Dryer</b><br>
<b>$300.00
</b><br>
T3 Tourmaline Ionic Blow Dryers are the most advanced professional dryers available today. Use of 100% crushed Tourmaline components makes them the most ionic and infrared. They deliver unbelievable results that you truly have to see to believe.
<br><br>
Features:<br>
� Made of 100% crushed Tourmaline stones<br>
� Generates the most negative ions and infrared heat of any professional dryer<br>
� Powerful, long-life professional motors<br>
� 48-month warranty (registration information inside of the box)
<br><br>
Benefits:<br>
� Tourmaline negative ions + maximum infrared heat = 60% faster drying<br>
� Leaves hair soft, smooth and shiny<br>
� Doesn�t damage hair like other blow dryers<br>
� Eliminates the frizzy, "blow dried" look<br>
� Negative ions add moisture and sheen<br>
� Closes the cuticle layer to protect hair


<br>

<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;dbname=products&amp;itemnum=146&amp;function=add">[Add to Cart]</a>&nbsp;
<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;function=show">[View Cart]</a><BR>
</font>
        <br />
        </td>
      </tr>
      <tr valign=top>
<td valign=top align=left class="outline">
<font class="title">&nbsp;<!-- rec 147 -->
&nbsp;<BR>
<a name="147"></a>
<!-- Default Product Template -->
<img  src="http://peterlouissalon.com/shopsite_sc/store/html/media/t3_overnight.jpg" alt="t3_overnight.jpg" width="200" height="200" vspace="3" hspace="3" border="0" align=left>

<b>T3 Tourmaline Overnight Travel Dryer</b><br>
<b>$130.00
</b><br>
Tourmaline Professional Ionic Hair Dryer for Travel delivers the performance and innovation of the �T3 Evolution� dryer in a compact, globe-trotting size. It dries hair quickly and safely, and with a static-free finish by utilizing the same breakthrough technologies that have made T3 dryers such a success. Forget hotel dryers! Now, your hair can always look great - even while traveling the world.
<br><Br>
Other benefits include:<br>
-Tourmaline, a semi-precious mineral, is infused into the dryer itself<br>
-Tourmaline is an unsurpassed emitter of negative ions & infrared heat<br>
-This manufacturing process is proprietary and patented<br>
-Negative ions deliver a smooth, shiny, silky, frizz-free finish<br>
-Negative ions seals the cuticle to protect hair, locking in its moisture and color<br>
-Infrared heat penetrates deeper to enable quick, safe drying<br>
<br>
Special Features:<br>
-Swivel handle design<br>
-Utilizes both Tourmaline negative ion technology and far infrared heat<br>
-Dual voltage (110V or 220V)<br>
-48-month comprehensive manufacture�s warranty<br>
-1200-watts of power<br>
-Compact and collapsible for easy storage and travel<br>
<br>

<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;dbname=products&amp;itemnum=147&amp;function=add">[Add to Cart]</a>&nbsp;
<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;function=show">[View Cart]</a><BR>
</font>
        <br />
        </td>
      </tr>
      <tr valign=top>
<td valign=top align=left class="outline">
<font class="title">&nbsp;<!-- rec 13 -->
&nbsp;<BR>
<a name="13"></a>
<!-- Default Product Template -->

<b>Rene Furterer Carthame Gentle Hydro-nutritive Mask 6.81oz (jar)</b><br>
<b>$28.00
</b><br>
Thanks to carthame and nut oils and its specific moisturizing complex, CARTHAME Gentle hydro-nutritive mask deeply rehydrates and nourishes dry hair, while its natural coating agents smooth the keratin fiber and soften dried out hair. It protects the hair and provides it with exceptional suppleness and softness, Its creamy texture leaves the hair light and luminous, perfectly detangled.<br>

<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;dbname=products&amp;itemnum=13&amp;function=add">[Add to Cart]</a>&nbsp;
<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;function=show">[View Cart]</a><BR>
</font>
        <br />
        </td>
      </tr>
      <tr valign=top>
<td valign=top align=left class="outline">
<font class="title">&nbsp;<!-- rec 36 -->
&nbsp;<BR>
<a name="36"></a>
<!-- Default Product Template -->
<img  src="http://peterlouissalon.com/shopsite_sc/store/html/media/RF_reactional_thin_treat.jpg" alt="RF_reactional_thin_treat.jpg" width="250" height="250" vspace="3" hspace="3" border="0" align=left>

<b>Rene Furterer Reactional Thinning Set</b><br>
<b>$129.00
</b><br>
Reactional Thinning Hair Set (3 Month Program) 
What it is:
A three-month program designed to strengthen thin, weak hair.

What it is formulated to do:
-Tones, deep cleans, and invigorates the scalp.
-Activates microcirculation, providing the hair bulb with stimulating elements that strengthen hair.
-Provides optimal nutrients for healthy hair growth.
-Restores hair's radiant beauty.

What else you need to know:
Set includes a 1.69 oz Complex 5 Regenerating Extract (tones, deep cleans, and prepares the scalp for subsequent treatments by activating its microcirculation), 1.7 oz Forticea Stimulating Shampoo (stimulates circulation and restores hair's radiant beauty), and twelve vials of RF80 Strengthening Formula (highly concentrated in nourishing elements, it promotes hair growth and reduces tissue aging for vigorous growth results). <br>

<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;dbname=products&amp;itemnum=36&amp;function=add">[Add to Cart]</a>&nbsp;
<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;function=show">[View Cart]</a><BR>
</font>
        <br />
        </td>
      </tr>
      <tr valign=top>
<td valign=top align=left class="outline">
<font class="title">&nbsp;<!-- rec 533 -->
&nbsp;<BR>
<a name="533"></a>
<!-- Default Product Template -->
<img  src="http://peterlouissalon.com/shopsite_sc/store/html/media/kariteserum.jpg" alt="kariteserum.jpg" width="300" height="300" vspace="3" hspace="3" border="0" align=left>

<b>Rene Furterer  Karite Serum</b><br>
<strike>$30.00</strike>&nbsp;<b>$25.00</b> On Sale!<br>
 Karite Serum is a specialized treatment serum, created to treat dry, brittle ends while healing and preventing split ends. All hair types - whether normal, oily, dry or damaged - have dry or split ends. Since the ends are the oldest part of the hair and the furthest away from the oils released from the scalp, ends need every day treatment designed specifically for them. Karit� Serum works to smooth and treat split and damaged ends with intensely hydrating Karite butter, also known as shea butter. Packaged in a glass bottle with an easy to use pump, Karit� Serum glides on to smooth, repair, beautify and protect the ends, helping them to become and stay healthy, shiny and gorgeous. Suitable for all hair types, and especially helpful when growing out the hair. 
1.01 fl oz<br>

<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;dbname=products&amp;itemnum=533&amp;function=add">[Add to Cart]</a>&nbsp;
<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;function=show">[View Cart]</a><BR>
</font>
        <br />
        </td>
      </tr>
      <tr valign=top>
<td valign=top align=left class="outline">
<font class="title">&nbsp;<!-- rec 479 -->
&nbsp;<BR>
<a name="479"></a>
<!-- Default Product Template -->
<a href="http://peterlouissalon.com/shopsite_sc/store/html/product479.html"><img  src="http://peterlouissalon.com/shopsite_sc/store/html/media/GHD_mk41" alt="GHD_mk41" vspace="3" hspace="3" border="0" align=left></a>

<a href="http://peterlouissalon.com/shopsite_sc/store/html/product479.html">
<b>ghd - Ceramic Iron MK 4.1</b></a>
<br>
<strike>$250.00</strike>&nbsp;<b>$229.00</b> On Sale!<br>
"The Best Iron I ever used,it looks like a Mercedes-Benz SLK",Peter Louis. The new ghd styler. Curls as easily as it straightens. Incorporating state-of-the-art styling technology, the new- generation ghd styler is the ultimate in versatility. Its rounder barrel means it is now even easier to create curls and movement as well as the perfect straight. And just to make sure you've got the full know -how at your fingertips, there's a step-by-step DVD in the box of every new ghd styler. Please click on the Photo for more info.<br>

<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;dbname=products&amp;itemnum=479&amp;function=add">[Add to Cart]</a>&nbsp;
<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;function=show">[View Cart]</a><BR>
</font>
        <br />
        </td>
      </tr>
      <tr valign=top>
<td valign=top align=left class="outline">
<font class="title">&nbsp;<!-- rec 548 -->
&nbsp;<BR>
<a name="548"></a>
<!-- Default Product Template -->
<img  src="http://peterlouissalon.com/shopsite_sc/store/html/media/Rene_Furterer_molding_paste.jpeg" alt="Rene_Furterer_molding_paste.jpeg" vspace="3" hspace="3" border="0" align=left>

<b>Rene Furterer Molding Paste</b><br>
<b>$24.00
</b><br>
Rene Furterer Matte Effect Modeling Paste is a new modeling clay texture to style your hair the way you want.  This paste creates a matte effect.  Anatide preserves moisture and brings natural shine to the hair. 1.5 oz<br>

<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;dbname=products&amp;itemnum=548&amp;function=add">[Add to Cart]</a>&nbsp;
<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;function=show">[View Cart]</a><BR>
</font>
        <br />
        </td>
      </tr>
      <tr valign=top>
<td valign=top align=left class="outline">
<font class="title">&nbsp;<!-- rec 549 -->
&nbsp;<BR>
<a name="549"></a>
<!-- Default Product Template -->
<img  src="http://peterlouissalon.com/shopsite_sc/store/html/media/Rene_Furterer_Okara_protective_radiance_serum.jpeg" alt="Rene_Furterer_Okara_protective_radiance_serum.jpeg" vspace="3" hspace="3" border="0" align=left>

<b>Rene Furterer Okara Protective Radiance Serum</b><br>
<b>$30.00
</b><br>
Rene Furterer Okara Radiance No Rinse Serum instantly illuminates hair and creates lightness and radiance in hair.  Exceptional soft texture.  More than 70% color maintained due to the protective benefits of natural extract of Okara and Hamamelis. 1.68 oz<br>

<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;dbname=products&amp;itemnum=549&amp;function=add">[Add to Cart]</a>&nbsp;
<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;function=show">[View Cart]</a><BR>
</font>
        <br />
        </td>
      </tr>
      <tr valign=top>
<td valign=top align=left class="outline">
<font class="title">&nbsp;<!-- rec 477 -->
&nbsp;<BR>
<a name="477"></a>
<!-- Default Product Template -->
<img  src="http://peterlouissalon.com/shopsite_sc/store/html/media/RF_OKARASET.jpg" alt="RF_OKARASET.jpg" width="300" height="176" vspace="3" hspace="3" border="0" align=left>

<b>Rene Furterer Okara Protect Color Program</b><br>
<b>$60.00
</b><br>
SAVE 10%!
The Okara Protect Color Complete Program includes:

1 Okara Protective Radiance Shampoo (CPF +70%)5.1.oz
1 Okara Restructuring and Protective Radiance Mask (CPF +80%) 5.1 oz
1 Okara 2-phase Leave-in Protective Conditioner (CPF + 80%)5.0 oz

<br>

<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;dbname=products&amp;itemnum=477&amp;function=add">[Add to Cart]</a>&nbsp;
<a href="http://peterlouissalon.com/shopsite_sc/shopping_cart/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;function=show">[View Cart]</a><BR>
</font>
        <br />
        </td>
      </tr>
          </table>