<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
      <img src="media/homepage/pl_147x47.gif" width="147" height="47" alt="Peter Louis" border="0" /> 
      <p><font class="title"><b>The Peter Louis Hair Philosophy</b></font></p>
      <p><font class="bodytext"><img border="0" src="peter_rounded1.jpg" width="157" height="258" align="right">Peter 
      Louis believes That true Classic Style is Simple, Elegant and Efficient, transcending 
      time. All great hairstyles start with a classic precision cut which can be modified 
      to suit a particular clients needs. The best haircuts are easy to style at home 
      and grow out beautifully.</font></p>
      <p>&nbsp;</p>
      <p>&nbsp;</p>
    </td>
  </tr>
</table>