<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language ?>" xml:lang="<?php print $language ?>">

<head>
  <title><?php print $head_title ?></title>
  <?php print $head ?>
  <?php print $styles ?>
  <?php print $scripts ?>
  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?> </script>
</head>

<body>

<table border="0" cellpadding="0" cellspacing="0" id="header">
  <tr id="test1" style="border-top: solid 4px #265e97;" >
<td style="border-top: solid 4px #265e97;" ></td><td style="border-top: solid 4px #265e97;" ><table border="0" cellpadding="0" cellspacing="0" width="100%" ><tr style="background-image: url(secondarybar.png);
  background-size: 100%;
  background-position: -2px -1px;"><td >
      <img style="background-color: #fff; float: left;" src="./themes/bluemarine/right-primary.png"/><?php if (isset($secondary_links)) { ?><?php print theme('links', $secondary_links, array('class' =>'links', 'id' => 'subnavlist')) ?><?php } ?></td><td width="205" id="sec" ><?php print $search_box ?></td></tr></table>
</td>
</tr>
<tr>
    <td id="logo">
      <?php if ($logo) { ?><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><img src="<?php print $logo ?>" alt="<?php print t('Home') ?>" /></a><?php } ?>

      <?php if ($site_slogan) { ?><div class='site-slogan'><?php print $site_slogan ?></div><?php } ?>
    </td>
    <td id="menu">
<?php
$nodes = mysql_query('SELECT n.nid, n.title, r.teaser, s.price, s.ptype, s.sku FROM node n INNER JOIN node_revisions r ON n.vid = r.vid INNER JOIN ec_product s ON s.vid = n.vid WHERE n.status = 1  ORDER BY n.changed DESC, n.created DESC LIMIT 0, 30');

while($row = mysql_fetch_array($nodes))
  {
$node_var[] = $row;
  }

//$nid = node_load($node_var['nid']);
//print_r($node_var);
print '<center><h1 class="title"> New Items </h1></center><br/><MARQUEE width="740"><table><tr>';
for ($a = 0; $node_var[$a]; $a++)
{
$nid = node_load($node_var[$a]['nid']);
//print_r($nid);
   
    print '<td><a href="?q=node/'.$nid->nid.'">';
    if ($nid->field_image_link[0]['filepath']){ 
    $mysock = getimagesize($nid->field_image_link[0]['filepath']); 
    print "<img src=\"";
    print $nid->field_image_link[0]['filepath'];
    print '"';
    imageResize($mysock[0], $mysock[1], 100) ;
    print "/></a>"."</td>";
    }
    else{

    print '<img src="./files/no-image.png"/>';
    }
}
print '</tr></table></MARQUEE>';


?>

      <?php if ($site_name) { ?><h1 class='site-name'><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><?php print $site_name ?></a></h1><?php } ?>
        
    </td>
  </tr>
<tr id="test2" class="test2" style="border-bottom: solid 2px #265e97;">
<td  style="border-bottom: solid 2px #265e97;">    
</td>
<td  style="border-bottom: solid 2px #265e97;"">
<img style="background-color: #fff; float: left;" src="./themes/bluemarine/left-primary.png"/><?php if (isset($primary_links)) { ?><?php print theme('links', $primary_links, array('class' =>'links', 'id' => 'navlist')) ?><?php } ?>
      
</td>
</tr>
  <tr>
    <td colspan="2"><div><?php print $header ?></div></td>
  </tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" id="content">
  <tr>
    <?php if ($sidebar_left) { ?><td id="sidebar-left">
      <?php print $sidebar_left ?>
    </td><?php } ?>
    <td valign="top">
      <?php if ($mission) { ?><div id="mission"><?php print $mission ?></div><?php } ?>
      <div id="main">
        <?php /*print $breadcrumb */?>
        <h1 class="title"><?php print $title ?></h1>
        <div class="tabs"><?php print $tabs ?></div>
        <?php print $help ?>
        <?php print $messages ?>
        <?php print $content; ?>
        <?php print $feed_icons; ?>
      </div>
    </td>
    <?php if ($sidebar_right) { ?><td id="sidebar-right">
      <?php print $sidebar_right ?>
    </td><?php } ?>
  </tr>
</table>

<div id="footer">
  <?php print $footer_message ?>
</div>
<?php print $closure ?>
</body>
</html>
