<?php

/* GetItemStatus */
require_once '../EbatNs_ServiceProxyShopping.php';
require_once '../EbatNs_Logger.php';

require_once '../GetItemStatusRequestType.php';

$session = new EbatNs_Session();
$session->setAppId($_REQUEST['app_id']);
$session->setSiteId($_REQUEST['site_id']);

$cs = new EbatNs_ServiceProxyShopping($session);
//$cs->attachLogger(new EbatNs_Logger());

$req = new GetItemStatusRequestType();
$req->setItemID($_REQUEST['item_id']);

$res = $cs->GetItemStatus($req);
?>