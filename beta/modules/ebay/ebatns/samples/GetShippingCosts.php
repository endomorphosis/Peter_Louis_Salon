<?php

require_once '../EbatNs_ServiceProxyShopping.php';
require_once '../EbatNs_Logger.php';

require_once '../GetShippingCostsRequestType.php';

$session = new EbatNs_Session();
$session->setAppId($_REQUEST['app_id']);
$session->setSiteId($_REQUEST['site_id']);

$cs = new EbatNs_ServiceProxyShopping($session);
//$cs->attachLogger(new EbatNs_Logger());

$req = new GetShippingCostsRequestType();

// attention this will calculate the shipping to Germany, PostalCode 51105
// be sure to change this to Codes that meets your needs
$CountryCode = isset($_REQUEST['shipping_country_code']) ? $_REQUEST['shipping_country_code'] : $Facet_CountryCodeType->DE;
$PostalCode  = isset($_REQUEST['shipping_postal_code']) ? $_REQUEST['shipping_postal_code'] : '51105';

$req->setDestinationCountryCode($CountryCode);
$req->setDestinationPostalCode($PostalCode);
$req->setIncludeDetails(true);
$req->setQuantitySold(1);
$req->setItemID($_REQUEST['item_id']);

$res = $cs->GetShippingCosts($req);
?>