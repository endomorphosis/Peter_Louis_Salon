<html>

<head>
	<title>EbatNs ShoppingAPI Sample</title>
</head>

<body>
	<h1>EbatNs ShoppingAPI Sample</h1>
	<form action="index.php" method="post">
	<input type="text" name="app_id" value="<?php echo ($_REQUEST['app_id'] ? htmlspecialchars($_REQUEST['app_id']) : ''); ?>" style="width: 400px;"> <b>ApplicationId</b>
	<br>
	<input type="text" name="query" value="<?php echo ($_REQUEST['query'] ? htmlspecialchars($_REQUEST['query']) : ''); ?>" style="width: 400px;"> <b>Query</b>
	<br>
	<input type="text" name="site_id" value="<?php echo (isset($_REQUEST['site_id']) ? htmlspecialchars($_REQUEST['site_id']) : '77'); ?>" style="width: 400px;"> <b>Site-Id (0=US, 77=DE, ...)</b>
	<br>
	<input type="submit" name="btnSubmit" value="Search">
	</form>

<?php
if ($_REQUEST['query'] && $_REQUEST['app_id'])
{
	echo '<hr>';
	
	// just include this file to switch over to non curl HTTP requests
	// if you have cURL installed you can just leave out the line and 
	// cURL will be used !
	require_once '../EbatNs_HttpTransport.php';
	
	include 'FindItems.php';
	
	if ($cs->isGood($res))
	{
		echo '<h2>FindItems-Result for "' . htmlspecialchars($_REQUEST['query']) . '"</h2>';
		
		echo '<table border="1" cellpadding="5">';
		foreach($res->Item as $item)
		{
			echo '<tr>';
			if (isset($item->GalleryURL))
				echo '<td><img style="border: 0px;" src="' . $item->GalleryURL . '"></td>';
			else
				echo '<td>&nbsp;</td>';
			
			echo '<td>' . $item->ItemID . '</td>';
			echo '<td><a target="item" href="' . $item->ViewItemURLForNaturalSearch . '">' . $item->Title . '</a></td>';
			echo '<td>' . $item->ConvertedCurrentPrice->value . ' ' . $item->ConvertedCurrentPrice->attributeValues['currencyID'];
			
			if (isset($item->ConvertedBuyItNowPrice))
				echo '<br>(BIN: ' .  $item->ConvertedBuyItNowPrice->value . ' ' . $item->ConvertedBuyItNowPrice->attributeValues['currencyID'] . ')';
			
			echo '</td>';
			echo '<td>';
			echo '<a href="index.php?query=' . urlencode($_REQUEST['query']) . '&site_id=' . urlencode($_REQUEST['site_id']) . '&app_id=' . urlencode($_REQUEST['app_id']) . '&item_id=' . $item->ItemID . '&action=GetSingleItem">GetSingleItem</a><br>';
			echo '<a href="index.php?show_description=!&query=' . urlencode($_REQUEST['query']) . '&site_id=' . urlencode($_REQUEST['site_id']) . '&app_id=' . urlencode($_REQUEST['app_id']) . '&item_id=' . $item->ItemID . '&action=GetSingleItem">GetSingleItem (with Description)</a><br>';
			echo '<a href="index.php?query=' . urlencode($_REQUEST['query']) . '&site_id=' . urlencode($_REQUEST['site_id']) . '&app_id=' . urlencode($_REQUEST['app_id']) . '&item_id=' . $item->ItemID . '&action=GetShippingCosts">GetShippingCosts</a><br>';
			echo '<a href="index.php?query=' . urlencode($_REQUEST['query']) . '&site_id=' . urlencode($_REQUEST['site_id']) . '&app_id=' . urlencode($_REQUEST['app_id']) . '&item_id=' . $item->ItemID . '&action=GetItemStatus">GetItemStatus</a>';
			echo '</td>';
			
			echo '</tr>';
		}
		echo '</table>';
		
		if ($_REQUEST['action'] == 'GetSingleItem')
		{
			echo '<hr>';
			include 'GetSingleItem.php';
			
			if ($cs->isGood($res))
			{
				echo '<h2>GetSingleItem-Result for "' . htmlspecialchars($_REQUEST['item_id']) . '"</h2>';
				echo '<b>Title</b>: ' . $res->Item->Title . '<br>';
				echo '<b>StartTime</b>: ' . $res->Item->StartTime . '<br>';
				echo '<b>EndTime</b>: ' . $res->Item->EndTime . '<br>';
				echo '<b>ListingType</b>: ' . $res->Item->ListingType . '<br>';
				echo '<b>Location</b>: ' . $res->Item->Location . '<br>';
				echo '<b>Site</b>: ' . $res->Item->Site . '<br>';
				echo '<b>PaymentMethods</b>: ' . implode(', ', $res->Item->PaymentMethods) . '<br>';
				echo '<b>ShipToLocations</b>: ' . implode(', ', $res->Item->ShipToLocations) . '<br>';
				if (isset($_REQUEST['show_description']))
				{
					echo '<b>Description</b>:<br>';
					echo $res->Item->Description;
				}
			}
			else
			{
				$apiError = 'GetSingleItem';
			}
		}
		elseif ($_REQUEST['action'] == 'GetShippingCosts')
		{
			echo '<hr>';
			include 'GetShippingCosts.php';
			
			if ($cs->isGood($res))
			{
				echo '<h2>GetShippingCosts-Result for "' . htmlspecialchars($_REQUEST['item_id']) . '"</h2>';
				echo '<b>SalesTax</b>: ' . $res->ShippingDetails->SalesTax->SalesTaxPercent . '%<br>';
				
				foreach($res->ShippingDetails->ShippingServiceOption as $key => $option)
				{
					echo '<b>ShippingServiceOption #' . ($key + 1) . '</b>: ';
					echo $option->ShippingServiceName . ', ';
					echo $option->ShippingServiceCost->value . ' ' . $option->ShippingServiceCost->attributeValues['currencyID'] . ' ';
					echo '(' . implode(', ', $option->ShipsTo) . ')<br>';
				}
			}
			else
			{
				$apiError = 'GetShippingCosts';
			}
		}
		elseif ($_REQUEST['action'] == 'GetItemStatus')
		{
			echo '<hr>';
			include 'GetItemStatus.php';
			
			if ($cs->isGood($res))
			{
				echo '<h2>GetItemStatus-Result for "' . htmlspecialchars($_REQUEST['item_id']) . '"</h2>';
				echo '<b>EndTime</b>: ' . $res->Item[0]->EndTime . '<br>';
				echo '<b>ListingStatus</b>: ' . $res->Item[0]->ListingStatus . '<br>';
				echo '<b>TimeLeft</b>: ' . $res->Item[0]->TimeLeft . '<br>';
				echo '<b>BidCount</b>: ' . $res->Item[0]->BidCount . '<br>';
				echo '<b>ConvertedCurrentPrice</b>: ' . $res->Item[0]->ConvertedCurrentPrice->value . ' ' . $res->Item[0]->ConvertedCurrentPrice->attributeValues['currencyID'];
			}
			else
			{
				$apiError = 'GetItemStatus';
			}
		}
		
	}
	else
	{
		$apiError = 'FindItems';
	}
	
	if ($apiError)
	{
		echo '<h2>API Error on call "' . $apiError . '"</h2>';
		foreach($res->Errors as $error)
		{
			echo 'Error #' . $error->ErrorCode . ': ' . $error->LongMessage . '<br>';
		}
	}
}
?>

</body>
</html>