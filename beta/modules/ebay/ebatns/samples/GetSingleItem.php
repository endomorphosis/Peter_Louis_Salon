<?php

/* GetSingleItem */
require_once '../EbatNs_ServiceProxyShopping.php';
require_once '../EbatNs_Logger.php';

require_once '../GetSingleItemRequestType.php';

$session = new EbatNs_Session();
$session->setAppId($_REQUEST['app_id']);
$session->setSiteId($_REQUEST['site_id']);

$cs = new EbatNs_ServiceProxyShopping($session);
//$cs->attachLogger(new EbatNs_Logger());

$req = new GetSingleItemRequestType();
$req->setItemID($_REQUEST['item_id']);

$details[] = 'Details';
$details[] = 'ShippingCosts';
$details[] = 'ItemSpecifics';

if (isset($_REQUEST['show_description']))
	$details[] = 'Description';
	
$req->setIncludeSelector(implode(',', $details));

$res = $cs->GetSingleItem($req);
?>