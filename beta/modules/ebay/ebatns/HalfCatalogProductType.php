<?php
// autogenerated file 27.08.2007 10:45
// $Id$
// $Log$
//
require_once 'EbatNs_ComplexType.php';
require_once 'NameValueListArrayType.php';
require_once 'ShippingCostSummaryType.php';
require_once 'SimpleItemArrayType.php';
require_once 'ProductIDType.php';

class HalfCatalogProductType extends EbatNs_ComplexType
{
	// start props
	// @var string $Title
	var $Title;
	// @var anyURI $DetailsURL
	var $DetailsURL;
	// @var anyURI $StockPhotoURL
	var $StockPhotoURL;
	// @var ShippingCostSummaryType $ShippingCostSummary
	var $ShippingCostSummary;
	// @var boolean $DisplayStockPhotos
	var $DisplayStockPhotos;
	// @var int $ItemCount
	var $ItemCount;
	// @var ProductIDType $ProductID
	var $ProductID;
	// @var string $DomainName
	var $DomainName;
	// @var NameValueListArrayType $ItemSpecifics
	var $ItemSpecifics;
	// @var SimpleItemArrayType $ItemArray
	var $ItemArray;
	// @var int $ReviewCount
	var $ReviewCount;
	// end props

/**
 *

 * @return string
 */
	function getTitle()
	{
		return $this->Title;
	}
/**
 *

 * @return void
 * @param  $value 
 */
	function setTitle($value)
	{
		$this->Title = $value;
	}
/**
 *

 * @return anyURI
 */
	function getDetailsURL()
	{
		return $this->DetailsURL;
	}
/**
 *

 * @return void
 * @param  $value 
 */
	function setDetailsURL($value)
	{
		$this->DetailsURL = $value;
	}
/**
 *

 * @return anyURI
 */
	function getStockPhotoURL()
	{
		return $this->StockPhotoURL;
	}
/**
 *

 * @return void
 * @param  $value 
 */
	function setStockPhotoURL($value)
	{
		$this->StockPhotoURL = $value;
	}
/**
 *

 * @return ShippingCostSummaryType
 */
	function getShippingCostSummary()
	{
		return $this->ShippingCostSummary;
	}
/**
 *

 * @return void
 * @param  $value 
 */
	function setShippingCostSummary($value)
	{
		$this->ShippingCostSummary = $value;
	}
/**
 *

 * @return boolean
 */
	function getDisplayStockPhotos()
	{
		return $this->DisplayStockPhotos;
	}
/**
 *

 * @return void
 * @param  $value 
 */
	function setDisplayStockPhotos($value)
	{
		$this->DisplayStockPhotos = $value;
	}
/**
 *

 * @return int
 */
	function getItemCount()
	{
		return $this->ItemCount;
	}
/**
 *

 * @return void
 * @param  $value 
 */
	function setItemCount($value)
	{
		$this->ItemCount = $value;
	}
/**
 *

 * @return ProductIDType
 * @param  $index 
 */
	function getProductID($index = null)
	{
		if ($index) {
		return $this->ProductID[$index];
	} else {
		return $this->ProductID;
	}

	}
/**
 *

 * @return void
 * @param  $value 
 * @param  $index 
 */
	function setProductID($value, $index = null)
	{
		if ($index) {
	$this->ProductID[$index] = $value;
	} else {
	$this->ProductID = $value;
	}

	}
/**
 *

 * @return string
 */
	function getDomainName()
	{
		return $this->DomainName;
	}
/**
 *

 * @return void
 * @param  $value 
 */
	function setDomainName($value)
	{
		$this->DomainName = $value;
	}
/**
 *

 * @return NameValueListArrayType
 */
	function getItemSpecifics()
	{
		return $this->ItemSpecifics;
	}
/**
 *

 * @return void
 * @param  $value 
 */
	function setItemSpecifics($value)
	{
		$this->ItemSpecifics = $value;
	}
/**
 *

 * @return SimpleItemArrayType
 */
	function getItemArray()
	{
		return $this->ItemArray;
	}
/**
 *

 * @return void
 * @param  $value 
 */
	function setItemArray($value)
	{
		$this->ItemArray = $value;
	}
/**
 *

 * @return int
 */
	function getReviewCount()
	{
		return $this->ReviewCount;
	}
/**
 *

 * @return void
 * @param  $value 
 */
	function setReviewCount($value)
	{
		$this->ReviewCount = $value;
	}
/**
 *

 * @return 
 */
	function HalfCatalogProductType()
	{
		$this->EbatNs_ComplexType('HalfCatalogProductType', 'urn:ebay:apis:eBLBaseComponents');
		$this->_elements = array_merge($this->_elements,
			array(
				'Title' =>
				array(
					'required' => false,
					'type' => 'string',
					'nsURI' => 'http://www.w3.org/2001/XMLSchema',
					'array' => false,
					'cardinality' => '0..1'
				),
				'DetailsURL' =>
				array(
					'required' => false,
					'type' => 'anyURI',
					'nsURI' => 'http://www.w3.org/2001/XMLSchema',
					'array' => false,
					'cardinality' => '0..1'
				),
				'StockPhotoURL' =>
				array(
					'required' => false,
					'type' => 'anyURI',
					'nsURI' => 'http://www.w3.org/2001/XMLSchema',
					'array' => false,
					'cardinality' => '0..1'
				),
				'ShippingCostSummary' =>
				array(
					'required' => false,
					'type' => 'ShippingCostSummaryType',
					'nsURI' => 'urn:ebay:apis:eBLBaseComponents',
					'array' => false,
					'cardinality' => '0..1'
				),
				'DisplayStockPhotos' =>
				array(
					'required' => false,
					'type' => 'boolean',
					'nsURI' => 'http://www.w3.org/2001/XMLSchema',
					'array' => false,
					'cardinality' => '0..1'
				),
				'ItemCount' =>
				array(
					'required' => false,
					'type' => 'int',
					'nsURI' => 'http://www.w3.org/2001/XMLSchema',
					'array' => false,
					'cardinality' => '0..1'
				),
				'ProductID' =>
				array(
					'required' => false,
					'type' => 'ProductIDType',
					'nsURI' => 'urn:ebay:apis:eBLBaseComponents',
					'array' => true,
					'cardinality' => '0..*'
				),
				'DomainName' =>
				array(
					'required' => false,
					'type' => 'string',
					'nsURI' => 'http://www.w3.org/2001/XMLSchema',
					'array' => false,
					'cardinality' => '0..1'
				),
				'ItemSpecifics' =>
				array(
					'required' => false,
					'type' => 'NameValueListArrayType',
					'nsURI' => 'urn:ebay:apis:eBLBaseComponents',
					'array' => false,
					'cardinality' => '0..1'
				),
				'ItemArray' =>
				array(
					'required' => false,
					'type' => 'SimpleItemArrayType',
					'nsURI' => 'urn:ebay:apis:eBLBaseComponents',
					'array' => false,
					'cardinality' => '0..1'
				),
				'ReviewCount' =>
				array(
					'required' => false,
					'type' => 'int',
					'nsURI' => 'http://www.w3.org/2001/XMLSchema',
					'array' => false,
					'cardinality' => '0..1'
				)
			));

	}
}
?>
