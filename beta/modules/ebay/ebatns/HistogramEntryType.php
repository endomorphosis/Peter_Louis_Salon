<?php
// autogenerated file 27.08.2007 10:45
// $Id$
// $Log$
//
require_once 'EbatNs_ComplexType.php';

class HistogramEntryType extends EbatNs_ComplexType
{
	// start props
	// @var string $Name
	var $Name;
	// @var int $Count
	var $Count;
	// end props

/**
 *

 * @return string
 */
	function getName()
	{
		return $this->Name;
	}
/**
 *

 * @return void
 * @param  $value 
 */
	function setName($value)
	{
		$this->Name = $value;
	}
/**
 *

 * @return int
 */
	function getCount()
	{
		return $this->Count;
	}
/**
 *

 * @return void
 * @param  $value 
 */
	function setCount($value)
	{
		$this->Count = $value;
	}
/**
 *

 * @return 
 */
	function HistogramEntryType()
	{
		$this->EbatNs_ComplexType('HistogramEntryType', 'urn:ebay:apis:eBLBaseComponents');
		$this->_elements = array_merge($this->_elements,
			array(
				'Name' =>
				array(
					'required' => false,
					'type' => 'string',
					'nsURI' => 'http://www.w3.org/2001/XMLSchema',
					'array' => false,
					'cardinality' => '0..1'
				),
				'Count' =>
				array(
					'required' => false,
					'type' => 'int',
					'nsURI' => 'http://www.w3.org/2001/XMLSchema',
					'array' => false,
					'cardinality' => '0..1'
				)
			));

	}
}
?>
