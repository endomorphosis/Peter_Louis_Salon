<?php
// autogenerated file 27.08.2007 10:45
// $Id$
// $Log$
//
require_once 'AbstractRequestType.php';

class GetUserProfileRequestType extends AbstractRequestType
{
	// start props
	// @var string $UserID
	var $UserID;
	// @var string $IncludeSelector
	var $IncludeSelector;
	// end props

/**
 *

 * @return string
 */
	function getUserID()
	{
		return $this->UserID;
	}
/**
 *

 * @return void
 * @param  $value 
 */
	function setUserID($value)
	{
		$this->UserID = $value;
	}
/**
 *

 * @return string
 */
	function getIncludeSelector()
	{
		return $this->IncludeSelector;
	}
/**
 *

 * @return void
 * @param  $value 
 */
	function setIncludeSelector($value)
	{
		$this->IncludeSelector = $value;
	}
/**
 *

 * @return 
 */
	function GetUserProfileRequestType()
	{
		$this->AbstractRequestType('GetUserProfileRequestType', 'urn:ebay:apis:eBLBaseComponents');
		$this->_elements = array_merge($this->_elements,
			array(
				'UserID' =>
				array(
					'required' => false,
					'type' => 'string',
					'nsURI' => 'http://www.w3.org/2001/XMLSchema',
					'array' => false,
					'cardinality' => '0..1'
				),
				'IncludeSelector' =>
				array(
					'required' => false,
					'type' => 'string',
					'nsURI' => 'http://www.w3.org/2001/XMLSchema',
					'array' => false,
					'cardinality' => '0..1'
				)
			));

	}
}
?>
