<?php
// $Id: ClientProxy.tpl.php,v 1.3 2007/08/01 11:22:26 michael Exp $
// $Log: ClientProxy.tpl.php,v $
// Revision 1.3  2007/08/01 11:22:26  michael
// *** empty log message ***
//
// Revision 1.2  2007/07/31 15:05:28  michael
// *** empty log message ***
//
// Revision 1.1  2007/07/31 15:01:45  michael
// *** empty log message ***
//
//
// 
// auto-generated 27.08.2007 10:45 
// Ebay-Schema Version 527
// ShoppingAPI Edition
//

require_once 'EbatNs_Client.php';
require_once 'EbatNs_Session.php';

/**
 * The WSDL version the SDK is built against.
 */
define('EBAY_WSDL_VERSION', 527);

/**
 * This class is the basic interface to the eBay-Webserice for the user.
 * We generated the "proxy" externally as the SOAP-wsdl proxy generator does
 * not really did what we needed.
 */
class EbatNs_ServiceProxyShopping extends EbatNs_Client
{
    function EbatNs_ServiceProxyShopping(& $session, $converter = 'EbatNs_DataConverterIso')
    {
        if (is_a($session, 'EbatNs_Session'))
        {
			// Initialize the SOAP Client.
			$this->EbatNs_Client($session, $converter);
		}
		else
		{
			if (is_string($session))
			{
				$session = & new EbatNs_Session($session);
				$this->EbatNs_Client($session, $converter);
			}
		}
    }

	function isGood($response, $ignoreWarnings = true)
	{
		if ($ignoreWarnings)
			return ($response->Ack == 'Success' || $response->Ack == 'Warning');		
		else
			return ($response->Ack == 'Success');
	}

	function isFailure($response)
	{
		return ($response->Ack == 'Failure');
	}
	
	function getErrorsToString($response, $asHtml = false, $addSlashes = true)
	{
		$errmsg = '';
		if (count($response->Errors))
			foreach ($response->Errors as $errorEle)
				$errmsg .= '#' . $errorEle->getErrorCode() . ' : ' . ($asHtml ? htmlentities($errorEle->getLongMessage()) :  $errorEle->getLongMessage()) . ($asHtml?"<br>" : "\r\n");
	
		if ($addSlashes)
			return addslashes($errmsg);
		else			
			return $errmsg;
	}
	/**
 *

 * @return FindHalfProductsResponseType
 * @param FindHalfProductsRequestType $request 
 */
	function &FindHalfProducts($request)
	{
			return ($res = & $this->callShoppingApiStyle('FindHalfProducts', $request));

	}
/**
 *

 * @return FindItemsResponseType
 * @param FindItemsRequestType $request 
 */
	function &FindItems($request)
	{
			return ($res = & $this->callShoppingApiStyle('FindItems', $request));

	}
/**
 *

 * @return FindItemsAdvancedResponseType
 * @param FindItemsAdvancedRequestType $request 
 */
	function &FindItemsAdvanced($request)
	{
			return ($res = & $this->callShoppingApiStyle('FindItemsAdvanced', $request));

	}
/**
 *

 * @return FindProductsResponseType
 * @param FindProductsRequestType $request 
 */
	function &FindProducts($request)
	{
			return ($res = & $this->callShoppingApiStyle('FindProducts', $request));

	}
/**
 *

 * @return GetItemStatusResponseType
 * @param GetItemStatusRequestType $request 
 */
	function &GetItemStatus($request)
	{
			return ($res = & $this->callShoppingApiStyle('GetItemStatus', $request));

	}
/**
 *

 * @return GetMultipleItemsResponseType
 * @param GetMultipleItemsRequestType $request 
 */
	function &GetMultipleItems($request)
	{
			return ($res = & $this->callShoppingApiStyle('GetMultipleItems', $request));

	}
/**
 *

 * @return GetShippingCostsResponseType
 * @param GetShippingCostsRequestType $request 
 */
	function &GetShippingCosts($request)
	{
			return ($res = & $this->callShoppingApiStyle('GetShippingCosts', $request));

	}
/**
 *

 * @return GetSingleItemResponseType
 * @param GetSingleItemRequestType $request 
 */
	function &GetSingleItem($request)
	{
			return ($res = & $this->callShoppingApiStyle('GetSingleItem', $request));

	}
/**
 *

 * @return GetUserProfileResponseType
 * @param GetUserProfileRequestType $request 
 */
	function &GetUserProfile($request)
	{
			return ($res = & $this->callShoppingApiStyle('GetUserProfile', $request));

	}

}
?>