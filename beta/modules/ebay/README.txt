ebay integration module
=======================

this module integrates with the ecommerce module to allow drupal products to be listed on Ebay automatically. Ebay products will be seperated from 
normal products, which means if you want to sell an item on ebay as well as on your website, you need to create 2 products: a normal one and one of 
type "ebay".
this is because we'd run into trouble if an item is sold on the webpage but still listed on ebay as well.

ebay products will be listed on drupal, but can't be bought: instead, the function links to ebay's item page. however drupal ecommerces payment 
functions may be used to allow the winning bidder to pay.


1. ebay API
this is really tricky: you register on developer.ebay.com and get your 
keys and the token. now you need to pair a sandbox user account with a 
given set of keys.
while "read-only" api requests worked fine, AddItem did NOT work - ebay 
API told me to register a credit card first in order to setup a sellers 
account. strange. i searched ebays website for hours but couldn't find a 
solution.
in the end, i found a blog entry somewhere as some people had the same 
problem: you need to register the sandbox user account through the 
developer.ebay.com interface (there's a link somewhere).
it does NOT work if you register the account directly on 
http://sandbox.ebay.com
need to remember that.


2. keys and token
ebay api requires 3 keys and a token. ebatns usually requires the user 
to put the token in a seperate file.
i don't think this is a good solution for drupal, so i put the token in 
a database field like any other variable.
somewhere in ebatns documentation i read that changed token are written 
to the token file automatically. i can't figure out when a change if 
token is to happen? (except the user changes it himself). anyway, should 
i stumble across a forced change of token, i can still detect it and 
write a new one to the database.
i also think this is ok from a security pov.


3. bundling of ebatns
ebay module requires the ebatns library. i thought a while about how 
this dependancy is best resolved. 
unfortunately, ebatns isn't released under GNU license so it must not be uploaded to drupal CVS. please download ebatns yourself and place the files 
directly into the ebatns subdirectory.
