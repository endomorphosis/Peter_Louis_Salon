$Id: README.txt,v 1.1 2008/01/12 16:22:24 corsix Exp $

TinyMCE Automatic Configuration Module
-----------------------------------------
Provides a secure configuration of TinyMCE with a single mouse click.

TinyMCE Automatic Configuration will create a new secure configuration of the TinyMCE
module, or restore a previously created one back to how it was originally. It will:
 * Give authenticated users permission to use TinyMCE
 * Create/restore a default TinyMCE configuration
 * Set Filtered HTML as the default input format and configure it securely
 
 Requires the TinyMCE module (http://drupal.org/project/tinymce).
 Once activated in the Drupal module list (Administer -> Site building -> Modules),
 you can access the automatic configuation at Administer -> Site configuration ->
 TinyMCE -- create default profile
 
 Comments/questions should be sent to http://drupal.org/node/203173 at the module does
 not have a full Drupal project page at the moment.
 