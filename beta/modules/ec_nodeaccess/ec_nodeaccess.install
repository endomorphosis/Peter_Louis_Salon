<?php
// $Id: ec_nodeaccess.install,v 1.1.2.2 2008/12/06 13:18:58 danielb Exp $


/**
 * Implementation of hook_install().
 *
 * ec_nodeaccess implements db schema for two reasons:
 *	1) Product settings can change, ec_nodeaccess can be configured to use settings as at the date of purchase
 *     and therefore must maintain an independent record of these settings.
 *  2) Transactions can be deleted from the ec tables by store admins in the backend.  By keeping a redundant
 *     set of transaction records we ensure users who should have access will retain access.
 */

function ec_nodeaccess_install() {
  switch ($GLOBALS['db_type']) {
    case 'mysql':
    case 'mysqli':
      $worked = db_query("CREATE TABLE {ec_nodeaccess_products} (
          entry_id int unsigned auto_increment NOT NULL,
          nid int unsigned NOT NULL default '0',
          categories longtext default NULL,
          nodes longtext default NULL,
          entry_date datetime default NULL,
          PRIMARY KEY (entry_id)
          ) /*!40100 DEFAULT CHARACTER SET UTF8 */ 
        ");
      if (!$worked) {
        drupal_set_message("Node Access Products table could not be installed", error);
        module_disable(array(0 => 'ec_nodeaccess', ));
        break;
      }
      $worked = db_query("CREATE TABLE {ec_nodeaccess_transactions} (  
          txn_id int unsigned NOT NULL default '0',
          uid int unsigned NOT NULL default '0',
          purchase_date datetime default NULL,
          PRIMARY KEY (txn_id)
          ) /*!40100 DEFAULT CHARACTER SET UTF8 */ 
        ");
      if (!$worked) {
        drupal_set_message("Node Access Transactions table could not be installed", error);
        module_disable(array(0 => 'ec_nodeaccess', ));
        break;
      }
      $worked = db_query("CREATE TABLE {ec_nodeaccess_purchases} (  
          txn_id int unsigned NOT NULL default '0',
          product_nid int unsigned default NULL,
          expiry_date datetime default NULL,
          PRIMARY KEY (txn_id, product_nid)
          ) /*!40100 DEFAULT CHARACTER SET UTF8 */ 
        ");
      if (!$worked) {
        drupal_set_message("Node Access Purchases table could not be installed", error);
        module_disable(array(0 => 'ec_nodeaccess', ));
        break;
      }
    break;
    case 'pgsql':
      $worked = db_query("CREATE TABLE {ec_nodeaccess_products} (
          entry_id serial NOT NULL,
          nid int_unsigned NOT NULL default '0',
          categories text default NULL,
          nodes text default NULL,
          entry_date timestamp default NULL,
          PRIMARY KEY (entry_id)
          )
        ");
      if (!$worked) {
        drupal_set_message("Node Access Products table could not be installed", error);
        module_disable(array(0 => 'ec_nodeaccess', ));
        break;
      }
      $worked = db_query("CREATE TABLE {ec_nodeaccess_transactions} (  
          txn_id int_unsigned NOT NULL default '0',
          uid int_unsigned NOT NULL default '0',
          purchase_date timestamp default NULL,
          PRIMARY KEY (txn_id)
          )
        ");
      if (!$worked) {
        drupal_set_message("Node Access Transactions table could not be installed", error);
        module_disable(array(0 => 'ec_nodeaccess', ));
        break;
      }
      $worked = db_query("CREATE TABLE {ec_nodeaccess_purchases} (  
          txn_id int_unsigned NOT NULL default '0',
          product_nid int_unsigned default NULL,
          expiry_date timestamp default NULL,
          PRIMARY KEY (txn_id, product_nid)
          )
        ");
      if (!$worked) {
        drupal_set_message("Node Access Purchases table could not be installed", error);
        module_disable(array(0 => 'ec_nodeaccess', ));
        break;
      }
    break;

  }
}


/**
 * Implementation of hook_uninstall().
 */

function ec_nodeaccess_uninstall() {
  $worked = db_query('DROP TABLE {ec_nodeaccess_products}');
  if (!$worked) {
    drupal_set_message("Node Access Products records could not be removed", error);
  }
  $worked = db_query('DROP TABLE {ec_nodeaccess_transactions}');
  if (!$worked) {
    drupal_set_message("Node Access Transactions records could not be removed", error);
  }
  $worked = db_query('DROP TABLE {ec_nodeaccess_purchases}');
  if (!$worked) {
    drupal_set_message("Node Access Purchases records could not be removed", error);
  }
  variable_del('ec_nodeaccess_use_purchase_date');
  variable_del('ec_nodeaccess_show_bynodes');
  variable_del('ec_nodeaccess_content_types');
  variable_del('ec_nodeaccess_taxonomy_terms');
  variable_del('ec_nodeaccess_show_children');
  variable_del('ec_nodeaccess_show_bycats');
  variable_del('ec_nodeaccess_taxonomy_vocabs');
  variable_del('ec_nodeaccess_revoke');
  variable_del('ec_nodeaccess_priority');
  variable_del('ec_nodeaccess_traverse_children');
}