<?php // $Id: drupal.inc,v 1.6 2008/08/03 05:39:34 vauxia Exp $

function mlm_drupal_info() {
  return (object) array(
    'name'            =>  t('Drupal'),
    'supported_types' => array('announcement', 'discussion'),
  );
}

function mlm_drupal_save() {
  if (!db_table_exists('mlm_drupal')) {
    return (bool) db_query("CREATE TABLE {mlm_drupal} (
      nid        INT NOT NULL DEFAULT 0,
      uid        INT NOT NULL DEFAULT 0,
      mail       VARCHAR(100),
      timestamp  INT NOT NULL DEFAULT 0,
      PRIMARY KEY(nid, uid, mail)
    )");
  }
  return true;
}

function mlm_drupal_subscribe($list, $mail, $type='subscriber') {

  if (db_result(db_query("SELECT 1 FROM {mlm_drupal}
    WHERE nid = %d AND (mail = '%s' OR (uid != 0 AND uid = %d))"
      , $list->nid, $mail, $uid))) {
    return true;                 // already subscribed
  }

  return (bool)db_query("INSERT INTO {mlm_drupal} ( nid, mail, uid, timestamp )
    VALUES ( '%s', '%s', %d, %d )", $list->nid, $mail, $uid, time());
}

function mlm_drupal_unsubscribe($list, $mail, $type='subscriber') {
  return (bool) db_query("DELETE FROM {mlm_drupal}
    WHERE nid = %d AND ( mail = '%s' OR (uid != 0 AND uid = %d ))"
      , $list->nid, $mail, $uid);
}

function mlm_drupal_count($list) {
  return (int) db_result(db_query("SELECT COUNT(1) FROM {mlm_drupal}
    WHERE nid = %d", $list->nid));
}

function mlm_drupal_subscribers($list, $type='subscriber', $limit=100, $filter='') {
  return pager_query(db_rewrite_sql("SELECT COALESCE(m.mail, u.mail) AS mail,
      u.name
      FROM {mlm_drupal} m LEFT OUTER JOIN {users} u USING ( mail )
      WHERE m.nid = %d ORDER BY mail", $primary_table = 'm'), $limit, 0, null, $list->nid); // must identify primary_table to avoid issues
}

function mlm_drupal_is_subscribed($list, $mail, $type='subscriber') {
  return (bool) db_result(db_query("SELECT 1 FROM {mlm_drupal} m
      WHERE m.nid = %d AND (m.mail = '%s' OR ( m.uid != 0 AND m.uid = %d))"
      , $list->nid, $mail, $uid));
}

function mlm_drupal_delete($list) {
  return (bool) db_query("DELETE FROM {mlm_drupal} WHERE nid = %d", $list->nid);
}

function mlm_drupal_post_address($list) {

  $res = db_query(db_rewrite_sql("SELECT u.uid,
        COALESCE(m.mail, u.mail) AS mail,
        COALESCE(u.name, m.mail) AS name,
        COALESCE(u.uid, 0) AS uid
      FROM {mlm_drupal} m LEFT OUTER JOIN {users} u USING ( mail )
      WHERE m.nid = %d ORDER BY mail", $primary_table = 'm'), $list->nid); // must identify primary_table to avoid issues

  $recipients = array();
  while ($row = db_fetch_array($res)) {
    $recipients[] = $row;
  }

  return $recipients;
}

function mlm_drupal_backend_settings() {
  $form = array();
  if (variable_get('mimemail_incoming', false)) {
    $form['mlm_drupal_incoming_address'] = array(
      '#type' => 'textfield',
      '#title' => t('Incoming address'),
      '#default_value' => variable_get('mlm_drupal_incoming_address', ''),
      '#description' => t('Handle incoming posts and replies sent to this address.'),
    );
  }
  return $form;
}
