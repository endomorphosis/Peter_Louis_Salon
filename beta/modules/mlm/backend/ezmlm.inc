<?php // $Id: ezmlm.inc,v 1.3.2.3 2008/12/17 18:04:07 vauxia Exp $

function mlm_ezmlm_info() {
  return (object) array(
    'name'            =>  t('ezmlm'),
    'supported_types' => array('announcement', 'discussion'),
  );
}

function mlm_ezmlm_node_form($node) {
  $form = array();
  $form['mlm_ezmlm_address'] = array(
    '#type'          => 'textfield',
    '#title'         => t('E-mail Address'),
    '#default_value' => $node->mlm_ezmlm->address,
    '#description'   => t('A mailing list must be configured on your mail server before you can use it on your site. Enter its e-mail address here.'),
  );
  return $form;
}

function mlm_ezmlm_node_form_validate($values) {
  if (!$values['mlm_ezmlm_address']) {
    form_set_error('mlm_ezmlm_address', t('E-mail address is required'));
  }
  if (!valid_email_address($node->mlm_ezmlm_address)) {
    form_set_error('mlm_ezmlm_address', t('Invalid mail address format'));
  }
}

function mlm_ezmlm_subscribe($list, $mail) {
  $mail = is_object($mail) ? $mail->mail : $mail;
  $action = '-subscribe-'. str_replace('@', '=', $mail);
  $address = str_replace('@', "{$action}@", $list->mlm_ezmlm->address);
  return mimemail($mail, $address, 'unsubscribe', '');
}

function mlm_ezmlm_unsubscribe($list, $mail) {
  $mail = is_object($mail) ? $user->mail : $mail;
  $action = '-unsubscribe-'. str_replace('@', '=', $mail);
  $address = str_replace('@', "{$action}@", $list->mlm_ezmlm->address);
  return mimemail($mail, $address, 'unsubscribe', '');
}

function mlm_ezmlm_post_address($list) {
  return $list->mlm_ezmlm->address;
}
