<?php // $Id: ezmlm_idx.inc,v 1.8.2.3 2008/12/17 18:04:07 vauxia Exp $

function mlm_ezmlm_idx_info() {
  return (object) array(
    'name'            =>  t('ezmlm-idx'),
    'supported_types' => array('announcement', 'discussion'),
  );
}

function mlm_ezmlm_idx_node_form($node) {
  $form = array();

  $form['mlm_ezmlm_idx_address'] = array(
    '#type'          => 'textfield',
    '#title'         => t('E-mail Address'),
    '#default_value' => $node->mlm_ezmlm_idx->address,
    '#description'   => t('A mailing list must be configured on your mail server before you can use it on your site. Enter its e-mail address here.'),
  );
  $form['mlm_ezmlm_idx_troot'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Table Prefix'),
    '#default_value' => $node->mlm_ezmlm_idx->troot,
    '#description'   => t('Each ezmlm list has a table prefix in the database to keep its data separate from other lists.  Enter the prefix that corresponds to the list\'s e-mail address.'),
  );
  if (variable_get('mlm_ezmlm_idx_autopost', 0)) {
    $form['mlm_ezmlm_idx_key'] = array(
      '#type'          => 'textfield',
      '#title'         => t('List Key'),
      '#default_value' => $node->mlm_ezmlm_idx->key,
      '#description'   => t('Required if using automatic posting.  Otherwise, you can safely ignore this'),
    );
  }
  return $form;
}

function mlm_ezmlm_idx_node_form_validate($values) {
  if (!$values['mlm_ezmlm_idx_address']) {
    form_set_error('mlm_ezmlm_idx_address', t('E-mail address is required'));
  }
  if (!valid_email_address($values['mlm_ezmlm_idx_address'])) {
    form_set_error('mlm_ezmlm_idx_address', t('Invalid mail address format'));
  }
  if (!preg_match('/^[a-z0-9_]*$/i', $values['mlm_ezmlm_idx_troot'])) {
    form_set_error('mlm_ezmlm_idx_troot', t('Invalid format for table prefix'));
  }
  if ($values['mlm_ezmlm_idx_autopost']) {
    if (!$values['mlm_ezmlm_idx_key']) {
      form_set_error('mlm_ezmlm_idx_key', t('List Key is required for auto-moderation'));
    }
  }
}

function mlm_ezmlm_idx_create() {
  return true;
}

function mlm_ezmlm_idx_subscribe($list, $mail, $type='subscriber') {
  _mlm_ezmlm_idx_db_set_active($list);
  $table =  _ezmlm_table($list, $type);

  if (db_result(db_query("SELECT 1 FROM %s WHERE address = '%s'", $table, $mail))) {
    db_set_active();
    return true;                 // already subscribed
  }

  db_query("INSERT INTO %s ( hash, address )
  		VALUES ( '%s', '%s' )", $table, ezmlm_hash($mail), $mail);

  if ($type = 'subscriber') {
    db_query("INSERT INTO %s ( tai, address, fromline, edir) VALUES ( '%s', '%s', '%s', '+')"
      , _ezmlm_table($list, 'slog'), date('Y-m-d H:i:s', time()), $mail, $mail);
  }
  db_set_active();
  return true;
}

function mlm_ezmlm_idx_unsubscribe($list, $mail, $type='subscriber') {
  _mlm_ezmlm_idx_db_set_active($list);

  db_query("DELETE FROM %s WHERE address = '%s'", _ezmlm_table($list, $type), $mail);
  if (db_affected_rows() && $type = 'subscriber') {
    db_query("INSERT INTO %s ( tai, address, fromline, edir) VALUES ( '%s', '%s', '%s', '-')"
      , _ezmlm_table($list, 'slog'), date('Y-m-d H:i:s', time()), $mail, $mail);
  }
  db_set_active();
  return true;
}

function mlm_ezmlm_idx_count($list, $type='subscriber') {
  _mlm_ezmlm_idx_db_set_active($list);
  $count = db_result(db_query("SELECT COUNT(1) FROM %s", _ezmlm_table($list, $type)));
  db_set_active();
  return $count;
}

function mlm_ezmlm_idx_subscribers($list, $type='subscriber', $limit=100, $filter='') {
  _mlm_ezmlm_idx_db_set_active($list);
  // it would be cool to join against the users table, but it may be a different DSN
  $table = _ezmlm_table($list, $type);
  $res = pager_query("SELECT l.address AS mail FROM $table l
  		ORDER BY l.address", $limit);
  db_set_active();
  return $res;
}

function mlm_ezmlm_idx_is_subscribed($list, $mail, $type='subscriber') {
  _mlm_ezmlm_idx_db_set_active($list);
  $res = (boolean) db_result(db_query("SELECT 1 FROM %s
  		WHERE address = '%s' LIMIT 1", _ezmlm_table($list, 'subscriber'), $mail));
  db_set_active();
  return $res;
}

function mlm_ezmlm_idx_post_address($list) {
  return $list->mlm_ezmlm_idx->address;
}

function mlm_ezmlm_idx_post($list, $user, $subject, $body) {
  $address = $list->mlm_ezmlm_idx->address;
  if (variable_get('mlm_ezmlm_idx_autopost', 0) && $list->mlm_ezmlm_idx->key) {
    global $base_url;
    $table   = _ezmlm_table($list, 'post');
    $key     = $list->mlm_ezmlm_idx->key;
    $created = time();
    $id      = db_next_id("{$table}_id");
    $hash    = sha1($id . $subject . $created . $key);
    $address = str_replace('@', "-autopost-{$hash}@", $address);

    _mlm_ezmlm_idx_db_set_active($list);
    db_query("INSERT INTO %s (id, uid, website, subject, created, sent)
      VALUES (%d, %d, '%s', '%s', UNIX_TIMESTAMP(NOW()), 0)",
      $table, $id, $user->uid, $base_url, $subject);
    db_set_active();
  }
}

function mlm_ezmlm_idx_save($list) {
  if (variable_get('mlm_ezmlm_idx_autopost', 0) && $list->mlm_ezmlm_idx->key) {
    $table   = _ezmlm_table($list, 'post');

    _mlm_ezmlm_idx_db_set_active($list);
    @db_query("SELECT 1 FROM %s", $table);
    if (db_error()) { // table does not exist
      $sql = "CREATE TABLE $table ( id INT, website VARCHAR(100), uid INT, subject VARCHAR(200), created INT, sent INT, PRIMARY KEY (id) )";
      @db_query($sql);
      if (db_error()) { // probably don't have permission
        drupal_set_message(t("You need to create a table to track Drupal posts for auto-moderation.  Execute the following query in the database containing your Ezmlm-idx list tables:") ."<br /><br /> <b>$sql;</b><br /><br />");
      }
    }
    db_set_active();
  }
}

function mlm_ezmlm_idx_backend_settings() {
  $form = array();
  $form['mlm_ezmlm_idx_dsn'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Connection Name'),
    '#default_value' => variable_get('mlm_ezmlm_idx_dsn', ''),
    '#description'   => t('If this list is stored in a separate database, <a href="http://drupal.org/node/18429" target="_blank">add the connection string to your settings file</a> and specify the connection name here.'),
  );
  $form['mlm_ezmlm_idx_autopost'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Enable Automatic Posting'),
    '#default_value' => variable_get('mlm_ezmlm_idx_autopost', false),
    '#description'   => t('Allow posts to go through automatically, bypassing moderation and list controls. This is not a standard ezmlm feature and requires additional configuration.'),
  );
  return $form;
}

function _mlm_ezmlm_idx_db_set_active($list) {
  if ($dsn = $list->mlm_ezmlm_idx->dsn) {
    return db_set_active($dsn);
  }
  db_set_active(variable_get('mlm_ezmlm_idx_dsn', ''));
  return;
}

function _ezmlm_table($list, $type) {
  $troot = $list->mlm_ezmlm_idx->troot;
  if (!$troot) {
    $troot = $list->mlm_ezmlm_idx_troot;
  }
  switch ( $type ) {

    case 'subscriber':
      return $troot;

    case 'moderator':
      return $troot .'_mod';

    case 'allow':
      return $troot .'_allow';

    case 'deny':
      return $troot .'_deny';

    case 'slog':
      return $troot .'_slog';

    case 'post':
      return $troot .'_post';

    default:
      return $troot;
      return false;
  }
}
