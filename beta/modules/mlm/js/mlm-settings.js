// $Id: mlm-settings.js,v 1.1 2008/07/13 18:48:30 vauxia Exp $

/**
 * MLM Settings administration.
 * Because there are so many combinations of options for backends and node 
 * types, we'll show all possible combinations on the settings pages, and use
 * JQuery to hide the irrelevant options.
 */

if (Drupal.jsEnabled) {
  $(document).ready(function() {
    $backends = $('#mlm-backends input');
    $nodetypes = $('#mlm-nodetypes input');

    $nodetypes.each(function() { mlmFilterNodetype(this, $backends); });

    $backends.bind('change', function() { mlmFilterBackend(this); });
    $nodetypes.bind('change', function() { mlmFilterNodetype(this, $backends); });
  });
}

var mlmFilterNodetype = function(nodetypeElement, backendElements) {
  // Hide/show the node type settings based on whether the type is in use.
  $item = $('#mlm-nodetype-' + $(nodetypeElement).attr('value') + ']');
  if (!$(nodetypeElement).attr('checked')) {
    $item.hide();
  }
  else {
    $item.show();
  }

  // Hide the enclosing fieldset if there are no active node types.
  var hideNodetypes = true;
  $('#mlm-nodetypes input').each(function() { 
    if ($(this).attr('checked')) hideNodetypes = false;
  });
  if (hideNodetypes) { $('#mlm-nodetype-settings').hide(); }
  else { $('#mlm-nodetype-settings').show(); }

  // Repopulate the backend checkbox form for this node type.
  backendElements.each(function() { mlmFilterBackend(this); });
};

var mlmFilterBackend = function(backendElement) {
  // Hide/show the backend settings based on whether the backend is in use.
  $items = $('.mlm-nodetype-backend input[@value=' + $(backendElement).attr('value') + ']');
  if ($(backendElement).attr('checked')) {
    $items.parent().show();
    $('#mlm-backend-' + $(backendElement).attr('value') + ']').show();
  }
  else {
    $items.parent().hide();
    $('#mlm-backend-' + $(backendElement).attr('value') + ']').hide();
  }

  // Hide the enclosing fieldset if there are no active node types.
  var hideBackends = true;
  $('#mlm-backends input').each(function() { 
    if ($(this).attr('checked')) hideBackends = false;
  });
  if (hideBackends) { $('#mlm-backend-settings').hide(); }
  else { $('#mlm-backend-settings').show(); }
};
