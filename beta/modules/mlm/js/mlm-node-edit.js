// $Id: mlm-node-edit.js,v 1.1 2008/07/13 18:48:30 vauxia Exp $

/**
 * MLM Node type administration.
 * Show/hide backend settings based on selected backend.
 */

if (Drupal.jsEnabled) {
  $(document).ready(function() {
    $backend = $('#edit-mlm-backend');
    $($backend).bind('change', function() { mlmFilterBackend(this); });
    mlmFilterBackend($backend);
  });
}

var mlmFilterBackend = function(backendElement) {
  // Hide/show the backend settings based on whether the backend is selected.
  var strSelector = '#mlm-' + $(backendElement).attr('value') + '-settings';
  $('.mlm-settings:not(' + strSelector + ')').fadeOut('fast');
  $(strSelector).fadeIn('fast');
};
