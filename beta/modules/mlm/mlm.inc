<?php // $Id: mlm.inc,v 1.9.2.6 2008/12/17 18:04:06 vauxia Exp $

/**
 * Obtain a list of backends by searching the backends directory
 */
function _mlm_backends($node_type = NULL, $list_only = false) {
  static $backends;
  if (!is_array($backends)) {
    $backends = module_invoke_all('mlm_backends');
  }

  $backend_list = $backends;

  if ($node_type) {
    // Unset any backend that's not enabled
    foreach (variable_get('mlm_backends', array()) as $key => $backend) {
      if (!$backend) unset($backend_list[$key]);
    }

    // Unset any backend that's not enabled for this node type.
    foreach (variable_get('mlm_backends_'. $node_type, array()) as $key => $backend) {
      if (!$backend) unset($backend_list[$key]);
    }
  }

  if ($list_only) {
    foreach ($backend_list as $key => $backend) {
      $backend_list[$key] = $backend->name;
    }
  }
  return $backend_list;
}

/*
 * Implementation of our own hook_mlm_backends()
 */
function mlm_mlm_backends() {
  $backends = array();
  foreach (file_scan_directory(dirname(__FILE__) .'/backend', '.inc$') as $file) {
    $backend = $file->name;
    if ($active_only && !in_array($backend, $active)) continue;

    if ($info = mlm_backend_call($backend, 'info')) {
      $info->file = $file->filename;

      $backends[$backend] = $info;
    }
  }
  return $backends;
}

/**
 * Get an array of available list types ( e.g. announcement, discussion )
 * optionally filtered by backend(s) in use.
 */
function _mlm_list_types($backends='') {
  if ($backend) {
    if (!is_array($backends)) $backends = array($backend);
    $filtered = array();
    foreach ($backends as $backend) {
      if (!is_object($backend)) {
        $all_backends = _mlm_backends();
        $backend = $all_backends[$backend];
      }
      foreach ($backend->supported_types as $t) {
        $filtered[$t] = t($t);
      }
    }
    return $filtered;
  }

  static $types = array();
  if (!count($types)) {
    foreach (_mlm_backends() as $backend) {
      $temp = array_merge($types, $backend->supported_types);
    }
    sort($types);
    foreach ($temp as $type) {
      $types[$type] = t($type);
    }
  }
  return $types;
}

/**
 * Subscription form on user page.
 */
function _mlm_user_form($user) {
  $form = array();
  $form['mlm_lists'] = array( '#type' => 'checkboxes' );
  foreach (mlm_lists(null, null, $user) as $list) {
    $form['mlm_lists'][$list->nid] = array(
      '#type' => 'checkbox',
      '#title' => l($list->title, 'node/'. $list->nid),
      '#description' => $list->teaser,
      '#default_value' => (int) $list->subscribed,
      '#prefix' => '<div class="mlm-list">',
      '#suffix' => '</div>',
    );
  }
  return $form;
}

/**
 * Node type settings form.
 */
function _mlm_node_type_form($type, $filter_options = TRUE, $scope = '') {
  $form = array();
  $form['mlm_backends'. $scope] = array(
    '#type' => 'checkboxes',
    '#title' => t('Backends'),
    '#options' => _mlm_backends($filter_options, TRUE),
    '#default_value' => variable_get('mlm_backends_'. $type, array()),
    '#prefix' => '<div class="mlm-nodetype-backend">',
    '#suffix' => '</div>',
  );

  $form['mlm_list_type'. $scope] = array(
    '#type' => 'select',
    '#title' => t('List type'),
    '#options' => _mlm_list_types(),
    '#default_value' => variable_get('mlm_list_type_'. $type, ''),
    '#prefix' => '<div class="mlm-nodetype-listtype">',
    '#suffix' => '</div>',
  );

  $form['mlm_notify'. $scope] = array(
    '#type' => 'checkbox',
    '#title' => t('Use subscription messages'),
    '#description' => t('Send subscription status messages to users when they subscribe or unsubscribe from a list'),
    '#default_value' => variable_get('mlm_notify'. $type, ''),
    '#prefix' => '<div class="mlm-nodetype-notify">',
    '#suffix' => '</div>',
  );

  $form['mlm_notify_default'. $scope] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow administrators to override subscription message text'),
    '#description' => t('Allow each list to have its own notification messages.  Otherwise, all messages will use the defaults above.'),
    '#default_value' => variable_get('mlm_notify_default'. $type, ''),
    '#prefix' => '<div class="mlm-nodetype-notify-default">',
    '#suffix' => '</div>',
  );

/* TODO add roles support 
  $form['mlm_roles'. $scope] = array(
    '#type' => 'checkboxes',
    '#title' => t('Roles'),
    '#options' => user_roles(),
    '#default_value' => variable_get('mlm_roles_'. $type, array()),
  );

  $form['mlm_access_setting'. $scope] = array(
    '#type' => 'checkbox',
    '#title' => t('Hide lists'),
    '#default_value' => variable_get('mlm_access_setting_'. $type, false),
  );
 */
  return $form;
}

function _mlm_messages_form($node = null) {
  $default = !$node || variable_get('mlm_text_default_'. $node->type, TRUE);
  $form['messages'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('Subscription messages'),
    '#collapsible'   => true,
    '#collapsed'     => true,
  );
  $form['messages']['mlm_text_welcome'] = array(
    '#type'          => 'textarea',
    '#title'         => t('Welcome Text'),
    '#rows'          => 10,
    '#default_value' => $node->mlm_text->welcome,
    '#weight'        => 7,
  );
  $form['messages']['mlm_text_goodbye'] = array(
    '#type'          => 'textarea',
    '#title'         => t('Goodbye Text'),
    '#rows'          => 10,
    '#default_value' => $node->mlm_text->goodbye,
    '#weight'        => 9,
  );
  $form['messages']['mlm_text_trailer'] = array(
    '#type'          => 'textarea',
    '#title'         => t('Goodbye Text'),
    '#rows'          => 10,
    '#default_value' => $node->mlm_text->goodbye,
    '#weight'        => 9,
  );
  return $form;
}

/**
 * MLM settings on a node edit form.  Called from hook_nodeapi if MLM is
 * applicable to this node or node type.
 */
function _mlm_node_form($node=null) {
  global $user;
  $form = array();
  $backends = _mlm_backends($node->type, TRUE);

  if (count($backends) == 0)  {
    drupal_set_message(t('No available list backends'), 'error');
    echo theme('page', t('You must visit the '. l('settings page', 'admin/user/mlm/settings') .' and enable one or more backends before you can create a list'));
    exit;
  }

  $backend = false;
  if (isset($node->mlm_backend)) $backend = $node->mlm_backend;
  elseif (count($backends) == 1) $backend = key($backends);

  if ($backend) {
    $form['mlm_backend'] = array(
      '#type'         => 'value',
      '#value'        => $backend,
    );
    // Reduce "backends" array to just this selection.
    $backends = array($backend => $backends[$backend]);
  }
  else {
    drupal_add_js(drupal_get_path('module', 'mlm') .'/js/mlm-node-edit.js');
    $form['mlm_backend'] = array(
      '#type'          => 'select',
      '#title'         => t('Mailing list Backend'),
      '#options'       => $backends,
      '#default_value' => $node->mlm_backend,
      '#weight'        => -8,
    );
  }

  // list type selector
  $list_type = variable_get('mlm_list_type_'. $node->type, '');
  if (count($types = _mlm_list_types($backends)) == 1) {
    $list_type = key($types);
  }
  if ($list_type) {
    $form['mlm_list_type'] = array(
      '#type' => 'value',
      '#value' => $list_type,
    );
  }
  else {
    $form['mlm_list_type'] = array(
      '#type'          => 'select',
      '#title'         => t('List Type'),
      '#options'       => $types,
      '#required'      => true,
      '#default_value' => $node->mlm_list_type,
    );
  }

  foreach ($backends as $name => $label) {
    if ($extra = mlm_backend_call($name, 'node_form', $node)) {
      $key = 'mlm_'. $name;
      $form[$key] = $extra;
      $form[$key]['#type']  = 'fieldset';
      $form[$key]['#title'] = t('%backend settings', array('%backend' => $label));
      $form[$key]['#prefix'] = '<div id="mlm-'. $name .'-settings" class="mlm-settings">';
      $form[$key]['#suffix'] = '</div>';
    }
  }

  return $form;
}

/**
 * Validate hook for node edit form.
 */
function _mlm_node_form_validate($form_id, $values, &$form) {
  if (!$backend = $values['mlm_backend']) return;
  mlm_backend_call($backend, 'node_form_validate', $values, $form);
}

/**
 * Submit hook for node edit form.
 */
function _mlm_node_submit(&$node) {
  if (!in_array($node->type, variable_get('mlm_node_types', array()))) return;
}

/**
 * Save MLM settings for a node, called from hook_nodeapi if applicable.
 */
function _mlm_node_save(&$node) {
  if (!isset($node->mlm_backend)) return;
  $fields = array('mlm_backend', 'mlm_list_type', 'mlm_address', 'mlm_type');
  if ($node->is_new) {
    db_query("INSERT INTO {mlm} (nid, incoming_address, backend, list_type) VALUES (%d, '%s', '%s', '%s')",
      $node->nid, $node->mlm_incoming_address, $node->mlm_backend, $node->mlm_list_type);
  }
  else {
    db_query("UPDATE {mlm} set list_type = '%s' WHERE nid = %d", $node->mlm_list_type, $node->nid);
  }
  db_query("DELETE FROM {mlm_setting} WHERE nid = %d", $node->nid);
  foreach ($node as $name => $val) {
    if (substr($name, 0, 4) == 'mlm_' && !in_array($name, $fields)) {
      if (!$val) continue;
      $name = substr($name, 4);
      if (substr($name, 0, strlen($node->mlm_backend)) == $node->mlm_backend) {
        $type = $node->mlm_backend;
      }
      else {
        $type = substr($name, 0, strpos($name, '_'));
      }
      if ($type) $name = substr($name, strlen($type)+1);

      // Handle checkboxes.  TODO: /only/ handles checkboxes...
      if (is_array($val)) {
        foreach ($val as $i => $v) if (!$v) unset($val[$i]);
        $val = join(',', $val);
      }
      if (is_string($val) || is_int($val)) {
        db_query("INSERT INTO {mlm_setting} ( nid, type, name, value )
            VALUES (%d, '%s', '%s', '%s')", $node->nid, $type, $name, $val);
      }
    }
  }

  mlm_backend_call($node->mlm_backend, 'save', $node);

  // Store thread information
  _mlm_set_thread($node, null);

  // Stay in the admin section
  if (substr($_GET['q'], 0, 5) == 'admin') {
    $_REQUEST['destination'] = 'admin/user/mlm';
  }
}

/**
 * Delete MLM settings for a node, called from hook_nodeapi on delete.
 */
function _mlm_node_delete(&$node) {
  db_query("DELETE FROM {mlm} WHERE nid = %d", $node->nid);
  db_query("DELETE FROM {mlm_setting} WHERE nid = %d", $node->nid);
  db_query("DELETE FROM {mlm_slog} WHERE nid = %d", $node->nid);
  mlm_backend_call($node->mlm_backend, 'delete', $node);
}

/**
 * Menu callback for a "subscribe" link on an MLM node.
 */
function _mlm_node_subscribe(&$list, $mail='') {
  if (!$mail) {
    global $user;
    $mail = $user;
  }
  mlm_subscribe($list, $mail);
  drupal_goto();
}

/**
 * Menu callback for an "unsubscribe" link on an MLM node.
 */
function _mlm_node_unsubscribe(&$list, $mail='') {
  if (!$mail) {
    global $user;
    $mail = $user;
  }
  mlm_unsubscribe($list, $mail);
  drupal_goto();
}

/**
 * Subscription import/export form on an MLM node.
 */
function _mlm_node_subscribers_form($list) {
  if (is_numeric($list)) $list = node_load($list);

  $form = array('#base' => '_mlm_node_subscribers_form');
  $form['list'] = array('#type' => 'value', '#value' => $list);
  $form['import'] = array(
    '#type' => 'fieldset',
    '#title' => t('Add Subscribers'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#tree' => TRUE,
  );

  if (user_access('upload files')) {
    $form['import']['file'] = array(
      '#type' => 'file',
      '#title' => t('Import subscribers from a file'),
    );
    $form['#attributes'] = array('enctype' => 'multipart/form-data');
  }
  $form['import']['text'] = array(
    '#type' => 'textarea',
    '#rows' => 20,
    '#title' => t('Add subscribers manually'),
  );
  if ($list->mlm_text->welcome) {
    $form['import']['notify'] = array(
      '#type' => 'checkbox',
      '#title' => t('Send Welcome E-mail'),
    );
  }
  $form['import']['submit'] = array('#type' => 'submit', '#value' => t('add subscribers'));

  $res = mlm_subscribers($list, 'subscriber', 1);
  if ($row = db_fetch_array($res)) {
    $fields = array();
    foreach (array_keys($row) as $f) $fields[$f] = $f;
  }

  $form['export'] = array(
    '#type' => 'fieldset',
    '#title' => t('Export List'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#tree' => TRUE,
  );
  if (count($fields) == 1) {
    $form['export']['fields'] = array(
      '#type' => 'value',
      '#value' => $fields,
    );
  }
  else {
    $form['export']['fields'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Fields to Export'),
      '#options' => $fields,
      '#default_value' => $fields,
    );
  }
  $form['export']['submit'] = array('#type' => 'submit', '#value' => t('export'));

  $form['subscribers'] = array('#type' => 'markup', '#value' => _mlm_node_subscribers($list));

  return $form;
}

/**
 * Submit hook for a subscription import/export form on an MLM node.
 */
function _mlm_node_subscribers_form_submit($form_id, $values) {
  $list = $values['list'];

  if ($values['op'] == 'add subscribers') {
    // Handle bulk imports.
    if ($values['import']['file']) {
print_r($values); die;
      _mlm_node_subscribers_import($list, $values);
    }

    // Handle addresses via manual text input.
    if ($text = trim(preg_replace('/<[^>]*>/', ' ', $values['import']['text']))) {
      $text = preg_replace(array('/\s/ms', '/,+/'), ',', $text);
      foreach (explode(',', $text) as $mail) {
        if (valid_email_address($mail)) {
          mlm_subscribe($list, $mail, 'subscriber', $values['import']['notify'], false);
        }
        else {
          drupal_set_message(t('%mail is not a valid email address', array('%mail' => $mail)), 'error');
        }
      }
    }
  }

  if ($values['op'] == 'export') {
    if ($res = mlm_subscribers($list, 'subscriber', 0)) {
      header('Content-Type: text/csv');
      header('Content-Disposition: attachment; filename='. urlencode($list->title) .'-'. date('Y-m-d') .'.csv');
      while ($row = db_fetch_object($res)) {
        if (!isset($header)) {
          $header = array();
          foreach ($values['export']['fields'] as $field) {
            if ($field) $header[] = strtoupper($field);
          }
          echo join(',', $header) ."\n";
        }
        $return = array();
        foreach ($values['export']['fields'] as $field) {
          if ($field) $return[] = $row->$field;
        }
        echo join(',', $return) ."\n";
      }
      return;
    }
    drupal_set_message(t('Error fetching subscribers'));
  }
}

/**
 * List of subscribers, in table form.
 */
function _mlm_node_subscribers($list) {
  $result = mlm_backend_call($list->mlm_backend, 'subscribers', $list);
  $rows = array();
  while ($row = db_fetch_array($result)) {
    if (!isset($header)) $header = array_merge(array_keys($row), array(('operations')));
    $row[] = l(t('unsubscribe'), 'node/'. $list->nid .'/unsubscribe/'. current($row), '', drupal_get_destination());
    $rows[] = $row;
  }
  $output .= theme('table', $header, $rows);
  $output .= theme('pager', NULL, 50, 0);
  return $output;
}

/**
 * Callback function to handle batch imports.
 * Maintains its progress in the user's session to avoid timeouts during
 * giant bulk imports.
 */
function _mlm_node_subscribers_import($list, $values = array()) {
  $nid = $list->nid;
  if (!isset($_SESSION['mlm_filename_'. $nid]) && !count($_FILES)) {
    return;
  }

  ini_set('auto_detect_line_endings', TRUE);

  if (isset($_SESSION['mlm_filename_'. $nid])) {
    $filename = $_SESSION['mlm_filename_'. $nid];
  }
  elseif (is_uploaded_file($file = $_FILES['files']['tmp_name']['import_file'])) {
    $filename = file_directory_path() .'/'. $_FILES['files']['name']['import_file'];
    move_uploaded_file($file, $filename );
    $_SESSION['mlm_filename_'. $nid] = $filename;
  }
  else {
    // not uploading a file at all
    return;
  }

  if ($timelimit = ini_get('max_execution_time')) {
    $timelimit = time() + $timelimit - 5; // 5-second padding on max_execution_time
  }

  if (!$fp = fopen($filename, 'r')) {
    drupal_set_message(t('Error opening file'), 'error');
  }

  $fpos = (isset($_SESSION['mlm_fpos_'. $nid])) ? $_SESSION['mlm_fpos_'. $nid] : 0;
  $ret = true;
  fseek($fp, $fpos);
  while ($row = fgetcsv($fp, 4096)) {

    // only allowing 1-column imports
    if (count($row) == 1) {
      $mail = $row[0];
    }
    else {
      drupal_set_message(t('Please upload a file with 1 address per line'), 'error');
    }
    if (valid_email_address($mail)) {
      $ret = mlm_subscribe($list, $mail, 'subscriber', $notify, true) && $ret;
    }
    else {
      drupal_set_message(t('%mail is not a valid email address', array('%mail' => $mail)), 'error');
    }

    // time's up!  clean up and try again
    if ($timelimit && (time() >= $timelimit)) {
      $_SESSION['mlm_fpos_'. $nid] = ftell($fp);
      drupal_set_message(t('File too long for one load. Trying to break it up'));
      drupal_goto($_GET['q']);
    }
  }
  if ($ret) {
    drupal_set_message(t('Subscribers imported successfully'));
  }
  unlink($filename);
  unset($_SESSION['mlm_fpos_'. $nid], $_SESSION['mlm_filename_'. $nid]);
  drupal_goto($_GET['q']);
}

/**
 * Administration form for list management.
 */
function _mlm_admin_overview() {
  $backends = _mlm_backends();
  $sql = "SELECT n.nid, n.status, l.list_type, l.backend, title
      FROM {node} n INNER JOIN {mlm} l USING (nid)";

  $header = array(
    array('data' => t('Name'), 'field' => 'title', 'sort' => 'asc'),
    array('data' => t('Active'), 'field' => 'status'),
    array('data' => t('Backend'), 'field' => 'backend'),
    array('data' => t('Type'), 'field' => 'type'),
    array('data' => t('Subscribers')),
    array('data' => t('Operations'), 'colspan' => '2')
  );
  $sql .= db_rewrite_sql(tablesort_sql($header));
  $result = pager_query($sql, 50);

  $dest = drupal_get_destination();
  while ($row = db_fetch_object($result)) {
    $count = mlm_backend_call($row->backend, 'count', node_load($row->nid));
    if ($count) $count = number_format((int) $count);
    $rows[] = array(
      l($row->title, 'node/'. $row->nid),
      $row->status,
      $backends[$row->backend]->name,
      $row->list_type,
      array('data' => $count ? l($count, 'admin/user/mlm/list/'. $row->nid) : '-', 'align' => 'right'),
      l(t('edit'), 'node/'. $row->nid .'/edit', array(), $dest) .' ', '');
  }

  if (!$rows) {
    $rows[] = array(array('data' => t('No lists available.'), 'colspan' => '4'));
  }

  $output = theme('table', $header, $rows);
  $output .= theme('pager', NULL, 50, 0);
  return $output;
}

/**
 * Administration form for overall MLM features and settings.
 */
function _mlm_admin_settings() {
  drupal_add_js(drupal_get_path('module', 'mlm') .'/js/mlm-settings.js');

  $form = array();

  // Get a list of node types.
  $types = array();
  foreach (node_get_types() as $key => $type) {
    if ($key) $types[$key] = $type->name;
  }

  // And a list of backends.
  $backends = _mlm_backends(FALSE, TRUE);

  $form['general'] = array(
    '#type' => 'fieldset',
    '#collapsible' => true,
    '#title' => t('General settings'),
  );

  $form['general']['mlm_nodetypes'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Mailing list content types'),
    '#description' => t('Content types that can be used as mailing lists.'),
    '#options' => $types,
    '#default_value' => variable_get('mlm_nodetypes', array('mlm')),
    '#prefix' => '<div id="mlm-nodetypes">',
    '#suffix' => '</div>',
  );

  $form['general']['mlm_backends'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Backends'),
    '#options' => $backends,
    '#default_value' => variable_get('mlm_backends', array()),
    '#prefix' => '<div id="mlm-backends">',
    '#suffix' => '</div>',
  );

  $form['general']['mlm_access_control'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use access control'),
    '#default_value' => variable_get('mlm_access_control', array()),
  );

  $form['general']['mlm_help'] = array(
    '#type' => 'textarea',
    '#title' => t('Subscription help text'),
    '#description' => t('This text will appear on the !mlm subscriptions page.  Enter some helpful or descriptive text for users changing their preferences.', array('!mlm' => l('mlm', 'mlm'))),
    '#default_value' => variable_get('mlm_help', ''),
  );
  $form['general']['mlm_help_format'] = filter_form(variable_get('mlm_help_format', FILTER_FORMAT_DEFAULT), null, array('mlm_help_format'));

  $form['types'] = array(
    '#type' => 'fieldset',
    '#collapsible' => true,
    '#title' => t('Content type settings'),
    '#prefix' => '<div id="mlm-nodetype-settings">',
    '#suffix' => '</div>',
  );

  foreach ($types as $type => $label) {
    $form['types'][$type] = _mlm_node_type_form($type, FALSE, '_'. $type);
    $form['types'][$type]['#type'] = 'fieldset';
    $form['types'][$type]['#title'] = $label;
    $form['types'][$type]['#prefix'] = '<div id="mlm-nodetype-'. $type .'">';
    $form['types'][$type]['#suffix'] = '</div>';
  }

  $form['backends'] = array(
    '#type' => 'fieldset',
    '#collapsible' => true,
    '#title' => t('Backend settings'),
    '#prefix' => '<div id="mlm-backend-settings">',
    '#suffix' => '</div>',
  );

  foreach ($backends as $name => $label) {
    if ($backend_form = mlm_backend_call($name, 'backend_settings')) {
      $form['backends'][$name] = $backend_form;
      $form['backends'][$name]['#type'] = 'fieldset';
      $form['backends'][$name]['#title'] = $label;
      $form['backends'][$name]['#prefix'] = '<div id="mlm-backend-'. $name .'">';
      $form['backends'][$name]['#suffix'] = '</div>';
    }
  }

  return system_settings_form($form);
}

/**
 * Page callback to handle user subscription requests for all lists.
 */
function _mlm_subscriptions_page($nid = null, $mail='') {
  global $user;

  // Set a reasonable $mail default
  if (!valid_email_address($mail)) $mail = '';
  if (!$mail && $user) $mail = $user->mail;

  if ($nid) {
    $lists = array(node_load($nid));
  }
  else {
    $lists = mlm_lists(null, null, $mail);
  }
  if (!count($lists)) {
    return t('There are no mailing lists for this site');
  }

  $form = array('#base' => 'mlm_subscribe_form');

  $help = variable_get('mlm_help', '');
  $help = check_markup($help, variable_get('mlm_help_format', 0));
  $form['help'] = array(
    '#type' => 'markup',
    '#value' => $help,
  );
  if (user_access('administer mlm')) {
    $form['help']['admin'] = array(
      '#type' => 'markup',
      '#value' => l(t('Edit help text'), 'admin/user/mlm/settings'),
    );
  }

  if ($mail && $mail == $user->mail) {
    $form['mlm_mail'] = array(
      '#type' => 'value',
      '#value' => $mail,
    );
  }
  else {
    $form['mlm_mail'] = array(
      '#type' => 'textfield',
      '#title' => t('E-Mail Address'),
      '#default_value' => $mail ? $mail : t('email address'),
      '#size' => 20,
      '#maxlength' => 80
    );
    if (!$mail) {
      $form['mlm_mail']['#attributes'] = array('onclick' => "this.value='';");
    }
  }
  $form['mlm_lists'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Mailing Lists'),
  );
  foreach ($lists as $l) {
    $form['mlm_lists'][$l->nid] = array(
      '#type' => 'checkbox',
      '#default_value' => (int) $l->subscribed,
      '#title' => l($l->title, 'node/'. $l->nid),
      '#prefix' => '<div class="mlm-list">',
      '#suffix' => '</div>',
      '#description' => $l->teaser,
    );
  }

  $form['submit'] = array('#type' => 'submit', '#value' => t('Save Preferences'));
  return $form;
}

/**
 * All-purpose callback to save a list of subscriptions preferences for
 * one or more lists.  Called from the list node form, block, and mlm page.
 */
function _mlm_save_subscriptions($mail, $selections = array(), $type='subscriber', $notify=true) {
  $lists = mlm_lists(null, null, $mail);

  foreach ($selections as $nid => $val) {
    if (!$val && $lists[$nid]->subscribed) {
      mlm_unsubscribe($lists[$nid], $mail, $type, $notify);
    }
    elseif ($val && !$lists[$nid]->subscribed) {
      mlm_subscribe($lists[$nid], $mail, $type, $notify);
    }
  }
}

/**
 * Configurtion settings, called by hook_block.
 */
function _mlm_subscriber_block_configure($edit=array()) {
  if (count($edit))  {
    foreach ($edit['mlm_block_lists'] as $k => $v) {
      if (!$v) unset($edit['mlm_block_lists'][$k]);
    }
    variable_set('mlm_block_helptext', $edit['mlm_block_helptext']);
    variable_set('mlm_block_lists', $edit['mlm_block_lists']);
    variable_set('mlm_block_hide', $edit['mlm_block_hide']);
    return;
  }

  $options = array();
  $values = variable_get('mlm_block_lists', array());

  foreach (mlm_lists() as $list => $info) {
    $options[$list] = $info->title;
  }
  $form['mlm_block_helptext'] = array(
    '#type' => 'textarea',
    '#title' => t('Help Text '),
    '#rows' => 5,
    '#default_value' => variable_get('mlm_block_helptext', ''),
  );
  $form['mlm_block_lists'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Available Lists'),
    '#options' => $options,
    '#default_value' => $values,
  );
  $form['mlm_block_hide'] = array(
    '#type' => 'checkbox',
    '#title' => t('Don\'t show subscribed'),
    '#description' => t('Hide available lists from users who are already subscribed'),
    '#default_value' => variable_get('mlm_block_hide', 1),
  );
  return $form;
}

/*
 * Archive an incoming message received via hook_mimemail_incoming()
 */
function _mlm_archive($list, $message) {
  global $user;

  // We'll be messing with the main user object for ownership and such.
  $old_user = $user;

  //  Make sure we haven't done this already.
  if (isset($message['headers']['X-Archived-By'])) {
    if ($message['headers']['X-Archived-By'] == $_SERVER['SERVER_NAME']) return;
  }

  //  Find parent message id for this thread.
  if (isset($message['headers']['In-Reply-To'])) {
    $parent_msgid = $message['headers']['In-Reply-To'];
  }
  elseif (isset($message['headers']['References'])) {
    $parent_msgid = $message['headers']['References'];
  }
  if ( $parent_msgid ) {
    $parent = db_fetch_object(db_query("SELECT * FROM {mlm_thread} WHERE msgid = '%s'", $parent_msgid));
  }

  // This message probably doesn't belong to MLM.
  if (!$parent) return;

  // TODO Validate user's list membership
  if ($new_user = user_load(array('mail' => $mail['from'])) ) {
    $user = $new_user;
  }

  $title     = $message['headers']['Subject'];
  $body      = _mlm_archive_cleanup($message['html']);;
  $timestamp = strtotime($message['headers']['Date']);

  $status    = 1;

  if ( false ) {  // TODO configurable node types.  For now we use comments.
    $type  = 'news';
    $node = array(
      'type' => $type,
      'mlm_parent' => $parent,
      'created'     => $timestamp,
      'name'        => $user->name,
      'mlm_message_source' => 'mail',
      'mlm_message_id' => $message['headers']['Message-Id'],
    );
    $values = array(
      'title'       => $title,
      'body'        => $body,
      'changed'     => time(),
      'status'      => $status,
      '#programmed' => true,
    );
    drupal_execute($type .'_node_form', $values, $node);
  }
  else {
    $values = array(
      'timestamp'  => $timestamp,
      'uid'        => $user->uid,
      'status'     => 0, // TODO
      'name'       => $user->name,
      'mail'       => $message['from'],
      'subject'    => $title,
      'comment'    => $body,
      'format'     => FILTER_FORMAT_DEFAULT,
      'nid'        => $parent->nid,
      'pid'        => $parent->cid,
      'mlm_message_source' => 'mail',
      'mlm_message_id' => $message['headers']['Message-Id'],
    );

    // I couldn't get drupal_execute() to work for comments.
    $values['cid'] = comment_save($values);

    // comment_save() hard-codes the timestamp and makes assumptions
    // about status -- so we re-save
    comment_save($values);
  }

  $user = $old_user;
}

/**
 * Post a comment to a mailing list.  Called from hook_comment if applicable.
 */
function _mlm_comment_post($comment) {
  $list    = node_load($comment['nid']);
  $msgid   = _mlm_set_thread($list, $comment);
  $body    = check_markup($comment['comment'], $comment['format']);

  mlm_post($list, $comment['mail'], $comment['subject'], $body, $msgid);
}

/*
 * Called from hook_nodeapi and hook_comment.
 * Determines the correct placement in the thread hierarchy and saves the value
 */
function _mlm_set_thread($node, $comment=null) {
  $cid    = $comment ? $comment['cid'] : 0;
  $uid    = $comment ? $comment['uid'] : $node->uid;

  // Already threaded
  if ($res = db_fetch_array(db_query("SELECT msgid FROM {mlm_thread} WHERE nid = %d AND cid = %d", $node->nid, $cid))) {
    return $res['msgid'];
  }

  $ts     = $comment ? $comment['timestamp'] : $node->created;
  $tid    = db_next_id("{mlm_thread}_tid");

  $source = 'drupal';  // Where this message came from ( e.g. drupal or mail )
  if ($comment && isset($comment['mlm_message_source'])) {
    $source = $comment['mlm_message_source'];
  }
  elseif (isset($node->mlm_message_source)) {
    $source = $node->mlm_message_source;
  }

  // Use the message id supplied by the user's MUA.
  $msgid  = '<'. $ts .'.'. $tid .'@'. $_SERVER['SERVER_NAME'] .'>';
  if ($comment && isset($comment['mlm_message_id'])) {
    $msgid  = $comment['mlm_message_id'];
  }
  elseif (isset($node->mlm_message_id)) {
    $msgid  = $node->mlm_message_id;
  }

  // Get the previous entry in the thread
  $pid = 0;
  if ($comment) {
    $row = db_fetch_object(db_query("SELECT tid FROM {mlm_thread} WHERE cid = %d", $comment['pid']));
    $pid = $row->tid;
  }

  db_query("INSERT INTO {mlm_thread} ( tid, pid, nid, cid, uid, source, msgid )
    VALUES ( %d, %d, %d, %d, %d, '%s', '%s' )", $tid, $pid, $node->nid, $cid, $uid, $source, $msgid );

  return $msgid;
}

/*
 * TODO abstract this better!  input filter to convert data? add to mimemail?
 */
function _mlm_archive_cleanup($text) {

  // Convert all Windows and Mac newlines to a single newline,
  // so filters only need to deal with one possibility.
  $text = str_replace(array("\r\n", "\r"), "\n", $text);

  // If this is HTML, we're only interested in the body content
  $text = preg_replace('/(.*<body[^>]*>)?(.*?)(<\/body>.*)?/is', '\2', $text);

  // Down with top-quoting!
  //This works, but only with some versions of Outlook:
  //$text = preg_replace('/(.*?)(<blockquote.*)?-----Original Message-----.*/is', '\1', $text);

  $text = preg_replace('/(.*?)From:.{1,200}Sent:.{1,100}To:.{1,200}.*/is', '\1', $text);

  // TODO Strip MSN and Yahoo signatures
  // $text = preg_replace('/(<hr[^>]>|-{5,}\n)(.*\n){0,2}.*(msn|yahoo)[^\n]\n/i', '', $text);

  // TODO Get rid of trailing rows containing entirely >'s and whitespace
  //$striplength = strspn(strrev($text),"> \t\n\r");
  //if($striplength) {
    //$text = substr($text,0,-$striplength);
  //}

  // TODO Convert mail-based brackets into quote markup
  //if ( module_exists('quote')) {
    //$text = preg_replace('/((<br[^>]*>)\s?(&gt;)([^<]*)/im)+/i', '[quote]\2\4[/quote]', $text);
  //}

  return $text;
}

/**
 * Subject line for MLM posts sent by e-mail.
 * @ingroup themeable
 */
function theme_mlm_post_subject($list, $subject) {
  $prefix = '';
  //if ($mlm->mlm_list_type == 'discussion') {
    $prefix = '['. variable_get('site_name', 'Drupal') .'] ';
  //}
  $subject = preg_replace($prefix, '', $subject);
  $subject = preg_replace('/^(\s*RE\s?:\s*)+/i', "Re: $prefix", $subject, -1, $c);
  if (!$c) $subject = $prefix . $subject;
  return $subject;
}

/**
 * Message body for MLM posts sent by e-mail.
 * @ingroup themeable
 */
function theme_mlm_post_body($list, $body) {
  return $body . theme('mlm_post_trailer', $list);
}

/**
 * Message trailer for MLM posts sent by e-mail.
 * @ingroup themeable
 */
function theme_mlm_post_trailer($list) {
  $out = '';
  return $out;
}
