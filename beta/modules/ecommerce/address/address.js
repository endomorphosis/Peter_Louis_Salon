// $Id: address.js,v 1.1.2.3.4.2 2007/11/15 01:46:40 gordon Exp $

if (Drupal.jsEnabled) {
  $(document).ready( function () {
    $('#edit-country').each( function () {
      Drupal.addressToggleFields(this.options[this.selectedIndex].value);
    });
    $('#edit-state').each( function () {
      this.remove(this.length);
    });
    $('#edit-country').change( function() {
      Drupal.addressToggleFields(this.options[this.selectedIndex].value);
    });
  });
}

Drupal.addressToggleFields = function(country) {
  if (country == 'us') {
    $('#wrapper-province').hide();
    $('#wrapper-state').show();
  }
  else {
    $('#wrapper-province').show();
    $('#wrapper-state').hide();
  }
}
