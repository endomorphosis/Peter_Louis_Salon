# $Id: ec_mail.pot,v 1.1.2.1 2007/05/03 14:52:05 darrenoh Exp $
#
# LANGUAGE translation of Drupal (ec_mail.module)
# Copyright YEAR NAME <EMAIL@ADDRESS>
# Generated from files:
#  ec_mail.module,v 1.1.2.16.2.13 2007/04/25 01:36:19 gordon
#  ec_mail.install,v 1.1.2.2.2.4 2007/02/19 03:07:55 gordon
#  ec_mail.info,v 1.1.2.8 2007/02/25 14:03:35 gordon
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"POT-Creation-Date: 2007-05-03 10:36-0400\n"
"PO-Revision-Date: YYYY-mm-DD HH:MM+ZZZZ\n"
"Last-Translator: NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <EMAIL@ADDRESS>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: ec_mail.module:44
msgid "Mail"
msgstr ""

#: ec_mail.module:53
msgid "List of mails"
msgstr ""

#: ec_mail.module:61;210
msgid "Add mail"
msgstr ""

#: ec_mail.module:70
msgid "Mail tokens"
msgstr ""

#: ec_mail.module:82
msgid "Mail: \"@name\""
msgstr ""

#: ec_mail.module:90
msgid "View mail"
msgstr ""

#: ec_mail.module:98
msgid "Edit mail"
msgstr ""

#: ec_mail.module:107
msgid "Delete mail"
msgstr ""

#: ec_mail.module:152
msgid "name"
msgstr ""

#: ec_mail.module:152
msgid "type"
msgstr ""

#: ec_mail.module:152
msgid "op"
msgstr ""

#: ec_mail.module:154
msgid "No mails were found"
msgstr ""

#: ec_mail.module:183
msgid "Mail type"
msgstr ""

#: ec_mail.module:190
msgid "Name"
msgstr ""

#: ec_mail.module:196;318;831
msgid "Subject"
msgstr ""

#: ec_mail.module:199;206;835;843
msgid "See available tokens"
msgstr ""

#: ec_mail.module:203;324;839
msgid "Body"
msgstr ""

#: ec_mail.module:210
msgid "Update mail"
msgstr ""

#: ec_mail.module:224
msgid "Name is too long"
msgstr ""

#: ec_mail.module:227
msgid "Subject is too long"
msgstr ""

#: ec_mail.module:259
msgid "Mail added"
msgstr ""

#: ec_mail.module:259
msgid "Mail updated"
msgstr ""

#: ec_mail.module:260;272
msgid "edit"
msgstr ""

#: ec_mail.module:268
msgid "view"
msgstr ""

#: ec_mail.module:277
msgid "delete"
msgstr ""

#: ec_mail.module:302;339
msgid "This mail is currently in use and can not be deleted."
msgstr ""

#: ec_mail.module:329
msgid "Are you sure you want to delete this mail?"
msgstr ""

#: ec_mail.module:331;376
msgid "This action cannot be undone."
msgstr ""

#: ec_mail.module:332;377
msgid "Delete"
msgstr ""

#: ec_mail.module:333;377
msgid "Cancel"
msgstr ""

#: ec_mail.module:375
msgid "Are you sure you want to delete all%type mails?"
msgstr ""

#: ec_mail.module:502
msgid "<p>This is a preview of the message.</p>"
msgstr ""

#: ec_mail.module:503
msgid "<p><strong>From:</strong> %from<br />"
msgstr ""

#: ec_mail.module:504
msgid "<strong>To:</strong> %to<br />"
msgstr ""

#: ec_mail.module:505
msgid "<strong>Subject:</strong> %subject</p>"
msgstr ""

#: ec_mail.module:506
msgid "<p><strong>Body:</strong></p><pre>%body</pre>"
msgstr ""

#: ec_mail.module:548
msgid "mail types"
msgstr ""

#: ec_mail.module:553
msgid "see tokens for all mail types"
msgstr ""

#: ec_mail.module:793
msgid "Mail template for %var could not be found!"
msgstr ""

#: ec_mail.module:805
msgid "<p>The message shown below is to be sent from %from to %to. You can take this opportunity to modify the message before sending it.</p>"
msgstr ""

#: ec_mail.module:825
msgid "To"
msgstr ""

#: ec_mail.module:845;868
msgid "Preview"
msgstr ""

#: ec_mail.module:846
msgid "Submit"
msgstr ""

#: ec_mail.module:854
msgid "To address is not a valid email address."
msgstr ""

#: ec_mail.module:871
msgid "Sending the mail failed. It was not accepted for delivery."
msgstr ""

#: ec_mail.module:927
msgid "mail"
msgstr ""

#: ec_mail.module:930
msgid "The mail templates shown here can be modified on the !mailpage."
msgstr ""

#: ec_mail.module:930
msgid "EC Mail settings page"
msgstr ""

#: ec_mail.module:260
msgid "ec_mail"
msgstr ""

#: ec_mail.module:32
msgid "administer ec emails"
msgstr ""

#: ec_mail.install:43
msgid "E-Commerce: Mail tables have been created."
msgstr ""

#: ec_mail.info:0
msgid "Mail subsystem"
msgstr ""

#: ec_mail.info:0
msgid "Implements mail API and centralized mail template management."
msgstr ""

#: ec_mail.info:0
msgid "E-Commerce Core"
msgstr ""

