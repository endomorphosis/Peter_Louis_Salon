<?php
/* $Id: amazon_US.inc,v 1.1 2007/01/02 01:54:06 prometheus6 Exp $ */
  global $_amazon_search_browse_fields;
  global $_amazon_search_browse_nodes;
  /**
    * $_amazon_search_browse_fields holds the field to search for each SearchIndex.
    * Valid combinations can be found at
    * http://www.amazon.com/gp/aws/sdk/main.html?s=AWSEcommerceService&v=4-0&p=ApiReference/UsSearchIndexMatrixArticle
  */
  $_amazon_search_browse_fields =
  array(
    'Books' => array('Author'=>'Author','Keywords'=>'Keywords','Title'=>'Title'),
    'Music' => array('Artist' => 'Artist','Keywords'=>'Keywords', 'Title' => 'Title',),
    'DVD' => array('Actor' => 'Actor','Keywords'=>'Keywords', 'Director' => 'Director', 'Title'=>'Title',),
  );

  $_amazon_search_browse_nodes =
  array(
    'Books'=> array(
      0=>'All',
      1=>'Arts & Photography',
      2=>'Biographies & Memoirs',
      3=>'Business & Investing',
      4=>'Children\'s Books',
      4366=>'Comics & Graphic Novels',
      5=>'Computers & Internet',
      6=>'Cooking, Food & Wine',
      13643=>'Engineering',
      86=>'Entertainment',
      301889=>'Gay & Lesbian',
      10=>'Health, Mind & Body',
      9=>'History',
      48=>'Home & Garden',
      49=>'Horror',
      10777=>'Law',
      17=>'Literature & Fiction',
      13996=>'Medicine',
      18=>'Mystery & Thrillers',
      53=>'Nonfiction',
      290060=>'Outdoors & Nature',
      20=>'Parenting & Families',
      173507=>'Professional & Technical',
      21=>'Reference',
      22=>'Religion & Spirituality',
      23=>'Romance',
      75=>'Science',
      25=>'Science Fiction & Fantasy',
      26=>'Sports',
      28=>'Teens',
      27=>'Travel'
    ),
    'Music' => array(
       0=>'All',
			30 => 'Alternative Rock',
			31 => 'Blues',
			265640 => 'Broadway & Vocalists',
			173425 => 'Children\'s Music',
			173429 => 'Christian & Gospel',
			37204 => 'Classic Rock',
			85 => 'Classical',
			16 => 'Country',
			7 => 'Dance & DJ',
			32 => 'Folk',
			67207 => 'Hard Rock & Metal',
			33 => 'International',
			34 => 'Jazz',
			289122 => 'Latin Music',
			35 => 'Miscellaneous',
			36 => 'New Age',
			84 => 'Opera & Vocal',
			37 => 'Pop',
			39 => 'R&B',
			38 => 'Rap & Hip-Hop',
			40 => 'Rock',
			42 => 'Soundtracks',
    ),
    'DVD' => array(
      0=>'All',
	  	163296 => 'Action & Adventure',
			538708 => 'African American Cinema',
			712256 => 'Animation',
			517956 => 'Anime & Manga',
			163313 => 'Art House & International',
			163345 => 'Classics',
			163357 => 'Comedy',
			466674 => 'Cult Movies',
			508532 => 'Documentary',
			163379 => 'Drama',
			290738 => 'Educational',
			578324 => 'Fitness & Yoga',
			301667 => 'Gay & Lesbian',
			163396 => 'Horror',
			163414 => 'Kids & Family',
			586156 => 'Military & War',
			163420 => 'Music Video & Concerts',
			508528 => 'Musicals & Performing Arts',
			512030 => 'Mystery & Suspense',
			163431 => 'Science Fiction & Fantasy',
			163448 => 'Special Interests',
			467970 => 'Sports',
			163450 => 'Television',
			163312 => 'Westerns',
    ),
  );


