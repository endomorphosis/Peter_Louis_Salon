<?php
/* $Id: amazon_FR.inc,v 1.2 2007/01/07 04:55:45 prometheus6 Exp $ */
  global $_amazon_search_browse_fields;
  global $_amazon_search_browse_nodes;
  /**
    * $_amazon_search_browse_fields holds the field to search for each SearchIndex.
    * Valid combinations can be found at
    * http://www.amazon.com/gp/aws/sdk/main.html?s=AWSEcommerceService&v=4-0&p=ApiReference/UsSearchIndexMatrixArticle
  */
  $_amazon_search_browse_fields =
  array(
    'Books' => array('Author'=>'Author','Keywords'=>'Keywords','Title'=>'Title'),
    'Music' => array('Artist' => 'Artist','Keywords'=>'Keywords', 'Title' => 'Title',),
    'DVD' => array('Actor' => 'Actor','Keywords'=>'Keywords', 'Director' => 'Director', 'Title'=>'Title',),
  );

  $_amazon_search_browse_nodes =
  array(
		'Books' => array(
			468256 => 'Actions �ditoriales',
			692186 => 'Auteurs de A � Z',
			655842 => 'Formats',
			463892 => 'Id�es cadeaux',
			585472 => 'Meilleures ventes',
			4140171 => 'Occasions et livres rares',
			518572 => 'Par �diteur',
			564382 => 'Partners',
			301130 => 'Th�mes',
		),
		'DVD' => array(
			923422 => 'Acteurs de A � Z',
			527596 => 'Actions �ditoriales',
			4142801 => 'DVD d\'occasion',
			409392 => 'Genres',
			596284 => 'Imports',
			463900 => 'Meilleures ventes',
			463904 => 'Nouveaut�s et � para�tre',
			1003194 => 'R�alisateurs de A � Z',
			908890 => 'Titres de A � Z',
		),
		'Music' => array(
			537366 => 'Actions �ditoriales',
			541638 => 'Imports',
			541640 => 'Nouveaut�s et � para�tre',
			4140191 => 'Disques d\'occasion',
			777120 => 'Formats',
			301164 => 'Genres',
			415810 => 'Labels de l�gende',
			463912 => 'Meilleures ventes',
			749240 => 'Partners',
			587174 => 'R�compenses',
		),
	);