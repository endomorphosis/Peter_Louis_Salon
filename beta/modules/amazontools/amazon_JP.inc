<?php
/* $Id: amazon_JP.inc,v 1.2 2007/01/07 04:55:45 prometheus6 Exp $ */
  global $_amazon_search_browse_fields;
  global $_amazon_search_browse_nodes;
  /**
    * $_amazon_search_browse_fields holds the field to search for each SearchIndex.
    * Valid combinations can be found at
    * http://www.amazon.com/gp/aws/sdk/main.html?s=AWSEcommerceService&v=4-0&p=ApiReference/UsSearchIndexMatrixArticle
  */
  $_amazon_search_browse_fields =
  array(
    'Books' => array('Author'=>'Author','Keywords'=>'Keywords','Title'=>'Title'),
    'Music' => array('Artist' => 'Artist','Keywords'=>'Keywords', 'Title' => 'Title',),
    'DVD' => array('Actor' => 'Actor','Keywords'=>'Keywords', 'Director' => 'Director', 'Title'=>'Title',),
  );

  $_amazon_search_browse_nodes =
  array(
		'Books' => array(
			466284 => 'Literature & Fiction',
			571582 => 'Philosophy & Religion',
			571584 => 'Society & Politics',
			492152 => 'Nonfiction',
			466286 => 'Travel & Geography',
			466282 => 'Business & Career',
			492054 => 'Investment & Financial Management',
			466298 => 'Computer & Internet',
			466294 => 'Art & Photography',
			466296 => 'Entertainment',
			466292 => 'Sports & Hobby',
			466304 => 'Home & Family',
			466302 => 'Foreign Language Reference',
			466306 => 'Children\'s Books',
			466280 => 'Cartoons & Anime',
			466300 => 'New Releases',
			746102 => 'Sheet Music',
		),
		'DVD' => array(
			562014 => 'Japanese',
			562016 => 'Foreign',
			562018 => 'Music',
			562020 => 'Animation',
			562022 => 'Hobby & Fitness',
			562024 => 'Sports',
			562026 => 'Family',
			562028 => 'TV Documentary',
			564522 => 'Box Sets',
			896246 => 'Adult',
		),
		'Music' => array(
			569170 => 'J-Pop',
			569290 => 'Popular',
			569292 => 'Rock',
			569298 => 'Hard Rock',
			562050 => 'Blues & Country',
			569318 => 'Soul & R&B',
			569320 => 'Hip Hop',
			569322 => 'Dance',
			562052 => 'Jazz Fusion',
			562056 => 'World',
			562064 => 'New Age',
			562058 => 'Soundtracks',
			562060 => 'Animation',
			562062 => 'Children & Family',
			569174 => 'Ballad',
			569186 => 'Traditional',
			899296 => 'Sports',
		)
	);