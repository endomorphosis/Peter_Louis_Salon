/* $Id: amazon_DE.inc,v 1.2 2007/01/07 04:55:45 prometheus6 Exp $ */
<?php
  global $_amazon_search_browse_fields;
  global $_amazon_search_browse_nodes;
  /**
    * $_amazon_search_browse_fields holds the field to search for each SearchIndex.
    * Valid combinations can be found at
    * http://www.amazon.com/gp/aws/sdk/main.html?s=AWSEcommerceService&v=4-0&p=ApiReference/UsSearchIndexMatrixArticle
  */
  $_amazon_search_browse_fields =
  array(
    'Books' => array('Author'=>'Author','Keywords'=>'Keywords','Title'=>'Title'),
    'Music' => array('Artist' => 'Artist','Keywords'=>'Keywords', 'Title' => 'Title',),
    'DVD' => array('Actor' => 'Actor','Keywords'=>'Keywords', 'Director' => 'Director', 'Title'=>'Title',),
  );

  $_amazon_search_browse_nodes =
  array(
	'Books' => array(
	 4185461 =>'Antiquarische B�cher',
	 117 => 'Belletristik',
	 403434 => 'Business & Karriere',
	 120 => 'B�rse & Geld',
	 124 => 'Computer & Internet',
	 11063821 => 'Erotik',
	 288100 => 'Fachb�cher',
	 548400 => 'Film, Kultur & Comics',
	 280652 => 'Kinder- & Jugendb�cher',
	 122 => 'Kochen & Lifestyle',
	 287480 => 'Krimis & Thriller',
	 403432 => 'Lernen & Nachschlagen',
	 1199902 =>'Musiknoten',
	 121 => 'Naturwissenschaften & Technik',
	 143 => 'Politik, Biografien & Geschichte',
	 536302 => 'Ratgeber',
	 298002 => 'Reise & Sport',
	 188795 => 'Religion & Esoterik',
	 142 => 'Science Fiction, Fantasy & Horror',
	),
	'DVD' => array(
	 289093 =>'Action, Thriller & Horror',
	 290505 => 'Kinder & Familie',
	 290800 => 'Kom�die & Drama',
	 289312 => 'Musik-DVDs',
	 526774 => 'Originalfassungen',
	 548404 => 'Science Fiction & Fantasy',
	 549228 => 'Sprachfassungen',
	 1027630 => 'TV & Dokumentationen',
	),
	'Music' => array(
	 400118 => 'Alternative',
	 255953 => 'Comedy & Kabarett',
	 255883 => 'Dance & Electronic',
	 255952 => 'Diverses',
	 464364 => 'Gothic & Wave',
	 264898 => 'Hard \'n Heavy',
	 255917 => 'Jazz & Blues',
	 255956 => 'Kindermusik & H�rspiele',
	 255966 => 'Klassik',
	 264875 => 'Pop',
	 255895 => 'R&B & Soul',
	 255902 => 'Rap & HipHop',
	 464356 => 'Reggae & Ska',
	 264886 => 'Rock',
	 264910 => 'Schlager & Oldies',
	 573044 => 'Songwriter, Folk & Country' ,
	 264918 => 'Soundtracks & Musicals',
	 265502 => 'Weltmusik',
	)
);