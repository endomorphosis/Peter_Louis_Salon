/* $Id: README.txt,v 1.10 2007/01/25 00:14:06 prometheus6 Exp $ */
amazon.module uses Amazon's ECS4 REST interface. The code (in amazon.inc) will work with all Amazon.com stores. The data (in amazon_init.inc) has the basic codes local to each store. If you edit amazon_init.inc, make sure your editor understands UTF-8 encoding. The data arrays for the German and French Amazon shops have accented characters that may be lost.

New for 5.0:
The amazon_init.inc data file has been split into the following country specific files.
amazon_US.inc
amazon_UK.inc
amazon_CA.inc
amazon_FR.inc
amazon_DE.inc
amazon_JA.inc

Each file creates two global variables which are populated with values you can find at Amazon E-Commerce Service (ECS) Software Development Kit (SDK) <http://developer.amazonwebservices.com/connect/kbcategory.jspa?categoryID=19>

	$_amazon_search_browse_fields =
  array(
  //'SearchIndex' => array('AllowedParameter1' ='AllowedParameter1', 'AllowedParameter2' ='AllowedParameter2'),
    'Books' => array('Author'=>'Author','Keywords'=>'Keywords','Title'=>'Title'),
    'Music' => array('Artist' => 'Artist','Keywords'=>'Keywords', 'Title' => 'Title',),
    'DVD' => array('Actor' => 'Actor','Keywords'=>'Keywords', 'Director' => 'Director', 'Title'=>'Title',),
  );

$_amazon_search_browse_fields defines the tabs on the search page and the fields that can me searched. The tabs are created in the order they appear in this array. Legal combinations are available on country specififc 'Search Index / Parameter Combinations' pages in the ECS documentation.

  $_amazon_search_browse_nodes =
  array(
    'Books'=> array(
      0 =>'All',
      ...
      26 =>'Sports',
      28 =>'Teens',
      27 =>'Travel'

    ),
    'Music' => array(
       0 =>'All',
			30 => 'Alternative Rock',
			31 => 'Blues',
			...
			40 => 'Rock',
			42 => 'Soundtracks',
    ),
    'DVD' => array(
      0 =>'All',
      ...
	  	163296 => 'Action & Adventure',
			538708 => 'African American Cinema',
    )
  );
$_amazon_search_browse_nodes creates the searchable categories for each page.  Legal combinations are available on country specififc 'Browse Node Values' pages in the ECS documentation.


Please note:
1 - this module requires the CURL optional extension to connect to Amazon.com. Most wehosts provide it, but it's possible yours does not.
2 - When running under PHP4, this module requires the DOM XML optional extension. Again, it's possible your web host may not provide it, but most do.
3 - As of this writing (August 2005) ECS assumes all input is ISO-8850-1 encoded, even as it returns UTF-8. If you have the iconv module enabled, your search terms will be ISO-8850-1 encoded. This is important if you search in languages other than English.
The best way to find out if your host has these modules installed is to make a php file (say, phpinfo.php) containing this:

<?php
phpinfo();
?>

on your server and load it into your web browser (say, http://www.example.com/phpinfo.php).
-----------------------------------------------------------------------

amazon.module creates two new node types:
1 - The node type 'amazon' is for stories about a book sold by Amazon.com. I picture it being used for reviews. One can have as many 'amazon' nodes connected to a single product as you wish.
2 - The node type 'amazon-node' is pure product information. They can be created individually or automatically for each review or related product link entered. This lets your users build your database of products. The administrative interface has an Amazon.com search facility that lets you look up books by keyword or list of ASINs and import them into 'amazon-node' nodes in bulk. Only one 'amazon-node' is created per ASIN.

A field for related products can be added to all node types. A comma delimited list of ASINs will produce Amazon links for each ASIN at the bottom of the post.

Because the 'amazon-node' nodes are standard nodes they can have categories attached and taxinomy based pages are automatically available.

The module has a block that displays a random item linked to the Amazon detail page for the item. The 4.7 version also has a random review (does not include items entered as related links) and recommended book (includes only those items rated above a configurable value).
-----------------------------------------------------------------------
The search functionality has been improved