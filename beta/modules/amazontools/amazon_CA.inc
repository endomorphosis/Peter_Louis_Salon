<?php
/* $Id: amazon_CA.inc,v 1.2 2007/01/07 04:55:45 prometheus6 Exp $ */
  global $_amazon_search_browse_fields;
  global $_amazon_search_browse_nodes;
  /**
    * $_amazon_search_browse_fields holds the field to search for each SearchIndex.
    * Valid combinations can be found at
    * http://www.amazon.com/gp/aws/sdk/main.html?s=AWSEcommerceService&v=4-0&p=ApiReference/UsSearchIndexMatrixArticle
  */
  $_amazon_search_browse_fields =
  array(
    'Books' => array('Author'=>'Author','Keywords'=>'Keywords','Title'=>'Title'),
    'Music' => array('Artist' => 'Artist','Keywords'=>'Keywords', 'Title' => 'Title',),
    'DVD' => array('Actor' => 'Actor','Keywords'=>'Keywords', 'Director' => 'Director', 'Title'=>'Title',),
  );

  $_amazon_search_browse_nodes =
  array(
  	'Books' => array(
			933484 => 'Arts & Photography',
			934986 => 'Biographies & Memoirs',
			935522 => 'Business & Investing',
			4142731 =>'Calendars',
			935948 => 'Children\'s Books',
			939082 => 'Computers & Internet',
			940804 => 'Cooking, Food & Wine',
			941112 => 'Engineering',
			941378 => 'Entertainment',
			942318 => 'Gay & Lesbian',
			942402 => 'Health, Mind & Body',
			927728 => 'History',
			943356 => 'Home & Garden',
			943958 => 'Horror',
			927730 => 'Law',
			927790 => 'Literature & Fiction',
			948300 => 'Medicine',
			948808 => 'Mystery & Thrillers',
			927734 => 'Nonfiction',
			950152 => 'Outdoors & Nature',
			950640 => 'Parenting & Families',
			950756 => 'Professional & Technical',
			952366 => 'Reference',
			953420 => 'Religion & Spirituality',
			955190 => 'Romance',
			956280 => 'Science',
			957368 => 'Science Fiction & Fantasy',
			959466 => 'Sports',
			959978 => 'Teens',
			960696 => 'Travel',
		),
		'DVD' => array(
			966110 => 'Action & Adventure',
			966514 => 'Animation',
			953060 => 'Art House & International',
			966756 => 'Classics',
			953088 => 'Comedy',
			966996 => 'Cult Movies',
			967078 => 'Documentary',
			953102 => 'Drama',
			952796 => 'En fran�ais',
			967164 => 'Gay & Lesbian',
			967180 => 'Horror',
			953108 => 'Kids & Family',
			967404 => 'Military & War',
			967492 => 'Music Video & Concerts',
			953118 => 'Musicals & Performing Arts',
			967798 => 'Mystery & Suspense',
			967942 => 'Science Fiction & Fantasy',
			968126 => 'Special Interests',
			968392 => 'Sports',
			953128 => 'Television',
			968484 => 'Westerns',
		),
		'Music' => array(
			962456 => 'Alternative Rock',
			962460 => 'Blues',
			13774611 => 'Box Sets',
			963212 => 'Broadway & Vocalists',
			13779501 => 'Canadian Music',
			962462 => 'Children\'s Music',
			962828 => 'Christian & Gospel',
			962464 => 'Classical',
			962466 => 'Country',
			962470 => 'Dance & DJ',
			962500 => 'En fran�ais',
			962472 => 'Folk',
			962476 => 'International',
			962480 => 'Jazz',
			962484 => 'Miscellaneous',
			963050 => 'New Age',
			963052 => 'Opera & Vocal',
			962486 => 'Pop',
			963094 => 'R&B',
			962488 => 'Rap & Hip-Hop',
			962490 => 'Rock',
			962498 => 'Soundtracks',
			3570911 => 'Used Music',
		)
	);
