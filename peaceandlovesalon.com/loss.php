<table width="100%" cellpadding="3" cellspacing="0" border="0">
<tr>
<td width="79%">
<p style="vertical-align: top;">
<img alt="anagen" src="anagen_logo.jpg" border="0" height="50" width="135"></p>
<p>&nbsp;</p>
<p><img alt="anagen" src="anagen2.jpg" height="228" width="400"></p>
<h2>Hair Revitalizing System</h2>
<table class="ae_noborder" border="0" cellpadding="0" cellspacing="0" width="99%">
<tbody>
<tr>
<td align="left" valign="top">
<p>Anagen Therapy products are formulated to enhance the anagen phase by creating the healthiest environment in which hair can grow. All Anagen Therapy products are made from only the purest pharmaceutical grade, natural ingredients, with all<br>harmful chemicals having been eliminated. Ninety percent (90%) of all hair care products on the market today contain caustic chemicals like Sodium Laurel Sulphate (kerosene based), Propyleneglycol (base for antifreeze), which are proven to impair the ability of hair to grow. These chemicals are absorbed into the tissues of the scalp and blood stream and over a period of time can cause the sebum of the hair shaft to crystallize, clogging the follicular channel and provoking hair loss.</p></td></tr></tbody></table>
<p>&nbsp;</p>
<table class="ae_noborder" border="0" cellpadding="0" cellspacing="3" width="99%">
<tbody>
<tr>
<td align="left" valign="top">
<p>After nearly 5 years of research, development and clinical testing, HairLabs International introduces the Anagen Therapy Hair Revitalizing System; an advanced formulation of all natural hair care products, dietary supplement, chlorine shower filter and a federally approved topical solution, proven to stop the progression of hair loss and stimulate regrowth. The Anagen Therapy Revitalizing System is light years ahead of other hair restoration programs and its superior botanical formulations have been adopted and exclusively recommended as a combination therapy by the world?s leading manufacturer of soft laser light hair therapy devices. The Anagen Therapy Hair Revitalizing System is nature's path to fuller, thicker, healthier and more hair.</p>
</td>
</tr>
<tr>
<td width="40%" align="left" valign="top">
	<a href="template.php?f=AnagenTherapyProducts"><img border="0" src="http://www.peterlouissalon.com/buynow.jpg"></a>
</td>
</tr>
</tbody></table>
<p></p>
<p>&nbsp;</p>
</td>
<td valign="top" width="21%">
<h2 class="style1">Bio 1</h2>
<img alt="anagen" src="anagen.jpg" border="0" height="429" width="185"> 
<p></p>
<p><strong>The word Anagen is a scientific term, which describes the active growing phase of the hair growth cycle, during which a single hair follicle produces a hair that will continue to grow for a period of 2 to 6 years.</strong></p>
<p><strong>The Anagen Therapy Bio Line is clinically proven to enhance the results of soft laser light therapy</strong></p>
</td>
</tr>

</table>