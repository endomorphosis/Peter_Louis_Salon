<?php
// $Id: ec_store.inc,v 1.5.2.17 2009/07/04 20:27:21 recidive Exp $

/**
 * @file
 * Code here is not specific to any process, might be any user, and is not run infrequently.
 */

/**
 * Saves all addresses in transaction using ec_store_transaction_address_save().
 *
 * @param $txn
 *   Object, transaction with address array.
 */
function ec_store_transaction_addresses_save($txn) {
  // If we where not passed a valid transaction and we can't save.
  if (!isset($txn->txnid) || !is_array($txn->address)) {
    return FALSE;
  }
  foreach ($txn->address as $type => $address) {
    $address['txnid'] = $txn->txnid;
    ec_store_transaction_address_save($address, $type);
  }
}

/**
 * Saves an individual address to the passed transaction.
 *
 * @param $address
 *   Object, address object.
 * @param $type
 *   String, address type(corresponds to type field in database).
 */
function ec_store_transaction_address_save($address, $type) {
  $address['type'] = $type;

  drupal_write_record('ec_transaction_address', $address, array('txnid', 'type'));
  if (!db_affected_rows()) {
    drupal_write_record('ec_transaction_address', $address);
  }
}

/**
 * Creates a standard address form.
 * When passed a display field we filter out all other sections and display only
 * the one given. If passed shipping only a shipping address will be shown.
 *
 * @param $txn
 *   Objectm this is a transaction object used for default values.
 * @param $display
 *   String, option field that allows the form to filter sections.
 */
function ec_store_transaction_addresses_form($txn, $display = NULL) {
  $form['address']['#tree'] = TRUE;

  $form['address']['shipping'] = ec_store_address_form($txn->address['shipping']);
  $form['address']['shipping']['#type'] = 'fieldset';
  $form['address']['shipping']['#title'] = t('Shipping address');

  $form['address']['billing'] = ec_store_address_form($txn->address['billing']);
  $form['address']['billing']['#type'] = 'fieldset';
  $form['address']['billing']['#title'] = t('Billing address');

  switch ($display) {
    case 'shipping':
      unset($form['billing']);
      break;

    case 'billing':
      unset($form['shipping']);
      break;
  }
  return $form;
}

function ec_store_address_form($address = array()) {
  module_load_include('inc', 'ec_store', 'ec_store.localization');
  
  $address += array(
    'firstname' => '', 
    'lastname' => '',
    'street1' => '',
    'street2' => '',
    'city' => '',
    'state' => '',
    'zip' => '',
    'country' => variable_get('ec_country', ''),
  );

  if (isset($address['aid'])) {
    $form['type'] = array(
      '#type'  => 'hidden',
      '#value' => $address['type'],
    );
  }

  $form['firstname'] = array(
    '#type'          => 'textfield',
    '#title'         => t('First name'),
    '#default_value' => $address['firstname'],
    '#size'          => 32,
    '#maxlength'     => 32,
  );
  $form['lastname'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Last name'),
    '#default_value' => $address['lastname'],
    '#size'          => 32,
    '#maxlength'     => 32,
  );
  $form['country'] = array(
    '#type'          => 'select',
    '#title'         => t('Country'),
    '#default_value' => $address['country'],
    '#options'       => _ec_store_location_countries(),
  );
  $form['street1'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Street'),
    '#default_value' => $address['street1'],
    '#size'          => 60,
    '#maxlength'     => 64,
  );
  $form['street2'] = array(
    '#type'          => 'textfield',
    '#default_value' => $address['street2'],
    '#size'          => 60,
    '#maxlength'     => 64,
  );
  $form['city'] = array(
    '#type'          => 'textfield',
    '#title'         => t('City'),
    '#default_value' => $address['city'],
    '#size'          => 32,
    '#maxlength'     => 32,
  );
  $form['state'] = array(
    '#type'          => 'textfield',
    '#title'         => t('State/Province'),
    '#default_value' => $address['state'],
    '#size'          => 32,
    '#maxlength'     => 32,
  );
  $form['zip'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Zip/Postal Code'),
    '#default_value' => $address['zip'],
    '#size'          => 10,
    '#maxlength'     => 10,
  );
  
  $form_state = array('values' => $address);
  $data = &$form;
  $data['__drupal_alter_by_ref'] = array(&$form_state);
  drupal_alter('form_ec_store_address_form', $data);
  
  return $form;
}

function ec_store_address_form_validate(&$form, &$values) {
    if (empty($values['firstname'])) {
      form_set_error(implode('][', $form['firstname']['#parents']), t('Empty First Name.'));
    }
    if (empty($values['lastname'])) {
      form_set_error(implode('][', $form['lastname']['#parents']), t('Empty Last Name.'));
    }

   if (!variable_get('store_ignore_state', FALSE)) {
    module_load_include('inc', 'ec_store', 'ec_store.localization');
    if (($states = _ec_store_location_states($values['country'])) && empty($states[$values['state']])) {
      form_set_error(implode('][', $form['state']['#parents']), t('Invalid state. Please use the capitalized state code.'));
    }
    if (empty($values['street1'])) {
      form_set_error(implode('][', $form['street1']['#parents']), t('Empty address.'));
    }	
       if (empty($values['city'])) {
      form_set_error(implode('][', $form['city']['#parents']), t('Empty city.'));
    }

    if (empty($values['zip'])) {
      form_set_error(implode('][', $form['zip']['#parents']), t('Empty zip.'));
    }

   if (!is_numeric($values['zip'])){

      form_set_error(implode('][', $form['zip']['#parents']), t('Invalid zip.'));
    }

  }
}

/**
 * A wrapper for theme('store_invoice').
 *
 * Because the menu callback can only handle functions with no parameters, we
 * use this function as a wrapper.
 */
function ec_store_invoice($txn) {
  return theme('invoice', $txn);
}

/**
 * Allow a user to cancel an invoice.
 */
function ec_store_invoice_cancel(&$form_state, $txn) {
  $form = array();
    
  $form['invoice'] = array(
    '#value' => theme('invoice', $txn),
    '#prefix' => '<div>',
    '#suffix' => '</div>',
  );

  $form['txn'] = array('#type' => 'value', '#value' => $txn);
  
  return confirm_form($form,
    t('Do you really want to cancel this transaction?'),
    '',
    t('Cancelling this transaction will stop delivery of these items.'));
}

function ec_store_invoice_cancel_submit(&$form, &$form_state) {
  $workflow = db_result(db_query_range('SELECT workflow FROM {ec_workflow_statuses} WHERE type = %d ORDER BY weight ASC', EC_WORKFLOW_TYPE_CANCEL, 0, 1));
  $txn = (object)array('txnid' => $form_state['values']['txn']->txnid, 'workflow' => $workflow);
  
  ec_store_transaction_save($txn);
  
  drupal_set_message(t('Transaction %txnid has been cancelled', array('%txnid' => $txn->txnid)));
  
  $form_state['redirect'] = '';
}