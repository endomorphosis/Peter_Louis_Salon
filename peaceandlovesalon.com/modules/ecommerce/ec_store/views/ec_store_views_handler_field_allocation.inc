<?php
// $Id: ec_store_views_handler_field_allocation.inc,v 1.1.2.3 2009/07/02 10:25:39 recidive Exp $

/**
 * @file
 * Provide output of allocation status description.
 */

class ec_store_views_handler_field_allocation extends views_handler_field {
  function render($values) {
    return ec_store_transaction_get_allocation($values->{$this->field_alias});
  }
}
