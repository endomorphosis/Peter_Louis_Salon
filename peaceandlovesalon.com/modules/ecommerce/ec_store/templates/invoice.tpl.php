<?php
// $Id: invoice.tpl.php,v 1.1.2.4 2009/06/11 20:33:50 darrenoh Exp $
/**
 * @file
 *
 */
?>
<div id="invoice-<?php print $txnid; ?>" class="invoice<?php echo $shippable ? ' shippable' : ' not-shippable'; ?>">
  <div class="header">
    <h1>Invoice from <?php print variable_get('site_name', 'Drupal'); ?></h1>
    Invoice# <?php print $txnid; ?>
  </div>
  <div class="invoice-addresses clear-block">
<?php if ($shippable): ?>
    <div id="shipping-address" class="address"><?php print $shipping_address; ?></div>
<?php endif; ?>
    <div id="billing-address" class="address"><?php print $billing_address; ?><br/><? $mail = $txn->mail;
   print $mail; ?></div>

  <div id="return-address" class="address"><p><span><strong>Our Address</strong><br> Peter Louis Salon<br> 143 E 57th Street 2nd Floor<br> New York, NY 10022</span></p></div>
  </div>
  </div>
  <div class="invoice-details">
    <table>
      <tr>
        <th>Description</th>
        <th>Quantity</th>
        <th>Total</th>
      </tr>
<?php foreach ($items as $item) { ?>
      <tr>
        <td><?php print $item->title; ?><? if($item->ptype == 'amazon'){ echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. $item->ptype.'.com fullfilled';}?></td>
        <td class="item-quantity"><?php print $item->quantity_visible ? $item->qty : ''; ?></td>
        <td class="item-price"><?php print $item->formatted_price; ?></td>
      </tr>
<?php } ?>
<?php if (!empty($misc)) { ?>
      <tr>
        <td colspan="2" class="subtotal-title">Subtotal</td>
        <td class="subtotal-amount"><?php print $subtotal; ?></td>
      </tr>
<?php foreach ($misc as $item) { ?>
      <tr>
        <td colspan="2" class="subtotal-title"><?php echo $item->description; ?></td>
        <td class="subtotal-amount"><?php print $item->price; ?></td>
      </tr>
<?php } ?>
<?php } ?>
      <tr>
        <td colspan="2" class="total-title">Total</td>
        <td class="total-amount"><?php print $gross; ?></td>
      </tr>
    </table>
  </div>
  <?php print $additional ?>
</div>
<div>
<p><span>PeterLouis.com is brought as a service to our customers, where you may luxuriate in information and shopping.PeterLouis.com provide its services to subject to the following conditions. By visiting or shopping at PeterLouis.com, you accept these conditions. Please read them carefully. In addition, when you use any current or future service at PeterLouis.com or visit or purchase from any business affiliated with PeterLouis.com, whether or not included in the PeterLouis.com website, you also will be subject to the guidelines and conditions applicable to such service or business.</span></p> <p><span><strong>Product Information</strong><br> PeterLouis.com attempts to ensure the efficiency of products provided, and refers to the manufacturer for detailed information about the products . PeterLouis.com has tried to display the original appearance of the products in our website. However, colors may vary depending on your monitor and package in display may not be accurate and are for reference only.</span></p> <p><span>If a product is listed at an incorrect price or with incorrect information due to typographical error or error in pricing or product information, PeterLouis.com has the right to refuse or cancel any orders placed for product listed at the incorrect price. PeterLouis.com has the right to refuse or cancel any such orders whether or not the order has been confirmed and your credit card charged. If your credit card has already been charged for the purchase and your order is cancelled, PeterLouis.com issues a credit to your credit card account in the amount of the charge.</span></p> <p><span><strong>SHIPPING &amp; RETURNS</strong></span></p> <p><span><strong>Shipping:</strong><br> All orders are shipped, within the continental US, via insured UPS Ground, 2nd Day Air, Next Day Air. UPS will not deliver to a PO Box. Shipping for locations outside of the US continent (Canada, Hawaii, Alaska, PR and others) are not available at this moment.</span></p> <p><span>* Note that 2nd Day Air and Next Day Air orders received in the afternoon might be shipped out next day depending on the time it was received.</span></p> <p><span>Be sure to give an accurate shipping address, or your order may not be delivered.</span></p> <p><span><strong>Returns:</strong><br> All items we sell are personal hygiene products, such as supplements, hairbrushes, combs, shampoos, conditioners, hair treatments, hair styling products, etc, and they are not returnable.</span></p> <p><span>If you received a product that does not match to your order and/or receipt, PeterLouis.com will exchange it to the correct one within 15 days from the date it was shipped.</span></p> <p><span>Please return the product with the copy of the receipt enclosed. Opened products will not be accepted back.</span></p> <p><span><strong>Lost, Damaged or Stolen Shipments:</strong><br> If you have not received your order after 15 days, PeterLouis.com will attempt to trace the shipment. All orders are sent insured via UPS with tracking numbers.</span></p> <p><span><strong>CANCELLATION OF ORDERS</strong></span></p> <p><span>If you decide that you don't want your order after it has been shipped, refuse delivery of the parcel and have UPS send it back. As soon as the parcel is received back, you will be refunded the amount of your order, but an additional $10 charges will be applied for shipping, handling and restocking.</span></p> <p><span><strong></strong></span></p> <p><span><strong>RISK OF LOSS</strong><br> All items purchased from PeterLouis.com are made pursuant to a shipment contract. This means that the risk of loss and title for such items pass to you upon our delivery to the carrier.</span></p> <p><span><strong>Liability Limitations and Disclaimer of Warranties</strong><br> THIS SITE IS PROVIDED BY PETERLOUIS.COM ON AN "AS IS"AND "AS AVAILABLE" BASIS. PETERLOUIS.COM MAKES NO REPRESENTATIONS OR WARRANTIES OF ANY KIND, EXPRESS OR IMPLIED, AS TO THE OPERATION OF THE SITE, THE INFORMATION, CONTENT, MATERIALS OR PRODUCTS, INCLUDED ON THIS SITE. TO THE FULL EXTENT PERMISSIBLE BY APPLICABLE LAW, PETERLOUIS.COM DISCLAIMS ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. PETERLOUIS.COM WILL NOT BE LIABLE FOR ANY DAMAGES OF ANY KIND ARISING FROM THE USE OF THIS SITE, INCLUDING BUT NOT LIMITED TO DIRECT, INDIRECT, INCIDENTAL PUNITIVE AND CONSEQUENTIAL DAMAGES. YOU HEREBY ACKNOWLEDGE THAT THIS PARAGRAPH SHALL APPLY TO ALL CONTENT, MERCHANDISE, AND SERVICES AVAILABLE THROUGH THE SITE.</span></p> <p><span>THIS SITE MAKES NO WARRANTIES, EXPRESS OR IMPLIED, WITH RESPECT TO GIFT CERTIFICATES, INCLUDING WITHOUT LIMITATION, ANY EXPRESS OR IMPLIED WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. IN THE EVENT A GIFT CERTIFICATE CODE IS NON-FUNCTIONAL, YOUR SOLE REMEDY, AND OUR SOLE LIABILITY, SHALL BE THE REPLACEMENT OF SUCH GIFT CERTIFICATE. CERTAIN STATE LAWS DO NOT ALLOW LIMITATIONS ON IMPLIED WARRANTIES OR THE EXCLUSION OR LIMITATION OF CERTAIN DAMAGES. IF THESE LAWS APPLY TO YOU, SOME OR ALL OF THE ABOVE DISCLAIMERS, EXCLUSIONS, OR LIMITATIONS MAY NOT APPLY TO YOU, AND YOU MIGHT HAVE ADDITIONAL RIGHTS.</span></p> <p><span><strong>Applicable Law and Disputes</strong><br> By visiting PeterLouis.com, you agree that the laws of the state of New York, without regard to principles of conflict of laws, will govern these Terms of Use and any dispute of any sort that might arise between you and PeterLouis.com.</span></p> <p><span>Any dispute relating in any way to your visit to PeterLouis.com or to products you purchase through PeterLouis.com shall be submitted to confidential arbitration in New York City, New York, except that, to the extent you have in any manner violated or threatened to violate PeterLouis.com's property rights, PeterLouis.com may seek injunctive or other appropriate relief in any state or federal court in the state of New York, and you consent to exclusive jurisdiction and venue in such courts. Arbitration under this agreement shall be conducted under the rules then prevailing of the American Arbitration Association. The arbitrator's award shall be binding and may be entered as a judgment in any court of competent jurisdiction. To the fullest extent permitted by applicable law, no arbitration under this Agreement shall be joined to an arbitration involving any other party subject to this Agreement, whether through class arbitration proceedings or otherwise.</span></p>
</div>
