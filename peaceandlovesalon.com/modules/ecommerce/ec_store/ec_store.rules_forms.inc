<?php
// $Id: ec_store.rules_forms.inc,v 1.1.2.5 2009/04/03 12:27:01 gordon Exp $

/**
 * @file
 * Implement supporting forms.
 */

/**
 * Condition: Check the gross amount of the transaction
 */
function ec_store_condition_gross_form($settings, &$form) {
  $settings+= array('condition' => '=', 'amount' => '');
  
  $form['settings']['condition'] = array(
    '#type' => 'select',
    '#title' => t('Condition'),
    '#default_value' => $settings['condition'],
    '#options' => array(
      '=' => t('Equal'),
      '<' => t('Less than'),
      '<=' => t('Less than equal'),
      '>' => t('Greater than'),
      '>=' => t('Greater than equal'),
    ), 
  );
  
  $form['settings']['amount'] = array(
    '#type' => 'textfield',
    '#title' => t('Amount'),
    '#default_value' => $settings['amount'],
    '#required' => TRUE,
    '#element_validate' => array('valid_number'),
  );
}

/**
 * Condition: Check the transaction workflow status.
 */
function ec_store_condition_workflow_status_form($settings, &$form) {
  $form['settings']['workflow'] = array(
    '#type' => 'select',
    '#title' => t('Workflow status'),
    '#default_value' => $settings['workflow'],
    '#options' => ec_store_transaction_workflow(),
    '#multiple' => TRUE,
    '#description' => t('Select the workflow statuses which the transaction is set to.'),
  );
}

/**
 * Condition: Check the transaction allocation status.
 */
function ec_store_condition_allocation_status_form($settings, &$form) {
  $form['settings']['allocation'] = array(
    '#type' => 'select',
    '#title' => t('Allocation status'),
    '#default_value' => $settings['allocation'],
    '#options' => ec_store_transaction_allocation(),
    '#multiple' => TRUE,
    '#description' => t('Select the allocation statuses which the transaction is set to.'),
  );
}

/**
 * Action: Set transaction workflow.
 */
function ec_store_rules_action_set_workflow_form($settings, &$form) {
  $form['settings']['workflow'] = array(
    '#type' => 'radios',
    '#title' => t('New workflow status'),
    '#default_value' => $settings['workflow'],
    '#options' => ec_store_transaction_workflow(),
    '#description' => t('Select the workflow that will be set by this action.'),
  );
}

/**
 * Action: Set transaction workflow.
 */
function ec_store_rules_action_set_allocation_form($settings, &$form) {
  $form['settings']['allocation'] = array(
    '#type' => 'radios',
    '#title' => t('New allocation status'),
    '#default_value' => $settings['allocation'],
    '#options' => ec_store_transaction_allocation(),
    '#description' => t('Select the allocation that will be set by this action.'),
  );
}
