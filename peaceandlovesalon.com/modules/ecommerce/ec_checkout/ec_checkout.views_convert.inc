<?php
// $Id: ec_checkout.views_convert.inc,v 1.1.2.1 2009/03/20 14:16:13 darrenoh Exp $

/**
 * @file
 * Field conversion for fields handled by this module.
 */

/**
 * Implementation of hook_views_convert().
 */
function ec_checkout_views_convert($display, $type, &$view, $field, $id = NULL) {
  switch ($type) {
    case 'field':
      switch ($field['tablename']) {
        case 'ec_product':
          switch ($field['field']) {
            case 'addtocartlink':
              $view->set_item_option($display, 'field', $id, 'field', 'checkout_links');
              break;
          }
          break;
      }
      break;
  }
}
