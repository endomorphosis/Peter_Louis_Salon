<div class="item-count">
  <?php echo $item_count; ?>
</div>
<div class="cart-items">
  <div class="items">
    <?php echo implode("\n", $items); ?>
  </div>
  <div class="total-wrapper clear-block">
    <span class="total">
      <?php echo $total; ?>
    </span>
  </div>
  <div class="checkout">
    <?php echo $checkout; ?>
  </div>
</div>
