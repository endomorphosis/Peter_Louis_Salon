<?php
// $Id: ec_product.features.api.php,v 1.1.2.2 2009/07/06 00:30:15 gordon Exp $

/**
 * @file
 * API Documentation of hooks related to product features
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Provide added product attributes via a feature
 *
 * Return additional attributes for a product based upon features that have
 * been added to a product.
 *
 * For more information see hook_attributes().
 *
 * @param $node
 *   The node of the product that is to be checked.
 *
 * @return
 *   Return an array of attributes which are to be enabled or disabled.
 */
function hook_feature_attributes($node) {
  return array('no_discount' => TRUE);
}

/**
 * Check that a product can be purchased by the current user.
 *
 * For every product a test is done to make sure the current user or some
 * other setting such as product availability to able to purchase the product
 * in question.
 *
 * See hook_ec_checkout_validate_item() for more information.
 *
 * @param $node
 *   The product in question to check the if it can be purchased.
 * @param $type
 *   Specifies the context of how this check is to be preformed. The product
 *   may be able to be purchased by buy now, but not the cart.
 * @param $qty
 *   Specifies the quantity to be purchased.
 * @param $data
 *   Provides the additional data which is used by the system for the
 *   pruchase.
 * @param $return
 *   provides the result of the previous check if it is implemented by
 *   multiple features
 *
 * @return
 *   Return TRUE or FALSE depending on if your check can be reversed.
 */
function hook_feature_ec_checkout_validate_item($node, $type, $qty, $data, $severity, $return) {
  return user_access('purchase products');
}

/**
 * Load additional information into the transaction item.
 *
 * When loading transaction this hook will allow the feature to load any
 * additional information that will be required.
 *
 * @param $item
 *  The product item that is attached to the transaction.
 *
 * @return
 *  An array of new fields that need to be added to the item.
 */
function hook_feature_transaction_load($item) {
  return array('qty_x_2' => $item->qty*2);
}

/**
 * Insert additional data into the system for later loading.
 */
function hook_feature_transaction_insert($item) {
  insert_item();
}

/**
 * Update additional data into the system for later loading.
 */
function hook_feature_transaction_update($item) {
  update_item();
}
/**
 * Delete additional data into the system for later loading.
 */
function hook_feature_transaction_delete($item) {
  delete_item();
}

/**
 * @} End of "addtogroup hooks".
 */