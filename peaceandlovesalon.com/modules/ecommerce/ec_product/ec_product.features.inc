<?php
// $Id: ec_product.features.inc,v 1.1.2.2 2009/07/05 04:53:29 gordon Exp $

/**
 * @file
 * Implementation of all the different features which have been implemented by the product module
 */

/**
 * Implementation of hook_feature_attributes().
 */
function ec_product_feature_no_discount_attributes($node) {
  return array('no_discount' => TRUE);
}

/**
 * Implementation of hook_feature_attributes().
 */
function ec_product_feature_shippable_attributes($node) {
  return array('is_shippable' => TRUE);
}

/**
 * Implementation of hook_feature_attributes().
 */
function ec_product_feature_no_quantity_attributes($node) {
  return array('no_quantity' => TRUE);
}

/**
 * Implementation of hook_feature_ec_checkout_validate_item().
 */
function ec_product_feature_permission_ec_checkout_validate_item($node, $type, $qty, $data, $severity, $return) {
  return user_access('purchase products');
}