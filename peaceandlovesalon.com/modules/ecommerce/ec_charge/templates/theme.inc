<?php
// $Id: theme.inc,v 1.1.2.2 2009/07/02 10:25:42 recidive Exp $

/**
 * @file
 * Provide preprocess functions for all the theme templates.
 */

function template_preprocess_ec_charge_variable_description(&$variables) {
  $descriptions = array();
  foreach ($variables['descriptions'] as $key => $value) {
    $descriptions[] = theme('ec_charge_variable_description_row', $key, $value);
  }
  $variables['descriptions'] = $descriptions;
}