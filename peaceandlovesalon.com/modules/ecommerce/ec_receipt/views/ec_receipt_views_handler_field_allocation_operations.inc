<?php
// $Id: ec_receipt_views_handler_field_allocation_operations.inc,v 1.1.2.1 2009/07/02 10:25:39 recidive Exp $

/**
 * @file
 * Handles list of operation links for a receipt.
 */

class ec_receipt_views_handler_field_allocation_operations extends views_handler_field {

  function render($values) {
    if ($receipt = db_fetch_object(db_query('SELECT * FROM {ec_receipt_allocation} WHERE eaid = %d', $values->{$this->field_alias}))) {
      return theme('links', module_invoke_all('link', 'ec_receipt_allocation', $receipt));
    }
  }
}
