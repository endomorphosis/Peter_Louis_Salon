<?php
// $Id: ec_receipt_views_handler_field_allocation_type.inc,v 1.1.2.1 2009/07/02 10:25:39 recidive Exp $

/**
 * @file
 * Provide translation of allocation type to human readable format.
 */

class ec_receipt_views_handler_field_allocation_type extends views_handler_field {
  function render($values) {
    return ec_receipt_get_atypes('name', $values->{$this->field_alias});
  }
}

