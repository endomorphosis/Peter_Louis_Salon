<?php
// $Id: ec_receipt_views_handler_field_type.inc,v 1.1.2.1 2009/07/02 10:25:39 recidive Exp $

/**
 * @file
 * Implement handler for receipt types.
 */

class ec_receipt_views_handler_field_type extends views_handler_field {
  function render($values) {
    return ec_receipt_get_types('name', $values->{$this->field_alias});
  }
}

