<?php
// $Id: ec_receipt.rules.inc,v 1.1.2.2 2009/07/02 10:25:39 recidive Exp $

/**
 * @file
 * Implement rules and actions for ec_receipt.
 */

/**
 * Implementation of hook_rules_event_info().
 */
function ec_receipt_rules_event_info() {
  return array(
    'ec_receipt_saved' => array(
      'label' => t('Receipt has been saved.'),
      'module' => 'eC Receipts',
      'arguments' => array(
        'receipt' => array('type' => 'receipt', 'label' => t('Receipt')),
        'orig_receipt' => array('type' => 'receipt', 'label' => t('Original Receipt')),
      ),
    ),
  );
}

/**
 * Implementation of hook_rules_condition_info().
 */
function ec_receipt_rules_condition_info() {
  return array(
    'ec_receipt_condition_receipt_status' => array(
      'label' => t('Receipt status'),
      'arguments' => array(
        'receipt' => array('type' => 'receipt', 'label' => t('Receipt')),
      ),
      'module' => 'eC Receipts',
    ),
  );
}

function ec_receipt_condition_receipt_status() {
}