<?php
// $Id: ec_receipt.views_default.inc,v 1.5.2.10 2009/07/04 20:27:21 recidive Exp $

/**
 * @file
 * Default views for ec_receipt module.
 */

/**
 * Implementation of hook_views_default_views().
 */
function ec_receipt_views_default_views() {
  // View 'customer_receipt_list'
  $view = new view;
  $view->name = 'customer_receipt_list';
  $view->description = 'List of receipts by customer';
  $view->tag = 'ec_receipt';
  $view->view_php = '';
  $view->base_table = 'ec_receipt';
  $view->is_cacheable = FALSE;
  $view->api_version = 2;
  $view->disabled = FALSE;
  $handler = $view->new_display('default', 'Defaults', 'default');
  $handler->override_option('fields', array(
    'erid' => array(
      'label' => 'Receipt no.',
      'exclude' => 0,
      'id' => 'erid',
      'table' => 'ec_receipt',
      'field' => 'erid',
      'relationship' => 'none',
    ),
    'type' => array(
      'label' => 'Type',
      'exclude' => 0,
      'id' => 'type',
      'table' => 'ec_receipt',
      'field' => 'type',
      'relationship' => 'none',
    ),
    'amount' => array(
      'label' => 'Amount',
      'exclude' => 0,
      'id' => 'amount',
      'table' => 'ec_receipt',
      'field' => 'amount',
      'relationship' => 'none',
    ),
    'balance' => array(
      'label' => 'Balance',
      'exclude' => 0,
      'id' => 'balance',
      'table' => 'ec_receipt',
      'field' => 'balance',
      'relationship' => 'none',
    ),
    'created' => array(
      'label' => 'Created',
      'date_format' => 'custom',
      'custom_date_format' => 'j M y',
      'exclude' => 0,
      'id' => 'created',
      'table' => 'ec_receipt',
      'field' => 'created',
      'relationship' => 'none',
    ),
    'status' => array(
      'label' => 'Status',
      'exclude' => 0,
      'id' => 'status',
      'table' => 'ec_receipt',
      'field' => 'status',
      'relationship' => 'none',
    ),
    'operations' => array(
      'label' => 'Operations',
      'exclude' => 0,
      'id' => 'operations',
      'table' => 'ec_receipt',
      'field' => 'operations',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('arguments', array(
    'ecid' => array(
      'default_action' => 'default',
      'style_plugin' => 'default_summary',
      'style_options' => array(),
      'wildcard' => 'all',
      'wildcard_substitution' => 'All',
      'title' => '%1',
      'default_argument_type' => 'user',
      'default_argument' => '',
      'validate_type' => 'none',
      'validate_fail' => 'not found',
      'break_phrase' => 1,
      'not' => 0,
      'id' => 'ecid',
      'table' => 'ec_customer',
      'field' => 'ecid',
      'relationship' => 'none',
      'default_options_div_prefix' => '',
      'default_argument_user' => 0,
      'default_argument_fixed' => '',
      'default_argument_php' => '',
      'validate_argument_node_type' => array(
        'deadwood_category' => 0,
        'deadwood_item' => 0,
        'amazon_book' => 0,
        'file' => 0,
        'page' => 0,
        'product' => 0,
        'product_some' => 0,
        'story' => 0,
      ),
      'validate_argument_node_access' => 0,
      'validate_argument_nid_type' => 'nid',
      'validate_argument_vocabulary' => array(),
      'validate_argument_type' => 'tid',
      'validate_argument_php' => '',
    ),
  ));
  $handler->override_option('access', array(
    'type' => 'ec_customer',
  ));
  $handler->override_option('use_ajax', TRUE);
  $handler->override_option('items_per_page', 50);
  $handler->override_option('use_pager', '1');
  $handler->override_option('style_plugin', 'table');
  $handler->override_option('style_options', array(
    'grouping' => '',
    'override' => 1,
    'sticky' => 1,
    'order' => 'desc',
    'columns' => array(
      'erid' => 'erid',
      'type' => 'type',
      'amount' => 'amount',
      'balance' => 'balance',
      'created' => 'created',
      'status' => 'status',
    ),
    'info' => array(
      'erid' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'type' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'amount' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'balance' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'created' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'status' => array(
        'sortable' => 1,
        'separator' => '',
      ),
    ),
    'default' => 'erid',
  ));
  $handler = $view->new_display('page', 'Page', 'page_1');
  $handler->override_option('path', 'user/%/store/receipts');
  $handler->override_option('menu', array(
    'type' => 'tab',
    'title' => 'Receipts',
    'description' => '',
    'weight' => '0',
    'name' => 'navigation',
  ));
  $handler->override_option('tab_options', array(
    'type' => 'none',
    'title' => '',
    'description' => '',
    'weight' => 0,
  ));
  $views[$view->name] = $view;

  // View 'ec_receipt_allocation_list'.
  $view = new view;
  $view->name = 'ec_receipt_allocation_list';
  $view->description = 'List of Allocations made for a receipt';
  $view->tag = 'ec_receipt';
  $view->view_php = '';
  $view->base_table = 'ec_receipt_allocation';
  $view->is_cacheable = FALSE;
  $view->api_version = 2;
  $view->disabled = FALSE;
  $handler = $view->new_display('default', 'Defaults', 'default');
  $handler->override_option('fields', array(
    'type' => array(
      'id' => 'type',
      'table' => 'ec_receipt_allocation',
      'field' => 'type',
    ),
    'eaid' => array(
      'label' => 'Id',
      'exclude' => 0,
      'id' => 'eaid',
      'table' => 'ec_receipt_allocation',
      'field' => 'eaid',
      'relationship' => 'none',
    ),
    'created' => array(
      'label' => 'Created',
      'date_format' => 'custom',
      'custom_date_format' => 'j M y',
      'exclude' => 0,
      'id' => 'created',
      'table' => 'ec_receipt_allocation',
      'field' => 'created',
      'relationship' => 'none',
    ),
    'amount' => array(
      'label' => 'Amount',
      'exclude' => 0,
      'id' => 'amount',
      'table' => 'ec_receipt_allocation',
      'field' => 'amount',
      'relationship' => 'none',
    ),
    'reversed' => array(
      'label' => 'Reversed',
      'type' => 'yes-no',
      'not' => 0,
      'exclude' => 0,
      'id' => 'reversed',
      'table' => 'ec_receipt_allocation',
      'field' => 'reversed',
      'relationship' => 'none',
    ),
    'operations' => array(
      'label' => 'Operations',
      'exclude' => 0,
      'id' => 'operations',
      'table' => 'ec_receipt_allocation',
      'field' => 'operations',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('arguments', array(
    'erid' => array(
      'default_action' => 'default',
      'style_plugin' => 'default_summary',
      'style_options' => array(),
      'wildcard' => 'all',
      'wildcard_substitution' => 'All',
      'title' => '',
      'default_argument_type' => 'receipt_id',
      'default_argument' => '',
      'validate_type' => 'none',
      'validate_fail' => 'empty',
      'break_phrase' => 0,
      'not' => 0,
      'id' => 'erid',
      'table' => 'ec_receipt_allocation',
      'field' => 'erid',
      'relationship' => 'none',
      'default_options_div_prefix' => '',
      'default_argument_user' => 0,
      'default_argument_fixed' => '',
      'default_argument_php' => '',
      'validate_argument_node_type' => array(
        'deadwood_category' => 0,
        'deadwood_item' => 0,
        'amazon_book' => 0,
        'file' => 0,
        'page' => 0,
        'product' => 0,
        'product_some' => 0,
        'story' => 0,
      ),
      'validate_argument_node_access' => 0,
      'validate_argument_nid_type' => 'nid',
      'validate_argument_vocabulary' => array(),
      'validate_argument_type' => 'tid',
      'validate_argument_php' => '',
    ),
  ));
  $handler->override_option('access', array(
    'type' => 'none',
  ));
  $handler->override_option('empty', 'No allocations');
  $handler->override_option('empty_format', '1');
  $handler->override_option('use_ajax', TRUE);
  $handler->override_option('use_pager', '1');
  $handler->override_option('style_plugin', 'table');
  $handler->override_option('style_options', array(
    'grouping' => '',
    'override' => 1,
    'sticky' => 1,
    'order' => 'asc',
    'columns' => array(
      'type' => 'type',
      'eaid' => 'eaid',
      'created' => 'created',
      'amount' => 'amount',
      'reversed' => 'reversed',
    ),
    'info' => array(
      'type' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'eaid' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'created' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'amount' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'reversed' => array(
        'sortable' => 1,
        'separator' => '',
      ),
    ),
    'default' => 'created',
  ));
  $views[$view->name] = $view;

  // View 'ec_receipt_list'.
  $view = new view;
  $view->name = 'ec_receipt_list';
  $view->description = t('List of all receipts.');
  $view->tag = 'ec_receipt';
  $view->view_php = '';
  $view->base_table = 'ec_receipt';
  $view->is_cacheable = FALSE;
  $view->api_version = 2;
  $view->disabled = FALSE;
  $handler = $view->new_display('default', t('Defaults'), 'default');
  $handler->override_option('fields', array(
    'erid' => array(
      'label' => t('Id'),
      'exclude' => 0,
      'id' => 'erid',
      'table' => 'ec_receipt',
      'field' => 'erid',
      'relationship' => 'none',
    ),
    'customer_name' => array(
      'label' => t('Customer name'),
      'exclude' => 0,
      'id' => 'customer_name',
      'table' => 'ec_customer',
      'field' => 'customer_name',
      'relationship' => 'none',
    ),
    'type' => array(
      'label' => t('Type'),
      'exclude' => 0,
      'id' => 'type',
      'table' => 'ec_receipt',
      'field' => 'type',
      'relationship' => 'none',
    ),
    'currency' => array(
      'label' => t('Currency'),
      'exclude' => 0,
      'id' => 'currency',
      'table' => 'ec_receipt',
      'field' => 'currency',
      'relationship' => 'none',
    ),
    'amount' => array(
      'label' => t('Amount'),
      'exclude' => 0,
      'id' => 'amount',
      'table' => 'ec_receipt',
      'field' => 'amount',
      'relationship' => 'none',
    ),
    'balance' => array(
      'label' => t('Balance'),
      'exclude' => 0,
      'id' => 'balance',
      'table' => 'ec_receipt',
      'field' => 'balance',
      'relationship' => 'none',
    ),
    'created' => array(
      'label' => t('Created'),
      'date_format' => 'custom',
      'custom_date_format' => 'j M y',
      'exclude' => 0,
      'id' => 'created',
      'table' => 'ec_receipt',
      'field' => 'created',
      'relationship' => 'none',
    ),
    'changed' => array(
      'label' => t('Changed'),
      'date_format' => 'custom',
      'custom_date_format' => 'j M y',
      'exclude' => 0,
      'id' => 'changed',
      'table' => 'ec_receipt',
      'field' => 'changed',
      'relationship' => 'none',
    ),
    'status' => array(
      'label' => t('Status'),
      'exclude' => 0,
      'id' => 'status',
      'table' => 'ec_receipt',
      'field' => 'status',
      'relationship' => 'none',
    ),
    'operations' => array(
      'id' => 'operations',
      'table' => 'ec_receipt',
      'field' => 'operations',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('filters', array(
    'created' => array(
      'operator' => '=',
      'value' => array(
        'type' => 'date',
        'value' => '',
        'min' => '',
        'max' => '',
      ),
      'group' => '0',
      'exposed' => TRUE,
      'expose' => array(
        'use_operator' => 1,
        'operator' => 'created_op',
        'identifier' => 'created',
        'label' => t('Created'),
        'optional' => 1,
        'remember' => 0,
      ),
      'id' => 'created',
      'table' => 'ec_receipt',
      'field' => 'created',
      'override' => array(
        'button' => t('Override'),
      ),
      'relationship' => 'none',
    ),
    'ec_exposed_fieldset' => array(
      'operator' => '=',
      'value' => '',
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'ec_exposed_fieldset',
      'table' => 'ec_receipt',
      'field' => 'ec_exposed_fieldset',
      'fields' => array(
        'created' => 'created',
        'erid' => 'erid',
        'type' => 'type',
        'amount' => 'amount',
        'allocated' => 'allocated',
        'balance' => 'balance',
        'changed' => 'changed',
        'type_1' => 'type_1',
        'amount_1' => 'amount_1',
        'created_1' => 'created_1',
        'etid' => 'etid',
        'reversed' => 'reversed',
        'type_2' => 'type_2',
        'keys' => 0,
      ),
      'relationship' => 'none',
      'fieldset_title' => t('Advanced Search'),
      'fieldset_collapsible' => 1,
      'fieldset_collapsed' => 1,
      'override' => array(
        'button' => t('Override'),
      ),
    ),
    'keys' => array(
      'operator' => 'optional',
      'value' => '',
      'group' => '0',
      'exposed' => TRUE,
      'expose' => array(
        'use_operator' => 0,
        'operator' => 'keys_op',
        'identifier' => 'keys',
        'label' => t('Search'),
        'optional' => 1,
        'remember' => 0,
      ),
      'id' => 'keys',
      'table' => 'search_index',
      'field' => 'keys',
      'relationship' => 'none',
    ),
    'erid' => array(
      'operator' => '=',
      'value' => array(
        'value' => '',
        'min' => '',
        'max' => '',
      ),
      'group' => '0',
      'exposed' => TRUE,
      'expose' => array(
        'use_operator' => 0,
        'operator' => 'erid_op',
        'identifier' => 'erid',
        'label' => t('Receipt no.'),
        'optional' => 1,
        'remember' => 0,
      ),
      'id' => 'erid',
      'table' => 'ec_receipt',
      'field' => 'erid',
      'relationship' => 'none',
    ),
    'type' => array(
      'operator' => 'in',
      'value' => array(),
      'group' => '0',
      'exposed' => TRUE,
      'expose' => array(
        'use_operator' => 0,
        'operator' => 'type_op',
        'identifier' => 'type',
        'label' => t('Type'),
        'optional' => 1,
        'single' => 0,
        'remember' => 0,
        'reduce' => 0,
      ),
      'id' => 'type',
      'table' => 'ec_receipt',
      'field' => 'type',
      'relationship' => 'none',
    ),
    'amount' => array(
      'operator' => '=',
      'value' => array(
        'value' => '',
        'min' => '',
        'max' => '',
      ),
      'group' => '0',
      'exposed' => TRUE,
      'expose' => array(
        'use_operator' => 1,
        'operator' => 'amount_op',
        'identifier' => 'amount',
        'label' => t('Amount'),
        'optional' => 1,
        'remember' => 0,
      ),
      'id' => 'amount',
      'table' => 'ec_receipt',
      'field' => 'amount',
      'override' => array(
        'button' => t('Override'),
      ),
      'relationship' => 'none',
    ),
    'allocated' => array(
      'operator' => '=',
      'value' => array(
        'value' => '',
        'min' => '',
        'max' => '',
      ),
      'group' => '0',
      'exposed' => TRUE,
      'expose' => array(
        'use_operator' => 1,
        'operator' => 'allocated_op',
        'identifier' => 'allocated',
        'label' => 'Allocated',
        'optional' => 1,
        'remember' => 0,
      ),
      'id' => 'allocated',
      'table' => 'ec_receipt',
      'field' => 'allocated',
      'override' => array(
        'button' => t('Override'),
      ),
      'relationship' => 'none',
    ),
    'balance' => array(
      'operator' => '=',
      'value' => array(
        'value' => '',
        'min' => '',
        'max' => '',
      ),
      'group' => '0',
      'exposed' => TRUE,
      'expose' => array(
        'use_operator' => 1,
        'operator' => 'balance_op',
        'identifier' => 'balance',
        'label' => t('Balance'),
        'optional' => 1,
        'remember' => 0,
      ),
      'id' => 'balance',
      'table' => 'ec_receipt',
      'field' => 'balance',
      'override' => array(
        'button' => t('Override'),
      ),
      'relationship' => 'none',
    ),
    'changed' => array(
      'operator' => '=',
      'value' => array(
        'type' => 'date',
        'value' => '',
        'min' => '',
        'max' => '',
      ),
      'group' => '0',
      'exposed' => TRUE,
      'expose' => array(
        'use_operator' => 1,
        'operator' => 'changed_op',
        'identifier' => 'changed',
        'label' => t('Changed'),
        'optional' => 1,
        'remember' => 0,
      ),
      'id' => 'changed',
      'table' => 'ec_receipt',
      'field' => 'changed',
      'override' => array(
        'button' => t('Override'),
      ),
      'relationship' => 'none',
    ),
    'type_1' => array(
      'operator' => 'in',
      'value' => array(),
      'group' => '0',
      'exposed' => TRUE,
      'expose' => array(
        'use_operator' => 0,
        'operator' => 'type_1_op',
        'identifier' => 'allocation',
        'label' => t('Allocation type'),
        'optional' => 1,
        'single' => 0,
        'remember' => 0,
        'reduce' => 0,
      ),
      'id' => 'type_1',
      'table' => 'ec_receipt_allocation',
      'field' => 'type',
      'override' => array(
        'button' => t('Override'),
      ),
      'relationship' => 'none',
    ),
    'amount_1' => array(
      'operator' => '=',
      'value' => array(
        'value' => '',
        'min' => '',
        'max' => '',
      ),
      'group' => '0',
      'exposed' => TRUE,
      'expose' => array(
        'use_operator' => 1,
        'operator' => 'amount_1_op',
        'identifier' => 'amount_1',
        'label' => t('Allocated amount'),
        'optional' => 1,
        'remember' => 0,
      ),
      'id' => 'amount_1',
      'table' => 'ec_receipt_allocation',
      'field' => 'amount',
      'override' => array(
        'button' => t('Override'),
      ),
      'relationship' => 'none',
    ),
    'created_1' => array(
      'operator' => '=',
      'value' => array(
        'type' => 'date',
        'value' => '',
        'min' => '',
        'max' => '',
      ),
      'group' => '0',
      'exposed' => TRUE,
      'expose' => array(
        'use_operator' => 1,
        'operator' => 'created_1_op',
        'identifier' => 'created_1',
        'label' => t('Allocation date'),
        'optional' => 1,
        'remember' => 0,
      ),
      'id' => 'created_1',
      'table' => 'ec_receipt_allocation',
      'field' => 'created',
      'override' => array(
        'button' => t('Override'),
      ),
      'relationship' => 'none',
    ),
    'etid' => array(
      'operator' => '=',
      'value' => '',
      'group' => '0',
      'exposed' => TRUE,
      'expose' => array(
        'use_operator' => 0,
        'operator' => 'etid_op',
        'identifier' => 'etid',
        'label' => t('External allocation id'),
        'optional' => 1,
        'remember' => 0,
      ),
      'case' => 1,
      'id' => 'etid',
      'table' => 'ec_receipt_allocation',
      'field' => 'etid',
      'override' => array(
        'button' => t('Override'),
      ),
      'relationship' => 'none',
    ),
    'reversed' => array(
      'operator' => '=',
      'value' => '0',
      'group' => '0',
      'exposed' => TRUE,
      'expose' => array(
        'operator' => '',
        'identifier' => 'reversed',
        'label' => t('Allocation Reversed'),
        'optional' => 1,
        'remember' => 0,
      ),
      'id' => 'reversed',
      'table' => 'ec_receipt_allocation',
      'field' => 'reversed',
      'override' => array(
        'button' => t('Override'),
      ),
      'relationship' => 'none',
    ),
    'type_2' => array(
      'operator' => 'in',
      'value' => array(),
      'group' => '0',
      'exposed' => TRUE,
      'expose' => array(
        'use_operator' => 0,
        'operator' => 'type_2_op',
        'identifier' => 'type_2',
        'label' => t('Allocation type'),
        'optional' => 1,
        'single' => 1,
        'remember' => 0,
        'reduce' => 0,
      ),
      'id' => 'type_2',
      'table' => 'ec_receipt_allocation',
      'field' => 'type',
      'override' => array(
        'button' => t('Override'),
      ),
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('access', array(
    'type' => 'perm',
    'perm' => 'store admin report',
  ));
  $handler->override_option('title', t('Receipts'));
  $handler->override_option('items_per_page', 50);
  $handler->override_option('use_pager', '1');
  $handler->override_option('style_plugin', 'bulk');
  $handler->override_option('style_options', array(
    'grouping' => '',
    'override' => 1,
    'sticky' => 1,
    'order' => 'desc',
    'columns' => array(
      'erid' => 'erid',
      'customer_name' => 'customer_name',
      'type' => 'type',
      'currency' => 'currency',
      'amount' => 'amount',
      'balance' => 'balance',
      'changed' => 'changed',
      'created' => 'created',
      'status' => 'status',
      'operations' => 'operations',
    ),
    'info' => array(
      'erid' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'customer_name' => array(
        'separator' => '',
      ),
      'type' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'currency' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'amount' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'balance' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'changed' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'created' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'status' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'operations' => array(
        'separator' => '',
      ),
    ),
    'default' => 'erid',
    'execution_type' => '1',
    'display_type' => '0',
    'skip_confirmation' => 0,
    'display_result' => 1,
    'merge_single_action' => 1,
    'selected_operations' => array(
      'ec_receipt_action_allocate',
      'ec_receipt_action_delete',
      'ec_receipt_action_refund',
    ),
  ));
  $handler = $view->new_display('page', t('Page'), 'page_1');
  $handler->override_option('path', 'admin/store/receipts');
  $handler->override_option('menu', array(
    'type' => 'normal',
    'title' => t('Receipts'),
    'description' => t('List of receipts that have been paid into the system.'),
    'weight' => '0',
    'name' => 'navigation',
  ));
  $handler->override_option('tab_options', array(
    'type' => 'none',
    'title' => '',
    'description' => '',
    'weight' => 0,
  ));
  $views[$view->name] = $view;

  // View 'receipt_allocation'.
  $view = new view;
  $view->name = 'receipt_allocation';
  $view->description = 'Allocation of Receipts';
  $view->tag = 'ec_receipt';
  $view->view_php = '';
  $view->base_table = 'ec_receipt_allocation';
  $view->is_cacheable = FALSE;
  $view->api_version = 2;
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
  $handler = $view->new_display('default', 'Defaults', 'default');
  $handler->override_option('fields', array(
    'erid' => array(
      'label' => 'Receipt no',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'exclude' => 1,
      'id' => 'erid',
      'table' => 'ec_receipt',
      'field' => 'erid',
      'relationship' => 'none',
      'override' => array(
        'button' => 'Override',
      ),
    ),
    'eaid' => array(
      'label' => 'Id',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'exclude' => 0,
      'id' => 'eaid',
      'table' => 'ec_receipt_allocation',
      'field' => 'eaid',
      'relationship' => 'none',
    ),
    'created' => array(
      'label' => 'Created',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'date_format' => 'custom',
      'custom_date_format' => 'd M y',
      'exclude' => 0,
      'id' => 'created',
      'table' => 'ec_receipt_allocation',
      'field' => 'created',
      'relationship' => 'none',
    ),
    'type' => array(
      'label' => 'Type',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'exclude' => 0,
      'id' => 'type',
      'table' => 'ec_receipt_allocation',
      'field' => 'type',
      'relationship' => 'none',
    ),
    'etid' => array(
      'label' => 'Txnid',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'exclude' => 0,
      'id' => 'etid',
      'table' => 'ec_receipt_allocation',
      'field' => 'etid',
      'relationship' => 'none',
    ),
    'status' => array(
      'label' => 'Status',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'exclude' => 0,
      'id' => 'status',
      'table' => 'ec_receipt',
      'field' => 'status',
      'relationship' => 'none',
    ),
    'amount' => array(
      'label' => 'Amount',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'exclude' => 0,
      'id' => 'amount',
      'table' => 'ec_receipt_allocation',
      'field' => 'amount',
      'relationship' => 'none',
    ),
    'balance' => array(
      'label' => 'Balance',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'exclude' => 0,
      'id' => 'balance',
      'table' => 'ec_receipt',
      'field' => 'balance',
      'relationship' => 'none',
    ),
    'operations' => array(
      'label' => 'Operations',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'exclude' => 0,
      'id' => 'operations',
      'table' => 'ec_receipt_allocation',
      'field' => 'operations',
      'relationship' => 'none',
    ),
    'operations_1' => array(
      'label' => 'Operations',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'exclude' => 0,
      'id' => 'operations_1',
      'table' => 'ec_receipt',
      'field' => 'operations',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('access', array(
    'type' => 'perm',
    'perm' => 'store admin report',
  ));
  $handler->override_option('title', 'Receipt Allocations');
  $handler->override_option('use_ajax', TRUE);
  $handler->override_option('style_plugin', 'table');
  $handler->override_option('style_options', array(
    'grouping' => 'erid',
    'override' => 1,
    'sticky' => 1,
    'order' => 'desc',
    'columns' => array(
      'erid' => 'erid',
      'eaid' => 'eaid',
      'created' => 'created',
      'type' => 'type',
      'etid' => 'etid',
      'status' => 'status',
      'amount' => 'amount',
      'balance' => 'balance',
      'operations' => 'operations',
      'operations_1' => 'operations',
    ),
    'info' => array(
      'erid' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'eaid' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'created' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'type' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'etid' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'status' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'amount' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'balance' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'operations' => array(
        'separator' => ' ',
      ),
      'operations_1' => array(
        'separator' => '',
      ),
    ),
    'default' => 'eaid',
  ));
  $handler = $view->new_display('page', 'Page', 'page_1');
  $handler->override_option('path', 'admin/store/receipts_alloc');
  $handler->override_option('menu', array(
    'type' => 'normal',
    'title' => 'Receipt Allocation',
    'description' => 'Shows how all the receipts have been allocated.',
    'weight' => '0',
    'name' => 'navigation',
  ));
  $handler->override_option('tab_options', array(
    'type' => 'none',
    'title' => '',
    'description' => '',
    'weight' => 0,
  ));
  $views[$view->name] = $view;

  return $views;
}
