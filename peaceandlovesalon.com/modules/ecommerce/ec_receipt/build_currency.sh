#!/bin/sh
# $Id: build_currency.sh,v 1.2.2.2 2009/07/04 20:27:21 recidive Exp $

export LANG=utf8

[ -e currency.inc ] && rm currency.inc && touch currency.inc

echo '<?php
// $Id: build_currency.sh,v 1.2.2.2 2009/07/04 20:27:21 recidive Exp $

/**
 * @file
 * List of currencies available.
 */

/**
 * Return a list of all currencies.
 */
function ec_receipt_get_all_currencies() {
  return array(' >> currency.inc

cut -f1,4 currency_list.txt| tail +2 | while read a b
do 
  printf "    '%s' => \"%s - %s\",\n" "$a" "$a" "$b" >> currency.inc
done

echo '  );
}' >> currency.inc
