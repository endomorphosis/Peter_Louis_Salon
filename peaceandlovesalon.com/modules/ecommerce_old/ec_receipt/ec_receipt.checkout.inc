<?php
// $Id: ec_receipt.checkout.inc,v 1.1.2.29 2009/10/25 02:32:55 davea Exp $

/**
 * @file
 * Provides checkout implementation for ec_receipt.
 */

/**
 * Implementation of hook_checkout_init().
 */
function ec_receipt_checkout_init(&$txn) {
  $txn->currency = variable_get('ec_default_currency', NULL);
  
  // since we are running from ec_checkout_create_transaction() we do not need to do any receipting
  if (isset($txn->no_interface) && $txn->no_interface) {
    return;
  }

  if (empty($txn->receipts['rtypes'])) {
    $default_filter = array(
      'currencies_supported' => $txn->currency,
      'allow_payments' => TRUE,
    );
    
    if (isset($txn->receipts['payment_filter'])) {
      $filter = $txn->receipts['payment_filter'];
      $filter+= $default_filter;
    }
    else {
      $filter = $default_filter;
    }
    $txn->receipts['rtypes'] = ec_receipt_type_filter($filter);
  }

  if (isset($_REQUEST['payment_method']) && isset($txn->receipts['rtypes'][$_REQUEST['payment_method']])) {
    $txn->payment_method = $_REQUEST['payment_method'];
  }
  else {
    $types = array_keys($txn->receipts['rtypes']);
    $txn->payment_method = $types[0];
  }
  
  if ($function = ec_receipt_get_invoke_function($txn->payment_method, 'init')) {
    $function('transaction', $txn);
  }
}

/**
 * Implementation of hook_checkout_form().
 */
function ec_receipt_checkout_form(&$form, &$form_state) {
  $txn =& $form_state['txn'];

  $form['receipt_no'] = array(
    '#type' => 'value',
    '#value' => '',
  );

  if (ec_store_transaction_calc_gross($txn) || ($txn->type == "ec_donate" && count($txn->items)==0)) {
    if (count($txn->receipts['rtypes']) > 1) {
      $options = array();

      foreach ($txn->receipts['rtypes'] as $type => $info) {
        $options[$type] = $info->name;
      }

      $form['ec_receipt']['buttons'] = array(
        '#prefix' => '<div id="payment-buttons" class="ec-inline-form">',
        '#suffix' => '</div>',
        '#weight' => -9,
      );
      $form['ec_receipt']['buttons']['select_payment_type'] = array(
        '#type' => 'select',
        '#title' => t('Select payment method'),
        '#default_value' => $txn->payment_method,
        '#options' => $options,
        '#access' => count($options) >= 1,
      );
      $form['ec_receipt']['buttons']['receipt_submit'] = array(
        '#type' => 'submit',
        '#value' => t('Select payment method'),
        '#access' => count($options) >= 1,
        '#submit' => array('ec_receipt_checkout_alter_rtype'),
        '#ahah' => array(
          'path' => 'checkout/ajax/rtype',
          'wrapper' => 'ec-receipt-form',
          'method' => 'replace',
        ),
      );
    }

    $form['ec_receipt']['ec_receipt_name'] = array(
      '#value' => ec_receipt_checkout_types(ec_receipt_get_types('type', $txn->payment_method)),
      '#weight' => -8,
      '#prefix' => '<div class="rtype-'. str_replace(array('][', '_', ' '), '-', $txn->payment_method) .'">',
      '#suffix' => '</div>',
    );
    if ($pform = ec_receipt_invoke($txn->payment_method, 'payment_form', 'transaction', $txn)) {
      $form['ec_receipt']['payment_form'] = array(
        '#tree' => TRUE,
      );
      $form['ec_receipt']['payment_form']+= $pform;
    }
    $form['ec_receipt']['#theme'] = 'ec_receipt_review_form';
  }
}

/**
 * Implementation of hook_checkout_post_update().
 */
function ec_receipt_checkout_post_update(&$form, &$form_state) {
  $txn =& $form_state['txn'];
  if (isset($form_state['clicked_button']['#create_txn']) && $form_state['clicked_button']['#create_txn'] && !form_get_errors() && ec_store_transaction_calc_gross($txn)) {
    $txn->payment_data = isset($form_state['values']['payment_form']) ? $form_state['values']['payment_form'] : array();

    $txn->receipt_no = ec_receipt_payment_process('transaction', $txn);
  }
}

/**
 * Handle changing of receipt type.
 */
function ec_receipt_checkout_alter_rtype(&$form, &$form_state) {
  $txn =& $form_state['txn'];
  $txn->payment_method = $form_state['values']['select_payment_type'];
}

/**
 * AHAH callback to change the payment method.
 */
function ec_receipt_checkout_submit_rtype_js() {
  ctools_include('object-cache');
  $cached_form_state = array();

  // Load the form from the Form API cache.
  if (!($cached_form = form_get_cache($_POST['form_build_id'], $cached_form_state)) || !($txn = ctools_object_cache_get('transaction', 'ec_checkout'))) {
    form_set_error('form_token', t('Validation error, please try again. If this error persists, please contact the site administrator.'));
    $output = theme('status_messages');
    print drupal_to_js(array('status' => TRUE, 'data' => $output));
    exit();
  }

  $form_state = array('values' => $_POST);
  $form_state['txn'] =& $txn;
  $txn->payment_method = $form_state['values']['select_payment_type'];

  ec_receipt_invoke($txn->payment_method, 'init', 'transaction', $txn);
  
  $form = array();

  ec_receipt_checkout_form($form, $form_state);
  $cached_form['ec_receipt'] = $form['ec_receipt'];
  $cached_form_state['storage'] = $form_state['storage'];

  $new_form['ec_receipt'] = $form['ec_receipt'];
  $form = $new_form;
  unset($form['ec_receipt']['#theme']);

  form_set_cache($_POST['form_build_id'], $cached_form, $cached_form_state);

  // Render the form for output.
  $form += array(
    '#post' => $_POST,
    '#programmed' => FALSE,
    '#tree' => FALSE,
    '#parents' => array(),
  );
  drupal_alter('form', $form, array(), 'ec_receipt_checkout_form');
  $form_state = array('submitted' => FALSE);
  $form = form_builder('ec_receipt_checkout_form', $form, $form_state);
  $output = theme('status_messages') . drupal_render($form);

  ctools_object_cache_set('transaction', 'ec_checkout', $txn);

  // We send the updated file attachments form.
  // Don't call drupal_json(). ahah.js uses an iframe and
  // the header output by drupal_json() causes problems in some browsers.
  print drupal_to_js(array('status' => TRUE, 'data' => $output));
  exit;
}

/**
 * Implementation of hook_checkout_post_submit().
 */
function ec_receipt_checkout_post_submit(&$txn, &$form_state) {
  if (ec_store_transaction_calc_gross($txn)) {
    $alloc[] = array(
      'type' => 'transaction',
      'id' => $txn->txnid,
    );
    if (empty($txn->receipt_no)) {
      return ec_receipt_payment_goto('transaction', $txn, $alloc);
    }
    else {
      if ($receipt = ec_receipt_load($txn->receipt_no)) {
        ec_receipt_allocate($receipt, $alloc);
      }
    }
    if (empty($form_state['redirect'])) {
      return variable_get('ec_receipt_return_url', 'node');
    }
  }
}
