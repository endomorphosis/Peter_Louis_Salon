<?php
// $Id: ec_receipt_views_handler_field_operations.inc,v 1.1.2.1 2009/07/02 10:25:39 recidive Exp $

/**
 * @file
 * Provide list of operation links for a receipt.
 */

class ec_receipt_views_handler_field_operations extends views_handler_field {

  function render($values) {
    if ($receipt = ec_receipt_load($values->{$this->field_alias})) {
      return theme('links', module_invoke_all('link', 'ec_receipt', $receipt));
    }
  }
}
