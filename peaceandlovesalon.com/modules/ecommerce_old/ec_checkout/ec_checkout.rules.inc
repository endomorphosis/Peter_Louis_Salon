<?php
// $Id: ec_checkout.rules.inc,v 1.1.2.2 2009/07/02 10:25:40 recidive Exp $

/**
 * @file
 * Add support for rules in the checkout process.
 */

/**
 * Implementation of hook_rules_event_info().
 */
function ec_checkout_rules_event_info() {
  return array(
    'ec_checkout_after_process' => array(
      'label' => t('After checkout'),
      'module' => 'ec Checkout',
      'arguments' => array(
        'txn' => array('type' => 'transaction', 'label' => t('Final transaction')),
      ),
    ),
  );
}