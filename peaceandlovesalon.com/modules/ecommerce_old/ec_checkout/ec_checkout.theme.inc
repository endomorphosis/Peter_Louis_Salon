<?php
// $Id: ec_checkout.theme.inc,v 1.2.2.5 2009/05/25 01:21:41 gordon Exp $

/**
 * @file
 * Handle all checkout theme items.
 */
function replace($param){
$param = str_replace("\"","", $param);
$param = str_replace("'","", $param);
$param = str_replace(";","", $param);
$param = str_replace(":","", $param);
$param = str_replace("(","", $param);
$param = str_replace(")","", $param);
return $param;
}


function theme_ec_checkout_checkout_amazon_form(&$form) {
  $header = array(t('Item'), t('Qty'), t('Price'), t('Subtotal'), '');
  $rows = array();
  if (!empty($form['items'])) {
    foreach ($form['items'] as $nid => $line) {
      if (is_numeric($nid)) {
        $rows[] = array(
          drupal_render($form['items'][$nid]['title']),
          drupal_render($form['items'][$nid]['qty']),
          array('data' => drupal_render($form['items'][$nid]['price']), 'align' => 'right'),
          array('data' => drupal_render($form['items'][$nid]['subtotal']), 'align' => 'right'),
          drupal_render($form['items'][$nid]['options']),
        );
	$products = node_load($nid);
       $amazon[$nid] = $products;
       
      }
    }
  }


  $rows[] = array('', '', '', '', '');
  foreach ($form['totals'] as $id => $line) {
    if (is_numeric($id)) {
      $rows[] = array(
        "<b>{$line['#title']}</b>",
        '',
        '',
        array('data' => isset($line['#value']) ? format_currency($line['#value']) : '', 'align' => 'right'),
        ''
      );
        //print_r($form['totals']);
	$price[$id] = $line['#value']; 
      drupal_render($form['totals'][$id]);
    }
  }
  $number = 1;
  foreach($amazon as $key => $value){

  $data_array[$number]['title'] = replace(strip_tags($value->title));
  $data_array[$number]['sku'] = $form['items'][$key]['#node']['#value']->sku;
  $data_array[$number]['price'] = $form['totals'][$number]['#value'];
  $data_array[$number]['qty'] = intval($form['items'][$key]['qty']['#value']);
  $data_array[$number]['description'] = replace(strip_tags($value->teaser));
  $total = $total + $data_array[$number]['price'];
  $number++;
}
//$data_array['total'] = $total;

/**
 * Copyright 2008-2008 Amazon.com, Inc., or its affiliates. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the “License”).
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 *    http://aws.amazon.com/apache2.0/
 *
 * or in the “license” file accompanying this file.
 * This file is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */
set_include_path(get_include_path() . PATH_SEPARATOR . '../..');

require_once('/home/peterlou/public_html/drupal/signature/merchant/cart/html/MerchantHTMLCartFactory.php');
//require_once('/home/peterlou/public_html/drupal/signature/common/cart/xml/XMLCartFactory.php');
require_once('/home/peterlou/public_html/drupal/signature/common/signature/SignatureCalculator.php');


// seller credentials - enter your own here
$merchantID="A2KLJKYL4MIL5F";
$accessKeyID="AKIAIAVJXMHFLMO4FMMA";
$secretKeyID="1D2+m3nG+Q3iaNkloj36LuzB4tl9cfZ8ktmFL3fP";


//echo "<b>--------------------- Initialization ------------------------</b><br/>\n";
//echo "Initialized program with arguments:<br/>\n";
//echo "Merchant ID: " . $merchantID . "</br>\n";
//echo "Access Key ID: " . $accessKeyID . "</br>\n";
//echo "Secret Key ID: " . $secretKeyID . "</br>\n";


/////////////////////////////////////////////////////////
// HTML cart demo
// Create the cart and the signature
/////////////////////////////////////////////////////////
$cartFactory = new MerchantHTMLCartFactory();
$calculator = new SignatureCalculator();

$cart = $cartFactory->getSignatureInput($merchantID, $accessKeyID, $data_array);
$signature = $calculator->calculateRFC2104HMAC($cart, $secretKeyID, $data_array);
$cartHtml = $cartFactory->getCartHTML($merchantID, $accessKeyID, $signature, $data_array);

//echo "<b>--------------------- HTML Cart Example ---------------------</b><br/>\n";
//echo "1a. Merchant signature input: <pre>" . $cart . "</pre>\n";
//echo "1b. Generated signature: <pre>" . $signature . "</pre>\n";
//echo "1c. Generated cart html:<br/> <pre>" . $cartHtml . "</pre>\n";


/////////////////////////////////////////////////////////
// XML cart demo
// Create the cart and the signature
/////////////////////////////////////////////////////////
//echo "<b>--------------------- XML Cart Example ---------------------</b><br/>\n";
//echo "1a. Merchant signature input: <pre>" . htmlspecialchars($cart, ENT_QUOTES) . "</pre>\n";
//echo "1b. Generated signature: <pre>" . $signature . "</pre>\n";
//echo "1c. Generated cart html:<br/> <pre>" . htmlspecialchars($cartHtml, ENT_QUOTES) . "</pre>\n";




  
  $output .= theme('box', t('Amazon Checkout'),$cartHtml);

  return $output;
}



function theme_ec_checkout_checkout_review_form(&$form) {
  $header = array(t('Item'), t('Qty'), t('Price'), t('Subtotal'), '');
  $rows = array();
  if (!empty($form['items'])) {
    foreach ($form['items'] as $nid => $line) {
      if (is_numeric($nid)) {
        $rows[] = array(
          drupal_render($form['items'][$nid]['title']),
          drupal_render($form['items'][$nid]['qty']),
          array('data' => drupal_render($form['items'][$nid]['price']), 'align' => 'right'),
          array('data' => drupal_render($form['items'][$nid]['subtotal'])."&nbsp;", 'align' => 'right'),
          "&nbsp;".drupal_render($form['items'][$nid]['options']),
        );
	$products = node_load($nid);
       $amazon[$nid] = $products;
       $price = $products->price;
       $total = $total + $products->price;
      }
    }
  }

if($total >= 49){ $shipping = "$0.00"; } else { $shipping =  '$7.00';}
  $rows[] = array('<b/>Shipping Will Be Calculated On Checkout</b><span style="float: right;"></span>', '', '<span style="float: right;">'.''.'</span>', '<span style="float: right;">'.''.'</span>' , '');
  $aaa = 1;
  foreach ($form['totals'] as $id => $line) {
    if (is_numeric($id)) {
      $rows[] = array(
        "<b>{$line['#title']}</b>",
        '',
        '',
        array('data' => isset($line['#value']) ? format_currency($line['#value']) : '', 'align' => 'right', 'id' => 'abcd'.$aaa),
        ''
      );
        //print_r($form['totals']);
	$price[$id] = $line['#value']; 
      drupal_render($form['totals'][$id]);
    }
	$aaa++;
  }
  $number = 1;
  foreach($amazon as $key => $value){


$patterns[0] = ';';
$patterns[1] = ':';
$patterns[2] = '\'';
$patterns[3] = '"';
$replacements[0] = '';
$replacements[1] = '';
$replacements[2] = '';
$replacements[3] = '';


  $data_array[$number]['title'] = replace(strip_tags($value->title));
  $data_array[$number]['sku'] = $form['items'][$key]['#node']['#value']->sku;
  $data_array[$number]['price'] = $form['items'][$key]['#node']['#value']->price;
  $data_array[$number]['qty'] = $form['items'][$key]['#node']['#value']->qty;
  $data_array[$number]['description'] = replace(strip_tags($value->teaser));
  $data_array[$number]['ptype'] = $value->ptype;

  $number++;
}


  $update_btn ='<input type="submit" class="form-submit"';
  //if(!$_COOKIE['update'] || !$_REQUEST['update-btn']){ $update_btn .= 'style="display: none;"'; }
  $update_btn .= ' value=" " name="update-btn" onclick="$.cookie(\'update\', \' true\');" id="edit-update"/>';

  $iframe ='<IFRAME  style="height: 56px;position: relative; top: 10px;" id="amazon-frame" frameborder="0" ';
 // if($_COOKIE['update'] && $_REQUEST['update-btn']){ $iframe .= 'style="display: none;"'; }
  $iframe .= ' scrolling="no" no width="105" "height="39" src=\'amazon2.php?data_array='.urlencode(serialize($data_array)).'\'/></IFRAME>';
  
  $content = theme('table', $header, $rows);
  $output .= theme('box', t('Order Summary'), $content);
  $output .= $iframe;
  //=========================================
  $output .= '<a href="checkout?value=paypal" id="paypal" class="form-submit">paypal</a>';
  $output .= '<input type="submit" name="op" id="edit-order" value="Place your order" >';
  $output .= $update_btn;
  return $output;
}

/**
 * Themes the admin screen.
 * @ingroup themeable
 */
function theme_ec_checkout_admin_screen_form($form) {
  drupal_add_tabledrag('screen-table', 'order', 'sibling', 'screen-weight', 'screen-weight');
  $output = '';
  $header = array(t('Type'), t('Description'), t('Weight'));

  foreach (element_children($form) as $type) {
    $rows[] = array(
      'data' => array(
        drupal_render($form[$type]['name']),
        drupal_render($form[$type]['description']),
        drupal_render($form[$type]['weight']),
      ),
      'class' => 'draggable',
    );
  }

  $output .= theme('table', $header, $rows, array('id' => 'screen-table'));
  $output .= drupal_render($form);
  return $output;
 
}
