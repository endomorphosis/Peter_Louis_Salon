<?php
// $Id: ec_store_views_handler_field_operations.inc,v 1.1.2.1 2009/07/02 10:25:39 recidive Exp $

/**
 * @file
 * Provide list of operation links for transactions.
 */

class ec_store_views_handler_field_operations extends views_handler_field {

  function render($values) {
    if ($txn = ec_store_transaction_load($values->{$this->field_alias})) {
      return theme('links', module_invoke_all('link', 'ec_store_transaction', $txn));
    }
  }
}
