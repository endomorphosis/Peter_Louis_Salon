<?php
// $Id: ec_store.theme.inc,v 1.1.2.5 2009/07/02 10:25:41 recidive Exp $

/**
 * @file
 * Stored themes for ec_store module.
 */

/**
 * Format an address with carriage returns or HTML line breaks.
 *
 * @param $txn
 *   Object, User's address information.
 * @param $type
 *   String, Is this a shipping or billing address.
 * @param $break
 *   String, Should we break using '\n' (text) or '<br />' (html).
 */
function theme_ec_store_format_address($txn, $type = 'shipping', $break = 'text') {
  if (empty($txn->address[$type])) {
    return FALSE;
  }
  $data = drupal_clone((object) $txn->address[$type]);

  // Cleanup values.
  if ($break != 'text') {
    foreach ($data as $key => $value) {
      if (is_scalar($value)) {
        $data->$key = check_plain($value);
      }
    }
  }
  $break = ($break == 'text') ? "\n" : '<br />';

  $address = '';
  if ((!empty($data->firstname) && !empty($data->lastname)) || !empty($data->fullname)) {
    if (!empty($data->firstname) || !empty($data->lastname)) {
      $address .= $data->firstname .' '. $data->lastname . $break;
    }
    else {
      $address .= !empty($data->fullname) ? $data->fullname . $break : '';
    }
    $address .= isset($data->street2) ? $data->street1 . $break . $data->street2 . $break : $data->street1. $break;
    $address .= drupal_ucfirst($data->city) .", ". drupal_strtoupper($data->state) ." ". $data->zip . $break;
    $address .= ec_store_get_country($data->country) . $break;
    $address .= $data->phone . $break;
  }

  return $address;
}

function theme_ec_store_workflow_settings($elements) {
  drupal_add_tabledrag('ec-workflow', 'order', 'sibling', 'ec-workflow-weight');

  $output = '';
  $header = array(t('Workflow'), t('Type'), t('Weight'), '');
  $rows = array();

  foreach (element_children($elements['ec_store_workflow']) as $id) {
    if (isset($elements['ec_store_workflow'][$id]['weight']['#attributes']['class'])) {
      $elements['ec_store_workflow'][$id]['weight']['#attributes']['class'].= ' ec-workflow-weight';
    }
    else {
      $elements['ec_store_workflow'][$id]['weight']['#attributes']['class'] = 'ec-workflow-weight';
    }
    $rows[] = array(
      'data' => array(
        drupal_render($elements['ec_store_workflow'][$id]['description']),
        drupal_render($elements['ec_store_workflow'][$id]['type']),
        drupal_render($elements['ec_store_workflow'][$id]['weight']),
        drupal_render($elements['ec_store_workflow'][$id]['operations']),
      ),
      'class' => 'draggable',
    );
  }

  $output.= theme('table', $header, $rows, array('id' => 'ec-workflow'));
  $output.= drupal_render($elements);

  return $output;
}
