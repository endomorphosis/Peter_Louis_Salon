<?php
// $Id: ec_store.charge.inc,v 1.1.2.4 2009/07/03 18:55:52 recidive Exp $

/**
 * @file
 * Charge functions for ec_store.
 */

function ec_store_misc_group_filter_form(&$form_state, $settings = array()) {
  $form = array();
  $settings+= array('group' => '');
  
  $form['group'] = array(
    '#type' => 'textfield',
    '#title' => t('Charge group'),
    '#default_value' => $settings['group'],
    '#required' => TRUE,
  );
  
  return $form;
}

function ec_store_misc_group_filter_settings() {
  return array('group');
}

function ec_store_misc_group_filter_process($type, $settings, $object, $charges = array()) {
  $$type =& $object;
  
  switch ($type) {
    case 'txn':
      if (!empty($txn->misc)) {
        $charges = $txn->misc;
      }
      break;
  }
  
  ec_store_filter_misc(array('misc_group' => $settings['group']), 'init');
  
  $charges = array_filter($charges, 'ec_store_filter_misc');
  
  return !empty($charges);
}

function ec_store_txn_type_filter_form($form_state, $settings = array()) {
  $settings += array(
    'txn_type' => array(),
  );
  
  $options = ec_store_transaction_types();
  
  $form['txn_type'] = array(
    '#type' => 'select',
    '#title' => t('Transaction type'),
    '#default_value' => $settings['txn_type'],
    '#options' => $options,
    '#description' => t('Select type of transactions to allow.'),
    '#required' => TRUE,
    '#multiple' => TRUE,
  );
  
  return $form;
}

function ec_store_txn_type_filter_settings() {
  return array('txn_type');
}

function ec_store_txn_type_filter_process($type, $settings, $object, $charges = array()) {
  $$type =& $object;
  
  switch ($type) {
    case 'txn':
      if (in_array($txn->type, $settings['txn_type'])) {
        return TRUE;
      }
      break;
  }
  
  return FALSE;
}

function ec_store_misc_group_variable_form(&$form_state, $settings = array()) {
  return ec_store_misc_group_filter_form($form_state, $settings);
}

function ec_store_misc_group_variable_settings() {
  return array('group');
}

function ec_store_misc_group_variable_process($type, $settings, $object, $charges) {
  $$type =& $object;
  
  switch ($type) {
    case 'txn':
      if (!empty($txn->misc)) {
        $charges = array_merge($charges, $txn->misc);
      }
      break;
  }
  
  ec_store_filter_misc(array('misc_group' => $settings['group']), 'init');
  
  $charges = array_filter($charges, 'ec_store_filter_misc');
  
  return array_sum(array_map('ec_store_map_price', $charges));
}

function ec_store_misc_group_variable_description($chg, $type, $object, $variables) {
  return t('The summation of all the additional charges with the tax group %group', array('%group' => $settings['group']));
}