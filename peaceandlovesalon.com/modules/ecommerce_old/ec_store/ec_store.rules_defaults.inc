<?php
// $Id: ec_store.rules_defaults.inc,v 1.1.2.6 2009/10/04 00:49:43 gordon Exp $

/**
 * @file
 * Implement default rules.
 */

/**
 * Implementation of hook_rules_defaults().
 */
function ec_store_rules_defaults() {
  $config = array (
    'rules' => 
    array (
      'rules_10' => 
      array (
        '#type' => 'rule',
        '#set' => 'event_ec_store_event_transactions_bef_save',
        '#label' => 'Mark zero price invoices as allocated',
        '#active' => 1,
        '#weight' => '-1',
        '#status' => 'custom',
        '#conditions' => 
        array (
          0 => 
          array (
            '#type' => 'condition',
            '#settings' => 
            array (
              'condition' => '=',
              'amount' => '0',
            ),
            '#name' => 'ec_store_condition_gross',
            '#info' => 
            array (
              'label' => 'Transaction gross amount',
              'arguments' => 
              array (
                'txn' => 
                array (
                  'type' => 'transaction',
                  'label' => 'Transaction',
                ),
              ),
              'module' => 'Transaction',
            ),
            '#weight' => 0,
          ),
          1 => 
          array (
            '#weight' => 0,
            '#info' => 
            array (
              'label' => 'Transaction allocation status',
              'arguments' => 
              array (
                'txn' => 
                array (
                  'type' => 'transaction',
                  'label' => 'Transaction',
                ),
              ),
              'module' => 'Transaction',
            ),
            '#name' => 'ec_store_condition_allocation_status',
            '#settings' => 
            array (
              'allocation' => 
              array (
                0 => '0',
              ),
            ),
            '#type' => 'condition',
          ),
        ),
        '#actions' => 
        array (
          0 => 
          array (
            '#weight' => 0,
            '#info' => 
            array (
              'label' => 'Set transaction allocation',
              'arguments' => 
              array (
                'transaction' => 
                array (
                  'type' => 'transaction',
                  'label' => 'Transaction',
                ),
              ),
              'module' => 'Transaction',
            ),
            '#name' => 'ec_store_rules_action_set_allocation',
            '#settings' => 
            array (
              'allocation' => '2',
              '#argument map' => 
              array (
                'transaction' => 'txn',
              ),
            ),
            '#type' => 'action',
          ),
        ),
        '#categories' => 
        array (
          0 => 'ec_store',
        ),
        '#version' => 6003,
      ),
      'rules_11' => 
      array (
        '#type' => 'rule',
        '#set' => 'event_ec_store_event_transactions_save',
        '#label' => 'Mail customer on completed transaction',
        '#active' => 0,
        '#weight' => '0',
        '#status' => 'custom',
        '#conditions' => 
        array (
          0 => 
          array (
            '#weight' => 0,
            '#info' => 
            array (
              'label' => 'Transaction workflow status changed',
              'arguments' => 
              array (
                'txn' => 
                array (
                  'type' => 'transaction',
                  'label' => 'Transaction',
                ),
                'orig_txn' => 
                array (
                  'type' => 'transaction',
                  'label' => 'Transaction',
                ),
              ),
              'module' => 'Transaction',
            ),
            '#name' => 'ec_store_condition_workflow_status_changed',
            '#type' => 'condition',
          ),
          1 => 
          array (
            '#type' => 'condition',
            '#settings' => 
            array (
              'workflow' => 
              array (
                6 => '6',
              ),
            ),
            '#name' => 'ec_store_condition_workflow_status',
            '#info' => 
            array (
              'label' => 'Transaction workflow status',
              'arguments' => 
              array (
                'txn' => 
                array (
                  'type' => 'transaction',
                  'label' => 'Transaction',
                ),
              ),
              'module' => 'Transaction',
            ),
            '#weight' => 0,
          ),
        ),
        '#actions' => 
        array (
          0 => 
          array (
            '#info' => 
            array (
              'label' => 'Send a mail to an arbitrary mail address',
              'module' => 'System',
              'eval input' => 
              array (
                0 => 'subject',
                1 => 'message',
                2 => 'from',
                3 => 'to',
              ),
            ),
            '#name' => 'rules_action_mail',
            '#settings' => 
            array (
              'to' => '[txn:txn-mail]',
              'from' => 'sales@example.com',
              'subject' => 'Order Complete',
              'message' => 'Transaction [txn:txn-id] was sent on date [txn:txn-order-date]

  Thank you for your order',
              '#eval input' => 
              array (
                'token_rules_input_evaluator' => 
                array (
                  'message' => 
                  array (
                    0 => 'txn',
                  ),
                  'to' => 
                  array (
                    0 => 'txn',
                  ),
                ),
              ),
            ),
            '#type' => 'action',
            '#weight' => 0,
          ),
        ),
        '#categories' => 
        array (
          0 => 'ec_store',
        ),
        '#version' => 6003,
      ),
      'rules_6' => 
      array (
        '#type' => 'rule',
        '#set' => 'event_ec_store_event_transactions_bef_save',
        '#label' => 'Set non-shippable transaction as complete',
        '#active' => 1,
        '#weight' => '0',
        '#status' => 'custom',
        '#conditions' => 
        array (
          3 => 
          array (
            '#negate' => 1,
            '#weight' => -1,
            '#info' => 
            array (
              'label' => 'Transaction is shippable',
              'arguments' => 
              array (
                'txn' => 
                array (
                  'type' => 'transaction',
                  'label' => 'Transaction',
                ),
              ),
              'module' => 'Transaction',
            ),
            '#name' => 'ec_store_condition_is_shippable',
            '#settings' => 
            array (
              '#argument map' => 
              array (
                'txn' => 'txn',
              ),
            ),
            '#type' => 'condition',
          ),
          0 => 
          array (
            '#info' => 
            array (
              'label' => 'Transaction allocation status changed',
              'arguments' => 
              array (
                'txn' => 
                array (
                  'type' => 'transaction',
                  'label' => 'Transaction',
                ),
                'orig_txn' => 
                array (
                  'type' => 'transaction',
                  'label' => 'Transaction',
                ),
              ),
              'module' => 'Transaction',
            ),
            '#name' => 'ec_store_condition_allocation_status_changed',
            '#settings' => 
            array (
              '#argument map' => 
              array (
                'txn' => 'txn',
                'orig_txn' => 'orig_txn',
              ),
            ),
            '#type' => 'condition',
            '#weight' => 0,
          ),
          1 => 
          array (
            '#weight' => 0,
            '#type' => 'condition',
            '#settings' => 
            array (
              'allocation' => 
              array (
                2 => '2',
              ),
              '#argument map' => 
              array (
                'txn' => 'txn',
              ),
            ),
            '#name' => 'ec_store_condition_allocation_status',
            '#info' => 
            array (
              'label' => 'Transaction allocation status',
              'arguments' => 
              array (
                'txn' => 
                array (
                  'type' => 'transaction',
                  'label' => 'Transaction',
                ),
              ),
              'module' => 'Transaction',
            ),
          ),
          2 => 
          array (
            '#info' => 
            array (
              'label' => 'Transaction workflow status',
              'arguments' => 
              array (
                'txn' => 
                array (
                  'type' => 'transaction',
                  'label' => 'Transaction',
                ),
              ),
              'module' => 'Transaction',
            ),
            '#name' => 'ec_store_condition_workflow_status',
            '#settings' => 
            array (
              'workflow' => 
              array (
                1 => '1',
                2 => '2',
              ),
              '#argument map' => 
              array (
                'txn' => 'txn',
              ),
            ),
            '#type' => 'condition',
            '#weight' => 0,
          ),
        ),
        '#actions' => 
        array (
          1 => 
          array (
            '#weight' => 0,
            '#info' => 
            array (
              'label' => 'Set transaction workflow',
              'arguments' => 
              array (
                'transaction' => 
                array (
                  'type' => 'transaction',
                  'label' => 'Transaction',
                ),
              ),
              'module' => 'Transaction',
            ),
            '#name' => 'ec_store_rules_action_set_workflow',
            '#settings' => 
            array (
              'workflow' => '6',
              '#argument map' => 
              array (
                'transaction' => 'txn',
              ),
            ),
            '#type' => 'action',
          ),
        ),
        '#version' => 6003,
        '#categories' => 
        array (
          0 => 'ec_store',
        ),
      ),
      'rules_7' => 
      array (
        '#type' => 'rule',
        '#set' => 'event_ec_store_event_transactions_bef_save',
        '#label' => 'Shippable orders now in picking',
        '#active' => 1,
        '#weight' => '0',
        '#status' => 'custom',
        '#conditions' => 
        array (
          0 => 
          array (
            '#type' => 'condition',
            '#settings' => 
            array (
              '#argument map' => 
              array (
                'txn' => 'txn',
              ),
            ),
            '#name' => 'ec_store_condition_is_shippable',
            '#info' => 
            array (
              'label' => 'Transaction is shippable',
              'arguments' => 
              array (
                'txn' => 
                array (
                  'type' => 'transaction',
                  'label' => 'Transaction',
                ),
              ),
              'module' => 'Transaction',
            ),
            '#weight' => 0,
          ),
          1 => 
          array (
            '#weight' => 0,
            '#info' => 
            array (
              'label' => 'Transaction allocation status changed',
              'arguments' => 
              array (
                'txn' => 
                array (
                  'type' => 'transaction',
                  'label' => 'Transaction',
                ),
                'orig_txn' => 
                array (
                  'type' => 'transaction',
                  'label' => 'Transaction',
                ),
              ),
              'module' => 'Transaction',
            ),
            '#name' => 'ec_store_condition_allocation_status_changed',
            '#settings' => 
            array (
              '#argument map' => 
              array (
                'txn' => 'txn',
                'orig_txn' => 'orig_txn',
              ),
            ),
            '#type' => 'condition',
          ),
          2 => 
          array (
            '#type' => 'condition',
            '#settings' => 
            array (
              'allocation' => 
              array (
                2 => '2',
              ),
              '#argument map' => 
              array (
                'txn' => 'txn',
              ),
            ),
            '#name' => 'ec_store_condition_allocation_status',
            '#info' => 
            array (
              'label' => 'Transaction allocation status',
              'arguments' => 
              array (
                'txn' => 
                array (
                  'type' => 'transaction',
                  'label' => 'Transaction',
                ),
              ),
              'module' => 'Transaction',
            ),
            '#weight' => 0,
          ),
          3 => 
          array (
            '#weight' => 0,
            '#info' => 
            array (
              'label' => 'Transaction workflow status',
              'arguments' => 
              array (
                'txn' => 
                array (
                  'type' => 'transaction',
                  'label' => 'Transaction',
                ),
              ),
              'module' => 'Transaction',
            ),
            '#name' => 'ec_store_condition_workflow_status',
            '#settings' => 
            array (
              'workflow' => 
              array (
                1 => '1',
              ),
              '#argument map' => 
              array (
                'txn' => 'txn',
              ),
            ),
            '#type' => 'condition',
          ),
        ),
        '#actions' => 
        array (
          0 => 
          array (
            '#weight' => 0,
            '#type' => 'action',
            '#settings' => 
            array (
              'workflow' => '10',
              '#argument map' => 
              array (
                'transaction' => 'txn',
              ),
            ),
            '#name' => 'ec_store_rules_action_set_workflow',
            '#info' => 
            array (
              'label' => 'Set transaction workflow',
              'arguments' => 
              array (
                'transaction' => 
                array (
                  'type' => 'transaction',
                  'label' => 'Transaction',
                ),
              ),
              'module' => 'Transaction',
            ),
          ),
        ),
        '#version' => 6003,
        '#categories' => 
        array (
          0 => 'ec_store',
        ),
      ),
      'rules_8' => 
      array (
        '#type' => 'rule',
        '#set' => 'event_ec_store_event_transactions_bef_save',
        '#label' => 'Mark shipped product as completed',
        '#active' => 1,
        '#weight' => '0',
        '#status' => 'custom',
        '#conditions' => 
        array (
          0 => 
          array (
            '#weight' => 0,
            '#info' => 
            array (
              'label' => 'Transaction workflow status changed',
              'arguments' => 
              array (
                'txn' => 
                array (
                  'type' => 'transaction',
                  'label' => 'Transaction',
                ),
                'orig_txn' => 
                array (
                  'type' => 'transaction',
                  'label' => 'Transaction',
                ),
              ),
              'module' => 'Transaction',
            ),
            '#name' => 'ec_store_condition_workflow_status_changed',
            '#type' => 'condition',
          ),
          1 => 
          array (
            '#type' => 'condition',
            '#settings' => 
            array (
              'workflow' => 
              array (
                3 => '3',
              ),
            ),
            '#name' => 'ec_store_condition_workflow_status',
            '#info' => 
            array (
              'label' => 'Transaction workflow status',
              'arguments' => 
              array (
                'txn' => 
                array (
                  'type' => 'transaction',
                  'label' => 'Transaction',
                ),
              ),
              'module' => 'Transaction',
            ),
            '#weight' => 0,
          ),
        ),
        '#actions' => 
        array (
          0 => 
          array (
            '#weight' => 0,
            '#info' => 
            array (
              'arguments' => 
              array (
                'task_date' => 
                array (
                  'type' => 'date',
                  'label' => 'Scheduled evaluation date',
                ),
                'transaction' => 
                array (
                  'label' => 'Transaction',
                  'type' => 'transaction',
                ),
              ),
              'label' => 'Schedule Schedule flaging transaction as completed',
              'status' => 'custom',
              'module' => 'Rule Scheduler',
              'base' => 'rules_scheduler_action',
              'set' => 'rules_txn_complete',
            ),
            '#name' => 'rules_action_schedule_set_rules_txn_complete',
            '#settings' => 
            array (
              'task_date' => '+12 hours',
            ),
            '#type' => 'action',
          ),
        ),
        '#version' => 6003,
        '#categories' => 
        array (
          0 => 'ec_store',
        ),
      ),
    ),
  );

  return $config;
}
