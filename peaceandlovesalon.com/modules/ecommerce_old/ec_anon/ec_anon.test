<?php
// $Id: ec_anon.test,v 1.1.2.7 2009/10/04 02:48:46 gordon Exp $

/**
 * @file
 * Provide tests for ec_anon module.
 */

class ecAnonTestCase extends DrupalWebTestCase {
  function getInfo() {
    return array(
      'name' => t('Anonymous purchasing'),
      'description' => t('Test the purchasing of products that are marked as anonymous or for registed users.'),
      'group' => t('e-Commerce'),
    );
  }

  function setUp() {
    parent::setUp('ec_product', 'ec_anon', 'ec_buynow', 'ec_cart', 'ctools', 'ec_checkout', 'ec_store', 'ec_common');
  }

  function testAnonUserPurchase() {
    $product = array(
      'type' => 'product',
      'ptype' => 'generic',
      'price' => '20.00',
      'anon_policy' => 1,
    );

    $node = $this->drupalCreateNode($product);

    $this->drupalGet('node/'. $node->nid);
    $this->assertNoLink(t('Buy now'), t('Buy now link doesn\'t exist'));
    $this->assertNoLink(t('Add to cart'), t('Add to cart link doesn\'t exist'));
    $this->drupalGet('node/'. $node->nid .'/checkout');
    
    $this->assertText(t('Only registered users can purchase this product'), t('Product can\'t be purchased by anonymous customers'));

    $product['anon_policy'] = 3;

    $node = $this->drupalCreateNode($product);

    $this->drupalGet('node/'. $node->nid);
    $this->assertLink(t('Buy now'));
    $this->assertLink(t('Add to cart'));
    $this->clickLink(t('Buy now'));
    $this->assertNoText(t('Only registered users can purchase this product'), t('Product can be purchased by anonymous customers'));
  }

  function testRegisteredUserPurchase() {
    $account = $this->drupalCreateUser();
    $this->drupalLogin($account);

    $product = array(
      'ptype' => 'generic',
      'price' => '20.00',
      'anon_policy' => 3,
    );

    $node = $this->drupalCreateNode($product);
    
    $this->drupalGet('node/'. $node->nid);
    $this->assertNoLink(t('Buy now'), t('Buy now link doesn\'t exist'));
    $this->assertNoLink(t('Add to cart'), t('Add to cart link doesn\'t exist'));
    $this->drupalGet('node/'. $node->nid .'/checkout');
    $this->assertText(t('Only non-registered users can purchase this product'), t('Product can\'t be purchased by registered customers'));

    $product['anon_policy'] = 1;

    $node = $this->drupalCreateNode($product);

    $this->drupalGet('node/'. $node->nid);
    $this->assertLink(t('Buy now'));
    $this->assertLink(t('Add to cart'));
    $this->clickLink(t('Buy now'));
    $this->assertNoText(t('Only non-registered users can purchase this product'), t('Product can be purchased by registered customers'));
  }
}
