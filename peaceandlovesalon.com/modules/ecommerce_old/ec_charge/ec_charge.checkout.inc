<?php
// $Id: ec_charge.checkout.inc,v 1.1.2.5 2009/05/30 13:25:11 gordon Exp $

/**
 * @file
 * Implements checkout integration for ec_charge.
 */

/**
 * Implementation of hook_checkout_calculate().
 */
function ec_charge_checkout_calculate(&$form_state) {
  $txn =& $form_state['txn'];
  $charges = array();
  $items = array();

  if (!empty($txn->misc)) {
    $txn->misc = array_filter($txn->misc, '_ec_charge_checkout_strip_charges');
  }

  $result = db_query('SELECT * FROM {ec_charge} WHERE enabled = 1 AND type = 0 ORDER BY weight ASC');

  while ($chg = db_fetch_array($result)) {
    $chg = ec_charge_load($chg['chgid']);

    if (ec_charge_filter($chg, 'txn', $txn, $items)) {
      $misc = ec_charge_create_charges($chg, 'txn', $txn, $items);

      foreach ($misc as $item) {
        if (isset($charges[$item['type']])) {
          $charges[$item['type']]++;
          $item['type'].= '-'. $charges[$item['type']];
        }
        else {
          $charges[$item['type']] = 0;
        }
        $txn->misc[] = (object)$item;
        $items[] = (object)$item;
      }
    }
  }
}

function _ec_charge_checkout_strip_charges($a) {
  return substr($a->type, 0, 3) != 'MT-';
}
