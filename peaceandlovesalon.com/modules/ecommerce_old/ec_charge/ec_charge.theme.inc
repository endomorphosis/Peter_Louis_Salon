<?php
// $Id: ec_charge.theme.inc,v 1.1.2.10 2009/07/02 10:25:40 recidive Exp $

/**
 * @file
 * Provide theme items for ec_charge.
 */

function theme_ec_charge_component_form($form) {
  $type = $form['#ctype'];
  $output = '<h3>'. drupal_ucfirst($type .'s') .'</h3>';
  $head = array(t('Description'));
  if ($type != 'variable') {
    $head[] = array('data' => t('Weight'), 'class' => $type .'-weight');
  }
  $head[] = t('Operations');
  $rows = array();

  foreach (_ec_charge_get_table_ids($form) as $key) {
    $rows[] = array(
      'data' => ec_charge_component_form_render_row($type, $form[$key]),
      'class' => 'draggable '. ($form[$key]['#parent'] ? 'tabledrag-root' : 'tabledrag-leaf'),
    );
  }
  $output .= '<div id="'. $type .'-wrapper">';
  if (!empty($rows)) {
    $attributes = array('id' => $type .'-table');

    $output .= theme('table', $head, $rows, $attributes);
  }
  $output .= '</div>';
  $output .= drupal_render($form);
  return $output;
}

function theme_ec_charge_admin_list_table($form) {
  drupal_add_tabledrag('ec-charge-table', 'order', 'sibling', 'ec-charge-weight');

  $head = array(t('Charge'), t('Description'), t('Type'), t('Enabled'), t('Weight'), '');

  $rows = array();
  foreach (element_children($form) as $id) {
    $form[$id]['weight']['#attributes']['class'] = 'ec-charge-weight';
    $rows[] = array(
      'data' => array(
        drupal_render($form[$id]['name']),
        drupal_render($form[$id]['description']),
        drupal_render($form[$id]['type']),
        drupal_render($form[$id]['enabled']),
        drupal_render($form[$id]['weight']),
        drupal_render($form[$id]['operations']),
      ),
      'class' => 'draggable',
    );
  }

  $output = theme('table', $head, $rows, array('id' => 'ec-charge-table'));
  $output.= drupal_render($form);

  return $output;
}
