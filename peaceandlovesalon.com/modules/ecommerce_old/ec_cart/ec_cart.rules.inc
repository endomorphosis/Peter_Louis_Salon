<?php
// $Id: ec_cart.rules.inc,v 1.1.2.3 2009/07/02 10:25:41 recidive Exp $

/**
 * @file
 * Enable rules integration into ec_cart to allow the manipluation of events.
 */

/**
 * Implementation of hook_rules_event_info().
 */
function ec_cart_rules_event_info() {
  return array(
    'ec_cart_add' => array(
      'label' => t('After item added/added to cart'),
      'module' => 'ec Cart',
      'arguments' => array(
        'product' => array('type' => 'node', 'label' => t('Product that was added to cart')),
      ),
    ),
    'ec_cart_add_form' => array(
      'label' => t('After item has been added by form'),
      'module' => 'ec Cart',
      'arguments' => array(
        'product' => array('type' => 'node', 'label' => t('Product that was added to cart')),
      ),
    ),
    'ec_cart_delete' => array(
      'label' => t('After item has been deleted from the cart'),
      'module' => 'ec Cart',
      'arguments' => array(
        'product' => array('type' => 'node', 'label' => t('Product that was deleted from the cart')),
      ),
    ),
    'ec_cart_edit' => array(
      'label' => t('After item has been edited in the cart'),
      'module' => 'ec Cart',
      'arguments' => array(
        'product' => array('type' => 'node', 'label' => t('Product that was edited in the cart')),
      ),
    ),
    'ec_cart_view' => array(
      'label' => t('Before cart view page'),
      'module' => 'ec Cart',
      'arguments' => array(),
    ),
  );
}

/**
 * Implementation of hook_rules_condition_info().
 */
function ec_cart_rules_condition_info() {
  return array(
    'ec_cart_is_empty' => array(
      'label' => t('Cart is empty'),
      'module' => 'ec Cart',
      'arguments' => array(
      ),
    ),
  );
}

function ec_cart_is_empty() {
  $items = ec_cart_current();
  return empty($items);
}