<?php
// $Id: ec_address_handler_field_link_delete.inc,v 1.1.2.1 2009/07/02 10:25:41 recidive Exp $

/**
 * @file
 * Provide link to delete an address.
 */

class ec_address_handler_field_link_delete extends ec_address_handler_field_link_edit {
  function render($values) {
    $text = !empty($this->options['text']) ? $this->options['text'] : t('delete');
    $aid = $values->{$this->aliases['aid']};
    $uid = $values->{$this->aliases['uid']};
    return l($text, "user/$uid/ec_address/$aid/delete");
  }
}
