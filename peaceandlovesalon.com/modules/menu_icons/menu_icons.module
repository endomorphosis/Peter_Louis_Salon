<?php
// $Id: menu_icons.module,v 1.2 2009/04/10 07:50:20 skilip Exp $

/**
 * @file
 * Module to associate icons with menu items
 *
 * @author dylan@opensourcery.com
 *
 */

// TODO - provide option for linked, inline img tags, in addition to background CSS
// TODO - implement blocks

/**
 * Implementation of hook_menu().
 */
function menu_icons_menu() {
  $items['menu_icons/css'] = array(
    'title' => 'Menu icons CSS',
    'page callback' => 'menu_icons_css',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );
  $items['admin/settings/menu_icons'] = array(
    'title' => 'Menu Icon settings',
    'description' =>  'Associates icons with menu items',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('menu_icons_admin_settings'),
    'access arguments' => array('administer site configuration'),
    'menu_name' => 'menu_icons',
  );
  return $items;
}

/**
 * Implementation of hook_form_alter().
 */
function menu_icons_form_alter(&$form, $form_state, $form_id) {

  if ($form_id == 'menu_edit_item') {
    $options = unserialize(db_result(db_query('SELECT options FROM {menu_links} WHERE mlid = %d', $form['menu']['mlid']['#value'])));

    $form['icon'] = array(
      '#type' => 'fieldset',
      '#weight' => 5,
      '#title' => t('Menu icon settings'),
      '#description' => t('If checked, the following icon will be used as background image for this menu item.'),
      '#attributes' => array('class' => 'theme-settings-bottom'),
    );
    $form['icon']["icon_logo"] = array(
      '#type' => 'checkbox',
      '#title' => t('Use an icon'),
      '#default_value' => $options['icon']['enable'],
      '#tree' => FALSE,
      '#description' => t('Check this if you want this icon to be used.')
    );
    $form['icon']['icon_path'] = array(
      '#type' => 'textfield',
      '#title' => t('Path to the icon'),
      '#default_value' => $options['icon']['path'],
      '#description' => t('The path to the image you would like to use as a backround image for this menu item.')
    );
    $form['icon']['icon_upload'] = array(
      '#type' => 'file',
      '#title' => t('Upload a new icon image'),
      '#maxlength' => 40,
      '#description' => t("If you don't have direct file access to the server, use this field to upload your icon.")
    );

    $form['submit']['#weight'] = 9;
    $form['delete']['#weight'] = 10;

    $form['#attributes']['enctype'] = 'multipart/form-data';
    $form['#submit'][] = 'menu_icons_form_submit';
  }
}

/**
 * Process the submitted form
 *
 */
function menu_icons_form_submit($form, &$form_state) {

  // Clear the cached css
  cache_clear_all('menu_icons', 'cache');

  // Get the global setings
  $file_validate_image_resolution = variable_get('menu_icons_file_validate_image_resolution', '45x45');
  $image_folder = variable_get('menu_icons_image_folder', 'menu_icons');

  // Check the destination folder, attempt to create it if it does't exist
  $directory_path = file_directory_path() ."/$image_folder";
  file_check_directory($directory_path, FILE_CREATE_DIRECTORY, 'file_directory_path');

  // Store the current icon path
  $path = $form_state['values']['icon_path'];

  // Define the validation settings
  if ($file_validate_image_resolution != '0x0') {
    $validate = array('file_validate_is_image' => array(), 'file_validate_image_resolution' => array($file_validate_image_resolution));
  }
  else {
    $validate = array('file_validate_is_image' => array());
  }

  // Check for a new uploaded logo, and use that instead.
  if ($file = file_save_upload('icon_upload', $validate)) {
    $parts = pathinfo($file->filename);
    $filename = "$directory_path/menu_icon_". $form_state['values']['menu']['mlid'] .'.'. $parts['extension'];

    // The image was saved using file_save_upload() and was added to the
    // files table as a temporary file. We'll make a copy and let the garbage
    // collector delete the original upload.
    file_copy($file, $filename, FILE_EXISTS_REPLACE);
    $path = $filename;
  }

  $options = unserialize(db_result(db_query('SELECT options FROM {menu_links} WHERE mlid = %d', $form_state['values']['menu']['mlid'])));
  $options['icon'] = array('enable' => $form_state['values']['icon_logo'], 'path' => $path);
  db_query('UPDATE {menu_links} SET options = "%s" WHERE mlid = %d', serialize($options), $form_state['values']['menu']['mlid']);
}

/**
 * Implementation of hook_init().
 */
function menu_icons_init() {
  drupal_set_html_head('<link type="text/css" rel="stylesheet" media="all" href="'. url('menu_icons/css') .'" />');
}

/**
 * Build the module settings form
 *
 * @return a form array
 */
function menu_icons_admin_settings() {

  $form['menu_icons_default_icon'] = array(
    '#type' => 'textfield',
    '#title' => t('Icon path'),
    '#default_value' => variable_get('menu_icons_default_icon', drupal_get_path('module', 'menu_icons') .'/images/default_icon.png'),
    '#description' => t('A Drupal path to the icon or image to use as a default.'),
    '#required' => FALSE,
  );
  $form['menu_icons_file_validate_image_resolution'] = array(
    '#type' => 'textfield',
    '#title' => t('Max image resolution'),
    '#default_value' => variable_get('menu_icons_file_validate_image_resolution', '45x45'),
    '#description' => t('The maximum image resolution for the menu-icons. If an uploaded image exceeds this size, the image is resized automatically.'),
    '#required' => FALSE,
  );
  $form['menu_icons_image_folder'] = array(
    '#type' => 'textfield',
    '#title' => t('Icon folder'),
    '#default_value' => variable_get('menu_icons_image_folder', 'menu_icons'),
    '#description' => t('The name of the files directory in which the new uploaded icons will be stored. This folder will be created in the files directory'),
    '#required' => FALSE,
  );
  $form['menu_icons_position'] = array(
    '#type' => 'select',
    '#title' => t('Position'),
    '#default_value' => variable_get('menu_icons_position', 'left'),
    '#options' => array(
      'right' => t('right'),
      'left' => t('left'),
    ),
    '#required' => FALSE,
  );

  // Clear the cached css
  cache_clear_all('menu_icons', 'cache');

  return system_settings_form($form);
}


/**
 * Page callback for generated CSS file
 *
 */
function menu_icons_css() {
  drupal_set_header('Content-Type: text/css; charset=utf-8');
  print _menu_icons_css_generate();
}

/**
 * Build CSS based on menu IDs
 *
 * @return A string with the CSS
 */
function _menu_icons_css_generate() {

  // Check if there's already stored data
  if ($cache = cache_get('menu_icons')) {
    return $cache->data;
  }
  else {
    $result = db_query("SELECT mlid, options FROM {menu_links}");
    $size = array_shift(split('x', variable_get('menu_icons_file_validate_image_resolution', '45x45')));
    $pos = variable_get('menu_icons_position', 'left');

    while ($item = db_fetch_array($result)) {

      $options = unserialize($item['options']);
      if (!empty($options['icon']['path']) && file_exists($options['icon']['path'])) {
        $css .= theme('menu_icons_css_item', $item['mlid'], check_url(url($options['icon']['path'])), $size, $pos);
      }
    }
    cache_set('menu_icons', $css);
    return $css;
  }
}

/**
 * Implementation of hook_theme().
 *
 */
function menu_icons_theme() {
  return array(
    'menu_icons_css_item' => array(
      'arguments' => array('mlid' => NULL, 'path' => NULL, 'size' => NULL, 'pos' => NULL),
      'template' => 'menu_icons_css_item',
    ),
  );
}

/**
 * Implementation of hook_cron().
 *
 */
function menu_icons_cron() {
  // FIXME - delete icons for menu items that no longer exist
}

/**
 * Implementation of theme_registry_alter().
 */
function menu_icons_theme_registry_alter(&$theme_registry) {
  $theme_registry['menu_item_link']['function'] = '_menu_icons_menu_item_link';
}

function _menu_icons_menu_item_link($link) {
  if ($link['options']['icon']['enable'] && file_exists($link['options']['icon']['path'])) {
    $link['localized_options']['html'] = TRUE;
    $link['localized_options']['attributes']['class'] = "menu_icon menu-". $link['mlid'];
    return l($link['title'], $link['href'], $link['localized_options']);
  }
  else {
    return l($link['title'], $link['href'], $link['localized_options']);
  }
}
