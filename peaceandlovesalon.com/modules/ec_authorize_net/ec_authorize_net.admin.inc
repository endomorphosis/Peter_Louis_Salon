<?php
// $Id: ec_authorize_net.admin.inc,v 1.1 2009/02/03 05:30:16 gordon Exp $
/**
 * @file
 * Provide integration with Authorize.net payment gateway.
 */

/**
 * Authorize.net Settings Page
 */
function ec_authorize_net_settings() {
  $form = array();

  $form['ec_authorize_net_login'] = array(
    '#type' => 'textfield',
    '#title' => t('Login ID'),
    '#default_value' => variable_get('ec_authorize_net_login', ''),
    '#size' => 70,
    '#maxlength' => 180,
    '#description' => t("Enter your merchant login ID."),
    '#required' => TRUE,
  );

  $form['ec_authorize_net_tran_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Transaction key'),
    '#default_value' => variable_get('ec_authorize_net_tran_key', ''),
    '#size' => 70,
    '#maxlength' => 180,
    '#description' => t("Enter your merchant transaction key."),
    '#required' => TRUE,
  );

  $form['ec_authorize_net_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Authorize.net processing URL'),
    '#default_value' => variable_get('ec_authorize_net_url', 'https://secure.authorize.net/gateway/transact.dll'),
    '#size' => 70,
    '#maxlength' => 180,
    '#description' => t('URL of the secure payment processing page.'),
  );

  $form['ec_authorize_net_debug'] = array(
    '#type' => 'radios',
    '#title' => t('Authorize.net test mode'),
    '#default_value' => variable_get('ec_authorize_net_debug', 0),
    '#options' => array(t('Disabled'), t('Enabled')),
    '#description' => t('If enabled, transactions will be sent in test mode and cards will not be charged.'),
  );

  $form['ec_authorize_net_email_customer'] = array(
    '#type' => 'radios',
    '#title' => t('Email Authorize.net Reciept'),
    '#default_value' => variable_get('ec_authorize_net_email_customer', 1),
    '#options' => array(t('Disabled'), t('Enabled')),
    '#description' => t('If enabled, the customer will recieve a payment confirmation email from Authorize.Net. Keep in mind the ecommerce package sends it own transaction summary as well. Enabling this option is recommended because it provides the customer with an accurate confirmation of the amount you have charged.'),
  );

  return system_settings_form($form);
}

