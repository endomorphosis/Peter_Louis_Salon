<?php
/*
** $Id: index.php,v 1.6 2003/07/18 18:15:37 damonp Exp $
**
** Copyright (C) 2002 siliconsys.com <dev@siliconsys.com>
** See LICENSE file in root directory of this installation for
** licensing information.
*/

	define('ADMIN', true);
	define('SECTION', 'catalog');

	require_once('../include/config.inc');

	set_default_null($function, 'list');
	set_default_null($default_page, 'admin/tmpl.default.inc');
	set_default_null($cmd_gen_thumb, "gen_thumbnail(\$image_file, \$form);");
	set_default_null($thumb_scale_form, "<td colspan=\"2\">Scale:<img src=\"{base}/image/pixel_white.png\" height=\"1\" width=\"5\"><input type=\"text\" name=\"thumb_scale\" value=\"50\" size=\"2\" class=\"input\">&nbsp;%</td>\n");
	$T->set_file('menu_box_content', $CFG->menu);

/*************************************
 * main
 ************************************/

	switch($function) {

/****************
 * add
 ***************/
		case('add'):
			if($form['submit'] == 'insert') {
				$form = process_db_input($form);
				$form['product_price'] = str_replace(array(',', ' '), array('', ''), $form['product_price']);

				$CFG->dbh->Query("INSERT
										INTO $CFG->phpc_table
											(product_t_stamp, product_sku, product_name,
											product_color, product_size, product_price,
											product_desc, product_use_tag)
										VALUES('" . T_STAMP . "', '$form[product_sku]', '$form[product_name]',
											'$form[product_color]', '$form[product_size]', '$form[product_price]',
											'$form[product_desc]', '$form[product_use_tag]')",
										__FILE__, __LINE__);
				if($product_id = $CFG->dbh->InsertID('phpc_' . $_SESSION['config']['catalog_name'], 'product_id')) {
					push_msg('product: <b>' . stripslashes($form['product_name']) . '</b> added.', 'info', $CFG->msg);
					$form['id'] = $product_id;
					update_categories($form);
					$image_file = upload_image('full_image', $form);
					if($form['gen_thumb'] == 'yes') $thumb_name = eval($cmd_gen_thumb);
					else if($_FILES['thumb_image']['tmp_name'] != 'none' && $_FILES['thumb_image']['tmp_name'] != '') {
						$thumb_image = upload_image('thumb_image', $form);
					}
					display_item('verify', 'add', 'get', $product_id, $sel_category);
				}  else {
					$T->set_file('content', 'admin/tmpl.verify.inc');
					push_msg('error inserting product.', 'warning', $CFG->msg);
				}
			}	else {
				$T->set_file('content', 'admin/tmpl.form.inc');
				require($CFG->libdir . '/class/class.File_upload.inc');
				$upload = new File_upload;
				$T->set_var(array(
								'full_input'	=> $upload->UploadForm('full_image', $CFG->max_img_size),
								'thumb_input'	=> $upload->UploadForm('thumb_image', $CFG->max_img_size),
								'thumb_scale_form' => $thumb_scale_form,
								'select_category_input'	=> select_category($sel_category, false, 'select_category_input'),
								'verify_js'		=> '<script src="{base}/admin/jscript/add.js" language="Javascript1.1"></script>',
								'button'			=> 'insert',
								'function'		=> 'add',
								'radio_OPEN'	=> 'checked',
								'max_price'		=> $CFG->max_price));
			}
			break;

/****************
 * delete
 ***************/
		case('delete'):
			if($form['submit'] == 'delete') {
				$CFG->dbh->Query("SELECT *
										FROM $CFG->phpc_table
										WHERE product_id = '$form[id]'");
				$r = $CFG->dbh->FetchObject();
				$thumb_path = $CFG->imagedir . '/products/' . $r->thumb_path;
				$image_path = $CFG->imagedir . '/products/' . $r->image_path;
				if(file_exists($thumb_path) && $thumb_path != 'not_available.png')
					if(@unlink($thumb_path))
						push_msg("Image: <b>$r->thumb_path</b> deleted.", 'warning', $CFG->msg);
				if(file_exists($image_path) && $image_path != 'not_available.png')
					if(@unlink($image_path))
						push_msg("Image: <b>$r->image_path</b> deleted.", 'warning', $CFG->msg);
				$CFG->dbh->Query("DELETE
										FROM $CFG->phpc_table
										WHERE product_id = '$form[id]'");
				push_msg('product: <b>' . stripslashes($form['product_name']) . '</b> deleted.', 'info', $CFG->msg);
				catalog_list($sort, $sel_category, $query, $start);
			}	else display_item('verify', $function, 'get', $id, $sel_category);
			break;

/****************
 * edit
 ***************/
		case('edit'):
			if($form['submit'] == 'save') {
				if($_FILES['full_image']['tmp_name'] != 'none' && $_FILES['full_image']['tmp_name'] != '') $image_file = upload_image('full_image', $form);
				if($form['gen_thumb'] == 'yes') $thumb_name = eval($cmd_gen_thumb);
 				else if($_FILES['thumb_image']['tmp_name'] != 'none' && $_FILES['thumb_image']['tmp_name'] != '') {
					$thumb_image = upload_image('thumb_image', $form);
				}

				$form = process_db_input($form);
				$form['product_price'] = str_replace(array(',', ' '), array('', ''), $form['product_price']);

				$CFG->dbh->Query("UPDATE $CFG->phpc_table
										SET
											product_t_stamp	= '" . T_STAMP . "',
											product_sku			= '$form[product_sku]',
											product_name		= '$form[product_name]',
										product_color		= '$form[product_color]',
											product_size		= '$form[product_size]',
											product_price		= '$form[product_price]',
											product_use_tag	= '$form[product_use_tag]',
											product_desc		= '$form[product_desc]'
										WHERE product_id = $form[id]",
										__FILE__, __LINE__);
				push_msg('product: <b>' . stripslashes($form['product_name']) . '</b> updated.', 'info', $CFG->msg);
				update_categories($form);
				catalog_list($sort, $sel_category, $query, $start);
			}	else {
				$T->set_var(array(
								'button'		=> 'save',
								'function'	=> 'edit',
								'max_price'	=> $CFG->max_price,
								'thumb_scale_form' => $thumb_scale_form,
								'verify_js'	=> '<script src="{base}/admin/jscript/edit.js" language="Javascript1.1"></script>'));
				display_item('edit', 'edit', 'get', $id, $sel_category);
			}
			break;

/****************
 * error
 ***************/
		case('error'):
			$T->set_var('error_msg', $_SESSION['LOGIN']['error_msg']);
			$T->set_file('content', 'admin/tmpl.error.inc');
			break;

/****************
 * list
 ***************/
		case('list'):
			catalog_list($sort, $sel_category, $query, $start);
			break;

/****************
 * search
 ***************/
		case('search'):
			catalog_list($sort, $sel_category, $query);
			break;

/****************
 * view
 ***************/
		case('view'):
			display_item('view', '', 'get', $id, $sel_category);
			break;

/****************
 * default
 ***************/
		default:
			$T->set_file('content', $default_page);
			break;
	}

	push_template();
?>