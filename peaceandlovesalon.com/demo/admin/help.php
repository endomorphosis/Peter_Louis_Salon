<?php
/*
** $Id: help.php,v 1.2 2003/07/18 18:15:37 damonp Exp $
**
** Copyright (C) 2002 siliconsys.com <dev@siliconsys.com>
** See LICENSE file in root directory of this installation for
** licensing information.
*/

	define('ADMIN', true);
	define('SECTION', 'help');

	require_once('../include/config.inc');

	$section	= passed_var('section');

	$T->set_file('page', 'admin/tmpl.help_page.inc');
	$T->set_var('pagetitle', 'PHPCatalog Help');

/*************************************
 * main
 ************************************/

	$T->set_var(array(
							'help_function'	=> $function,
							'help_section'		=> $section));

	if($method != 'admin') {
		$section = $section == '' ? 'welcome':$section;
		$T->set_file('content', 'admin/tmpl.help.inc');
		$CFG->dbh->Query("SELECT *
								FROM phpcatalog_help
								WHERE help_section = '$section'
									AND help_function = '$function'");
		if($CFG->dbh->RowCount > 0) {
			$r = $CFG->dbh->FetchObject();
			$T->set_var(array(
								'help_section'	=> $r->help_section,
								'help_function'=> $r->help_function,
								'help_text'		=> nl2br($r->help_text)));
		}	else {
			$CFG->dbh->Query("SELECT *
									FROM phpcatalog_help
									WHERE help_function = 'all'
											AND help_section = '$section'");
			if($CFG->dbh->RowCount > 0) {
				$r = $CFG->dbh->FetchObject();
				$T->set_var(array(
									'help_section'	=> $r->help_section,
									'help_function'=> $r->help_function,
									'help_text'		=> $r->help_text));
			}	elseif($section == 'index') {
				$T->set_file('content', 'admin/tmpl.help_index.inc');
				help_index('content');
			}	else {
				$T->set_var('help_text', 'There is no help associated with this function');
			}
		}
	}	 else {
		$T->set_file('content', 'admin/tmpl.help_form.inc');
		$T->set_file('help_index', 'admin/tmpl.help_index.inc');
		if($form['submit']) {
			$CFG->dbh->Query("SELECT *
									FROM phpcatalog_help
									WHERE help_function = '$form[help_function]'
										AND help_section = '$form[help_section]'");
			$r = $CFG->dbh->FetchObject();
			$help_id = $r->help_id;
			if($CFG->dbh->RowCount > 0) {
				$CFG->dbh->Query("UPDATE phpcatalog_help
										SET
											help_section 	= '$form[help_section]',
											help_function 	= '$form[help_function]',
											help_text 		= '$form[help_text]'
										WHERE help_id = $help_id");
				push_msg("help function $form[help_section].$form[help_function] updated.", 'info', $CFG->msg);
			}	else {
				$CFG->dbh->Query("INSERT
										INTO phpcatalog_help
											(help_section, help_function, help_text)
										VALUES
											('$form[help_section]', '$form[help_function]',
											'$form[help_text]')");
				$help_id = $CFG->dbh->InsertID(phpcatalog_help, 'help_id');
				push_msg("help function $form[help_section].$form[help_function] inserted.", 'info', $CFG->msg);
			}
			$CFG->dbh->Query("SELECT *
									FROM phpcatalog_help
									WHERE help_id = $help_id");
			$r = $CFG->dbh->FetchObject();
			$T->set_var(array(
									'help_function'	=> $r->help_function,
									'help_section'		=> $r->help_section,
									'help_text'			=> nl2br($r->help_text),
									'help_text_input'	=> htmlspecialchars($r->help_text),
									'h_method'			=> '&method=admin'));
		} 	else {
			$CFG->dbh->Query("SELECT *
									FROM phpcatalog_help
									WHERE help_function = '$function'
										AND help_section = '$section'");
			$r = $CFG->dbh->FetchObject();
			$T->set_var(array(
									'help_function'	=> $r->help_function,
									'help_section'		=> $r->help_section,
									'help_text'			=> nl2br($r->help_text),
									'help_text_input'	=> htmlspecialchars($r->help_text),
									'h_method'			=> '&method=admin'));
		}
		help_index('help_index');
	}
	$T->parse('content', 'content');
	push_template();

/***********************************
**	functions
***********************************/

	function help_index($page) {
		Global $CFG, $T;

		$CFG->dbh->Query("SELECT *
								FROM phpcatalog_help
								ORDER BY help_section, help_function");

		$T->set_block($page, 'help_row', 'help_rows');
		while($r = $CFG->dbh->FetchObject()) {
			$row_color = $row_color == $CFG->color_tbl1 ? $CFG->color_tbl2:$CFG->color_tbl1;
			$T->set_var(array(
								'row_color'	=> $row_color,
								'h_id'		=> $r->help_id,
								'h_function'=> $r->help_function,
								'h_section'	=> $r->help_section));
			$T->parse('help_rows', 'help_row', TRUE);
		}
		array_push($CFG->page_arr, $page);
	}
?>