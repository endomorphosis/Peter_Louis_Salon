<?php
/*
**	$Id: index.php,v 1.3 2003/09/16 20:31:48 damonp Exp $
**
** Copyright (C) 2002 siliconsys.com <dev@siliconsys.com>
** See LICENSE file in root directory of this installation for
** licensing information.
*/

	require_once('./include/config.inc');

 	$cat_id 			= passed_var('cat_id', 'get', 'integer');
 	$catalog_id		= passed_var('catalog_id', 'both', 'integer');

	$category = isset($sel_category) ? $sel_category:'%';
	$_SESSION['category'] = $category;
	$d_category = $category == '%' ? 'All Categories':$category;
	$cat_id = $sel_category == '' ? $cat_id:$sel_category;

	if(isset($cat_id)) $cat = get_category_name($cat_id);
	set_default($CFG->tmpl_header, $CFG->libdir . '/template/header.inc');
	set_default($CFG->tmpl_footer, $CFG->libdir . '/template/footer.inc');
	set_default($CFG->tmpl_prev_next, $CFG->libdir . '/template/tmpl.prev_next.inc');
	set_default($CFG->tmpl_box, 'tmpl.box.inc');
	set_default($CFG->tmpl_detail, 'tmpl.detail.inc');
	set_default($CFG->tmpl_image_view, 'tmpl.image_view.inc');
	set_default($CFG->tmpl_list, 'tmpl.list.inc');
	set_default($CFG->tmpl_page, 'tmpl.page.inc');

	$T->set_file('page', $CFG->tmpl_page);

	$CFG->color_tbl1	= '#cccccc';
	$CFG->color_tbl2	= '#eeeeee';

	$T->set_var(array(
							  	'me'			=>	me(),
								'base'		=> $CFG->wwwroot,
								'currency'	=> $_SESSION['config']['currency_sym'],
								'category'	=> $d_category,
								'select_category'	=> select_category($cat_id, true)));

	if($id != '' && $function == '') $function = 'detail';
	elseif(($cat_id != '' || $start != '' || $query != '') && $function == '') $function = 'list';

	if($_SESSION['config']['catalog_name'] == '')	throw_error('internal configuration error, rebuild config table');

/*****************************
 * main
 ****************************/
	include($CFG->tmpl_header);
	switch($function) {
/****************
 * detail
 ***************/
		case('detail'):
			$CFG->dbh->Query("SELECT phpc.*, cat.category_name AS product_category
									FROM $CFG->phpc_table AS phpc
									LEFT JOIN phpcatalog_categories AS cat USING (category_id)
									WHERE product_id = '$id'");
			if($CFG->dbh->RowCount > 0) {
				$T->set_file('content', $CFG->tmpl_detail);
				$r	= $CFG->dbh->FetchObject('', 1);
				format_product_row($T, $r, $function);
				$T->set_var(array(
										'bottom'	=> "<tr height=\"70\"><td colspan=\"2\" align=\"center\" valign=\"bottom\"><a href=\"javascript:history.go(-1)\" class=\"bold\">back</a></td></tr>\n",
										'select_category'	=> select_category($r->category_id, true)));
				stats_log_hit($id);
			}	else {
				$T->set_var('content', 'Item not found');
			}
			break;
/****************
 * image_view
 ***************/
		case('image_view'):
			$T->set_file('content', $CFG->tmpl_image_view);
			$T->set_var('bottom', "<tr height=\"70\"><td colspan=\"2\" align=\"center\" valign=\"bottom\"><a href=\"javascript:history.go(-1)\" class=\"bold\">back</a></td></tr>\n");
			$CFG->dbh->Query("SELECT image_path
									FROM $CFG->phpc_table
									WHERE product_id = '$id'");
			$r = $CFG->dbh->FetchObject();
			$T->set_var('image_path', $r->image_path);
			break;
/****************
 * list
 ***************/
		case('list'):
			$T->set_file('content', $CFG->tmpl_list);
			catalog_list($query, $start, $cat_id);

			if($show_msg) {
				$T->set_var('msg', $msg);
				$T->parse('msg', 'msg');
			}
			$T->set_var('pagetitle', $d_category);
			break;
/****************
 * index
 ***************/
		default:
			$T->set_file('page', $CFG->tmpl_page);
			$T->set_file('content', 'tmpl.index.inc');
			$T->set_var('pagetitle', '');
			break;
	}

	push_template();
	include($CFG->tmpl_footer);

/*****************************
 * functions
 ****************************/
	function catalog_list($query, $start, $cat_id) {
		global $CFG, $T;

		set_default($start, 0);
		$num_items_page = $_SESSION['config']['num_items_page'] != '' ? $_SESSION['config']['num_items_page']:10;
		$db_cat_id = $cat_id == '' ? '%':$cat_id;
		if($query != '') {
			$msg = "<tr><td colspan=\"6\" height=\"30\" valign=\"top\">Searching for &lt;<b>$query</b>&gt; returned ";
			$search_string = '%' . $query . '%';
			$like 		= $CFG->dbh->Ident() == "DBI_PG" ? "ILIKE":"LIKE";
			$db_query 	= "AND (product_name $like '$search_string'";
			$db_query 	.= " OR product_sku $like '$search_string'";
			$db_query 	.= " OR product_desc $like '$search_string')";
			$show_msg 	= true;
			$db_limit 	= '';
		}	else {
			$db_limit = ($CFG->dbh->Ident() == "DBI_PG") ? $num_items_page. ", $start":"$start, " . $num_items_page;
			$db_limit = "LIMIT $db_limit";
			$db_query = '';
		}

		$CFG->dbh->Query("SELECT count(product_id) as num_rows
								FROM $CFG->phpc_table as phpc
								LEFT JOIN phpcatalog_categories as cat USING(category_id)
								WHERE cat.catalog_id = $CFG->catalog_id
									AND phpc.category_id LIKE '$db_cat_id'
									$db_query
								ORDER	BY	product_name ASC",
								__FILE__, __LINE__);

		$r = $CFG->dbh->FetchObject('', 1);
		$num_items	= $r->num_rows;
		set_default_null($num_items, 0);
		$num_pages = ceil($num_items / $num_items_page);

		if(isset($msg)) {
			$product = $num_items > 1 ? 'products':'product';
			$msg .= "$num_items $product.</td></tr>";
		}

		$T->set_block('content', 'row', 'rows');
		if($num_items > 0) {
			$CFG->dbh->Query("SELECT *
									FROM $CFG->phpc_table as phpc
									LEFT JOIN phpcatalog_categories as cat USING(category_id)
									WHERE cat.catalog_id = $CFG->catalog_id
										AND phpc.category_id LIKE '$db_cat_id'
										$db_query
									ORDER	BY	product_name ASC
									$db_limit",
									__FILE__, __LINE__);

			while($r	= $CFG->dbh->FetchObject()) {
				format_product_row($T, $r, 'list');
				$T->parse('rows', 'row', true);
			}
			if(isset($msg))
				$T->set_var('search_msg', $msg);
			if($num_items > $num_items_page && $query == '')
				show_prev_next($num_items, $num_pages, $start, $db_cat_id);
		}	else {
			$T->set_var('rows', '<tr><td align="center">No Products Found</td></tr>');
			$T->implodeBlock('row');
		}
	}


	function show_prev_next($num_items, $num_pages, $start, $category) {
		global $CFG, $T;

		$T->set_file('bottom', $CFG->tmpl_prev_next);
		$category = ($category == '' || $category == '%') ? '':"cat_id=$category&";
		$num_items_page = $_SESSION['config']['num_items_page'] != '' ? $_SESSION['config']['num_items_page']:10;
		if($start >	0) {
			$prev = "<a href=\"./?".$category."start=" . ($start - $num_items_page) . "\">&lt;prev</a> | ";
		}	else $prev = "&lt;prev | ";
		$i	= 0;
		while($i	< $num_pages) {
			$page	= $i * $num_items_page;
			$i++;
			if($page	!=	$start) {
				$pages .= "<a href=\"./?".$category."start=$page\">$i</a> | \n";
			}	else $pages .= "<b>$i</b> | \n";
		}
		if($start <	(($num_pages - 1) * $num_items_page)) {
			$next .= "<a href=\"./?".$category."start=" . ($start + $num_items_page) . "\">next&gt;</a>";
		}	else $next .= "next&gt;";
		$T->set_var(array(
							'prev' => $prev,
							'next' => $next,
							'pages'=> $pages));
		array_push($CFG->page_arr, 'bottom');
	}
?>