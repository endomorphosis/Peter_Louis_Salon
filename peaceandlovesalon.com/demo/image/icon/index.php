<?php
/*
** $Id: index.php,v 1.1 2003/07/03 16:01:21 damonp Exp $
**
** Copyright (C) 2002 siliconsys.com <dev@siliconsys.com>
** See LICENSE file in root directory of this installation for
** licensing information.
*/

	define('NO_PROCESS', true);
	if(file_exists('./config.inc'))	{
		include('./config.inc');
		header("Location: $CFG->wwwroot");
	}	else	header("Location: ../");
?>