<?php
/*
**	$Id: phpcatalog_admin.inc,v 1.11 2003/09/16 20:31:48 damonp Exp $
**
** Copyright (C) 2002 siliconsys.com <dev@siliconsys.com>
** See LICENSE file in root directory of this installation for
** licensing information.
*/

/***********************************
**	variables
***********************************/
	set_default($CFG->bottom, date(LONG_TIME, T_STAMP));
	set_default($CFG->menu, 'admin/tmpl.menu_' . SECTION . '.inc');

	$CFG->color_tbl1	= '#cccccc';
	$CFG->color_tbl2	= '#eeeeee';
	$CFG->color_hilite= '#ccffcc';
	$CFG->color_bg		= '#999999';
	$CFG->color_label	= '#bbbbbb';
	$CFG->color_select= '#ffcc99';

	/* some local error checks */
	if(SECTION == 'catalog' || SECTION == 'system' || SECTION == 'config')	{
		if(file_exists($CFG->dirroot . '/admin/setup.php')) local_error('please delete file: <b>' . $CFG->dirroot . '/admin/setup.php</b> for production use (or move out of web server space)</b>');
		if(file_exists($CFG->dirroot . '/admin/') && ! file_exists($CFG->dirroot . '/admin/.htaccess')) local_error('admin directory: <b>' . $CFG->dirroot . '/admin/</b> appears to be publicly accessible. <br />See: <b>http://httpd.apache.org/docs-2.0/howto/auth.html</b> for information on protecting this directory, or contact your hosting provider.');
		if(! ini_get('file_uploads'))	local_error('file_uploads is turned off in your PHP settings, image uploads will not be possible. please check your server configuration.' .
														'see: <a href="http://www.php.net/manual/en/configuration.directives.php#ini.file-uploads">http://www.php.net/manual/en/configuration.directives.php#ini.file-uploads</a> for more info');
		if(! is_writeable($CFG->prod_dir))	local_error($CFG->prod_dir . ' is not writeable, image uploads will not be possible. please check your server configuration');
		if($_SESSION['config']['catalog_name'] == '')	throw_error('internal configuration error, rebuild config table');
	}


	$T->set_root($CFG->libdir . '/template/', __FILE__, __LINE__);
	$T->set_file('page', 'admin/tmpl.page.inc');

	switch(SECTION) {
		case('catalog'):
			break;
		case('config'):
			$CFG->menu_config['config'] = "<a class=\"list\" href=\"{me}?function=config\">config</a>";
			break;
	}

	if(SECTION != 'help') {
		$T->set_file('menu', 'admin/tmpl.box.inc');
		array_push($CFG->page_arr, 'menu');
		array_push($CFG->page_arr, 'menu_box_content');
	}

	$locbar = '[loc: ' . $_SERVER['HTTP_HOST'] . ' | db: ' . $CFG->dbname . ']';
	$currency_loc = $_SESSION['config']['currency_location'] == 'after' ? 'currency_after':'currency_before';
	$T->set_var(array(
								'pagetitle'    => 'PHPCatalog Admin - ' . $locbar,
								'appversion'	=> $CFG->appversion,
								'locbar'			=> $locbar,
								'me'           => me(),
								'cur_table'		=> $_SESSION['config']['catalog_name'],
								'currency'		=> $_SESSION['config']['currency_sym'],
								$currency_loc	=> '{currency}',
								'base' 			=> $CFG->wwwroot,
								'bottom'			=> $CFG->bottom,
								'menu_box_name'=> SECTION,
								'category'		=> $category,
								'sel_category'	=> $sel_category,
								'catalog_id'	=> $CFG->catalog_id,
								'id'				=> $id,
								'section'		=> SECTION,
								'function'     => $function,
								'action'       => $action,
								'sort'         => $sort,
								'color_bg'		=> $CFG->color_bg,
								'color_tbl1'	=> $CFG->color_tbl1,
								'color_tbl2'	=> $CFG->color_tbl2,
								'color_hilite'	=> $CFG->color_hilite,
								'color_label'	=> $CFG->color_label,
								'color_select'	=> $CFG->color_select));

/***********************************
**	functions
***********************************/

	function display_item($action, $function, $db_action, $product_id='%', $sel_category='%') {
		global $CFG, $T, $form, $display;

		if($db_action == 'get') {
			$CFG->dbh->Query("SELECT phpc.*, category_name as product_category
									FROM $CFG->phpc_table as phpc
									LEFT JOIN phpcatalog_categories as cat ON phpc.category_id = cat.category_id
									WHERE product_id = $product_id
										AND cat.catalog_id = $CFG->catalog_id",
									__FILE__, __LINE__);

			if($CFG->dbh->RowCount < 1) throw_error('item: <b>' . $product_id . '</b> not found in db');

			$r = $CFG->dbh->FetchObject('', 1);
			format_product_row($T, $r, $action);
			$T->set_var(array(
									'select_category_input'=> select_category($r->category_id, false, 'select_category_input')));
		}

		switch($action) {
			case('edit'):
				$T->set_file('content', 'admin/tmpl.form.inc');
				require($CFG->libdir . '/class/class.File_upload.inc');
				$upload = new File_upload;
				$image_path = $r->image_path == 'not_available.png' ? 'not_available.png':$CFG->prod_dir . $r->image_path;
				$thumb_path = $r->thumb_path == 'not_available.png' ? 'not_available.png':$CFG->prod_dir . $r->thumb_path;

				$image_size = @getimagesize($image_path);
				$img_wd		= $image_size[0] < 250 ? 300:$image_size[0] + 50;
				$img_ht		= $image_size[1] < 125 ? 200:$image_size[1] + 75;
				$thumb_size = @getimagesize($thumb_path);
				$thumb_wd	= $thumb_size[0] < 250 ? 300:$thumb_size[0] + 50;
				$thumb_ht	= $thumb_size[1] < 125 ? 200:$thumb_size[1] + 75;
				$T->set_var(array(
									'img_ht'			=> $img_ht,
									'img_wd'			=> $img_wd,
									'thumb_ht'		=> $thumb_ht,
									'thumb_wd'		=> $thumb_wd,
									'product_price'=> $r->product_price,
									'full_input'	=>	$upload->UploadForm('full_image', $CFG->max_img_size),
									'thumb_input' 	=> $upload->UploadForm('thumb_image', $CFG->max_img_size)));
				break;

			case('verify'):
				$T->set_file('content', 'admin/tmpl.view.inc');
				switch($function) {
					case('add'):
						$T->set_var(array(
										'button' 		=> "<td colspan=\"4\"><a href=\"{me}?function=add&sel_category=$sel_category\" class=\"label\">add another record</a></td>\n"));
						break;

					case('delete'):
						$T->set_var(array(
										'form_header' 	=> "<form action=\"index.php\" method=\"post\">\n",
										'form_hidden'	=> "<input type=\"hidden\" name=\"id\" value=\"$r->product_id\">
																<input type=\"hidden\" name=\"function\" value=\"delete\">
																<input type=\"hidden\" name=\"sel_category\" value=\"$sel_category\">
																<input type=\"hidden\" name=\"product_name\" value=\"$r->product_name\">",
										'button' 		=> "<td colspan=\"4\" height=\"70\" align=\"center\" valign=\"bottom\"><input type=\"submit\" name=\"submit\"value=\"delete\" class=\"input\"></td>\n",
										'form_footer' 	=> "</form>"));
						break;
				}
				break;

			case('view'):
				$T->set_file('content', 'admin/tmpl.view.inc');
				$T->set_var(array(
									'product_price'=> $r->product_price,
									'button' 		=> "<td height=\"70\" colspan=\"4\" align=\"center\" valign=\"bottom\"><a href=\"javascript:history.go(-1);\" class=\"admin\">back</a></td>\n"));
				break;

		}
		return $r;
	}

	function get_category_info($id, $tmpl=true) {
		global $CFG, $T;

		$id = $id == '%' ? 1:$id;
		if(!is_numeric($id)) return false;
		$CFG->dbh->Query("SELECT *
								FROM phpcatalog_categories
								WHERE category_id LIKE $id",
								__FILE__, __LINE__);
		if($CFG->dbh->RowCount < 1) {
			throw_error("<b>Top Level</b> category is missing, please fix manually.");
		}	else $r = $CFG->dbh->FetchObject('', 1);
		if($tmpl) {
			$T->set_var(array(
								'category_id'		=> $id,
								'category_name'	=> $r->category_name,
								'parent_id'			=> $r->parent_id));
		}	else $T->set_var('parent_id', $id);
		$return_ob = $r;
		$parent_id = $r->parent_id != '' ? $r->parent_id:0;
		$CFG->dbh->Query("SELECT *
								FROM phpcatalog_categories
								WHERE category_id = $parent_id",
								__FILE__, __LINE__);
		$r = $CFG->dbh->FetchObject('', 1);
		$T->set_var('parent_name', $r->category_name);
		return $return_ob;
	}

	function update_categories($form) {
		global $CFG;

		if(! $form['product_category'] == '') {
			$product_category = trim($form['product_category']);
			$CFG->dbh->Query("SELECT category_id
									FROM phpcatalog_categories
									WHERE category_name = '$product_category'
										AND catalog_id = $CFG->catalog_id",
									__FILE__, __LINE__);

			if($CFG->dbh->RowCount < 1) {
				if($form['select_category_input'] != '%') $parent_id = $form['select_category_input'];
				else {
					$CFG->dbh->Query("SELECT MIN(category_id) AS min_category_id
											FROM phpcatalog_categories
											WHERE catalog_id = $CFG->catalog_id",
										__FILE__, __LINE__);
					$r = $CFG->dbh->FetchObject('', 1);
					$parent_id = $r->min_category_id;
				}

				$CFG->dbh->Query("INSERT
										INTO phpcatalog_categories
											(parent_id, catalog_id, category_name)
										VALUES ('$parent_id', '$CFG->catalog_id', '$form[product_category]')",
									__FILE__, __LINE__);
				$category = $form['product_category'];
				push_msg('category: <b>' . $category . '</b> added', 'info', $CFG->msg);
				$category_id = $CFG->dbh->InsertID('phpcatalog_categories', 'category_id');
			}	else {
				push_msg('category: <b>' . $form['product_category'] . '</b> exists', 'info', $CFG->msg);
				$r = $CFG->dbh->FetchObject('', 1);
				$category_id = $r->category_id;
			}
		}	else $category_id = $form['select_category_input'];

		$CFG->dbh->Query("UPDATE $CFG->phpc_table
								SET category_id = $category_id
								WHERE product_id = $form[id]",
								__FILE__, __LINE__);
		return true;
	}

	function catalog_list($sort, $category, $query='', $start=0) {
		global $CFG, $T;

		list($sort_order, $dir) = explode('|', $sort);
		$db_sort_order = $sort_order == '' ? 'product_id':'product_' . $sort_order;
		set_default_null($dir, 'ASC');
		if($dir == 'ASC')	{
			$db_dir = 'DESC';
			$dis_dir= ' \\/';
		}	else {
			$db_dir = 'ASC';
			$dis_dir= ' /\\';
		}

		$db_cat_id = $category == '' ? '%':$category;
		$num_items_page = $CFG->admin_items_page;
		set_default_null($num_items_page, 20);
		set_default_null($start, 0);

		$like = $CFG->dbh->Ident() == 'DBI_PG' ? 'ILIKE':'LIKE';
		if($query != '') {
			$msg = 'Searching for: <b>' . $query . '</b> returned ';
			$search_string = '%' . $query . '%';
			$db_query = "AND (product_name $like '$search_string'";
			$db_query .= " OR product_sku $like '$search_string'";
			$db_query .= " OR product_id $like '$query'";
			$db_query .= " OR product_desc $like '$search_string')";
			$T->set_var('query', $query);
		}	else	{
			$db_query = '';
			$db_limit = ($CFG->dbh->Ident() == 'DBI_PG') ? $num_items_page . ', ' .$start:$start . ', ' . $num_items_page;
			$db_limit = 'LIMIT ' .$db_limit;
		}

		$CFG->dbh->Query("SELECT count(product_id) as num_items
								FROM	$CFG->phpc_table as phpc
								LEFT JOIN phpcatalog_categories as cat ON phpc.category_id = cat.category_id
								WHERE phpc.category_id LIKE '$db_cat_id'
									AND cat.catalog_id = $CFG->catalog_id
									$db_query",
								__FILE__, __LINE__);

		$r				= $CFG->dbh->FetchObject('', 1);
		$num_items	= $r->num_items;
		$num_pages	= ceil($num_items / $num_items_page);
		if($num_items == 0)	{
			$start = -1;
			$end = $num_items;
		}	else	if($query == '')	{
			$end	= $num_items % $num_items_page;
			$end	= ($start + $num_items_page) > $num_items ? ($start + $end):($start + $num_items_page);
		}	else $end = $num_items;

		$T->set_var(array(
								'l_start'					=> $start + 1,
								'l_end'						=> $end,
								'l_total'					=> $num_items,
								's_extra_vars'				=> extra_vars(array('sort', 'start')),
								'pn_extra_vars'			=> extra_vars('start'),
								'product_id_label'		=> 'label',
								'product_sku_label'		=> 'label',
								'product_category_label'=> 'label',
								'product_name_label'		=> 'label',
								'product_price_label'	=> 'label',
								'product_id_dir'			=> $dir,
								'product_sku_dir'			=> $dir,
								'product_category_dir'	=> $dir,
								'product_name_dir'		=> $dir,
								'product_price_dir'		=> $dir,
								$db_sort_order . '_label' 		=> 'selectlabel',
								$db_sort_order . '_dir_disp'	=> $dis_dir,
								$db_sort_order . '_dir'			=> $db_dir,
								'sort' 								=> urlencode($sort_order),
								's_category' 						=> urlencode($category)));

		$CFG->dbh->Query("SELECT product_id, product_name, product_price,
									product_sku, category_name as product_category
								FROM	$CFG->phpc_table as phpc
								LEFT JOIN phpcatalog_categories as cat ON phpc.category_id = cat.category_id
								WHERE phpc.category_id LIKE '$db_cat_id'
									AND cat.catalog_id = $CFG->catalog_id
									$db_query
								ORDER	BY	$db_sort_order $db_dir
								$db_limit",
								__FILE__, __LINE__);

		set_default_null($num_items, $CFG->dbh->RowCount);
		if(isset($msg))	$msg .= $num_items . ' product(s) ';

		$T->set_file('content', 'admin/tmpl.list.inc');
		$T->set_block('content', 'row', 'rows');

		if($CFG->dbh->RowCount > 0) {
			while($r = $CFG->dbh->FetchObject()) {
				$row_color = $row_color == $CFG->color_tbl2 ? $CFG->color_tbl1:$CFG->color_tbl2;
				if(strlen($r->product_category) > 15)
					$r->product_category = substr($r->product_category, 0, 15) . '..';
				format_product_row($T, $r, $function);
				$T->set_var('row_color', $row_color);
				$T->parse('rows', 'row', true);
			}
			if($num_items > $num_items_page && $query == '')
				show_prev_next($num_items, $num_pages, $start, $db_cat_id);
			$T->set_var('select_category', select_category($category, true));
		}	else {
			$T->set_var(array(
								'select_category'	=> select_category($category, true),
								'rows'				=> '<tr><td colspan="10" align="center" bgcolor="{color_tbl2}" height="28">no products found</td></tr>'));
			$T->implodeBlock('row');
		}

		if($query != '' && isset($msg))	push_msg($msg, 'info', $CFG->msg);
		return $CFG->dbh->RowCount;
	}

	function show_prev_next($num_items, $num_pages, $start, $category) {
		global $CFG, $T;

		$num_items_page = $CFG->admin_items_page;
		$T->set_file('prev_next', 'admin/tmpl.prev_next.inc');
		$cat_id = ($category == '' || $category == '%') ? '':"sel_category=$category&";

		if($start >	0) {
			$prev = "<a href=\"./?" . $cat_id . "start=" . ($start - $num_items_page) . "{pn_extra_vars}\">&lt;prev</a> | ";
		}	else $prev = "&lt;prev | ";

		$i	= 0;
		while($i	< $num_pages) {
			$page	= $i * $num_items_page;
			$i++;
			if($page	!=	$start) {
				$pages .= "<a href=\"./?" . $cat_id . "start=$page{pn_extra_vars}\">$i</a> | \n";
			}	else $pages .= "<b>$i</b> | \n";
		}

		if($start <	(($num_pages - 1) * $num_items_page)) {
			$next .= "<a href=\"./?" . $cat_id . "start=" . ($start + $num_items_page) . "{pn_extra_vars}\">next&gt;</a>";
		}	else $next .= "next&gt;";

		$T->set_var(array(
							'prev' => $prev,
							'next' => $next,
							'pages'=> $pages));

		array_push($CFG->page_arr, 'prev_next');
	}

	function gen_thumbnail($image_file, $form) {
		global $CFG;

		$thumb_type = $_FILES['full_image']['type'];
		if($thumb_type == '') return false;

		if($thumb_name = resize_image($image_file, $thumb_type, $form['thumb_scale'])) {
			push_msg('thumbnail image: <b>' . $thumb_name . '</b> created.', 'info', $CFG->msg);
			if($form['old_thumb_path'] != '' && file_exists($CFG->prod_dir . $form['old_thumb_path']) && $form['old_thumb_path'] != $image['name']) {
				if(@unlink($CFG->prod_dir . $form['old_thumb_path']))
					push_msg('file: <b>' . $form['old_thumb_path'] . '</b> deleted.', 'info', $CFG->msg);
			}

			$CFG->dbh->Query("UPDATE $CFG->phpc_table
									SET thumb_path = '$thumb_name'
									WHERE product_id = $form[id]",
									__FILE__, __LINE__);

			return basename($thumb_name);
		}  else {
			if($form['old_thumb_path'] == '')
				$CFG->dbh->Query("UPDATE $CFG->phpc_table
										SET thumb_path = 'not_available.png'
										WHERE product_id = $form[id]",
										__FILE__, __LINE__);

			$thumb_name = get_thumbname($image_type, $thumb_type);
			push_msg('thumb image: <b>' . $thumb_name . '</b> could not be created', 'warning', $CFG->msg);
			return false;
		}
	}

	function resize_image($image, $image_type, $scale) {
		global $CFG;

		switch($image_type) {
			case('image/gif'):
				$fn_image 	= 'imagegif';
				$image_and	= 'IMG_GIF';
				$fn_imagecreatefrom = 'imagecreatefromgif';
				$thumb_name = str_replace('.gif', '_thumb.gif', $image);
			break;

			case('image/jpeg'):
			case('image/pjpeg'):
				$fn_image 	= 'imagejpeg';
				$image_and	= 'IMG_JPG';
				$fn_imagecreatefrom = 'imagecreatefromjpeg';
				$thumb_name = str_replace('.jpg', '_thumb.jpg', $image);
				$interlace	= true;
			break;

			case('image/x-png'):
				$fn_image 	= 'imagepng';
				$image_and	= 'IMG_PNG';
				$fn_imagecreatefrom = 'imagecreatefrompng';
				$thumb_name = str_replace('.png', '_thumb.png', $image);
				break;

			default:
				$image_type = 'unknown';
				break;
		}

		if(function_exists('imagetypes'))	$imagetypes = ! imagetypes() & $image_and;
		else $image_types = true;
		if($image_types || ! function_exists($fn_imagecreatefrom) || $fn_imagecreatefrom == '') {
			push_msg('no support for: <b>' . $image_type . '</b> in your PHP build', 'warning', $CFG->msg);
			return false;
		}
		$image 	= $CFG->prod_dir . $image;
		$image_size = @getimagesize($image);
		$width	= $image_size[0];
		$height	= $image_size[1];
		$thumb_width	= ($scale /	100) * $width;
		$thumb_height	= ($scale /	100) * $height;

	// uncomment if you need to limit all images to max_img_width ... _height
/*	 		if($thumb_width > $CFG->max_img_width || $thumb_height > $CFG->max_img_height) {
			if(($width - $thumb_width) > ($height - $thumb_height)) {
				$thumb_width	= $CFG->max_img_width;
				$scale 			= $thumb_width / $width;
				$thumb_height 	= $scale * $height;
			}	else {
				$thumb_height	= $CFG->max_img_height;
				$scale 			= $thumb_height / $height;
				$thumb_width 	= $scale * $width;
			}
		}*/

		$thumb_name		= $CFG->prod_dir . $thumb_name;
		if(!$image_ptr	= $fn_imagecreatefrom($image))
			throw_error('no support for: <b>' . $image_type . '</b> in your PHP build');

		$gd2	= @imagecreatetruecolor(1, 1);

		if($gd2)	{
			if($interlace)	$fn_imagecreate = 'imagecreatetruecolor';
			else	$fn_imagecreate = 'imagecreate';
			$fn_imagecopyre = 'imagecopyresampled';
		}	else	{
			$fn_imagecreate = 'imagecreate';
			$fn_imagecopyre = 'imagecopyresized';
		}

		$thumb_ptr = $fn_imagecreate($thumb_width, $thumb_height);
		if($interlace)	imageinterlace($thumb_ptr);
		$color_white = imagecolorallocate($thumb_ptr, 255, 255, 255);
		imagefill($thumb_ptr, 0, 0, $color_white);
		$fn_imagecopyre($thumb_ptr, $image_ptr, 0, 0, 0, 0, $thumb_width, $thumb_height, $width, $height);
		$fn_image($thumb_ptr, $thumb_name, 100);
		imagedestroy($thumb_ptr);
		imagedestroy($image_ptr);
		return str_replace($CFG->prod_dir, '', $thumb_name);
	}

	function upload_image($image_type, $form) {
		global $CFG;

		require_once($CFG->libdir . '/class/class.File_upload.inc');
		$upload 		= new File_upload;
		$image_path = $image_type == 'full_image' ? 'image_path':'thumb_path';
		$old_path 	= $image_type == 'full_image' ? $form['old_image_path']:$form['old_thumb_path'];

		if($image = $upload->Upload($image_type, $CFG->prod_dir, $CFG->max_img_size, null, null, null, __FILE__, __LINE__)) {
			if(is_array($image['msg']))
				foreach($image['msg'] as $msg) push_msg($msg, 'info', $CFG->msg);
			push_msg("file: <b>$image[name]</b> uploaded.", 'info', $CFG->msg);
			if($old_path != '' && file_exists($CFG->prod_dir . $old_path) && $old_path != $image['name']) {
				if(@unlink($CFG->prod_dir . $old_path))
					push_msg("file: <b>$old_path</b> deleted.", 'info', $CFG->msg);
			}
			$CFG->dbh->Query("UPDATE $CFG->phpc_table
									SET $image_path	= '$image[name]'
									WHERE product_id = $form[id]",
									__FILE__, __LINE__);
		}  else {
			$CFG->dbh->Query("UPDATE $CFG->phpc_table
									SET $image_path = 'not_available.png'
									WHERE product_id = $form[id]",
									__FILE__, __LINE__);
			$err = $upload->ErrorMsg();
			push_msg($upload->UploadReport($image_type), false, $CFG->msg);
			$display = ucfirst($image_type);
			$file_name = $_FILES[$file_name]['name'];
			push_msg("$display: $file_name could not be uploaded", 'warning', $CFG->msg);
			push_msg("image not inserted!", 'warning', $CFG->msg);
			return 'not_available.png';
		}
		upload_error_msg($upload->ErrorMsg());
		return $image['name'];
	}

	function upload_error_msg($upload_err) {
		if($upload_err != '') {
			if(! is_array($upload_err))
				$upload_err = array($upload_err);
			foreach($upload_err as $msg)
				push_msg($msg, 'warning', $CFG->msg);
		}
	}

	function get_thumbname($file_name, $image_type) {
		global $CFG;

		switch($image_type) {
			case('image/gif'):
				$thumb_name = str_replace('.gif', '_thumb.gif', $file_name);
			break;

			case('image/pjpeg'):
				$thumb_name = str_replace('.jpg', '_thumb.jpg', $file_name);
			break;

			case('image/pjpeg'):
				$thumb_name = str_replace('.png', '_thumb.png', $file_name);
				break;

			default:
				return false;
				break;
		}
		return $thumb_name;
	}

	function db_prepare_sql($sql) {
		$sql				= trim($sql);
	 	$sql_len			= strlen($sql);
		$char				= '';
		$string_start	= '';
		$in_string		= false;
		$buffer			= array();

	 	for ($i = 0; $i < $sql_len; ++$i) {
			$char = $sql[$i];
			if($in_string) {
				for (;;) {
					$i = strpos($sql, $string_start, $i);
					if(! $i) {
						$buffer[] = $sql;
						return $buffer;
					}	else if ($string_start == '`' || $sql[$i-1] != '\\') {
						$string_start	= '';
						$in_string		= false;
						break;
					}	else {
						// ... first checks for escaped backslashes
						$j = 2;
						$escaped_backslash = false;
						while($i-$j > 0 && $sql[$i-$j] == '\\') {
							$escaped_backslash = !$escaped_backslash;
							$j++;
						}
						if ($escaped_backslash) {
							$string_start	= '';
							$in_string		= false;
							break;
						}	else $i++;
					}
				}
		  }	else if ($char == ';') {
				// if delimiter found, add the parsed part to the returned array
				$buffer[]	= substr($sql, 0, $i);
				$sql			= ltrim(substr($sql, min($i + 1, $sql_len)));
				$sql_len		= strlen($sql);
				if ($sql_len) $i = -1;
				else return $buffer;
		  }	else if (($char == '"') || ($char == '\'') || ($char == '`')) {
				$in_string    = true;
				$string_start = $char;
		  }	else if ($char == '#' || ($char == ' ' && $i > 1 && $sql[$i-2] . $sql[$i-1] == '--')) {
				$start_of_comment = (($sql[$i] == '#') ? $i : $i-2);
				$end_of_comment   = (strpos(' ' . $sql, "\012", $i+2))
										? strpos(' ' . $sql, "\012", $i+2)
										: strpos(' ' . $sql, "\015", $i+2);
				if (!$end_of_comment) {
					 if ($start_of_comment > 0) $buffer[]    = trim(substr($sql, 0, $start_of_comment));
					 return $buffer;
				}	else {
					 $sql          = substr($sql, 0, $start_of_comment)
										. ltrim(substr($sql, $end_of_comment));
					 $sql_len      = strlen($sql);
					 $i--;
				}
			}
		}

	 if (! empty($sql) && ereg('[^[:space:]]+', $sql)) $buffer[] = $sql;
	 return $buffer;
	}

	function install($modules) {
		global $CFG;

		$full_me	= full_me();
		$me		= basename($full_me);
		$status	= 0;

		$param[]	= 'phpcat';
		$param[] = $CFG->appversion;
		$param[]	= getenv('SERVER_SOFTWARE');
		$param[]	= str_replace('admin/', '', str_replace($me, '', $full_me));
		$param[]	= is_array($modules) ? implode(', ', $modules):'none';

		$payload = "<?xml version=\"1.0\"?>\r\n<methodCall>\r\n<methodName>soap.install</methodName>\r\n<params>\r\n";
		foreach($param as $val)
			$payload .= "<param>\r\n<value><string>$val</string></value>\r\n</param>\r\n";

		$payload .= "</params>\r\n</methodCall>\r\n";

		$fp = @fsockopen('siliconsys.com', 80, $errno, $errstr, 15);
		if($fp)	{
			@socket_set_timeout($fp, 5);
			$post =  "POST /soap_server.php HTTP/1.0\r\nUser-Agent: siSOAP 1.0\r\nHost: siliconsys.com\r\nContent-Type: text/xml\r\nContent-Length: " . strlen($payload) . "\r\n\r\n$payload";

			@fputs($fp, $post, strlen($post));

			while($buffer = @fgets($fp, 1024))
				if($buffer == "\r\n")	break;

			$buffer = @fread($fp, 1024);
			@fclose($fp);
		}	else $buffer = -1;
		$status = ! empty($buffer) ? $buffer:$status;
		if(preg_match("|<value><int>([-]?[0-9])</int></value>|", $status, $match))
			$status = (int)$match[1];
		return $status;
	}

	function db_read_sql_file($filename) {
		global $CFG;

		$count = 0;
		$dbh = dbi_conn($CFG->dbtype, $CFG->dbname);
		if($fp = @fopen($filename, "r"))
			if($sql = @fread($fp, filesize($filename)))
				if(@fclose($fp)) {
					foreach(db_prepare_sql($sql) as $query) {
						$dbh->Query($query, __FILE__, __LINE__);
						if(! preg_match("/^#.*/", $query, $matches))
							$count++;
					}

					return $count;
				}	else	local_error("cannot close sql file: $filename");
			else	local_error("cannot read sql file: $filename");
		else	local_error("cannot open sql file: $filename for reading");
	}


	function platform() {
		if(!empty($_SERVER['HTTP_USER_AGENT']))
			$user_agent = $_SERVER['HTTP_USER_AGENT'];

		if(strstr($user_agent, 'Win')) return 'Win';
		elseif(strstr($user_agent, 'Mac')) return 'Mac';
		elseif(strstr($user_agent, 'Linux')) return 'Linux';
		elseif(strstr($user_agent, 'Unix')) return 'Unix';
		elseif(strstr($user_agent, 'OS/2')) return 'OS/2';
		else	return 'Other';
	}

	function browser() {

		if(!empty($_SERVER['HTTP_USER_AGENT']))
			$user_agent = $_SERVER['HTTP_USER_AGENT'];

		if(ereg('MSIE ([0-9].[0-9]{1,2})', $user_agent, $log_version)) {
			$browser['version']	= $log_version[1];
			$browser['agent']		= 'IE';
		}	elseif(ereg('Opera(/| )([0-9].[0-9]{1,2})', $user_agent, $log_version)) {
			$browser['version']	= $log_version[2];
			$browser['agent']		= 'OPERA';
		}	elseif(ereg('OmniWeb/([0-9].[0-9]{1,2})', $user_agent, $log_version)) {
			$browser['version']	= $log_version[1];
			$browser['agent']		= 'OMNIWEB';
		}	elseif(ereg('Mozilla/([0-9].[0-9]{1,2})', $user_agent, $log_version)) {
			$browser['version']	= $log_version[1];
			$browser['agent']		= 'MOZILLA';
		}	elseif(ereg('Konqueror/([0-9].[0-9]{1,2})', $user_agent, $log_version)) {
			$browser['version']	= $log_version[1];
			$browser['agent']		= 'KONQUEROR';
		}	else {
			$browser['version']	= 0;
			$browser['agent']		= 'OTHER';
		}
		return $browser;
	}

	function crlf($os='') {
		$os = $os == '' ? platform():$os;
		switch($os) {
			case('Mac'):
				$crlf = '\r';
				break;
			case('Win'):
				$crlf = '\r\n';
				break;
			default:
				$crlf = '\n';
				break;
		}
		return $crlf;
	}
?>