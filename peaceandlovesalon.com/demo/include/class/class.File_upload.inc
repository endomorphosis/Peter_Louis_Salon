<?php
/*
**	$Id: class.File_upload.inc,v 1.3 2003/09/17 16:29:33 damonp Exp $
**
**	Copyright (C) 2002 siliconsys.com <dev@siliconsys.com>
**	See LICENSE file in root directory of this installation for
**	licensing information.
*/

	class File_upload {

		/** Internal Use - class error instance */
		var $c_error = '';
/***********************************
**	constructor
***********************************/
		function File_upload() {
			$this->c_error = create_object("Error", "File_upload");
			$this->c_error->SetFile(__FILE__);
			$this->c_error->SetDebug("VERBOSE");
		}

/***********************************
**	public functions
***********************************/

		function UploadReport($file_name) {

			$upload_file = $_FILES[$file_name];
			extract($upload_file, EXTR_PREFIX_ALL, "f");

			$buffer = "
<br><br>
<center>
 <table>
  <tr><th colspan=2 align=center><u>UPLOAD REPORT</u></th></tr>
  <tr><td align=right><b>UPLOAD TMP FILE:</b></td><td>$f_tmp_name</td></tr>
  <tr><td align=right><b>FILE NAME:</b></td><td>$f_name</td></tr>
  <tr><td align=right><b>FILE SIZE:</b></td><td>$f_size</td></tr>
  <tr><td align=right><b>FILE TYPE:</b></td><td>$f_type</td></tr>
 </table>
</center>\n";

			return $buffer;
    	}

    	function UploadForm($file_name, $file_size) {
			return "<input type=\"hidden\" name=\"MAX_FILE_SIZE\" value=\"$file_size\">\n<input type=\"file\" name=\"$file_name\" size=\"40\" class=\"input\">";
		}

		function Upload($file_name, $dest_path, $max_file_size='',
									$max_width='', $max_height='', $image_type='', $file='', $line='') {

			$temp_name = $_FILES[$file_name]['tmp_name'];

			switch($_FILES[$file_name]['error'])	{
				case(1):
					$file_error = 'The uploaded file exceeds the upload_max_filesize directive in php.ini. ';
					break;
				case(2):
					$file_error = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form. ';
					break;
				case(3):
					$file_error = 'The uploaded file was only partially uploaded.';
					break;
				case(4):
					$file_error = 'No file was uploaded.';
					break;
			}
			if($file_error != '') $buffer['msg'][] = $file_error;

			if($temp_name == 'none' || $temp_name == '' || ! is_uploaded_file($temp_name)) {
				$this->_halt('No file to upload.', $file, $line);
				return false;
			}	else {
				$upload_file = $_FILES[$file_name];
				extract(pathinfo($upload_file['name']), EXTR_PREFIX_ALL, "f");
				$buffer['type'] = $upload_file['type'];
				$buffer['size'] = $upload_file['size'];
				if($max_file_size != '') {
					$image_size = @getimagesize($upload_file['tmp_name']);
					if($upload_file['size'] > $max_file_size) {
						$this->_halt("file size: <b>($upload_file_size) > $max_file_size kb</b>", $file, $line);
						return false;
					}
				}

				if($image_type != '') {
					if($image_size[2] != $image_type) {
						$this->_halt("image is of the wrong type.", $file, $line);
						$this->_halt("image must be of type: <b>$image_type</b>", $file, $line);
						return false;
					}
				}

				if($max_width != '' && $max_height != '') {
					if( ! (! ($image_size[0] > $max_width) && ! ($image_size[1] > $max_height))) {
						$this->_halt("image size is too big", $file, $line);
						$this->_halt("image must be: <b>$max_width x $max_height</b>", $file, $line);
						$this->_halt("image is <b>$image_size[0] x $image_size[1]</b>", $file, $line);
						return false;
					}
				}

				if($dest_path == "VARIABLE") {
		  			if($tmp = addslashes(fread(fopen("$upload_file", "r"), filesize($upload_file)))) {
		  				return $tmp;
					}	else {
						$this->_halt("image data not loaded into variable.", $file, $line);
						return false;
					}
				}	else {
					$dest_file = $dest_path . $f_basename;
					$base_name = str_replace("." . $f_extension, '', $f_basename);
					$base_name = $this->_clean_filename($base_name);
					$orig_name = $base_name;
					$last_name = $f_basename;
					$count = 1;
					while(file_exists($dest_file) && $count < 20) {
						$file_name = $dest_path . $orig_name . $count;
						$dest_file = $file_name . "." . $f_extension;
						$base_name = basename($dest_file);
						$buffer['msg'][] = "file: <b>$last_name</b> exists. Renaming to <b>$base_name</b>.";
						$last_name = $base_name;
						$count++;
					}
					if (! @copy($upload_file['tmp_name'], $dest_file)) {
					  $this->_halt("failed to copy file: <b>$upload_file[tmp_name]</b> to <b>$dest_file</b>. check the permissions of the destination directory.", $file, $line);
					  return false;
		      	} 	else {
		      		$buffer['name'] = basename($dest_file);
		      		return $buffer;
		      	}
		      }
	      }
		}

		function ErrorMsg() {
			return $this->c_error->ReturnMsg();
		}

		function _clean_filename($filename)	{
			$filename = str_replace(array(' ', '%20'), '_', $filename);
			$filename = str_replace(array('\'', '"', ';', '>', '<', '@', '#', '&', '*', '(', ')'), '', $filename);
			return $filename;
		}

		function _halt($msg, $file, $line) {
			$this->c_error->SetFile($file);
			$error_msg = "$msg<br>\n" . $this->_error;
			$this->c_error->Halt($error_msg, $this->_errno, $line);
		}

		function _warn($msg, $file, $line) {
			$this->c_error->SetFile($file);
			$error_msg = "$msg<br>\n" . $this->_error;
			$this->c_error->Warn($error_msg, $this->_errno, $line);
		}
	}
?>