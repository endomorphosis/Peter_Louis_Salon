<?php
/*
**	$Id: class.DBI_mysql.inc,v 1.1 2003/04/10 20:20:47 damonp Exp $
**
**
*/

	class DBI {

		/*private vars*/
	  	var $_db_linkid 	= 0;
	  	var $_db_name 		= '';
	  	var $_db_qresult 	= 0;
	  	var $_auto_commit = false;
	  	var $_db_debug 	= false;
	   var $_halt_on_err	= true;
	  	var $_errno   		= 0;
	  	var $_error    	= '';
	  	var $_next_row		= 0;

		/*public vars*/
		var $RowData 		= array();
		var $RowCount 		= 0;
		var $FieldCount 	= 0;

/***********************************
**	constructor
***********************************/

		function DBI($host, $user, $pass, $db_name, $autocommit = true) {
			$this->c_error = create_object("Error", "DBI::MySQL");
			$this->c_error->SetDebug("VERBOSE");
			$this->_db_name = $db_name;

			$this->Open($host, $user, $pass, $db_name, $autocommit);
			if ($db_name != '')
				return $this->SelectDB($db_name);
			else return false;
		}
/***********************************
**	public methods
***********************************/

		function Open($host, $user, $pass, $db_name = '', $autocommit = true) {
			$this->_db_linkid = mysql_connect($host, $user, $pass) OR die("Unable to connect to database");
		}

		function Close() {
			@mysql_free_result($this->_db_qresult);
			return mysql_close($this->_db_linkid);
		}

		function SelectDB($db_name) {
			if (@mysql_select_db($db_name, $this->_db_linkid) == true) {
				return true;
			}	else	{
				$this->_warn("UNABLE TO CONNECT TO: $db_name", $file, $line);
				$this->_db_name = false;
				return false;
			}
		}

		function  Query($query, $file='', $line='') {
			if ($this->_db_debug == "VERBOSE")
				printf("<code><b>query:</b> %s<br>\nFile: $file<br>\nLine: $line</code><br>\n", nl2br($query));

			$this->_db_qresult = @mysql_query ($query, $this->_db_linkid);
			$this->_error = @mysql_error($this->_db_linkid);
			$this->_errno = @mysql_errno($this->_db_linkid);

			if (! $this->_db_qresult) {
				$this->_halt("Invalid SQL: " . $query, $file, $line);
				return false;
			}	else {
				$this->RowData 	= array();
				$this->RowCount 	= @mysql_num_rows($this->_db_qresult);
				$this->FieldCount = @mysql_num_fields($this->_db_qresult);
				if(! $this->RowCount) {
					/// The query was probably an INSERT/REPLACE etc.
					$this->RowCount 		= 0;
					$this->_db_qresult 	= 0;
					$this->_next_row 		= 0;
					return true;
				}
				return $this->_db_qresult;
			}
		}

		function InsertID($table, $col) {
			return mysql_insert_id();
		}

		function NextID($table, $col) {
			$query = "SELECT MAX($col) FROM $table LIMIT 1";
			if ($this->_db_debug == "VERBOSE")
				printf("<code><b>query:</b> %s</code><br>\n", $query);
			$this->Query($query);
			$this->FetchObject($this->_db_qresult);
			return $this->$col;
		}

		function SeekRow($row=0, $file='', $line='') {
			if((! mysql_data_seek ($this->_db_qresult, $row)) or
												($row > $this->RowCount-1)) {
				$this->_halt("SeekRow: Cannot seek to row $row", $file, $line);
				return false;
			}	else return true;
		}

		function FetchArray($result='', $mode=3) {
		/**	fetch data row from result
		 *		input		-	@result - ptr to query result
		 *						@mode - 1 => ASSOC, 2 => NUM, 3 => BOTH
		 *		output	-	false or next row data
		 */
			$result = $result == '' ? $this->_db_qresult:$result;
			$this->RowData = @mysql_fetch_array($result, $mode);
			if(! is_array($this->RowData)) {
				$this->Free($result);
				return false;
			}	else {
				$this->_next_row++;
				return $this->RowData;
			}
		}

		function FetchObject($result='', $mode=3) {
		/**	fetch data row from result
		 *		input		-	@result - ptr to query result
		 *						@mode - 1 => ASSOC, 2 => NUM, 3 => BOTH
		 *		output	-	false or next row data
		 */
			$result = $result == '' ? $this->_db_qresult:$result;
			$this->RowData = @mysql_fetch_object($result, $mode);
			if (! is_object($this->RowData)) {
				$this->Free($result);
				return false;
			}	else {
				$this->_next_row++;
				return $this->RowData;
			}
		}

		function RowsAffected() {
			return mysql_affected_rows($this->_db_linkid);
		}

		function FieldName($offset) {
			return mysql_field_name($this->_db_qresult, $offset);
		}

		function FieldType($offset) {
			return mysql_field_type($this->_db_qresult, $offset);
		}

		function ListTables() {
			$query = "SHOW TABLES";
			if ($this->_db_debug == "VERBOSE")
				printf("<code><b>query:</b> %s</code><br>\n", $query);
			$this->Query($query);
			while($r = $this->FetchArray())
				$return_array[] = $r[0];
			return $return_array;
		}

		function Lock($table, $mode="write") {
			$query = "LOCK TABLES ";
			if (is_array ($table)) {
				while (list ($key, $value) = each($table)) {
					if ($key == "read" && $key != 0) {
						$query .= "$value READ, ";
					} 	else {
						$query .= "$value $mode, ";
					}
				}
				$query = substr($query,0,-2);
			} 	else $query .= "$table $mode";
			if ($this->_db_debug == "VERBOSE")
				printf("<code><b>query:</b> %s</code><br>\n", $query);
			$this->Query($query);
			if (! $this->_db_qresult) {
				$this->_halt("Lock($table, $mode) Failed.");
				return false;
			}
			return $this->_db_qresult;
		}

		function UnLock() {
			$query = "UNLOCK TABLES";
			if ($this->_db_debug == "VERBOSE")
				printf("<code><b>query:</b> %s</code><br>\n", $query);
			$this->Query($query);
			if (! $this->_db_qresult) {
				$this->_halt("Unlock() Failed.");
				return false;
			}
			return $this->_db_qresult;
		}

		function Commit() {
			return true;
		}

		function Rollback() {
			$this->_halt("WARNING: Rollback is not supported by MySQL");
		}

		function Free($result='') {
			$result = $result == '' ? $this->_db_qresult:$result;
			$result->_next_row = 0;
			$result->RowCount = 0;
			return @mysql_free_result($result);
		}

		function EscapeStr($str) {
			return mysql_esacpe_string($str);
		}

		function SetDebug($debug) {
			$this->_db_debug = $debug;
		}

		function Ident() {
			return "DBI_MYSQL";
		}

/***********************************
**	private methods
***********************************/

		function _halt($msg, $file, $line) {
			$this->c_error->SetFile($file);
			$error_msg = "$msg<br>\n" . $this->_error;
			$this->c_error->Halt($error_msg, $this->_errno, $line);
		}

		function _warn($msg, $file, $line) {
			if($this->_db_debug != 'VERBOSE' || $this->_db_debug != 'WARN') return;
			$this->c_error->SetFile($file);
			$error_msg = "$msg<br>\n" . $this->_error;
			$this->c_error->Warn($error_msg, $this->_errno, $line);
		}
	}

/***********************************
**	dbi_mysql
***********************************/

	class DBI_mysql extends DBI {
		function DBI_mysql($host, $user, $pass, $db_name, $autocommit=true) {
			$this->c_error = create_object("Error", "DBI::MySQL");
			$this->c_error->SetDebug("VERBOSE");

			$this->Open ($host, $user, $pass, $db_name, $autocommit);
			if ($db_name != '')
				if($this->SelectDB($db_name)) return true;
				else return false;
			else return false;
		}
	}
?>