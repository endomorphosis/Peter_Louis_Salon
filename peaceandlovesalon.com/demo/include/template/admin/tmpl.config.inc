<!-- START admin/config -->
<table cellpadding="0" cellspacing="0" border="0" width="100%">
<tr><td background="{base}/image/pixel_black.png" height="1" vAlign="bottom"><img src="{base}/image/pixel_black.png" height="1" width="1"></td></tr>
<tr bgcolor="{color_tbl2}">
<td>
<table cellpadding="4" cellspacing="2" border="0" width="100%">
<form name="admin_config" action="{me}" method="post">
<input type="hidden" name="function" value="config">
<input type="hidden" name="catalog_id" value="{catalog_id}">
<input type="hidden" name="cfg_catalog_name" value="catalog">
<tr height="28" bgcolor="{color_label}"><td colspan="3"><img src="{base}/image/pixel_gray.png"></td></tr>
<tr>
<td bgcolor="{color_label}" class="label" width="25%">currency symbol</td>
<td width="25%"><input type="text" name="cfg_currency_sym" value="{currency_sym}" class="input" size="3"></td>
<td width="50%" class="small">USD = '$', &#163; = '&amp;#163;', &#8364; = '&amp;#8364;', &#165; = '&amp;#165;</td>
</tr>
<tr>
<td bgcolor="{color_label}" class="label">currency location</td>
<td class="small"><input type="radio" name="cfg_currency_location" value="before" class="input"{currency_loc_before}>before&nbsp;&nbsp;<input type="radio" name="cfg_currency_location" value="after" class="input"{currency_loc_after}>after</td>
<td class="small">before: {currency} 1.00, after: 1.00 {currency}</td>
</tr>
<tr>
<td bgcolor="{color_label}" class="label">thousand separator</td>
<td><input type="text" name="cfg_thousand_sep" value="{thousand_sep}" class="input" size="3"></td>
<td class="small">USD = ',', Euro = '.'</td>
</tr>
<tr>
<td bgcolor="{color_label}" class="label">decimal separator</td>
<td><input type="text" name="cfg_decimal_sep" value="{decimal_sep}" class="input" size="3"></td>
<td class="small">USD = '.', Euro = ','</td>
</tr>
<tr>
<td bgcolor="{color_label}" class="label">number of items page</td>
<td><input type="text" name="cfg_num_items_page" value="{num_items_page}" class="input" size="3"></td>
<td class="small">default = '10'</td>
</tr>
<tr>
<td bgcolor="{color_label}" class="label">number of 'top items' to show</td>
<td><input type="text" name="cfg_num_stats" value="{num_stats}" class="input" size="3"></td>
<td class="small">set to '0' to disable</td>
</tr>
<tr>
<td bgcolor="{color_label}" class="label">duration of stats</td>
<td><input type="text" name="cfg_stats_duration" value="{stats_duration}" class="input" size="3"></td>
<td class="small">in days</td>
</tr>
<tr>
<td bgcolor="{color_label}" class="label">number of specials to show</td>
<td><input type="text" name="cfg_num_specials" value="{num_specials}" class="input" size="3"></td>
<td class="small">set to '0' to disable</td>
</tr>
<tr>
<td bgcolor="{color_label}" class="label">price on request text</td>
<td colspan="2"><input type="text" name="cfg_price_on_request" value="{price_on_request}" class="input" size="60"></td>
</tr>
<tr bgcolor="{color_label}"><td colspan="3" align="center"><input type="submit" name="submit" value="save" class="input"></td></tr>
</form>
</td></tr>
</table>
</td>
</tr>
<tr><td background="{base}/image/pixel_black.png" height="1" vAlign="top"><img src="{base}/image/pixel_black.png" height="1" width="1"></td></tr>
</table>
<!-- FINISH admin/config -->
