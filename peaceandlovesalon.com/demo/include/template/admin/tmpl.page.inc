<html>
<head>
<title>{pagetitle}</title>
<meta content="text/html; charset=windows-1252" http-equiv="Content-Type">
<link rel="stylesheet" href="./admin.css" type="text/css">
<script src="./jscript/admin.js" language="Javascript1.1"></script>
{user_js}
{verify_js}
</head>
<body>
<table border="0" cellpadding="6" cellspacing="0" width="98%" align="center">
<tr>
<td colspan="2" width="100%">
<table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
<tr><td colspan="6" background="{base}/image/pixel_black.png" height="1"><img src="{base}/image/pixel_black.png" height="1" width="1"></td></tr>
<tr vAlign="middle" height="50" bgcolor="#cccccc">
<td background="{base}/image/pixel_black.png" width="1" rowspan="2"><img src="{base}/image/pixel_black.png" height="1" width="1"></td>
<td align="left" valign="top" width="300" height="50">
<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" align="center" bgcolor="#cccccc">
<tr height="50%">
<td rowspan="2" width="5"><img src="{base}/image/pixel_gray.png" height="1" width="1"></td>
<td nowrap><font size="5" face="verdana"><b>PHPCatalog</b></font></td>
</tr>
<tr height="50%" valign="bottom"><td><div id="text"><img src="{base}/image/pixel_gray.png"></div></td></tr>
</table>
</td>
<td><img src="{base}/image/pixel_gray.png" height="1" width="1"></td>
<td valign="bottom" width="100">
<table border="0" cellpadding="0" cellspacing="0" width="100%" align="right" bgcolor="#cccccc">
<tr>
<td align="center"><a href="javascript: void(0);" onclick="help_win('{section}', '{function}');" onMouseOver="flash_blurb('help');" onMouseOut="flash_blurb('');"><img src="{base}/image/icon/help.png" title="help" border="0"></a></td>
<td align="center"><a href="./index.php" onMouseOver="flash_blurb('catalog');" onMouseOut="flash_blurb('');"><img src="{base}/image/icon/catalog.png" title="catalog" border="0"></a></td>
<td align="center"><a href="./config.php" onMouseOver="flash_blurb('configure system');" onMouseOut="flash_blurb('');"><img src="{base}/image/icon/config.png" title="configure system" border="0"></td>
</tr>
</table>
</td>
<td width="5"><img src="{base}/image/pixel_gray.png" height="1" width="1"></td>
<td background="{base}/image/pixel_black.png" width="1" rowspan="4"><img src="{base}/image/pixel_black.png" height="1" width="1"></td>
</tr>
<tr><td colspan="4" bgcolor="#cccccc" height="5"><img src="{base}/image/pixel_gray.png" height="1" width="1"></td></tr>
<tr><td colspan="6" background="{base}/image/pixel_black.png" height="1"><img src="{base}/image/pixel_black.png" height="1" width="1"></td></tr>
</table>
</td>
</tr>
<tr>
<td height="350" vAlign="top" width="15%">
{menu}
</td>
<td height="350" width="85%" vAlign="top">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr><td colspan="3" background="{base}/image/pixel_black.png" height="1"><img src="{base}/image/pixel_black.png" height="1" width="1"></td></tr>
<tr>
<td rowspan="3" background="{base}/image/pixel_black.png" width="1"><img src="{base}/image/pixel_black.png" height="1" width="1"></td>
<td>
<table border="0" cellpadding="4" cellspacing="0" width="100%">
<td class="locBar"><img src="{base}/image/pixel_white.png" height="1" width="10">|| {section} -|- {function} -|- {action} -|- {d_id} ||</td>
<td align="right">{locbar}</td>
</tr>
<tr height="1"><td colspan="2">
{msg}
</td></tr>
<tr>
<td colspan="2" height="350" vAlign="top" align="center">
{content}
</td>
</tr>
</table>
</td>
<td background="{base}/image/pixel_black.png" width="1"><img src="{base}/image/pixel_black.png" height="1" width="1"></td>
</tr>
<tr><td colspan="3" background="{base}/image/pixel_black.png" height="1"><img src="{base}/image/pixel_black.png" height="1" width="1"></td></tr>
</table>
<font class="small">
{bottom}
</font>
</td></tr>
<tr><td>&nbsp;</td><td align="center" class="footer"><a href="http://www.siliconsys.com/products.php">PHPCatalog&#8482;</a> {appversion} &copy; 1999-2003 <a href="http://www.siliconsys.com/" target="_new">siliconsys.com</a></td></tr>
</table>
</body>
</html>
