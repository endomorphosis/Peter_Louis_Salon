
<!-- START admin/category_list -->
<table cellpadding="0" cellspacing="0" border="0" width="100%">
<form name="category_form" action="{me}" method="post">
<input type="hidden" name="function" value="categories">
<input type="hidden" name="action" value="delete">
<input type="hidden" name="id" value="">
<tr><td background="{base}/image/pixel_black.png" height="1" vAlign="bottom"><img src="{base}/image/pixel_black.png" height="1" width="1"></td></tr>
<tr bgcolor="{color_tbl2}">
<td>
<table cellpadding="0" cellspacing="0" border="0" width="100%">
<tr bgcolor="{color_label}" height="20">
<td width="10%" align="center"><b>id</b></td>
<td width="75%" align="left"><b>category</b></td>
<td width="15%" colspan="3" align="center"><b>action</b></td>
</tr>

<!-- BEGIN category_row -->
<tr bgcolor="{row_color}" class="list" onMouseOver="highlight(this, '{color_hilite}');" onMouseOut="highlight(this, '{row_color}');" onMouseDown="highlight(this, '{color_select}');">
<td align="center">{category_id}</td>
<td align="left">
{indent}{category_name}
</td>
<td width="5%"><a href="{me}?function=categories&action=edit&id={category_id}">
<img src="{base}/image/icon/edit.png" border="0" title="edit"></a>
</td>
<td width="5%"><a href="javascript:confirm_submit('category_form', 'delete category: {category_name}', {category_id});">
<img src="{base}/image/icon/delete.png" border="0" title="delete"></a>
</td>
<td width="5%"><a href="{me}?function=categories&action=add&id={category_id}">
<img src="{base}/image/icon/view.png" border="0" title="add"></a>
</td>
</tr>

<!-- END category_row -->

</td>
</tr>
</table>
</td>
</tr>
<tr><td background="{base}/image/pixel_black.png" height="1" vAlign="top"><img src="{base}/image/pixel_black.png" height="1" width="1"></td></tr>
</form>
</table>
<!-- FINISH admin/category_list -->
