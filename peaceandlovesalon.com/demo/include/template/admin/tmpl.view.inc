
<!-- START admin/view -->
{form_header}
{form_hidden}
<table cellpadding="4" cellspacing="2" border="0" width="100%">
<tr>
<td width="10%" class="label">sku:</td>
<td width="40%">{product_sku}</td>
<td width="10%" class="label">name:</td>
<td width="40%">{product_name}</td>
</tr>
<tr>
<td class="label">price:</td>
<td>{f_product_price}</td>
<td class="label">color:</td>
<td>{product_color}</td>
</tr>
<tr>
<td class="label">size:</td>
<td>{product_size}</td>
<td class="label">category:</td>
<td>{product_category}</td>
</tr>
<tr>
<td class="label">description:</td>
<td colspan="3" align="left"><p>{product_desc}</p></td>
</tr>
<tr>
<td class="label">thumbnail:</td>
<td colspan="3"><img src="{thumb_path}" border="0"></td>
</tr>
<tr>
<td class="label">image:</td>
<td colspan="3"><img src="{image_path}" border="0"></td>
</tr>
{im_extra_images}
<tr>
{button}
</tr>
</table>
{form_footer}
<!-- FINISH admin/view -->
