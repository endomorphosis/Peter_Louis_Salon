$Id: README,v 1.4 2004/03/16 21:24:53 damonp Exp $

PHPCatalog - README

v2.6

Copyright (C) 1999 - 2004 siliconsys.com <dev@siliconsys.com>

See LICENSE file in this directory for licensing information.

REQUIREMENTS
	Webserver
		Apache 1.3 + or
		optionally supported
		iPlanet 4.1 +
		IIS 6.0
	PHP-4.1.0 (with GD and libjpeg for gif, jpeg and png manipulation)
	DB Server
   	MySQL 3.23.x +
   	optionally supported:
   	PostGres 7.1 + or
   	Oracle 8i + or
		Informix

FEATURES
PHPCatalog provides a simple, easy to use online catalog. It contains modules for
the front-end catalog of items as well as the database schema and administration
pages for adding, updating and removing items. The front end modules do not contain
any HTML other than the database output for easy integration into your site.

Current features include:

	- Administration interface
	- Item Search
	- Ability to mark items as SOLD
	- User configurable 'Specials' or 'Sale' items
	- Upgrade to PHPCatalog+ for full shopping capabilities
	- Automatic generation of thumbnail images (for gif, jpeg or png files depending on your PHP setup)
	- Automatically reports statistics of products
	- Support for unlimited categories
	- Popup help functions
	- Database Independant (contact us for other database support)
	- Uses HTML templates for easy integration into your site


Commercial version includes all of this plus drop in module support,
user administration, products statistics and graphing, search engine
optimization, and customized index pages plus one year of support for
your domain. For a full comparison go to:
http://emailurl.com/phpccompare

We currently have the following additional modules:

	Module: Data Import/Export - allows easy importation/exportation
		of catalog data from any program that reads comma-separated
		data (csv).

	Module: Download Delivery - allows administrator to allow access to
		users to download software for a specified period of time.
		Protects directories and files from unauthorized access.

	Module: Forms - adds customizable forms to your catalog. Currently
		includes 'Contact Us', 'Send Link to Friend' and 'Request More
		Information for XXXX Product' template forms. Installation includes
		installation/customization of four forms.

	Module: Groups - allows specification of access permissions for
		categories and products. Can be used to "hide" any number of
		products or categories from different users. Also can specify
		price discounts for each group.

	Module: Image Manipulation - generates thumbnail images using
		a third-party, open source application, ImageMagick.
		It produces cleaner, sharper files and works with virtually
		all image formats. Also allows the layout of more than one
		set of images for each product.

	Module: Related Products - matches products with related items.
		Product detail view shows a configurable number of these
		related items with links to them.

	Module: Shopping Cart (Basic) - adds a simple shopping cart to the
		catalog. Allows users to add items from PHPCatalog to a cart.
		When the user is ready to checkout, they are provided with a
		form to enter their contact information. This form is emailed to
		a user-specified email address so that orders may be manually
		confirmed and processed.

	Module: Shopping Cart (Ecommerce) - adds a full ecommerce shopping
		cart to the catalog. Allows users to add items from PHPCatalog to
		a cart. Checkout provides full realtime ecommerce payment processing
		and shipping calculations.

We have many Custom Modules developed for previous projects:

   - Affiliates Program
   - Content Management System - interface to add informative pages
   - Inventory Control
   - Multi-Catalog - run multiple catalogs from the same directory and database
   - Multi-Language
   - Multi-Template - change the look of pages in different categories or per product
   - Product Comments - forms to submit and display user comments per product

Inquire at:
http://emailurl.com/phpcpsales

We also offer hosted catalog solutions where we install the the catalog on our servers,
set it up to match with your site layout, provide instruction and support for the
software and perform all of the maintenance. All you must do is supply the items and
handle the orders.

http://siliconsys.com/store/order/orderwiz.php?v=1

Quick Links:

PHPCatalog
http://emailurl.com/phpc

PHPCatalog Demo
http://emailurl.com/phpcdemo

PHPCatalog+ Demo
http://emailurl.com/phpcdemo3

PHPCatalog Ordering and Pricing Information
http://emailurl.com/phpcorder

PHPCatalog vs PHPCatalog+ Comparison
http://emailurl.com/phpccompare

Ask any pre-sales questions:
http://emailurl.com/phpcpsales


INSTALLATION

UPLOADING FILES
The first step is to extract the files from the downloaded file and copy the
files to a directory on the web server.  If you are using a Windows system you
can use a tool like WinZip to expand the tar.gz or .zip file.  On a Linux or
Unix based web server to can use the tar command to expand the tar.gz file.

tar -zxvf PHPCatalog-x.tar.gz


CONFIGURATION
Edit the file PHPCatalog/include/config.inc to match your setup.  Specifically
the $CFG variables:

$CFG->dbtype	= 'mysql';			// database server type (mysql, ifx, oci, pg)
$CFG->dbhost	= 'localhost';   	// hostname of your database server
$CFG->dbname 	= 'PHPCatalog';	// the database name
$CFG->dbuser	= 'username';		// database user authorized for this database
$CFG->dbpass	= 'password';		// password for this database user

$CFG->wwwroot	= 'http://www.domain.com/url/path/to/PHPCatalog';
$CFG->dirroot	= '/dir/path/to/PHPCatalog';

$CFG->dbtype
Type of database server you are using.  Valid choices are 'mysql' for MySQL,
'pg' for PostGreSQL, 'oci' for Oracle and 'ifx' for Informix.  Not all features
are supported in databases other than MySQL.

$CFG->dbhost
Hostname of the computer the database will be hosted on.  Check with your
hosting provider.  In most case 'localhost' will be correct.

$CFG->dbname
The name of the database you want to install PHPCatalog into.

$CFG->dbuser
The user authorized to access the above database.

$CFG->dbpass
The password for the above user.

$CFG->wwwroot
The url (ie. webserver) path to PHPCatalog on your webserver.  In most cases
'http://www.YOURDOMAIN.com/catalog' or 'http://www.YOURDOMAIN.com/phpcatalog'.

$CFG->dirroot
The directory path to PHPCatalog on your webserver.  In most cases something
like: '/www/vhosts/yourdomain.com/htdocs/catalog' or
'c:/www/vhosts/yourdomain.com/htdocs/catalog'.  If you have problems with this
contact your hosting provider.

If you are unsure of these last two parameters setup.php will try to guess them
later.

$CFG->admin_items_page
Number of items to list per page in the admin area.

$CFG->site_email
The main site email address to be displayed in email notifications and public
interfaces.

$CFG->site_name
The name of your site to be used in page titles, email notifications and public
interfaces.

define('DEBUG', true);
During installation and testing, you may uncomment and set this to true to
display any errors that may appear.  This will append debugging information to
the bottom of your page.  To turn this off for production use, comment out the
line like:

//define('DEBUG', true);

And it will no longer be parsed by PHP.

You shouldn't have to change any of the other variables unless you really know
what you are doing.

If you are using any additional modules, see the README file in each module
directory located under PHPCatalog/include/module/

Confirm that your webserver can write to the PHPCatalog/image/products/
directory.  This will vary from system to system and is beyond the scope this
document.  (Ed. note... _please_ do not ask for help with this, seek assistance
from you server admin or hosting company, we do not know how a particular server
is set up, and in most cases do not have the required file level permissions on
your server to make the changes).  On unix/linux systems execute the following
command if you absolutely cannot get the images to upload (execute from the
command line
in the PHPCatalog/image/ directory):

chmod -R 777 *

This makes everything under the directory PHPCatalog/image/ readable/writeable
to every user on the system.  Not very secure, but this will allow confirmation
that uploads are working properly.  Check with the system admin on how to lower
these permissions to something more secure.

Point your browser to: http://yourdomain.com/admin/setup.php to continue the
configuration.

Setup.php
Setup.php will evaluate your system and the location where PHPCatalog has been
installed.  It will attempt to connect to your specified database and create and
delete a test table to verify that it has the required permissions on the
database.

Setup.php follows through five steps.  The last step installs the default admin
user according to the values submitted.  The user is then instructed to remove
the setup.php file and go to the admin configure area to complete the
installation.   The setup.php is a security hazard to leave in the admin
directory.  You should probably keep a copy of it somewhere just in case the
setup didn't complete properly, but it should be move out of public web space.

Point your browser to: http://yourdomain.com/catalog/admin/config.php to finish
the configuration steps.

The admin configure page allows you to specify the final configuration variables
such as the number of columns to display in listing views, the numbering format
for currency figures, the currency symbol and others for additional modules.
They are all well documented in the configure page.

Confirm that the your webserver can write to the PHPCatalog/image/products/ directory.
This will vary from system to system and is beyond the scope this document.  (Ed. note...
_please_ do not ask for help with this, seek assitance from you server admin or hosting
company, we do not know how a particular server is set up and frankly don't have the
time to help with this for a free application.)  On unix/linux systems execute the following
command if you absolutely cannot get the images to upload (execute from the command line
in the PHPCatalog/image/ directory):

chmod -R 777 *

You just made everything under the directory PHPCatalog/image/ readable/writeable to
every user on the system...  Not very secure, but this will allow confirmation that
uploads are working properly.  Check with the system admin on how to lower these permissions
to something more secure.

Point your browser to: http://www.domain.com/PHPCatalog/admin/setup.php to finish the
configuration steps.


TEMPLATE SYSTEM

All template file names are commented in the html source.  To find what template file
controls a certain page area look at the html source of the page in your browser.
Surrounding each template files html, are output within html comment tags as below:

<!-- START tmpl.detail.inc -->
... html for file /include/template/tmpl.detail.inc ....
<!-- FINISH tmpl.detail.inc -->

<!-- START cart/tmpl.detail.inc -->
... html for file /include/module/cart/template/tmpl.detail.inc ....
<!-- FINISH cart/tmpl.detail.inc -->

***** Security Notes *****
Not all webservers are configured to parse .inc files as PHP files.  You may need to
add add a .htaccess file to $CFG->wwwroot to correct this problem.  Putting the following
line in a file named .htaccess may work on your system:

AddType  application/x-httpd-php .inc

No support is built in to secure the admin directory from unauthorized access.  We suggest
you use http authentication by adding a .htaccess to the admin directory.  Contact your
hosting provider for information on password protecting webserver directories.  It is also
possible to rename the admin directory something like:

PHPCatalog/adminzzz12/  (obviously change this to a specific random name for your site)

to 'hide' the directory from public access.  This method should not be considered secure
enough for the sole means of security, but provides a more secure addition to the http
authentication referred to above.


SUPPORT

Full support information may be found at:
http://siliconsys.com/content/support/

For this free version we provide support through our public forums, basic registration or
Priority Support only.

Forum:
http://siliconsys.com/forum/

Basic Registration:
Basic Registration costs $25 and includes a 15 plus page PDF covering installation and
customization of PHPCatalog in depth plus access to our PHPCatalog+ Knowledge Base where
you'll find articles culled from support tickets, prior customizations and user requests.

http://siliconsys.com/content/applications/phpcatalog/register/

Registration also entitles you to installation of PHPCatalog on your server for an
additional $25.


Priority Support:
http://siliconsys.com/content/support/priority/




FUTURE

