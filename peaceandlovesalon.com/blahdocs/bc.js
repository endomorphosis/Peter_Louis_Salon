/**********************************************************
 * Part of the Blah Core Code ...                         *
 * Copyrighted � 2001-2004 e-blah! - All Rights Reserved. *
 * TO BE USED WITH E-BLAH ONLY, DO NOT USE WITH ANY OTHER *
 * SOFTWARE WITHOUT PRIOR WRITTEN PERMISSION.             *
 * BC Version: Platinum     Last Update: August 3rd, 2004 *
 **********************************************************/

function use(u,c) {
	var bb_obj = document.msend.message;

	if(document.all && window.navigator.userAgent.toLowerCase().indexOf('opera')) {
		bb_obj.focus();
		selecttext = document.selection.createRange().text;

		if((selecttext && c) || bb_obj.isTextEdit) {
			if(bb_obj.isTextEdit && !selecttext) {
				var sel = document.selection;
				var nrn = sel.createRange();
				nrn.colapse;
				nrn.text = u;
				if(c) { nrn.text = c; }
			} else {
				document.selection.createRange().text = u+selecttext+c;
			}
		} else {
			bb_obj.value += u;
			if(c) { bb_obj.value += c; }
		}
	} else {
		if(!c) { c = ''; }
		bb_obj.value += u+c;
	}
	bb_obj.focus();
	selecttext = '';
}

function openuse(u) {
	var bbo_obj = opener.document.msend.message;
	bbo_obj.value += u;
}

function AddNewValue(bc,thenewvalue) {
	if(bc != '' && thenewvalue != '') {
		use("["+bc+"="+thenewvalue+"]","[/"+bc+"]");
		document.msend.color.value = '';
		document.msend.face.value = '';
		document.msend.size.value = '';
		document.msend.glow.value = '';
		document.msend.shadow.value = '';
	}
}

function funclu (lookup) {
	document.getElementById('about').innerHTML = eval('func_'+lookup);
}