<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="fr-FR">
<head profile="http://gmpg.org/xfn/11">

<!-- HTML Title -->
<title>FaceBook Like - jQuery and autosuggest Search Engine</title>

<!-- META TAGS-->
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="keywords" content="Web2, search engine, autosuggest, facebook, jquery, autocompletion, search" />

<!-- Loading jQuery Framework -->
<script type="text/javascript" src="lib/jquery-1.2.1.pack.js"></script>	

<!-- Autosuggest module -->
<script type="text/javascript" src="lib/jquery.watermarkinput.js"></script>	
<link rel="stylesheet" href="lib/autosuggest/autosuggest_inquisitor.css" type="text/css" media="screen" charset="utf-8">	
 
<!-- SearchBar CSS -->
<style type='text/css'>
	body {
		color:#000000;
		font-family:Arial,Helvetica,sans-serif;
		font-size:12px;
		margin:10px 40px;
	}
	a {
		color:#18479B;
		cursor:pointer;
		outline-color:invert;
		outline-style:none;
		outline-width:medium;
		text-decoration:underline;
	}
	a:hover {
		color:#CA0002;
		outline-color:invert;
		outline-style:none;
		outline-width:medium;
		text-decoration:underline;
	}	
	h1 {
		color:#5D5D5D;
		font-family:Arial,sans-serif;
		font-size:25px;
		font-weight:bold;
		padding:15px 0px 0px;	
	}
	h1 em {
		font-size:20px;
	}	
	h2 {
		color:#333333;
		font-family:Arial,sans-serif;
		font-size:small;
		font-weight:bold;
		padding:0px;	
	}
	h2 p {
		color:#333333;
		font-family:Arial,sans-serif;
		font-size:small;
		font-weight:normal;	
	}	
	h3 {
		color:#333333;
		font-family:Arial,sans-serif;
		font-size:13px;
		font-weight:bold;
		padding:0px 0px;	
		margin:5px 0px;
	}	
	.footer {
		color:#666666;
		font-family:Arial,sans-serif;
		font-size:13px;
		font-weight:bold;
		padding:0px 0px;	
		margin:5px 0px;
		text-align:left;
		width:40%;
	}		
	.footer hr {
		color:#666666;
		border:1px solid #666666;
		text-align:left;
		width:80%;
	}			
	.search_example {
		margin:0px 20px 0px 10px;
	}
	.search_bar {
		position:relative;	
		color:#000000;
		font-weight:bold;
		margin:8px 0px;
		padding:0px 5px;
		height:40px;
	}
	.search_bar form {
		display:inline;
	}	
	.search_bar input {
		font-family:Arial,Helvetica,sans-serif;
		font-size:12px;
	}	
	.search_bar ul {
		line-height:19px;
		list-style-image:none;
		list-style-position:outside;
		list-style-type:none;
		margin:3px 0pt 0pt;
		padding:0pt;
		z-index:10000000;
	}	
	.search_bar li {
		color:#333333;
		float:left;
		font-family:Arial,Helvetica,sans-serif;
		font-size:12px;
		font-weight:bold;
		margin-left:5px;
		margin-right:0px;
		width:auto;
	}	
	.search_bar  input.search_txt {
		background:white url(img/searchglass.png) no-repeat scroll 3px 4px;
		border:1px solid #95A5C6;
		color:#000000;
		font-weight:normal;
		padding:2px 0px 2px 17px;
	}	
	.search_bar input.searchBtnOK {
		background:white none repeat scroll 0%;
		border:1px solid #95A5C6;
		color:#000000;
		font-weight:bold;
		padding:1px;
	}	
	
	.search_response {
		position:relative;
		border:2px solid #f8e89d;
		padding:10px;
		padding-left:50px;
		margin:10px;
		background:#ffffff url(img/kghostview.png) no-repeat 0px 10px;
	}
</style>

<!-- Init AutoSuggest -->
<script type="text/javascript">
/** AutoSuggest Packed JS **/
if(typeof(bsn)=="undefined")_b=bsn={};if(typeof(_b.Autosuggest)=="undefined")_b.Autosuggest={};else alert("Autosuggest is already set!");_b.AutoSuggest=function(id,param){if(!document.getElementById)return 0;this.fld=_b.DOM.gE(id);if(!this.fld)return 0;this.sInp="";this.nInpC=0;this.aSug=[];this.iHigh=0;this.oP=param?param:{};var k,def={minchars:1,meth:"get",varname:"input",className:"autosuggest",timeout:2500,delay:500,offsety:-5,shownoresults:true,noresults:"No results!",maxheight:250,cache:true,maxentries:25};for(k in def){if(typeof(this.oP[k])!=typeof(def[k]))this.oP[k]=def[k]}var p=this;this.fld.onkeypress=function(ev){return p.onKeyPress(ev)};this.fld.onkeyup=function(ev){return p.onKeyUp(ev)};this.fld.setAttribute("autocomplete","off")};_b.AutoSuggest.prototype.onKeyPress=function(ev){var key=(window.event)?window.event.keyCode:ev.keyCode;var RETURN=13;var TAB=9;var ESC=27;var bubble=1;switch(key){case RETURN:this.setHighlightedValue();bubble=0;break;case ESC:this.clearSuggestions();break}return bubble};_b.AutoSuggest.prototype.onKeyUp=function(ev){var key=(window.event)?window.event.keyCode:ev.keyCode;var ARRUP=38;var ARRDN=40;var bubble=1;switch(key){case ARRUP:this.changeHighlight(key);bubble=0;break;case ARRDN:this.changeHighlight(key);bubble=0;break;default:this.getSuggestions(this.fld.value)}return bubble};_b.AutoSuggest.prototype.getSuggestions=function(val){if(val==this.sInp)return 0;_b.DOM.remE(this.idAs);this.sInp=val;if(val.length<this.oP.minchars){this.aSug=[];this.nInpC=val.length;return 0}var ol=this.nInpC;this.nInpC=val.length?val.length:0;var l=this.aSug.length;if(this.nInpC>ol&&l&&l<this.oP.maxentries&&this.oP.cache){var arr=[];for(var i=0;i<l;i++){if(this.aSug[i].value.substr(0,val.length).toLowerCase()==val.toLowerCase()||this.aSug[i].info=='as_header')arr.push(this.aSug[i])}this.aSug=arr;this.createList(this.aSug);return false}else{var pointer=this;var input=this.sInp;clearTimeout(this.ajID);this.ajID=setTimeout(function(){pointer.doAjaxRequest(input)},this.oP.delay)}return false};_b.AutoSuggest.prototype.doAjaxRequest=function(input){if(input!=this.fld.value)return false;var pointer=this;if(typeof(this.oP.script)=="function")var url=this.oP.script(encodeURIComponent(this.sInp));else var url=this.oP.script+this.oP.varname+"="+encodeURIComponent(this.sInp);if(!url)return false;var meth=this.oP.meth;var input=this.sInp;var onSuccessFunc=function(req){pointer.setSuggestions(req,input)};var onErrorFunc=function(status){alert("AJAX error: "+status)};var myAjax=new _b.Ajax();myAjax.makeRequest(url,meth,onSuccessFunc,onErrorFunc)};_b.AutoSuggest.prototype.setSuggestions=function(req,input){if(input!=this.fld.value)return false;this.aSug=[];if(this.oP.json){var jsondata=eval('('+req.responseText+')');for(var i=0;i<jsondata.results.length;i++){this.aSug.push({'id':jsondata.results[i].id,'value':jsondata.results[i].value,'info':jsondata.results[i].info})}}else{var xml=req.responseXML;var results=xml.getElementsByTagName('results')[0].childNodes;for(var i=0;i<results.length;i++){if(results[i].hasChildNodes())this.aSug.push({'id':results[i].getAttribute('id'),'value':results[i].childNodes[0].nodeValue,'info':results[i].getAttribute('info')})}}this.idAs="as_"+this.fld.id;this.createList(this.aSug)};_b.AutoSuggest.prototype.createList=function(arr){var pointer=this;_b.DOM.remE(this.idAs);this.killTimeout();if(arr.length==0&&!this.oP.shownoresults)return false;var div=_b.DOM.cE("div",{id:this.idAs,className:this.oP.className});var hcorner=_b.DOM.cE("div",{className:"as_corner"});var hbar=_b.DOM.cE("div",{className:"as_bar"});var header=_b.DOM.cE("div",{className:"as_header"});header.appendChild(hcorner);header.appendChild(hbar);div.appendChild(header);var ul=_b.DOM.cE("ul",{id:"as_ul"});for(var i=0;i<arr.length;i++){if(arr[i].info=="plugin_header"){var li=_b.DOM.cE("li",{className:"as_header"},arr[i].value);ul.appendChild(li);i++}var val=arr[i].value;var st=val.toLowerCase().indexOf(this.sInp.toLowerCase());var output=val.substring(0,st)+"<em>"+val.substring(st,st+this.sInp.length)+"</em>"+val.substring(st+this.sInp.length);var span=_b.DOM.cE("span",{},output,true);if(arr[i].info!=""){var br=_b.DOM.cE("br",{});span.appendChild(br);var small=_b.DOM.cE("small",{},arr[i].info);span.appendChild(small)}var a=_b.DOM.cE("a",{href:"#"});var tl=_b.DOM.cE("span",{className:"tl"}," ");var tr=_b.DOM.cE("span",{className:"tr"}," ");a.appendChild(tl);a.appendChild(tr);a.appendChild(span);a.name=i+1;a.onclick=function(){pointer.setHighlightedValue();return false};a.onmouseover=function(){pointer.setHighlight(this.name)};var li=_b.DOM.cE("li",{},a);ul.appendChild(li)}if(arr.length==0&&this.oP.shownoresults){var li=_b.DOM.cE("li",{className:"as_warning"},this.oP.noresults);ul.appendChild(li)}div.appendChild(ul);var fcorner=_b.DOM.cE("div",{className:"as_corner"});var fbar=_b.DOM.cE("div",{className:"as_bar"});var footer=_b.DOM.cE("div",{className:"as_footer"});footer.appendChild(fcorner);footer.appendChild(fbar);div.appendChild(footer);var pos=_b.DOM.getPos(this.fld);div.style.left=pos.x+"px";div.style.top=(pos.y+this.fld.offsetHeight+this.oP.offsety)+"px";div.style.width=this.fld.offsetWidth+"px";div.onmouseover=function(){pointer.killTimeout()};div.onmouseout=function(){pointer.resetTimeout()};document.getElementsByTagName("body")[0].appendChild(div);this.iHigh=0;var pointer=this;this.toID=setTimeout(function(){pointer.clearSuggestions()},this.oP.timeout)};_b.AutoSuggest.prototype.changeHighlight=function(key){var list=_b.DOM.gE("as_ul");if(!list)return false;var n;if(key==40)n=this.iHigh+1;else if(key==38)n=this.iHigh-1;if(n>list.childNodes.length)n=list.childNodes.length;if(n<1)n=1;this.setHighlight(n)};_b.AutoSuggest.prototype.setHighlight=function(n){var list=_b.DOM.gE("as_ul");if(!list)return false;if(this.iHigh>0)this.clearHighlight();this.iHigh=Number(n);if(list.childNodes[this.iHigh-1].className!="as_header")list.childNodes[this.iHigh-1].className="as_highlight";this.killTimeout()};_b.AutoSuggest.prototype.clearHighlight=function(){var list=_b.DOM.gE("as_ul");if(!list)return false;if(this.iHigh>0){if(list.childNodes[this.iHigh-1].className!="as_header")list.childNodes[this.iHigh-1].className="";this.iHigh=0}};_b.AutoSuggest.prototype.setHighlightedValue=function(){if(this.iHigh){this.sInp=this.fld.value=this.aSug[this.iHigh-1].value;this.fld.focus();if(this.fld.selectionStart)this.fld.setSelectionRange(this.sInp.length,this.sInp.length);this.clearSuggestions();if(typeof(this.oP.callback)=="function")this.oP.callback(this.aSug[this.iHigh-1])}};_b.AutoSuggest.prototype.killTimeout=function(){clearTimeout(this.toID)};_b.AutoSuggest.prototype.resetTimeout=function(){clearTimeout(this.toID);var pointer=this;this.toID=setTimeout(function(){pointer.clearSuggestions()},1000)};_b.AutoSuggest.prototype.clearSuggestions=function(){this.killTimeout();var ele=_b.DOM.gE(this.idAs);var pointer=this;if(ele){var fade=new _b.Fader(ele,1,0,250,function(){_b.DOM.remE(pointer.idAs)})}};if(typeof(_b.Ajax)=="undefined")_b.Ajax={};_b.Ajax=function(){this.req={};this.isIE=false};_b.Ajax.prototype.makeRequest=function(url,meth,onComp,onErr){if(meth!="POST")meth="GET";this.onComplete=onComp;this.onError=onErr;var pointer=this;if(window.XMLHttpRequest){this.req=new XMLHttpRequest();this.req.onreadystatechange=function(){pointer.processReqChange()};this.req.open("GET",url,true);this.req.send(null)}else if(window.ActiveXObject){this.req=new ActiveXObject("Microsoft.XMLHTTP");if(this.req){this.req.onreadystatechange=function(){pointer.processReqChange()};this.req.open(meth,url,true);this.req.send()}}};_b.Ajax.prototype.processReqChange=function(){if(this.req.readyState==4){if(this.req.status==200){this.onComplete(this.req)}else{this.onError(this.req.status)}}};if(typeof(_b.DOM)=="undefined")_b.DOM={};_b.DOM.cE=function(type,attr,cont,html){var ne=document.createElement(type);if(!ne)return 0;for(var a in attr)ne[a]=attr[a];var t=typeof(cont);if(t=="string"&&!html)ne.appendChild(document.createTextNode(cont));else if(t=="string"&&html)ne.innerHTML=cont;else if(t=="object")ne.appendChild(cont);return ne};_b.DOM.gE=function(e){var t=typeof(e);if(t=="undefined")return 0;else if(t=="string"){var re=document.getElementById(e);if(!re)return 0;else if(typeof(re.appendChild)!="undefined")return re;else return 0}else if(typeof(e.appendChild)!="undefined")return e;else return 0};_b.DOM.remE=function(ele){var e=this.gE(ele);if(!e)return 0;else if(e.parentNode.removeChild(e))return true;else return 0};_b.DOM.getPos=function(e){var e=this.gE(e);var obj=e;var curleft=0;if(obj.offsetParent){while(obj.offsetParent){curleft+=obj.offsetLeft;obj=obj.offsetParent}}else if(obj.x)curleft+=obj.x;var obj=e;var curtop=0;if(obj.offsetParent){while(obj.offsetParent){curtop+=obj.offsetTop;obj=obj.offsetParent}}else if(obj.y)curtop+=obj.y;return{x:curleft,y:curtop}};if(typeof(_b.Fader)=="undefined")_b.Fader={};_b.Fader=function(ele,from,to,fadetime,callback){if(!ele)return 0;this.e=ele;this.from=from;this.to=to;this.cb=callback;this.nDur=fadetime;this.nInt=50;this.nTime=0;var p=this;this.nID=setInterval(function(){p._fade()},this.nInt)};_b.Fader.prototype._fade=function(){this.nTime+=this.nInt;var ieop=Math.round(this._tween(this.nTime,this.from,this.to,this.nDur)*100);var op=ieop/100;if(this.e.filters){try{this.e.filters.item("DXImageTransform.Microsoft.Alpha").opacity=ieop}catch(e){this.e.style.filter='progid:DXImageTransform.Microsoft.Alpha(opacity='+ieop+')'}}else{this.e.style.opacity=op}if(this.nTime==this.nDur){clearInterval(this.nID);if(this.cb!=undefined)this.cb()}};_b.Fader.prototype._tween=function(t,b,c,d){return b+((c-b)*(t/d))};

/** Init autosuggest on Search Input **/
jQuery(function() {

	//==================== Search With all plugins =================================================
	// Unbind form submit
	$('.home_searchEngine').bind('submit', function() {return true;} ) ;
	
	//==================== Search With "Country" plugin =================================================	
	// Set autosuggest options with all plugins activated
	var options = {
		script:"AjaxSearch/_doAjaxSearch.action.php?json=true&plugin=country&limit=8&",
		varname:"input",
		json:true,						// Returned response type
		shownoresults:true,				// If disable, display nothing if no results
		noresults:"No Results",			// String displayed when no results
		maxresults:8,					// Max num results displayed
		cache:false,					// To enable cache
		minchars:2,						// Start AJAX request with at leat 2 chars
		timeout:100000,					// AutoHide in XX ms
		callback: function (obj) { 		// Callback after click or selection
			// For example use :
						
			// Build HTML
			//var html = "ID : " + obj.id + "<br>Main Text : " + obj.value + "<br>Info : " + obj.info;
			//$('#input_search_country_response').html(html).show() ;
			
			// => TO submit form (general use)
			$('#search_country_value').val(obj.id); 
			//$('#form_search_country').submit(); 
		}
	};
	// Init autosuggest
	var as_json = new bsn.AutoSuggest('input_search_country', options);
	
	// Display a little watermak	
	$("#input_search_country").Watermark("ex : France, Canada...");	
});
</script>

</head>
	<body>
		<h2>&nbsp;</h2>
<br>
<div class="search_example"> 
			<h3>Search Peterlouis</h3>
			<div class="search_bar">
			<form method="post" action="test.php" class="home_searchEngine" id="form_search_country"enctype="multipart/form-data" >
			<input type="hidden" id="search_country_value" name="search_value">
			<ul>			
			<li><input type="text" size="24" name="search_txt" id="input_search_country" class="search_txt"></li>
      <li>
        <select size="1" name="peteroption" id="peteroption">
            <option value="0" selected>All Products</option>
            <option value="1">Peter Louis</option>
            <option value="2">Phytologie/Phyto</option>
            <option value="3">Rene Furterer</option>
            <option value="4">Crede</option>
            <option value="5">jf lazartigue</option>
            <option value="6">Philip B</option>
            <option value="7">GHD</option>
            <option value="8">Japanese Products</option>
            <option value="9">Unique Styling Products</option>
            <option value="10">Great Lengths/NXT</option>
        </select>
      </li>
			<li><input type="submit" class="searchBtnOK" value="Ok"></li>
			</ul>
			</form>		
			</div>		
			
			<div style="clear:both;margin:0px 10px;height:5px;"></div>
					
			<div style="margin-top:0px;">
				<div class="search_response" style="display:none;" id="input_search_country_response"></div>
			</div>
		</div>	
		
		</body>
</html>