<?php
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' ); 
/* Joomla! 1.5 One Page Checkout Module for VirtueMart
*
* @version $Id: get_final_confirmation.tpl.php 05-24-2011
 * author: Polished Geek
 * Website: PolishedGeek.com
 * Email: info@PolishedGeek.com
 * Phone: 919-374-2425
 *
 * license http://www.gnu.org/copyleft/gpl.html GNU/GPL
 *
*/

$db = new ps_DB();

?>
<div id="cust_note_section" align="center">
<?php 

// instructions prompt
if ($opcoConfig->confirmPlaceOrder->showOrderInstructionsPrompt) 
{
?>
<?php echo $VM_LANG->_('PHPSHOP_CHECKOUT_CUSTOMER_NOTE') ?>:<br />
<textarea title="<?php echo $VM_LANG->_('PHPSHOP_CHECKOUT_CUSTOMER_NOTE') ?>" cols="50" rows="5" name="customer_note"></textarea>
<br />
<?php
}

// terms of service
if (PSHOP_AGREE_TO_TOS_ONORDER == '1') 
{
?>
<br />
<input type="checkbox" name="agreed" value="1" class="inputbox" />&nbsp;&nbsp;
<?php 
    $link = $mosConfig_live_site .'/index2.php?option=com_virtuemart&amp;page=shop.tos&amp;pop=1&amp;Itemid='. $Itemid;
    $text = $VM_LANG->_('PHPSHOP_I_AGREE_TO_TOS');
    echo vmPopupLink( $link, $text );
    echo '<br />';
}
else
{
    echo '<input type="hidden" name="agreed" value="1" />';
}
?>
</div>
<?php 

// legal info link
if( VM_ONCHECKOUT_SHOW_LEGALINFO == '1' ) {
	$link =  sefRelToAbs('index2.php?option=com_content&amp;task=view&amp;id='.VM_ONCHECKOUT_LEGALINFO_LINK );
	$jslink = "window.open('$link', 'win2', 'status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;";
    
    if( @VM_ONCHECKOUT_LEGALINFO_SHORTTEXT=='' || !defined('VM_ONCHECKOUT_LEGALINFO_SHORTTEXT')) {
		$text = $VM_LANG->_('VM_LEGALINFO_SHORTTEXT');
	} else {
		$text = VM_ONCHECKOUT_LEGALINFO_SHORTTEXT;
	}
?>
<div class="legalinfo"><?php echo sprintf( $text, $link, $jslink ); ?></div>
<br />
<?php
}
?>
<div id="submit_section" align="center">
<?php

// calculate tax button
if ($opcoConfig->confirmPlaceOrder->showCalculateTaxButton) 
{
?>
<input type="button" name="calculatetax_button" class="button" id="calculatetax_button" value="<?php echo JText::_('CALCULATE_TAX_BUTTON_TEXT'); ?>" onclick="javascript:calculatetax_ajax();" />
<?php 
}

// submit order button
?>
<input type="submit" onclick="return( submit_order( this.form ) );" class="button" name="formSubmit" id="formSubmitButton" value="<?php echo JText::_('Place Order') ?>" <?php if($opcoConfig->confirmPlaceOrder->showCalculateTaxButton) {echo 'style="visibility: hidden; display: none;"'; } ?>/>
<?php

echo '<img src="' . JURI::root(true).'/modules/mod_virtuemart_onepagecheckout/assets/images/spinner.gif" class="ajaxprogress" style="display: none;" />';
echo '</div>';

// final submit javascript handling
if(  PSHOP_AGREE_TO_TOS_ONORDER == '1' ) 
{
	echo vmCommonHTML::scriptTag('', "function submit_order( form ) {
    if (!form.agreed.checked) {
        alert( \"". $VM_LANG->_('PHPSHOP_AGREE_TO_TOS',false) ."\" );
        return false;
    }
    else {
        return submit_allinone();
    }
}" );
} else {
	echo vmCommonHTML::scriptTag('', "function submit_order( form ) { return submit_allinone();  }" );
}
?>
