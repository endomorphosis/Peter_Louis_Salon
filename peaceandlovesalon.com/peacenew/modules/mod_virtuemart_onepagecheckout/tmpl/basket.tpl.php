<?php if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' );
/* Joomla! 1.5 One Page Checkout Module for VirtueMart
*
* @version $Id: basket.tpl.php 05-24-2011
 * author: Polished Geek
 * Website: PolishedGeek.com
 * Email: info@PolishedGeek.com
 * Phone: 919-374-2425
 *
 * license http://www.gnu.org/copyleft/gpl.html GNU/GPL
 *
*/

if($empty_cart) { ?>
    
    <div style="margin: 0 auto;">
    <?php if(!$vmMinicart) { ?>
        <a href="http://virtuemart.net/" target="_blank">
        <img src="<?php echo (strrpos($mm_action_url, '/') == (strlen($mm_action_url) - 1) ? $mm_action_url : $mm_action_url . '/') ?>components/com_virtuemart/shop_image/ps_image/menu_logo.gif" alt="VirtueMart" width="80" border="0" /></a>
        <br />
    <?php }
    echo $VM_LANG->_('PHPSHOP_EMPTY_CART') ?>
    </div>
<?php } 
else {
?>
<table id="opcoCartUpdateTable">
  <tbody>
    <tr align="left" class="sectiontableheader">
      <th>Name</th>
<?php if ($opcoConfig->reviewCart->showProductSkus) { ?>
      <th>SKU</th>
<?php } ?>
	  <th>Price</th>
	  <th>Quantity / Update</th>
	  <th>Subtotal</th>
    </tr>
<?php
    // Loop through each row and build the table
    foreach( $minicart as $cart ) { 		
?>
  <tr valign="top" class="sectiontableentry1">
	<td><a href="<?php echo $cart['url'] ?>"><?php echo $cart['product_name'] ?></a>
	    <?php echo (!empty($cart['attributes']) ? ('<br />'.$cart['attributes']) : ''); ?>
	</td>
<?php if ($opcoConfig->reviewCart->showProductSkus) { ?>
	    	<td><?php echo $cart['sku']; ?></td>
<?php } ?>
	<td align="right"><?php echo $cart['product_price'] ?></td>
	<td>
    	<?php echo $cart['update_form'] ?>
    	<?php echo $cart['delete_form'] ?>
	</td>
	<td align="right"><?php echo $cart['price'] ?></td>
  </tr>
<?php 
    }
?>
</tbody></table>
<?php
}

if(!$vmMinicart) { ?>
    <hr style="clear: both;" />
<?php } ?>
<div style="float: left;" >
<?php echo $total_products ?>
</div>
<div style="float: right;">
<?php echo $total_price ?>
</div>
<div style="clear: both;"></div>
<?php echo $saved_cart ?>
