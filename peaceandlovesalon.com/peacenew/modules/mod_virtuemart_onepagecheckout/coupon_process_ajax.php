<?php
/* Joomla! 1.5 One Page Checkout Module for VirtueMart
*
* @version $Id: coupon_process_ajax.php 05-24-2011
 * author: Polished Geek
 * Website: PolishedGeek.com
 * Email: info@PolishedGeek.com
 * Phone: 919-374-2425
 *
 * license http://www.gnu.org/copyleft/gpl.html GNU/GPL
 *
*/


global $_SESSION, $_REQUEST;

define( '_JEXEC', 1 );
define( 'DS', DIRECTORY_SEPARATOR );
define('JPATH_BASE', dirname('..'.DS.'..'.DS.'configuration.php'));

require_once ( JPATH_BASE .DS.'includes'.DS.'defines.php' );
require_once ( JPATH_BASE .DS.'includes'.DS.'framework.php' );

$mainframe =& JFactory::getApplication('site');
$mainframe->initialise();

$session =& JFactory::getSession();

$user = JFactory::getUser();

$username = $user->name;

global $VM_LANG, $vm_mainframe, $sess, $mm_action_url, $_SESSION, $db, $_REQUEST, $GLOBALS, $ps_html, $VM_LANG, $my, $vm_mainframe, $mainframe;

if($user->id > 0) {
	$_SESSION['auth']['user_id'] = $user->id;
	$auth = $_SESSION['auth'];
}


$vmCompatFile = JPATH_ADMINISTRATOR.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_virtuemart'.DIRECTORY_SEPARATOR.'compat.joomla1.5.php';
if(file_exists($vmCompatFile)) {
	require_once($vmCompatFile);
	$my = $GLOBALS['my'];
}

$vmCfgFile = JPATH_ADMINISTRATOR.DS.'components'.DS.'com_virtuemart'.DS.'virtuemart.cfg.php';
	if(file_exists($vmCfgFile))
		defined('ADMINPATH') or require_once($vmCfgFile);

$vmGlobalFile = JPATH_ADMINISTRATOR.DS.'components'.DS.'com_virtuemart'.DS.'global.php';
if(file_exists($vmGlobalFile)) {
	require_once($vmGlobalFile);
}



$vmParserFile = JPATH_SITE.DS.'components'.DS.'com_virtuemart'.DS.'virtuemart_parser.php';
if( file_exists($vmParserFile)) {
	require_once( $vmParserFile);
}



$vmDBFile = JPATH_ADMINISTRATOR.DS.'components'.DS.'com_virtuemart'.DS.'classes'.DS.'ps_database.php';

$basket_html = '';


////require_once(CLASSPATH. 'ps_product.php' );
////$ps_product = new ps_product;
////require_once(CLASSPATH. 'ps_shipping_method.php' );
////require_once(CLASSPATH. 'ps_checkout.php' );
////$ps_checkout = new ps_checkout;

$language =& JFactory::getLanguage();
$language->load('mod_virtuemart_onepagecheckout', JPATH_BASE, $language->getTag());

require_once ( 'mod_load_ajax.php' );

if(file_exists($vmDBFile)) {
	require_once($vmDBFile);


	if($_SESSION['auth']['user_id'] != 0) {
		$db = new ps_DB();
		$q =  "SELECT * FROM #__{vm}_user_info
                        WHERE user_id='" . $_SESSION['auth']['user_id'] . "'
                        AND address_type='BT' ";
		$db->query($q);
		$db->next_record();
		$ship_to_info_id = $db->f("user_info_id");

	} else {
		$db = null;
		$ship_to_info_id = "";
	}


	$shipping_rate_id = null;

	if(isset($_REQUEST['shipping_rate_id'])) {
		$shipping_rate_id = $_REQUEST['shipping_rate_id'];
	}
	
	$payment_method_id = null;
	if(isset($_REQUEST['payment_method_id'])) {
		$payment_method_id = $_REQUEST['payment_method_id'];
	}
}


$vmCouponFile = JPATH_ADMINISTRATOR.DS.'components'.DS.'com_virtuemart'.DS.'classes'.DS.'ps_coupon.php';

	
if(file_exists($vmCouponFile)) 
{
	$d = $_REQUEST;

	require_once($vmCouponFile);

	$couponClass = new ps_coupon();

	if (!$couponClass->process_coupon_code($d)) 
	{
		$couponInvalid = true;
		$couponInvalidMessage = $GLOBALS['coupon_error'];
	}
}

echo '<br><div id=couponsection>';
require('coupon_ajax.php');
echo '</div><br>';

require_once('checkoutTotals_ajax.php');
?>
