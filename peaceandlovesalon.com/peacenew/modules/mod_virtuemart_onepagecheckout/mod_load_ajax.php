<?php
/* Joomla! 1.5 One Page Checkout Module for VirtueMart
*
* @version $Id: mod_load_ajax.php 05-24-2011
 * author: Polished Geek
 * Website: PolishedGeek.com
 * Email: info@PolishedGeek.com
 * Phone: 919-374-2425
 *
 * license http://www.gnu.org/copyleft/gpl.html GNU/GPL
 *
*/

defined('_JEXEC') or define( '_JEXEC', 1 );

require_once ( JPATH_BASE .DS.'includes'.DS.'defines.php' );
require_once ( JPATH_BASE .DS.'includes'.DS.'framework.php' );
require_once ( JPATH_BASE .DS.'libraries'.DS.'joomla'.DS.'language'.DS.'language.php');
require_once ( JPATH_BASE .DS.'libraries'.DS.'joomla'.DS.'language'.DS.'helper.php');

$mainframe = &JFactory::getApplication('site');
$mainframe->initialise();

jimport( 'joomla.plugin.plugin' );
jimport( 'joomla.application.module.helper');

if(isset($_REQUEST['opco_title'])) {
	$opco_title = $_REQUEST['opco_title'];
} else {
	$opco_title = '';
}
$module = JModuleHelper::getModule( 'virtuemart_onepagecheckout', $opco_title );

// Get module parameters
$params = new JParameter($module->params);

require_once('lib'.DS.'opco_config.php' );
require_once('lib'.DS.'style_helper.php' );

$opcoConfig = new OpcoConfig();
$opcoConfig->load($params);

$styleHelper = new StyleHelper();
$styleHelper->init($opcoConfig);

require_once(CLASSPATH. 'ps_product.php' );
$ps_product = new ps_product;
require_once(CLASSPATH. 'ps_shipping_method.php' );
require_once(CLASSPATH. 'ps_checkout.php' );
$ps_checkout = new ps_checkout;

$opcoConfig->updateCheckout($ps_checkout, isset($auth['user_id']) && $auth['user_id'] != '0');

// reset the display logger to suppress notifications
require_once(CLASSPATH . 'Log' . DS. 'null.php');
//$vmDisplayLogger = new vmLog_null('');	

?>
