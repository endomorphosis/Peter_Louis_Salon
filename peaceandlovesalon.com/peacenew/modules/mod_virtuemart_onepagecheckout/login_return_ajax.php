<?php
/* Joomla! 1.5 One Page Checkout Module for VirtueMart
*
* @version $Id: login_return_ajax.php 05-24-2011
 * author: Polished Geek
 * Website: PolishedGeek.com
 * Email: info@PolishedGeek.com
 * Phone: 919-374-2425
 *
 * license http://www.gnu.org/copyleft/gpl.html GNU/GPL
 *
*/

define( '_JEXEC', 1 );
define( 'DS', DIRECTORY_SEPARATOR );
define('JPATH_BASE', dirname('..'.DS.'..'.DS.'configuration.php'));

require_once ( JPATH_BASE .DS.'includes'.DS.'defines.php' );
require_once ( JPATH_BASE .DS.'includes'.DS.'framework.php' );

$mainframe =& JFactory::getApplication('site');
$mainframe->initialise();

$language =& JFactory::getLanguage();
$language->load('mod_virtuemart_onepagecheckout', JPATH_BASE, $language->getTag());

$session =& JFactory::getSession();

$user = JFactory::getUser();

$username = $user->name;

global $VM_LANG, $vm_mainframe, $sess, $mm_action_url, $_SESSION, $db, $_REQUEST, $GLOBALS, $ps_html, $VM_LANG, $my, $vm_mainframe, $mainframe, $auth, $page, $mosConfig_live_site, $cache_id;


//JHTML::script('onepage.js', 'modules/mod_virtuemart_onepagecheckout/', false);
//JHTML::script('joomla.javascript.js', 'includes/js/', true);
//JHTML::_('behavior.tooltip');
//JHTML::script('wz_tooltip.js', 'components/com_virtuemart/js/', true);


if($user->id > 0) {
	$_SESSION['auth']['user_id'] = $user->id;
	$auth = $_SESSION['auth'];
}


$vmCompatFile = JPATH_ADMINISTRATOR.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_virtuemart'.DIRECTORY_SEPARATOR.'compat.joomla1.5.php';
if(file_exists($vmCompatFile)) {
	require_once($vmCompatFile);
	$my = $GLOBALS['my'];

}

$vmCfgFile = JPATH_ADMINISTRATOR.DS.'components'.DS.'com_virtuemart'.DS.'virtuemart.cfg.php';
	if(file_exists($vmCfgFile))
		defined('ADMINPATH') or require_once($vmCfgFile);

$vmGlobalFile = JPATH_ADMINISTRATOR.DS.'components'.DS.'com_virtuemart'.DS.'global.php';
if(file_exists($vmGlobalFile)) {
	require_once($vmGlobalFile);
}

$mosConfig_live_site = SECUREURL;
$GLOBALS['mosConfig_live_site'] = $mosConfig_live_site;


/*$vmParserFile = JPATH_SITE.DS.'components'.DS.'com_virtuemart'.DS.'virtuemart_parser.php';
if( file_exists($vmParserFile)) {
	require_once( $vmParserFile);
}
 */


$vmDBFile = JPATH_ADMINISTRATOR.DS.'components'.DS.'com_virtuemart'.DS.'classes'.DS.'ps_database.php';

$basket_html = '';


////require_once(CLASSPATH. 'ps_product.php' );
////$ps_product = new ps_product;
////require_once(CLASSPATH. 'ps_shipping_method.php' );
////require_once(CLASSPATH. 'ps_checkout.php' );
////$ps_checkout = new ps_checkout;

require_once ( 'mod_load_ajax.php' );

if(file_exists($vmDBFile)) {
	require_once($vmDBFile);


	if($_SESSION['auth']['user_id'] != 0) {
		$db = new ps_DB();
		$q =  "SELECT * FROM #__{vm}_user_info
                        WHERE user_id='" . $_SESSION['auth']['user_id'] . "'
                        AND address_type='BT' ";
		$db->query($q);
		$db->next_record();
		$ship_to_info_id = $db->f("user_info_id");

	} else {
		$db = null;
		$ship_to_info_id = "";
	}


	$shipping_rate_id = null;

	if(isset($_REQUEST['shipping_rate_id'])) {
		$shipping_rate_id = $_REQUEST['shipping_rate_id'];
	}
	
	$payment_method_id = null;
	if(isset($_REQUEST['payment_method_id'])) {
		$payment_method_id = $_REQUEST['payment_method_id'];
	}
}


if($user->id > 0) {

	$auth = $_SESSION['auth'];

	echo "<div id=loginsection>".JText::_("Hello").", ". $user->name ."</div><br>";
	


echo '<div id=registersection>'; 
include('checkout_register_form.php');
echo '<input type="hidden" name="address_type_name" id="address_type_name_field" value="-default-" />';
echo '</div><br>';

echo '<div id=newshippingaddress>';
		$fields = ps_userfield::getUserFields( 'shipping' );
		ps_userfield::listUserFields( $fields, array(), $db, false );
		echo '<input type="hidden" name="checkout_this_step[]" value="CHECK_OUT_GET_SHIPPING_ADDR" />';
		echo '</div><br>';

echo '<div id=existingshippingaddress>';
$ps_checkout->ship_to_addresses_radio($auth["user_id"], "ship_to_info_id", $ship_to_info_id);
echo '</div><br>';


echo '<div id=payment>';
    require(JModuleHelper::getLayoutpath('mod_virtuemart_onepagecheckout', 'get_payment_method.tpl'));
    echo '<input type="hidden" name="checkout_this_step[]" value="CHECK_OUT_GET_PAYMENT_METHOD" />';
    echo '</div><br>';
 



echo "<script LANGUAGE=javascript>
	opcoHelper.hideById('logintoggle');
	opcoHelper.showById('registersection', '');
	";
	
	if ($opcoConfig->billingShipping->useJanrain)
	{
	    echo "
    		opcoHelper.hideById('loginFormJanrain');
    	";
	}
	
	echo "
	var ship_id = document.getElementsByName('ship_to_info_id');

	for(var j=0; j < ship_id.length; j++) {
		if(ship_id[j].value == '') {
			ship_id[j].value = '".$ship_to_info_id."';
		}
	}
    
    initShippingAddressArea();
    initPaymentMethodArea();
	removeBRTags('registersection');

	if(shippingEnabled() == true) {
		calculateshipping();
	} else {
		updatetotals();
	}	
</script><br>";




} else {
	echo '<div id=loginerror>';
	echo $styleHelper->showError(JText::_('Your username and password do not match or you do not have an account yet.'), '');
	echo "</div><br>";
}

?>
