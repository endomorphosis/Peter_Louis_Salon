<?php
/**
 * One Page Checkout Module for VirtueMart
 *
 * This plugin is used in conjuction with the One Page Checkout Module for VirtueMart. 
 * 
 * @author Polished Geek
 * @package vmonepagecheckout
 * @version $Revision: 1.0 
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */



// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.plugin.plugin' );

/**
 * One Page Checkout Module for VirtueMart plugin
 */
class plgSystemVmonepagecheckout extends JPlugin
{
        /**
         * Constructor
         *
         * For php4 compatability we must not use the __constructor as a constructor for plugins
         * because func_get_args ( void ) returns a copy of all passed arguments NOT references.
         * This causes problems with cross-referencing necessary for the observer design pattern.
         *
         * @access      protected
         * @param       object  $subject The object to observe
         * @param       array   $config  An array that holds the plugin configuration
         * @since       1.0
         */
        function plgSystemVmonepagecheckout( &$subject, $config )
        {
                parent::__construct( $subject, $config );

                // Do some extra initialisation in this constructor if required
        }

        /**
         * Do something onAfterInitialise 
         */
        function onAfterInitialise()
        {
        }

        /**
         * Do something onAfterRoute 
         */
        function onAfterRoute()
        {
        }

        /**
         * Do something onAfterDispatch 
         */
        function onAfterDispatch()
	{
		global $mainframe, $Itemid, $page, $option, $cart, $_SESSION, $_POST, $_REQUEST, $VM_CHECKOUT_MODULES, $ship_to_info_id, $vars, $auth;

		// if Admin side, just exit
		$application = JFactory::getApplication();
		if ($application->isAdmin()) { 
			return;
		}

		if($option != 'com_virtuemart')
		{
			return;
		}


		$coDoCheck = false;


		$plugin =& JPluginHelper::getPlugin( 'system', 'vmonepagecheckout' );
	
		$params = new JParameter($plugin->params);

		$currentPage = $page;




		switch($currentPage) {


		case 'checkout.index':

			$onepageCheckoutValue = 0;
			if(isset($_REQUEST['onepagecheckout'])) {
				if($_REQUEST['onepagecheckout'] == 1) {
					$onepageCheckoutValue = 1;
				}

				return;
			}



			$sessionSetForOnePage = false;

			if(isset($_SESSION['VMOnepageCheckout'])) 
				if($_SESSION['VMOnepageCheckout']  == "1")
					$sessionSetForOnePage = true;




			$coDoCheck = true;
			break;

		case 'checkout.confirm':
			$coDoCheck = true;
			break;
		}


	
 
		if($coDoCheck) {
			$application->redirect($params->get('new_checkout'));
		}


	}

        /**
         * Do something onAfterRender 
         */
        function onAfterRender()
        {
	}

		

}





?>
