function validate()
{
  var product_name, weight, dimensions, box_sizes;
  var dimensions_option, box_sizes_option;
  var ship_options, field_id, recnum;
  var current_recnum = -20;
  var i, n;

  with (document.forms[0])
  {
    n = elements.length;
    for (i = 0; i < n; i++)
    {
      if (elements[i].name.search(regexp(name_fieldnum)) == 0)
      {
        product_name = elements[i];
        if (product_name.value.length == 0)
        {
          alert(validate_msg1);
          product_name.focus();
          return false;
        }
      }
      else
      if (elements[i].name.search(regexp(weight_fieldnum)) == 0)
      {
        weight = elements[i];
      }
      else
      if (elements[i].name.search(regexp(ship_options_fieldnum)) == 0)
      {
        field_id = elements[i].name.split(':');
        recnum = field_id[0];

        if (recnum != current_recnum)
        {
          ship_options = i;
          current_recnum = recnum;
        }

        field_id.length = 0;
      }
      else
      if (elements[i].name.search(regexp(dimensions_fieldnum)) == 0)
      {
        dimensions = elements[i];
        dimensions_option = elements[ship_options + 1];
        if (dimensions_option.checked)
        {
          if (dimensions.value.length == 0)
          {
            alert(validate_msg2);
            dimensions.focus();
            return false;
          }

          if (dimensions_changed)
          {
            dimensions_changed = false;

            if (check_dimensions(dimensions) == false)
            {
              dimensions.focus();
              return false;
            }
          }
        }
      }
      else
      if (elements[i].name.search(regexp(box_sizes_fieldnum)) == 0)
      {
        box_sizes = elements[i];
        box_sizes_option = elements[ship_options + 3];

        if (box_sizes_option.checked)
        {
          if (box_sizes.selectedIndex == 0)
          {
            alert(validate_msg3);
            box_sizes.focus();
            return false;
          }
        }
      }
      else
      if (elements[i].name.search(regexp('import_option')) == 0)
      {
        if (elements[i+3].checked && elements[i+4].value.length == 0)
        {
          //alert(validate_msg4);  /* ignore for now */
          //elements[i].focus();
          //return false;
        }
      }
      else
      if (elements[i].name.search(regexp(fedex_containers_fieldnum)) == 0)
      {
        if (weight == undefined)
          return confirm(validate_msg5);

        if (check_fedex_container(elements[i], weight) == false)
          return false;
      }
      else
      if (elements[i].name.search(regexp(usps_containers_fieldnum)) == 0)
      {
        var size = elements[i + 1];
        var girth = elements[i + 2];

        if (dimensions_option == undefined)
          return confirm(validate_msg6);

        var dimensions = dimensions_option.checked || box_sizes_option.checked;

        if (check_usps_container(elements[i], weight, size, girth, dimensions) == false)
          return false;
      }
    }

    return true;
  }
}

function regexp(fieldid)
{
  return '[-]?[0-9]+:' + fieldid + '$';
}

function check_dimensions(dimensions)
{
  var measurements = dimensions.value.toLowerCase();
  var lwh = measurements.split('x');
  var length, width, height;

  if (lwh.length != 3)
  {
    alert(dimensions_invalid);
    dimensions.focus();
    return false;
  }

  length = Number(lwh[0]);
  width  = Number(lwh[1]);
  height = Number(lwh[2]);

  if (length < width || length < height)
  {
    alert(dimensions_warning);
    return false;
  }

  return true;
}

function check_fedex_container(container, weight)
{
  var weight_limit_exceeded = false;
  var type, limit, text1, text2;

  if (container.selectedIndex > 0)
  {
    type = container.value.slice(5);
    weight = Number(weight.value);

    if (type == 'ENVELOPE')
    {
      limit = 0.5;
      if (weight > limit)
        weight_limit_exceeded = true;
    }
    else
    if (type == 'PAK')
    {
      limit = 20.0;
      if (weight > limit)
        weight_limit_exceeded = true;
    }
    else
    if (type == '10KGBOX')
    {
      limit = 22.0;
      if (weight > limit)
        weight_limit_exceeded = true;
    }
    else
    if (type == '25KGBOX')
    {
      limit = 55.0;
      if (weight > limit)
        weight_limit_exceeded = true;
    }
    else
    {
      limit = 20.0;
      if (weight > limit)
        weight_limit_exceeded = true;
    }

    if (weight_limit_exceeded)
    {
      text1 = weight_limit_is + limit + pounds;
      text2 = fedex_msg1;
      alert(container.options[container.selectedIndex].text + ': ' + text1 + '\n' + text2);
      container.focus();
      return false;
    }
  }

  return true;
}

function check_usps_container(container, weight, size, girth, dimensions)
{
  var text, value, type, units;

  text = container.options[container.selectedIndex].text;

  if (text.indexOf('**') > 0)  // girth required
  {
    if (girth.value.length == 0)
    {
      alert(usps_msg1);
      girth.focus();
      return false;
    }
  }

  if (text.indexOf('*') > 0)  // dimensions required
  {
    if (dimensions == false)
    {
      alert(usps_msg2);
      container.focus();
      return false;
    }
  }

  if (container.selectedIndex > 0 && text.indexOf('*') < 0)
  {
    if (size.selectedIndex < 1)
    {
      alert(usps_msg3);
      size.focus();
      return false;
    }

    if (weight != undefined)
    {
      text  = container.options[container.selectedIndex].text;
      value = container.options[container.selectedIndex].value;
      weight = Number(weight.value);

      limit = (value == 'FLAT' || value == 'PARCEL')? 13/16 : 70.0;
      limit = (value == 'LETTER')? 3.5/16 : limit;

      if (weight > limit)
      {
        if (limit < 1.0)
        {
          limit = limit * 16;
          units = ounces;
        }
        else
          units = pounds;

        type = text.split(' *');
        alert('USPS ' + type[0] + ': ' + weight_limit_is + limit + units);
        container.focus();
        return false;
      }
    }
  }

  return true;
}
