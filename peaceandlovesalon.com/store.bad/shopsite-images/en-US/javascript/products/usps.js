function girth_illustrated(imageURL)
{
  if ((girthPopup == null || girthPopup.closed) && !girth_button_clicked)
    return;

  girth_button_clicked = false;

  if (girthPopup == null || girthPopup.closed)
  {
    var features = 'width=500,height=520,left=0,top=0,resizable=yes,scrollbars=yes';
    girthPopup = window.open('', '', features);
  }

  with (girthPopup)
  {
    document.open();
    document.write('<html><head><title>' + girth_illustrated_title + '</title></head>');
    document.write('<body bgcolor="#FFFFFF">');
    document.write('<img src="' + imageURL + '">');
    document.write('</body></html>');
    document.close();
  }
}

function extra_postage_parcels(imageURL)
{
  if ((extra_postage_popup == null || extra_postage_popup.closed) && !extra_postage_button_clicked)
    return;

  extra_postage_button_clicked = false;

  if (extra_postage_popup == null || extra_postage_popup.closed)
  {
    var features = 'width=610,height=250,left=0,top=0,resizable=yes,scrollbars=yes';
    extra_postage_popup = window.open('', '', features);
  }

  with (extra_postage_popup)
  {
    document.open();
    document.write('<html><head><title>' + extra_postage_title + '</title></head>');
    document.write('<body bgcolor="#FFFFFF">');
    document.write('<img src="' + imageURL + '">');
    document.write('</body></html>');
    document.close();
  }
}
