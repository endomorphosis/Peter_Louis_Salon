function select_extra_field(field_id)
{
  var divViewEdit, divFldNum, divExtraField, divTextArea;
  var editor_field, extra_field, current_field_id;
  var oEditor = null;

  current_field_id = current_extra_field;

  divViewEdit = document.getElementById('divViewEdit_' + current_field_id);
  divViewEdit.style.display = 'none';

  divViewEdit = document.getElementById('divViewEdit_' + field_id);
  divViewEdit.style.display = '';

  divFldNum = document.getElementById('divFldNum_' + current_field_id + 'b');
  divFldNum.style.display = 'none';

  divFldNum = document.getElementById('divFldNum_' + current_field_id);
  divFldNum.style.display = '';

  divFldNum = document.getElementById('divFldNum_' + field_id);
  divFldNum.style.display = 'none';

  divFldNum = document.getElementById('divFldNum_' + field_id + 'b');
  divFldNum.style.display = '';

  extra_field = document.getElementById(current_field_id);
  editor_field = document.getElementById('ExtraFields');
  divTextArea = document.getElementById('divTextArea_ExtraFields');

  if (typeof(FCKeditorAPI) != 'undefined')
    oEditor = FCKeditorAPI.GetInstance('ExtraFields');

  if (divTextArea.style.display == 'none')
    editor_field.value = oEditor.GetXHTML();

  extra_field.value = editor_field.value;
  extra_field.style.backgroundColor = textarea_style.backgroundColor;
  extra_field.style.borderStyle = textarea_style.borderStyle;
  extra_field.style.borderWidth = textarea_style.borderWidth;
  extra_field.readOnly = false;

  extra_field = document.getElementById(field_id);
  editor_field.value = extra_field.value;
  extra_field.style.backgroundColor = '#EFEFEF';
  extra_field.style.borderStyle = 'dashed';
  extra_field.style.borderWidth = '1';
  extra_field.readOnly = true;

  if (divTextArea.style.display == 'none')
  {
    if (oEditor != null)
    {
      oEditor.SetData(editor_field.value);
      oEditor.Focus();
    }
  }
  else
    editor_field.focus();

  current_extra_field = field_id;
}

function update_extra_field()
{
  var extra_field = document.getElementById(current_extra_field);
  var divTextArea = document.getElementById('divTextArea_ExtraFields');
  var editor_field = document.getElementById('ExtraFields');
  var oEditor = null;

  if (typeof(FCKeditorAPI) != 'undefined')
    oEditor = FCKeditorAPI.GetInstance('ExtraFields');

  if (divTextArea.style.display == 'none')
    editor_field.value = oEditor.GetXHTML();

  extra_field.value = editor_field.value;
}

function expand_extra_fields(expand)
{
  var divExpandExtraFields = document.getElementById('divExpandExtraFields');
  var divCollapseExtraFields = document.getElementById('divCollapseExtraFields');
  var divExtraField, field_id, current_field_id, mode, i;

  update_extra_field();
  current_field_id = current_extra_field;

  for (i = 0; i < extra_fields.length; i++)
  {
    field_id = 'divExtraField_' + extra_fields[i];
    if ((divExtraField = document.getElementById(field_id)) != null)
    {
      divExtraField.style.display = expand? '' : 'none';

      if (extra_fields[i] == current_field_id)
        divExtraField.readOnly = true;
      else
        divExtraField.readOnly = false;
    }
  }

  divExpandExtraFields.style.display = expand? 'none' : '';
  divCollapseExtraFields.style.display = expand? '' : 'none';
}
