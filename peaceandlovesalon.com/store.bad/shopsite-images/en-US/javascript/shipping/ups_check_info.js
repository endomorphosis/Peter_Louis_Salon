function checkInfo()
{
  var digits = "0123456789";
  var digit_count;
  var digit;
  var error = "";
  var i, j, m;

  with (document.forms[0])
  {
    var state = elements[state_fld];
    var country = elements[country_fld];
    var postal_code = elements[postal_code_fld];
    var email = elements[email_fld];
    var phone = elements[phone_fld];
    var fax = elements[fax_fld];

    // check for info in required fields
    for (j = 0; j < num_fields; j++)
    {
      if (j != state_fld && j != country_fld && j != fax_fld && j != account_fld)
      {
        if (j == postal_code_fld && postal_code.value.length == 0)
        {
          // postal code required for US and Canada
          i = country.selectedIndex;
          m = country.options[i].value;
          if (m != "US" && m != "CA")
              continue;
        }

        if (elements[j].value.length == 0)
          error += fill_in_msg + " " + labels[j] + ".\n";
      }
    }

    // check for state/province if country is US or CA
    i = country.selectedIndex;
    m = country.options[i].value;
    if (m == "US" || m == "CA")
    {
      if (state.selectedIndex == 0)
        error += select_state_msg + " " + labels[state_fld] + ".\n";
    }

    // validate the email address
    m = email.value;
    if (m.length > 0 && (m.indexOf('@', 0) == -1 || m.indexOf('.', 0) == -1))
        error += verify_email_msg + "\n";

    // validate the phone number
    m = phone.value;
    if (m.length > 0)
    {
      digit_count = 0;
      for (i = 0; i < m.length; i++)
      {
        digit = m.substring(i, i + 1);
        if (digits.indexOf(digit) != -1)
          digit_count++;
      }

      if (digit_count < 10 || digit_count > 14)
        error += invalid_phone_msg + "\n";
    }

    // validate the fax number
    m = fax.value;
    if (m.length > 0)
    {
      digit_count = 0;
      for (i = 0; i < m.length; i++)
      {
        digit = m.substring(i, i + 1);
        if (digits.indexOf(digit) != -1)
          digit_count++;
      }

      if (digit_count < 10 || digit_count > 14)
        error += invalid_fax_msg + "\n";
    }
  }

  if (error.length > 0)
  {
    alert(error);
    return false;
  }
  else
    return true;
}
