<?php
    header("Content-type: text/xml");
echo '<?xml version="1.0" encoding="UTF-8" ?>'; 

  //  chdir('/home/peterlou/public_html/drupal/');
    require_once("./includes/bootstrap.inc");
    drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL); 

    ?>

<rss version="2.0" xmlns:g="http://base.google.com/ns/1.0">
<channel>
<title>Peter Louis Salon</title>
<description>Peter Louis Products</description>
<link>http://www.peterlouissalon.com</link>
    <?
  //  $path = 'http://'.str_replace("froogle.php", "", $_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']);
$path = "http://peterlouissalon.com/";
    $imagepath = 'sites/default/files/';
    for($a = 0; $a < 500; $a++){
    $node = node_load($a);
    $nodes[$a] = node_load($a);
    $image = $node->field_image;
    $brand = $node->field_brand;
    if($node->type == 'product'){
    print "<item>\n";
    print '<title>'.urlencode($node->title)."</title>\n";
    print '<link>'.$path.$node->path."</link>\n";
    print '<description>'.htmlspecialchars(strip_tags($node->body), ENT_QUOTES)."</description>\n";
    print '<g:id>'.$node->nid."</g:id>";
   print '<guid>'.$path.$node->path."</guid>";
    print '<g:image_link>'.$path.$imagepath.urlencode($image[0]['filename'])."</g:image_link>\n";
    print '<g:brand>'.$brand[0]['value']."</g:brand>\n";
    print "<g:condition>new</g:condition>\n";
    print '<g:price>'.$node->price."</g:price>\n";
    print "</item>\n";
    //print_r($node);
    }
   // print $a;
    }
    ?>
</channel>
</rss>
