<?php
require_once("/home/peterlou/public_html/drupal/signature/common/cart/html/HTMLCartFactory.php");
/**
 * Returns a simple static cart to generate a signature from.
 *
 * Copyright 2008-2008 Amazon.com, Inc., or its affiliates. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the “License”).
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 *    http://aws.amazon.com/apache2.0/
 *
 * or in the “license” file accompanying this file.
 * This file is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */
class MerchantHTMLCartFactory extends HTMLCartFactory {

   /**
    * Instantiate an instance of the cart factory.
    */
   public function MerchantHTMLCartFactory()
   {
   }

   public function getCart($merchantID, $awsAccessKeyID, $data_array) {
       $parameterMap = $this->getCartMap($merchantID, $awsAccessKeyID, $data_array);
       return $this->getCartFromMap($merchantID, $awsAccessKeyID, $parameterMap);
   }


   /**
    * Get map representation of the cart.
    * Replace with your own cart here to try out
    * different promotions, tax, shipping, etc. 
    */
   protected function getCartMap($merchantID, $awsAccessKeyID, $data_array) {
      foreach($data_array as $key => $value){
	  if(isset($value['sku'])){
	  $cart["item_merchant_id_".$key] = $merchantID;
          $cart["item_title_".$key] =  $value['title'];
          $cart["item_sku_".$key] =  $value['sku'];
          $cart["item_description_".$key] = $value['description'];
          $cart["item_price_".$key] = $value['price'];
          $cart["item_quantity_".$key] = $value['qty'];
          $cart["currency_code"] = "USD";
          $cart["aws_access_key_id"] = $awsAccessKeyID;
	  if($value['ptype'] == 'amazon') { $cart["item_fulfillment_network_".$key] = "AMAZON_NA";}
}}

//print_r($cart);
      // sort cart by key
      ksort($cart);

      return $cart;
   }

   /**
    * Construct a very basic cart with one item.
    */
   public function getCartHTML($merchantID, $awsAccessKeyID, $signature, $data_array) {
       $cartInput = $this->getCart($merchantID, $awsAccessKeyID, $data_array);
       return $this->getCartHTMLFromCartInput($merchantID, $awsAccessKeyID, $signature,
                                 $cartInput);
   }


   /**
    * Get the input to generate the hmac-sha1 signature.
    */
   public function getSignatureInput($merchantID, $awsAccessKeyID, $data_array) {
      $parameterMap = $this->getCartMap($merchantID, $awsAccessKeyID, $data_array);
      return $this->getSignatureInputFromMap($parameterMap);
   }
}
?>
