function select_image(subsystem, image_id, return_image_fields, form)
{
  var image, url;
  var i, n;

  n = form.elements.length;
  for (i = 0; i < n; i++)
  {
    if (form.elements[i].name == image_id)
    {
      image = form.elements[i].value;
      break;
    }
  }

  if (image.substr(0, 4) == 'http')
  {
    i = image.indexOf('/media/');
    if (i > 0)
      image = image.substr(i + 7); 
  }
  else
  {
    // remove any ampersands from the image name
    parts = image.split('&');
    image = parts[0];
    for (i = 1; i < parts.length; i++)
      image += parts[i];
  }

  url = backoffice_url + '/mediam.cgi?subsystem=' + subsystem + '&popup=1&image_return='
    + image_id + '&return_image_fields=' + return_image_fields + '&image=' + image;
  window.open(url, 'mediam', 'width=500,height=480,resizable=yes,scrollbars=yes');
  return false;
}
