<?php

// $Id: index.php,v 1.94 2007/12/26 08:46:48 dries Exp $
/**
 * @file
 * The PHP page that serves all page requests on a Drupal installation.
 *
 * The routines here dispatch control to the appropriate handler, which then
 * prints the appropriate page.
 *
 * All Drupal code is released under the GNU General Public License.
 * See COPYRIGHT.txt and LICENSE.txt.
 */


error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);



require_once './includes/bootstrap.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
if(arg(0) == 'cart' && arg(1) == 'add'){

setcookie("_cart" ,arg(0), time()+15);
}
$refersrc = $_SERVER['HTTP_REFERER'];

$refer =  $_SERVER['HTTP_REFERER'];
//$from = $_COOKIE['refer'];
$from = $_SESSION['refer'];
//$_term = $_COOKIE['_term'];
$_term = $_SESSION['_term'];
if(strstr($refer, "renefurtererhaircare")){$from = 'rene'; $_term = 28;}
if(strstr($refer, "phytoamerica")){$from = 'phyto'; $_term = 15;}
if(strstr($refer, "jflazartiguehaircare")){$from = 'jfl'; $_term = 44;}
if(strstr($refer, "philipbhaircare")){$from = 'phb'; $_term = 13;}
if(strstr($refer, "ghdproduct")){$from = 'ghd'; $_term = 46;} 
if(strstr($refer, "greatlengthsofnewyork")){$from = 'glny'; $_term = 70;}
if(strstr($refer, "hairdreamsofnewyork")){$from = 'hdny'; $_term = 28;}
if(strstr($refer, "hairlocsofnewyork")){$from = 'hlny'; $_term = 28;}

if(!empty($from)){
setcookie("refer" ,$from, time()+3600);
$_SESSION['refer'] = $from;
} 
if(!empty($from)){
setcookie("_term" ,$_term, time()+3600);
$_SESSION['_term'] = $_term;
}
$return = menu_execute_active_handler();

// Menu status constants are integers; page content is a string.
if (is_int($return)) {
  switch ($return) {
    case MENU_NOT_FOUND:
      drupal_not_found();
      break;
    case MENU_ACCESS_DENIED:
      drupal_access_denied();
      break;
    case MENU_SITE_OFFLINE:
      drupal_site_offline();
      break;
  }
}
elseif (isset($return)) {
  // Print any value (including an empty string) except NULL or undefined:
  print theme('page', $return);
}

drupal_page_footer();
