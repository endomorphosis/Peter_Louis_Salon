<?php
ini_set('display_errors','Off');
ini_set('display_startup_errors','Off');
ini_set('log_errors','On');
ini_set('error_log','./error_log');
ini_set('error_reporting','2047');

  include("db.php");
	$file=$_REQUEST['f'];
	switch($file)
	{
	//---HOME-------------------------------------------------------------------------------------------------------------
	case 'home':
		$title="Peter Louis SALON";
		$includefile="home.php";
		//no products		
		break;
	//---HAIR PRODUCTS==================================================================================================B-
	case 'allproducts':
		//NEW MODULE-----------------------------------------------
		$title="Full Product List";
//		$includefile="store/allproducts.php";
		$template_include_file="hairproduct_type.php";
    $cat_list_title='Full Product List';
    $template_title='Full Product List';
    $cat_num=91; //correct
    $override=false;
    $item_ids="";
		$includefile="store/z_product_list.php";
		//NEW MODULE-----------------------------------------------
		break;
	//---PETER LOUIS----------------------------------------------------------------------------------------------------b-	
	case 'derma':
		//NEW MODULE-----------------------------------------------
		$title="Peter Louis SALON";
//		$includefile="store/derma.php";
		$template_include_file="peterlouis_type.php";
    $cat_list_title='Peter Louis Products: Full Product List';
    $template_title='Peter Louis Products';
    $cat_num=57; //correct
    $override=false;
    $item_ids="";
		$includefile="store/z_product_list.php";
		//NEW MODULE-----------------------------------------------
		break;
	case 'dermaShamp_Cond':
		//NEW MODULE-----------------------------------------------
		$title="Peter Louis Shampoos and Conditioners";
//		$includefile="store/dermaShamp_Cond.php";
		$template_include_file="peterlouis_type.php";
    $cat_list_title='Peter Louis Products: Shampoos and Conditioners';
    $template_title='Shampoos and Conditioners';
    $cat_num=97; //correct
    $override=false;
    $item_ids="262, 76, 276, 267, 82, 434, 519, 200, 518, 199, 90, 104, 99, 101, 61, 31, 28, 205, 206";
		$includefile="store/z_product_list.php";
		//NEW MODULE-----------------------------------------------
		// unknown category
		// added category and assigned products in shopsite
		break;	
	case 'dermaStyling_Prod':
		$title="Peter Louis Styling Products";
//		$includefile="store/dermaStyling_Prod.php";
		$template_include_file="peterlouis_type.php";
    $cat_list_title='Peter Louis Products: Styling Products';
    $template_title='Styling Products';
    $cat_num=98; //correct
    $override=false;
    $item_ids="71, 88, 157, 208, 14, 207, 203, 70, 205, 209, 210, 37, 552";
		$includefile="store/z_product_list.php";
		//NEW MODULE-----------------------------------------------
		// added category and assigned products in shopsite
		break;
	//---PETER LOUIS----------------------------------------------------------------------------------------------------e-	
	//---PHYTOLOGIE-----------------------------------------------------------------------------------------------------b-	
	case 'page22':
		$title="Phytologie/PHYTO";
//		$includefile="store/page22.php";
		$template_include_file="Phytologie_type.php";
    $cat_list_title='Phytologie/PHYTO: Full Product List';
    $template_title='Phytologie/PHYTO Full Product List';
    $cat_num=61; //correct
    $override=false;
    $item_ids="";
		$includefile="store/z_product_list.php";
		//NEW MODULE-----------------------------------------------
		//new query done
		break;
	case 'dryhair':
		$title="Phytologie: Dry / Ultra Dry Hair";
//		$includefile="store/dryhair.php";
		$template_include_file="Phytologie_type.php";
    $cat_list_title='<img src="http://peterlouissalon.com/media/products/phyto_122x42.gif" width="122" height="42" />';
    $template_title='Phytologie: Dry / Ultra Dry Hair';
    $cat_num=65; //correct
    $override=false;
    $item_ids="";
		$includefile="store/z_product_list.php";
		//NEW MODULE-----------------------------------------------
		//new query done
		break;
	case 'verydry':	
		$title="Phytologie: Sun Care";
//		$includefile="store/verydry.php";
		$template_include_file="Phytologie_type.php";
    $cat_list_title='<img src="http://peterlouissalon.com/media/products/phyto_122x42.gif" width="122" height="42" />';
    $template_title='Phytologie: Sun Care';
    $cat_num=71; //correct
    $override=false;
    $item_ids="";
		$includefile="store/z_product_list.php";
		//NEW MODULE-----------------------------------------------
		//new query done
		break;
	case 'permedcolored':
		$title="Phytologie: Permed-Colored";
//		$includefile="store/permedcolored.php";
		$template_include_file="Phytologie_type.php";
    $cat_list_title='<img src="http://peterlouissalon.com/media/products/phyto_122x42.gif" width="122" height="42" />';
    $template_title='Phytologie: Permed-Colored';
    $cat_num=69; //correct
    $override=false;
    $item_ids="";
		$includefile="store/z_product_list.php";
		//NEW MODULE-----------------------------------------------
		//new query done
		break;
	case 'stylingproducts':
		$title="Phytologie: Styling Products";
//		$includefile="store/stylingproducts.php";
		$template_include_file="Phytologie_type.php";
    $cat_list_title='<img src="http://peterlouissalon.com/media/products/phyto_122x42.gif" width="122" height="42" />';
    $template_title='Phytologie: Styling Products';
    $cat_num=70; //correct
    $override=false;
    $item_ids="";
		$includefile="store/z_product_list.php";
		//NEW MODULE-----------------------------------------------
		//new query done
		break;
	case 'volume':
		$title="Phytologie: Volume";
//		$includefile="store/volume.php";
		$template_include_file="Phytologie_type.php";
    $cat_list_title='<img src="http://peterlouissalon.com/media/products/phyto_122x42.gif" width="122" height="42" />';
    $template_title='Phytologie: Volume';
    $cat_num=72; //correct
    $override=false;
    $item_ids="";
		$includefile="store/z_product_list.php";
		//NEW MODULE-----------------------------------------------
		//new query done
		break;
	case 'PHYTOPRO':
		$title="Phytologie/Phyto: Anti-Frizz";
//		$includefile="store/PHYTOPRO.php";
		$template_include_file="Phytologie_type.php";
    $cat_list_title='<img src="http://peterlouissalon.com/media/products/phyto_122x42.gif" width="122" height="42" />';
    $template_title='Phytologie/Phyto: Anti-Frizz';
    $cat_num=62; //correct
    $override=false;
    $item_ids="";
		$includefile="store/z_product_list.php";
		//NEW MODULE-----------------------------------------------
		//new query done
		break;
	case 'phytoplage':
		$title="Phytologie/Phyto: All Hair Types";
//		$includefile="store/phytoplage.php";
		$template_include_file="Phytologie_type.php";
    $cat_list_title='<img src="http://peterlouissalon.com/media/products/phyto_122x42.gif" width="122" height="42" />';
    $template_title='Phytologie/Phyto: All Hair Types';
    $cat_num=63; //correct
    $override=false;
    $item_ids="";
		$includefile="store/z_product_list.php";
		//NEW MODULE-----------------------------------------------
		//new query done
		break;
	case 'thinninghair':
		$title="Phytologie/Phyto: Hair Loss";
//		$includefile="store/thinninghair.php";
		$template_include_file="Phytologie_type.php";
    $cat_list_title='<img src="http://peterlouissalon.com/media/products/phyto_122x42.gif" width="122" height="42" />';
    $template_title='Phytologie/Phyto: Hair Loss';
    $cat_num=67; //correct
    $override=false;
    $item_ids="";
		$includefile="store/z_product_list.php";
		//NEW MODULE-----------------------------------------------
		//new query done
		break;
	case 'dandruff':
		$title="Phytologie/Phyto: Dandruff";
//		$includefile="store/dandruff.php";
		$template_include_file="Phytologie_type.php";
    $cat_list_title='<img src="http://peterlouissalon.com/media/products/phyto_122x42.gif" width="122" height="42" />';
    $template_title='Phytologie/Phyto: Dandruff';
    $cat_num=64; //correct
    $override=false;
    $item_ids="";
		$includefile="store/z_product_list.php";
		//NEW MODULE-----------------------------------------------
		//new query done
		break;
	case 'sensitivescalp':
		$title="Phytologie/Phyto: Frequent Use";
//		$includefile="store/sensitivescalp.php";
		$template_include_file="Phytologie_type.php";
    $cat_list_title='<img src="http://peterlouissalon.com/media/products/phyto_122x42.gif" width="122" height="42" />';
    $template_title='Phytologie/Phyto: Frequent Use';
    $cat_num=66; //correct
    $override=false;
    $item_ids="";
		$includefile="store/z_product_list.php";
		//NEW MODULE-----------------------------------------------
		//new query done
		break;
	case 'oilyscalp':
		$title="Phytologie/Phyto: Other";
//		$includefile="store/oilyscalp.php";
		$template_include_file="Phytologie_type.php";
    $cat_list_title='<img src="http://peterlouissalon.com/media/products/phyto_122x42.gif" width="122" height="42" />';
    $template_title='Phytologie/Phyto: Other';
    $cat_num=68; //correct
    $override=false;
    $item_ids="";
		$includefile="store/z_product_list.php";
		//NEW MODULE-----------------------------------------------
		//new query done
		break;
	case 'phytospecific':
		$title="Phytologie: Phytospecific";
//		$includefile="store/phytospecific.php";
//		$template_include_file="Phytologie_type.php";
		$template_include_file="phytospecific_type.php";
    $cat_list_title='<img src="http://peterlouissalon.com/media/products/phyto_122x42.gif" width="122" height="42" />';
    $template_title='Phytologie/Phyto: Phytospecific';
    $cat_num=73; //correct
    $override=false;
    $item_ids="";
		$includefile="store/z_product_list.php";
		//NEW MODULE-----------------------------------------------
		//new query done
		break;
	case 'phytospecific_dry':
		$title="Phytospecific: Normal to Dry Hair";
//		$includefile="store/phytospecific_dry.php";
		$template_include_file="phytospecific_type.php";
    $cat_list_title='<img src="http://peterlouissalon.com/media/products/phyto_122x42.gif" width="122" height="42" />';
    $template_title='Phytospecific: Normal to Dry Hair';
    $cat_num=26; //correct
    $override=true;
    $item_ids="48, 455";
		$includefile="store/z_product_list.php";
		//NEW MODULE-----------------------------------------------
		//now not used
		break;
	case 'phytospecific_ultradry':
		$title="Phytospecific: Ultra Dry Hair";
//		$includefile="store/phytospecific_ultradry.php";
		$template_include_file="phytospecific_type.php";
    $cat_list_title='<img src="http://peterlouissalon.com/media/products/phyto_122x42.gif" width="122" height="42" />';
    $template_title='Phytospecific: Ultra Dry Hair';
    $cat_num=27; //correct
    $override=true;
    $item_ids="50, 49, 523, 522";
		$includefile="store/z_product_list.php";
		//NEW MODULE-----------------------------------------------
		//now not used
		break;
	case 'phytospecific_damaged':
		$title="Phytospecific: Damaged Brittle Hair";
//		$includefile="store/phytospecific_damaged.php";
		$template_include_file="phytospecific_type.php";
    $cat_list_title='<img src="http://peterlouissalon.com/media/products/phyto_122x42.gif" width="122" height="42" />';
    $template_title='Phytospecific: Damaged Brittle Hair';
    $cat_num=28; //correct
    $override=true;
    $item_ids="51, 59, 522, 523, 526";
		$includefile="store/z_product_list.php";
		//NEW MODULE-----------------------------------------------
		//now not used
		break;
	case 'phytospecific_scalp':
		$title="Phytospecific: Dry Hair and Scalp";
//		$includefile="store/phytospecific_scalp.php";
		$template_include_file="phytospecific_type.php";
    $cat_list_title='<img src="http://peterlouissalon.com/media/products/phyto_122x42.gif" width="122" height="42" />';
    $template_title='Phytospecific: Dry Hair and Scalp';
    $cat_num=29; //correct
    $override=true;
    $item_ids="47, 23, 526, 522";
		$includefile="store/z_product_list.php";
		//NEW MODULE-----------------------------------------------
		//now not used
		break;
	case 'phytospecific_allhairtypes':
		$title="Phytospecific: Styling: All Hair Types";
//		$includefile="store/phytospecific_allhairtypes.php";
		$template_include_file="phytospecific_type.php";
    $cat_list_title='<img src="http://peterlouissalon.com/media/products/phyto_122x42.gif" width="122" height="42" />';
    $template_title='Phytospecific: Styling: All Hair Types';
    $cat_num=30; //correct
    $override=true;
    $item_ids="62, 60, 524, 550";
		$includefile="store/z_product_list.php";
		//NEW MODULE-----------------------------------------------
		//now not used
		break;
	case 'phytospecific_delicate':
		$title="Phytospecific: Mild: For Delicate and Fine Hair";
//		$includefile="store/phytospecific_delicate.php";
		$template_include_file="phytospecific_type.php";
    $cat_list_title='<img src="http://peterlouissalon.com/media/products/phyto_122x42.gif" width="122" height="42" />';
    $template_title='Phytospecific: Mild: For Delicate and Fine Hair';
    $cat_num=31; //correct
    $override=true;
    $item_ids="46, 523, 522, 526";
		$includefile="store/z_product_list.php";
		//NEW MODULE-----------------------------------------------
		//now not used
		break;
	case 'phytospecific_normal':
		$title="Phytospecific: Regular: For Normal and Thick Coarse Hair";
//		$includefile="store/phytospecific_normal.php";
		$template_include_file="phytospecific_type.php";
    $cat_list_title='<img src="http://peterlouissalon.com/media/products/phyto_122x42.gif" width="122" height="42" />';
    $template_title='Phytospecific: Regular: For Normal and Thick Coarse Hair';
    $cat_num=32; //correct
    $override=true;
    $item_ids="20";
		$includefile="store/z_product_list.php";
		//NEW MODULE-----------------------------------------------
		//now not used
		break;
	case 'phytospecific_men':
		$title="Phytospecific: Phyto Men";
//		$includefile="store/phytospecific_men.php";
		$template_include_file="phytospecific_type.php";
    $cat_list_title='<img src="http://peterlouissalon.com/media/products/phyto_122x42.gif" width="122" height="42" />';
    $template_title='Phytospecific: Phyto Men';
    $cat_num=8;
    $override=true;
    $item_ids="";
		$includefile="store/z_product_list.php";
		//NEW MODULE-----------------------------------------------
		//now not used
		break;							
	//---PHYTOLOGIE-----------------------------------------------------------------------------------------------------e-	
	//---RENE FURTERER--------------------------------------------------------------------------------------------------b-	
	case 'ReneFurterer':
		$title="Rene Furterer";
//		$includefile="store/ReneFurterer.php";
		$template_include_file="ReneFurterer_type.php";
    $cat_list_title='<IMG src="images/furterer_83x18.gif" width="83" height="18" border="0" />';
    $template_title='Rene Furterer: Full Product List';
    $cat_num=74; //correct
    $override=false;
    $item_ids="";
		$includefile="store/z_product_list.php";
		//NEW MODULE-----------------------------------------------
		//new query done
 		break;		
	case 'rene_essential':
		$title="Rene Furterer: Essential Care";
//		$includefile="store/rene_essential.php";
		$template_include_file="ReneFurterer_type.php";
    $cat_list_title='<IMG src="images/furterer_83x18.gif" width="83" height="18" border="0" />';
    $template_title='Rene Furterer: Essential Care';
    $cat_num=81; //correct
    $override=false;
    $item_ids="";
		$includefile="store/z_product_list.php";
		//NEW MODULE-----------------------------------------------
		//new query done
		break;
	case 'rene_scalp':
		$title="Rene Furterer: Oily Scalp Treatment";
//		$includefile="store/rene_scalp.php";
		$template_include_file="ReneFurterer_type.php";
    $cat_list_title='<IMG src="images/furterer_83x18.gif" width="83" height="18" border="0" />';
    $template_title='Rene Furterer: Oily Scalp Treatment';
    $cat_num=83; //correct
    $override=false;
    $item_ids="";
		$includefile="store/z_product_list.php";
		//NEW MODULE-----------------------------------------------
		//new query done		
		break;
	case 'rene_dryscalp':
		$title="Rene Furterer: Dry Scalp and Hair Treatment";
//		$includefile="store/rene_dryscalp.php";
		$template_include_file="ReneFurterer_type.php";
    $cat_list_title='<IMG src="images/furterer_83x18.gif" width="83" height="18" border="0" />';
    $template_title='Rene Furterer: Dry Scalp and Hair Treatment';
    $cat_num=80; //correct
    $override=false;
    $item_ids="";
		$includefile="store/z_product_list.php";
		//NEW MODULE-----------------------------------------------
		//new query done		
		break;
	case 'rene_hairloss':
		$title="Rene Furterer: Thinning Hair Treatment";
//		$includefile="store/rene_hairloss.php";
		$template_include_file="ReneFurterer_type.php";
    $cat_list_title='<IMG src="images/furterer_83x18.gif" width="83" height="18" border="0" />';
    $template_title='Rene Furterer: Thinning Hair Treatment';
    $cat_num=88; //correct
    $override=false;
    $item_ids="";
		$includefile="store/z_product_list.php";
		//NEW MODULE-----------------------------------------------
		//new query done		
		break;
	case 'rene_dandruff':
		$title="Rene Furterer: Anti Dandruff Treatment";
//		$includefile="store/rene_dandruff.php";
		$template_include_file="ReneFurterer_type.php";
    $cat_list_title='<IMG src="images/furterer_83x18.gif" width="83" height="18" border="0" />';
    $template_title='Rene Furterer: Dry Scalp and Hair Treatment';
    $cat_num=75; //correct
    $override=false;
    $item_ids="";
		$includefile="store/z_product_list.php";
		//NEW MODULE-----------------------------------------------
		//new query done		
		break;
	case 'rene_frequent':
		$title="Rene Furterer: Frequent Use";
//		$includefile="store/rene_frequent.php";
		$template_include_file="ReneFurterer_type.php";
    $cat_list_title='<IMG src="images/furterer_83x18.gif" width="83" height="18" border="0" />';
    $template_title='Rene Furterer: Frequent Use';
    $cat_num=82; //correct
    $override=false;
    $item_ids="";
		$includefile="store/z_product_list.php";
		//NEW MODULE-----------------------------------------------
		//new query done		
		break;
	case 'rene_sensitivescalp':
		$title="Rene Furterer: Sensitive Scalp";
//		$includefile="store/rene_sensitivescalp.php";
		$template_include_file="ReneFurterer_type.php";
    $cat_list_title='<IMG src="images/furterer_83x18.gif" width="83" height="18" border="0" />';
    $template_title='Rene Furterer: Sensitive Scalp';
    $cat_num=85; //correct
    $override=false;
    $item_ids="";
		$includefile="store/z_product_list.php";
		//NEW MODULE-----------------------------------------------
		//new query done		
		break;
	case 'rene_colorperm':
		$title="Rene Furterer:  Permed & Colored Hair Treatment";
//		$includefile="store/rene_colorperm.php";
		$template_include_file="ReneFurterer_type.php";
    $cat_list_title='<IMG src="images/furterer_83x18.gif" width="83" height="18" border="0" />';
    $template_title='Rene Furterer: Permed & Colored Hair Treatment';
    $cat_num=84; //correct
    $override=false;
    $item_ids="";
		$includefile="store/z_product_list.php";
		//NEW MODULE-----------------------------------------------
		//new query done		
		break;
	case 'rene_styling':
		$title="Rene Furterer: Styling";
//		$includefile="store/rene_styling.php";
		$template_include_file="ReneFurterer_type.php";
    $cat_list_title='<IMG src="images/furterer_83x18.gif" width="83" height="18" border="0" />';
    $template_title='Rene Furterer: Styling';
    $cat_num=86; //correct
    $override=false;
    $item_ids="";
		$includefile="store/z_product_list.php";
		//NEW MODULE-----------------------------------------------
		//new query done		
		break;
	case 'rene_beauty':
		$title="Rene Furterer: Beautifying";
//		$includefile="store/rene_beauty.php";
		$template_include_file="ReneFurterer_type.php";
    $cat_list_title='<IMG src="images/furterer_83x18.gif" width="83" height="18" border="0" />';
    $template_title='Rene Furterer: Beautifying';
    $cat_num=76; //correct
    $override=false;
    $item_ids="";
		$includefile="store/z_product_list.php";
		//NEW MODULE-----------------------------------------------
		//new query done		
		break;
 	case 'rene_colorenhance':
		$title="Rene Furterer: Color Enhancing Mask";
//		$includefile="store/rene_colorenhance.php";
		$template_include_file="ReneFurterer_type.php";
    $cat_list_title='<IMG src="images/furterer_83x18.gif" width="83" height="18" border="0" />';
    $template_title='Rene Furterer: Color Enhancing Mask';
    $cat_num=77; //correct
    $override=false;
    $item_ids="";
		$includefile="store/z_product_list.php";
		//NEW MODULE-----------------------------------------------
		//new query done		
		break;
	case 'rene_suncare':
		$title="Rene Furterer: Sun Care";
//		$includefile="store/rene_suncare.php";
		$template_include_file="ReneFurterer_type.php";
    $cat_list_title='<IMG src="images/furterer_83x18.gif" width="83" height="18" border="0" />';
    $template_title='Rene Furterer: Sun Care';
    $cat_num=87; //correct
    $override=false;
    $item_ids="";
		$includefile="store/z_product_list.php";
		//NEW MODULE-----------------------------------------------
		//new query done		
		break;
	case 'rene_dietery':
		$title="Rene Furterer: Dietary Supplement";
		$includefile="store/rene_dietery.php";
		$template_include_file="ReneFurterer_type.php";
    $cat_list_title='<IMG src="images/furterer_83x18.gif" width="83" height="18" border="0" />';
    $template_title='Rene Furterer: Dietary Supplement';
    $cat_num=89; //correct
    $override=false;
    $item_ids="";
		$includefile="store/z_product_list.php";
		//NEW MODULE-----------------------------------------------
		//new query done		
		break;
	case 'rene_beauty1':
		$title="Rene Furterer: Beautifying";
		$includefile="store/rene_beauty1.php";
	//---RENE FURTERER--------------------------------------------------------------------------------------------------e-	
	//---CREDE----------------------------------------------------------------------------------------------------------b-	
	case 'crede':
		$title="Crede Products";
//		$includefile="store/crede.php";
		$template_include_file="hairproduct_type.php";
    $cat_list_title='<img src="http://www.peterlouissalon.com/nadia/media/homepage/crede_79x18.gif" alt="Crede" border="0" width="79" height="18" />';
    $template_title='Crede: Full Product List';
    $cat_num=58; //correct
    $override=false;
    $item_ids="";
		$includefile="store/z_product_list.php";
		//NEW MODULE-----------------------------------------------
		//new query done		
		break;
	//---CREDE----------------------------------------------------------------------------------------------------------e-	
	//---JF LAZARTIGUE--------------------------------------------------------------------------------------------------b-	
	case 'jflazartigue':
		$title="j.f. lazartigue";
//		$includefile="store/jflazartigue.php";
		$template_include_file="jflazartigue_type.php";
    $cat_list_title='j.f. lazartigue';
    $template_title='j.f. lazartigue: Full Product List';
    $cat_num=90; //correct
    $override=false;
    $item_ids="";
		$includefile="store/z_product_list.php";
		//NEW MODULE-----------------------------------------------
		//new query done
		break;
	case 'jflazartigue_Shampoos':
		$title="j.f. lazartigue: Shampoos";
//		$includefile="store/jflazartigue_Shampoos.php";
		$template_include_file="jflazartigue_type.php";
    $cat_list_title='j.f. lazartigue: Shampoos';
    $template_title='j.f. lazartigue: Shampoos';
    $cat_num=99; //correct
    $override=false;
    $item_ids="158, 161, 162, 163, 164, 165, 166, 167, 169, 170, 171, 172, 195, 168, 458, 435, 440";
		$includefile="store/z_product_list.php";
		//NEW MODULE-----------------------------------------------
		break;
	case 'jflazartigue_OilTreatment':
		$title="j.f. lazartigue: Oil Treatment ";
//		$includefile="store/jflazartigue_OilTreatment.php";
		$template_include_file="jflazartigue_type.php";
    $cat_list_title='j.f. lazartigue: Oil Treatment';
    $template_title='j.f. lazartigue: Oil Treatment';
    $cat_num=101; //correct
    $override=false;
    $item_ids="438, 439, 440, 435, 192";
		$includefile="store/z_product_list.php";
		//NEW MODULE-----------------------------------------------
		break;
	case 'jflazartigue_StylingProd':
		$title="j.f. lazartigue: Styling Products";
//		$includefile="store/jflazartigue_StylingProd.php";
		$template_include_file="jflazartigue_type.php";
    $cat_list_title='j.f. lazartigue: Styling Products';
    $template_title='j.f. lazartigue: Styling Products';
    $cat_num=102; //correct
    $override=false;
    $item_ids="159, 194, 193, 181, 180, 184";
		$includefile="store/z_product_list.php";
		//NEW MODULE-----------------------------------------------
		break;
	case 'jflazartigue_Conditioners':
		$title="j.f. lazartigue: Conditioners ";
//		$includefile="store/jflazartigue_Conditioners.php";
		$template_include_file="jflazartigue_type.php";
    $cat_list_title='j.f. lazartigue: Conditioners';
    $template_title='j.f. lazartigue: Conditioners';
    $cat_num=103; //correct
    $override=false;
    $item_ids="106, 440, 160, 193, 182, 175, 435, 190, 178, 183, 458, 439";
		$includefile="store/z_product_list.php";
		//NEW MODULE-----------------------------------------------
		break;	
	//---j.f. lazartigue--------------------------------------------------------------------------------------------------e-	
	//---PHILLIP B------------------------------------------------------------------------------------------------------b-	
	case 'philipb':
		$title="Philip B Products";
//		$includefile="store/philipb.php";
		$template_include_file="philipb_type.php";
    $cat_list_title='Phillip B: Full Product List';
    $template_title='Phillip B: Full Product List';
    $cat_num=59; //correct
    $override=false;
    $item_ids="217, 218, 103, 246, 238, 240, 241, 83, 89, 74, 75, 84, 78, 79, 87, 80, 268, 77, 85, 86, 81, 247, 478";
		$includefile="store/z_product_list.php";
		//NEW MODULE-----------------------------------------------
		break;
	case 'philipb_bodyproduct':
		$title="Philip B: Body Products";
//		$includefile="store/philipb_bodyproduct.php";
		$template_include_file="philipb_type.php";
    $cat_list_title='Phillip B: Body Products';
    $template_title='Phillip B: Body Products';
    $cat_num=104; //correct
    $override=false;
    $item_ids="103, 246, 240, 241";
		$includefile="store/z_product_list.php";
		//NEW MODULE-----------------------------------------------
		break;
	case 'philipb_ShampTreat':
		$title="Philip B: Shampoos and Treatments";
//		$includefile="store/philipb_ShampTreat.php";
		$template_include_file="philipb_type.php";
    $cat_list_title='Phillip B: Shampoos and Treatments';
    $template_title='Phillip B: Shampoos and Treatments';
    $cat_num=105; //correct
    $override=false;
    $item_ids="81, 247, 78, 84, 75, 74, 89, 238, 103, 478";
		$includefile="store/z_product_list.php";
		//NEW MODULE-----------------------------------------------
		break;
	case 'philipb_StylingProd':
		$title="Philip B: Styling Products";
//		$includefile="store/philipb_StylingProd.php";
		$template_include_file="philipb_type.php";
    $cat_list_title='Phillip B: Styling Products';
    $template_title='Phillip B: Styling Products';
    $cat_num=106; //correct
    $override=false;
    $item_ids="83, 217, 218, 80, 77, 86, 268, 85, 87";
		$includefile="store/z_product_list.php";
		//NEW MODULE-----------------------------------------------
		break;	
	//---PHILLIP B------------------------------------------------------------------------------------------------------e-	
	//---GHD------------------------------------------------------------------------------------------------------------b-	
	case 'Silica':
		$title="GHD";
//		$includefile="store/Silica.php";
		$template_include_file="Silica_type.php";
    $cat_list_title='GHD: Full Product List';
    $template_title='GHD: Full Product List';
    $cat_num=93; //correct
    $override=false;
    $item_ids="";
		$includefile="store/z_product_list.php";
		//NEW MODULE-----------------------------------------------
		//new query done
		break;
	case 'Silica_StylingTools':
		$title="GHD: Styling Tools";
//		$includefile="store/Silica_StylingTools.php";
		$template_include_file="Silica_type.php";
    $cat_list_title='GHD: Styling Tools';
    $template_title='GHD: Styling Tools';
    $cat_num=108; //correct
    $override=false;
    $item_ids="475, 474, 473, 472, 471, 470, 528, 529, 530, 531, 532, 479, 441, 98, 330, 527";
		$includefile="store/z_product_list.php";
		//NEW MODULE-----------------------------------------------
		break;
	case 'Silica_StylingProd':
		$title="GHD: Styling Products";
//		$includefile="store/Silica_StylingProd.php";
		$template_include_file="Silica_type.php";
    $cat_list_title='GHD: Styling Products';
    $template_title='GHD: Styling Products';
    $cat_num=107; //correct
    $override=false;
    $item_ids="338, 336, 335, 334, 333, 332, 64, 331, 329, 328, 327, 320, 319, 293, 292, 289";
		$includefile="store/z_product_list.php";
		//NEW MODULE-----------------------------------------------
		break;
	case 'Silica_ShampCondi':
		$title="GHD: Shampoos and Conditioners";
//		$includefile="store/Silica_ShampCondi.php";
		$template_include_file="Silica_type.php";
    $cat_list_title='GHD: Shampoos and Conditioners';
    $template_title='GHD: Shampoos and Conditioners';
    $cat_num=109; //correct
    $override=false;
    $item_ids="236, 239, 258, 259, 264, 269, 270, 279, 280, 288";
		$includefile="store/z_product_list.php";
		//NEW MODULE-----------------------------------------------
		break;
	case 'prod_detailGHD':
		$title="GHD Products Details";
		$includefile="store/prod_detailGHD.php";
		break;	
	//---GHD------------------------------------------------------------------------------------------------------------e-	
	//---JAPANESE PRODUCTS----------------------------------------------------------------------------------------------b-	
	case 'unique':
		$title="Unique &amp; Japanese Products: Full Product List";
//		$includefile="store/unique.php";
		$template_include_file="unique_type.php";
    $cat_list_title='Unique &amp; Japanese Products: Full Product List';
    $template_title='Unique &amp; Japanese Products: Full Product List';
    $cat_num=78; //correct
    $override=true;
    $item_ids="153, 271, 272, 273, 108, 263, 278, 309, 282, 308, 281, 121, 227, 287, 295, 294, 291, 290, 297, 296, 286, 284, 285, 326, 324, 122, 325, 476, 299, 303, 302, 301, 300, 307, 306, 68, 67, 65, 66, 433, 200, 230, 231, 40, 41, 232, 233, 234, 235, 248, 249, 250, 251, 252, 253, 254, 255, 256, 257, 260, 261, 156, 97, 154, 197, 541, 551, 536, 537, 538, 539, 540, 542, 543, 544, 545, 479, 552";
		$includefile="store/z_product_list.php";
		//NEW MODULE-----------------------------------------------
		break;
	case 'unique_Liscio':
		$title="Unique &amp; Japanese Products: Liscio";
//		$includefile="store/unique_Liscio.php";
		$template_include_file="unique_type.php";
    $cat_list_title='Unique &amp; Japanese Products: Liscio';
    $template_title='Unique &amp; Japanese Products: Liscio';
    $cat_num=117; //correct
    $override=false;
    $item_ids="121, 227, 476";
		$includefile="store/z_product_list.php";
		//NEW MODULE-----------------------------------------------
		break;
	case 'unique_idcare':
		$title="Unique &amp; Japanese Products: ID Care";
	//	$includefile="store/unique_idcare.php";
		$template_include_file="unique_type.php";
    $cat_list_title='Unique &amp; Japanese Products: ID Care';
    $template_title='Unique &amp; Japanese Products: ID Care';
    $cat_num=116; //correct
    $override=false;
    $item_ids="108, 299, 40, 41, 153, 154, 197";
		$includefile="store/z_product_list.php";
		//NEW MODULE-----------------------------------------------
		break;
	case 'unique_MoltoBene':
		$title="Unique &amp; Japanese Products: Molto Bene";
//		$includefile="store/unique_MoltoBene.php";
		$template_include_file="unique_type.php";
    $cat_list_title='Unique &amp; Japanese Products: Molto Bene';
    $template_title='Unique &amp; Japanese Products: Molto Bene';
    $cat_num=115; //correct
    $override=false;
    $item_ids="248, 307, 306, 249, 250, 251, 252, 253, 254, 255, 256, 257";
		$includefile="store/z_product_list.php";
		//NEW MODULE-----------------------------------------------
		break;
	case 'unique_Nigelle':
		$title="Unique &amp; Japanese Products: Nigelle";
//		$includefile="store/unique_Nigelle.php";
		$template_include_file="unique_type.php";
    $cat_list_title='Unique &amp; Japanese Products: Nigelle';
    $template_title='Unique &amp; Japanese Products: Nigelle';
    $cat_num=114; //correct
    $override=false;
    $item_ids="300, 301, 303, 302, 271, 272, 273, 541, 536, 537, 538, 539, 540, 542, 543, 544, 545";
		$includefile="store/z_product_list.php";
		//NEW MODULE-----------------------------------------------
		break;
	case 'unique_IS':
		$title="Unique &amp; Japanese Products: IS";
//		$includefile="store/unique_IS.php";
		$template_include_file="unique_type.php";
    $cat_list_title='Unique &amp; Japanese Products: IS';
    $template_title='Unique &amp; Japanese Products: IS';
    $cat_num=113; //correct
    $override=false;
    $item_ids="278, 309, 282, 308, 281";
		$includefile="store/z_product_list.php";
		//NEW MODULE-----------------------------------------------
		break;
	case 'unique_Meros':
		$title="Unique &amp; Japanese Products: Meros";
//		$includefile="store/unique_Meros.php";
		$template_include_file="unique_type.php";
    $cat_list_title='Unique &amp; Japanese Products: Meros';
    $template_title='Unique &amp; Japanese Products: Meros';
    $cat_num=112; //correct
    $override=false;
    $item_ids="263, 551";
		$includefile="store/z_product_list.php";
		//NEW MODULE-----------------------------------------------
		break;
	case 'unique_Yuko':
		$title="Unique &amp; Japanese Products: Yuko";
//		$includefile="store/unique_Yuko.php";
		$template_include_file="unique_type.php";
    $cat_list_title='Unique &amp; Japanese Products: Yuko';
    $template_title='Unique &amp; Japanese Products: Yuko';
    $cat_num=110; //correct
    $override=false;
    $item_ids="287, 295, 294, 291, 290, 297, 296, 286, 284, 285, 326, 324, 122, 325";
		$includefile="store/z_product_list.php";
		//NEW MODULE-----------------------------------------------
		break;
	case 'unique_Rona':
		$title="Unique &amp; Japanese Products: Rona";
//		$includefile="store/unique_Rona.php";
		$template_include_file="unique_type.php";
    $cat_list_title='Unique &amp; Japanese Products: Rona';
    $template_title='Unique &amp; Japanese Products: Rona';
    $cat_num=111; //correct
    $override=false;
    $item_ids="260, 261";
		$includefile="store/z_product_list.php";
		//NEW MODULE-----------------------------------------------
		break;
	case 'unique_Crede':
		$title="Unique &amp; Japanese Products: Crede";
//		$includefile="store/unique_Crede.php";
		$template_include_file="unique_type.php";
    $cat_list_title='Unique &amp; Japanese Products: Crede';
    $template_title='Unique &amp; Japanese Products: Crede';
    $cat_num=118; //correct
    $override=false;
    $item_ids="68, 67, 65, 66, 433, 230, 231, 232, 233, 234, 235, 97";
		$includefile="store/z_product_list.php";
		//NEW MODULE-----------------------------------------------
		break;
	case 'prod_detailunique':
		$title="Japanese Products ";
		$includefile="store/prod_detailunique.php";
		break;
	//---JAPANESE PRODUCTS----------------------------------------------------------------------------------------------e-
	//---UNIQUE STYLING PRODUCTS----------------------------------------------------------------------------------------b-
	case 'uniquestylingproducts':
		$title="Unique Styling Products: Full Product List";
		//$includefile="store/uniquestylingproducts.php";
		$template_include_file="uniquestylingproducts_type.php";
    $cat_list_title='Unique Styling Products: Full Product List';
    $template_title='Unique Styling Products: Full Product List';
    $cat_num=60; //correct
    $override=false;
    $item_ids="441, 98, 330, 146, 147, 473, 472, 471, 470, 474, 475, 37, 479, 552";
		$includefile="store/z_product_list.php";
		//NEW MODULE-----------------------------------------------
		//new query done		
		break;
	case 'uniquestylingproducts_BlowDryers':
		$title="Unique Styling Products: Blow Dryers";
//		$includefile="store/uniquestylingproducts_BlowDryers.php";
		$template_include_file="uniquestylingproducts_type.php";
    $cat_list_title='Unique Styling Products: Blow Dryers';
    $template_title='Unique Styling Products: Blow Dryers';
    $cat_num=138; //correct
    $override=true;
    $item_ids="146, 147";
		$includefile="store/z_product_list.php";
		//NEW MODULE-----------------------------------------------
		break;
	case 'uniquestylingproducts_StylingIrons':
		$title="Unique Styling Products: Styling Irons";
//		$includefile="store/uniquestylingproducts_StylingIrons.php";
		$template_include_file="uniquestylingproducts_type.php";
    $cat_list_title='Unique Styling Products: Styling Irons';
    $template_title='Unique Styling Products: Styling Irons';
    $cat_num=137; //correct
    $override=true;
    $item_ids="441, 98, 330, 479, 527";
		$includefile="store/z_product_list.php";
		//NEW MODULE-----------------------------------------------
		break;
	case 'uniquestylingproducts_Brushes':
		$title="Unique Styling Products: Brushes";
//		$includefile="store/uniquestylingproducts_Brushes.php";
		$template_include_file="uniquestylingproducts_type.php";
    $cat_list_title='Unique Styling Products: Brushes';
    $template_title='Unique Styling Products: Brushes';
    $cat_num=136; //correct
    $override=true;
    $item_ids="473, 472, 471, 470, 474, 475, 528, 529, 530, 531, 532";
		$includefile="store/z_product_list.php";
		//NEW MODULE-----------------------------------------------
		//unknown category
		break;
	case 'prod_detail_uniqueSty':
		$title="Unique Styling Products ";
		$includefile="store/prod_detail_uniqueSty.php";
		break;	
	//---UNIQUE STYLING PRODUCTS----------------------------------------------------------------------------------------e-
	//---LEONOR GREYL PRODUCTS------------------------------------------------------------------------------------------b-
	case 'LeonorGreyl':
		$title="Leonor Greyl: Full Product List";
//		$includefile="store/LeonorGreyl.php";
		$template_include_file="LeonorGreyl_type.php";
    $cat_list_title='Leonor Greyl: Full Product List';
    $template_title='Leonor Greyl: Full Product List';
    $cat_num=124; //correct
    $override=false;
    $item_ids="555, 556, 557, 558, 559, 560, 561, 562, 563, 564, 565, 566, 567, 568, 569, 570, 571, 572, 573, 574, 575, 576, 577, 578, 579, 580, 581,594,595,596,597,598,599";
		$includefile="store/z_product_list.php";
		//NEW MODULE-----------------------------------------------		
		break;
	case 'LeonorGreyl_pages':
		$title="Leonor Greyl Products";
//		$includefile="store/LeonorGreyl_pages.php";
		$template_include_file="LeonorGreyl_type.php";
    $cat_list_title='Leonor Greyl Products';
    $template_title='Leonor Greyl Products';
    $override=true;
						switch($_REQUEST['p'])
						{
								case'1':
    $cat_num=125; //correct
									$ids="556,560,559,562,561,571,570,572,572,597,598,573,574";
									break;
								case'2':
    $cat_num=126; //correct
									$ids="556,560,561,559,571,570,572,574,597,598,573,575,601";
									break;
								case'3':
    $cat_num=127; //correct
									$ids="556,555,559,560,562,571,597,570,572,573,574,598,575,599";
									break;
								case'4':
    $cat_num=128; //correct
									$ids="564,566,567,563,567,565,575,594,596,581,598,599,601";
									break;
								case'5':
    $cat_num=129; //correct
									$ids="555,559,581,598,600";
									break;
								case'6':
    $cat_num=130; //correct
									$ids="579,568,569,565,580,564,581,598";
									break;
								case'7':
    $cat_num=131; //correct
									$ids="555,563,579,568,576,577,578,595,596,594,575,599,601";
									break;
								case'8':
    $cat_num=132; //correct
									$ids="563,564,566,597,576,577,578,581,598,600";
									break;
								case'9':
    $cat_num=133; //correct
									$ids="556,559,563,566,562,571,597,570,599,557,601";
									break;
								case'10':
    $cat_num=134; //correct
									$ids="581,597,598,594,575,573,599,595,596,601";
									break;
								case'11':
    $cat_num=135; //correct
									$ids="557,566,565";
									break;

								default:
    $cat_num=124; //correct
							    $ids="";
						}    
    $item_ids=$ids;
		$includefile="store/z_product_list.php";
		//NEW MODULE-----------------------------------------------		
		break;
	case 'prod_detailLG':
		$title="LEONOR GREYL PRODUCTS";
		$includefile="store/prod_detailLG.php";
		break;
	//---LEONOR GREYL PRODUCTS------------------------------------------------------------------------------------------e-
	//---ANAGEN THERAPY PRODUCTS----------------------------------------------------------------------------------------b-
	case 'AnagenTherapyProducts':
		$title="Anagen Therapy";
//		$includefile="store/AnagenTherapy.php";
		$template_include_file="anagen_type.php";
    $cat_list_title='Anagen Therapy Products : All Products of Anagen Therapy';
    $template_title='Anagen Therapy Products';
    $cat_num=119; //correct
    $override=false;
    $item_ids="701, 702, 703, 705, 704, 700";
		$includefile="store/z_product_list.php";		
		//these are in sub-menu, why?
		//no category, no static list
		break;
	//---ANAGEN THERAPY PRODUCTS----------------------------------------------------------------------------------------e-
	//---GREAT LENGTHS/NXT----------------------------------------------------------------------------------------------b-
	case 'skincare':
		$title="Great Lengths NXT Extension Products";
		$includefile="store/skincare.php";
		//new query done
		$template_include_file="skincare_type.php";
    $cat_list_title='Great Lengths NXT Extension Products';
    $template_title='Great Lengths NXT Extension Products';
    $cat_num=120; //correct
    $override=false;
    $item_ids="";
		$includefile="store/z_product_list.php";
		//NEW MODULE-----------------------------------------------
		break;
	case 'skincare_shampoos':
		$title="Great Lengths/NXT: Shampoos";
//		$includefile="store/skincare_shampoos.php";
		$template_include_file="skincare_type.php";
    $cat_list_title='Great Lengths/NXT: Shampoos';
    $template_title='Great Lengths/NXT: Shampoos';
    $cat_num=122; //correct
    $override=false;
    $item_ids="310, 311, 312, 313";
		$includefile="store/z_product_list.php";
		//NEW MODULE-----------------------------------------------
		break;
	case 'skincare_conditionar':
		$title="Great Lengths/NXT: Conditioners";
//		$includefile="store/skincare_conditionar.php";
		$template_include_file="skincare_type.php";
    $cat_list_title='Great Lengths/NXT: Conditioners';
    $template_title='Great Lengths/NXT: Conditioners';
    $cat_num=123; //correct
    $override=false;
    $item_ids="315, 317";
		$includefile="store/z_product_list.php";
		//NEW MODULE-----------------------------------------------
		break;
	case 'skincare_styling':
		$title="Great Lengths/NXT: Styling Products";
//		$includefile="store/skincare_styling.php";
		$template_include_file="skincare_type.php";
    $cat_list_title='Great Lengths/NXT: Styling Products';
    $template_title='Great Lengths/NXT: Styling Products';
    $cat_num=121; //correct
    $override=false;
    $item_ids="314, 318, 316";
		$includefile="store/z_product_list.php";
		//NEW MODULE-----------------------------------------------
		break;
	//---GREAT LENGTHS/NXT----------------------------------------------------------------------------------------------e-
	//---HAIR PRODUCTS==================================================================================================E-
	//---HAIR SERVICES==================================================================================================B-
	case 'japanesestraightening':
		$title="Peter Louis SALON";
		$includefile="japanesestraightening.php";
		//no products
		break;
	case 'index':
		$title="Peter Louis SALON";
		$includefile="greatlengths/index.php";
		//no products
		break;
	case 'hair_color':
		$title="Peter Louis SALON";
		$includefile="hair_color.php";
		//no products		
		break;
	case 'services':
		$title="Peter Louis SALON";
		$includefile="services.php";
  	//no products					
		break;
	case 'prevention':
		$title="Peter Louis SALON";
		$includefile="prevention.php";
		//these are in sub-menu, why?
		break;
	case 'llht':
		$title="Peter Louis SALON";
		$includefile="llht.php";
		//these are in sub-menu, why?
		break;
	case 'light':
		$title="Peter Louis SALON";
		$includefile="light.php";
		//these are in sub-menu, why?
		break;
	case 'eyelash':
		$title="Peter Louis SALON";
		$includefile="eyelash.php";
		//these are in sub-menu, why?
		break;
	//---HAIR SERVICES==================================================================================================E-
	case 'clearancesale':
		$title="Gift Certificates ";
		$includefile="store/clearancesale.php";
		break;
	case 'prod_detail':
		$title="Peter Louis SALON";
		$includefile="store/prod_detail.php";
		break;
	case 'prod_detaildry':
		$title="Phytologie: Phytospecific";
		$includefile="store/prod_detaildry.php";
		break;
	case 'salonpic':
		$title="Peter Louis SALON";
		$includefile="salonpic.php";
		//no products		
		break;
	case 'media_press':
		$title="Peter Louis SALON";
		$includefile="media_press.php";
		//no products		
		break;
	case 'contact':
		$title="Peter Louis SALON";
		$includefile="contact.php";
		//no products		
		break;
	case 'petersbio':
		$title="Peter Louis SALON";
		$includefile="petersbio.php";
		//no products				
		break;
	case 'gallery':
		$title="Peter Louis SALON";
		$includefile="greatlengths/gallery1.php";
		break;
	case 'gallery2':
		$title="Peter Louis SALON";
		$includefile="greatlengths/gallery2.php";
		break;
	case 'gallery3':
		$title="Peter Louis SALON";
		$includefile="greatlengths/gallery3.php";
		break;
	case 'gallery4':
		$title="Peter Louis SALON";
		$includefile="greatlengths/gallery4.php";
		break;
	case 'gallery5':
		$title="Peter Louis SALON";
		$includefile="greatlengths/gallery5.php";
		break;
	case 'gallery6':
		$title="Peter Louis SALON";
		$includefile="greatlengths/gallery6.php";
		break;
	case 'gallery7':
		$title="Peter Louis SALON";
		$includefile="greatlengths/gallery7.php";
		break;
	case 'gallery8':
		$title="Peter Louis SALON";
		$includefile="greatlengths/gallery8.php";
		break;
	case 'gallery9':
		$title="Peter Louis SALON";
		$includefile="greatlengths/gallery9.php";
		break;
	case 'gallery10':
		$title="Peter Louis SALON";
		$includefile="greatlengths/gallery10.php";
		break;
	case 'gallery11':
		$title="Peter Louis SALON";
		$includefile="greatlengths/gallery11.php";
		break;
	case 'gallery12':
		$title="Peter Louis SALON";
		$includefile="greatlengths/gallery12.php";
		break;
	case 'gallery13':
		$title="Peter Louis SALON";
		$includefile="greatlengths/gallery13.php";
		break;
	case 'about':
		$title="Peter Louis SALON";
		$includefile="greatlengths/about.php";
		break;
	case 'long':
		$title="Peter Louis SALON";
		$includefile="greatlengths/long.php";
		break;
	case 'long1':
		$title="Peter Louis SALON";
		$includefile="greatlengths/long1.php";
		break;
	case 'volume_strong1':
		$title="Peter Louis SALON";
		$includefile="greatlengths/volume_strong1.php";
		break;
	case 'volume_strong2':
		$title="Peter Louis SALON";
		$includefile="greatlengths/volume_strong2.php";
		break;
	case 'thick1':
		$title="Peter Louis SALON";
		$includefile="greatlengths/thick1.php";
		break;
	case 'thick2':
		$title="Peter Louis SALON";
		$includefile="greatlengths/thick2.php";
		break;
	case 'methodds1':
		$title="Peter Louis SALON";
		$includefile="greatlengths/methodds1.php";
		break;
	case 'methodds2':
		$title="Peter Louis SALON";
		$includefile="greatlengths/methodds2.php";
		break;
	case 'methodds3':
		$title="Peter Louis SALON";
		$includefile="greatlengths/methodds3.php";
		break;
	case 'methodds4':
		$title="Peter Louis SALON";
		$includefile="greatlengths/methodds4.php";
		break;
	case 'methodds5':
		$title="Peter Louis SALON";
		$includefile="greatlengths/methodds5.php";
		break;
	case 'history1':
			$title="Peter Louis SALON";
			$includefile="greatlengths/history1.php";
			break;
	case 'history2':
			$title="Peter Louis SALON";
			$includefile="greatlengths/history2.php";
			break;
	case 'history3':
			$title="Peter Louis SALON";
			$includefile="greatlengths/history3.php";
			break;
	case 'history4':
			$title="Peter Louis SALON";
			$includefile="greatlengths/history4.php";
			break;
	case 'history5':
			$title="Peter Louis SALON";
			$includefile="greatlengths/history5.php";
			break;
	case 'history6':
			$title="Peter Louis SALON";
			$includefile="greatlengths/history6.php";
			break;
	case 'history7':
			$title="Peter Louis SALON";
			$includefile="greatlengths/history7.php";
			break;
	case 'history8':
			$title="Peter Louis SALON";
			$includefile="greatlengths/history8.php";
			break;
	case 'AnagenTherapy':
			$title="Peter Louis SALON";
			$includefile="loss.php";
			break;
	case 'blow':
			$title="Peter Louis SALON";
			$includefile="blow.php";
  		//no products		
			break;
	case 'privacy':
			$title="Peter Louis SALON";
			$includefile="privacy.php";
  		//no products		
			break;
	case 'terms':
			$title="Peter Louis SALON";
			$includefile="terms_conditions.php";
			break;
	case 'search':
			$title="Peter Louis SALON";
			$includefile="search.php";
			break;

		default :
			$title="Peter Louis SALON";
			$includefile="home.php";
			break;
	}

//---load meta tags---------------------------------------------------------------------------------------------------b-	
	$sqls="select * from meta_tag where file='".$file."'";
	$re=mysql_query($sqls);
	if (mysql_num_rows($re) > 0)
	  {
  	$obj=mysql_fetch_object($re);
		$metadescription	=	$obj->metadescription;
		$metakeywords	=	$obj->metakeywords;
	  }
    else
    {
		$metadescription	=	"Shop for Phytologie hair care products, shampoos and conditioners, the ultimate in hair care! Brought to you by Peter Louis Salon in New York City.";
		$metakeywords	=	"Phyto, shampoo, gifts, Phyto9, Phytologie, Phytology, makeup, salon, gel, makeovers, dry hair, phytodefrisant, phytosesame, oily, dandruff shampoo, conditioner, nail care, phytologist, phytojoba, hairstylist, discount, Cosmetics, cosmotology, Peter Louis Salon, New York, Manhattan, Second, color, foil, blonde, bad hair day, Upper East Side";
	  }
//---load meta tags---------------------------------------------------------------------------------------------------e-	
?>