function radio_button(buttonName, num)
{
  var i, j, n;

  with (document.forms[0])
  {
    n = elements.length;
    for (i = 0, j = 0; i < n; i++)
    {
      if (elements[i].name == buttonName)
      {
        if (j++ == num)
          return elements[i];
      }
    }
  }

  return null;
}

function radio_button_checked(buttonName, num)
{
  var button;

  if ((button = radio_button(buttonName, num)) != null)
    return button.checked;
  else
    return false;
}

function CheckRadioButton(buttonName, num)
{
  var button;

  if ((button = radio_button(buttonName, num)) != null)
    button.checked = true;
}

function UpdateImages(root, imageListName, imageSizeName, imageArray, imageArrayIndex, form)
{
  var imagelist;
  var dir, image;
  var i, j, n;

  imagelist = form_element(imageListName);
  if (imagelist == null)
    return;

  // populate the image list for the selected image directory
  j = imageArrayIndex;
  n = imageArray[j].length;
  dir = imageArray[j][0];
  imagelist.options.length = 0;

  for (i = 1; i < n; i++)
  {
    image = imageArray[j][i];

    path = i > 1? dir + slash + image : image;
    if (!button_images)
    {
      if (path.slice(0, root.length) == root)
        path = path.slice(root.length+1);
    }

    imagelist.options[i-1] = new Option(image, path, false, false);
  }

  ChangeImage(imageListName, imageSizeName);
}

function ChangeImage(imageList, imageSize)
{
  var imageName, imageDir;
  var imageURL, imagePath;
  var image, size;
  var width, height;
  var features, i;

  if ((imageWindow == null || imageWindow.closed) && !view_button_clicked)
    return;

  view_button_clicked = false;

  imageList = form_element(imageList);
  if (imageList == null)
    return;

  imageName = imageList.value;
  imageURL = MakeURL(imageName);

  if (imageURL.length > 0 && imageSize.length > 0)
  {
    imageSize = form_element(imageSize);
    if (imageSize != null)
    {
      size = imageSize.value;
      if (size > 0)
      {
        i = imageURL.lastIndexOf('/') + 1;
        imageName = imageURL.slice(i);
        imageURL = imageURL.slice(0, i) + 'ss_size' + size + '/' + imageName;
      }
    }
  }

  if (imageURL.length == 0)
  {
    imageURL = image_url + '/screen/dotclear.gif';
    size = 0;
  }

  image = new Image;
  image.src = imageURL;

  width = Math.max(image.width + 60, 400);
  height = Math.max(image.height + 130, 300);

  if (imageWindow == null || imageWindow.closed)
  {
    features = 'width=' + width + ', ' + 'height=' + height + ', ' +
      'left=0, top=0, resizable=yes, scrollbars=yes';

    imageWindow = window.open('', 'imageWindow', features);
  }
  else
    imageWindow.resizeTo(width, height);

  with (imageWindow)
  {
    document.open();
    document.write('<html><head><title>' + view_image + '</title></head>\n');
    document.write('<body bgcolor="#FFFFE0">\n');
    document.write('<img src="' + imageURL + '">\n');

    if (size > 0)
    {
      imageDir = form_element('d:' + imageList.name);
      imagePath = imageDir.value + slash + imageName;
      imagePath = imagePath.replace(/\s/g, '+');

      document.write('<p>\n');
      document.write('<a href="' + backoffice_url + '/mediam.cgi?create_resized_image=1'
        + '&image=' + imagePath + '&size=' + size + '" target=_self>\n');
      document.write('<font size=1>' + create_resized_image + '</a></font>\n');
    }

    document.write('</body></html>\n');
    document.close();
  }
}

function ChangeURL(imageList)
{
  var imageName, imageURL, imageField;
  var i, n;

  imageList = form_element(imageList);
  if (imageList == null)
    return;

  imageName = imageList.value;
  imageURL = MakeURL(imageName);

  imageField = 't:' + imageList.name;
  imageField = form_element(imageField);
  
  if (imageField != null)
    imageField.value = imageURL;
}

function MaxImages(exceeded)
{
  if (exceeded)
    alert(too_many_images + '\n' + click_the_link + '\n\n' + after_switching);
}
