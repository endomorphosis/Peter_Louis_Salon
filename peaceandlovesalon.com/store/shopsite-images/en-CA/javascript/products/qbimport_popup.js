function return_qbaccnt()
{
  var accnt_name = document.forms[0].accnts.value;
  var i, n;

  if (accnt_name.length == 0)
  {
    alert(qbaccnt_msg);
    return false;
  }

  with (window.opener)
  {
    with (document.forms[0])
    {
      n = elements.length;
      for (i = 0; i < n; i++)
      {
        if (elements[i].name == qbaccnt)
        {
          elements[i].value = accnt_name;
          elements[i-2].checked = true;
          break;
        }
      }
    }
  }

  window.close();
}

function return_qbitem()
{
  var item_name = document.forms[0].item_names.value;
  var item_desc = document.forms[0].item_descs.value;
  var i, n;

  if (item_name.length == 0)
  {
    alert(qbitem_msg);
    return false;
  }

  with (window.opener)
  {
    with (document.forms[0])
    {
      n = elements.length;
      for (i = 0; i < n; i++)
      {
        if (elements[i].name == qbitem)
        {
          elements[i].value = item_name;
          elements[i+1].value = item_desc;
          elements[i-1].checked = true;
          break;
        }
      }
    }
  }

  window.close();
}

function show_instructions(i)
{
  var url = backoffice_url + '/products.cgi?qbimport_instructions=' + i;
  window.open(url, 'qbimport', 'width=465,height=240,scrollbars=yes,resizable=yes');
  return false;
}
