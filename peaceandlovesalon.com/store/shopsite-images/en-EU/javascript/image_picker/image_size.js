function form_element(element_name)
{
  var i, n;

  with (document.forms[0])
  {
    n = elements.length;
    for (i = 0; i < n; i++)
    {
      if (elements[i].name == element_name)
        return elements[i];
    }
  }

  return null;
}

function ImageURL(image_name)
{
  return image_name.indexOf('http://') == 0;
}

function ImageResizeable(image_name)
{
  var image_type;
  var i;

  if ((i = image_name.lastIndexOf('.')) >= 0)
  {
    image_type = image_name.slice(i + 1);
    i = image_types.indexOf(image_type.toLowerCase());
  }

  return i >= 0;
}

function CheckImageSize(image_id, size_id, m_size_id)
{
  var image_name, image_size;
  var m_image_size = null;
  var none = 'none';
  var i;

  if (size_id.length == 0)
    return true;  // no size associated with the image

  image_size = form_element(size_id);
  if (image_size == null || image_size.selectedIndex == 0)
    return true;

  if (m_size_id.length > 0)
    m_image_size = form_element(m_size_id);

  if (image_id.slice(0, 2) == 't:')
  {
    image_name = form_element(image_id);

    if (image_name == null)
      return true;

    if (image_name.value == none || image_name.value.length == 0)
    {
      image_size.selectedIndex = 0;
      if (m_image_size != null)
        m_image_size.selectedIndex = 0;
      return true;
    }

    if (ImageURL(image_name.value))
    {
      alert(url_msg);
      image_size.selectedIndex = 0;
      if (m_image_size != null)
        m_image_size.selectedIndex = 0;
      return false;
    }

    if (!ImageResizeable(image_name.value))
    {
      alert(type_msg);
      image_size.selectedIndex = 0;
      if (m_image_size != null)
        m_image_size.selectedIndex = 0;
      return false;
    }

    return true;
  }

  if (image_id.slice(0, 3) == 'r1:')
  {
    if (radio_button_checked(image_id, 0))
      image_name = form_element(image_id.slice(3));
    else
      image_name = form_element('t:' + image_id.slice(3));

    if (image_name == null)
      return true;

    if (image_name.value == none || image_name.value.length == 0)
    {
      image_size.selectedIndex = 0;
      if (m_image_size != null)
        m_image_size.selectedIndex = 0;
      return true;
    }

    if (ImageURL(image_name.value))
    {
      alert(url_msg);
      image_size.selectedIndex = 0;
      if (m_image_size != null)
        m_image_size.selectedIndex = 0;
      return false;
    }

    if (!ImageResizeable(image_name.value))
    {
      alert(type_msg);
      image_size.selectedIndex = 0;
      if (m_image_size != null)
        m_image_size.selectedIndex = 0;
      return false;
    }

    return true;
  }

  if (radio_button_checked('r1:' + image_id, 0))
    image_name = form_element(image_id);
  else
    image_name = form_element('t:' + image_id);

  if (image_name == null)
    return true;

  if (image_name.value == none || image_name.value.length == 0)
  {
    image_size.selectedIndex = 0;
    if (m_image_size != null)
      m_image_size.selectedIndex = 0;
    return true;
  }

  if (!ImageResizeable(image_name.value))
  {
    alert(type_msg);
    image_size.selectedIndex = 0;
    if (m_image_size != null)
      m_image_size.selectedIndex = 0;
    return false;
  }

  return true;
}

function SelectSameImage(src, target, form)
{
  var srcDir, srcImage;
  var targetDir, targetImage;
  var targetButton;
  var name;
  var i, n;

  srcDir = form_element('d:' + src);
  srcImage = form_element(src);

  targetDir = form_element('d:' + target);
  targetImage = form_element(target);

  if (targetDir != null && targetImage != null)
  {
    if (targetDir.selectedIndex != srcDir.selectedIndex)
    {
      targetDir.selectedIndex = srcDir.selectedIndex;
      UpdateImages('media', targetImage.name, '', imagedir, srcDir.selectedIndex, form);
    }

    targetImage.selectedIndex = srcImage.selectedIndex;
    CheckRadioButton('r1:' + target, 0);
  }
}

function CopyImagePath(src, target)
{
  var t = form_element(target);
  if (t != null)
    t.value = form_element(src).value;
}
