function get_ship_params(carrier_id)
{
  var params;
  var i;

  // Collect parameters to be passed to the configuration screen
  // for the selected shipping carrier.

  params = carrier_config[carrier_id];

  with (document.forms[0])
  {
    for (i = 0; i < shipmethod.length; i++)
    {
      if (shipmethod[i].checked == true)
      {
        params += '&shipmethod=' + shipmethod[i].value;
        break;
      }
    }

    for (i = 0; i < selfmt.length; i++)
    {
      if (selfmt[i].checked == true)
      {
        params += '&selfmt=' + selfmt[i].value;
        break;
      }
    }

    if (pro_store)
    {
      if (shipapi.checked == true)
        params += '&shipapi=' + shipapi.value;
    }

    if (ship_order.value.length > 0)
      params += '&ship_order=' + ship_order.value;

    if (ups_active.checked == true)
      params += '&ups_active=1';

    if (fedex_active.checked == true)
      params += '&fedex_active=1';

    if (usps_active.checked == true)
      params += '&usps_active=1';
  }

  return params;
}

function configure_carrier(carrier_id, carrier_name)
{
  var params = get_ship_params(carrier_id, carrier_name);
  var config_url;

  config_url = backoffice_url + '/' + carrier_name + '.cgi?' + params;
  window.location.href = config_url;
}

function move_ship_item(incr)
{
  var items, text, value;
  var i, j, n;

  with (document.forms[0])
  {
    items = ship_items.options;
    i = ship_items.selectedIndex;
    if (i < 0)
    {
      alert(select_list_item);
      return false;
    }

    j = i + incr;
    n = items.length;

    if ((incr < 0 && j < 0) || (incr > 0 && j >= n))
      return false;

    text = items[j].text;
    value = items[j].value;
    items[j].text = items[i].text;
    items[j].value = items[i].value;
    items[i].text = text;
    items[i].value = value;

    items[i].selected = false;
    items[j].selected = true;

    update_ship_order();
  }

  return false;
}

function include_ship_item(checked, text, value)
{
  var items;
  var i, n;

  with (document.forms[0])
  {
    items = ship_items.options;
    n = items.length;
    
    for (i = 0; i < n; i++)
    {
      if (items[i].value == value)
        break;
    }

    if (checked)
    {
      if (i == n)
        items[i] = new Option(text, value, false, false);
    }
    else
    if (i < n)
      items[i] = null;

    update_ship_order();
  }
}

function update_ship_order()
{
  var items;
  var i, n;

  with (document.forms[0])
  {
    ship_order.value = '';
    items = ship_items.options;
    n = items.length;

    for (i = 0; i < n; i++)
    {
      if (i > 0)
        ship_order.value += '|';

      ship_order.value += items[i].value;
    }
  }
}
