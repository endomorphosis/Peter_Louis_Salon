function set_color(savedColorValue, colorName)
{
  var selectedColor = document.getElementById('selectedColor');
  var savedColor = document.getElementById('savedColor');
  var colorList = document.getElementById('colorList');
  var inputobj = window.opener.document.getElementById(colorName);;
  var selectedColorValue = inputobj.value;

  if (savedColorValue.length == 0)
  {
    savedColorValue = selectedColorValue;
    colorList.options[0].value = selectedColorValue;
  }

  with (document.colorForm)
  {
    selectedColor.value = selectedColorValue;
    savedColor.value = savedColorValue;
    color[1].checked = true;
  }

  if (document.layers)
  {
    selectedColor.bgColor = selectedColorValue;
    savedColor.bgColor = savedColorValue;
  }
  else
  if (document.all)
  {
    selectedColor.style.backgroundColor = selectedColorValue;
    savedColor.style.backgroundColor = savedColorValue;
  }
  else
  if (document.getElementById)
  {
    selectedColor.style.backgroundColor = selectedColorValue;
    savedColor.style.backgroundColor = savedColorValue;
  }
}

function change_color(colorList)
{
  var i = colorList.selectedIndex;
  var colorValue = colorList.options[i].value;
  var selectedColor = document.getElementById('selectedColor');

  with (document.colorForm)
  {
    selectedColor.value = colorValue;
    color[0].checked = true;
  }
  
  if (document.layers)
    selectedColor.bgColor = colorValue;
  else
  if (document.all)
    selectedColor.style.backgroundColor = colorValue;
  else
  if (document.getElementById)
    selectedColor.style.backgroundColor = colorValue;
}

function apply_color(colorName)
{
  var selectedColor = document.getElementById('selectedColor');
  var savedColor = document.getElementById('saveddColor');
  var inputobj, showcolor;
  var colorValue;

  with (document.colorForm)
  {
    colorValue = color[0].checked? selectedColor.value : savedColor.value;
  }

  with (window.opener)
  {
    inputobj = document.getElementById(colorName);
    showcolor = document.getElementById('show_' + colorName);
  }

  if (document.layers)
  {
    showcolor.bgColor = colorValue;
    inputobj.value = colorValue;
  }
  else
  if (document.all)
  {
    showcolor.style.backgroundColor = colorValue;
    inputobj.value = colorValue;
  }
  else
  if (document.getElementById)
  {
    showcolor.style.backgroundColor = colorValue;
    inputobj.value = colorValue;
  }
}
