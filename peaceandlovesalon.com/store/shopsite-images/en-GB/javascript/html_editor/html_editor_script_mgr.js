function toggle_toolbar(text_id)
{
  var divToolbar = document.getElementById('divToolbar_' + text_id);
  var display_mode = divToolbar.style.display == 'none'? '' : 'none';

  divToolbar.style.display = display_mode;
}

function edit_toolbar(text_id)
{
  var editor_toolbar = document.getElementById('Toolbar_' + text_id);
  var toolbar_name, url;

  toolbar_name = editor_toolbar.value;
  url = backoffice_url + '/html_editor.cgi?edit_toolbar=' + 
    encodeURI(toolbar_name) + '&editor_id=' + encodeURI(text_id);
  window.open(url, '', 'width=680,height=450,resizable=yes,scrollbars=yes');
}

function change_toolbar(text_id)
{
  var editor_config = document.getElementById(text_id + '___Config');
  var editor_toolbar = document.getElementById('Toolbar_' + text_id);
  var text_area = document.getElementById(text_id);
  var location, search, editor_name, toolbar_name;
  var parts, url, num, i;

  var oEditor = FCKeditorAPI.GetInstance(text_id);

  for (i = 0; i < window.frames.length; i++)
  {
    location = window.frames[i].location;
    search = location.search;

    parts = search.split('&');
    parts = parts[0].split('=');
    editor_name = parts[1];

    if (editor_name == text_id)
      break;
  }

  toolbar_name = editor_toolbar.value;
  num = Math.floor(Math.random() * 10000);
  editor_config.value = editorConfig + toolbar_url + '/' + 
    toolbar_name + '.js?' + num;

  text_area.value = oEditor.GetXHTML();

  url = editor_url + '/editor/fckeditor.html?' + 
    'InstanceName=' + text_id + '&Toolbar=' + encodeURI(toolbar_name);
  location.replace(url);
}
