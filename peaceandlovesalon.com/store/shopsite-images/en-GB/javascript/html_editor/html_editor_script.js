function html_editor_popup(text_id)
{
  var oEditor = FCKeditorAPI.GetInstance(text_id);
  var toolbar = document.getElementById('Toolbar_' + text_id);
  var toolbar_name, editor_text, features, url;

  toolbar_name = encodeURI(toolbar.value);
  editor_text = encodeURIComponent(oEditor.GetXHTML());
  url = backoffice_url + '/html_editor.cgi?popup=editor&toolbar=' + toolbar_name + 
    '&text=' + editor_text;

  features = 'width=530,height=300,left=0,top=0,screenX=0,screenY=0,resizable=yes';
  window.open(url, '', features);
}

function toggle_text_editor(text_id)
{
  var divTextArea = document.getElementById('divTextArea_' + text_id);
  var divTextEditor = document.getElementById('divTextEditor_' + text_id);
  var text_area = document.getElementById(text_id);
  var oEditor = null;
  var num;

  if (typeof(FCKeditorAPI) != 'undefined')
    oEditor = FCKeditorAPI.GetInstance(text_id);

  if (divTextArea.style.display == 'none')
  {
    // Set the textarea value to the editor value
    if (oEditor != null)
      text_area.value = oEditor.GetXHTML();

    divTextArea.style.display = '';
    divTextEditor.style.display = 'none';
    text_area.focus();
  }
  else
  {
    if (oEditor == null)
    {
      var editor_toolbar = document.getElementById('Toolbar_' + text_id);
      var editor_config = document.getElementById(text_id + '___Config');
      var location, search, editor_name, toolbar_name;
      var parts, url, i;

      // Find the editor iframe for this textarea
      for (i = 0; i < window.frames.length; i++)
      {
        location = window.frames[i].location;
        search = location.search;

        parts = search.split('&');
        parts = parts[0].split('=');
        editor_name = parts[1];

        if (editor_name == text_id)
          break;
      }

      toolbar_name = editor_toolbar.value;
      num = Math.floor(Math.random() * 10000);
      editor_config.value = editorConfig + toolbar_url + '/' + 
        toolbar_name + '.js?' + num;

      url = editor_url + '/editor/fckeditor.html?' + 
        'InstanceName=' + text_id + '&Toolbar=' + encodeURI(toolbar_name);
      location.replace(url);
    }
    else
      oEditor.SetData(text_area.value);

    divTextArea.style.display = 'none';
    divTextEditor.style.display = '';

    if (oEditor != null)
      oEditor.Focus();
  }
}
