var npick = 0;

function color_picker(colorName, colorValue, anchorName)
{
  var coordinates = getAnchorWindowPosition(anchorName);
  var xpos = coordinates.x + 120;
  var ypos = coordinates.y - 160;
  var features = 'width=256,height=480,left=' + xpos + ',top=' + ypos;
  var url;

  npick++; // forces loading a new page instead of a cached page
  url = cp_url + '/' + cp_caller + '.cgi?color_picker=0&color_name=' +
    colorName + '&color_value=' + colorValue + '&caller=' + cp_caller +
    '&pick=' + npick;
  window.open(url, 'colorPicker', features, true);
  return false;
}

function change_color(colorName)
{
  var inputobj = document.getElementById(colorName);
  var showcolor = document.getElementById('show_' + colorName);
  var color = inputobj.value.toUpperCase();
  var hex = "#0123456789ABCDEF";
  var i, n;

  n = color.length;
  if (n == 0)
  {
    color = '#FFFFFF';
    n = color.length;
  }

  if (n != 6 && n != 7)
    n = 0;  // invalid color value

  for (i = 0; i < n; i++)
  {
    if (hex.indexOf(color.charAt(i)) < 0)
      break;
  }

  if (n == 0 || i < n)
  {
      alert(cp_msg + ': ' + color);
      return false;
  }

  if (color.charAt(0) != '#')
    color = '#' + color;

  inputobj.value = color;

  if (document.layers)
  {
    showcolor.bgColor = color;
  }
  else
  if (document.all)
  {
    showcolor.style.backgroundColor = color;
  }
  else
  if (document.getElementById)
  {
    showcolor.style.backgroundColor = color;
  }

  return false;
}
