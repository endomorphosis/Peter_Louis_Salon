function select_qbaccnt(product_name, accnt_name)
{
  var url = backoffice_url + '/products.cgi?qbimport_popup=' + qb_accnts +
   '&product_name=' + product_name + '&accnt_name=' + accnt_name;
  window.open(url, 'qbaccnts', 'width=600,height=485,scrollbars=yes,resizable=yes');
  return false;
}

function select_qbitem(product_name, item_name, item_desc)
{
  var url = backoffice_url + '/products.cgi?qbimport_popup=' + qb_items +
   '&product_name=' + product_name + '&item_name=' + item_name + 
   '&item_desc=' + item_desc;
  window.open(url, 'qbitems', 'width=600,height=505,scrollbars=yes,resizable=yes');
  return false;
}

function set_import_option(button_name, index, value)
{
  var i, j, n;

  with (document.forms[0])
  {
    n = elements.length;
    for (i = 0, j = 0; i < n; i++)
    {
      if (elements[i].name == button_name)
      {
        if (j++ == index)
        {
          elements[i].checked = value;
          break;
        }
      }
    }
  }
}
