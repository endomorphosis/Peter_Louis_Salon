// Percent Bar - Version 1.0
// Author: Brian Gosselin of http://scriptasylum.com
// Script featured on http://www.dynamicdrive.com
// Note: Modified by Dynamicdrive so incr/decrCount() accepts any percentage
// Note: Modified by ShopSite, Inc. to display multiple progress bars on a page

var loadedcolor = 'navy' ;            // PROGRESS BAR COLOR
var unloadedcolor = 'lightgrey';      // BGCOLOR OF UNLOADED AREA
var barheight = 15;                   // HEIGHT OF PROGRESS BAR IN PIXELS
var barwidth = 350;                   // WIDTH OF THE BAR IN PIXELS
var bordercolor = 'black';            // COLOR OF THE BORDER

// THE FUNCTION BELOW CONTAINS THE ACTION(S) TAKEN ONCE BAR REACHES 100%.
// IF NO ACTION IS DESIRED, TAKE EVERYTHING OUT FROM BETWEEN THE CURLY BRACES ({})
// BUT LEAVE THE FUNCTION NAME AND CURLY BRACES IN PLACE.
// PRESENTLY, IT IS SET TO DO NOTHING, BUT CAN BE CHANGED EASILY.
// TO CAUSE A REDIRECT, INSERT THE FOLLOWING LINE IN BETWEEN THE CURLY BRACES:
// window.location="http://redirect_page.html";
// JUST CHANGE THE ACTUAL URL IT "POINTS" TO.

var action=function()
{
//window.location="http://www.dynamicdrive.com
}

// Uncomment to initialize a single progress bar with the default name.
// window.onload = initialize;

//*****************************************************//
//**********  DO NOT EDIT BEYOND THIS POINT  **********//
//*****************************************************//

var w3c = (document.getElementById)? true:false;
var ns4 = (document.layers)? true:false;
var ie4 = (document.all && !w3c)? true:false;
var ie5 = (document.all && w3c)? true:false;
var ns6 = (w3c && navigator.appName.indexOf("Netscape") >= 0)? true:false;

var ctrlName;   // name of currently active progress bar
var ctrlRec;    // current progress bar's div or layer object
var ctrlInd;    // current progress bar's percent done indicator

var rec = Array(3);     // holds div or layer object for each progress bar
var ind = Array(3);     // holds percent done indicator object for each progress bar
var loaded = Array(3);  // holds current indicator setting for each progress bar

var blocksize = (barwidth - 2) / 100;
barheight = Math.max(4, barheight);


////////////////////////////////////////////////////////////////////////
//
// This function is called by the window.onload event to initialize
// a progress bar created with the default name.
//
////////////////////////////////////////////////////////////////////////

function initialize()
{
  initProgressBar(1);
}

////////////////////////////////////////////////////////////////////////
//
// Create progress bar with the default name.
//
////////////////////////////////////////////////////////////////////////

function progressBar()
{
  createProgressBar(1);
}

////////////////////////////////////////////////////////////////////////
//
// Create a progress bar and initialize it.
//
////////////////////////////////////////////////////////////////////////

function createProgressBar(name)
{
  rec[name] = name + '_R';
  ind[name] = name + '_I';

  printProgressBar(name);
  initProgressBar(name);
}

////////////////////////////////////////////////////////////////////////
//
// Print a progress bar with the given name.
//
////////////////////////////////////////////////////////////////////////

function printProgressBar(name)
{
  var txt = '';
  
  if (ns4)
  {
    txt += '<table cellpadding=0 cellspacing=0 border=0><tr><td>';
    txt += '<ilayer name="'+rec[name]+'" width="'+barwidth+'" height="'+barheight+'">';
    txt += '<layer width="'+barwidth+'" height="'+barheight+'" bgcolor="'+bordercolor+'" top="0" left="0"></layer>';
    txt += '<layer width="'+(barwidth-2)+'" height="'+(barheight-2)+'" bgcolor="'+unloadedcolor+'" top="1" left="1"></layer>';
    txt += '<layer name="'+ind[name]+'" width="'+(barwidth-2)+'" height="'+(barheight-2)+'" bgcolor="'+loadedcolor+'" top="1" left="1"></layer>';
    txt += '</ilayer>';
    txt += '</td></tr></table>';
  }
  else
  {
    txt += '<div id="'+rec[name]+'" onmouseup="hidebar()" style="position:relative; visibility:hidden; background-color:'+bordercolor+'; width:'+barwidth+'px; height:'+barheight+'px;">';
    txt += '<div style="position:absolute; top:1px; left:1px; width:'+(barwidth-2)+'px; height:'+(barheight-2)+'px; background-color:'+unloadedcolor+'; z-index:100; font-size:1px;"></div>';
    txt += '<div id="'+ind[name]+'" style="position:absolute; top:1px; left:1px; width:0px; height:'+(barheight-2)+'px; background-color:'+loadedcolor+'; z-index:100; font-size:1px;"></div>';
    txt += '</div>';
  }
  
  document.write(txt);
}

////////////////////////////////////////////////////////////////////////
//
// Initialize a progress bar and make it visible.
//
////////////////////////////////////////////////////////////////////////

function initProgressBar(name)
{
  selectProgressBar(name);

  clipid(ctrlInd, 0, 0, barheight - 2, 0);
  loaded[name] = 0;

  if (ns4)
    ctrlRec.visibility = "show";
  else
    ctrlRec.style.visibility = "visible";
}

////////////////////////////////////////////////////////////////////////
//
// Select the named progress bar and make it active.
//
////////////////////////////////////////////////////////////////////////

function selectProgressBar(name)
{
  ctrlRec = (ns4)?findlayer(rec[name],document):(ie4)?document.all[rec[name]]:document.getElementById(rec[name]);
  ctrlInd = (ns4)?ctrlRec.document.layers[ind[name]]:(ie4)?document.all[ind[name]]:document.getElementById(ind[name]);
  ctrlName = name;
}

// THIS FUNCTION BY MIKE HALL OF BRAINJAR.COM
function findlayer(name, doc)
{
  var i, layer;
  
  for (i = 0; i < doc.layers.length; i++)
  {
    layer = doc.layers[i];
    if (layer.name == name)
      return layer;

    if (layer.document.layers.length > 0)
      if ((layer = findlayer(name, layer.document)) != null)
        return layer;
  }

  return null;
}

////////////////////////////////////////////////////////////////////////
//
// Set the count (percent) of the currently active progress bar.
//
////////////////////////////////////////////////////////////////////////

function incrCount(prcnt)
{
  setCount(loaded[ctrlName] + prcnt);
}

function decrCount(prcnt)
{
  setCount(loaded[ctrlName] - prcnt);
}

function setCount(prcnt)
{
  var n = prcnt;
  
  if (n < 0)
    n = 0;
  
  if (n >= 100)
  {
    n = 100;
    setTimeout('hidebar()', 400);
  }

  clipid(ctrlInd, 0, n * blocksize, barheight - 2, 0);
  loaded[ctrlName] = n;
}

function clipid(id, t, r, b, l)
{
  if (ns4)
  {
    id.clip.left = l;
    id.clip.top = t;
    id.clip.right = r;
    id.clip.bottom = b;
  }
  else
    id.style.width = r;
}

////////////////////////////////////////////////////////////////////////
//
// Misc. functions
//
////////////////////////////////////////////////////////////////////////

function hidebar()
{
  action();
  //(ns4)? ctrlRec.visibility = "hide" : ctrlRec.style.visibility = "hidden";
}

window.onresize = function()
{
  if (ns4)
    setTimeout('history.go(0)', 400);
}

