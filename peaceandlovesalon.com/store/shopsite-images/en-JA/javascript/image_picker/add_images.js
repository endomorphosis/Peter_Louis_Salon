function AddImages(root, dirListName, imageListName, imageArray, imagePath, imageName)
{
  var dirlist, imagelist;
  var dir, path, image;
  var select;
  var i, j, n;

  // find the directory and image list objects
  with (document.forms[0])
  {
    n = elements.length;
    for (i = 0, j = 0; i < n && j < 2; i++)
    {
      if (elements[i].name == dirListName)
      {
        dirlist = elements[i];
        j++;
      }
      else
      if (elements[i].name == imageListName)
      {
        imagelist = elements[i];
        j++;
      }
    }
  }

  if (root == 'http')
  {
    // get the image media path and file name from the URL
    i = imageName.indexOf('/media/');
    if (i > 0)
    {
      imageName = imageName.substr(i + 1);
      i = imageName.lastIndexOf('/');
      if (i > 0)
      {
        imagePath = imageName.substr(0, i);
        imageName = imageName.substr(i + 1);

        if (win32)
        {
          nameParts = imagePath.split('/');
          imagePath = nameParts[0];
          for (i = 1; i < nameParts.length; i++)
            imagePath += slash + nameParts[i];
        }
      }
    }
  }

  // populate the image directory list
  n = imageArray.length;
  for (i = 0, j = 0; i < n; i++)
  {
    dir = imageArray[i][0];
    dirlist.options[i] = new Option(dir, dir, false, false);

    if (dir == imagePath)
    {
      dirlist.options[i].selected = true;
      j = i;  // remember the selected directory
    }
  }

  // populate the image list for the selected image directory
  n = imageArray[j].length;
  dir = imageArray[j][0];

  for (i = 1; i < n; i++)
  {
    image = imageArray[j][i];

    path = i > 1? dir + slash + image : image;
    if (!button_images)
    {
      if (path.slice(0, root.length) == root)
        path = path.slice(root.length+1);
    }

    imagelist.options[i-1] = new Option(image, path, false, false);

    if (image == imageName)
      imagelist.options[i-1].selected = true;
  }
}
