function MakeURL(imageName)
{
  var nameParts, imageURL;
  var mediaDir = 'media/';

  if (imageName == 'none')
    return '';

  if (win32)
  {
    nameParts = imageName.split("\\");
    imageName = nameParts[0];
    for (i = 1; i < nameParts.length; i++)
      imageName += '/' + nameParts[i];
  }

  if (imageName.substr(0, mediaDir.length) != mediaDir)
    imageName = mediaDir + imageName;

  imageURL = output_url + '/' + imageName;

  return imageURL;
}
