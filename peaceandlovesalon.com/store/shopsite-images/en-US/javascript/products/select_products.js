function move_selected_products(from_list, to_list)
{
  var from, to;
  var i, j, n;
  
  from = from_list.options;
  to = to_list.options;

  n = from.length;
  j = to.length;

  for (i = 0; i < n; i++)
  {
    if (from[i].selected == true)
    {
      to[j++] = new Option(from[i].text, from[i].value, false, false);
    }
  }

  for (i = n - 1; i >= 0; i--)
  {
    if (from[i].selected == true)
      from[i] = null;
  }

  return false;
}

function move_selected_products_up(list)
{
  var from;
  var i, n;
  
  from = list.options;

  n = from.length;

  for (i = 0; i < n; i++)
  {
    if (from[i].selected == true)
    {
      if (i != 0)
      {
        // need to see if any non-selected option slot is available
        for (j = i - 1; j > -1; j--)
        {
          if (from[j].selected == false)
            break;
        }
        
        if (j != -1)
        {
          var new_entry = new Option(from[i].text, from[i].value, false, true);
          from[i] = null;
          from.add(new_entry, i - 1);
        }
      }
    }
  }
  
  return false;
}

function move_selected_products_down(list)
{
  var from;
  var i, n;
  
  from = list.options;

  n = from.length;

  for (i = n-1; i > -1; i--)
  {
    if (from[i].selected == true)
    {
      if (i != n - 1)
      {
        // need to see if any non-selected option slot is available
        for (j = i + 1; j < n; j++)
        {
          if (from[j].selected == false)
            break;
        }
        
        if (j != n)
        {
          var new_entry = new Option(from[i].text, from[i].value, false, true);
          from[i] = null;
          from.add(new_entry, i + 1);
        }
      }
    }
  }
  
  return false;
}

function return_products_assigned(rnum, assigned, unassigned, okbutton)
{
  var assigned_products_val = "|";
  var unassigned_products_val = "|";
  var products, i, n, cnt;

  products = assigned.options;
  cnt = products.length;
  for (i = 0; i < cnt; i++)
  {
    assigned_products_val += products[i].value + '|';
  }

  products = unassigned.options;
  n = products.length;
  for (i = 0; i < n; i++)
  {
    unassigned_products_val += products[i].value + '|';
  }

  if (okbutton == 1)
  {
    with (window.opener.document.forms[0])
    {
      n = elements.length;
      for (i = 0; i < n; i++)
      {
        if (elements[i].name == assigned_fname)
          elements[i].value = assigned_products_val;
        else
         if (elements[i].name == unassigned_fname)
           elements[i].value = unassigned_products_val;
        else
         if (elements[i].name == assigned_count_fname)
           elements[i].value = cnt;
      }
    }

    return true;
  }
  else
  {
    with (window.document.forms[0])
    {
      assigned_products_t1.value = assigned_products_val;
      unassigned_products_t1.value = unassigned_products_val;
    }
    
    return true;
  }
}

function assign_cross_sell(rnum, global_flag,assigned_products, unassigned_products, assigned_name, product_type)
{
  var url = backoffice_url + '/selectproduct.cgi?rnum=' + rnum;

  url += '&global_flag=' + global_flag;
  url += '&product_type=' + product_type.value;
  url += '&assigned_name=' + assigned_name.value;
  url += '&assigned_products=' + assigned_products.value;
  url += '&unassigned_products=' + unassigned_products.value;

  window.open(url,'products','width=560,height=710,resizable=yes,scrollbars=yes,status=yes');
  return false;
}
