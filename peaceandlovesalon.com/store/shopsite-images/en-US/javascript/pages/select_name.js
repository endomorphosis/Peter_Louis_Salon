function select_name()
{
  var i, n;

  with (document.forms[0])
  {
    n = fields.length;
    for (i = 0; i < n; i++)
    {
      if (fields.options[i].value == page_name_fieldnum)
      {
        if (fields.options[i].selected == false)
        {
          alert(page_name_required);
          fields.options[i].selected = true;
          return false;
        }
      }
    }
  }

  return true;
}
