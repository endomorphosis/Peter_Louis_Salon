function select_page_text_field(field_id)
{
  var divViewEdit, divButton, divExpand, divPageTextField, divTextArea;
  var editor_field, page_text_field, current_field_id;
  var oEditor = null;

  current_field_id = current_page_text_field;

  divViewEdit = document.getElementById('divViewEdit_' + current_field_id);
  divViewEdit.style.display = 'none';

  divViewEdit = document.getElementById('divViewEdit_' + field_id);
  divViewEdit.style.display = '';

  divButton = document.getElementById('divButton_' + current_field_id);
  divButton.checked = false;

  divButton = document.getElementById('divButton_' + field_id);
  divButton.checked = true;

  page_text_field = document.getElementById(current_field_id);
  editor_field = document.getElementById('PageTextFields');
  divTextArea = document.getElementById('divTextArea_PageTextFields');

  if (typeof(FCKeditorAPI) != 'undefined')
    oEditor = FCKeditorAPI.GetInstance('PageTextFields');

  if (divTextArea.style.display == 'none')
    editor_field.value = oEditor.GetXHTML();

  page_text_field.value = editor_field.value;
  page_text_field.style.backgroundColor = textarea_style.backgroundColor;
  page_text_field.style.borderStyle = textarea_style.borderStyle;
  page_text_field.style.borderWidth = textarea_style.borderWidth;
  page_text_field.readOnly = false;

  page_text_field = document.getElementById(field_id);
  editor_field.value = page_text_field.value;
  page_text_field.style.backgroundColor = '#EFEFEF';
  page_text_field.style.borderStyle = 'dashed';
  page_text_field.style.borderWidth = '1';
  page_text_field.readOnly = true;

  if (divTextArea.style.display == 'none')
  {
    if (oEditor != null)
    {
      oEditor.SetData(editor_field.value);
      oEditor.Focus();
    }
  }
  else
    editor_field.focus();

  current_page_text_field = field_id;
}

function update_page_text_field()
{
  var page_text_field = document.getElementById(current_page_text_field);
  var divTextArea = document.getElementById('divTextArea_PageTextFields');
  var editor_field = document.getElementById('PageTextFields');
  var oEditor = null;

  if (typeof(FCKeditorAPI) != 'undefined')
    oEditor = FCKeditorAPI.GetInstance('PageTextFields');

  if (divTextArea.style.display == 'none')
    editor_field.value = oEditor.GetXHTML();

  page_text_field.value = editor_field.value;
}

function expand_page_text_fields(expand)
{
  var divExpandPageTextFields = document.getElementById('divExpandPageTextFields');
  var divCollapsePageTextFields = document.getElementById('divCollapsePageTextFields');
  var divPageTextField, field_id, current_field_id, mode, i;

  update_page_text_field();
  current_field_id = current_page_text_field;

  for (i = 0; i < page_text_fields.length; i++)
  {
    field_id = 'divPageTextField_' + page_text_fields[i];
    if ((divPageTextField = document.getElementById(field_id)) != null)
    {
      divPageTextField.style.display = expand? '' : 'none';

      if (page_text_fields[i] == current_field_id)
        divPageTextField.readOnly = true;
      else
        divPageTextField.readOnly = false;
    }
  }

  divExpandPageTextFields.style.display = expand? 'none' : '';
  divCollapsePageTextFields.style.display = expand? '' : 'none';
}
