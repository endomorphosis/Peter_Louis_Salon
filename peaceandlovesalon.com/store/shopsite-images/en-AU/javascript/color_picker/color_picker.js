
// Initial code obtained from http://www.karakas-online.de/myServices/wheel.html
// Also see http://www.ficml.org/jemimap/style/color/wheel21.html
// Code has been modified and additional code added by ShopSite, Inc.

var addary = new Array();           
var clrary = new Array(360);
var mouseColor, selectedColor;
var mouseOverWheel;
var layobj;

addary[0] = new Array(0,1,0);   // red
addary[1] = new Array(-1,0,0);  // red green
addary[2] = new Array(0,0,1);   // green
addary[3] = new Array(0,-1,0);  // green blue
addary[4] = new Array(1,0,0);   // blue
addary[5] = new Array(0,0,-1);  // red blue
addary[6] = new Array(255,1,1); // red

for (i = 0; i < 6; i++)
{
  for (j = 0; j < 60; j++)
  {
    clrary[60 * i + j] = new Array(3);
    for (k = 0; k < 3; k++)
    {
      clrary[60 * i + j][k] = addary[6][k];
      addary[6][k] += (addary[i][k] * 4);
    }
  }
}

function capture()
{
  if (document.layers)
  {
    layobj = document.layers["wheel"];
    layobj.document.captureEvents(Event.MOUSEMOVE);
    layobj.document.onmousemove = mouseMoved;
  }
  
  else
  if (document.all)
  {
    layobj = document.all["wheel"];
    layobj.onmousemove = mouseMoved;
  }
  
  else
  if (document.getElementById)
  {
    layobj = document.getElementById("wheel");
    layobj.onmousemove = mouseMoved;
  }
  
  mouseColor = window.document.getElementById("mouseColor");
  selectedColor = window.document.getElementById("selectedColor");
}

function mouseMoved(e)
{
  if (!mouseOverWheel)
    return false;
  
  if (document.layers)
  {
    x = 4 * e.layerX;
    y = 4 * e.layerY;
  }
  else
  if (document.all)
  {
    x = 4 * event.offsetX;
    y = 4 * event.offsetY;
  }
  else
  if (document.getElementById)
  {
    x = 4 * (e.pageX - document.getElementById("wheel").offsetLeft);
    y = 4 * (e.pageY - document.getElementById("wheel").offsetTop);
  }
 
  sx = x - 512;
  sy = y - 512;
  qx = (sx < 0)?0:1;
  qy = (sy < 0)?0:1;
  q = 2 * qy + qx;
  quad = new Array(-180,360,180,0);
  xa = Math.abs(sx);
  ya = Math.abs(sy);
  d = ya * 45 / xa;
  if (ya > xa)
    d = 90 - (xa * 45 / ya);
 
  deg = Math.floor(Math.abs(quad[q] - d));
  n = 0;
  sx = Math.abs(x - 512);
  sy = Math.abs(y - 512);
  r = Math.sqrt((sx * sx) + (sy * sy));
  if (x == 512 & y == 512)
    c = "000000";
  else
  {
    for (i = 0; i < 3; i++)
    {
      r2 = clrary[deg][i] * r / 256;
      
      if (r > 256)
        r2 += Math.floor(r - 256);
      
      if (r2 > 255)
        r2 = 255;
      
      n = 256 * n + Math.floor(r2);
    }

    c = n.toString(16);
    
    while (c.length < 6)
      c = "0" + c;
    
    c = "#" + c.toUpperCase();
  }
  
  if (document.layers)
  {
    mouseColor.bgColor = c;
    layobj.document.colorForm.mouseColor.value = c;
  }
  else
  if (document.all)
  {
    mouseColor.style.backgroundColor = c;
    layobj.document.colorForm.mouseColor.value = c;
  }
  else
  if (document.getElementById)
  {
    mouseColor.style.backgroundColor = c;
    document.colorForm.mouseColor.value = c;
  }

  return false;
}

function select_color(color)
{
  if (document.layers)
  {
    if (color.length == 0)
      color = layobj.document.colorForm.mouseColor.value;
    layobj.document.colorForm.selectedColor.value = color;
    selectedColor.bgColor = color;
  }
  else
  if (document.all)
  {
    if (color.length == 0)
      color = layobj.document.colorForm.mouseColor.value;
    layobj.document.colorForm.selectedColor.value = color;
    selectedColor.style.backgroundColor = color;
  }
  else
  if (document.getElementById)
  {
    if (color.length == 0)
      color = document.colorForm.mouseColor.value;
    document.colorForm.selectedColor.value = color;
    selectedColor.style.backgroundColor = color;
  }
}

function apply_color(colorName)
{
  var color, inputobj, showcolor;

  with (window.opener)
  {
    inputobj = document.getElementById(colorName);
    showcolor = document.getElementById('show_' + colorName);
  }

  if (document.layers)
  {
    color = layobj.document.colorForm.selectedColor.value;
    showcolor.bgColor = color;
    inputobj.value = color;
  }
  else
  if (document.all)
  {
    color = layobj.document.colorForm.selectedColor.value;
    showcolor.style.backgroundColor = color;
    inputobj.value = color;
  }
  else
  if (document.getElementById)
  {
    color = document.colorForm.selectedColor.value;
    showcolor.style.backgroundColor = color;
    inputobj.value = color;
  }
}

function set_color(colorName)
{
  var inputobj = window.opener.document.getElementById(colorName);
  var color = inputobj.value;
  select_color(color);
}

