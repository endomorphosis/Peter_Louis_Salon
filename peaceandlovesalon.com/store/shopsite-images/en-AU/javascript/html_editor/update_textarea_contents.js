function update_textarea_contents(recs)
{
  var recnums = null;
  var i;

  if (typeof(recs) != 'undefined')
    recnums = recs.split(',');

  if (typeof(page_text_fields) != 'undefined')
  {
    if (recnums != null)
    {
      for (i = 0; i < recnums.length; i++)
        update_page_text_field(recnums[i]);
    }
    else
      update_page_text_field();
  }

  if (typeof(extra_fields) != 'undefined')
  {
    if (recnums != null)
    {
      for (i = 0; i < recnums.length; i++)
        update_extra_field(recnums[i]);
    }
    else
      update_extra_field();
  }
}
