function check_name()
{
  var regexp = '[-]?[0-9]+:' + page_name_fieldnum + '$';
  var page_name;
  var i, n;

  with (document.forms[0])
  {
    n = elements.length;
    for (i = 0; i < n; i++)
    {
      if (elements[i].name.search(regexp) == 0)
      {
        page_name = elements[i];
        if (page_name.value.length == 0)
        {
          alert(page_name_required);
          page_name.focus();
          return false;
        }
      }
    }
  }

  return true;
}
