function apply_changes(editor_id)
{
  divFlashDone = document.getElementById('flash_done');
  var option, element, value, i;

  editorConfig = '';

  for (i = 0; i < config_options.length; i++)
  {
    option = config_options[i];
    element = document.getElementById(option.config_name);

    if (option.html_type == 'checkbox')
      value = element.checked? 'true' : 'false';
    else
      value = element.value;

    if (i > 0)
      editorConfig += '&';

    editorConfig += option.editor_name + '=' + value;
  }

  editorConfig += '&CustomConfigurationsPath=';

  if (typeof(FCKeditorAPI) == 'undefined')
  {
    divFlashDone.style.display = '';
    window.setTimeout('flash_off()', 1500);
  }
  else
    change_toolbar(editor_id);

  return false;
}

function enter_key_mode(form)
{
  var enter_mode = form.enter_mode;
  var shift_mode = form.shift_enter_mode;

  if (enter_mode.value == 'br')
  {
    shift_mode.value = 'br';
    shift_mode.disabled = true;
  }
  else
    shift_mode.disabled = false;
}

function manage_toolbars(operation)
{
  var toolbars = document.getElementById('toolbars');
  var size, features, url, i;
  var toolbar_name;

  if ((i = toolbars.selectedIndex) >= 0)
    toolbar_name = toolbars.options[i].value;
  else
    return false;

  switch (operation)
  {
    case toolbar_edit:
      operation = 'edit_toolbar=';
      size = 'width=680,height=450';
      break;

    case toolbar_copy:
      operation = 'copy_toolbar=';
      size = 'width=430,height=250';
      break;

    case toolbar_add:
      operation = 'add_toolbar=';
      size = 'width=430,height=210';
      break;

    case toolbar_rename:
      operation = 'rename_toolbar=';
      size = 'width=430,height=250';
      if (toolbar_name == default_toolbar)
      {
        alert(default_toolbar_msg);
        return false;
      }
      break;

    case toolbar_delete:
      operation = 'delete_toolbar=';
      size = 'width=430,height=250';
      if (toolbar_name == default_toolbar)
      {
        alert(default_toolbar_msg);
        return false;
      }
      break;

    default:
      return false;
  }

  url = backoffice_url + '/html_editor.cgi?' + operation + toolbar_name + 
    '&editor_id=' + editor_id;

  features = size + ',left=0,top=0,screenX=0,screenY=0,' + 
    'resizable=yes,scrollbars=yes';

  window.open(url, '', features);

  return false;
}

function validate_name(new_name)
{
  var dest = new_name.value.toLowerCase();
  var valid = 'abcdefghijklmnopqrstuvwxyz0123456789_- ';
  var name, i;

  if (new_name.value.length == 0)
  {
    alert(validate_msg1);
    new_name.focus();
    return false;
  }

  for (i = 0; i < dest.length; i++)
  {
    if (valid.indexOf(dest.charAt(i)) == -1)
    {
      alert(validate_msg2);
      new_name.focus();
      return false;
    }
  }

  for (i = 0; i < toolbar_names.length; i++)
  {
    name = toolbar_names[i].toLowerCase();
    if (name == dest)
    {
      if (toolbar_operation == toolbar_copy)
      {
        return confirm(validate_msg3 + '\n' + validate_msg4);
      }
      else
      {
        alert(validate_msg3 + '\n' + validate_msg5);
        new_name.focus();
        return false;
      }
    }
  }

  return true;
}

function flash_off()
{
  document.getElementById('flash_done').style.display = 'none';
}
