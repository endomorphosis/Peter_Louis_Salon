function validate()
{
  var regexp = '[-]?[0-9]+:' + product_name_fieldnum + '$';
  var product_name;
  var i, n;

  with (document.forms[0])
  {
    n = elements.length;
    for (i = 0; i < n; i++)
    {
      if (elements[i].name.search(regexp) == 0)
      {
        product_name = elements[i];
        if (product_name.value.length == 0)
        {
          alert(product_name_required);
          product_name.focus();
          return false;
        }
      }
    }
  }

  return true;
}
