function move_selected_pages(from_list, to_list)
{
  var from, to;
  var i, j, n;
  
  from = from_list.options;
  to = to_list.options;

  n = from.length;
  j = to.length;

  for (i = 0; i < n; i++)
  {
    if (from[i].selected == true)
    {
      to[j++] = new Option(from[i].text, from[i].value, false, false);
    }
  }

  for (i = n - 1; i >= 0; i--)
  {
    if (from[i].selected == true)
      from[i] = null;
  }

  return false;
}

function return_page_assignments(rnum, assigned, unassigned)
{
  var assigned_pages = "|";
  var unassigned_pages = "|";
  var assigned_fname = "assigned_pages_" + rnum;
  var unassigned_fname = "unassigned_pages_" + rnum;
  var pages, i, n;

  pages = assigned.options;
  n = pages.length;
  for (i = 0; i < n; i++)
  {
    assigned_pages += pages[i].value + '|';
  }

  if (n == 0)
    assigned_pages = "~";

  pages = unassigned.options;
  n = pages.length;
  for (i = 0; i < n; i++)
  {
    unassigned_pages += pages[i].value + '|';
  }

  if (n == 0)
    unassigned_pages = "~";

  with (window.opener.document.forms[0])
  {
    n = elements.length;
    for (i = 0; i < n; i++)
    {
      if (elements[i].name == assigned_fname)
        elements[i].value = assigned_pages;
      else
      if (elements[i].name == unassigned_fname)
        elements[i].value = unassigned_pages;
    }
  }

  window.close();
  return false;
}

function assign_to_pages(rnum, assigned_pages, unassigned_pages)
{
  var url = backoffice_url + '/products.cgi?assign_to_pages=1&rnum=' + rnum;
  url += '&assigned_pages=' + assigned_pages.value;
  url += '&unassigned_pages=' + unassigned_pages.value;
  window.open(url, 'products', 'width=560,height=560,resizable=yes,scrollbars=yes,status=yes');
  return false;
}
