function change_toolbar_row(new_toolbar_row)
{
  var divToolBar, divRowNum;
  var toolbar_row;

  divToolBar = document.getElementById('divToolbar_' + current_toolbar_row);
  divToolBar.style.display = 'none';

  divToolBar = document.getElementById('divToolbar_' + new_toolbar_row);
  divToolBar.style.display = '';

  divRowNum = document.getElementById('divRowNum_' + current_toolbar_row + 'b');
  divRowNum.style.display = 'none';

  divRowNum = document.getElementById('divRowNum_' + current_toolbar_row);
  divRowNum.style.display = '';

  divRowNum = document.getElementById('divRowNum_' + new_toolbar_row);
  divRowNum.style.display = 'none';

  divRowNum = document.getElementById('divRowNum_' + new_toolbar_row + 'b');
  divRowNum.style.display = '';

  toolbar_row = document.getElementById('toolbar_row_' + current_toolbar_row);
  toolbar_row.selectedIndex = -1;

  current_toolbar_row = new_toolbar_row;
}

function insert_toolbar_items()  // from editor buttons or from clipboard
{
  var toolbar_row = document.getElementById('toolbar_row_' + current_toolbar_row);
  var form = document.forms[0];
  var editor_buttons = form.editor_buttons;
  var insertPosition, buttons, items;
  var i, j, n;

  if ((i = editor_buttons.selectedIndex) >= 0)
  {
    // discard clipboard contents
    if (clipboard != null)
      clipboard.length = 0;

    buttons = editor_buttons.options;
    for (i = i + 1, n = 1; i < buttons.length; i++)
    {
      if (buttons[i].selected)
        n++;
    }
  }
  else
  if (clipboard != null && clipboard.length > 0)
  {
    buttons = clipboard;
    n = buttons.length;
  }
  else
    return;

  items = toolbar_row.options;
  for (i = 0, j = items.length; i < n; i++)
    items[j++] = new Option('', '', false, false);

  insertPosition = toolbar_row.selectedIndex;
  for (i = items.length - n - 1, j = items.length - 1; i > insertPosition; i--, j--)
  {
    items[j].text = items[i].text;
    items[j].value = items[i].value;
  }

  j = insertPosition + 1;
  
  if (clipboard != null && clipboard.length > 0)
  {
    for (i = 0; i < buttons.length; i++)
    {
      items[j].text = buttons[i].text;
      items[j++].value = buttons[i].value;
    }

    clipboard.length = 0;
  }
  else
  {
    for (i = editor_buttons.selectedIndex; i < buttons.length; i++)
    {
      if (buttons[i].selected)
      {
        items[j].text = buttons[i].text;
        items[j++].value = buttons[i].value;
      }
    }

    editor_buttons.selectedIndex = -1;
  }
}

function add_toolbar_items()  // or paste from clipboard
{
  var toolbar_row = document.getElementById('toolbar_row_' + current_toolbar_row);
  var form = document.forms[0];
  var editor_buttons = form.editor_buttons;
  var items, buttons;
  var i, j;

  if (editor_buttons.selectedIndex >= 0)
  {
    // discard clipboard contents
    if (clipboard != null)
      clipboard.length = 0;

    buttons = editor_buttons.options;
  }
  else
  if (clipboard != null && clipboard.length > 0)
  {
    buttons = clipboard;
  }
  else
  {
    alert(add_toolbar_msg);
    return;
  }

  items = toolbar_row.options;
  j = items.length;

  if (clipboard != null && clipboard.length > 0)
  {
    for (i = 0; i < buttons.length; i++)
      items[j++] = new Option(buttons[i].text, buttons[i].value, false, false);

    clipboard.length = 0;
  }
  else
  {
    for (i = editor_buttons.selectedIndex; i < buttons.length; i++)
    {
      if (buttons[i].selected)
        items[j++] = new Option(buttons[i].text, buttons[i].value, false, false);
    }

    editor_buttons.selectedIndex = -1;
  }
}

function remove_toolbar_items()  // to the clipboard
{
  var toolbar_row = document.getElementById('toolbar_row_' + current_toolbar_row);
  var items, i, j, n;

  items = toolbar_row.options;
  if (items.length == 0)
    return;

  if (toolbar_row.selectedIndex < 0)
  {
    alert(remove_toolbar_msg);
    return;
  }

  for (i = 0, n = 0; i < items.length; i++)
  {
    if (items[i].selected)
      n++;
  }

  if (clipboard == null)
    clipboard = new Array(n);
  else
    clipboard.length = n;

  for (i = 0, j = 0; i < items.length; i++)
  {
    if (items[i].selected)
      clipboard[j++] = new Option(items[i].text, items[i].value, false, false);
  }

  for (i = items.length - 1; i >= 0; i--)
  {
    if (items[i].selected)
      items[i] = null;
  }

  toolbar_row.selectedIndex = -1;
}

function delete_toolbar_items()
{
  var toolbar_row = document.getElementById('toolbar_row_' + current_toolbar_row);
  var items, i;

  items = toolbar_row.options;
  if (items.length == 0)
    return;

  if (toolbar_row.selectedIndex < 0)
  {
    alert(delete_toolbar_msg);
    return;
  }

  for (i = items.length - 1; i >= 0; i--)
  {
    if (items[i].selected)
      items[i] = null;
  }

  toolbar_row.selectedIndex = -1;
}

function move_toolbar_items(increment)
{
  var toolbar_row = document.getElementById('toolbar_row_' + current_toolbar_row);
  var items, text, value;
  var i, j;

  items = toolbar_row.options;
  if (items.length == 0)
    return;

  if (toolbar_row.selectedIndex < 0)
  {
    alert(move_toolbar_msg);
    return;
  }

  if (increment < 0)
  {
    // move selected items up
    for (i = items.selectedIndex; i < items.length; i++)
    {
      if (items[i].selected)
      {
        if ((j = i - 1) < 0)
          break;

        text = items[j].text;
        value = items[j].value;

        items[j].text = items[i].text;
        items[j].value = items[i].value;
        items[i].text = text;
        items[i].value = value;

        items[i].selected = false;
        items[j].selected = true;
      }
    }
  }
  else
  {
    // move selected items down
    for (i = items.length - 1; i >= 0; i--)
    {
      if (items[i].selected)
      {
        if ((j = i + 1) >= items.length)
          break;

        text = items[j].text;
        value = items[j].value;

        items[j].text = items[i].text;
        items[j].value = items[i].value;
        items[i].text = text;
        items[i].value = value;

        items[i].selected = false;
        items[j].selected = true;
      }
    }  
  }
}

function save_toolbar()
{
  var form = document.forms[0];
  var toolbar_name = form.toolbar_name;
  var toolbar_row, name, items, buttons;
  var i, j;

  if (toolbar_name.value.length == 0)
  {
    alert(save_toolbar_msg);
    toolbar_name.focus();
    return false;
  }

  for (i = 1; i <= 6; i++)
  {
    toolbar_row = document.getElementById('toolbar_row_' + i);
    items = toolbar_row.options;
    buttons = '';

    for (j = 0; j < items.length; j++)
    {
      name = items[j].value;
      if (name.length > 0)
      {
        if (buttons.length > 0)
          buttons += ',';

        buttons += items[j].value;
      }
    }

    document.getElementById('row' + i).value = buttons;
  }

  return true;
}

