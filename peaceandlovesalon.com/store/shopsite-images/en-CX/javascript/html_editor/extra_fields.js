function select_extra_field_by_name(field_names)
{
  var index = field_names.selectedIndex;
  var field_id = field_names.id.split(':');
  var recnum = field_id[0];
  var field_num;

  field_id = field_names[index].value.split(':');
  field_num = Number(field_id[0]);
  if (field_num > 0)
  {
    field_id = recnum + ':' + field_num;
    select_extra_field(field_id);
  }
}

function select_extra_field(field_id)
{
  var divViewEdit, divFldNum, divExtraField, divTextArea;
  var editor_field, extra_field, current_field_id;
  var field_nums = field_id.split(':');
  var recnum = field_nums[0];
  var field_num = field_nums[1];
  var field_names;
  var oEditor = null;
  var i;

  current_field_id = current_extra_field[recnum];

  divViewEdit = document.getElementById('divViewEdit_' + current_field_id);
  divViewEdit.style.display = 'none';

  divViewEdit = document.getElementById('divViewEdit_' + field_id);
  divViewEdit.style.display = '';

  divFldNum = document.getElementById('divFldNum_' + current_field_id + 'b');
  if (divFldNum != null)
  {
    divFldNum.style.display = 'none';

    divFldNum = document.getElementById('divFldNum_' + current_field_id);
    divFldNum.style.display = '';

    divFldNum = document.getElementById('divFldNum_' + field_id);
    divFldNum.style.display = 'none';

    divFldNum = document.getElementById('divFldNum_' + field_id + 'b');
    divFldNum.style.display = '';
  }

  extra_field = document.getElementById(current_field_id);
  editor_field = document.getElementById('ExtraFields_' + recnum);
  divTextArea = document.getElementById('divTextArea_ExtraFields_' + recnum);

  if (typeof(FCKeditorAPI) != 'undefined')
    oEditor = FCKeditorAPI.GetInstance('ExtraFields_' + recnum);

  if (divTextArea.style.display == 'none')
    editor_field.value = oEditor.GetXHTML();

  extra_field.value = editor_field.value;
  extra_field.style.backgroundColor = textarea_style.backgroundColor;
  extra_field.style.borderStyle = textarea_style.borderStyle;
  extra_field.style.borderWidth = textarea_style.borderWidth;
  extra_field.readOnly = false;

  extra_field = document.getElementById(field_id);
  editor_field.value = extra_field.value;
  extra_field.style.backgroundColor = '#EFEFEF';
  extra_field.style.borderStyle = 'dashed';
  extra_field.style.borderWidth = '1';
  extra_field.readOnly = true;

  if (divTextArea.style.display == 'none')
  {
    if (oEditor != null)
    {
      oEditor.SetData(editor_field.value);
      oEditor.Focus();
    }
  }
  else
    editor_field.focus();

  current_extra_field[recnum] = field_id;

  field_names = document.getElementById(recnum + ':extra_field_names');
  if (field_names != null)
  {
    for (i = 0; i < field_names.length; i++)
    {
      field_id = field_names.options[i].value.split(':');
      if (field_num == field_id[0])
        break;
    }

    if (i == field_names.length)
      i--;

    field_names.selectedIndex = i;
  }
}

function update_extra_field(recnum)
{
  var extra_field = document.getElementById(current_extra_field[recnum]);
  var divTextArea = document.getElementById('divTextArea_ExtraFields_' + recnum);
  var editor_field = document.getElementById('ExtraFields_' + recnum);
  var oEditor = null;

  if (typeof(FCKeditorAPI) != 'undefined')
    oEditor = FCKeditorAPI.GetInstance('ExtraFields_' + recnum);

  if (divTextArea.style.display == 'none')
    editor_field.value = oEditor.GetXHTML();

  extra_field.value = editor_field.value;
}

function expand_extra_fields(recnum, expand)
{
  var divExpandExtraFields = document.getElementById('divExpandExtraFields_' + recnum);
  var divCollapseExtraFields = document.getElementById('divCollapseExtraFields_' + recnum);
  var divExtraField, field_id, field_nums, current_field_num, mode, i;

  update_extra_field(recnum);

  field_id = current_extra_field[recnum];
  field_nums = field_id.split(':');
  current_field_num = field_nums[1];

  for (i = 0; i < extra_fields.length; i++)
  {
    field_id = 'divExtraField_' + recnum + ':' + extra_fields[i];
    if ((divExtraField = document.getElementById(field_id)) != null)
    {
      divExtraField.style.display = expand? '' : 'none';

      if (extra_fields[i] == current_field_num)
        divExtraField.readOnly = true;
      else
        divExtraField.readOnly = false;
    }
  }

  divExpandExtraFields.style.display = expand? 'none' : '';
  divCollapseExtraFields.style.display = expand? '' : 'none';
}
