function check_date(date)
{
  var mmddyy, mm, dd, yy;
  var current_date = new Date();
  var days = new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
  var valid = true;

  if (date.value.length == 0)
    return true;

  mmddyy = date.value.split('/');

  mm = Number(mmddyy[0]);
  dd = mmddyy.length > 1? Number(mmddyy[1]) : 1;
  yy = mmddyy.length > 2? Number(mmddyy[2]) : current_date.getFullYear();

  if (isNaN(mm) || isNaN(dd) || isNaN(yy))
    valid = false;

  if (valid)
  {
    if ((yy % 4 == 0 && yy % 100 != 0) || yy % 400 == 0)
      days[1] = 29;

    if (mm < 1 || mm > 12 || dd < 1 || dd > days[mm - 1] || yy < 1996 || yy > 2100)
      valid = false
  }

  if (valid)
  {
    date.value = mm + '/' + dd + '/' + yy;
    return true;
  }
  else
  {
    alert(check_date_msg);
    date.focus();
    return false;
  }
}
