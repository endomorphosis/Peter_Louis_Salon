function image_returned(imageFields)
{
  var field_ids;
  var p_image_id, m_image_id;
  var p_size_id, m_size_id;

  field_ids = imageFields.split('|');
  if (field_ids.length == 4)
  {
    p_image_id = field_ids[0];
    m_image_id = field_ids[1];
    p_size_id  = field_ids[2];
    m_size_id  = field_ids[3];
  }
  else
  {
    p_image_id = field_ids[0];
    p_size_id  = field_ids[1];
    m_image_id = '';
    m_size_id  = '';
  }

  if (p_image_id.indexOf(product_imageid) > 0)
  {
    CopyImagePath(p_image_id, m_image_id);
    CheckImageSize(p_image_id, p_size_id, m_size_id);
  }
  else
  if (p_image_id.indexOf(moreinfo_imageid) > 0)
    CheckImageSize(p_image_id, p_size_id, '');
}
