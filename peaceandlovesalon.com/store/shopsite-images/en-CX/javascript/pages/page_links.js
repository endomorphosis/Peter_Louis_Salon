function move_selected_pages(from_list, to_list)
{
  var from, to;
  var i, j, n;
  
  from = from_list.options;
  to = to_list.options;

  n = from.length;
  j = to.length;

  for (i = 0; i < n; i++)
  {
    if (from[i].selected == true)
    {
      to[j++] = new Option(from[i].text, from[i].value, false, false);
    }
  }

  for (i = n - 1; i >= 0; i--)
  {
    if (from[i].selected == true)
      from[i] = null;
  }

  return false;
}

function get_page_links()
{
  var pages, i, n;

  with (document.forms[0])
  {
    linked_pages.value = "|";
    pages = assigned_pages.options;
    n = pages.length;
    for (i = 0; i < n; i++)
    {
      linked_pages.value += pages[i].value + '|';
    }

    if (n == 0)
      linked_pages.value = "~";

    unlinked_pages.value = "|";
    pages = unassigned_pages.options;
    n = pages.length;
    for (i = 0; i < n; i++)
    {
      unlinked_pages.value += pages[i].value + '|';
    }

    if (n == 0)
      unlinked_pages.value = "~";
  }

  return true;
}

function return_page_links(rnum, linked, unlinked)
{
  var linked_pages = "|";
  var unlinked_pages = "|";
  var linked_fname = "linking_pages_" + rnum;
  var unlinked_fname = "unlinked_pages_" + rnum;
  var pages, i, n;

  pages = linked.options;
  n = pages.length;
  for (i = 0; i < n; i++)
  {
    linked_pages += pages[i].value + '|';
  }

  if (n == 0)
    linked_pages = "~";

  pages = unlinked.options;
  n = pages.length;
  for (i = 0; i < n; i++)
  {
    unlinked_pages += pages[i].value + '|';
  }

  if (n == 0)
    unlinked_pages = "~";

  with (window.opener.document.forms[0])
  {
    n = elements.length;
    for (i = 0; i < n; i++)
    {
      if (elements[i].name == linked_fname)
        elements[i].value = linked_pages;
      else
      if (elements[i].name == unlinked_fname)
        elements[i].value = unlinked_pages;
    }
  }

  window.close();
  return false;
}

function assign_page_links(rnum, linking_pages, unlinked_pages)
{
  var url = backoffice_url + '/pages.cgi?links_to_page=1&rnum=' + rnum;
  url += '&linking_pages=' + linking_pages.value;
  url += '&unlinked_pages=' + unlinked_pages.value;
  window.open(url, 'pages', 'width=560,height=560,resizable=yes,scrollbars=yes,status=yes');
  return false;
}
