			<link rel="stylesheet" href="http://www.hairdreamsofnewyork.com/jquery-ui/themes/base/jquery-ui-1.7.2.custom.css" type="text/css" media="all" />
			<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js" type="text/javascript"></script>
			<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/jquery-ui.min.js" type="text/javascript"></script>
			<script src="http://jquery-ui.googlecode.com/svn/tags/latest/external/bgiframe/jquery.bgiframe.min.js" type="text/javascript"></script>
			<script src="http://jquery-ui.googlecode.com/svn/tags/latest/ui/minified/i18n/jquery-ui-i18n.min.js" type="text/javascript"></script>

			<script src="/js/demos.js" type="text/javascript"></script>
			<script src="/themeroller/themeswitchertool/" type="text/javascript"></script>

 <style type="text/css">
		.toggler { width: 500px; height: 200px; }
		#button {  padding: .5em 1em; text-decoration: none;  z-index: 3;}
		.button { z-index: 3; font-size: 9px;}
		.button2 { z-index: 3; font-size: 9px;}
		.nullbutton { z-index: 3; font-size: 9px;}
		#effect { padding: 0.4em; position: relative; }
		#effect h3 { margin: 0; padding: 0.4em; text-align: center; }
		.ui-effects-transfer { border: 2px dotted gray; } 
		.ui-widget-content ul { position: relative; left: -15px;}
		.button_row3{padding-left: 2px; padding-right: 2px;}
		.button_row{vertical-align: top; margin: none; padding: none;}
		.ui-widget-content{ display: none; font-size: 9px; padding-left: 5px; padding-bottom: 10px; padding-right: -10px; position: relative; top: 5px; z-index: 10;}
                .ui-state-default{ font-size: 9px;}
		.jquery_menu{ font-size: 9px; font-weight:300; }
		.div3{display: none; }

                #share{display: none;}
		#cart{display: none;}
	</style>
	<script type="text/javascript">
	$(function() {

		//run the currently selected effect
		function runEffect(some_value){
			//get effect type from 
			var selectedEffect = 'blind';
			
			//most effect types need no options passed by default
			var options = {};
			//check if it's scale, transfer, or size - they need options explicitly set
			if(selectedEffect == 'scale'){  options = {percent: 100}; }
			else if(selectedEffect == 'transfer'){ options = { to: "#button", className: 'ui-effects-transfer' }; }
			else if(selectedEffect == 'size'){ options = { to: {width: 280,height: 185} }; }
			//run the effect
			$(some_value).show(selectedEffect,options,400);
		};

		function hideEffect(some_value){
			//get effect type from 
			var selectedEffect = 'blind';
			
			//most effect types need no options passed by default
			var options = {};
			//check if it's scale, transfer, or size - they need options explicitly set
			if(selectedEffect == 'scale'){  options = {percent: 100}; }
			else if(selectedEffect == 'transfer'){ options = { to: "#button", className: 'ui-effects-transfer' }; }
			else if(selectedEffect == 'size'){ options = { to: {width: 280,height: 185} }; }
			//run the effect
			$(some_value).hide(selectedEffect,options,400);
		};
		
		//callback function to bring a hidden box back
		
		//set effect from select menu value


		

	        $(".button_row3").bind("mouseenter",function(){
    		$(".div").hide();
		$(".div2").hide();
		//runEffect($(this).attr('name'));
		}).bind("mouseleave",function(){
      		});
		
		$("span.button").bind("mouseenter",function(){
		$(".ui-widget-content").hide();
		//runEffect($(this).attr('name'));
		}).bind("mouseleave",function(){
      		});

		$(".button2").mouseover(function() {
		$("div.div2").hide();
		//$($(this).attr('name')).show('slide', '', 400);
		return false;
		});

		$(".ui-corner-all").bind("mouseenter",function(){
		//$(this).addClass('ui-state-active');	
		 $(this).addClass('ui-state-focus'); 
			
		}).bind("mouseleave",function(){
      		 $(this).removeClass('ui-state-focus'); 
			$(this).removeClass('ui-state-active');
		});
		
		$(".ui-widget-content").hide();
		
		$("div.button").bind("mouseenter",function(){
		$(".ui-widget-content").hide();
	
		//runEffect($(this).attr('name'));
		}).bind("mouseleave",function(){
      		});

		$("div.div3").bind("mouseenter",function(){
		}).bind("mouseleave",function(){
		$(".ui-widget-content").hide();
      		});

		$("#banner2").bind("mouseenter",function(){
		}).bind("mouseleave",function(){
		$(".ui-widget-content").hide();
      		});

		

	
		$("#main").mouseover(function() {
		$("div.div3").hide();
		});

	});

		
	</script>


</script><br/><br/><div width="720" id="banner2" class="banner2" style="position: absolute;  width: 720px; text-align: center; top: -25px; margin-left: auto; margin-right: auto;">


<center><strong>
<table width="720" style=" text-align: center;  position: relative; top: 141px; font-size: 9px; font-weight: 700;" valign="top"  cellspacing="0">
<tr class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header">
<td class="button_row3" width="140" name="#thickening">

<div id="button" class="ui-state-default ui-corner-all" ><a href="http://www.hairdreamsofnewyork.com/template.php?f=hdny_thickening" class="ui-state-default ui-corner-all" class="button">Hair Thickening</a></div>
</div>
</td>


<td class="button_row3" width="140" name="#extensions">
<div id="button" class="ui-state-default ui-corner-all" ><a href="http://www.hairdreamsofnewyork.com/template.php?f=hdny_extensions" class="ui-state-default ui-corner-all" class="button">Extensions</a></div>



</td>

<td class="button_row3" width="140" name="#strands_effects">
<div id="button" class="ui-state-default ui-corner-all" ><a href="http://www.hairdreamsofnewyork.com/template.php?f=hdny_styling_examples"  class="ui-state-default ui-corner-all" class="button">Styling Examples</a></div>




</td>
<td class="button_row3" width="140" name="#strands">
<div id="button" class="ui-state-default ui-corner-all" ><a href="http://www.hairdreamsofnewyork.com/template.php?f=hdny_strands_effects"  class="ui-state-default ui-corner-all" class="button">Strands &amp; Effects</a></div>




</td>
<td class="button_row3" width="140" name="#shop">
<div id="button" class="ui-state-default ui-corner-all" ><a href="http://www.hairdreamsofnewyork.com/template.php?f=hairdreams"  class="ui-state-default ui-corner-all" class="button">SHOP</a></div>

</td>

</tr>
</table>
</strong>
<style>
.button_row2{
/* border: solid 1px #cccccc; */
}

.div2{
z-index: 8;
}
</style>
<table width="720" style="text-align: center; position: relative; top: 135px;" style=" vertical-align: top; font-size: 9px;" cellpadding="0" cellspacing="0">
<tr>
<td class="button_row2" width="140">
<div id="thickening" style=" z-index: 9; background: transparent; border: 0px; margin: 0px: padding: 0px; display: block;" class="div ui-widget-content"  margin="0" padding="0">

<div id="button" class="ui-state-default ui-corner-all" ><a href="http://www.hairdreamsofnewyork.com/template.php?f=hdny_problem" class="button2">Thin Hair</a></div>
<div id="button" class="ui-state-default ui-corner-all" ><a href="http://www.hairdreamsofnewyork.com/template.php?f=hdny_fine_hair" name="#thickeningB" class="button2">Fine Hair</a></div></a>
<div id="button" class="ui-state-default ui-corner-all" ><a href="http://www.hairdreamsofnewyork.com/template.php?f=hdny_thinning_hair" name="#thickeningC" class="button2">Hair Loss</a></div>
<div id="button" class="ui-state-default ui-corner-all" ><a href="http://www.hairdreamsofnewyork.com/template.php?f=hdny_style_examples" class="button2">Examples</a></div></div>

<div id="thickeningB" class="div2 ui-widget-content" style=" background: transparent; border: 0px; margin: 0px: padding: 0px; top: -71px; left: 139px;">
<div id="button" class="ui-state-default ui-corner-all" ><a href="http://www.hairdreamsofnewyork.com/template.php?f=hdny_extensions" class="nullbutton">Extensions</a></div>
<div id="button" class="ui-state-default ui-corner-all" ><a href="http://www.hairdreamsofnewyork.com/template.php?f=hdny_top_hair" class="nullbutton">Top Hair</a></div>
<div id="button" class="ui-state-default ui-corner-all" ><a href="http://www.hairdreamsofnewyork.com/template.php?f=hdny_high_lines" class="nullbutton">HighLines</a></div>
</div>

<div id="thickeningC" class="div2 ui-widget-content" style=" background: transparent; border: 0px; margin: 0px: padding: 0px; top: -49px; left: 139px;">
<div id="button" class="ui-state-default ui-corner-all" ><a href="http://www.hairdreamsofnewyork.com/template.php?f=hdny_microlines" class="nullbutton">Microlines</div>
<div id="button" class="ui-state-default ui-corner-all" ><a href="http://www.hairdreamsofnewyork.com/template.php?f=hdny_top_hair" class="nullbutton">Top Hair</div>
<div id="button" class="ui-state-default ui-corner-all" ><a href="http://www.hairdreamsofnewyork.com/template.php?f=hdny_high_lines" class="nullbutton">HighLines</div></div>
</td>
<td class="button_row2"  width="140">

<div id="extensions" style=" background: transparent; border: 0px; margin: 0px: padding: 0px; display: block;" class="div ui-widget-content" margin="0" padding="0">
<div id="button" class="ui-state-default ui-corner-all" ><a href="http://www.hairdreamsofnewyork.com/template.php?f=hdny_colors_waves_lengths" class="button2" name="#cwl">Colors Waves Lengths</a></div>
<div id="button" class="ui-state-default ui-corner-all" ><a href="http://www.hairdreamsofnewyork.com/template.php?f=hdny_qualities" class="button2" name="#qualities">Qualities</a></div>
<div id="button" class="ui-state-default ui-corner-all" ><a href="http://www.hairdreamsofnewyork.com/template.php?f=hdny_hair_extension_bondings" class="button2" name="#eb">Bondings</a></div>
<div id="button" class="ui-state-default ui-corner-all" ><a href="http://www.hairdreamsofnewyork.com/template.php?f=hdny_how_it_works" class="button2" name="#faq">How It Works</a></div></div>

<div id="qualities" class="div2 ui-widget-content " style=" background: transparent; border: 0px; margin: 0px: padding: 0px; display: block; top: -71px; left: 139px;">
<div id="button" class="ui-state-default ui-corner-all" ><a href="http://www.hairdreamsofnewyork.com/template.php?f=hdny_hair_quality_guarantee" class="nullbutton">Quality Guarantee</div>
<div id="button" class="ui-state-default ui-corner-all" ><a href="http://www.hairdreamsofnewyork.com/template.php?f=hdny_special_quality" class="nullbutton">Special Quality</div>
<div id="button" class="ui-state-default ui-corner-all" ><a href="http://www.hairdreamsofnewyork.com/template.php?f=hdny_basic_quality" class="nullbutton">Basic Quality</div>
<div id="button" class="ui-state-default ui-corner-all" ><a href="http://www.hairdreamsofnewyork.com/template.php?f=hdny_quality_proofed" class="nullbutton" >Quality Proof</div></div>


<div id="eb" class="div2 ui-widget-content" style=" background: transparent; border: 0px; margin: 0px: padding: 0px; display: block; top: -49px; left: 139px;">
<div id="button" class="ui-state-default ui-corner-all" ><a href="http://www.hairdreamsofnewyork.com/template.php?f=hdny_standard_bonding" class="nullbutton" >Standard Bonding</div>
<div id="button" class="ui-state-default ui-corner-all" ><a href="http://www.hairdreamsofnewyork.com/template.php?f=hdny_comfort_point_bonding" class="nullbutton" >Comfort Points</div>
</div>


<div id="faq" class="div2 ui-widget-content" style=" background: transparent; border: 0px; margin: 0px: padding: 0px; display: block; top: -28px; left: 139px;">
<div id="button" class="ui-state-default ui-corner-all" ><a href="http://www.hairdreamsofnewyork.com/template.php?f=hdny_selector" class="nullbutton" >Selector</div>
<div id="button" class="ui-state-default ui-corner-all" ><a href="http://www.hairdreamsofnewyork.com/template.php?f=hdny_laserbeamer" class="nullbutton" >Laserbeamer</div>
</div>

<div id="cwl" class="div2 ui-widget-content" style=" background: transparent; border: 0px; margin: 0px: padding: 0px; display: block; top: -93px; left: 139px;">
<div id="button" class="ui-state-default ui-corner-all" ><a href="http://www.hairdreamsofnewyork.com/template.php?f=hdny_colors"  class="nullbutton" >Colors</div>
<div id="button" class="ui-state-default ui-corner-all" ><a href="http://www.hairdreamsofnewyork.com/template.php?f=hdny_waves" class="nullbutton"  >Waves</div>
<div id="button" class="ui-state-default ui-corner-all" ><a href="http://www.hairdreamsofnewyork.com/template.php?f=hdny_lengths" class="nullbutton" >Lengths</div>
</div>
</td>
<td class="button_row2"  width="140">


<div id="strands_effects" style=" z-index: 9; background: transparent; border: 0px; margin: 0px: padding: 0px; display: block;" class="div ui-widget-content"  margin="0" padding="0">
<div id="button" class="ui-state-default ui-corner-all" ><a href="http://www.hairdreamsofnewyork.com/template.php?f=hdny_style_examples" class="button2">Hair Thickening</a></div>
<div id="button" class="ui-state-default ui-corner-all" ><a href="http://www.hairdreamsofnewyork.com/template.php?f=hdny_extention" class="button2">Hair Extension</a></div>
<div id="button" class="ui-state-default ui-corner-all" ><a href="http://www.hairdreamsofnewyork.com/template.php?f=hdny_trendy_colorstrands" class="button2">Trendy Color Strands</a></div>
<div id="button" class="ui-state-default ui-corner-all" ><a href="http://www.hairdreamsofnewyork.com/template.php?f=hdny_extravagant_effects" class="button2">Extravagant Effects</a></div>
<div id="button" class="ui-state-default ui-corner-all" ><a href="http://www.hairdreamsofnewyork.com/template.php?f=hdny_trends_2007" class="button2" name="#2007">Trends 2007</a></div>
<div id="button" class="ui-state-default ui-corner-all" ><a href="http://www.hairdreamsofnewyork.com/template.php?f=hdny_trends_2009" class="button2" name="#2009">Trends 2009</a></div>
</div>


<div id="2007" class="div2 ui-widget-content" style=" background: transparent; border: 0px; margin: 0px: padding: 0px; display: block; top: -49px; left: 139px;"">
<div id="button" class="ui-state-default ui-corner-all" ><a href="http://www.hairdreamsofnewyork.com/template.php?f=hdny_sixties" class="nullbutton" >Sixties</div>
<div id="button" class="ui-state-default ui-corner-all" ><a href="http://www.hairdreamsofnewyork.com/template.php?f=hdny_black_white" class="nullbutton" >Black &amp; White</div>
<div id="button" class="ui-state-default ui-corner-all" ><a href="http://www.hairdreamsofnewyork.com/template.php?f=hdny_romantic" class="nullbutton" >Romantic</div>
</div>


<div id="2009" class="div2 ui-widget-content" style=" background: transparent; border: 0px; margin: 0px: padding: 0px; display: block; top: -27px; left: 139px;">
<div id="button" class="ui-state-default ui-corner-all" ><a href="http://www.hairdreamsofnewyork.com/template.php?f=hdny_best_nature" class="nullbutton">Best Nature</div>
<div id="button" class="ui-state-default ui-corner-all" ><a href="http://www.hairdreamsofnewyork.com/template.php?f=hdny_diva" class="nullbutton" >Diva</div>
<div id="button" class="ui-state-default ui-corner-all" ><a href="http://www.hairdreamsofnewyork.com/template.php?f=hdny_pop_art" class="nullbutton" >Pop Art</div>
<div id="button" class="ui-state-default ui-corner-all" ><a href="http://www.hairdreamsofnewyork.com/template.php?f=hdny_retro_rebell" class="nullbutton">Retro Rebell</div>
</div>
</td>

<td class="button_row2" width="140">

<div id="strands" style=" z-index: 9; background: transparent; border: 0px; margin: 0px: padding: 0px; display: block;" class="div ui-widget-content" margin="0" padding="0">

<div id="button" class="ui-state-default ui-corner-all" ><a href="http://www.hairdreamsofnewyork.com/template.php?f=hdny_trendy_colorstrands" class="nullbutton">Trendy Color Strands</a></div>
<div id="button" class="ui-state-default ui-corner-all" ><a href="http://www.hairdreamsofnewyork.com/template.php?f=hdny_extravagant_effects" class="nullbutton">Extravagant Effects</a></div>
<div id="button" class="ui-state-default ui-corner-all" ><a href="http://www.hairdreamsofnewyork.com/template.php?f=hdny_swarovski_jewels" class="nullbutton">Swarovski Jewels</a></div>
<div id="button" class="ui-state-default ui-corner-all" ><a href="http://www.hairdreamsofnewyork.com/template.php?f=hdny_leather_ribbons" class="nullbutton">Leather Ribbons</a></div>
</div>
</td>

<td class="button_row2" width="140">
</td>

</table>
</center>
</div>



<!-- End demo -->