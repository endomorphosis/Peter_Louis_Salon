<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="12"><img src="media/sitewide/trans_1x1.gif" width="10" height="1" /></td>
<td><img src="media/homepage/pl_147x47.gif" width="147" height="47" alt="Peter Louis" border="0" /> 
<p><font class="title"><b>PeterLouis.com Privacy Notice</b></font></p>
<p><font class="bodytext">Peterlouis.com knows that you care how information about you is used, and we appreciate your trust that we keep your information strictly confidential. This notice describes our privacy policy. By visiting Peterlouis.com, you are accepting the practices described in this Privacy Notice.</font></p> 
<p><font class="bodytext"><b>What Personal Information About Customers Does Peterlouis.com Gather?</b><br />
The information we learn from customers helps us personalize and continually improve your shopping experience at Peterlouis.com. Here are the types of information we gather.</font></p>
<p><font class="bodytext">&#149; Information You Give Us: We receive and store any information you enter on our Web site or give us in any other way. We use the information that you provide for such purposes as responding to your requests, customizing future shopping for you, improving our stores, and communicating with you.</font></p> 
<p><font class="bodytext">&#149; Automatic Information: We receive and store certain types of information whenever you interact with us. For example, like many Web sites, we use "cookies," and we obtain certain types of information when your Web browser accesses Peterlouis.com</font></p> 
<p><font class="bodytext">&#149; E-mail Communications: To help us make e-mails more useful and interesting, we often receive a confirmation when you open e-mail from Amazon.com if your computer supports such capabilities.</font></p> 

<p><font class="bodytext"><b>What About Cookies?</b><br />
&#149; Cookies are alphanumeric identifiers that we transfer to your computer's hard drive through your Web browser to enable our systems to recognize your browser and storage of items in your Shopping Cart.</font></p> 
<p><font class="bodytext">The Help portion of the toolbar on most browsers will tell you how to prevent your browser from accepting new cookies, how to have the browser notify you when you receive a new cookie, or how to disable cookies altogether.</font></p>  
<p><font class="bodytext"><b>Does Peterlouis.com Share the Information It Receives?</b><br />
Information about our customers is an important part of our business, and we are not in the business of selling it to others.</font></p>    
<p><font class="bodytext"><b>How Secure Is Information About Me?</b><br />
&#149; We work to protect the security of your information during transmission by using Secure Sockets Layer (SSL) software, which encrypts information you input.</font></p>  
<p><font class="bodytext">&#149; We reveal only the last five digits of your credit card numbers when confirming an order. Of course, we transmit the entire credit card number to the appropriate credit card company during order processing.</font></p>
<p><font class="bodytext">&#149; It is important for you to protect against unauthorized access to your password and to your computer. Be sure to sign off when finished using a shared computer. <a href="template.php?f=terms">Click here</a> for more information on how to sign off.</font></p>
<p><font class="bodytext"><b>Children</b><br />
Peterlouis.com does not sell products for purchase by children. We sell children's products for purchase by adults. If you are under 18, you may use Peterlouis.com only with the involvement of a parent or guardian.</font></p> 
<p><font class="bodytext"><b><a href="template.php?f=terms">Conditions of Use</a>, Notices, and Revisions</b><br />
If you choose to visit Peterlouis.com, your visit and any dispute over privacy is subject to this Notice and our <a href="template.php?f=terms">Conditions of Use</a>, including limitations on damages, arbitration of disputes, and application of the law of the state of New York. If you have any concern about privacy at Peterlouis.com, please send us a thorough description to <a href="mailto:info@peterlouissalon.com">info@peterlouissalon.com</a>, and we will try to resolve it.</font></p> 
<p><font class="bodytext">Our business changes constantly, and our Privacy Notice and the <a href="template.php?f=terms">Conditions of Use</a> will change also. You should check our Web site frequently to see recent changes. Unless stated otherwise, our current Privacy Notice applies to all information that we have about you and your account. We stand behind the promises we make, however, and will never materially change our policies and practices to make them less protective of customer information collected in the past without the consent of affected customers.</font></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
</td>
</tr>
</table>