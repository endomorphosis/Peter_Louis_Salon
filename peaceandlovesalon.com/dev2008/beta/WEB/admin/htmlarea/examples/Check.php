<HTML>
<HEAD>
<TITLE>Simple, CMS Style, Website Interface Design with Tabs</TITLE>
<meta name="description" content="Simple CMS Interface Template Design with Tabs is offered as a free download for website developers and designers.">
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
<script type="text/javascript">
  _editor_lang = "en";
  _editor_url = "../";
</script>
<!-- load the main HTMLArea files -->
<script type="text/javascript" src="../htmlarea.js"></script>

<style type="text/css">
html, body {
  font-family: Verdana,sans-serif;
  background-color: #fea;
  color: #000;
}
a:link, a:visited { color: #00f; }
a:hover { color: #048; }
a:active { color: #f00; }

textarea { background-color: #fff; border: 1px solid 00f; }
</style>

<script type="text/javascript">
// load the plugin files
HTMLArea.loadPlugin("TableOperations");

var editor = null;
function initEditor() {
  // create an editor for the "ta" textbox
  editor = new HTMLArea("ta");

  // register the TableOperations plugin with our editor
  editor.registerPlugin(TableOperations);

  editor.generate();
  return false;
}

function insertHTML() {
  var html = prompt("Enter some HTML code here");
  if (html) {
    editor.insertHTML(html);
  }
}
function highlight() {
  editor.surroundHTML('<span style="background-color: yellow">', '</span>');
}
</script>

</head>

<body onload="initEditor()">
<form action="new.php" method="post" enctype="multipart/form-data" name="tew" target="_self">
<textarea id="ta" name="ta" style="width:100%" rows="24" cols="80">

</textarea>
<input type="submit" name="ok" value="  submit  " />
<input type="button" name="ins" value="  insert html  " onclick="return insertHTML();" />
<input type="button" name="hil" value="  highlight text  " onclick="return highlight();" />

<a href="javascript:mySubmit()">submit</a>

<script type="text/javascript">
function mySubmit() {
// document.edit.save.value = "yes";
document.edit.onsubmit(); // workaround browser bugs.
document.edit.submit();
};
</script>

</form>
</BODY>