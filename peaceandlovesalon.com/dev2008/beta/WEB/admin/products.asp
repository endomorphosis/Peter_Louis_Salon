<!-- #include file="openconnection.asp" -->
<!-- #include file="functions.asp" -->


<script language="JavaScript" type="text/JavaScript">
function check_valid(formName) {
if (formName.add_p_name.value=="")
		{
		alert ("Product Name is Required.");
		formName.add_p_name.focus();
		return false;
		}
if (formName.add_p_price.value=="")
		{
		alert ("Price is Required.");
		formName.add_p_price.focus();
		return false;
		}
else
		{
		if (isNaN(formName.add_p_price.value))
		{
			alert ("Valid Price is Required.");
			formName.add_p_price.focus();
			return false;
		}
		}
		
if (formName.add_p_qty.value=="")
		{
		alert ("Quantity is Required.");
		formName.add_p_qty.focus();
		return false;
		}
else
		{
		if (isNaN(formName.add_p_qty.value))
		{
			alert ("Valid Quantity is Required.");
			formName.add_p_qty.focus();
			return false;
		}
		}		
return true;
}
		
</script>

<%
if Session("user") = "" or Session("pass") = "" Then
	response.redirect "login.asp?pwd=required"
end if
%>

<%
if trim(request.querystring("del")) <> "" Then
	set rs_cat = server.CreateObject("adodb.recordset")
	sql = "delete from PLS_Products where P_ID = "&request.querystring("del")&" "		
	rs_cat.open sql, conn,2, 3
	'rs_cat.close	

end if

if trim(request.querystring("add_btn")) = "Add Product"	Then
	set rs_cat = server.CreateObject("adodb.recordset")
	sql = "select * from PLS_Products"		
	rs_cat.open sql, conn,2, 3
	
	rs_cat.AddNew
	rs_cat.fields("P_Name") = trim(request.querystring("add_p_name")) 
	rs_cat.fields("P_Description") = trim(request.querystring("add_p_description")) 	
	rs_cat.fields("P_Category") = trim(request.querystring("select_p_category")) 	
	rs_cat.fields("Price") = trim(request.querystring("add_p_price")) 	
	rs_cat.fields("Qty") = trim(request.querystring("add_p_qty")) 	
	rs_cat.fields("P_Image") = trim(request.querystring("img"))
	rs_cat.Update
	rs_cat.close
	
elseif trim(request.querystring("updateProduct_btn")) = "Update Product"	Then
	
	set rs_cat = server.CreateObject("adodb.recordset")
	sql = "select * from PLS_Products where P_ID = "&request.querystring("updateID")&" "		
	rs_cat.open sql, conn,2, 3
	
	rs_cat.fields("P_Name") = trim(request.querystring("add_p_name")) 
	rs_cat.fields("P_Description") = trim(request.querystring("add_p_description")) 	
	rs_cat.fields("P_Category") = trim(request.querystring("select_p_category")) 	
	rs_cat.fields("Price") = trim(request.querystring("add_p_price")) 	
	rs_cat.fields("Qty") = trim(request.querystring("add_p_qty")) 	
	rs_cat.fields("P_Image") = trim(request.querystring("img"))
	rs_cat.Update
	rs_cat.close
	
end if
%>

<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Peter Louis-AdminPanel</title>
<LINK href="../css/style.css" type=text/css rel=stylesheet >
<LINK href="../css/global.css" type=text/css rel=stylesheet>

</head>

<body>

<table border="0" width="780" id="table22" cellspacing="0" cellpadding="0">
	<tr>
		<td colspan="3" style="border-right-style: solid; border-right-width: 1px">
		<div align="left">
			<table border="0" width="780" id="table23">
				<tr>
					<td>
					<p align="left"><b><font face="Verdana">
					<img border="0" src="../images/logo.gif"></font></b></td>
				</tr>
			</table></div></td>
	</tr>	
	<tr>
		<td height="1" bgcolor="#000000" colspan=3></td>
	</tr>
	<tr>
		<td width="11%" align="left" valign="top" background="../images/leftnav_back.jpg">
		<div id="leftmenu" style="width: 160px; height: 153px" align="left">
			<p>
	<img border="0" src="../images/dash.gif" width="162" height="16" align="left"><br>
	<br>
	<span style="background-color: #4674D1; background-position: 0% 0%"><b>
	<a href="index.asp?option=CMSMain"><font color="#FFFFFF">CMS</font></a> </b></span>
	<br>
	<img border="0" src="../images/dash.gif" width="162" height="16" align="left"><span style="background-color: #4674D1; background-position: 0% 0%"><b><font color="#FFFFFF"><br>
	<br>
			Categories</font> </b></span>
			&nbsp; <a href="index.asp?option=addCategory">&nbsp;&nbsp; Add Category</a>
			<a href="index.asp?option=updateCategory">&nbsp;&nbsp; Update Category</a>
			<a href="index.asp?option=removeCategory">&nbsp;&nbsp; Remove Category</a><br>
	<img border="0" src="../images/dash.gif" width="162" height="16" align="left"><font color="#FFFFFF"><span style="background-color: #4674D1; background-position: 0% 0%"><b><br>
	<br>
	Products</b></span></font> <a href="products.asp?option=addProduct">&nbsp; Add Product</a>
	<a href="products.asp?option=allProducts">&nbsp; View All Products</a>
	<a href="products.asp?option=allProducts">&nbsp; Remove Products</a>
	<a href="products.asp?option=allProducts">&nbsp; Update Products</a><br>
	<img border="0" src="../images/dash.gif" width="162" height="16" align="left"><br>
	<br>
	<span style="background-color: #4674D1; background-position: 0% 0%">
			<b><font color="#FFFFFF">Customer</font> <font color="#FFFFFF">History</font></b></span>
	<a href="index.asp?pageName=customerHistory">&nbsp; View 
Customer History</a><br>
	<img border="0" src="../images/dash.gif" width="162" height="16" align="left"><br>
	<br>
	<font color="#FFFFFF">
			<span style="background-color: #4674D1; background-position: 0% 0%">
			<b>Marketing 
Module</b></span></font>
			<a href="index.asp?pageName=manageNewsletter">&nbsp; Manage 
Newsletter</a> <a href="index.asp?pageName=addNewsletter">&nbsp; Add Newsletter</a>
	<a href="index.asp?pageName=manageMailingList">&nbsp; Manage Mailing List</a><br>
	<img border="0" src="../images/dash.gif" width="162" height="16" align="left"><br>
	<br>
	<font color="#FFFFFF">
			<span style="background-color: #4674D1; background-position: 0% 0%">
			<b>Other 
Options</b></span></font> <a href="signout.asp">&nbsp; Sign Out</a> </p>
		</div>
		<p>&nbsp;</td>
		<td width="1%" align="left" valign="top">&nbsp;</td>
		<td width="80%" align="right" valign="top" style="border-right-style: solid; border-right-width: 1px">
								<table border="0" width="100%" id="table30" cellspacing="0" cellpadding="0" style="border-left-width:1px; border-right-width:1px; border-top-width:1px; border-bottom-width:0px" height="283">
									<tr>
										<td style="border-right-style: none; border-right-width: medium" valign="top">&nbsp;</td>
									</tr>
									<tr>
										<td style="border-right-style: none; border-right-width: medium" valign="top">
										<div align="left">
									
						<% if trim(request.querystring("option")) = "addProduct" Then%> 

											<div align="center">

											<table border="0" width="99%" id="table31" cellspacing="0">
												<form name="form1" action="products.asp" method="get" onsubmit="return check_valid(this);">
													<tr>
														<td colspan="4" height="19">
														<p align="center"><b>
<%	if trim(request.querystring("add_btn")) = "Add Product"	Then %>
														<font color="#FF0000">
														Product has been added</font>
														<%response.end%>
<% end if %>
							</b></td>
													</tr>
													<tr>
														<td colspan="4" height="41" bgcolor="#1B7DCC">
														<b><span >
														<font color="#FFFFFF">Add 
										Product</font> </span></b></td>
													</tr>
													<tr>
														<td width="3%" height="30" bgcolor="#8CC1EC">&nbsp;</td>
														<td width="26%" height="30" colspan="2" align="left" bgcolor="#8CC1EC">
														<span style="font-size: 8pt">Product 
																Name:</span></td>
														<td width="69%" height="30" align="left" bgcolor="#8CC1EC">
														<input name="add_p_name" type="text" class="normmiddletext" size="30" maxlength="40"></td>
													</tr>
													<tr>
														<td width="3%" height="30" bgcolor="#8CC1EC">&nbsp;</td>
														<td width="21%" height="30" align="left" bgcolor="#8CC1EC">
														<span style="font-size: 8pt">Price:</span></td>
														<td width="3%" height="30" align="left" bgcolor="#8CC1EC">
														<p align="right">$</td>
														<td width="69%" height="30" align="left" bgcolor="#8CC1EC">
														<input name="add_p_price" type="text" class="normmiddletext" size="18" maxlength="40"><span style="font-size: 8pt">
														</span>
														<span style="font-size: 7pt">(eg. 1500)</span></td>
													</tr>
													<tr>
														<td width="3%" height="30" bgcolor="#8CC1EC">&nbsp;</td>
														<td width="26%" height="30" colspan="2" align="left" bgcolor="#8CC1EC">
														<span style="font-size: 8pt">Quantity:</span></td>
														<td width="69%" height="30" align="left" bgcolor="#8CC1EC">
														<input name="add_p_qty" type="text" class="normmiddletext" size="18" maxlength="40">
														</td>
													</tr>
													<tr>
														<td width="3%" height="30" bgcolor="#8CC1EC">&nbsp;</td>
														<td width="26%" height="30" colspan="2" align="left" bgcolor="#8CC1EC">
														<span style="font-size: 8pt">Category:</span></td>
														<td width="69%" height="30" align="left" bgcolor="#8CC1EC">
														<span style="font-size: 9pt">
														<select name="select_p_category" class="normmiddletext" size="1">
									 <%
										set rs_cat = server.CreateObject("adodb.recordset")
										sql = "select * from PLS_Catagories"		
										rs_cat.open sql, conn
										do while not rs_cat.eof
									%>
									
														<option value=<%=rs_cat.Fields("C_ID")%>> <% =rs_cat.Fields("C_Name") %> 
														</option>
                                      
                                    <%
                                    	rs_cat.MoveNext
                                    	Loop
                                    	rs_cat.close
                                    %></select></span></td>
													</tr>
													<tr>
														<td height="30" bgcolor="#8CC1EC">&nbsp;</td>
														<td height="30" colspan="2" align="left" bgcolor="#8CC1EC">
														<span style="font-size: 8pt">Product 
																			Description:</span></td>
														<td width="69%" height="30" align="left" bgcolor="#8CC1EC">
														<!--webbot bot="Validation" b-value-required="TRUE" i-maximum-length="200" --><textarea name="add_p_description" cols="54" class="normmiddletext" rows="4"></textarea></td>
													</tr>
													<tr>
														<td height="30" bgcolor="#8CC1EC">&nbsp;</td>
														<td height="30" colspan="2" align="left" bgcolor="#8CC1EC">
														<span style="font-size: 8pt">Product 
										Image:</span></td>
														<td width="69%" height="30" align="left" bgcolor="#8CC1EC">
														<input readonly name="img" type="text" id="img" class=normmiddletext size="37"><input type="button" name="Submit1" value="Browse" onClick="JavaScript:doOpen('img');" class=normmiddletext></td>
													</tr>
<SCRIPT Language="JavaScript">
	function doOpen(mobj)
	{  
	   window.open ("uploadfile.asp?mObjName=" + mobj, "UploadWin", "width=400,height=300,scrollbar=no;");
	}
</SCRIPT>										
													<tr>
														<td height="30" bgcolor="#8CC1EC">&nbsp;</td>
														<td height="30" colspan="2" align="left" bgcolor="#8CC1EC">&nbsp;</td>
														<td width="69%" height="30" bgcolor="#8CC1EC">&nbsp;</td>
													</tr>
													<tr>
														<td height="30" bgcolor="#1B7DCC">&nbsp;</td>
														<td height="30" colspan="2" align="left" bgcolor="#1B7DCC">
														<input type=hidden name="option" value="<%=request.querystring("option")%>">
														<input onmousedown="chkAddVal()" type=submit value = "Add Product" class="normmiddletext" name="add_btn"></td>
								
								
				<SCRIPT language="JavaScript" type="text/JavaScript">
					function chkAddVal()
					{
						if(document.form1.add_p_name.value == "" || document.form1.add_p_price.value == "" || document.form1.add_p_qty.value == "" || document.form1.add_p_description.value == "")
							{
								alert("All form fields are required");
								
								//if(document.form1.add_p_img.value == "")
									//document.form1.add_p_img.focus();

								if(document.form1.add_p_description.value == "")
									document.form1.add_p_description.focus();									
									
								if(document.form1.add_p_qty.value == "")
									document.form1.add_p_qty.focus();										
									
								if(document.form1.add_p_price.value == "")
									document.form1.add_p_price.focus();										
									
								if(document.form1.add_p_name.value == "")
									document.form1.add_p_name.focus();		
									
							}
					}								
				</script>
								
														<td width="69%" height="30" bgcolor="#1B7DCC">&nbsp;</td>
													</tr>
													<tr>
														<td>&nbsp;</td>
														<td colspan="2">&nbsp;</td>
														<td width="69%">&nbsp;</td>
													</tr>
												</form>
											</table>

								
								
								
								
								
							</div>

								
								
								
								
								
							<% elseif trim(request.querystring("option")) = "allProducts" Then%> 
											<table border="0" width="632" id="table32" cellspacing="0" cellpadding="0">
												<form name="updateCatForm" action="index.asp" method="get">
													<input type=hidden name="option" value="<%=request.querystring("option")%>">
													<tr>
														<td height="30" valign="top">
														<font face="Arial">
														<span ><b>
														<font size="2">&nbsp;All 
														Products</font></b></span></font></td>
													</tr>
							<% if trim(request.querystring("del")) <> "" Then %>
	<tr><td height="30">
													<p align="center">
													<font color="#FF0000"><b>P</b></font><b><font color="#FF0000">roduct has been deleted</font></b>
													</td></tr>
							
													
													<% end if %>	
				<%	if trim(request.querystring("updateID")) <> ""	Then %>													
													<tr><td height="30">
													<p align="center">
													<font color="#FF0000"><b>P</b></font><b><font color="#FF0000">roduct has been updated</font></b>
													</td></tr>
						<% end if %>													
												
													
													
													
													<tr>
														<td valign="top">
														<table border="0" width="100%" id="table36" cellspacing="0" cellpadding="0">
															<tr>
          <TD width="2%" height="20" align="center" valign="top" bordercolorlight="#FFFFFF" bordercolordark="#FFFFFF">


&nbsp;</TD>
          <TD width="98%" height="20" align="center" valign="top" bordercolorlight="#FFFFFF" bordercolordark="#FFFFFF">

<font size="1">



															<table border="0" width="100%" id="table40" cellspacing="0" cellpadding="0" style="border-width: 0px">
																<tr>
																	<td align="center" style="border-style: none; border-width: medium; padding-left: 4px; padding-right: 4px" bordercolor="#8CC1EC" height="23" width="27%" valign="top">
																	<p align="left">

<font size="1">



														<span >Select a 
														category:<br>
														</span></font>
														<span style="font-size: 9pt">
														<select name="select_p_category0" class="Item10" size="1" onchange="if(!this.value==''){window.location='products.asp?option=allProducts&catId='+this.value;}">
														<option value="">Select Category</option>
									 <%
										set rs_cat = server.CreateObject("adodb.recordset")
										sql = "select * from PLS_Catagories"		
										rs_cat.open sql, conn
										do while not rs_cat.eof
									%>
									
														<option value="<% =rs_cat.Fields("C_ID") %>" <%if request.querystring("catId")=cStr(rs_cat.Fields("C_ID")) then%>selected<%end if%>> <% =rs_cat.Fields("C_Name") %> 
														</option>
                                      
                                    <%
                                    	rs_cat.MoveNext
                                    	Loop
                                    	rs_cat.close
                                    %></select></span></td>
																	<td align="center" style="border-style: none; border-width: medium; padding-left: 4px; padding-right: 4px" bordercolor="#8CC1EC" height="23" width="50%" valign="top">
																	<p align="left">

<font size="1">



														<span >Select a product to 
														Update or Delete:<br>
																	</span>
									 <%
										set rs_cat = server.CreateObject("adodb.recordset")
										sql = "select * from PLS_Products order by P_ID desc"		
											if request.queryString("catId")<>"" then											
												sql  = "select * from PLS_Products where P_Category="&request.queryString("catId")&" order by P_ID desc"											
											end if

										rs_cat.open sql, conn
										if rs_cat.eof then										
									%>
									No Product Listing!
									
									<%else%>
									<span style="font-size: 9pt"><select name="select_product" class="Item10" size="1" onchange="window.location='index.asp?PageName=moreinfo&Id='+this.value">
									<%									
										do while not rs_cat.eof
									%>
									
														<option value=<%=rs_cat.Fields("P_ID")%>> <% =rs_cat.Fields("P_Name") %> 
														</option>
                                      
                                    <%
                                    	rs_cat.MoveNext
                                    	Loop
                                    	end if
                                    	rs_cat.close                                    	
                                    %></select></span></td>
																	<td align="center" style="border-style: none; border-width: medium; padding-left: 4px; padding-right: 4px" bordercolor="#8CC1EC" bgcolor="#1B7DCC">
														<p align="right"><b>
														<a class="subMenu2" href="products.asp?option=addProduct">
														<font color="#FFFFFF">
														Add New Product</font></a></b></td>
																</tr>
		

											
															</table></TD>
        														</tr>
															<tr>
          <TD width="2%" height="20" align="center" valign="top" bordercolorlight="#FFFFFF" bordercolordark="#FFFFFF">


&nbsp;</TD>
          <TD width="98%" height="20" align="center" valign="top" bordercolorlight="#FFFFFF" bordercolordark="#FFFFFF">

<font size="1">



<%

	set rs_cart = server.CreateObject("adodb.recordset")
	'rs_cart.cursorlocation = 3
	
	Sql_cart = "select * from PLS_Products order by P_ID desc"
	if request.queryString("catId")<>"" then
		Sql_cart = "select * from PLS_Products where P_Category="&request.queryString("catId")&" order by P_ID desc"
	end if
	'Sql_cart = "select * from pp_Cart where Session_Code = '"&session("cart")&"' "
	rs_cart.open Sql_cart, conn

%>




	<%if not rs_cart.eof then%>

<table width="100%" cellpadding="4" cellspacing="0" border="0" id="table37" bordercolorlight="#FFFFFF" bordercolordark="#FFFFFF">

  <%
			  x=0
			  do while not rs_cart.eof
			  x=x+1
 
			  
			  %> 

<tr valign=top>
<td valign=top align=left bgcolor="#FEF3F8">
<%
if not rs_cart.eof then
	'rs_cart.moveNext
%>
<font class="title">&nbsp;<!--- rec 262 -->
&nbsp;<BR>
<a name="262"></a>
<!-- Default Product Template -->
<img  src="../productImages/<%=rs_cart("P_Image")%>" width="60" height="100" hspace="3" vspace="3" border="1" align=left>
<font color="#000080">

<span style="font-size: 9pt; text-decoration: font-weight:700; font-weight:700"><a name="<%=rs_cart("P_ID")%>"><%=rs_cart("P_Name")%></a></span></font><br>
<font face="Arial" style="font-size: 8pt">
<%'rs_cart("P_description")%></font><span style="font-size: 8pt"><font color="#808080"><%c=rs_cart("P_description")
dim c
c=Mid(c,1,100)
response.write(c)
%></font></span>
</font>



<font class="title" style="font-size: 7pt">
<a href="index.asp?PageName=moreinfo&Id=<%=rs_cart("P_id")%>">More Info</a></font><br>
&nbsp;<table border="0" id="table38" cellspacing="0" cellpadding="0" width="196">
	<tr>
		<td width="184">

<font size="1">



		<p align="left">
		<font  face="Arial" style="font-size: 8pt" color="#808080">
		<b>Price
		: $ <%=rs_cart("Price")%></b></font></td>
		<td>
		&nbsp;</td>
	</tr>
	<tr>

<font size="1">



		<td width="184">

<font style="font-size: 8pt" color="#808080">



		<p align="left">
		<b><font face="Arial">Available&nbsp; Quantity :</font></b></font><font  face="Arial" style="font-size: 8pt" color="#808080" size="1"><b> 
		<%=rs_cart("Qty")%></b></font></td>
		<td>
		&nbsp;</td>
		</font>
	</tr>
</table>
        <br>
																			<span style="font-size: 8pt">

<font size="1">



																			<a href="products.asp?option=allProducts&del=<%=rs_cart.Fields("P_ID")%>">
																			<font color="#FFFFFF">
																			<img border="0" src="images/delete.png" width="16" height="16"></font></a></font></span><font size="1" color="#FFFFFF"><a href="#" onclick="if(confirm('Are you sure you want to delete this product?')){window.location='products.asp?option=allProducts&del=<%=rs_cart.Fields("P_ID")%>';}"><span style="text-decoration: none">Delete 
														this Product </span> </a>
																			</font>

<font size="1">



<span style="font-size: 8pt"><font color="#FFFFFF"><a href="products.asp?option=updateProduct&id=<%=rs_cart.Fields("P_ID")%>"><img border="0" src="images/edit.png" width="16" height="16"></a></font></span></font><font size="1" color="#FFFFFF"><a href="products.asp?option=updateProduct&id=<%=rs_cart.Fields("P_ID")%>"><span style="text-decoration: none">Update this 
														Product</span></a></font></td>

<%
end if
%>        
<td valign=top align=left width="50%" bgcolor="#FEF3F8">
<p align="justify">
<%rs_cart.moveNext
if not rs_cart.eof then
	
%>
<font class="title">&nbsp;<!--- rec 262 -->
&nbsp;<BR>
<a name="263"></a>
<!-- Default Product Template -->
<img  src="../productImages/<%=rs_cart("P_Image")%>" width="60" height="100" hspace="3" vspace="3" border="1" align=left>

</font>



<font color="#000080" face="Arial" style="font-size: 8pt">

<span style="font-size: 9pt; text-decoration: font-weight:700; font-weight:700"><a name="<%=rs_cart("P_ID")%>"><%=rs_cart("P_Name")%></a></span></font><font class="title"><br>
<%'rs_cart("P_description")%></font><font size="1"><font class="title"><font color="#808080"><span style="font-size: 8pt"><%b=rs_cart("P_description")
dim b
b=Mid(b,1,100)
response.write(b)
%></span> </font></font><font style="font-size: 7pt"><a href="Index.asp?PageName=moreinfo&Id=<%=rs_cart("P_id")%>">
More Info</a></font><br>
&nbsp;<table border="0" id="table39" cellspacing="0" cellpadding="0" width="159">
	<tr>
		<td width="146"><font size="1">
		<p align="left">
		<font face="Arial" style="font-size: 8pt" color="#808080">
		<b>Price
		: $ <%=rs_cart("Price")%></b></font></font></td>
		<td>
		&nbsp;</td>
	</tr>
	<tr>
		<td width="146">

<font size="1">



<font style="font-size: 8pt" color="#808080">



		<p align="left">
		<b><font face="Arial">Available&nbsp; Quantity : </font></b></font><font  face="Arial" style="font-size: 8pt" color="#808080"><b> 
		&nbsp;<%=rs_cart("Qty")%></b></font></td>
		<td>
		&nbsp;</td>
	</tr>
</table>
<p><span style="font-size: 8pt">
																			<a href="products.asp?option=allProducts&del=<%=rs_cart.Fields("P_ID")%>">
																			<font color="#FFFFFF">
																			<img border="0" src="images/delete.png" width="16" height="16"></font></a></span><font size="1" color="#FFFFFF"><a href="#" onclick="if(confirm('Are you sure you want to delete this product?')){window.location='products.asp?option=allProducts&del=<%=rs_cart.Fields("P_ID")%>';}"><span style="text-decoration: none">Delete 
														this Product </span> </a>
																			</font>

<span style="font-size: 8pt">
																			<font color="#FFFFFF">
<a href="products.asp?option=updateProduct&id=<%=rs_cart.Fields("P_ID")%>"><img border="0" src="images/edit.png" width="16" height="16"></a></font></span><font size="1" color="#FFFFFF"><a href="products.asp?option=updateProduct&id=<%=rs_cart.Fields("P_ID")%>"><span style="text-decoration: none">Update this 
														Product</span></a></font></font></td>
      </tr>
      <%
end if
%>       

        
          <tr valign=top>
<td valign=top align=left bgcolor="#FEF3F8" colspan="2">
<hr noshade color="#DBDBDB" size="1">        
        </td>
      </tr>
      <%
			if not rs_cart.eof then
				rs_cart.movenext
			end if
			loop
			%>

    
      
      
          </table>
		   
		   
<p align="justify">	
<%else%>

		
            <p>	
<font color="#FF0000">No Product Found!!</font>
	
<%end if%>


&nbsp;</TD>
        														</tr>
																<tr>
																	<td>&nbsp;</td>
																</tr>
															</table>
														</td>
													</tr>
												</form>
											</table>
								
								
								
								
								
						<% elseif trim(request.querystring("option")) = "updateProduct" Then%> 
											<table border="0" width="99%" id="table35" cellspacing="0">
												<form name="form1" action="" method="get">
													<tr>
														<td colspan="4" height="19">
														<p align="center"><b>


									 <%
										set rs_cat = server.CreateObject("adodb.recordset")
										sql = "select * from PLS_Products where P_ID = "&request.querystring("id")&" "		
										rs_cat.open sql, conn
										'do while not rs_cat.eof
									%>


							</b></td>
													</tr>
													<tr>
														<td colspan="4" height="41" bgcolor="#1B7DCC">
														<b><span >
														<font color="#FFFFFF">Update 
										Product</font> </span></b></td>
													</tr>
													<tr>
														<td width="3%" height="30" bgcolor="#8CC1EC">&nbsp;</td>
														<td width="26%" height="30" colspan="2" align="left" bgcolor="#8CC1EC">
														<span style="font-size: 8pt">Product 
																Name:</span></td>
														<td width="77%" height="30" align="left" bgcolor="#8CC1EC">
														<input type=hidden value="<%=rs_cat.Fields("P_ID")%>" name="updateID">
														<input name="add_p_name" type="text" class="normmiddletext" size="30" maxlength="40" value="<%=rs_cat.Fields("P_Name")%>"></td>
													</tr>
													<tr>
														<td width="3%" height="30" bgcolor="#8CC1EC">&nbsp;</td>
														<td width="21%" height="30" align="left" bgcolor="#8CC1EC">
														<span style="font-size: 8pt">Price:</span></td>
														<td width="3%" height="30" align="left" bgcolor="#8CC1EC">
														<p align="right">$</td>
														<td width="77%" height="30" align="left" bgcolor="#8CC1EC">
														<input name="add_p_price" type="text" class="normmiddletext" size="18" maxlength="40" value="<%=rs_cat.Fields("Price")%>"><span style="font-size: 8pt">
														</span>
														<span style="font-size: 7pt">(eg. 1500)</span></td>
													</tr>
													<tr>
														<td width="3%" height="30" bgcolor="#8CC1EC">&nbsp;</td>
														<td width="26%" height="30" colspan="2" align="left" bgcolor="#8CC1EC">
														<span style="font-size: 8pt">Quantity:</span></td>
														<td width="77%" height="30" align="left" bgcolor="#8CC1EC">
														<input name="add_p_qty" type="text" class="normmiddletext" size="18" maxlength="40" value="<%=rs_cat.Fields("Qty")%>">
														</td>
													</tr>
													<tr>
														<td width="3%" height="30" bgcolor="#8CC1EC">&nbsp;</td>
														<td width="26%" height="30" colspan="2" align="left" bgcolor="#8CC1EC">
														<span style="font-size: 8pt">Category:</span></td>
														<td width="77%" height="30" align="left" bgcolor="#8CC1EC">
														<span style="font-size: 9pt">
														<select name="select_p_category" class="normmiddletext" size="1">
									 <%
										set rs_cat2 = server.CreateObject("adodb.recordset")
										sql2 = "select * from PLS_Catagories"		
										rs_cat2.open sql2, conn
										do while not rs_cat2.eof
									%>
									
														<option <%if rs_cat.Fields("P_Category") = rs_cat2.Fields("C_ID") Then%>selected<%end if%> value=<%=rs_cat2.Fields("C_ID")%>> <% =rs_cat2.Fields("C_Name") %> 
														</option>
                                      
                                    <%
                                    	rs_cat2.MoveNext
                                    	Loop
                                    	rs_cat2.close
                                    %></select></span></td>
													</tr>
													<tr>
														<td height="30" bgcolor="#8CC1EC">&nbsp;</td>
														<td height="30" colspan="2" align="left" bgcolor="#8CC1EC">
														<span style="font-size: 8pt">Product 
																			Description:</span></td>
														<td width="77%" height="30" align="left" bgcolor="#8CC1EC">
														&nbsp;<!--webbot bot="Validation" b-value-required="TRUE" i-maximum-length="200" --><textarea name="add_p_description" cols="54" class="normmiddletext" rows="4"><%=rs_cat.Fields("P_Description")%></textarea></td>
													</tr>
													<tr>
														<td height="30" bgcolor="#8CC1EC">&nbsp;</td>
														<td height="30" colspan="2" align="left" bgcolor="#8CC1EC">
														<span style="font-size: 8pt">Product 
										Image:</span></td>
														<td width="77%" height="30" align="left" bgcolor="#8CC1EC">
														<input readonly name="img" type="text" id="img" class=normmiddletext size="37" value="<%=rs_cat.Fields("P_Image")%>"><input type="button" name="Submit" value="Browse" onClick="JavaScript:doOpen('img');" class=normmiddletext></td>
													</tr>
									<%
										's_cat.close
									%>
<SCRIPT Language="JavaScript">
	function doOpen(mobj)
	{  
	   window.open ("uploadfile.asp?mObjName=" + mobj, "UploadWin", "width=400,height=300,scrollbar=no;");
	}
</SCRIPT>										
													<tr>
														<td height="30" bgcolor="#8CC1EC">&nbsp;</td>
														<td height="30" colspan="2" align="left" bgcolor="#8CC1EC">&nbsp;</td>
														<td width="77%" height="30" bgcolor="#8CC1EC">&nbsp;</td>
													</tr>
													<tr>
														<td height="30" bgcolor="#1B7DCC">&nbsp;</td>
														<td height="30" colspan="2" align="left" bgcolor="#1B7DCC">
														<input type=hidden name="option" value="allProducts">
														<input onmousedown="chkAddVal()" type=submit value = "Update Product" class="normmiddletext" name="updateProduct_btn"></td>
								
								
				<SCRIPT language="JavaScript" type="text/JavaScript">
					function chkAddVal()
					{
						if(document.form1.add_p_name.value == "" || document.form1.add_p_price.value == "" || document.form1.add_p_qty.value == "" || document.form1.add_p_description.value == "")
							{
								alert("All form fields are required");
								
								//if(document.form1.add_p_img.value == "")
									//document.form1.add_p_img.focus();

								if(document.form1.add_p_description.value == "")
									document.form1.add_p_description.focus();									
									
								if(document.form1.add_p_qty.value == "")
									document.form1.add_p_qty.focus();										
									
								if(document.form1.add_p_price.value == "")
									document.form1.add_p_price.focus();										
									
								if(document.form1.add_p_name.value == "")
									document.form1.add_p_name.focus();		
									
							}
					}								
				</script>
								
														<td width="77%" height="30" bgcolor="#1B7DCC">&nbsp;</td>
													</tr>
													<tr>
														<td>&nbsp;</td>
														<td colspan="2">&nbsp;</td>
														<td width="77%">&nbsp;</td>
													</tr>
												</form>
											</table>
						<%end if%>												
						
						
						
							
							</div></td>
									</tr>
									<tr>
										<td style="border-right-style: none; border-right-width: medium" valign="top">&nbsp;</td>
									</tr>
								</table></td>
	</tr>
	<tr>
		<td width="90%" align="left" valign="top" colspan="3">
		<table width="100%" border="0" cellspacing="0" cellpadding="4" class="stripe-two" id="table29">
			<tr>
				<td>
				<p align="center"><font color="#FFFFFF">Pete Louis - Admin Panel</font></td>
			</tr>
		</table></td>
	</tr>
</table>

</body>

</html>
