<!--#include file="connection.inc"-->
<%
dim result, result2, myMail, memberID,chargingDate,subsType 

'----------------------- insert the record ---------------- 
function insertRecord(sql, startNumber, endNumber, resultMessage, errorMessage)
	rs.open sql, con, 3, 2, 1
	if rs.eof then
		rs.addnew
			j  = 1
			for i = startNumber to endNumber
				rs(j) = request.form("T"&i)			 
				j = j + 1
			next 
		rs.update
		j = 1
		rs.movelast()
		memberID = rs(0)
		insertRecord = resultMessage
	else 
		insertRecord = errorMessage 
	end if 
	rs.close
end function

'----------------------- update the record ---------------- 
function updateRecord(sql, startNumber, endNumber, resultMessage, errorMessage)
	rs.open sql, con, 3, 2, 1
	if not rs.eof then
			j  = 1
			for i = startNumber to endNumber
				rs(j) = request.form("T"&i)			 
				j = j + 1
			next 
		rs.update
		j = 1

		memberID = rs(0)
		updateRecord =  resultMessage
	else 
		updateRecord =   errorMessage 
	end if 
	rs.close
end function

'--------------------- Custome update record-----------------
function reactivateAccount(sql, startNumber, endNumber, resultMessage, errorMessage)
	rs.open sql, con, 3, 2, 1
	if not rs.eof then
			j  = 1
			rs(1) = request.form("T1")
			rs(2) = request.form("T3")
			for i = startNumber to endNumber
				rs(i) = request.form("T"&i)			 
				j = j + 1
			next 
		rs.update
		j = 1

		memberID = rs(0)
		reactivateAccount =  resultMessage
	else 
		reactivateAccount =   errorMessage 
	end if 
	rs.close
end function



'----------------------- delete the record ---------------- 
function deleteRecord(sql, resultMessage)
	con.execute sql
	deleteRecord = resultMessage
end function


'----------------------- searchRecord the record ---------------- 
function checkLogin(sql, resultMessage, errorMessage)
	rs.open sql, con, 3, 2, 1
	if not rs.eof then
		memberID = rs(0)
		chargingDate = rs("chargingDate")
		subsType = rs("subscriptionType")
		session("isActive") = rs("isActive")
		checkLogin = resultMessage
	else 
		checkLogin= errorMessage 
	end if 
	rs.close
end function

'----------------------- Send An email ---------------- 
function sendEmail(fromWho, toWho,  Subject, Body, mailFormat, codeType, resultMessage, errorMessage)

		if codeType="CDONTS" then 
			SET myMail = Server.CreateObject("CDONTS.Newmail")
				myMail.From = fromWho
				myMail.To = toWho
				myMail.Cc = ccTo
				myMail.Bcc = bccTo
				myMail.Subject = Subject
				myMail.Body = Body
			
				SELECT CASE mailFormat
			
				CASE "TEXT"
					myMail.MailFormat = 0
					myMail.BodyFormat = 0
			
				CASE "HTML"
					myMail.MailFormat = 1
					myMail.BodyFormat = 1
			
				CASE ELSE
					myMail.MailFormat = 1
					myMail.BodyFormat = 1
			
				END SELECT
			myMail.Send
			sendEmail = resultMessage
		
		elseif codeType="ASP Mail" then 
		
			SET myMail = Server.CreateObject("Persits.MailSender")
				myMail.Host = "mail.cymakcanada.com"
				myMail.Username = "dummy@cymakcanada.com"
				myMail.Password = "dummy"
				myMail.From = fromWho
				myMail.AddAddress toWho
				myMail.Subject = Subject
				myMail.body = Body
				myMail.IsHTML = True
				
				MyMail.Send
		end if
		'Set MyMail = Nothing
end function


'----------------------- Incrept the text ---------------- 
function encryptText(value)
		encryptText = (ASC(Mid(value,1,1)) *19) & "$" & Mid(value,2,Len(value))
end function



'----------------------- decrept the txt   ---------------- 
function decryptText(value)
		dim dollarFlag
		dollarFlag = 0
		dim result 
		result = ""
		For i = 1 to Len(value)
				if Mid(value, i, 1) <> "$"  AND dollarFlag = 0 Then
					result = result & Mid(value, i, 1)
				else
					dollarFlag = 1
					result2 = result2 & Mid(value, i, 1)
				end if
		Next
	
		decryptText = CHR(result/19) & Mid(result2, 2, Len(result2))
end function



'-------------------------- Check File System 
function checkFile(fileName, filePath)
		dim tempFilepath
		set fso = Server.CreateObject("Scripting.FileSystemObject")
		tempFilepath = server.MapPath("./"&filePath) &"\"& fileName
		
		if fso.FileExists(tempFilePath&".gif") then  
			checkFile = filePath & "\" & fileName & ".gif"			
		elseif fso.FileExists(tempFilePath&".jpg") then
			checkFile = filePath & "\" & fileName & ".jpg"			
		elseif fso.FileExists(tempFilePath&".bmp") then
			checkFile = filePath & "\" & fileName & ".bmp"			
		else
			checkFile = filePath & "\"& "s_men.gif"
		end if
			
end function




'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Supporting Funtions
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


'----------------------- Random code generator ----------------

FUNCTION getRandomcode(codelength,numbercombinations,charCodes)
	if charCodes > 35 then
		charCodes = 35
	end if	

codearray = Array("a","b","c","d","e","f","g","h","i","j","k","l", _
"m","n","o","p","q","r","s","t","u","v","w","x", _
"y","z","1","2","3","4","5","6","7","8","9")
	
	FOR x = 1 TO codelength
		RANDOMIZE
		thiscode = (Int(((charCodes - 1) * Rnd) + 1))
		totalcode = totalcode & codearray(thiscode)
		
		IF numbercombinations = "" THEN 
			numbercombinations = 1
			numbercombinations = numbercombinations * charCodes
		end if
	NEXT
		
		getRandomcode = totalcode 
	
END FUNCTION








'----------------------- LongDate Function -----------------------
	Function longDate()
	DIM MonthName(12)
	DIM DayName(7)
	DIM D, M, Y, DMY
	    theDate = Date
	    MonthName(1) = "January"
	    MonthName(2) = "Feburary"
	    MonthName(3) = "March"
	    MonthName(4) = "April"
	    MonthName(5) = "May"
	    MonthName(6) = "Jun"
	    MonthName(7) = "July"
	    MonthName(8) = "August"
	    MonthName(9) = "September"
	    MonthName(10) = "October"
	    MonthName(11) = "November"
	    MonthName(12) = "December"
	    DayName(1) = "Sunday"
	    DayName(2) = "Monday"
	    DayName(3) = "Tuesday"
	    DayName(4) = "Wednesday"
	    DayName(5) = "Thursday"
	    DayName(6) = "Friday"
	    DayName(7) = "Saturday"

	    D = WEEKDAY(theDate)
	    DD = DAY(theDate)
	    M = MONTH(theDate)
	    Y = YEAR(theDate)
	    
	       DMY = DayName(D) & ", " & DD & " " & MonthName(M) & " " & Y
        
	    myLongDate = DMY
		longDate =  myLongDate
	END FUNCTION


%>