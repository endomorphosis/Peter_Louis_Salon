<?php
    include("../db.php");
	$file=$_REQUEST['f'];
	switch($file)
	{
	case 'home':
		$title="Peter Louis SALON";
		$includefile="home.php";
		break;
	case 'billing':
		$title="Shipping & Billing";
		$includefile="billing.php";
		break;

	case 'allproducts':
		$title="Full Product List";
		$includefile="shopsite_sc/store/html/allproducts.php";
		break;
	case 'jflazartigue':
		$title="j f lazartigue";
		$includefile="shopsite_sc/store/html/jflazartigue.php";
		break;
	case 'jflazartigue_Shampoos':
		$title="Jf Lazartigue : Shampoos ";
		$includefile="shopsite_sc/store/html/jflazartigue_Shampoos.php";
		break;
	case 'jflazartigue_OilTreatment':
		$title="Jf Lazartigue : Oil Treatment ";
		$includefile="shopsite_sc/store/html/jflazartigue_OilTreatment.php";
		break;
	case 'jflazartigue_StylingProd':
		$title="Jf Lazartigue : Styling Products ";
		$includefile="shopsite_sc/store/html/jflazartigue_StylingProd.php";
		break;
	case 'jflazartigue_Conditioners':
		$title="Jf Lazartigue : Conditioners ";
		$includefile="shopsite_sc/store/html/jflazartigue_Conditioners.php";
		break;	
	case 'philipb':
		$title="Philip B Products";
		$includefile="shopsite_sc/store/html/philipb.php";
		break;
	case 'philipb_bodyproduct':
		$title="Philip B : Body Products ";
		$includefile="shopsite_sc/store/html/philipb_bodyproduct.php";
		break;
	case 'philipb_ShampTreat':
		$title="Philip B : Shampoos and Treatments  ";
		$includefile="shopsite_sc/store/html/philipb_ShampTreat.php";
		break;
	case 'philipb_StylingProd':
		$title="Philip B : Styling Products ";
		$includefile="shopsite_sc/store/html/philipb_StylingProd.php";
		break;	
	case 'Silica':
		$title="GHD";
		$includefile="shopsite_sc/store/html/Silica.php";
		break;
	case 'Silica_StylingTools':
		$title="GHD : Styling Products";
		$includefile="shopsite_sc/store/html/Silica_StylingTools.php";
		break;
	case 'Silica_StylingProd':
		$title="GHD : Styling Tools";
		$includefile="shopsite_sc/store/html/Silica_StylingProd.php";
		break;
	case 'Silica_ShampCondi':
		$title="GHD : Shampoos and Conditioners ";
		$includefile="shopsite_sc/store/html/Silica_ShampCondi.php";
		break;
	case 'prod_detailGHD':
		$title="GHD Products Details";
		$includefile="shopsite_sc/store/html/prod_detailGHD.php";
		break;	
	case 'skincare':
		$title="Great Lengths NXT Extension Products";
		$includefile="shopsite_sc/store/html/skincare.php";
		break;
	case 'skincare_shampoos':
		$title="Great Lengths/NXT : Shampoos ";
		$includefile="shopsite_sc/store/html/skincare_shampoos.php";
		break;
	case 'skincare_conditionar':
		$title="Great Lengths/NXT : Conditioners ";
		$includefile="shopsite_sc/store/html/skincare_conditionar.php";
		break;
	case 'skincare_styling':
		$title="Great Lengths/NXT : Styling Products";
		$includefile="shopsite_sc/store/html/skincare_styling.php";
		break;
	case 'unique':
		$title="Japanese Products ";
		$includefile="shopsite_sc/store/html/unique.php";
		break;
	case 'unique_Liscio':
		$title="Japanese Products : Liscio ";
		$includefile="shopsite_sc/store/html/unique_Liscio.php";
		break;
	case 'unique_idcare':
		$title="Japanese Products : ID Care  ";
		$includefile="shopsite_sc/store/html/unique_idcare.php";
		break;
	case 'unique_MoltoBene':
		$title="Japanese Products : Molto Bene ";
		$includefile="shopsite_sc/store/html/unique_MoltoBene.php";
		break;
	case 'unique_Nigelle':
		$title="Japanese Products : Nigelle ";
		$includefile="shopsite_sc/store/html/unique_Nigelle.php";
		break;
	case 'unique_IS':
		$title="Japanese Products : IS ";
		$includefile="shopsite_sc/store/html/unique_IS.php";
		break;
	case 'unique_Meros':
		$title="Japanese Products : Meros ";
		$includefile="shopsite_sc/store/html/unique_Meros.php";
		break;
	case 'unique_Yuko':
		$title="Japanese Products : Yuko ";
		$includefile="shopsite_sc/store/html/unique_Yuko.php";
		break;
	case 'unique_Rona':
		$title="Japanese Products : Rona ";
		$includefile="shopsite_sc/store/html/unique_Rona.php";
		break;
	case 'unique_Crede':
		$title="Japanese Products : Crede ";
		$includefile="shopsite_sc/store/html/unique_Crede.php";
		break;
	case 'prod_detailunique':
		$title="Japanese Products ";
		$includefile="shopsite_sc/store/html/prod_detailunique.php";
		break;
	case 'LeonorGreyl':
		$title="LEONOR GREYL PRODUCTS";
		$includefile="shopsite_sc/store/html/LeonorGreyl.php";
		break;
	case 'prod_detailLG':
		$title="LEONOR GREYL PRODUCTS";
		$includefile="shopsite_sc/store/html/prod_detailLG.php";
		break;
	case 'AnagenTherapyProducts':
		$title="Anagen Therapy";
		$includefile="shopsite_sc/store/html/AnagenTherapy.php";
		break;
	case 'clearancesale':
		$title="Gift Certificates ";
		$includefile="shopsite_sc/store/html/clearancesale.php";
		break;
	case 'uniquestylingproducts':
		$title="Unique Styling Products ";
		$includefile="shopsite_sc/store/html/uniquestylingproducts.php";
		break;
	case 'uniquestylingproducts_BlowDryers':
		$title="Unique Styling Products : Blow Dryers ";
		$includefile="shopsite_sc/store/html/uniquestylingproducts_BlowDryers.php";
		break;
	case 'uniquestylingproducts_StylingIrons':
		$title="Unique Styling Products : Styling Irons";
		$includefile="shopsite_sc/store/html/uniquestylingproducts_StylingIrons.php";
		break;
	case 'uniquestylingproducts_Brushes':
		$title="Unique Styling Products : Brushes";
		$includefile="shopsite_sc/store/html/uniquestylingproducts_Brushes.php";
		break;
	case 'prod_detail_uniqueSty':
		$title="Unique Styling Products ";
		$includefile="shopsite_sc/store/html/prod_detail_uniqueSty.php";
		break;	
	case 'derma':
		$title="Peter Louis SALON";
		$includefile="shopsite_sc/store/html/derma.php";
		break;
	case 'dermaShamp_Cond':
		$title="Peter Louis Shampoos and Conditioners";
		$includefile="shopsite_sc/store/html/dermaShamp_Cond.php";
		break;	
	case 'dermaStyling_Prod':
		$title="Peter Louis Styling Products";
		$includefile="shopsite_sc/store/html/dermaStyling_Prod.php";
		break;
	case 'page22':
		$title="Phytologie/PHYTO";
		$includefile="shopsite_sc/store/html/page22.php";
		break;
	case 'dryhair':
		$title="Phytologie: Dry / Ultra Dry Hair";
		$includefile="shopsite_sc/store/html/dryhair.php";
		break;
	case 'verydry':	
		$title="Phytologie: Sun Care";
		$includefile="shopsite_sc/store/html/verydry.php";
		break;
	case 'permedcolored':
		$title="Phytologie: Permed-Colored";
		$includefile="shopsite_sc/store/html/permedcolored.php";
		break;
	case 'stylingproducts':
		$title="Phytologie: Styling Products";
		$includefile="shopsite_sc/store/html/stylingproducts.php";
		break;
	case 'volume':
		$title="Phytologie: Volume";
		$includefile="shopsite_sc/store/html/volume.php";
		break;
	case 'PHYTOPRO':
		$title="Phytologie/Phyto: Anti-Frizz";
		$includefile="shopsite_sc/store/html/PHYTOPRO.php";
		break;
	case 'phytoplage':
		$title="Phytologie/Phyto: All Hair Types";
		$includefile="shopsite_sc/store/html/phytoplage.php";
		break;
	case 'thinninghair':
		$title="Phytologie/Phyto: Hair Loss";
		$includefile="shopsite_sc/store/html/thinninghair.php";
		break;
	case 'dandruff':
		$title="Phytologie/Phyto: dandruff";
		$includefile="shopsite_sc/store/html/dandruff.php";
		break;
	case 'sensitivescalp':
		$title="Phytologie/Phyto: Frequent Use";
		$includefile="shopsite_sc/store/html/sensitivescalp.php";
		break;
	case 'oilyscalp':
		$title="Phytologie/Phyto: Other";
		$includefile="shopsite_sc/store/html/oilyscalp.php";
		break;
	case 'phytospecific':
		$title="Phytologie: Phytospecific";
		$includefile="shopsite_sc/store/html/phytospecific.php";
		break;
	case 'phytospecific_dry':
		$title="Phytospecific: Normal to Dry Hair";
		$includefile="shopsite_sc/store/html/phytospecific_dry.php";
		break;
	case 'phytospecific_ultradry':
		$title="Phytospecific: Ultra Dry Hair";
		$includefile="shopsite_sc/store/html/phytospecific_ultradry.php";
		break;
	case 'phytospecific_damaged':
		$title="Phytospecific: Damaged Brittle Hair";
		$includefile="shopsite_sc/store/html/phytospecific_damaged.php";
		break;
	case 'phytospecific_scalp':
		$title="Phytospecific: Dry Hair and Scalp";
		$includefile="shopsite_sc/store/html/phytospecific_scalp.php";
		break;
	case 'phytospecific_allhairtypes':
		$title="Phytospecific: Styling: All Hair Types";
		$includefile="shopsite_sc/store/html/phytospecific_allhairtypes.php";
		break;
	case 'phytospecific_delicate':
		$title="Phytospecific: Mild: For Delicate and Fine Hair";
		$includefile="shopsite_sc/store/html/phytospecific_delicate.php";
		break;
	case 'phytospecific_normal':
		$title="Phytospecific: Regular: For Normal and Thick Coarse Hair";
		$includefile="shopsite_sc/store/html/phytospecific_normal.php";
		break;
	case 'phytospecific_men':
		$title="Phytospecific: Phyto Men";
		$includefile="shopsite_sc/store/html/phytospecific_men.php";
		break;							
	case 'prod_detail':
		$title="Peter Louis SALON";
		$includefile="shopsite_sc/store/html/prod_detail.php";
		break;
	case 'prod_detaildry':
		$title="Phytologie: Phytospecific";
		$includefile="shopsite_sc/store/html/prod_detaildry.php";
		break;
	case 'ReneFurterer':
		$title="Rene Furterer";
		$includefile="shopsite_sc/store/html/ReneFurterer.php";
		break;		
	case 'rene_essential':
		$title="Rene Furterer: Essential Care";
		$includefile="shopsite_sc/store/html/rene_essential.php";
		break;
	case 'rene_scalp':
		$title="Rene Furterer: Oily Scalp Treatment";
		$includefile="shopsite_sc/store/html/rene_scalp.php";
		break;
	case 'rene_dryscalp':
		$title="Rene Furterer: Dry Scalp and Hair Treatment";
		$includefile="shopsite_sc/store/html/rene_dryscalp.php";
		break;
	case 'rene_hairloss':
		$title="Rene Furterer: Thinning Hair Treatment";
		$includefile="shopsite_sc/store/html/rene_hairloss.php";
		break;
	case 'rene_dandruff':
		$title="Rene Furterer:  Anti Dandruff Treatment";
		$includefile="shopsite_sc/store/html/rene_dandruff.php";
		break;
	case 'rene_frequent':
		$title="Rene Furterer:  Anti Dandruff Treatment";
		$includefile="shopsite_sc/store/html/rene_frequent.php";
		break;
	case 'rene_sensitivescalp':
		$title="Rene Furterer:  Sensitive Scalp";
		$includefile="shopsite_sc/store/html/rene_sensitivescalp.php";
		break;
	case 'rene_colorperm':
		$title="Rene Furterer:  Permed & Colored Hair Treatment";
		$includefile="shopsite_sc/store/html/rene_colorperm.php";
		break;
	case 'rene_styling':
		$title="Rene Furterer:  Styling";
		$includefile="shopsite_sc/store/html/rene_styling.php";
		break;
	case 'rene_beauty':
		$title="Rene Furterer:  Beautifying";
		$includefile="shopsite_sc/store/html/rene_beauty.php";
		break;
	case 'rene_beauty1':
		$title="Rene Furterer:  Beautifying";
		$includefile="shopsite_sc/store/html/rene_beauty1.php";
	case 'LeonorGreyl_pages':
		$title="Leonor Greyl:  Beautifying";
		$includefile="shopsite_sc/store/html/LeonorGreyl_pages.php";
		
		
		
		
		break;


	case 'rene_colorenhance':
		$title="Rene Furterer:  Color Enhancing Mask";
		$includefile="shopsite_sc/store/html/rene_colorenhance.php";
		break;
	case 'rene_suncare':
		$title="Rene Furterer:  Sun Care";
		$includefile="shopsite_sc/store/html/rene_suncare.php";
		break;
	case 'rene_dietery':
		$title="Rene Furterer:  Dietary Supplement";
		$includefile="shopsite_sc/store/html/rene_dietery.php";
		break;
	case 'crede':
		$title="Crede Products";
		$includefile="shopsite_sc/store/html/crede.php";
		break;
	case 'salonpic':
		$title="Peter Louis SALON";
		$includefile="salonpic.php";
		break;
	case 'media_press':
		$title="Peter Louis SALON";
		$includefile="media_press.php";
		break;
	case 'contact':
		$title="Peter Louis SALON";
		$includefile="contact.php";
		break;
	case 'petersbio':
		$title="Peter Louis SALON";
		$includefile="petersbio.php";
		break;

	case 'gallery':
		$title="Peter Louis SALON";
		$includefile="greatlengths/gallery1.php";
		break;
	case 'gallery2':
		$title="Peter Louis SALON";
		$includefile="greatlengths/gallery2.php";
		break;
	case 'gallery3':
		$title="Peter Louis SALON";
		$includefile="greatlengths/gallery3.php";
		break;
	case 'gallery4':
		$title="Peter Louis SALON";
		$includefile="greatlengths/gallery4.php";
		break;
	case 'gallery5':
		$title="Peter Louis SALON";
		$includefile="greatlengths/gallery5.php";
		break;
	case 'gallery6':
		$title="Peter Louis SALON";
		$includefile="greatlengths/gallery6.php";
		break;
	case 'gallery7':
		$title="Peter Louis SALON";
		$includefile="greatlengths/gallery7.php";
		break;
	case 'gallery8':
		$title="Peter Louis SALON";
		$includefile="greatlengths/gallery8.php";
		break;
	case 'gallery9':
		$title="Peter Louis SALON";
		$includefile="greatlengths/gallery9.php";
		break;
	case 'gallery10':
		$title="Peter Louis SALON";
		$includefile="greatlengths/gallery10.php";
		break;
	case 'gallery11':
		$title="Peter Louis SALON";
		$includefile="greatlengths/gallery11.php";
		break;
	case 'gallery12':
		$title="Peter Louis SALON";
		$includefile="greatlengths/gallery12.php";
		break;
	case 'gallery13':
		$title="Peter Louis SALON";
		$includefile="greatlengths/gallery13.php";
		break;
	case 'japanesestraightening':
		$title="Peter Louis SALON";
		$includefile="japanesestraightening.php";
		break;
	case 'index':
		$title="Peter Louis SALON";
		$includefile="greatlengths/index.php";
		break;
	case 'about':
		$title="Peter Louis SALON";
		$includefile="greatlengths/about.php";
		break;
	case 'long':
		$title="Peter Louis SALON";
		$includefile="greatlengths/long.php";
		break;
	case 'long1':
		$title="Peter Louis SALON";
		$includefile="greatlengths/long1.php";
		break;
	case 'volume_strong1':
		$title="Peter Louis SALON";
		$includefile="greatlengths/volume_strong1.php";
		break;
	case 'volume_strong2':
		$title="Peter Louis SALON";
		$includefile="greatlengths/volume_strong2.php";
		break;
	case 'thick1':
		$title="Peter Louis SALON";
		$includefile="greatlengths/thick1.php";
		break;
	case 'thick2':
		$title="Peter Louis SALON";
		$includefile="greatlengths/thick2.php";
		break;
	case 'methodds1':
		$title="Peter Louis SALON";
		$includefile="greatlengths/methodds1.php";
		break;
	case 'methodds2':
		$title="Peter Louis SALON";
		$includefile="greatlengths/methodds2.php";
		break;
	case 'methodds3':
		$title="Peter Louis SALON";
		$includefile="greatlengths/methodds3.php";
		break;
	case 'methodds4':
		$title="Peter Louis SALON";
		$includefile="greatlengths/methodds4.php";
		break;
	case 'methodds5':
		$title="Peter Louis SALON";
		$includefile="greatlengths/methodds5.php";
		break;
	case 'history1':
			$title="Peter Louis SALON";
			$includefile="greatlengths/history1.php";
			break;
	case 'history2':
			$title="Peter Louis SALON";
			$includefile="greatlengths/history2.php";
			break;
	case 'history3':
			$title="Peter Louis SALON";
			$includefile="greatlengths/history3.php";
			break;
	case 'history4':
			$title="Peter Louis SALON";
			$includefile="greatlengths/history4.php";
			break;
	case 'history5':
			$title="Peter Louis SALON";
			$includefile="greatlengths/history5.php";
			break;
	case 'history6':
			$title="Peter Louis SALON";
			$includefile="greatlengths/history6.php";
			break;
	case 'history7':
			$title="Peter Louis SALON";
			$includefile="greatlengths/history7.php";
			break;
	case 'history8':
			$title="Peter Louis SALON";
			$includefile="greatlengths/history8.php";
			break;
	case 'hair_color':
			$title="Peter Louis SALON";
			$includefile="hair_color.php";
			break;
	case 'services':
			$title="Peter Louis SALON";
			$includefile="services.php";
			break;
	case 'AnagenTherapy':
			$title="Peter Louis SALON";
			$includefile="loss.php";
			break;
	case 'prevention':
			$title="Peter Louis SALON";
			$includefile="prevention.php";
			break;
	case 'llht':
			$title="Peter Louis SALON";
			$includefile="llht.php";
			break;
	case 'light':
			$title="Peter Louis SALON";
			$includefile="light.php";
			break;
	case 'eyelash':
			$title="Peter Louis SALON";
			$includefile="eyelash.php";
			break;
	case 'blow':
			$title="Peter Louis SALON";
			$includefile="blow.php";
			break;
	case 'privacy':
			$title="Peter Louis SALON";
			$includefile="privacy.php";
			break;
	case 'terms':
			$title="Peter Louis SALON";
			$includefile="terms_conditions.php";
			break;
	case 'search':
			$title="Peter Louis SALON";
			$includefile="search.php";
			break;
	case 'shopping_cart':
			$title="Peter Louis SALON";
			$includefile="shopping_cart.php";
			break;

		default :
			$title="Peter Louis SALON";
			$includefile="home.php";
			break;
	}
	
	$sqls="select * from meta_tag where file='".$file."'";
	$re=mysql_query($sqls);
	if(mysql_num_rows($re) > 0)
	{
	  	$obj=mysql_fetch_object($re);
		$metadescription	=	$obj->metadescription;
		$metakeywords	=	$obj->metakeywords;
	}else{
		$metadescription	=	"Shop for Phytologie hair care products, shampoos and conditioners, the ultimate in hair care! Brought to you by Peter Louis Salon in New York City.";
		$metakeywords	=	"Phyto, shampoo, gifts, Phyto9, Phytologie, Phytology, makeup, salon, gel, makeovers, dry hair, phytodefrisant, phytosesame, oily, dandruff shampoo, conditioner, nail care, phytologist, phytojoba, hairstylist, discount, Cosmetics, cosmotology, Peter Louis Salon, New York, Manhattan, Second, color, foil, blonde, bad hair day, Upper East Side";
	}
	
	
?>