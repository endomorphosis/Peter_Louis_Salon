#/* --------------------------------------------------- */#
#/* Customer Registration JavaScript                    */#
#/* filename: core_001_pa_customer_registration.js      */#
#/* TEMPLATE: Page                                      */#
#/* THEME: Core                                         */#
#/* CORE: Core 001                                      */#
#/* copyright 2005 ShopSite inc. Design by Stunk        */#
#/* --------------------------------------------------- */#
<script language="JavaScript" type="text/javascript">
  function DisplayLogName(name) {
    var cookies=document.cookie;
    var start = cookies.indexOf(name + "=");
    var name = "";
    var start1;
    var end1;
    var tmp;
    var signed_in = -1;

    if (start != -1) {
      start = cookies.indexOf("=", start) +1;
      var end = cookies.indexOf("|", start);
      if (end != -1) {
        signed_in = cookies.indexOf("|yes", start);
        name = unescape(cookies.substring(start,end-1));
        if (signed_in != -1) {
/* Links When Signed In */
          //document.write("<b>" + name + "</b>");
          //document.write("<br>");
[-- IF ANALYTICS_MULTI_DOMAIN --]
          document.write("�&nbsp;<a class=\"reglink\" href=\"javascript:__utmLinker(\'[-- SHOPPING_CART_URL BASE --]/order.cgi?func=3&storeid=[-- STORE.ID --]&html_reg=html\');\">[-- STORE.ViewEdit --]</a>");
[-- ELSE --]
          document.write("�&nbsp;<a class=\"reglink\" href=\"[-- SHOPPING_CART_URL BASE --]/order.cgi?func=3&storeid=[-- STORE.ID --]&html_reg=html\">[-- STORE.ViewEdit --]</a>");
[-- END_IF --]
          document.write("&nbsp;&nbsp;&nbsp;&nbsp;");
[-- IF ANALYTICS_MULTI_DOMAIN --]
          document.write("�&nbsp;<a class=\"reglink\" href=\"javascript:__utmLinker(\'[-- SHOPPING_CART_URL BASE --]/order.cgi?func=4&storeid=[-- STORE.ID --]&html_reg=html\');\">[-- STORE.SignOut --]</a>");
[-- ELSE --]
          document.write("�&nbsp;<a class=\"reglink\" href=\"[-- SHOPPING_CART_URL BASE --]/order.cgi?func=4&storeid=[-- STORE.ID --]&html_reg=html\">[-- STORE.SignOut --]</a>");
[-- END_IF --]
          //document.write("<br>");

        }
        else {
/* Links When Signed Out */
          //document.write(" - You are no longer signed in<br>");
        }
      }
    }
    if (signed_in == -1) {
/* Links When NOT Signed In */
[-- IF ANALYTICS_MULTI_DOMAIN --]
      document.write("�&nbsp;<a class=\"reglink\" href=\"javascript:__utmLinker(\'[-- SHOPPING_CART_URL BASE --]/order.cgi?func=2&storeid=[-- STORE.ID --]&html_reg=html\');\">[-- STORE.ToSignIn --]</a>");
[-- ELSE --]
      document.write("�&nbsp;<a class=\"reglink\" href=\"[-- SHOPPING_CART_URL BASE --]/order.cgi?func=2&storeid=[-- STORE.ID --]&html_reg=html\">[-- STORE.ToSignIn --]</a>");
[-- END_IF --]
      document.write(" &nbsp;&nbsp; ");
[-- IF ANALYTICS_MULTI_DOMAIN --]
      document.write("�&nbsp;<a class=\"reglink\" href=\"javascript:__utmLinker(\'[-- SHOPPING_CART_URL BASE --]/order.cgi?func=1&storeid=[-- STORE.ID --]&html_reg=html\');\">[-- STORE.ToRegister --]</a>");
[-- ELSE --]
      document.write("�&nbsp;<a class=\"reglink\" href=\"[-- SHOPPING_CART_URL BASE --]/order.cgi?func=1&storeid=[-- STORE.ID --]&html_reg=html\">[-- STORE.ToRegister --]</a>");
[-- END_IF --]
      //document.write("<br>");
    }
  }
  DisplayLogName("[-- RegCookieName --]");
</script>