################################################
# E-Blah Bulliten Board Systems       Platinum #
################################################
# Copyright (c) 2001 - 2004 e-blah!            #
# All Rights Reserved.                         #
################################################
# Allows users language support.               #
################################################

$usersetcount = 45;   # Num. of user settings (current # is for a *NEW* value)
$pwcrypt      = 'ya'; # If 'ya' (below) is changed, change this also! This is conversion key!

if($yabbconver) { $pwcry = 'ya'; } else { $pwcry = "B~"; } # Forum Encryption Key

$uflock = 1; # File Locking (Dis|En)abler
$fopen  = 2;
$fclose = 8;

sub fopen {
	my($fh,$fn) = @_;

	if($fn =~ m~/\.\./~) {
		eval { error("OPEN_ERROR",2); };
		die "\n\nYou MUST use absolute paths.\n\n";
	}

	if($debug == 3 || $debug == 4) { ++$openedfiles; $openedfilenames .= $fn."<br>"; }

	open($fh,$fn);
	if($uflock) {
		flock($fh,$fopen);
		seek($fh,0,0);
	} else { return(1); }
}

sub fclose {
	my($fh) = $_[0];
	if($uflock) { flock($fh,$fclose); }
	close($fh);
}

sub CheckCookies { # Find users cookies, or SID
	foreach(split(/; /,$ENV{'HTTP_COOKIE'})) {
		$_ =~ s/%([a-fA-F0-9][a-fA-F0-9])/pack("C", hex($1))/eg;
		($cname,$cval) = split(/=/);
		$Blah{$cname} = $cval;
	}

	if($Blah{$cookpw}) {
		$password = $Blah{$cookpw};
		$username = $Blah{$cooknme};
	} else { &AutoSid; }
	if($username ne 'Guest') { GetUser(); }
	if($URL{'theme'}) { $usert .= ",theme=$URL{'theme'},"; }

	$scripturl = "Blah.$scripttype?b=$URL{'b'}$usert";
	$surl = "Blah.$scripttype?$usert";
	$rurl = "$realurl/Blah.$scripttype?";
}

sub AutoSid { # See if this is a running SID, find the user in the SID DB.
	my($sidip,$sidfnd,$sid,$tsid,$sidip,$sidun,$sidpw,$endtime,$baccess);
	if(!$ensids) { return; } # Disabled

	if($ENV{'REMOTE_ADDR'} eq '') { return; }
	$sidfnd = 0;
	fopen(SID,"$prefs/Sids.sid");
	while($sid = <SID>) {
		chomp $sid;
		($tsid,$sidip,$sidun,$sidpw,$endtime,$baccess) = split(/\|/,$sid);
		if(length($sidip) == length($ENV{'REMOTE_ADDR'}) && $ENV{'REMOTE_ADDR'} eq $sidip && $endtime > time) { $usert = ",sid=$tsid,"; $sidfnd = 1; $sidinuse = 1; $endtime = 10800+(time); $username = $sidun; $password = $sidpw; } # Get SID in memory
		if($endtime > time) { $swrite .= "$tsid|$sidip|$sidun|$sidpw|$endtime|$baccess\n"; }
	}
	fclose(SID);

	fopen(SID,">$prefs/Sids.sid");
	print SID $swrite;
	fclose(SID);

	if(!$sidfnd) { $username = 'Guest'; $password = ''; } # Valid SID initiated
}

sub GetUser {
	my(@temp);
	fopen(FILE,"$members/$username.dat") or $username = "Guest";
	@temp = <FILE>;
	fclose(FILE);
	chomp @temp;
	if($yabbconver) { $cryptpw = $temp[0]; } else { $cryptpw = crypt($temp[0],$pwcry); }
	if($password ne $cryptpw) { $username = 'Guest'; $password = ''; return; }
	if($temp[23]) { $settings[23] = $temp[23]; return; }

	@settings = @temp;
	@Osettings = @temp; # Keeps member groups correct
	if($settings[34] && -e("$languages/$settings[34].lng")) { $languagep = $settings[34]; }
	if($settings[38]) { $pmdisable = 1; }
	@blockedusers = split(/\|/,$settings[40]);
}
1;