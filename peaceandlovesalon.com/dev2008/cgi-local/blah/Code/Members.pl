################################################
# E-Blah Bulliten Board Systems       Platinum #
################################################
# Copyright (c) 2001 - 2004 e-blah!            #
# All Rights Reserved.                         #
################################################

CoreLoad('Members',1);

sub Members {
	if(!$memguest) {
		if($username eq 'Guest') { &error($gtxt{'noguest'}); }
		&is_member;
	}

	$a = $URL{'a'};
	fopen(FILE,"$members/List.txt");
	while(<FILE>) {
		chomp $_;
		if($URL{'a'} eq '') { # Lets not load the entire database!
			next if !-e("$members/$_.dat");
			push(@listz,$_);
		} else {
			loaduser($_);
			if($userset{$_}->[1] ne '') { push(@mlist,"$userset{$_}->[1]|$_"); }
		}
	}
	fclose(FILE);

	$found = 0;
	if($hideposts != 1) {
		if($a eq 'top') { $top = qq~</td><td class="win"><b>&nbsp; $memtext[2] &nbsp;</b></td><td><span class="smalltext">&nbsp; ~; &TopPosters; }
			else { $top = qq~<a href="$scripturl,v=members,a=top,s=$bk">$memtext[2]</a>~; }
	}
	if($a eq 'let') {
		$userletter = uc($URL{'l'});
		if($userletter && ($userletter ne 'NUM' && $userletter =~ s~([A-Z]{1})~~)) { $userletter = $1; }
		elsif($userletter && $userletter ne 'NUM') { $userletter = A; }
		$letter = <<"EOT";
 <tr>
  <td class="win" colspan="9">
   <table cellpadding="4" cellspacing="0" align="center"><tr>
   <td><span class="smalltext">
EOT
		for($ll = 'A'; $ll ne 'AA'; $ll++) {
			if($userletter eq $ll || ($userletter eq '' && $ll eq 'A')) { $letter .= qq~</td><td class="win2"><b>&nbsp; $ll &nbsp;</b></td><td><span class="smalltext">&nbsp; ~; }
				else { $letter .= qq~<a href="$scripturl,v=members,a=$URL{'a'},l=$ll">$ll</a>&nbsp; &nbsp; ~; }
		}
		if($userletter ne 'NUM') { $letter .= qq~<a href="$scripturl,v=members,a=$URL{'a'},l=num" title="$memtext[5]">#</a>~; }
			else { $letter .= qq~</td><td class="win2"><b>&nbsp;#&nbsp;</b></td><td>~; }
		$letter .= <<"EOT";
   </span></td>
   </tr>
  </table>
  </td>
 </tr>
EOT
		$let = qq~</td><td class="win"><b>&nbsp; $memtext[6] &nbsp;</b></td><td><span class="smalltext">&nbsp; ~; &Letter;
		$templetter = $userletter;
	}
		else { $let = qq~<a href="$scripturl,v=members,a=let,s=$bk">$memtext[6]</a>~; }
	if($a eq 'groups') {
		push(@listz,FindRanks('Administrator'));
		for($q = 0; $q < @memgrps; $q++) {
			if($q < 7) { next; }
			push(@listz,FindRanks($memgrps[$q]));
		}
		$pro = qq~</td><td class="win"><b>&nbsp; $memtext[8] &nbsp;</b></td><td><span class="smalltext">&nbsp; ~;
	}
		else { $pro = qq~<a href="$scripturl,v=members,a=groups,s=$bk">$memtext[8]</a>~; }
	if($a eq '') { $list = qq~</td><td class="win"><b>&nbsp; $memtext[10] &nbsp;</b></td><td><span class="smalltext">&nbsp; ~; }
		else { $list = qq~<a href="$scripturl,v=members,s=$bk">$memtext[10]</a>~; }
	$sortedby = "$list &nbsp; $top &nbsp; $let &nbsp; $pro";

	# How many page links?
	$tmax = $totalpp*20;
	$treplies = @listz < 0 ? 1 : @listz;
	if($treplies < $mmpp) { $URL{'s'} = 0; }
	$tstart = $URL{'s'} || 0;
	$counter = 1;
	$link = "$scripturl,v=members,a=$URL{'a'},l=$userletter,s";
	if($tstart > $treplies) { $tstart = $treplies; }
	$tstart = (int($tstart/$mmpp)*$mmpp);
	if($tstart > 0) { $bk = ($tstart-$mmpp); $pagelinks = qq~<a href="$link=$bk">&#171;</a> ~; }
	if($treplies > ($tmax/2) && $tstart > $mmpp*((($totalpp*2)/5)+1) && $treplies > $tmax) { $pagelinks .= qq~<a href="$link=0">...</a> ~; }
	for($i = 0; $i < $treplies+1; $i += $mmpp) {
		if($i < $bk-($mmpp*(($totalpp*2)/5)) && $treplies > $tmax) { ++$counter; $final = $counter-1; next; }
		if($URL{'s'} ne 'all' && $i == $tstart || $treplies < $mmpp) { $pagelinks .= qq~<b>$counter</b>, ~; $nxt = ($tstart+$mmpp); }
			else { $pagelinks .= qq~<a href="$link=$i">$counter</a>, ~; }
		++$counter;
		if($counter > $totalpp+$final && $treplies > $tmax) { $gbk = (int($treplies/$mmpp)*$mmpp); $pagelinks =~ s/, \Z//gsi; $pagelinks .= qq~ <a href="$link=$gbk">...</a> ~; ++$i; last; }
	}
	$pagelinks =~ s/, \Z//gsi;
	if(($tstart+$mmpp) != $i && $URL{'s'} ne 'all') { $pagelinks .= qq~ <a href="$link=$nxt">&#187;</a>~; }

	$counter = -1;

	$title = $memtext[12];
	&header;
	$ebout .= <<"EOT";
<table class="border" cellpadding="0" cellspacing="1" width="95%" align="center"><tr>
<td class="titlebg"><table cellpadding="5" cellspacing="0"><tr><td><b><img src="$images/profile_sm.gif" align="left"> $title</b></td></tr></table></td></tr>
  <td class="win2" colspan="9">
   <table cellpadding="5" cellspacing="0" align="center">
    <tr>
     <td><span class="smalltext">$sortedby</span></td>
    </tr>
   </table>
  </td>
 </tr>$letter
</table>
<br><table class="border" cellpadding="5" cellspacing="1" width="95%" align="center">
 <tr>
  <td class="titlebg" align="center" width="5%"><span class="smalltext"><img src="$images/pm_sm.gif" alt="$memtext[14]"></span></td>
  <td class="titlebg" align="center" width="25%"><span class="smalltext"><b>$memtext[15]</b></span></td>
  <td class="titlebg" align="center" width="7%"><span class="smalltext"><b>$var{'22'}</b></span></td>
  <td class="titlebg" align="center" width="5%"><img src="$images/site_sm.gif" alt="$memtext[16]"></td>
  <td class="titlebg" align="center" width="7%"><span class="smalltext"><b>$memtext[2]</b></span></td>
EOT
	$ebout .= <<"EOT";
  <td class="titlebg" align="center" width="20%"><span class="smalltext"><b>$memtext[18]</b></span></td>
  <td class="titlebg" align="center" width="25%"><span class="smalltext"><b>$memtext[19]</b></span></td>
EOT
	if($enablerep) { $ebout .= qq~<td class="titlebg" align="center" width="10%"><span class="smalltext"><b>$gtxt{'rep'}</b></span></td>~; $tspan = 8; } else { $tspan = 7; }
	$ebout .= "</tr>";

	&LoadColors;
	fopen(FILE,"$prefs/Active.txt");
	@actlog = <FILE>;
	fclose(FILE);
	chomp @actlog;
	foreach $temp (@listz) {
		if($a eq 'groups' || $a eq '') { $_ = $temp; }
			else { ($temp2,$_) = split(/\|/,$temp); }

		++$counter;
		if($counter < $tstart) { next; }
		if($counter >= ($tstart+$mmpp)) { last; }

		if($userset{$temp}->[1] eq '') { loaduser($temp); }

		$homepage = '';
		$offonline = '';

		if($userset{$_}->[1] eq '') { next; }
			else { $userset{$_}->[1] = $gcolors{$userset{$_}->[4]} ? qq~<font color="$gcolors{$userset{$_}->[4]}"><b>$userset{$_}->[1]</b></font>~ : qq~$userset{$_}->[1]~; }

		if($userset{$_}->[12] && $hmail && $settings[4] ne 'Administrator') { $email = qq~<img src="$images/lockmail.gif" alt="$gtxt{'27'}">~; }
			else { $email = qq~<a href="mailto:$userset{$_}->[2]"><img src="$images/email_sm.gif" border="0" alt="$userset{$_}->[2]"></a>~; }
		if($userset{$_}->[20] ne '' && $userset{$_}->[19] eq '') { $userset{$_}->[19] = $userset{$_}->[20]; }
		if($userset{$_}->[19] ne '') { $homepage = qq~<a href="$userset{$_}->[20]"><img src="$images/site_sm.gif" border="0" alt="$userset{$_}->[19]"></a>~; }
		if($messagecnt > 0) {
			$postcount = sprintf("%.2f",(($userset{$_}->[3]/$messagecnt)*100));
			if($postcount > 100) { $postcount = "100"; }
		}

		foreach $on (@actlog) {
			($onoff) = split(/\|/,$on);
			if($onoff eq $_) { $offonline = qq~<a href="$scripturl,v=memberpanel,a=pm,s=write,t=$_"><img src="$images/pm_sm.gif" border="0" alt="$gtxt{'30'}"></a>~; last; }
		}
		if($offonline eq '' || $userset{$_}->[18]) { $offonline = qq~<a href="$scripturl,v=memberpanel,a=pm,s=write,t=$_"><img src="$images/pm_sm_off.gif" border="0" alt="$gtxt{'31'}"></a>~; }
		$regdate = &get_date($userset{$_}->[14]);

		$age = calage($userset{$_}->[16]);
		if(!$userset{$_}->[16]) { $calendar = ''; }
			else { $calendar = qq~<a href="$ageurl" title="$memtext[21]: $age)"><img src="$images/cal_sm.gif" border="0"></a>~; }

		$change = $stat = $var{'60'};
		foreach $grps (@grpsposts) {
			($num,$memgrp) = split(/\|/,$grps);
			if($userset{$_}->[3] >= $num) { $stat = $memgrp; }
		}

		$change = $stat;
		if($userset{$_}->[4] ne '') {
			$team = '';
			if($team{$userset{$_}->[4]}) { $team = qq~<img src="$images/team.gif" alt="$gtxt{'29'}"> ~; }
			if($star{$userset{$_}->[4]} || $team) { $memberstat = qq~<b>$userset{$_}->[4]</b>~; }
				else { $memberstat = "<i>$userset{$_}->[4]</i>"; }
			$stat = "$team$memberstat";
		}
		if($userset{$_}->[4] eq 'Administrator') { $stat = qq~<img src="$images/team.gif" alt="$gtxt{'29'}"> <b>$memgrps[0]</b>~; $team = 1; }

		if($URL{'a'} eq 'groups' && $userset{$_}->[4] ne $tempplace) {
			if($team) { $team = qq~<img src="$images/admin_sm.gif" align="left"> ~; }
			if($userset{$_}->[4] eq 'Administrator') { $grp = $memgrps[0]; }
				else { $grp = $userset{$_}->[4]; }
			$ebout .= <<"EOT";
 <tr>
  <td class="catbg" colspan="$tspan"><b>$team$grp</b></td>
 </tr>
EOT
			$tempplace = $userset{$_}->[4];
		}

		$ts = lc(substr($temp2,0,1));
		if($URL{'a'} eq 'let' && $ts ne $templetter) {
			$tsb = uc(substr($temp2,0,1));
			$ebout .= <<"EOT";
 <tr>
  <td class="catbg" colspan="$tspan"><b>$tsb</b></td>
 </tr>
EOT
			$templetter = $ts;
		}

		if($URL{'a'} eq 'top' && $change ne $tempstat) {
			$tsb = uc(substr($_,0,1));
			$ebout .= <<"EOT";
 <tr>
  <td class="catbg" colspan="$tspan"><b>$change</b></td>
 </tr>
EOT
			$tempstat = $change;
		}

		$ebout .= <<"EOT";
 <tr>
  <td class="win2" align="center"><span class="smalltext">$offonline</span></td>
  <td class="win"><span class="smalltext"><a href="$scripturl,v=memberpanel,a=view,u=$_">$userset{$_}->[1]</a></span></td>
  <td class="win2" align="center">$email</td>
  <td class="win2" align="center">$homepage</td>
EOT
		if($hideposts != 1) { $ebout .= qq~<td class="win2" width="3%" align="center"><span class="smalltext">~.MakeComma($userset{$_}->[3]).qq~</span></td>~; }
			else { $ebout .= qq~<td><span class="smalltext">$gtxt{13}</span></td>~; }

		$ebout .= <<"EOT";
  <td class="win"><span class="smalltext">$stat</span></td>
  <td class="win"><span class="smalltext">$regdate</span></td>
EOT
		if($enablerep) {
			$reputation = '';
			if($userset{$_}->[43] ne '') {
				$color = '';
				if($userset{$_}->[43] < 50) { $color = qq~class="redrep"~; } # Horrible
				elsif($userset{$_}->[43] > 75) { $color = qq~class="greenrep"~; } # Best
					else { $color = qq~class="grayrep"~; }

				$reputation = qq~<font $color>$userset{$_}->[43]%</font>~;
			}
			$ebout .= qq~<td class="win2" align="center"><span class="smalltext">$reputation</span></td>~;
		}

		$ebout .= "</tr>";
	}
	if($listz[0] eq '') { $ebout .= <<"EOT";
 <tr>
  <td class="win" colspan="9" align="center"><b>$memtext[23]</b></td>
 </tr>
EOT
	}
	$ebout .= <<"EOT";
 <tr>
  <td colspan="$tspan" class="catbg"><span class="smalltext"><b><img src="$images/board.gif"> $gtxt{'17'}:</b> $pagelinks</span></td>
 </tr>
</table>
EOT
	&footer;
	exit;
}


sub Letter {
	$l = $userletter || 'A';
	@mlist = sort{lc($a) cmp lc($b)} @mlist;
	foreach $listme (@mlist) {
		($_,$usename) = split(/\|/,$listme);
		$snu = uc(substr($_,0,1));
		if($snu =~ s~[0-9#%+,\\\/:?"<>'|@^\$\&\~'\)\(\]\[\;{}!`=-]~~gsi) { $snu = 'NUM'; }
		$letter{$snu} .= "$_:$usename|";
	}

	for($ll = 'A'; $ll ne 'AB'; $ll++) {
		if($ll eq 'AA') { $ll = 'NUM'; }
		if(uc($l) ne $ll && !$nope) { next; }
		$nope = 1;
		@array = split(/\|/,$letter{$ll});
		foreach(@array) {
			($p1,$p2) = split(/\:/,$_);
			push(@listz,"$p1|$p2");
		}
		if($ll eq 'NUM') { return; }
	}
}

sub TopPosters {
	foreach $temp (@mlist) {
		($t,$_) = split(/\|/,$temp);
		push(@slist,"$userset{$_}->[3]|$_");
	}
	foreach(sort {$b <=> $a} @slist) {
		($t,$usr) = split(/\|/,$_);
		push(@listz,"$userset{$_}->[1]|$usr");
	}
}
1;