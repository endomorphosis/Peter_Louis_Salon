################################################
# E-Blah Bulliten Board Systems       Platinum #
################################################
# Copyright (c) 2001 - 2004 e-blah!            #
# All Rights Reserved.                         #
################################################

CoreLoad('Login',1);

sub error_log {
	if($URL{'r'} == 1 && $er eq '') { $er = $logintxt[13]; }
	elsif($er ne '') { } # Already added
		else { $er = $logintxt[14]; }
	$error = <<"EOT";
<table class="border" cellpadding="6" cellspacing="1" width="500" align="center">
 <tr>
  <td class="titlebg"><span class="smalltext"><b><img src="$images/warning_sm.gif"> $logintxt[41]</b></span></td>
 </tr><tr>
  <td class="win"><span class="smalltext"><b>$logintxt[42]</b><br><br>&nbsp; &nbsp; &nbsp; &#149; $er<br><br>$appendattempts</span></td>
 </tr>
</table><br>
EOT
}

sub Login {
	if($maxattempts > 0 && $loginfailtime > 0) {
		fopen(FILE,"$prefs/loginlock.txt");
		@locked = <FILE>;
		fclose(FILE);
		chmod @locked;
		foreach(@locked) {
			($ipaddy,$time,$attempts) = split(/\|/,$_);
			if($ipaddy eq $ENV{'REMOTE_ADDR'}) {
				$attempts = $maxattempts-$attempts;
				if((time-$time) < (60*$loginfailtime)) { $appendattempts = "$logintxt[43] $attempts $logintxt[44]"; }
					else { last; }
				if($attempts <= 0) { $er = "$logintxt[45] $loginfailtime $logintxt[46]"; $appendattempts = ''; }
				last;
			}
		}
	}

	if($URL{'p'} > 2) { &Logout; }
	if($username ne 'Guest') { &error($logintxt[1]); }
	$gdisable = 1;
	if($URL{'p'} eq 'forgotpw') { &SendPassword; }
	if($URL{'p'} eq 'forgotpw2') { &SendPassword2; }
	if($URL{'p'} eq 'forgotpw3') { &SendPassword3; }
	if($URL{'p'} == 2) { &Login2; }
	if($URL{'r'} || $er) { &error_log; }

	$title = $logintxt[12];
	&header;
	$ebout .= <<"EOT";
<script language="javascript1.2">
<!--
function tdisable(val) {
 if(document.login.nocookie.checked == false) {
  document.login.days.checked  = true;
  document.login.days.disabled = false;
 } else {
  document.login.days.checked  = false;
  document.login.days.disabled = true;
 }
}
-->
</script>
$error$warnonthis<table cellspacing="1" cellpadding="4" class="border" width="500" align="center">
 <tr><form action="$scripturl,v=login,p=2" method="POST" name="login">
  <td class="titlebg"><b>$title</b></td>
 </tr><tr>
  <td class="win">
  <table cellpadding="3" cellspacing=0 width=100%>
   <tr>
    <td align="right" width="40%"><b>$logintxt[2]:</b></td>
    <td width="60%"><input type="text" class="textinput" name="username" size="25" value="$URL{'u'}" tabindex="1"></td>
   </tr><tr>
    <td></td>
    <td><span class="smalltext"><b><img src="$images/register_sm.gif" align="left"> <a href="$scripturl,v=register">$logintxt[3]</a></b></span></td>
   </tr><tr>
    <td align="right" width="40%"><b>$logintxt[4]:</b></td>
    <td width="60%"><input type="password"  class="textinput" name="password" size="23" tabindex="2"></td>
   </tr><tr>
    <td></td>
    <td><span class="smalltext"><b><img src="$images/keys.gif" align="left"> <a href="$scripturl,v=login,p=forgotpw">$logintxt[5]</a></b></span></td>
   </tr>
  </table>
  </td>
 </tr><tr>
  <td class="catbg"><span class="smalltext"><b>Session Prefrences</b></span></td>
 </tr><tr>
  <td class="win2" width="100%">
  <table cellpadding="3" cellspacing="0" width="100%">
   <tr>
    <td width="5%"><input type="checkbox" name="days" value="forever" tabindex="3" checked></td>
    <td width="95%">$logintxt[54]</td>
   </tr>
EOT
	if($ensids) {
		$ebout .= <<"EOT";
   <tr>
    <td width="5%" class="win"><input type="checkbox" name="nocookie" value="1" tabindex="4" onClick="tdisable()"></td>
    <td width="95%">$logintxt[53]</td>
   </tr><tr>
    <td colspan="2" class="win"><div class="smalltext" align="justify">$logintxt[11]</div></td>
   </tr>
EOT
	}
	$ebout .= <<"EOT";
  </table>
  </td>
 </tr><tr>
  <td class="win" align="center">
  <input type="submit" class="button" value=" $logintxt[12] " tabindex="5">
  </td></form>
 </tr>
</table>
EOT

	&footer;
	exit;
}

sub Login2 {
	my(@files,$username);

	if($maxattempts > 0 && $loginfailtime > 0) {
		$timecur = time;
		fopen(FILE,">$prefs/loginlock.txt");
		foreach(@locked) {
			($ipaddy,$time,$attempts) = split(/\|/,$_);

			if(($timecur-$time) > (60*$loginfailtime)) { next; }

			if($ipaddy eq $ENV{'REMOTE_ADDR'}) {
				++$attempts;
				print FILE "$ENV{'REMOTE_ADDR'}|$timecur|$attempts\n";
				$exout = 1;
				if($maxattempts-$attempts < 0) { $redirafter = 1; }
			} else { print FILE "$_\n"; }
		}
		if(!$exout) { print FILE "$ENV{'REMOTE_ADDR'}|$timecur|1\n"; }
		fclose(FILE);
		if($redirafter) { redirect("$scripturl,v=login,r=3"); }
	}

	$fnd = 1;

	opendir(DIR,"$members/");
	while( $_ = readdir(DIR) ) {
		if($_ eq '' || $_ eq '.' || $_ eq '..' || $_ !~ /\.dat\Z/) { next; }
		$list .= $_;
		if($FORM{'username'} eq $_) { $username = $_; }
	}
	closedir(DIR);
	loaduser($username);

	if($userset{$FORM{'username'}}->[1] eq '') {
		$FORM{'username'} =~ s/ //gsi;
		fopen(FILE,"$members/List.txt");
		@list = <FILE>;
		fclose(FILE);
		foreach(@list) {
			chomp $_;
			loaduser($_);
			$userset{$_}->[1] =~ s/ //gsi;
			if(lc($userset{$_}->[1]) eq lc($FORM{'username'}) || lc($_) eq lc($FORM{'username'})) { $username = $_; $fnd = 0; last; }
		}
	} else { $fnd = 0; }
	if($fnd) { redirect("$scripturl,v=login,r=1"); }

	# Get passwords
	$password1 = crypt("$FORM{'password'}",$pwcry);
	if($yabbconver) { $password2 = $userset{$username}->[0]; } else { $password2 = crypt("$userset{$username}->[0]",$pwcry); }

	if($password2 ne $password1) { redirect("$scripturl,v=login,r=2,u=$FORM{'username'}"); }

	if($FORM{'nocookie'} && $ensids && $ENV{'REMOTE_ADDR'} ne '') { # Create a random sid ...
		$randomsid = sprintf("%.0f",rand(int(time*9497)*9429));
		$randomsid =~ tr/2963/qmzpledxo/;
		fopen(FILE,"$prefs/Sids.sid");
		while( $sid = <FILE> ) {
			chomp $sid;
			($tsid,$ip,$sidname,$t,$endtime) = split(/\|/,$sid);
			unless($endtime > time && $sidname eq $username) { $othersids .= "$sid\n"; }
			if($tsid eq $randomsid) { $randomsid = time; }
		}
		fclose(FILE);
		$ptime = 10800+time;
		fopen(FILE,"+>$prefs/Sids.sid");
		print FILE qq~$randomsid|$ENV{'REMOTE_ADDR'}|$username|$password1|$ptime\n$othersids~;
		fclose(FILE);
		$url = "$surl,sid=$randomsid";
	} else {
		if($FORM{'days'} eq '') { $FORM{'days'} = '.5'; } # Set's it to 12 hours
		if($FORM{'days'} eq 'forever') {
			$exp = 'Sat, 31-Dec-2039 00:00:00 GMT';
		} else {
			$time = time;
			if($FORM{'days'} !~ /[0-9]/) { $FORM{'days'} = 365; } # Blank? Lets make it a year then
			if($FORM{'days'} eq '') { $FORM{'days'} = 1; }
			$maxdays = $FORM{'days'}*86400;
			$maxdays = $maxdays+$time;
			($csec,$cmin,$chour,$cday,$cmonth,$cyear,$cweek,$cydays,$cdst) = gmtime($maxdays);
			if($chour < 10) { $chour = "0$chour"; }
			if($cmonth < 10) { $cmonth = "0$cmonth"; }
			$xyear = $cyear+1900;
			@xdays = ('Sun','Mon','Tues','Wed','Thur','Fri','Sat');
			@xmonths = ('Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec');
			$exp = "$xdays[$cweek], $cday-$xmonths[$cmonth]-$xyear $chour:00:00 GMT";
		}
		SetCookie($cooknme,$username,$exp);
		SetCookie($cookpw,$password1,$exp);
		$url = $surl;
	}
	fopen(FILE,"$prefs/Active.txt");
	@active = <FILE>;
	fclose(FILE);
	chomp @active;
	fopen(FILE,"+>$prefs/Active.txt");
	foreach(@active) {
		($luser) = split(/\|/,$_);
		if($luser ne $ENV{'REMOTE_ADDR'}) { print FILE "$_\n"; }
	}
	fclose(FILE);
	if($logip) {
		$curtime = time;
		fopen(FILE,">>$prefs/IpLog.txt");
		print FILE "$username|1|$ENV{'REMOTE_ADDR'}|$curtime\n";
		fclose(FILE);
	}
	$redirectfix = 1;
	if($FORM{'redirect'}) { $url .= ",b=$FORM{'redirect'}"; }
	&redirect;
}

sub Logout {
	fopen(FILE,"$members/$username.dat");
	@memberdata = <FILE>;
	fclose(FILE);
	chomp @memberdata;

	if($username eq 'Guest') { &error($logintxt[15]); }
	if($memberdata[13] || $URL{'p'} == 4) { &Logout2; }
	$title = $logintxt[16];
	&header;
	$ebout .= <<"EOT";
<table class="border" cellspacing="1" cellpadding="4" align="center" width="450">
 <tr>
  <td class="titlebg"><b>$title</b></td>
 </tr><tr>
  <td class="win"><table><tr><td>$logintxt[17]</td></tr></table></td>
 </tr><tr>
  <td class="win2"><table width="100%" cellpadding="0"><tr>
   <td><b><a href="javascript:history.back(-1)">&#171; $gtxt{'32'}</a> | <a href="$scripturl,v=login,p=4">$gtxt{'33'} &#187;</a></b></td>
   <td align="right"><span class="smalltext"><a href="$scripturl,v=login,p=4,a=disable">$logintxt[40]</a></span></td></tr>
  </table></td>
 </tr>
</table>
EOT
	&footer;
	exit;
}

sub Logout2 {
	if($URL{'a'} eq 'disable') {
		$memberdata[13] = 1;
		fopen(FILE,"+>$members/$username.dat");
		for($q = 0; $q < $usersetcount; $q++) { print FILE "$memberdata[$q]\n"; }
		fclose(FILE);
	}

	if($URL{'sid'} && $Blah{$cookpw} eq '' && $ensids) {
		fopen(FILE,"$prefs/Sids.sid");
		while( $sid = <FILE> ) {
			chomp $sid;
			($tsid,$ip,$sidname,$t,$endtime) = split(/\|/,$sid);
			unless($endtime > time || $username eq $sidname) { $othersids .= "$sid\n"; }
		}
		fclose(FILE);
		fopen(FILE,"+>$prefs/Sids.sid");
		print FILE $othersids;
		fclose(FILE);
	}
	SetCookie($cooknme,'','Sat, 31-Dec-2039 00:00:00 GMT');
	SetCookie($cookpw,'','Sat, 31-Dec-2039 00:00:00 GMT');

	# Log last THREE logins for later board use ...
	if($Blah{"$cooknme\Logout1"} eq $username || $Blah{"$cooknme\Logout2"} eq $username || $Blah{"$cooknme\Logout3"} eq $username) { $skip = 1; }
	for($x = 1; $x < 3; ++$x) {
		if($skip) { last; }
		if(!$Blah{"$cooknme\Logout$x"}) {
			SetCookie("$cooknme\Logout$x",'$username','Sat, 31-Dec-2039 00:00:00 GMT');
			last;
		}
	}

	fopen(FILE,"$prefs/Active.txt");
	@active = <FILE>;
	fclose(FILE);
	fopen(FILE,"+>$prefs/Active.txt");
	foreach (@active) {
		chomp;
		($luser) = split(/\|/,$_);
		if($luser ne $username) { print FILE "$_\n"; }
	}
	fclose(FILE);
	if($logip) {
		$curtime = time;
		fopen(FILE,">>$prefs/IpLog.txt");
		print FILE "$username|2|$ENV{'REMOTE_ADDR'}|$curtime\n";
		fclose(FILE);
	}
	$url = "$surl,b=$URL{'b'}";
	$redirectfix = 1;
	$username = 'Guest';
	$settings[4] = '';
	&redirect;
}

sub SendPassword {
	$title = $logintxt[5];
	&header;
	$ebout .= <<"EOT";
<table class="border" cellpadding="5" cellspacing="1" width="450" align="center">
 <tr><form action="$scripturl,v=login,p=forgotpw2" method="POST">
  <td class="titlebg"><b><img src="$images/keys.gif"> $logintxt[5]</b></td>
 </tr><tr>
  <td class="win"><span class="smalltext">$logintxt[29]</span></td>
 </tr><tr>
  <td class="win2">
  <table cellpadding="4" cellspacing="0" width="100%">
   <tr>
    <td align="right" width="40%"><b>$logintxt[2]:</b></td>
    <td width="60%"><input type="text" class="textinput" name="username" size="25"></td>
   </tr><tr>
    <td align="right" width="40%"><b>$gtxt{'23'}:</b></td>
    <td width="60%"><input type="text" class="textinput" name="email" size="30"></td>
   </tr>
  </table>
  </td>
 </tr><tr>
  <td class="win" align="center"><input type="submit" class="button" name="submit" value=" $logintxt[31] "></td>
 </form></tr>
</table>
EOT
	&footer;
	exit;
}

sub SendPassword2 {
	loaduser($FORM{'username'});
	if($userset{$FORM{'username'}}->[1] eq '') { # Not in database, search each file extensivly
		$FORM{'username'} =~ s/ //gsi;
		fopen(FILE,"$members/List.txt");
		@list = <FILE>;
		fclose(FILE);
		foreach(@list) {
			chomp $_;
			loaduser($_);
			$userset{$_}->[1] =~ s/ //gsi;
			if(lc($userset{$_}->[1]) eq lc($FORM{'username'}) || lc($_) eq lc($FORM{'username'})) { $FORM{'username'} = $_; last; }
		}
	}
	if($userset{$FORM{'username'}}->[1] eq '') { error($logintxt[13]); }
	if(lc($userset{$FORM{'username'}}->[2]) ne lc($FORM{'email'})) { error($logintxt[39]); }

	if($userset{$FORM{'username'}}->[44] ne '') {
		($t,$oldtime) = split(/\|/,$userset{$FORM{'username'}}->[44]);
		if($oldtime+43200 > time) { error($logintxt[47]); }
	}

	$randomsid = sprintf("%.0f",rand(int(time/9)*7000));
	$userset{$FORM{'username'}}->[44] = $randomsid."|".time;
	fopen(FILE,">$members/$FORM{'username'}.dat");
	for($x = 0; $x < $usersetcount; $x++) { print FILE $userset{$FORM{'username'}}->[$x]."\n"; }
	fclose(FILE);

	$message = <<"EOT";
$logintxt[33] <a href="$rurl,v=login,p=forgotpw3,uid=$FORM{'username'},id=$randomsid">$logintxt[48]</a>.


$gtxt{'25'}



$logintxt[37] "$mbname", $logintxt[38]
EOT
	$warnonthis = qq~<body onLoad="javascript:window.alert('$logintxt[49]');">~;
	smail($userset{$FORM{'username'}}->[2],$logintxt[5],$message);
}

sub SendPassword3 {
	loaduser($URL{'uid'});
	if($userset{$URL{'uid'}}->[44] ne '') {
		($tempsid,$oldtime) = split(/\|/,$userset{$URL{'uid'}}->[44]);
		if(($oldtime+43200 > time) && $tempsid eq $URL{'id'}) {
			$randomsid = sprintf("%.0f",rand(int(time/9)*8000));
			$userset{$URL{'uid'}}->[0] = $randomsid;
			if($yabbconver) { $userset{$URL{'uid'}}->[0] = crypt($randomsid,$pwcry); }
			$userset{$URL{'uid'}}->[44] = ''; # Can't use this vailidation ID again
			fopen(FILE,">$members/$URL{'uid'}.dat");
			for($x = 0; $x < $usersetcount; $x++) { print FILE $userset{$URL{'uid'}}->[$x]."\n"; }
			fclose(FILE);
		}
			else { error($logintxt[51]); }
	} else { error($logintxt[51]); }
	$message = <<"EOT";
$logintxt[50]

<b>$logintxt[34]</b> $randomsid
<b>$logintxt[35]</b> $URL{'uid'}
<b>$logintxt[36]</b> $userset{$URL{'uid'}}->[1]


$gtxt{'25'}



$logintxt[37] "$mbname", $logintxt[38]
EOT
	$warnonthis = qq~<body onLoad="javascript:window.alert('$logintxt[52]');">~;
	smail($userset{$URL{'uid'}}->[2],$logintxt[5],$message);
}
1;