################################################
# E-Blah Bulliten Board Systems       Platinum #
################################################
# Copyright (c) 2001 - 2004 e-blah!            #
# All Rights Reserved.                         #
################################################

CoreLoad('Admin1',1);
CoreLoad('Admin2',1);

&is_admin;

sub PostRecount {
	foreach $use (@boardbase) {
		($board,$t,$t,$t,$t,$t,$t,$t,$t,$pcnt) = split('/',$use);
		if($pcnt) { next; }
		fopen(FILE,"$boards/$board.msg");
		while(<FILE>) {
			chomp $_;
			($id,$msub) = split(/\|/,$_);
			fopen(MESSAGE,"$messages/$id.txt");
			while( $buffer = <MESSAGE> ) {
				chomp $buffer;
				($user,$message,$ip,$t,$ptime) = split(/\|/,$buffer);
				++$posts{$user};
			}
			fclose(MESSAGE);
		}
		fclose(FILE);
	}
	fopen(FILE,"$members/List.txt") || error($admintxt2[304],1);
	while(<FILE>) {
		chomp $_;
		@tempsettings = ();
		fopen(WRITE,"$members/$_.dat");
		@tempsettings = <WRITE>;
		fclose(WRITE);
		chomp @tempsettings;

		$tempsettings[3] = $posts{$_} || 0;
		if($username eq $_) { @settings = @tempsettings; }
		fopen(WRITE,"+>$members/$_.dat");
		for($q = 0; $q < $usersetcount; $q++) { print WRITE "$tempsettings[$q]\n"; }
		fclose(WRITE);
	}
	fclose(FILE);

	redirect("$scripturl,v=admin,r=3");
}

sub Smiley {
	if($URL{'s'}) { &Smiley2; }
	$title = $admintxt2[5];
	&headerA;
	$ebout .= <<"EOT";
<script language="javascript">
<!-- //
function clear(url) {
 if(window.confirm("$admintxt2[4]")) { location = url; }
}
// -->
</script>
<table class="border" cellpadding="4" cellspacing="1" width="98%" align="center">
 <tr><form action="$scripturl,v=admin,a=smiley,s=1" method="post" name="post"><input type="hidden" name="ok" value="ok">
  <td class="titlebg" colspan="5"><b><img src="$images/smiley.gif"> $title</b></td>
 </tr><tr>
  <td class="catbg" colspan="2" width="200"><span class="smalltext"><b>$var{'44'}</b></span></td>
  <td class="catbg" width="250"><span class="smalltext"><b>$admintxt2[7]</b></span></td>
  <td class="catbg" width="200" align="center"><span class="smalltext"><b>$admintxt2[231]</b></span></td>
  <td class="catbg" width="50" align="center"><span class="smalltext"><b>$admintxt2[8]</b></span></td>
 </tr>
EOT
	fopen(FILE,"$prefs/smiley.txt");
	@smile = <FILE>;
	fclose(FILE);
	chomp @smile;
	$counter = 0;
	foreach (@smile) {
		($smiley,$url,$pack) = split(/\|/,$_);
		if($pack) { $packn = qq~$pack (<a href="$scripturl,v=admin,a=smiley,s=1,p=$pack,o=1">$admintxt2[229]</a>)~; }
			else { $packn = $admintxt2[157]; }
		$ebout .= <<"EOT";
 <tr>
  <td class="win2" width="50" align="center"><img src="$simages/$url"></td>
  <td class="win" align="center"><input type="text" class="textinput" name="smile_$counter" value="$smiley" size="25"></td>
  <td class="win2"><span class="smalltext"><b>smilies / <input type="text" class="textinput" name="url_$counter" value="$url" size="30"><input type="hidden" name="pack_$counter" value="$pack"></b></span></td>
  <td class="win" align="center"><span class="smalltext"><b>$packn</b></span></td>
  <td class="win2" align="center"><input type="checkbox" class="checkboxinput" name="del_$counter" value="1"></td>
 </tr>
EOT
		++$counter;
	}
	if($smile[0] eq '') {
		$ebout .= <<"EOT";
<tr>
 <td class="win2" colspan="5" align="center"><span class="smalltext"><br>$admintxt2[9]<br><br></span></td>
</tr>
EOT
	}

	opendir(DIR,"$modsdir/");
	@smiley = readdir(DIR);
	closedir(DIR);
	foreach(@smiley) {
		if($_ =~ m/(.*)(.smp)\Z/) {
			if(-e "$modsdir/$_.installed") { $action = qq~<a href="$scripturl,v=admin,a=smiley,s=1,p=$1,o=1">$admintxt2[229]</a>~; }
				else { $action = qq~<a href="$scripturl,v=admin,a=smiley,s=1,p=$1,o=0">$admintxt2[228]</a>~; }
			$smileypacks .= "<br><b>$1</b> - $action";
		}
	}
	if(!$smileypacks) { $smileypacks = "<br>$admintxt2[227]"; }

	$ebout .= <<"EOT";
 <tr>
  <td class="catbg" colspan="5"><span class="smalltext"><b>$admintxt2[10]</b></span></td>
 </tr><tr>
  <td class="win2" width="50" align="center"><img src="$images/question.gif"></td>
  <td class="win" width="150" align="center"><input type="text" class="textinput" name="smile_new" size="25"></td>
  <td class="win2" colspan="3"><span class="smalltext"><b>smilies / <input type="text" class="textinput" name="url_new" value="" size="30"></b></span></td>
 </tr><tr>
  <td class="win2" width="50" align="center"><img src="$images/smiley.gif"></td>
  <td class="win" colspan="4"><span class="smalltext"><b>$admintxt2[226]</b>$smileypacks</span></td>
 </tr><tr>
  <td class="win2" colspan="5" align=center><input type="hidden" name="cnt" value="$counter"><input type="submit" class="button" name="submit" value=" $admintxt2[11] "></td>
 </tr></form>
</table>
EOT
	&footerA;
	exit;
}

sub Smiley2 {
	fopen(FILE,"$prefs/smiley.txt");
	@data = <FILE>;
	fclose(FILE);
	chomp @data;
	if($URL{'p'}) { # Smiley packs
		if(!-e "$modsdir/$URL{'p'}.smp") { &error($admintxt2[230]); }
		fopen(FILE,"$modsdir/$URL{'p'}.smp");
		@pack = <FILE>;
		fclose(FILE);
		chomp @pack;
		foreach (@pack) { $pak{"$_|$URL{p}"} = 1; }

		fopen(FILE,"+>$prefs/smiley.txt");
		foreach(@data) {
			if(!$pak{$_}) { print FILE "$_\n"; }
		}
		if(!$URL{'o'}) {
			foreach (@pack) { print FILE "$_|$URL{'p'}\n"; }
			fopen(FILE,">$modsdir/$URL{'p'}.smp.installed");
			fclose(FILE);
		} else { unlink("$modsdir/$URL{'p'}.smp.installed"); }
		fclose(FILE);
	} else { # Add / Delete / Edit Smileys
		while(($in,$out) = each(%FORM)) {
			$output = Format($out);
			if($output =~ s/"//gsi) { &error($gtxt{'bfield'}); }
			$FORM{$in} = $output;
		}
		fopen(FILE,"+>$prefs/smiley.txt");
		for($i = 0; $i < $FORM{'cnt'}; $i++) {
			if(!$FORM{"del_$i"}) { print FILE qq~$FORM{"smile_$i"}|$FORM{"url_$i"}|$FORM{"pack_$i"}\n~; }
		}
		if($FORM{'smile_new'} ne '') {
			print FILE "$FORM{'smile_new'}|$FORM{'url_new'}\n";
		}
		fclose(FILE);
	}
	redirect("$surl,v=admin,a=smiley");
}

sub Settings {
	$l = $URL{'l'};
	if($bversion != $theblahver) { $l = "all"; }
	if($URL{'s'} eq 'save') { &Settings3; }
	elsif($l ne '') { &Settings2; }
	$title = $admintxt2[12];
	&headerA;
	$ebout .= <<"EOT";
<table class="border" cellpadding="4" cellspacing="1" width="600" align="center">
 <tr>
  <td class="titlebg" colspan="2"><b><img src="$images/settings.gif"> $title</b></td>
 </tr><tr>
  <td class="win" align="center" width="30"><img src="$images/set_dir.gif"></td>
  <td class="win"><b><a href="$scripturl,v=admin,a=sets,l=dir">$admintxt2[13]</a></b><br><span class="smalltext">&nbsp;<b>&#155;</b> $admintxt2[14]</span></td>
 </tr><tr>
  <td class="win2" align="center" width="30"><img src="$images/set_ess.gif"></td>
  <td class="win2"><b><a href="$scripturl,v=admin,a=sets,l=ess">$admintxt2[17]</a></b><br><span class="smalltext">&nbsp;<b>&#155;</b> $admintxt2[18]</span></td>
 </tr><tr>
  <td class="win" align="center" width="30"><img src="$images/set_block.gif"></td>
  <td class="win"><b><a href="$scripturl,v=admin,a=sets,l=blk">$admintxt2[19]</a></b><br><span class="smalltext">&nbsp;<b>&#155;</b> $admintxt2[20]</span></td>
 </tr><tr>
  <td class="win2" align="center" width="30"><img src="$images/convert.gif"></td>
  <td class="win2"><b><a href="$scripturl,v=admin,a=sets,l=upl">$admintxt2[21]</a></b><br><span class="smalltext">&nbsp;<b>&#155;</b> $admintxt2[22]</span></td>
 </tr><tr>
  <td class="win" align="center" width="30"><img src="$images/boardinfo.gif"></td>
  <td class="win"><b><a href="$scripturl,v=admin,a=sets,l=bro">$admintxt2[23]</a></b><br><span class="smalltext">&nbsp;<b>&#155;</b> $admintxt2[24]</span></td>
 </tr><tr>
  <td class="win2" align="center" width="30"><img src="$images/clicklog.gif"></td>
  <td class="win2"><b><a href="$scripturl,v=admin,a=sets,l=log">$admintxt2[25]</a></b><br><span class="smalltext">&nbsp;<b>&#155;</b> $admintxt2[26]</span></td>
 </tr><tr>
  <td class="win" align="center" width="30"><img src="$images/set_mod.gif"></td>
  <td class="win"><b><a href="$scripturl,v=admin,a=sets,l=mods">$admintxt2[31]</a></b><br><span class="smalltext">&nbsp;<b>&#155;</b> $admintxt2[32]</span></td>
 </tr><tr>
  <td class="catbg" colspan="2"><span class="smalltext"><b><a href="$scripturl,v=admin,a=sets,l=all">$admintxt2[33]</a></b></span></td>
 </tr>
</table>
EOT
	&footerA;
	exit;
}

sub Settings2 {
	$title = "$admintxt2[12] - ";
	if($l eq 'dir') { $title .= $admintxt2[13]; }
	elsif($l eq 'ess') { $title .= $admintxt2[17]; }
	elsif($l eq 'blk') { $title .= $admintxt2[19]; }
	elsif($l eq 'bro') { $title .= $admintxt2[23]; }
	elsif($l eq 'upl') { $title .= $admintxt2[21]; }
	elsif($l eq 'log') { $title .= $admintxt2[25]; }
	elsif($l eq 'flk') { $title .= $admintxt2[27]; }
	elsif($l eq 'news') { $title .= $admintxt2[29]; }
	elsif($l eq 'mods') { $title .= $admintxt2[31]; }
	elsif($l eq 'all') { $title .= $admintxt2[35]; }
		else { &error($admintxt2[36]); }
	$gzipen2 = $gzipen;
	&headerA;
	$ebout .= <<"EOT";
<table class="border" cellspacing="1" cellpadding="4" width="98%" align="center">
 <tr><form action="$scripturl,v=admin,a=sets,l=$URL{'l'},s=save" method="post" name="post" enctype="multipart/form-data">
  <td class="titlebg"><b><img src="$images/settings.gif"> $admintxt2[12]</b></td>
 </tr>
EOT
	if($l eq 'dir' || $l eq 'all') {
		$rurl =~ s~/Blah.pl\?~~gsi;
		$ebout .= <<"EOT";
 <tr>
  <td class="catbg"><table cellpadding="0" cellspacing="0"><tr><td><img src="$images/set_dir.gif"></td><td class="catbgtext"><b>&nbsp;$admintxt2[13]</b></td></tr></table></td>
 </tr><tr>
  <td class="win"><span class="smalltext">$admintxt2[14]<br>$admintxt2[244]</span></td>
 </tr><tr>
  <td class="win2">
   <table cellspacing="0" cellpadding="2" width="100%">
    <tr>
     <td align="right" width="50%"><b>Root<span class="smalltext"> (DIR)</span>:</b><br><span class="smalltext">$admintxt2[38]</span></td>
     <td valign="top"><input type="text" class="textinput" name="root" value="$root" size=30></td>
    </tr><tr>
     <td align="right"><b>Code<span class="smalltext"> (DIR)</span>:</b><br></td>
     <td valign="top"><input type="text" class="textinput" name="code" value="$code" size=30></td>
    </tr><tr>
     <td align="right"><b>Boards<span class="smalltext"> (DIR)</span>:</b></td>
     <td valign="top"><input type="text" class="textinput" name="boards" value="$boards" size=30></td>
    </tr><tr>
     <td align=right><b>$admintxt2[41]<span class="smalltext"> (Prefs, DIR)</span>:</b></td>
     <td valign=top><input type="text" class="textinput" name="prefs" value="$prefs" size=30></td>
    </tr><tr>
     <td align=right><b>Members<span class="smalltext"> (DIR)</span>:</b></td>
     <td valign=top><input type="text" class="textinput" name="members" value="$members" size=30></td>
    </tr><tr>
     <td align=right><b>Messages<span class="smalltext"> (DIR)</span>:</b></td>
     <td valign=top><input type="text" class="textinput" name="messages" value="$messages" size=30></td>
    </tr><tr>
     <td align=right><b>Themes<span class="smalltext"> (DIR)</span>:</b></td>
     <td valign=top><input type="text" class="textinput" name="theme" value="$theme" size=30></td>
    </tr><tr>
     <td align=right><b>Languages<span class="smalltext"> (DIR)</span>:</b></td>
     <td valign=top><input type="text" class="textinput" name="languages" value="$languages" size="30"></td>
    </tr><tr>
     <td align=right><b>Mods<span class="smalltext"> (DIR)</span>:</b></td>
     <td valign=top><input type="text" class="textinput" name="modsdir" value="$modsdir" size="30"></td>
    </tr><tr>
     <td colspan="2" class="win"><span class="smalltext"><b>Templates</b></span></td>
    </tr><tr>
     <td align=right><b>Templates<span class="smalltext"> (DIR)</span>:</b></td>
     <td valign=top><input type="text" class="textinput" name="templates" value="$templates" size="30"></td>
    </tr><tr>
     <td align=right><b>Templates<span class="smalltext"> (URL)</span>:</b></td>
     <td valign=top><input type="text" class="textinput" name="templatesu" value="$templatesu" size="30"></td>
    </tr><tr>
     <td colspan="2" class="win"><span class="smalltext"><b>$admintxt2[292]</b></span></td>
    </tr><tr>
     <td align=right><b>Avatars<span class="smalltext"> (DIR)</span>:</b></td>
     <td valign=top><input type="text" class="textinput" name="avdir" value="$avdir" size=30></td>
    </tr><tr>
     <td align=right><b>Avatars<span class="smalltext"> (URL)</span>:</b></td>
     <td valign=top><input type="text" class="textinput" name="avsurl" value="$avsurl" size=30></td>
    </tr><tr>
     <td colspan="2" class="win"><span class="smalltext"><b>$admintxt2[293]</b></span></td>
    </tr><tr>
     <td align=right><b>$admintxt2[40]<span class="smalltext"> (URL)</span>:</b></td>
     <td valign=top><input type="text" class="textinput" name="rurl" value="$realurl" size=30></td>
    </tr><tr>
     <td align=right><b>Images<span class="smalltext"> (URL)</span>:</b></td>
     <td valign=top><input type="text" class="textinput" name="images" value="$images" size=30></td>
    </tr><tr>
     <td align=right><b>Smilies<span class="smalltext"> (URL)</span>:</b></td>
     <td valign=top><input type="text" class="textinput" name="simages" value="$simages" size=30></td>
    </tr><tr>
     <td align=right><b>$admintxt2[42]<span class="smalltext"> (BC.js, URL)</span>:</b></td>
     <td valign=top><input type="text" class="textinput" name="bcloc" value="$bcloc" size=30></td>
    </tr><tr>
     <td align=right><b>News URL<span class="smalltext"> (news.js, URL)</span>:</b></td>
     <td valign=top><input type="text" class="textinput" name="newsloc" value="$newsloc" size=30></td>
    </tr><tr>
     <td align=right><b>Help<span class="smalltext"> (URL)</span>:</b></td>
     <td valign=top><input type="text" class="textinput" name="helpdesk" value="$helpdesk" size="30"></td>
    </tr>
   </table>
  </td>
 </tr>
EOT
	} if($l eq 'ess' || $l eq 'all') {
		push(@checks,('mailauth'));
		&CheckBoxes;
		$mail{$mailuse} = ' selected';
		$emailsig = Unformat($emailsig);
		$ebout .= <<"EOT";
 <tr>
  <td class="catbg"><table cellpadding="0" cellspacing="0"><tr><td><img src="$images/set_ess.gif"></td><td class="catbgtext"><b>&nbsp;$admintxt2[17]</b></td></tr></table></td>
 </tr><tr>
  <td class="win"><span class="smalltext">$admintxt2[18]</span></td>
 </tr><tr>
  <td class="win2">
   <table cellspacing=0 cellpadding=2 width=100%>
    <tr>
     <td align=right width="50%"><b>$admintxt2[48]:</b></td>
     <td valign=top><input type="text" class="textinput" name="regto" value='$regto' size=30></td>
    </tr><tr>
     <td align=right><b>$admintxt2[49]:</b></td>
     <td valign=top><input type="text" class="textinput" name="eadmin" value='$eadmin' size=30></td>
    </tr><tr>
     <td colspan="2" class="win"><span class="smalltext"><b>$admintxt2[294]</b></span></td>
    </tr><tr>
     <td align=right><b>$admintxt2[51]:</b></td>
     <td valign=top><input type="text" class="textinput" name="cooknme" value='$cooknme' size=30></td>
    </tr><tr>
     <td align=right><b>$admintxt2[50]:</b></td>
     <td valign=top><input type="text" class="textinput" name="cookpw" value='$cookpw' size=30></td>
    </tr><tr>
     <td colspan="2" class="win"><span class="smalltext"><b>$admintxt2[295]</b></span></td>
    </tr><tr>
     <td align="right"><b>$admintxt2[52]:</b></td>
     <td valign="top"><select name="mailuse"><option value="1"$mail{'1'}>Sendmail</option><option value="2"$mail{'2'}>SMTP</option></select></td>
    </tr><tr>
     <td align=right><b>Sendmail $admintxt2[53]:</b></td>
     <td valign=top><input type="text" class="textinput" name="smaill" value="$smaill" size=30></td>
    </tr><tr>
     <td align=right valign="top"><b>SMTP $admintxt2[54]:</b></td>
     <td><input type="text" class="textinput" name="mailhost" value="$mailhost" size=30></td>
    </tr><tr>
     <td>&nbsp;</td>
     <td><table>
      <tr>
       <td colspan="2"><input type="checkbox" class="checkboxinput" name="mailauth" value="1"$mailauth{1}> <span class="smalltext"><b>$admintxt2[268]</b></span></td>
      </tr><tr>
       <td align=right><span class="smalltext"><b>SMTP $admintxt2[173]:</b></span></td>
       <td valign=top><input type="text" class="textinput" name="mailusername" value="$mailusername" size="30"></td>
      </tr><tr>
       <td align=right><span class="smalltext"><b>SMTP $admintxt2[267]:</b></span></td>
       <td valign=top><input type="password"  class="textinput" name="mailpassword" value="$mailpassword" size="30"></td>
      </tr>
     </table></td>
    </tr><tr>
     <td align="right" valign="top"><b>$admintxt2[252]:</b><br><span class="smalltext">$admintxt2[251]</span></td>
     <td valign="top"><textarea name="emailsig" wrap="virtual" rows="3" cols="50">$emailsig</textarea></td>
    </tr>
   </table>
  </td>
 </tr>
EOT
	} if($l eq 'blk' || $l eq 'all') {
		push(@checks,('maintance','lockout','noguest','creg'));
		&CheckBoxes;
		$maintancer = $maintancer || $admintxt2[257];
		$gzipen{$gzipen2} = ' selected';

		$ebout .= <<"EOT";
 <tr>
  <td class="catbg"><table cellpadding="0" cellspacing="0"><tr><td><img src="$images/set_block.gif"></td><td class="catbgtext"><b>&nbsp;$admintxt2[19]</b></td></tr></table></td>
 </tr><tr>
  <td class="win"><span class="smalltext">$admintxt2[20]</span></td>
 </tr><tr>
  <td class="win2">
   <table cellspacing="0" cellpadding="2" width="100%">
    <tr>
     <td align=right width="50%"><b>$admintxt2[55]:</b><br><span class="smalltext">$admintxt2[56]</span></td>
     <td valign=top><input type="checkbox" class="checkboxinput" name="maintance" value="1"$maintance{1}></td>
    </tr><tr>
     <td align=right width="50%" valign="top"><b>$admintxt2[57]:</b></td>
     <td valign=top><textarea name="maintancer" wrap="virtual" rows="4" cols="50">$maintancer</textarea></td>
    </tr><tr>
     <td colspan="2" class="win"><span class="smalltext"><b>$admintxt2[58]</b></span></td>
    </tr><tr>
     <td align="right"><b>$admintxt2[58]:</b><br><span class="smalltext">$admintxt2[59]</span></td>
     <td valign="top"><input type="checkbox" class="checkboxinput" name="lockout" value="1"$lockout{1}></td>
    </tr><tr>
     <td colspan="2" class="win"><span class="smalltext"><b>$admintxt2[296]</b></span></td>
    </tr><tr>
     <td align="right"><b>$admintxt2[60]:</b></td>
     <td valign="top"><input type="checkbox" class="checkboxinput" name="noguest" value="1"$noguest{1}></td>
    </tr><tr>
     <td align="right"><b>$admintxt2[61]:</b></td>
     <td valign="top"><input type="checkbox" class="checkboxinput" name="creg" value="1"$creg{1}></td>
    </tr><tr>
     <td colspan="2" class="win"><span class="smalltext"><b>$admintxt2[254]</b></span></td>
    </tr><tr>
     <td align="right"><b>$admintxt2[254]:</b><br><span class="smalltext">$admintxt2[253]</span></td>
     <td valign="top"><input type="text" class="textinput" name="serload" size="4" value="$serload"></td>
    </tr><tr>
     <td colspan="2" class="win"><span class="smalltext"><b>$admintxt2[332]</b></span></td>
    </tr><tr>
     <td align="right"><b>$admintxt2[329]:</b><br><span class="smalltext">$admintxt2[328]</td>
     <td valign="top"><select name="gzipen"><option value="0"$gzipen{0}>$admintxt2[157]</option><option value="2"$gzipen{2}>gzip (*nix)</option><option value="1"$gzipen{1}>Zlib (Win32/*nix)</option></select></td>
    </tr>
   </table>
  </td>
 </tr>
EOT
	} if($l eq 'upl' || $l eq 'all') {
		push(@checks,('ntsys','avupload'));
		&CheckBoxes;
		$uallows{$uallow} = " selected";

		$ebout .= <<"EOT";
 <tr>
  <td class="catbg"><table cellpadding="0" cellspacing="0"><tr><td><img src="$images/convert.gif"></td><td class="catbgtext"><b>&nbsp;$admintxt2[21]</b></td></tr></table></td>
 </tr><tr>
  <td class="win"><span class="smalltext">$admintxt2[22]</span></td>
 </tr><tr>
  <td class="win2">
   <table cellspacing=0 cellpadding=2 width=100%>
    <tr>
     <td align=right width="50%"><b>$admintxt2[63]:</b></td>
     <td valign=top><select name="uallow"><option value="0"$uallows{0}>$admintxt2[64]</option><option value="1"$uallows{1}>$admintxt2[65]</option><option value="2"$uallows{2}>$admintxt2[66]</option><option value="3"$uallows{3}>$admintxt2[67]</option></select></td>
    </tr><tr>
     <td align=right><b>$admintxt2[206]</b><br><span class="smalltext">$admintxt2[207]</span></td>
     <td valign=top><input type="checkbox" class="checkboxinput" name="avupload" value="1"$avupload{1}></td>
    </tr><tr>
     <td colspan="2" class="win"><span class="smalltext"><b>$admintxt2[297]</b></span></td>
    </tr><tr>
     <td align=right><b>$admintxt2[69]<span class="smalltext"> (DIR)</span>:</b></td>
     <td valign=top><input type="text" class="textinput" name="uploaddir" value="$uploaddir" size="30"></td>
    </tr><tr>
     <td align=right><b>$admintxt2[69]<span class="smalltext"> (URL)</span>:</b></td>
     <td valign=top><input type="text" class="textinput" name="uploadurl" value="$uploadurl"  size="30"></td>
    </tr><tr>
     <td colspan="2" class="win"><span class="smalltext"><b>$admintxt2[298]</b></span></td>
    </tr><tr>
     <td align=right><b>$admintxt2[68]</b></td>
     <td valign=top><input type="checkbox" class="checkboxinput" name="ntsys" value="1"$ntsys{1}></td>
    </tr><tr>
     <td align=right><b>$admintxt2[70]:</b><br><span class="smalltext">$admintxt2[71]</span></td>
     <td valign=top><input type="text" class="textinput" name="allowedext" value="$allowedext" size="25"></td>
    </tr><tr>
     <td align=right><b>$admintxt2[72]:</b><br><span class="smalltext">$admintxt2[73]</span></td>
     <td valign=top><input type="text" class="textinput" name="maxsize" value="$maxsize" size="6"><span class="smalltext"> $admintxt2[303]</span><br><input type="text" class="textinput" name="maxsize2" value="$maxsize2" size="6"><span class="smalltext"> $admintxt2[302]</span></td>
    </tr>
   </table>
  </td>
 </tr>
EOT
	} if($l eq 'bro' || $l eq 'all') {
		push(@checks,('enablerep','emailadmin','enbdays','showlocation','ensids','nonotify','html','hiddenmail','allowrate','pmdisable','gdisable','reversesum','showevents','logactive','showactive','quickreg','sview','showtheme','sall','showdes','showgender','btod','mtext','amar','sauser','enews','upbc','showreg','BCLoad','BCSmile','slpoller','hmail','quickreply','polltop','showmove','whereis','al','sppd','invfri','gsearch','noguestp','memguest'));
		&CheckBoxes;

		$pollops = $pollops || 7;
		foreach(@htmls) { $tags .= "$_,"; }

		$apic{$apic}  = ' selected';
		$DF{$datedis} = ' selected';
		$TF{$timedis} = ' selected';

		($sec,$min,$hour,$day,$month,$year,$week,$ydays,$dst) = localtime($date+(3600*($gtoff+$gtzone)));
		$year = $year+1900;

		$VRA{$vradmin} = ' selected';

		opendir(DIR,"$languages/");
		@lngs = readdir(DIR);
		closedir(DIR);

		foreach(@lngs) {
			($lng,$type) = split(/\./,$_);
			if($type ne 'lng') { next; }
			$check = '';
			if($language eq "$languages/$lng") { $check = ' selected'; }
			$lngs .= qq~<option value="$lng"$check>$lng</option>~;
		}

		$redirectfix{$redirectfix} = " selected";
		$hms{$hiddenmail} = ' selected';
		$hps{$hideposts} = ' selected';

		$ebout .= <<"EOT";
 <tr>
  <td class="catbg"><table cellpadding="0" cellspacing="0"><tr><td><img src="$images/boardinfo.gif"></td><td class="catbgtext"><b>&nbsp;$admintxt2[23]</b></td></tr></table></td>
 </tr><tr>
  <td class="win"><span class="smalltext">$admintxt2[24]</span></td>
 </tr><tr>
  <td class="win2">
   <table cellspacing="0" cellpadding="3" width=100%>
    <tr>
     <td align=right><b>$admintxt2[123]:</b></td>
     <td valign=top><input type="text" class="textinput" name="mbname" value="$mbname" size="40"></td>
    </tr><tr>
     <td align=right><b>$admintxt2[119]:</b></td>
     <td valign=top><select name="lng">$lngs</select></td>
    </tr><tr>
     <td align=right><b>$admintxt2[94]</b></td>
     <td valign=top><input type="checkbox" class="checkboxinput" name="btod" value="1"$btod{1}></td>
    </tr><tr>
     <td align=right><b>$admintxt2[224]:</b><br><span class="smalltext">$admintxt2[223]</span></td>
     <td valign=top><input type="text" class="textinput" name="nocomma" value="$nocomma" size="5"></td>
    </tr><tr>
     <td align=right><b>$admintxt2[269]:</b></td>
     <td valign=top><select name="redirectfix"><option value="0"$redirectfix{0}>Location $admintxt2[270]</option><option value="2"$redirectfix{2}>Redirect (no html)</option><option value="1"$redirectfix{1}>HTML Redirect ($admintxt2[271])</option></select></td>
    </tr><tr>
     <td align=right><b>$admintxt2[98]</b></td>
     <td valign=top><input type="checkbox" class="checkboxinput" name="showtheme" value="1"$showtheme{1}></td>
    </tr><tr>
     <td align=right><b>$admintxt2[93]</b></td>
     <td valign=top><input type="checkbox" class="checkboxinput" name="mtext" value="1"$mtext{1}></td>
    </tr><tr>
     <td colspan="2" class="win"><span class="smalltext"><b>$admintxt2[322]</b></span></td>
    </tr><tr>
     <td align="right"><b>$admintxt2[323]:</b><br><span class="smalltext">$admintxt2[324]</span></td>
     <td valign="top"><input type="text" class="textinput" name="maxattempts" value="$maxattempts" size="5"></td>
    </tr><tr>
     <td align="right"><b>$admintxt2[325]:</b><br><span class="smalltext">$admintxt2[326]</span></td>
     <td valign="top"><input type="text" class="textinput" name="loginfailtime" value="$loginfailtime" size="5"></td>
    </tr><tr>
     <td align="right"><b>$admintxt2[316]</b><br><span class="smalltext">$admintxt2[315]</span></td>
     <td valign="top"><input type="checkbox" class="checkboxinput" name="ensids" value="1"$ensids{1}></td>
    </tr><tr>
     <td colspan="2" class="win"><span class="smalltext"><b>$admintxt2[278]</b></span></td>
    </tr><tr>
     <td align=right><b>$admintxt2[117]:</b></td>
     <td valign=top><select name="datedis"><option value="0"$DF{'0'}>$months[6] 6th, 2002</option><option value="1"$DF{'1'}>$days[6], $months[6] 6th, 2002</option><option value="2"$DF{'2'}>6 $months[6] 2002</option><option value="3"$DF{'3'}>7.6.2002</option><option value="4"$DF{'4'}>6/7/2002</option><option value="5"$DF{'5'}>2002/7/6</option></select></td>
    </tr><tr>
     <td align=right><b>$admintxt2[118]:</b></td>
     <td valign=top><select name="timedis"><option value="0"$TF{'0'}>7:40pm</option><option value="1"$TF{'1'}>7:40:00pm</option><option value="2"$TF{'2'}>19:40</option><option value="3"$TF{'3'}>19:40:00</option></select></td>
    </tr><tr>
     <td align=right><b>$admintxt2[212]:</b><br><span class="smalltext">$admintxt2[211]</span></td>
     <td valign=top><input type="text" class="textinput" name="gtoff" value="$gtoff" size="5"></td>
    </tr><tr>
     <td align=right><b>$admintxt2[245]:</b><br><span class="smalltext">$admintxt2[246]</span></td>
     <td valign=top><input type="text" class="textinput" name="gtzone" value="$gtzone" size="5"></td>
    </tr><tr>
     <td colspan="2" class="win"><span class="smalltext"><b>$admintxt2[279]</b></span></td>
    </tr><tr>
     <td align=right><b>$admintxt2[75]</b></td>
     <td valign=top><input type="checkbox" class="checkboxinput" name="noguestp" value="1"$noguestp{1}></td>
    </tr><tr>
     <td align=right><b>$admintxt2[76]</b></td>
     <td valign=top><input type="checkbox" class="checkboxinput" name="memguest" value="1"$memguest{1}></td>
    </tr><tr>
     <td align=right><b>$admintxt2[77]</b></td>
     <td valign=top><input type="checkbox" class="checkboxinput" name="gsearch" value="1"$gsearch{1}></td>
    </tr><tr>
     <td align=right><b>$admintxt2[209]</b></td>
     <td valign=top><input type="checkbox" class="checkboxinput" name="gdisable" value="1"$gdisable{1}></td>
    </tr><tr>
     <td colspan="2" class="win"><span class="smalltext"><b>$admintxt2[280]</b></span></td>
    </tr><tr>
     <td align=right><b>$admintxt2[84]</b></td>
     <td valign=top><select name="apic"><option value="0"$apic{0}>$admintxt[3]</option><option value="2"$apic{2}>$admintxt2[313]</option><option value="1"$apic{1}>$admintxt2[314]</option></select></td>
    </tr><tr>
     <td align=right><b>$admintxt2[129]:</b></td>
     <td valign=top><input type="text" class="textinput" name="picwidth" value="$picwidth" size="4"> <b>�</b> <input type="text" class="textinput" name="picheight" value="$picheight" size="4"> <span class="smalltext">$admintxt2[130]</span></td>
    </tr><tr>
     <td align=right><b>$admintxt2[78]</b></td>
     <td valign=top><input type="checkbox" class="checkboxinput" name="invfri" value="1"$invfri{1}></td>
    </tr><tr>
     <td align=right><b>$admintxt2[92]</b></td>
     <td valign=top><input type="checkbox" class="checkboxinput" name="amar" value="1"$amar{1}></td>
    </tr><tr>
     <td align=right><b>$admintxt2[102]</b></td>
     <td valign=top><input type="checkbox" class="checkboxinput" name="logactive" value="1"$logactive{1}></td>
    </tr><tr>
     <td align=right><b>$admintxt2[114]:</b></td>
     <td valign=top><input type="text" class="textinput" name="maxsig" value="$maxsig" size="10"></td>
    </tr><tr>
     <td align=right><b>$admintxt2[85]</b></td>
     <td valign=top><input type="checkbox" class="checkboxinput" name="hmail" value="1"$hmail{1}></td>
    </tr><tr>
     <td align="right"><b>$admintxt2[225]:</b><br><span class="smalltext">$admintxt2[274]</span></td>
     <td valign="top"><select name="hiddenmail"><option value="0"$hms{0}>$admintxt2[275]</option><option value="1"$hms{1}>$admintxt2[276]</option><option value="2"$hms{2}>$admintxt2[277]</option></select></td>
    </tr><tr>
     <td align="right"><b>$admintxt2[232]</b></td>
     <td valign="top"><input type="checkbox" class="checkboxinput" name="nonotify" value="1"$nonotify{1}></td>
    </tr><tr>
     <td align="right"><b>$admintxt2[310]:</b><br><span class="smalltext">$admintxt2[309]</span></td>
     <td valign="top"><select name="hideposts"><option value="0"$hps{0}>$admintxt2[311]</option><option value="2"$hps{2}>$admintxt2[334]</option><option value="1"$hps{1}>$admintxt2[312]</option></select></td>
    </tr><tr>
     <td align="right"><b>$admintxt2[327]</b></td>
     <td valign="top"><input type="checkbox" class="checkboxinput" name="enablerep" value="1" $enablerep{1}></td>
    </tr><tr>
     <td colspan="2" class="win"><span class="smalltext"><b>$admintxt2[281]</b></span></td>
    </tr><tr>
     <td align=right><b>$admintxt2[89]</b></td>
     <td valign=top><input type="checkbox" class="checkboxinput" name="showreg" value="1"$showreg{1}></td>
    </tr><tr>
     <td align=right><b>$admintxt2[100]</b><br><span class="smalltext">$admintxt2[305]</span></td>
     <td valign=top><input type="checkbox" class="checkboxinput" name="quickreg" value="1"$quickreg{1}></td>
    </tr><tr>
     <td align=right><b>$admintxt2[131]:</b><br><span class="smalltext">$admintxt2[132]</span></td>
     <td valign=top><select name="vradmin"><option value="0"$VRA{'0'}>$admintxt2[133]</option><option value="1"$VRA{'1'}>$admintxt2[134]</option><option value="2"$VRA{'2'}>$admintxt2[135]</option></select></td>
    </tr><tr>
     <td align=right><b>$admintxt2[320]</b><br><span class="smalltext">$admintxt2[321]</span></td>
     <td valign=top><input type="checkbox" class="checkboxinput" name="emailadmin" value="1"$emailadmin{1}></td>
    </tr><tr>
     <td colspan="2" class="win"><span class="smalltext"><b>$admintxt2[282]</b></span></td>
    </tr><tr>
     <td align=right><b>$admintxt2[107]</b></td>
     <td valign=top><input type="checkbox" class="checkboxinput" name="pmdisable" value="1"$pmdisable{1}></td>
    </tr><tr>
     <td align=right><b>$admintxt2[108]:</b></td>
     <td valign=top><input type="text" class="textinput" name="pmmaxquota" value="$pmmaxquota" size="10"></td>
    </tr><tr>
     <td colspan="2" class="win"><span class="smalltext"><b>$admintxt2[283]</b></span></td>
    </tr><tr>
     <td align=right><b>$admintxt2[111]:</b></td>
     <td valign=top><input type="text" class="textinput" name="mmpp" value="$mmpp" size="10"></td>
    </tr><tr>
     <td colspan="2" class="win"><span class="smalltext"><b>$admintxt2[284]</b></span></td>
    </tr><tr>
     <td align=right><b>$admintxt2[90]</b></td>
     <td valign=top><input type="checkbox" class="checkboxinput" name="enews" value="1"$enews{1}></td>
    </tr><tr>
     <td align=right><b>$admintxt2[103]</b></td>
     <td valign=top><input type="checkbox" class="checkboxinput" name="showevents" value="1"$showevents{1}></td>
    </tr><tr>
     <td align=right><b>$admintxt2[318]</b><br><span class="smalltext"><img src="$images/warning_sm.gif"> $admintxt2[319]</span></td>
     <td valign=top><input type="checkbox" class="checkboxinput" name="enbdays" value="1"$enbdays{1}></td>
    </tr><tr>
     <td align=right><b>$admintxt2[301]:</b></td>
     <td valign=top><input type="text" class="textinput" name="upevents" value="$upevents" size="4"></td>
    </tr><tr>
     <td align=right><b>$admintxt2[91]</b></td>
     <td valign=top><input type="checkbox" class="checkboxinput" name="sauser" value="1"$sauser{1}></td>
    </tr><tr>
     <td align=right><b>$admintxt2[81]</b><br><span class="smalltext">$admintxt2[82]</span></td>
     <td valign=top><input type="checkbox" class="checkboxinput" name="whereis" value="1"$whereis{1}></td>
    </tr><tr>
     <td colspan="2" class="win"><span class="smalltext"><b>$admintxt2[285]</b></span></td>
    </tr><tr>
     <td align=right><b>$admintxt2[96]</b></td>
     <td valign=top><input type="checkbox" class="checkboxinput" name="showdes" value="1"$showdes{1}></td>
    </tr><tr>
     <td align=right><b>$admintxt2[121]:</b></td>
     <td valign=top><input type="text" class="textinput" name="maxdis" value="$maxdis" size="10"></td>
    </tr><tr>
     <td colspan="2" class="win"><span class="smalltext"><b>$admintxt2[286]</b></span></td>
    </tr><tr>
     <td align=right><b>$admintxt2[87]</b></td>
     <td valign=top><input type="checkbox" class="checkboxinput" name="BCSmile" value="1"$BCSmile{1}></td>
    </tr><tr>
     <td align=right><b>$admintxt2[265]:</b></td>
     <td valign=top><input type="text" class="textinput" name="gmaxsmils" value="$gmaxsmils" size="5"></td>
    </tr><tr>
     <td align=right><b>$admintxt2[88]</b></td>
     <td valign=top><input type="checkbox" class="checkboxinput" name="BCLoad" value="1"$BCLoad{1}></td>
    </tr><tr>
     <td align=right width="50%"><b>$admintxt2[74]</b></td>
     <td valign=top><input type="checkbox" class="checkboxinput" name="upbc" value="1"$upbc{1}></td>
    </tr><tr>
     <td align=right><b>$admintxt2[80]</b></td>
     <td valign=top><input type="checkbox" class="checkboxinput" name="al" value="1"$al{1}></td>
    </tr><tr>
     <td align=right><b>$admintxt2[95]</b></td>
     <td valign=top><input type="checkbox" class="checkboxinput" name="showgender" value="1"$showgender{1}></td>
    </tr><tr>
     <td align=right><b>$admintxt2[79]</b></td>
     <td valign=top><input type="checkbox" class="checkboxinput" name="sppd" value="1"$sppd{1}></td>
    </tr><tr>
     <td align=right><b>$admintxt2[317]</b></td>
     <td valign=top><input type="checkbox" class="checkboxinput" name="showlocation" value="1"$showlocation{1}></td>
    </tr><tr>
     <td align=right><b>$admintxt2[99]</b></td>
     <td valign=top><input type="checkbox" class="checkboxinput" name="sview" value="1"$sview{1}></td>
    </tr><tr>
     <td align=right><b>$admintxt2[83]</b></td>
     <td valign=top><input type="checkbox" class="checkboxinput" name="showmove" value="1"$showmove{1}></td>
    </tr><tr>
     <td align=right><b>$admintxt2[255]</b><br><span class="smalltext">$admintxt2[256]</span></td><!-- Yes, Jennifer I love you.  I never want to see you cry.  I love you.  You're all that's important to me.  I love you.-->
     <td valign=top><input type="checkbox" class="checkboxinput" name="quickreply" value="1"$quickreply{1}></td>
    </tr><tr>
     <td align=right><b>$admintxt2[120]:</b></td>
     <td valign=top><input type="text" class="textinput" name="maxmess" value="$maxmess" size="10"></td>
    </tr><tr>
     <td colspan="2" class="win"><span class="smalltext"><b>$admintxt2[287]</b></span></td>
    </tr><tr>
     <td align=right><b>$admintxt2[97]</b></td>
     <td valign=top><input type="checkbox" class="checkboxinput" name="sall" value="1"$sall{1}></td>
    </tr><tr>
     <td align=right><b>$admintxt2[210]</b></td>
     <td valign=top><input type="checkbox" class="checkboxinput" name="allowrate" value="1"$allowrate{1}></td>
    </tr><tr>
     <td align=right><b>$admintxt2[101]</b></td>
     <td valign=top><input type="checkbox" class="checkboxinput" name="showactive" value="1"$showactive{1}></td>
    </tr><tr>
     <td align=right><b>$admintxt2[122]:</b></td>
     <td valign=top><input type="text" class="textinput" name="totalpp" value="$totalpp" size="10"></td>
    </tr><tr>
     <td align=right><b>$admintxt2[112]:</b></td>
     <td valign=top><input type="text" class="textinput" name="htdmax" value="$htdmax" size="10"></td>
    </tr><tr>
     <td align=right><b>$admintxt2[113]:</b></td>
     <td valign=top><input type="text" class="textinput" name="vhtdmax" value="$vhtdmax" size="10"></td>
    </tr><tr>
     <td colspan="2" class="win"><span class="smalltext"><b>$admintxt2[289]</b></span></td>
    </tr><tr>
     <td align=right><b>$admintxt2[105]</b></td>
     <td valign=top><input type="checkbox" class="checkboxinput" name="reversesum" value="1"$reversesum{1}></td>
    </tr><tr>
     <td align=right><b>$admintxt2[272]:</b><br><span class="smalltext">$admintxt2[273]</span></td>
     <td valign=top><input type="text" class="textinput" name="maxsumc" value="$maxsumc" size="5"></td>
    </tr><tr>
     <td align=right><b>$admintxt2[109]:</b><br><span class="smalltext">$admintxt2[110]</span></td>
     <td valign=top><input type="text" class="textinput" name="iptimeout" value="$iptimeout" size="5"></td>
    </tr><tr>
     <td align=right><b>$admintxt2[266]:</b></td>
     <td valign=top><input type="text" class="textinput" name="maxmesslth" value="$maxmesslth" size="10"></td>
    </tr><tr>
     <td align=right><b>$admintxt2[330]:</b><br><span class="smalltext">$admintxt2[331]</span></td>
     <td valign=top><input type="text" class="textinput" name="modifytime" value="$modifytime" size="10"></td>
    </tr><tr>
     <td colspan="2" class="win"><span class="smalltext"><b>$admintxt2[290]</b></span></td>
    </tr><tr>
     <td align=right><b>$admintxt2[219]</b></td>
     <td valign=top><input type="checkbox" class="checkboxinput" name="html" value="1"$html{1}></td>
    </tr><tr>
     <td align=right valign="top"><b>$admintxt2[220]:</b><br><span class="smalltext">$admintxt2[221]</span></td>
     <td valign=top><textarea name="htmls" wrap="virtual" rows="3" cols="50">$tags</textarea></td>
    </tr><tr>
     <td colspan="2" class="win"><span class="smalltext"><b>$admintxt2[291]</b></span></td>
    </tr><tr>
     <td align=right><b>$admintxt2[86]</b></td>
     <td valign=top><input type="checkbox" class="checkboxinput" name="slpoller" value="1"$slpoller{1}></td>
    </tr><tr>
     <td align=right><b>$admintxt2[239]</b></td>
     <td valign=top><input type="checkbox" class="checkboxinput" name="polltop" value="1"$polltop{1}></td>
    </tr><tr>
     <td align=right><b>$admintxt2[240]:</b></td>
     <td valign=top><input type="text" class="textinput" name="pollops" value="$pollops" size="4" maxlength="4"></td>
    </tr>
   </table>
  </td>
 </tr>
EOT
	} if($l eq 'log' || $l eq 'all') {
		push(@checks,('eclick','uextlog','kelog','logip','hideelog','hideclog'));
		&CheckBoxes;
		$logdays = $logdays || 365;

		$ebout .= <<"EOT";
 <tr>
  <td class="catbg"><table cellpadding="0" cellspacing="0"><tr><td><img src="$images/clicklog.gif"></td><td class="catbgtext"><b>&nbsp;$admintxt2[25]</b></td></tr></table></td>
 </tr><tr>
  <td class="win"><span class="smalltext">$admintxt2[26]</span></td>
 </tr><tr>
  <td class="win2">
   <table cellspacing="0" cellpadding="2" width="100%">
    <tr>
     <td align=right width="50%"><b>$admintxt2[136]</b></td>
     <td valign=top><input type="checkbox" class="checkboxinput" name="eclick" value="1"$eclick{1}></td>
    </tr><tr>
     <td align=right><b>$admintxt2[137]:</b><br><span class="smalltext">$admintxt2[138]</span></td>
     <td valign=top><input type="text" class="textinput" name="logcnt" value="$logcnt" size="10"></td>
    </tr><tr>
     <td colspan="2" class="win"><span class="smalltext"><b>$admintxt2[300]</b></span></td>
    </tr><tr>
     <td align=right><b>$admintxt2[139]</b><br><span class="smalltext">$admintxt2[248]</span></td>
     <td valign=top><input type="checkbox" class="checkboxinput" name="uextlog" value="1"$uextlog{1}></td>
    </tr><tr>
     <td align=right><b>$admintxt2[140]</b><br><span class="smalltext">$admintxt2[249]</span></td>
     <td valign=top><input type="checkbox" class="checkboxinput" name="kelog" value="1"$kelog{1}></td>
    </tr><tr>
     <td align=right><b>$admintxt2[141]</b><br><span class="smalltext">$admintxt2[250]</span></td>
     <td valign=top><input type="checkbox" class="checkboxinput" name="logip" value="1"$logip{1}></td>
    </tr><tr>
     <td colspan="2" class="win"><span class="smalltext"><b>$admintxt2[299]</b></span></td>
    </tr><tr>
     <td align=right><b>$admintxt2[142]:</b><br><span class="smalltext">$admintxt2[241]</span></td>
     <td valign=top><input type="text" class="textinput" name="logdays" value="$logdays" size="5"></td>
    </tr><tr>
     <td align="right"><b>$admintxt2[243]:</b><br><span class="smalltext">$admintxt2[242]</span></td>
     <td valign="top"><input type="text" class="textinput" name="activeuserslog" value="$activeuserslog" size="5"></td>
    </tr><tr>
     <td colspan="2" class="win"><span class="smalltext"><b>$admintxt2[306]</b></span></td>
    </tr><tr>
     <td align=right><b>$admintxt2[307]</b></td>
     <td valign=top><input type="checkbox" class="checkboxinput" name="hideclog" value="1"$hideclog{1}></td>
    </tr><tr>
     <td align=right><b>$admintxt2[308]</b></td>
     <td valign=top><input type="checkbox" class="checkboxinput" name="hideelog" value="1"$hideelog{1}></td>
    </tr>
   </table>
  </td>
 </tr>
EOT
	} if($l eq 'mods' || $l eq 'all') {
		push(@checks,('yabbconver'));
		&CheckBoxes;

		@selected = split(",",$newsboard);
		foreach(@selected) { $sel{$_} = " selected"; }
		$boards2 = qq~<select name="newsboard" multiple size="4">~;
		foreach(@catbase) {
			($t,$t,$access,$bhere) = split(/\|/,$_);
			@bdsh = split("/",$bhere);
			foreach $hh (@bdsh) {
				foreach $bbase (@boardbase) {
					($bid,$t,$t,$name) = split("/",$bbase);
					if($bid ne $hh) { next; }
					$boards2 .= qq~<option value="$bid"$sel{$bid}>$name</option>~;
					last;
				}
			}
		}
		$boards2 .= qq~</select>~;

		$ebout .= <<"EOT";
 <tr>
  <td class="catbg"><table cellpadding="0" cellspacing="0"><tr><td><img src="$images/set_mod.gif"></td><td class="catbgtext"><b>&nbsp;$admintxt2[31]</b></td></tr></table></td>
 </tr><tr>
  <td class="win"><span class="smalltext">$admintxt2[32]</span></td>
 </tr><tr>
  <td class="win2">
   <table cellspacing=0 cellpadding=2 width=100%>
    <tr>
     <td colspan="2" class="win"><span class="smalltext"><b>$admintxt2[333]</b></span></td>
    </tr><tr>
     <td align=right width="50%"><b>$admintxt2[150]</b><br><span class="smalltext">$admintxt2[151]</span></td>
     <td valign=top><input type="checkbox" class="checkboxinput" name="_dis_" value="0"$yabbconver{1} disabled><input type="hidden" name="yabbconver" value="$yabbconver"></td>
    </tr><tr>
     <td colspan="2" class="win"><span class="smalltext"><b>$admintxt2[29]</b></span></td>
    </tr><tr>
     <td align=right width="50%" valign="top"><b>$admintxt2[147]:</b><br><span class="smalltext">$admintxt2[148]</span></td>
     <td valign=top>$boards2</td>
    </tr><tr>
     <td align=right width="50%" valign="top"><b>$admintxt2[149]:</b></td>
     <td valign=top><input type="text" class="textinput" value="$newsshow" name="newsshow" size="5"></td>
    </tr><tr>
     <td align=right width="50%" valign="top"><b>$admintxt2[222]:</b></td>
     <td valign=top><input type="text" class="textinput" value="$newslength" name="newslength" size="5"></td>
    </tr>
   </table>
  </td>
 </tr>
EOT
	}
	$ebout .= <<"EOT";
 <tr>
  <td class="win" align=center><input type="submit" class="button" name="submit" value="$admintxt2[152]"></td></form>
 </tr>
</table>
EOT
	&footerA;
	exit;
}

sub CheckBoxes {
	foreach(@checks) { ${$_}{${$_}} = ' checked'; }
	@checks = (); # Clear cache
}

sub Settings3 {
	if($l eq 'blk' || $l eq 'all') {
		push(@switches,('maintance','lockout','noguest','creg','serload'));
		$maintancer = $FORM{'maintancer'} || $admintxt2[153];
		$gzipen = $FORM{'gzipen'} || 0;
	} if($l eq 'dir' || $l eq 'all') {
		while (($name,$value) = each(%FORM)) {
			$value =~ s~\\~/~gi;
			$FORM{"$name"} = $value;
		}
		$root = $FORM{'root'} || ".";
		$code = $FORM{'code'} || "./Code";
		$boards = $FORM{'boards'} || "./Boards";
		$prefs = $FORM{'prefs'} || "./Prefs";
		$members = $FORM{'members'} || "./Members";
		$messages = $FORM{'messages'} || "./Messages";
		$theme = $FORM{'theme'} || "./Themes";
		$images = $FORM{'images'} || "/";
		$simages = $FORM{'simages'} || "/";
		$avsurl = $FORM{'avsurl'} || $ENV{'DOCUMENT_ROOT'};
		$avdir = $FORM{'avdir'};
		$realurl = $FORM{'rurl'} || "/";
		$bcloc = $FORM{'bcloc'} || "/bc.js";
		$helpdesk = $FORM{'helpdesk'};
		$newsloc = $FORM{'newsloc'} || '/news.js';
		$languages = $FORM{'languages'} || './Languages';
		$templates = $FORM{'templates'} || "$ENV{'DOCUMENT_ROOT'}/blahdocs/template";
		$templatesu = $FORM{'templatesu'} || "/blahdocs/template";
		$modsdir = $FORM{'modsdir'} || "$root/Mods";
	} if($l eq 'ess' || $l eq 'all') {
		push(@switches,'mailauth');
		$regto = $FORM{'regto'} || $username;
		$eadmin = $FORM{'eadmin'} || "unknown\@unknown.unknown";
		$cookpw = $FORM{'cookpw'} || "BlahPW";
		$cooknme = $FORM{'cooknme'} || "BlahNAME";
		$mailuse = $FORM{'mailuse'} || 1;
		$smaill = $FORM{'smaill'};
		$mailhost = $FORM{'mailhost'};
		$mailusername = $FORM{'mailusername'};
		$mailpassword = $FORM{'mailpassword'};
		$emailsig = Format($FORM{'emailsig'});
		$emailsig =~ s/\*//sgi;
	} if($l eq 'upl' || $l eq 'all') {
		push(@switches,('uallow','ntsys','avupload'));
		$uploaddir = $FORM{'uploaddir'};
		$uploadurl = $FORM{'uploadurl'};
		$allowedext = $FORM{'allowedext'};
		$maxsize = $FORM{'maxsize'};
		$maxsize2 = $FORM{'maxsize2'};
		if($maxsize2 eq '') { $maxsize2 = $maxsize; }
	} if($l eq 'bro' || $l eq 'all') {
		push(@switches,('enablerep','emailadmin','enbdays','showlocation','html','ensids','maxmesslth','maxsumc','redirectfix','quickreply','hiddenmail','gtzone','pollops','polltop','nonotify','allowrate','pmdisable','gdisable','reversesum','showevents','logactive','showactive','quickreg','sview','showtheme','sall','showdes','gsearch','memguest','showgender','iptimeout','btod','mtext','amar','sauser','enews','picwidth','picheight','timedis','datedis','gtoff','maxsig','pmmaxquota','showreg','BCLoad','BCSmile','slpoller','hmail','apic','showmove','whereis','al','sppd','invfri','noguestp','upbc'));
		$maxmess = $FORM{'maxmess'} || '15';
		$maxdis = $FORM{'maxdis'} || '20';
		$mbname = $FORM{'mbname'};
		$mbname =~ s/\*//sgi; # Error prevention
		$vhtdmax = $FORM{'vhtdmax'} || 50;
		$htdmax = $FORM{'htdmax'} || 25;
		$mmpp = $FORM{'mmpp'} || 25;
		$totalpp = $FORM{'totalpp'} || 20;
		$vradmin = $FORM{'vradmin'};
		$languagep = $FORM{'lng'} || 'English';
		$nocomma = $FORM{'nocomma'};
		$gmaxsmils = $FORM{'gmaxsmils'} || 200;
		$upevents = $FORM{'upevents'} < 28 ? $FORM{'upevents'} : 0;
		$hideposts = $FORM{'hideposts'} || 0;
		@htmls = split(',',$FORM{'htmls'});
		$loginfailtime = $FORM{'loginfailtime'} || 0;
		$maxattempts = $FORM{'maxattempts'} || 0;
		$modifytime = $FORM{'modifytime'} || 0;
	} if($l eq 'log' || $l eq 'all') {
		push(@switches,('eclick','uextlog','kelog','logip','hideelog','hideclog'));
		$logcnt = $FORM{'logcnt'} || 1440;
		$logdays = $FORM{'logdays'} || 365;
		$activeuserslog = $FORM{'activeuserslog'} || 15;
	} if($l eq 'mods' || $l eq 'all') { # Board Modifications Settings Below ...
		push(@switches,'yabbconver');
		push(@switches,'newsshow');
		$newsboard = $FORM{'newsboard'};
		$newslength = $FORM{'newslength'} || 2000;
	}

	if($URL{'a'} eq 'encrypt') {
		$FORM{'yabbconver'} = 1;
		push(@switches,'yabbconver');
	}

	$rurl =~ s~/Blah.pl\?~~gsi;

	foreach(@htmls) {
		$_ =~ s/'//gsi;
		if($_ eq '') { next; }
		$htmls .= "'$_',";
	}
	$htmls =~ s/,\Z//gsi;
	foreach(@switches) { ${$_} = $FORM{$_} || 0; }

	$printtofile = <<"EOT";
####################################################
# E-Blah Bulliten Board Systems           Platinum #
####################################################
# This file contains setup information vital to    #
# running e-blah. Read Blah.pl for full headers.   #
# You should not edit this file manualy. Use Admin #
# Center on your Forum to change these settings.   #
####################################################

\$bversion = $theblahver;      # Version

####### Directories Sets #######
\$root = "$root";
\$code = "$code";
\$boards = "$boards";
\$prefs = "$prefs";
\$members = "$members";
\$messages = "$messages";
\$theme = "$theme";
\$modsdir = "$modsdir";
\$images = "$images";
\$simages = "$simages";
\$avsurl = "$avsurl";
\$avdir = "$avdir";
\$realurl = "$realurl";
\$bcloc = "$bcloc";
\$helpdesk = "$helpdesk";
\$languages = "$languages";
\$newsloc = "$newsloc";
\$templates = "$templates";
\$templatesu = "$templatesu";

#######  Essential Sets  #######
\$regto = qq*$regto*;
\$eadmin = q*$eadmin*;
\$cookpw = qq*$cookpw*;
\$cooknme = qq*$cooknme*;
\$mailuse = $mailuse;
\$smaill = "$smaill";
\$mailhost = "$mailhost";
\$emailsig = q*$emailsig*;
\$mailauth = $mailauth;
\$mailusername = '$mailusername';
\$mailpassword = '$mailpassword';

#######   Board Locks    #######
\$maintance = $maintance;
\$maintancer = qq~$maintancer~;
\$lockout = $lockout;
\$noguest = $noguest;
\$creg = $creg;
\$serload = $serload;
\$gzipen = $gzipen;

#######   Upload Prefs   #######
\$uallow = $uallow;
\$avupload = $avupload;
\$ntsys = $ntsys;
\$uploaddir = "$uploaddir";
\$uploadurl = "$uploadurl";
\$allowedext = "$allowedext";
\$maxsize = "$maxsize";
\$maxsize2 = "$maxsize2";

#######    Main Prefs    #######
\$upbc = $upbc;
\$noguestp = $noguestp;
\$invfri = $invfri;
\$sppd = $sppd;
\$al = $al;
\$whereis = $whereis;
\$showmove = $showmove;
\$apic = $apic;
\$hmail = $hmail;
\$slpoller = $slpoller;
\$BCSmile = $BCSmile;
\$BCLoad = $BCLoad;
\$showreg = $showreg;
\$pmmaxquota = "$pmmaxquota";
\$maxsig = "$maxsig";
\$gtoff = '$gtoff';
\$datedis = $datedis;
\$timedis = $timedis;
\$maxmess = $maxmess;
\$maxdis = $maxdis;
\$mbname = q*$mbname*;
\$picheight = "$picheight";
\$picwidth = "$picwidth";
\$btod = $btod;
\$mtext = $mtext;
\$amar = $amar;
\$sauser = $sauser;
\$mmpp = $mmpp;
\$iptimeout = $iptimeout;
\$vhtdmax = $vhtdmax;
\$htdmax = $htdmax;
\$enews = $enews;
\$showgender = $showgender;
\$totalpp = $totalpp;
\$memguest = $memguest;
\$gsearch = $gsearch;
\$showdes = $showdes;
\$sall = $sall;
\$vradmin = $vradmin;
\$showtheme = $showtheme;
\$sview = $sview;
\$quickreg = $quickreg;
\$showactive = $showactive;
\$logactive = $logactive;
\$showevents = $showevents;
\$reversesum = $reversesum;
\$gdisable = $gdisable;
\$redirectfix = $redirectfix;
\$pmdisable = $pmdisable;
\$languagep = "$languagep";
\$allowrate = $allowrate;
\$nocomma = '$nocomma';
\$html = $html;
\@htmls = ($htmls);
\$hiddenmail = $hiddenmail;
\$nonotify = $nonotify;
\$polltop = $polltop;
\$pollops = "$pollops";
\$gtzone = "$gtzone";
\$quickreply = $quickreply;
\$gmaxsmils = $gmaxsmils;
\$maxmesslth = $maxmesslth;
\$maxsumc = "$maxsumc";
\$upevents = $upevents;
\$hideposts = $hideposts;
\$ensids = $ensids;
\$showlocation = $showlocation;
\$enbdays = $enbdays;
\$emailadmin = $emailadmin;
\$maxattempts = $maxattempts;
\$loginfailtime = $loginfailtime;
\$enablerep = $enablerep;
\$modifytime = $modifytime;

#######     Logging      #######
\$eclick = $eclick;
\$uextlog = $uextlog;
\$kelog = $kelog;
\$logip = $logip;
\$logcnt = "$logcnt";
\$logdays = $logdays;
\$activeuserslog = $activeuserslog;
\$hideclog = $hideclog;
\$hideelog = $hideelog;

#######       News       #######
\$newsboard = "$newsboard";
\$newsshow = $newsshow;
\$newslength = $newslength;

#######       MODS       #######
\$yabbconver = $yabbconver;
1;
EOT
	$printtofile =~ s~\$(.*?) \= ;~\$$1 \= 0;~sig; # Check for blanks (error handeling)

	# Print and redirect
	fopen(FILE,"+>$root/Settings.pl");
	print FILE $printtofile;
	fclose(FILE);
	redirect("$scripturl,v=admin,r=3");
}

sub RemoveSpectators {
	if($URL{'p'} == 2) {
		fopen(FILE,"$members/List.txt");
		@list = <FILE>;
		fclose(FILE);
		chomp @list;
		if($FORM{'days'} !~ /[0-9]/) { &error($admintxt2[155]); }
		if($FORM{'posts'} !~ /[0-9]/) { &error($admintxt2[156]); }
		$time = time;
		$days = $time-($FORM{'days'}*86400);
		$active = $logactive ? $time-($FORM{'active'}*86400) : 1;

		$title = $admintxt2[161];
		&headerA;
		$ebout .= <<"EOT";
<table class="border" cellpadding="4" cellspacing="1" align="center" width="700">
 <tr>
  <td class="titlebg"><b><img src="$images/mem_main.gif"> $title</b></td>
 </tr><tr>
  <td class="win"><span class="smalltext">$admintxt2[264]</span></td>
 </tr><tr><form action="$scripturl,v=admin,a=delspec,p=3" method="post" name="post">
  <td class="win2">
   <table width="100%">
    <tr>
     <td width="30%"><span class="smalltext"><b>$admintxt2[263]</b></span></td>
     <td width="25%" align="center"><span class="smalltext"><b>$admintxt2[262]</b></span></td>
     <td width="25%"><span class="smalltext"><b>$admintxt2[261]</b></span></td>
     <td width="20%" align="center"><span class="smalltext"><b>$admintxt2[203]</b></span></td>
    </tr>
EOT
		$counts = 0;
		foreach (@list) {
			&loaduser($_);
			$deletea = $userset{$_}->[14]-$days;
			$deleteb = $userset{$_}->[28]-$active;
			$fnd = 0;
			$posts = ($userset{$_}->[3]-$FORM{'posts'});
			if($deletea <= 0 && $deleteb <= 0) {
				if($posts <= 0 && $username ne $_) {
					if($username ne $_) {
						if($userset{$_}->[28]) { $lastact = get_date($userset{$_}->[28]); }
							else { $lastact = $gtxt{'13'}; }
						$ebout .= <<"EOT";
<tr>
 <td><span class="smalltext">$userset{$_}->[1] &nbsp; (<a href="$scripturl,v=memberpanel,a=view,u=$_" target="_blank"><i>$_</i></a>)</span></td>
 <td align="center"><span class="smalltext">$userset{$_}->[3]</span></td>
 <td><span class="smalltext">$lastact</span></td>
 <td align="center"><input type="checkbox" class="checkboxinput" name="$counts" value="$_" checked></td>
</tr>
EOT
					}
					++$counts;
				}
			}
		}
		if($counts == 0) {
			$ebout .= <<"EOT";
<tr>
 <td colspan="4" align="center"><span class="smalltext"><br>$admintxt2[260]<br><br></span></td>
</tr>
EOT
		}
		$ebout .= <<"EOT";
   </table>
  </td>
 </tr><tr>
  <td class="win" align="center" colspan="2"><input type="hidden" name="cnt" value="$counts"><input type="submit" class="button" value=" $admintxt2[168] "></td>
 </tr>
</table>
EOT
		&footerA;
		exit;
	} elsif($URL{'p'} == 3) {
		for($dsp = 0; $dsp < $FORM{'cnt'}; ++$dsp) {
			$deldata = $FORM{$dsp};
			if($deldata eq '') { next; }
			loaduser($deldata);
			if($userset{$deldata}->[32]) {
				$userset{$deldata}->[5] =~ s/$uploadurl\///gsi;
				unlink("$uploaddir/$userset{$deldata}->[5]");
			}
			unlink("$members/$deldata.dat","$members/$deldata.vlog","$members/$deldata.lo","$members/$deldata.log","$members/$deldata.pm","$members/$deldata.prefs","$members/$deldata.msg");
		}
		CoreLoad('Admin1');
		&Remem;
		redirect("$scripturl,v=admin,r=3");
	}

	$title = $admintxt2[161];
	&headerA;
	$ebout .= <<"EOT";
<table class="border" cellpadding="4" cellspacing="1" align="center" width="500">
 <tr>
  <td class="titlebg"><b><img src="$images/mem_main.gif"> $title</b></td>
 </tr><tr>
  <td class="win"><span class="smalltext">$admintxt2[162]</span></td>
 </tr><form action="$scripturl,v=admin,a=delspec,p=2" method="post" name="post"><tr>
  <td class="catbg"><span class="smalltext"><b>$admintxt2[163]</b></span></td>
 </tr><tr>
  <td class="win2">
   <table cellpadding="3" cellspacing="0">
    <tr>
     <td valign="top"><input type="text" class="textinput" value="60" name="days" size="4"></td>
     <td><b>$admintxt2[164]</b></td>
    </tr><tr>
     <td valign="top"><input type="text" class="textinput" value="30" name="active" size="4"></td>
     <td><b>$admintxt2[165]</b></td>
    </tr><tr>
     <td valign="top"><input type="text" class="textinput" value="5" name="posts" size="4"></td>
     <td><b>$admintxt2[166]</b></td>
    </tr>
   </table>
  </td>
 </tr><tr>
  <td class="win" align="center" colspan="2"><input type="submit" class="button" value=" $admintxt2[259] "></td>
 </tr>
</table>
EOT
	&footerA;
	exit;
}

sub BanLog {
	if($URL{'p'}) {
		unlink("$prefs/NoAccess.txt");
		redirect("$scripturl,v=admin,r=3");
	}
	$title = $admintxt2[170];
	&headerA;
	$ebout .= <<"EOT";
<script language="javascript">
<!-- //
function clear(url) {
 if(window.confirm("$admintxt2[169]")) { location = url; }
}
// -->
</script>
<table class="border" cellpadding="3" cellspacing="1" align="center" width="650">
 <tr>
  <td class="titlebg" colspan="3"><b><img src="$images/ip.gif"> $title</b></td>
 </tr><tr>
  <td class="win" colspan="3"><span class="smalltext">$admintxt2[171]</span></td>
 </tr><tr>
  <td class="catbg" align="center"><span class="smalltext"><b>$gtxt{'18'}</b></span></td>
  <td class="catbg" align="center"><span class="smalltext"><b>$admintxt2[173]</b></span></td>
  <td class="catbg" align="center"><span class="smalltext"><b>$admintxt2[174]</b></span></td>
 </tr>
EOT
	fopen(FILE,"$prefs/NoAccess.txt");
	@nac = <FILE>;
	fclose(FILE);
	chomp @nac;
	foreach(@nac) {
		($user,$ip,$date) = split(/\|/,$_);
		$datetime = get_date($date);
		loaduser($user);
		if($userset{$user}->[1]) { $userset{$user}->[1] = qq~<a href="$scripturl,v=memberpanel,a=view,u=$user">$userset{$user}->[1]</a>~; } else { $userset{$user}->[1] = $user; }
		$ebout .= <<"EOT";
 <tr>
  <td class="win" align="center">$ip</td>
  <td class="win2" align="center">$userset{$user}->[1]</td>
  <td class="win" align="center">$datetime</td>
 </tr>
EOT
	}
	if($nac[0]) {
		$ebout .= <<"EOT";
 <tr>
  <td class="catbg" align="center" colspan="3"><span class="smalltext"><b><a href="javascript:clear('$scripturl,v=admin,a=banlog,p=clear')">$admintxt2[175]</a></b></span></td>
 </tr>
EOT
	} else {
		$ebout .= <<"EOT";
 <tr>
  <td class="win" align="center" colspan="3"><b>$admintxt2[176]</b></td>
 </tr>
EOT
	}
	$ebout .= "</table>";

	&footerA;
	exit;
}

sub BanUser {
	fopen(FILE,"$prefs/BanList.txt");
	@blist = <FILE>;
	fclose(FILE);
	chomp @blist;
	if($URL{'g'}) { &BanUser2; }
	push(@groups,$gtxt{'13'});
	$BAN{"$gtxt{'13'}"} = 0;
	foreach(@blist) {
		($ipaddy,$banlimit,$bantime,$bangrp) = split(/\|/,$_);
		if($bangrp eq '') { $bangrp = $gtxt{'13'}; }
		++$BAN{$bangrp};
		if($BAN{$bangrp} == 1 && $bangrp ne $gtxt{'13'}) { push(@groups,$bangrp); }
	}
	$title = $admintxt2[179];
	&headerA;
	$ebout .= <<"EOT";
<script language="javascript">
<!-- //
function del(grp) {
 if(window.confirm("$admintxt2[180]")) { location = '$scripturl,v=admin,a=ban,g='+grp+',p=del'; }
}
// -->
</script>
<table class="border" cellpadding="4" cellspacing="1" align="center" width="500">
 <tr>
  <td class="titlebg" colspan="3"><b><img src="$images/mem_main.gif"> $title</b></td>
 </tr><tr>
  <td class="win"><span class="smalltext">$admintxt2[181]</span></td>
 </tr><tr>
  <td class="win2"><table width="100%">

EOT
	foreach(@groups) {
		++$count;
		$name = $_;
		$_ =~ s/ /_/gsi;
		if($name ne $gtxt{'13'}) { $delete = qq~<br><b><a href="javascript:del('$_')">$admintxt2[178]</a></b>~; }
		$ebout .= <<"EOT";
   <tr>
    <td width="5"><b>$count.</b></td>
    <td><b><a href="$scripturl,v=admin,a=ban,g=$_">$name</a></b></td>
   </tr><tr>
    <td>&nbsp;</td>
    <td><span class="smalltext"><b>$admintxt2[177]:</b> $BAN{$name}$delete</span></td>
   </tr>
EOT
	}
	$ebout .= <<"EOT";
  </table></td>
 </tr>
</table>
EOT
	&footerA;
	exit;
}

sub BanUser2 {
	if($URL{'p'} eq 'save' || $URL{'p'} eq 'del') { &BanUser3; }
	$title = $admintxt2[179];
	&headerA;
	$ebout .= <<"EOT";
<table class="border" cellpadding="4" cellspacing="1" align="center" width="600">
 <tr><form action="$scripturl,v=admin,a=ban,g=$URL{'g'},p=save" method="post" name="post">
  <td class="titlebg" colspan="4"><b><img src="$images/mem_main.gif"> $title</b></td>
 </tr><tr>
  <td class="win" colspan="4"><span class="smalltext">$admintxt2[182]</span></td>
 </tr><tr>
  <td class="catbg" colspan="4">
   <table cellpadding="0" cellspacing="0" width="100%">
    <td width="25%" align="center"><span class="smalltext"><b>$admintxt2[258]</b></span></td>
    <td width="25%"><b>$admintxt2[183]</b></td>
    <td width="25%" align="center"><span class="smalltext"><b>$admintxt2[184]</b></span></td>
    <td width="25%"><b>$admintxt2[185]</b></td>
   </table>
  </td>
 </tr><tr>
  <td class="win2" colspan="4">
   <table cellpadding="2" cellspacing="0" width="100%">
    
EOT
	if($URL{'g'} =~ /_/) { $URL{'g2'} =~ s/_/ /gsi; }

	$counter = 0;
	push(@blist,"|||$URL{'g'}");
	foreach(@blist) {
		($ipaddy,$banlimit,$bantime,$bangrp) = split(/\|/,$_);
		if($bangrp eq '') { $bangrp = $gtxt{'13'}; }
		if($bangrp ne $URL{'g'} && $bangrp ne $URL{'g2'}) { next; }
		$bantime = $bantime eq '' ? $admintxt2[186] : &get_date($bantime);
		$sel{$banlimit} = " selected";
		$ebout .= <<"EOT";
    <tr>
     <td width="25%"><input type="text" class="textinput" name="$counter-ipaddy" value="$ipaddy"></td>
     <td width="25%"><select name="$counter-banlimit">
<option value="forever">forever</option>
<option value="1"$sel{1}>1 $admintxt2[187]</option>
<option value="3"$sel{3}>3 $admintxt2[187]</option>
<option value="7"$sel{7}>1 $gtxt{'39'}</option>
<option value="30"$sel{30}>1 $gtxt{'40'}</option>
</select></a></td>
     <td width="25%" align="center"><span class="smalltext">$bantime</span></td>
     <td width="25%"><input type="text" class="textinput" name="$counter-bangrp" value="$bangrp"></td>
    </tr>
EOT
		$sel{$banlimit} = '';
		++$counter;
	}
	$ebout .= <<"EOT";
   </table>
  </td>
 </tr><tr>
  <td class="win" colspan="4"><input type="hidden" name="vals" value="$counter"><table width="100%"><tr><td><span class="smalltext"><b><a href="$scripturl,v=admin,a=ban">$admintxt2[188]</a></b></span></td><td align="right"><input type="submit" class="button" value=" $admintxt2[152] "></td></tr></table></td>
 </form></tr>
</table>
EOT
	&footerA;
	exit;
}

sub BanUser3 {
	$xURLg = $URL{'g'};
	$URL{'g'} =~ s/_/ /gsi;
	foreach(@blist) { # Save banned list ...
		($ipaddy,$banlimit,$bantime,$bangrp) = split(/\|/,$_);
		if($bangrp eq '') { $bangrp = $gtxt{'13'}; }
		if(($bangrp ne $URL{'g'} && $bangrp ne $xURLg) && !$prev{$ipaddy}) { $all .= "$_\n"; $prev{$ipaddy} = 1; }
		$BANA{$ipaddy} = $banlimit;
		$BANB{$ipaddy} = $bantime;
	}

	if($URL{'p'} eq 'save') {
		for($x = 0; $x < $FORM{'vals'}; $x++) {
			$updatetime = '';
			$ipaddy = $FORM{"$x-ipaddy"};
			if(!$ipaddy || $prev{$ipaddy}) { next; }
			$FORM{'$x-bangrp'} =~ s/ /_/g;
			$FORM{'$x-bangrp'} =~ s/[#%+,\\\/:?"<>'|@^\$\&~'\)\(\]\[\;{}!`=-]//g;

			if($FORM{"$x-banlimit"} ne $BANA{$ipaddy}) { $updatetime = time+($FORM{"$x-banlimit"}*86400); }
			elsif($BANB{$ipaddy}) { $updatetime = $BANB{$ipaddy}; }
			if($FORM{"$x-banlimit"} eq 'forever') { $FORM{"$x-banlimit"} = ''; $updatetime = ''; }

			$all .= qq~$FORM{"$x-ipaddy"}|$FORM{"$x-banlimit"}|$updatetime|$FORM{"$x-bangrp"}\n~;
			$prev{$ipaddy} = 1;
		}
	}

	fopen(FILE,">$prefs/BanList.txt");
	print FILE $all;
	fclose(FILE);
	redirect("$scripturl,v=admin,a=ban");
}

sub Censor {
	if($URL{'s'}) { &Censor2; }
	$title = $admintxt2[200];
	&headerA;
	$ebout .= <<"EOT";
<script language="javascript">
<!-- //
function clear(url) {
 if(window.confirm("$admintxt2[199]")) { location = url; }
}
// -->
</script>
<table class="border" cellpadding="4" cellspacing="1" width="450" align="center">
 <tr><form action="$scripturl,v=admin,a=censor,s=1" method="post" name="post"><input type="hidden" value="ok" name="ok">
  <td class="titlebg" colspan="3"><b><img src="$images/ban.gif"> $title</b></td>
 </tr><tr>
  <td class="catbg"><span class="smalltext"><b>$admintxt2[201]</b></span></td>
  <td class="catbg"><span class="smalltext"><b>$admintxt2[202]</b></span></td>
  <td class="catbg" width="60" align="center"><span class="smalltext"><b>$admintxt2[203]</b></span></td>
 </tr>
EOT
	fopen(FILE,"$prefs/Censor.txt");
	@censor = <FILE>;
	fclose(FILE);
	chomp @censor;
	$counter = 0;
	foreach(@censor) {
		($censor,$change) = split(/\|/,$_);
		$ebout .= <<"EOT";
 <tr>
  <td class="win" align="center"><input type="text" class="textinput" name="censor_$counter" value="$censor"></td>
  <td class="win2" align="center"><input type="text" class="textinput" name="change_$counter" value="$change"></td>
  <td class="win" align="center"><input type="checkbox" class="checkboxinput" name="del_$counter" value="1"></td>
 </tr>
EOT
		++$counter;
	}
	if(!$censor[0]) {
		$ebout .= <<"EOT";
 <tr>
  <td class="win2" colspan="3" align="center"><span class="smalltext"><br>$admintxt2[204]<br><br></span></td>
 </tr>
EOT
	}
	$ebout .= <<"EOT";
 <tr>
  <td class="catbg" colspan="3"><b>$admintxt2[205]</b></td>
 </tr><tr>
  <td class="win" align="center"><b><input type="text" class="textinput" name="censor_new" value=""></td>
  <td class="win" align="center" colspan="2"><b><input type="text" class="textinput" name="change_new" value=""></td>
 </tr><tr>
  <td class="win2" colspan="3" align="center"><input type="hidden" name="cnt" value="$counter"><input type="submit" class="button" name="submit" value=" $admintxt2[152] "></td>
 </tr></form>
</table>
EOT
	&footerA;
	exit;
}

sub Censor2 {
	while(($in,$out) = each(%FORM)) {
		$output = Format($out);
		$FORM{$in} = $output;
	}
	fopen(FILE,"+>$prefs/Censor.txt");
	for($i = 0; $i < $FORM{'cnt'}; $i++) {
		if(!$FORM{"del_$i"}) { print FILE qq~$FORM{"censor_$i"}|$FORM{"change_$i"}\n~; }
	}
	if($FORM{'censor_new'} ne '') { print FILE "$FORM{'censor_new'}|$FORM{'change_new'}\n"; }
	fclose(FILE);

	redirect("$surl,v=admin,a=censor");
}
1;