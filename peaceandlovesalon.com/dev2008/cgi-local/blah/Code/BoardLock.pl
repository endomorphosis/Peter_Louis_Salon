################################################
# E-Blah Bulliten Board Systems       Platinum #
################################################
# Copyright (c) 2001 - 2004 e-blah!            #
# All Rights Reserved.                         #
################################################

CoreLoad('BoardLock',1);

sub MainLO {
	if($URL{'v'} eq 'shownews') { return; }
	if(-e("$root/Maintance.lock")) { require("$root/Maintance.lock"); }
	if($URL{'id'} ne '' && $URL{'v'} eq 'invite') { CoreLoad('Invite'); &LinkMeUp; }
	if($maintance && $settings[4] ne 'Administrator') { &Maintain; }
	if($lockout) {
		fopen(FILE,"$members/$username.lo");
		@lock = <FILE>;
		fclose(FILE);
		chomp @lock;
		if($lock[0] eq 'ALLOW') { 1; }
		elsif($lock[0] eq 'TEMP') {
			($for,$long) = split(/\|/,$lock[1]);
			$time = $for*60;
			$maxtime = $long+$time;
			$thistime = time;
			$allowed = $maxtime-$thistime;
			if($allowed > 0) { 1; }
			elsif($URL{'v'} ne 'login' && $URL{'p'} != 2) { &LockOut; }
		}
		elsif($URL{'v'} eq 'login' && $URL{'p'} == 2) { return; }
		else { &LockOut; }
	}
	elsif($noguest && ($username eq 'Guest' || $settings[1] eq '')) { &KickGuest; }
		else { return; }
}

sub Password {
	if($username eq 'Guest') { &error($gtxt{'noguest'}); }
	if($settings[4] eq 'Administrator') { return; } # Admin has full access
	&is_member;
	if($Blah{"$URL{'b'}_pw"} eq $binfo[6]) { return; }
	elsif($sidinuse) {
		@boardaccess = split("/",$baccess);
		foreach(@boardaccess) {
			($board,$pass) = split("=",$_);
			if($board eq $URL{'b'} && $pass eq $binfo[6]) { return; }
		}
	}
	$password = crypt($FORM{'pw'},$pwcry);
	$password =~ s/\///g;
	if($password eq $binfo[6]) { # SIDs: Skip the cookies and add a password handle.
		if($sidinuse) {
			fopen(FILE,"$prefs/Sids.sid");
			@sidlist = <FILE>;
			fclose(FILE);
			chomp @sidlist;
			fopen(FILE,"+>$prefs/Sids.sid");
			foreach(@sidlist) {
				($tsid,$ip,$username,$tpw,$endtime,$baccess) = split(/\|/,$_);
				if($URL{'sid'} eq $tsid) {
					if($baccess) { $baccess .= "/"; }
					print FILE "$tsid|$ip|$username|$tpw|$endtime|$baccess$URL{'b'}=$password\n";
				} else { print FILE "$_\n"; }
			}
			fclose(FILE);
		} else { SetCookie("$URL{'b'}_pw",$password,"temp"); }
		return;
	}
	elsif($FORM{'pw'}) { $in = qq~<br>&nbsp;<b>&#149; $boardlock[2]</b>~; }
	$title = $boardlock[3];
	&header;
	$message = $binfo[0];
	$message =~ s/&#47;/\//gsi;
	&BC;
	$ebout .= <<"EOT";
<table class="border" cellpadding="4" cellspacing="1" align=center width="450">
 <tr><form action="$scripturl,v=mindex" method="POST">
  <td class="titlebg"><b><img src="$images/keys.gif"> $title</b></td>
 </tr><tr>
  <td class="win"><span class="smalltext">$boardlock[4]$in</font></td>
 </tr><tr>
  <td class="win2">
   <table cellpadding=2 cellspacing=0 width="100%">
    <tr>
     <td align="right" valign="top"><b>$boardlock[5]:</b></td>
     <td><b>$binfo[2]</b><span class="smalltext"><br>$message</font></td>
    </tr><tr>
     <td align="right" width="40%"><b>$boardlock[6]:</b></td>
     <td width="60%"><input type="password"  class="textinput" name="pw" size="25"></td>
    </tr>
   </table>
  </td>
 </tr><tr>
  <td class="win" align="center"><input type="submit" class="button" value=" $gtxt{'26'} "></td>
 </form></tr>
</table>
EOT
	&footer;
	exit;
}

sub KickGuest {
	$gdisable = 1;
	if($URL{'v'} eq 'login') { CoreLoad('Login'); &Login; }
	if($URL{'v'} eq 'register') { CoreLoad('Register'); &Register; }
	$title = $mbname;
	&header;
	if($creg != 1) { $register = "\n  <br>&nbsp; &#187; <a href=$surl,v=register>$boardlock[7]</a>"; }
	if($username eq 'Guest') { $access = qq~$boardlock[35]<br><br><center><b>$rtxt[3] <a href="$surl,v=login">$rtxt[4]</a> $rtxt[44] <a href="$surl,v=register">$rtxt[5]</a>.</b></font></center>~; }
		else { $access = $boardlock[9]; }
	$ebout .= <<"EOT";
<table class="border" cellspacing="1" cellpadding="4" width="500" align="center">
 <tr>
  <td class="titlebg"><b><img src="$images/ban.gif"> $boardlock[10]</b></td>
 </tr><tr>
  <td class="win">$access</center></td>
 </tr>
</table>
EOT
	&footer;
	exit;
}

sub Maintain {
	$gdisable = 1;
	if($URL{'v'} eq 'login' && $URL{'p'} == 2) { return; }

	$title = "$mbname $boardlock[11]";
	&header;
	$ebout .= <<"EOT";
<table class="border" cellpadding="4" cellspacing="1" width="750" align="center">
 <tr>
  <td class="titlebg" colspan="2"><b><img src="$images/ban.gif"> $title</b></td>
 </tr><tr>
  <td class="win" colspan="2">$boardlock[12]<blockquote>$maintancer</blockquote></td>
 </tr>
</table><br>
<table class="border" cellpadding="4" cellspacing="1" width="750" align="center">
 <tr>
  <td class="titlebg" colspan=2><b>$boardlock[13] $boardlock[8]</b></td>
 </tr><tr><form action="$scripturl,v=login,p=2" method="POST">
  <td class="win">
   <table cellpadding=3 width=100%>
    <tr>
     <td align=right width=25%><b>$boardlock[15]:</b></td>
     <td width=25%><input type="text" class="textinput" name=username size=20></td>
     <td align=right><b>$boardlock[16]:</b></td>
     <td><input type="checkbox" class="checkboxinput" name="nocookie" value="1"></td>
    </tr><tr>
     <td align=right><b>$boardlock[6]:</b></td>
     <td><input type="password"  class="textinput" name=password size=20></td>
     <td align=right><b>$boardlock[18]:</b></td>
     <td><select name="days">
      <option value=".5">12 $gtxt{'3'}</option>
      <option value="1">1 $boardlock[19]</option>
      <option value="4">4 $boardlock[19]</option>
      <option value="7">1 $gtxt{'39'}</option>
      <option value="31">1 $gtxt{'40'}</option>
      <option value="forever" selected>$boardlock[21]</option>
     </select>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <input type="submit" class="button" value="&nbsp;&nbsp;$boardlock[8]&nbsp;&nbsp;">
     </td>
    </tr>
   </table>
  </td>
 </form></tr>
</table>
EOT
	&footer;
	exit;
}

sub LockOut {
	$gdisable = 1;
	print "Content type: text/html\n\n";
	print <<"EOT";
<html>
<head><title>$mbname $boardlock[23]</title></head>
<body link="#000000" vlink="#000000" alink="#000000" topmargin="6">
<table cellspacing="0" cellpadding="1" width="100%">
 <tr>
  <td align="center" colspan="2"><b><font size="4" face="Verdana">$mbname $boardlock[23]<hr color="#000000" size="1"></font></b></td>
 </tr><tr>
  <td colspan="2"><font size="2" face="Verdana"><br>$boardlock[24]<br><br>$boardlock[25]<br><br></font></td>
 </tr><tr><form action="$scripturl,v=login,p=2" method="POST">
  <td align="right" width="50%"><font size="2" face="Verdana"><b>$boardlock[15]:&nbsp; &nbsp; </b></font></td>
  <td width="50%"><input type="text" class="textinput" name="username" size="25" value="$URL{'u'}"></td>
 </tr><tr>
  <td align="right" width="50%"><font size="2" face="Verdana"><b>$boardlock[6]:&nbsp; &nbsp; </b></font></td>
  <td width="50%"><input type="password"  class="textinput" name="password" size="20" value="$URL{'p'}"></td>
 </tr><tr>
  <td colspan="2" align="center"><input type="submit" class="button" value="&nbsp;&nbsp;$boardlock[8]&nbsp;&nbsp;"></td>
 </tr><tr>
  <td colspan="2" align="center"><br><hr color="#000000" size="1"><span class="smalltext" face="Verdana"><br><br><br><br>$mbname $boardlock[23]<br>$copyright</span></td>
 </tr>
</table>
</body>
</html>
EOT
	exit;
}

sub Banned {
	$btod = 0;
	$gdisable = 1;
	($banlimit,$bantime) = @_;
	if($banlimit && time < $bantime) { $timelimit = qq~[hr][size=1]$boardlock[30] [b]$banlimit $boardlock[19]\[/b]. $boardlock[32] [b]~.get_date($bantime).'.[/b][/size]'; }
	elsif($banlimit && time > $bantime) { # Unban this user, his banning is completed ...
		fopen(FILE,">$prefs/BanList.txt");
		foreach(@banlist) {
			($ipaddy,$banlimit,$bantime) = split(/\|/,$_);
   			$length = length($ipaddy);
   			$ipsearch = substr($ENV{'REMOTE_ADDR'},0,$length);
			if($ipsearch =~ /\Q$ipaddy/i || $username eq $ipaddy || $settings[2] eq $ipaddy) { next; }
			print FILE "$_\n";
		}
		fclose(FILE);
		return;
	}
	$time = time;
	@settings = ();
	fopen(FILE,"+>>$prefs/NoAccess.txt");
	print FILE "$username|$ENV{'REMOTE_ADDR'}|$time\n";
	fclose(FILE);
	$username = 'Guest'; # Reset to guest
	error("$boardlock[34]$timelimit",3);
}
1;