################################################
# E-Blah Bulliten Board Systems       Platinum #
################################################
# Copyright (c) 2001 - 2004 e-blah!            #
# All Rights Reserved.                         #
################################################

VerifyBoard();
CoreLoad('Stats',1);

sub Stats {
	if($URL{'a'} eq 'whereis') { &WhereIs; }

	if(!$uextlog) { &error($statstxt[1]); }
	if($hideelog) { &is_admin; }
	fopen(FILE,"$members/LastMem.txt");
	@lastmem = <FILE>;
	fclose(FILE);
	chomp @lastmem;
	loaduser($lastmem[0]);

	if($eclick) {
		$clicklog = 0;
		fopen(FILE,"$prefs/ClickLog.txt");
		@clicklog = <FILE>;
		fclose(FILE);
		chomp @clicklog;
		$clicklogc = @clicklog;
		if($logcnt > 59) { $logcnt = sprintf("%.0f",($logcnt/60))." $gtxt{'3'}"; }
			else { $logcnt = "$logcnt $gtxt{'2'}"; }
	}

	fopen(FILE,"$prefs/MaxLog.txt");
	@maxlogged = <FILE>;
	fclose(FILE);
	chomp @maxlogged;
	$maxtime = get_date($maxlogged[1]);

	$active = 0;
	fopen(FILE,"$prefs/Active.txt");
	while(<FILE>) { ++$active; }
	fclose(FILE);

	$title = $statstxt[2];
	&header;
	&BoardStats;

	fopen(FILE,"$members/List.txt");
	@memlist = <FILE>;
	fclose(FILE);
	chomp @memlist;
	$maxpostcnt = 0;
	foreach(@memlist) {
		loaduser($_);
		push(@maxmem,"$userset{$_}->[3]|$_");
		push(@newmembers,"$userset{$_}->[14]|$_");
	}
	@maxmem = sort{$b <=> $a} @maxmem;
	@newmembers = sort{$b <=> $a} @newmembers;

	# Find average member per day
	($old) = split(/\|/,$newmembers[$lastmem[1]-1]);
	$memperday = sprintf("%.4f",$lastmem[1]/((time-$old)/86400));

	$ebout .= <<"EOT";
<table class="border" cellpadding="5" cellspacing="1" width="98%" align="center">
 <tr>
  <td class="titlebg" colspan="7"><b><img src="$images/clicklog_sm.gif"> $title</b></td>
 </tr><tr>
  <td class="catbg" align="center" colspan="7"><span class="smalltext"><b>$statstxt[3]</b></span></td>
 </tr><tr>
  <td class="win2" colspan="7">
   <table cellpadding="3" cellspacing="0" width="100%">
    <tr>
     <td width="50%"><b><img src="$images/profile_sm.gif"> $statstxt[4]</b></td>
     <td width="50%"><b><img src="$images/open_thread.gif"> $statstxt[5]</b></td>
    </tr>
   </table>
  </td>
 </tr><tr>
  <td class="win" colspan="7" valign="top">
   <table cellpadding="2" cellspacing="0" width="100%">
    <tr>
     <td width="50%" valign="top">
      <table cellpadding="3" cellspacing="0" width="100%">
       <tr>
        <td><b>$statstxt[6]:</b></td>
        <td align="right">$lastmem[1]</td>
       </tr><tr>
        <td><b>$statstxt[7]:</b></td>
        <td align="right"><a href="$scripturl,v=memberpanel,a=view,u=$lastmem[0]">$userset{$lastmem[0]}->[1]</a></td>
       </tr><tr>
        <td><b>$statstxt[8]:</b></td>
        <td align="right">$active</td>
       </tr><tr>
        <td valign="top"><b>$statstxt[9]:</b></td>
        <td align="right" valign="top">$maxtime<br><span class="smalltext">($maxlogged[0] $gtxt{'4'})</span></td>
       </tr><tr>
        <td valign="top"><b>$statstxt[29]:</b></td>
        <td align="right" valign="top">~ $memperday</td>
       </tr>
      </table>
     </td>
     <td width="50%" valign="top">
      <table cellpadding="3" cellspacing="0" width="100%">
EOT
	if($eclick && (!$hideclog || $settings[4] eq 'Administrator')) {
		foreach(@clicklog) {
			($logtime,$ipaddy,$ref,$page,$info) = split(/\|/,$_);
			$f1 = 1;
			if(@ipaddys) { foreach(@ipaddys) { if($_ eq $ipaddy) { $f1 = 0; last; } } }
			if($f1) { push(@ipaddys,$ipaddy); ++$ipcnt; }
		}
		$clicklogc = MakeComma($clicklogc);
		$ebout .= <<"EOT";
       <tr>
        <td valign="top"><b>$statstxt[10] $logcnt:</b></td>
        <td align=right>$clicklogc</span><br><span class="smalltext">($ipcnt $gtxt{'5'})</td>
       </tr>
EOT
	}
	$threads = MakeComma($threads);
	$messagecnt = MakeComma($messagecnt);
	$ebout .= <<"EOT";
       <tr>
        <td valign="top"><b>$gtxt{'6'}:</b></td>
        <td align="right">$catcnt</td>
       </tr><tr>
        <td valign="top"><b>$gtxt{'7'}:</b></td>
        <td align="right">$bdscnt</td>
       </tr><tr>
        <td valign="top"><b>$gtxt{'8'}:</b></td>
        <td align="right">$threads</td>
       </tr><tr>
        <td valign="top"><b>$gtxt{'9'}:</b></td>
        <td align="right">$messagecnt</td>
       </tr>
      </table>
     </td>
    </tr>
   </table>
  </td>
 </tr><tr>
  <td class="catbg" align="center" colspan="7"><span class="smalltext"><b>$statstxt[11]</b></span></td>
 </tr><tr>
  <td class="win2" colspan="7">
   <table cellpadding="3" cellspacing="0" width="100%">
    <tr>
     <td width="50%"><b><img src="$images/lamp.gif"> $statstxt[12]</b></td>
     <td width="50%"><b><img src="$images/profile_sm.gif"> $statstxt[28]</b></td>
    </tr>
   </table>
  </td>
 </tr><tr>
  <td class="win" colspan="7" valign="top">
   <table cellpadding="3" cellspacing="0" width="100%">
    <tr>
EOT
	$team{'Administrator'} = 1;
	$c = 1;
	for($g = 0; $g < 10; ++$g) {
		$team = '';
		($posts,$member) = split(/\|/,$maxmem[$g]);
		$posts = MakeComma($posts);
		if($member eq '') { next; }
		if($team{$userset{$member}->[4]}) { $team = qq~ <img src="$images/team.gif" alt="$gtxt{'29'}"> ~; }
		$topmems .= qq~<b>$c.</b>$team <a href="$surl,v=memberpanel,a=view,u=$member">$userset{$member}->[1]</a> <span class="smalltext">($posts $gtxt{'10'})</span><br>~;
		++$c;
	}
	$c = 1;
	for($g = 0; $g < 10; ++$g) {
		$team = '';
		($regged,$member) = split(/\|/,$newmembers[$g]);
		$regged = get_date($regged);
		if($member eq '') { next; }
		if($team{$userset{$member}->[4]}) { $team = qq~ <img src="$images/team.gif" alt="$gtxt{'29'}"> ~; }
		$regnew .= qq~<b>$c.</b>$team <a href="$surl,v=memberpanel,a=view,u=$member">$userset{$member}->[1]</a> ($regged)<br>~;
		++$c;
	}

	$ebout .= <<"EOT";
     <td valign="top" width="50%"><span class="smalltext">$topmems</span></td>
     <td valign="top" width="50%"><span class="smalltext">$regnew</span></td>
    </tr>
   </table>
  </td>
 </tr><tr>
  <td class="win2" colspan="7">
   <table cellpadding="3" cellspacing="0" width="100%">
    <tr>
     <td width="50%"><b><img src="$images/hotthread.gif"> $statstxt[13]</b></td>
     <td width="50%"><b><img src="$images/veryhotthread.gif"> $statstxt[14] ($gtxt{'12'})</b></td>
    </tr>
   </table>
  </td>
 </tr><tr>
  <td class="win" colspan="7" valign="top">
   <table cellpadding="3" cellspacing="0" width="100%">
    <tr>
EOT
	foreach $use (@boardbase) {
		($board,$t,$t,$t,$t,$t,$t,$apwbrd) = split('/',$use);
		if(!$boardallow{$board}) { next; }
		fopen(FILE,"$boards/$board.msg");
		while(<FILE>) {
			chomp $_;
			($id,$msub,$t,$t,$replies) = split(/\|/,$_);
			push(@mdata,"$replies|$msub|$id|$board");
		}
		fclose(FILE);
	}
	@mdata = sort{$b <=> $a} @mdata;

	$c = 1;
	for($g = 0; $g < 10; ++$g) {
		($replies,$msub,$id,$bdat) = split(/\|/,$mdata[$g]);
		$replies = MakeComma($replies);
		if($msub) { $toptops .= qq~<b>$c.</b> <a href="$surl,v=display,m=$id,b=$bdat,s=$replies">$msub</a> <span class="smalltext">($replies $gtxt{'12'})</span><br>~; }
		++$c;
	}
	if(!$toptops) { $toptops = $statstxt[15]; }
	@tbds = sort{$b <=> $a} @tbds;

	$c = 1;
	for($g = 0; $g < 10; ++$g) {
		($mcnt,$boardnm,$bid) = split(/\|/,$tbds[$g]);
		$mcnt = MakeComma($mcnt);
		if($boardnm) { $topboards .= qq~<b>$c.</b> <a href="$surl,v=mindex,b=$bid">$boardnm</a> <span class="smalltext">($mcnt $gtxt{'11'})</span><br>~; }
		++$c;
	}
	$ebout .= <<"EOT";
     <td valign="top" width="50%"><span class="smalltext">$topboards</span></td>
     <td valign="top" width="50%" colspan="2"><span class="smalltext">$toptops</span></td>
    </tr>
   </table>
  </td>
 </tr><tr>
  <td class="catbg" align="center" colspan="7"><span class="smalltext"><b>$statstxt[16]</span></td>
 </tr><tr>
EOT
	($t,$t,$t,$day,$month,$year) = localtime(time);
	$save = $day.$month.$year;

	$found = 0;
	for($i = 0; $i < 7; ++$i) {
		fopen(FILE,"$prefs/BHits/$save.txt");
		@log = <FILE>;
		fclose(FILE);
		chomp @log;
		if(!$log[0]) {
			++$found;
			if($found > 100) {
				$ebout .= qq~  <td class="win2" align="center" width="14%"><span class="smalltext"><b>* $gtxt{'13'} *</b></span></td>~;
				push(@list,'|');
			} else { --$i; }
			if($day == 1) { --$month; $day = CheckDays($month)+1; }
			$save = --$day.$month.$year;
			next;
		}
		($t,$t,$t,$dayz,$umon,$yr) = localtime($log[6]);
		$dayz = &datest($dayz);
		$yearz = $yr+1900;

		$ebout .= qq~  <td class="win2" align="center" width="14%"><span class="smalltext"><b>$months[$umon] $dayz, $yearz</b></span></td>~;
		push(@list,"$log[0]|$log[1]|$log[2]|$log[3]|$log[4]|$log[5]|$log[7]");
		if($day == 1) { --$month; $day = CheckDays($month)+1; }
		$save = --$day.$month.$year;
		++$found;
	}
	$ebout .= " </tr><tr>";
	foreach(@list) {
		($clicks,$threads,$replies,$members,$newatt,$attdown,$moston) = split(/\|/,$_);
		if($clicks > $mclicks) { $mclicks = $clicks; }
		if($threads > $mthreads) { $mthreads = $threads; }
		if($replies > $mreplies) { $mreplies = $replies; }
		if($members > $mmembers) { $mmembers = $members; }
		if($newatt > $mnewatt) { $mnewatt = $newatt; }
		if($attdown > $mattdown) { $mattdown = $attdown; }
		if($moston > $mmoston) { $mmoston = $moston; }
	}

	@mtxt = ($var{'89'},$statstxt[22],$gtxt{'38'},$var{'83'},$statstxt[25],$statstxt[26],$gtxt{'14'});

	for($z = 0; $z < 7; ++$z) {
		($clicks,$threads,$replies,$members,$newatt,$attdown,$moston) = split(/\|/,$list[$z]);
		if($clicks eq '') {
			$ebout .= qq~  <td class="win" width="14%" valign="top"><span class="smalltext">$gtxt{'13'}</span></td>\n~;
			next;
		}

		if($clicks == $mclicks) { $clicks = '<b><u>'.MakeComma($clicks).'</u></b>'; }
			else { $clicks = MakeComma($clicks); }
		if($threads == $mthreads) { $threads = '<b><u>'.MakeComma($threads).'</u></b>'; }
			else { $threads = MakeComma($threads); }
		if($replies == $mreplies) { $replies = '<b><u>'.MakeComma($replies).'</u></b>'; }
			else { $replies = MakeComma($replies); }
		if($members == $mmembers) { $members = '<b><u>'.MakeComma($members).'</u></b>'; }
			else { $members = MakeComma($members); }
		if($newatt == $mnewatt) { $newatt = '<b><u>'.MakeComma($newatt).'</u></b>'; }
			else { $newatt = MakeComma($newatt); }
		if($attdown == $mattdown) { $attdown = '<b><u>'.MakeComma($attdown).'</u></b>'; }
			else { $attdown = MakeComma($attdown); }
		if($moston == $mmoston) { $moston = '<b><u>'.MakeComma($moston).'</u></b>'; }
			else { $moston = MakeComma($moston); }

		$ebout .= <<"EOT";
  <td class="win" width="14%">
   <table cellpadding="2" cellspacing="0" width="100%">
    <tr>
     <td align="right"><span class="smalltext"><b>$mtxt[0]:</b></span></td>
     <td><span class="smalltext">$clicks</span></td>
    </tr><tr>
     <td align="right"><span class="smalltext"><b>$mtxt[1]:</b></span></td>
     <td><span class="smalltext">$threads</span></td>
    </tr><tr>
     <td align="right"><span class="smalltext"><b>$mtxt[2]:</b></span></td>
     <td><span class="smalltext">$replies</span></td>
    </tr><tr>
     <td align="right"><span class="smalltext"><b>$mtxt[3]:</b></span></td>
     <td><span class="smalltext">$members</span></td>
    </tr><tr>
     <td align="right"><span class="smalltext"><b>$mtxt[4]:</b><br><br></span></td>
     <td><span class="smalltext">$moston<br><br></span></td>
    </tr>
EOT
		if($uallow) {
			$ebout .= <<"EOT";
    <tr>
     <td colspan="2" align="center" class="win2"><span class="smalltext"><b>$statstxt[18]</b></span></td>
    </tr><tr>
     <td align="right"><span class="smalltext"><b>$statstxt[19]:</b></span></td>
     <td><span class="smalltext">$newatt</span></td>
    </tr><tr>
     <td align="right"><span class="smalltext"><b>$gtxt{'14'}:</b></span></td>
     <td><span class="smalltext">$attdown</span></td>
    </tr>
EOT
		}
		$ebout .= "</table></td>";
	}
	opendir(DIR,"$prefs/BHits/");
	@hits = readdir(DIR);
	closedir(DIR);
	foreach(@hits) {
		if($_ eq '.' || $_ eq '..' || $_ eq '.txt' || length($_) > 10) { next; }
		fopen(FILE,"$prefs/BHits/$_");
		@hitsL = <FILE>;
		fclose(FILE);
		chomp @hitsL;
		if($maxhcnt[0] < $hitsL[0]) { $max[0] = $hitsL[6]; $maxhcnt[0] = $hitsL[0]; }
		if($maxhcnt[1] < $hitsL[1]) { $max[1] = $hitsL[6]; $maxhcnt[1] = $hitsL[1]; }
		if($maxhcnt[2] < $hitsL[2]) { $max[2] = $hitsL[6]; $maxhcnt[2] = $hitsL[2]; }
		if($maxhcnt[3] < $hitsL[3]) { $max[3] = $hitsL[6]; $maxhcnt[3] = $hitsL[3]; }
		if($maxhcnt[4] < $hitsL[7]) { $max[4] = $hitsL[6]; $maxhcnt[4] = $hitsL[7]; }
		if($maxhcnt[5] < $hitsL[4]) { $max[5] = $hitsL[6]; $maxhcnt[5] = $hitsL[4]; }
		if($maxhcnt[6] < $hitsL[5]) { $max[6] = $hitsL[6]; $maxhcnt[6] = $hitsL[5]; }

		($t,$t,$t,$dayz,$umon,$yr) = localtime($hitsL[6]);
		$dayz = &datest($dayz);
		$yearz = $yr+1900;
		$mt{"click_$umon\_$yearz"} += $hitsL[0];
		$mt{"threads_$umon\_$yearz"} += $hitsL[1];
		$mt{"replies_$umon\_$yearz"} += $hitsL[2];
		$mt{"members_$umon\_$yearz"} += $hitsL[3];
	}
	for($i = 0; $i < 6; ++$i) { $maxhcnt[$i] = MakeComma($maxhcnt[$i]); }

	$ebout .= <<"EOT";
 <tr>
  <td class="catbg" align="center" colspan="7"><span class="smalltext"><b>$statstxt[30]</b></span></td>
 </tr><tr>
EOT
	($t,$t,$t,$t,$month,$year) = localtime(time);
	$year = $year+1900;
	for($x = 0; $x < 7; ++$x) {
		$ebout .= qq~<td class="win2" align="center" width="14%"><span class="smalltext"><b>$months[$month] $year</b></span></td>\n~;
		if($month == 0) { $month = 11; --$year; } else { --$month; }
	}
	$ebout .= " </tr><tr>\n";
	($t,$t,$t,$t,$month,$year) = localtime(time);
	$year = $year+1900;
	for($x = 0; $x < 7; ++$x) {
		$clicks = MakeComma($mt{"click_$month\_$year"}) || 0;
		$threads = MakeComma($mt{"threads_$month\_$year"}) || 0;
		$replies = MakeComma($mt{"replies_$month\_$year"}) || 0;
		$members = MakeComma($mt{"members_$month\_$year"}) || 0;
		$moston = MakeComma($mt{"moston_$month\_$year"}) || 0;

		$ebout .= <<"EOT";
  <td class="win" width="14%">
   <table cellpadding="2" cellspacing="0" width="100%">
    <tr>
     <td align="right"><span class="smalltext"><b>$mtxt[0]:</b></span></td>
     <td><span class="smalltext">$clicks</span></td>
    </tr><tr>
     <td align="right"><span class="smalltext"><b>$mtxt[1]:</b></span></td>
     <td><span class="smalltext">$threads</span></td>
    </tr><tr>
     <td align="right"><span class="smalltext"><b>$mtxt[2]:</b></span></td>
     <td><span class="smalltext">$replies</span></td>
    </tr><tr>
     <td align="right"><span class="smalltext"><b>$mtxt[3]:</b></span></td>
     <td><span class="smalltext">$members</span></td>
    </tr>
   </table>
  </td>
EOT
		if($month == 0) { $month = 11; --$year; } else { --$month; }
	}
	$ebout .= <<"EOT";
 </tr><tr>
  <td class="catbg" align="center" colspan="7"><span class="smalltext"><b>$statstxt[20]</b></span></td>
 </tr><tr>
EOT
	for($x = 0; $x < 7; ++$x) {
		($t,$t,$t,$dayz,$umon,$yr) = localtime($max[$x]);
		if(!$max[$x]) { ($t,$t,$t,$dayz,$umon,$yr) = localtime($max[0]); }
		$dayz = &datest($dayz);
		$yearz = $yr+1900;
		$ebout .= qq~<td class="win2" align="center" width="14%"><span class="smalltext"><b>$months[$umon] $dayz, $yearz</b></span></td>\n~;
	}
	$ebout .= " </tr><tr>\n";
	for($x = 0; $x < 7; ++$x) {
		if(!$maxhcnt[$x]) { $maxhcnt[$x] = 0; }
		$ebout .= qq~<td class="win" align="center" width="14%"><span class="smalltext"><b>$mtxt[$x]</b><br>$maxhcnt[$x]</span></td>\n~;
	}
	$ebout .= <<"EOT";
 </tr>
</table>
EOT
	&footer;
	exit;
}

sub StatsDate {
	my($date) = $_[0];
	($t,$t,$t,$day,$month,$year) = localtime($date);
	$years = 1900 + $year;
	my $ddis = "$months[$month] $day, $years";
}

sub BoardStats {
	foreach(@boardbase) {
		($board,$t,$t,$bname,$t,$t,$t,$apwbrd) = split("/",$_);
		if(!$boardallow{$board}) { next; }
		fopen(FILE,"$boards/$board.ino");
		@ino = <FILE>;
		fclose(FILE);
		if(!$ino[1]) { $ino[1] = 0; }
		push(@tbds,"$ino[1]|$bname|$board");
		chomp @ino;
		$threads += $ino[0];
		$messagecnt += $ino[1];
	}
	$catcnt = $catcounter;
	$bdscnt = $boardcounter;
}

sub WhereIs {
	if(!$whereis) { &is_admin; }
	$title = $whereistxt[1];
	&header;
	$ebout .= <<"EOT";
<table class="border" cellpadding="6" cellspacing="1" width="750" align="center">
 <tr>
  <td class="titlebg" colspan="4"><b><img src="$images/clicklog_sm.gif" align="left"> $title</b></td>
 </tr><tr>
  <td class="catbg" align="center" width="30" align="center"><img src="$images/pm2_sm.gif" title="$whereistxt[2]"></td>
  <td class="catbg" align="center" width="170"><b>$whereistxt[3]</b></td>
  <td class="catbg" align="center" width="175"><span class="smalltext"><b>$whereistxt[4]</b></span></td>
  <td class="catbg" align="center" width="375"><b>$gtxt{'15'}</b></td>
 </tr>
EOT
	fopen(FILE,"$prefs/Active.txt");
	while( $active = <FILE> ) {
		chomp $active;
		$member = 2;

		($luser,$ldate,$noshow,$lvis,$oboard,$viewid) = split(/\|/,$active);
		loaduser($luser);
		if($userset{$luser}->[1] || $botsearch{$luser} ne '') { $member = 1; ++$monline; } else { ++$gonline; }
		push(@active,"$member|$ldate|$luser|$noshow|$lvis|$oboard|$viewid");
	}
	fclose(FILE);

	@active = sort {$a <=> $b} @active;

	LoadColors();

	foreach(@active) {
		($usertype,$ldate,$luser,$noshow,$lvis,$oboard,$viewid) = split(/\|/,$_);
		$pm = '';

		if($usertype != $changetype) {
			$ebout .= qq~<tr><td class="catbg" colspan="4"><span class="smalltext"><b>$whereistxt[39] ~;
			if($usertype == 1) { $ebout .= "$monline $whereistxt[40]"; }
			if($usertype == 2) { $ebout .= "$gonline $whereistxt[41]"; }
			$ebout .= "</b></span></tr>";
			$changetype = $usertype;
		}

		if($usertype == 2) { $viewuser = $gtxt{'0'}; }
		elsif($botsearch{$luser} ne '') { $viewuser = qq~<span class="onlinebots">$botsearch{$luser}</span>~; }
		elsif($userset{$luser}->[1] eq '') { $viewuser = $luser; }
			else {
				$duser = $userset{$luser}->[1] && $gcolors{$userset{$luser}->[4]} ? qq~<font color="$gcolors{$userset{$luser}->[4]}">$userset{$luser}->[1]</font>~ : $userset{$luser}->[1];
				$viewuser = qq~<a href="$scripturl,v=memberpanel,a=view,u=$luser">$duser</a>~;
			}

		if($usertype == 2 && $settings[4] eq 'Administrator') { $viewuser = $luser; }

		$lastact = get_date($ldate);
		$a = 1;

		&Map;

		if($userset{$luser}->[1] ne '' && (($username ne 'Guest' && !$noshow) || $settings[4] eq 'Administrator')) { $pm = qq~<a href="$scripturl,v=memberpanel,a=pm,s=write,t=$luser" title="$whereistxt[7] $whereistxt[2]"><img src="$images/pm2_sm.gif" border="0"></a>~; }
		if($a) { $curaction = qq~<a href="$scripturl,v=$lvis">$curaction</a>~; }
		if($noshow && $settings[4] ne 'Administrator') { $viewuser = $gtxt{'1'}; }

		$ebout .= <<"EOT";
 <tr>
  <td class="win" align="center" width="30" align="center">$pm</td>
  <td class="win2" align="center">$viewuser</td>
  <td class="win" align="center"><span class="smalltext">$lastact</span></td>
  <td class="win2">$curaction</td>
 </tr>
EOT
	}

	$ebout .= "</table>";
	&footer;
	exit;
}

sub Map {
	if($lvis eq 'stats' && $URL{'a'} eq 'whereis') { $curaction = $whereistxt[8]; $a = 0; }
	elsif($lvis eq 'mindex') { $curaction = $whereistxt[10]; &BoardLoad; $a = 0; }
	elsif($lvis eq 'post') { $curaction = $whereistxt[11]; if($viewid eq '') { &BoardLoad; } else { &MessageLoad; } $a = 0; }
	elsif($lvis eq 'admin') { $curaction = $whereistxt[13]; }
	elsif($lvis eq 'login') { $curaction = $whereistxt[14]; if($username ne 'Guest') { $a = 0; } }
	elsif($lvis eq 'mod') { $curaction = $whereistxt[15]; $a = 0; }
	elsif($lvis eq 'register') { $curaction = $whereistxt[16]; if($username ne 'Guest') { $a = 0; } }
	elsif($lvis eq 'print') { $curaction = $whereistxt[17]; $a = 0; }
	elsif($lvis eq 'members') { $curaction = $whereistxt[18]; }
	elsif($lvis eq 'report') { $curaction = $whereistxt[19]; $a = 0; }
	elsif($lvis eq 'cal') { $curaction = $whereistxt[20]; }
	elsif($lvis eq 'download') { $curaction = $whereistxt[21]; $a = 0; }
	elsif($lvis eq 'stats') { $curaction = $whereistxt[22]; $a = 0; }
	elsif($lvis eq 'search') { $curaction = $whereistxt[23]; }
	elsif($lvis eq 'memberpanel') { $curaction = $whereistxt[2]; if($username eq 'Guest') { $a = 0; } }
	elsif($lvis eq 'shownews') { $curaction = $whereistxt[38]; $a = 0; }
	elsif($oboard ne '') { $curaction = $whereistxt[10]; &BoardLoad; $a = 0; }
	elsif($oboard ne '' && $viewid ne '') { $curaction = $whereistxt[9]; &MessageLoad; $a = 0; }
	elsif($lvis eq '') { $curaction = $whereistxt[25]; }
		else { $curaction = $whereistxt[26]; }
}

sub MessageLoad {
	$fast = 1;
	foreach(@msearched) {
		if($_ eq $oboard) { $fast = 0; last; }
	}
	if($fast) {
		push(@msearched,"$oboard");
		if(!$boardallow{$oboard}) { return; }
		fopen(FILE,"$boards/$oboard.msg");
		while(<FILE>) {
			chomp $_;
			($id,$msub) = split(/\|/,$_);
			push(@mdata,"$id|$msub");
		}
		fclose(FILE);
	}
	foreach(@mdata) {
		($id,$msub) = split(/\|/,$_);
		if($id eq $viewid) {
			if($lvis eq 'post') { $curaction = qq~<b>$whereistxt[27]:</b> <a href="$surl,v=display,m=$id,b=$oboard" title="$whereistxt[27] $whereistxt[29]">$msub</a>~; }
				else { $curaction = qq~<b>$whereistxt[29]:</b> <a href="$surl,v=display,m=$id,b=$oboard" title="$whereistxt[31] $whereistxt[29]">$msub</a>~; }
			last;
		}
	}
}

sub BoardLoad {
	foreach(@boardbase) {
		($id,$t,$t,$bname) = split("/",$_);
		if(!$boardallow{$id}) { return; }
		if($id eq $oboard) {
			if($lvis eq 'post') { $curaction = qq~<b>$whereistxt[11] $whereistxt[34]:</b> <a href="$surl,v=mindex,b=$oboard" title="$whereistxt[11] $whereistxt[34] $bname">$bname</a>~; }
				else { $curaction = qq~<b>$whereistxt[37]:</b> <a href="$surl,v=mindex,b=$oboard" title="$whereistxt[10]">$bname</a>~; }
			last;
		}
	}
}
1;