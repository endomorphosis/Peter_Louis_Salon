################################################
# E-Blah Bulliten Board Systems       Platinum #
################################################
# Copyright (c) 2001 - 2004 e-blah!            #
# All Rights Reserved.                         #
################################################

CoreLoad('MessageIndex',1);

sub MessageIndex {
	$boardurl = $URL{'b'};
	if($boardurl eq '') { &error($ltxt[5]); }
	fopen(FILE,"$boards/$boardurl.ino");
	@btemp = <FILE>;
	fclose(FILE);
	chomp $btemp[0];
	$maxm = $btemp[0] || 1;

	# How many page links?
	$tmax = $totalpp*20;
	$startdot = ($totalpp*2)/5;

	fopen(FILE,"$boards/Stick.txt"); # Only show new message start
	@sticky = <FILE>;
	fclose(FILE);
	chomp @sticky;
	foreach(@sticky) {
		($sb,$sid) = split(/\|/,$_);
		if($sb eq $URL{'b'}) { $stickme{$sid} = 1; }
	}

	&LogPage;
	if($username ne 'Guest') {
		$isnew = 0;
		foreach $logged (@log) {
			($mbah,$lmtime) = split(/\|/,$logged);
			$logged{$mbah} = $lmtime;
		}
	}

	fopen(FILE,"$boards/$boardurl.msg");
	while( $mlata = <FILE> ) {
		chomp $mlata;
		($del,$t,$t,$t,$t,$t,$t,$t,$date) = split(/\|/,$mlata);

		if($URL{'n'} && (($logged{$del}-$date) > 0 || ($logged{"AllRead_$URL{'b'}"}-$date) > 0)) { next; }

		if($stickme{$del}) { push(@mlata3,$mlata); } else { push(@mlata2,$mlata); }
	}
	fclose(FILE);
	unshift(@mlata2,@mlata3);
	$maxm = @mlata2-1 || 1; # /END Come up with new cnt

	$treplies = $maxm < 0 ? 1 : $maxm;
	if($treplies < $maxdis) { $URL{'s'} = 0; }
	$tstart = $URL{'s'} || 0;
	$counter = 1;
	$link = "$scripturl,v=mindex,n=$URL{'n'},s";
	if($tstart > $treplies) { $tstart = $treplies; }
	$tstart = (int($tstart/$maxdis)*$maxdis);
	if($tstart > 0) { $bk = ($tstart-$maxdis); $pagelinks = qq~<a href="$link=$bk">&#171;</a> ~; }
	if($treplies > ($tmax/2) && $tstart > $maxdis*($startdot+1) && $treplies > $tmax) { $pagelinks .= qq~<a href="$link=0">...</a> ~; }
	for($i = 0; $i < $treplies+1; $i += $maxdis) {
		if($i < $bk-($maxdis*$startdot) && $treplies > $tmax) { ++$counter; $final = $counter-1; next; }
		if($URL{'s'} ne 'all' && $i == $tstart || $treplies < $maxdis) { $pagelinks .= qq~<b>$counter</b>, ~; $nxt = ($tstart+$maxdis); }
			else { $pagelinks .= qq~<a href="$link=$i">$counter</a>, ~; }
		++$counter;
		if($counter > $totalpp+$final && $treplies > $tmax) { $gbk = (int($treplies/$maxdis)*$maxdis); $pagelinks =~ s/, \Z//gsi; $pagelinks .= qq~ <a href="$link=$gbk">...</a> ~; ++$i; last; }
	}
	$pagelinks =~ s/, \Z//gsi;
	if(($tstart+$maxdis) != $i && $URL{'s'} ne 'all') { $pagelinks .= qq~ <a href="$link=$nxt">&#187;</a>~; }
	if($treplies > $maxdis && !$sall) { $pagelinks .= $URL{'s'} ne 'all' ? qq~ : <a href="$link=all">$messageindex[1]</a>~ : qq~ : <b>$messageindex[1]</b>~; }

	if($memgrpp[6]) { $team = qq~<img src="$images/team.gif" alt="$gtxt{'29'}"> ~; }

	if($username ne 'Guest' && $amar) { $maread = qq~ $msp<a href="$scripturl,v=mark,l=bdis">$img{'markread'}</a>~; }
	if(!$binfo[5]) { $spoll = qq~$msp<a href="$scripturl,v=post,a=poll">$img{'newpoll'}</a>~; }
	$title = $boardnm;
	&header;
	&Mods;
	if($modz) { $modz = qq~<div style="float: right"><b>$ltxt[7]:</b> $modz&nbsp;</div>~; }
	if($allowrate) { $ratewid = 7; } else { $ratewid = 6; }

	if($settings[4] eq 'Administrator' || $ismod) { ++$ratewid; }

	$ebout .= <<"EOT";
<SCRIPT LANGUAGE="JavaScript">
<!-- Start
function checkAll() {
 shortname = document.msend;
 if(shortname.checkall.checked) { cvalue = true; } else { cvalue = false; }
 for(i = 0; i < shortname.elements.length; i++) {
  if(shortname.elements[i].name == 'opt1') { return; }
  shortname.elements[i].checked = cvalue;
 }
}
// End -->
</script>
<table cellspacing="1" width="100%" class="border">
 <tr>
  <td class="win" height="22"><span class="smalltext"><div style="float: left">&nbsp;<a href="$surl">$gtxt{'44'}</a> &nbsp;<b>&#155;</b> &nbsp;<a href="$surl,c=$catid">$catname</a> &nbsp;<b>&#155; &nbsp;$boardnm</b></div>$modz</span></td>
 </tr>
EOT
	if($showactive) {
		BoardUsers();
		$ebout .= <<"EOT";
 <tr>
  <td class="win"><span class="smalltext">
   <div class="win2" style="padding: 5px"><b>$var{'90'}</b></div>
   <div style="padding: 5px">$activeusers $var{'97'} $gcnt $var{'91'}</div></span>
  </td>
 </tr>
EOT
	}
	$ebout .= <<"EOT";
</table><br>
<table cellpadding="4" cellspacing="1" class="border" width="100%">
EOT
	if($showdes && $binfo[0] ne '') {
		$message = $binfo[0];
		$message =~ s/&#47;/\//gsi;
		&BC;
		$ebout .= <<"EOT";
<tr>
 <td class="win" colspan="$ratewid"><div class="smalltext" style="padding: 5px;">$message</div></td>
</tr>
EOT
	}
	$stata = $binfo[3] == 3 || $binfo[4] == 3 ? 'locked' : 'thread';
	if($username ne 'Guest' && !$nonotify) { $notify = qq~ $msp<a href="$scripturl,v=memberpanel,a=notify,m=brd">$img{'notify'}</a>~; }
	if(($binfo[3] == 1 && $username ne 'Guest') || ($binfo[3] == 2 && $settings[4] eq 'Administrator') || ($binfo[3] == 4 && $settings[4] eq 'Administrator' || $ismod) || ($binfo[3] eq '' || $binfo[3] == 0) && $binfo[3] != 3) { $allowposts = qq~<a href="$scripturl,v=post">$img{'newthread'}</a> $spoll $notify~; }
	$size = $allowrate ? 31 : 36;
	$ebout .= <<"EOT";
 <tr>
  <td class="titlebg" colspan="$ratewid"><span class="smalltext"><div style="float: left; padding: 3px;"><img src="$images/$stata.gif" align="left">&nbsp;<b>$gtxt{'17'}:</b> $pagelinks</div><div style="float: right">$allowposts$maread</div></span></td>
 </tr><tr>
  <td class="catbg">&nbsp;</td>
  <td class="catbg" width="36%"><span class="smalltext"><b>$messageindex[9]</b></span></td>
  <td class="catbg" width="18%" align="center"><span class="smalltext"><b>$gtxt{'36'}</b></span></td>
  <td class="catbg" width="8%" align="center"><span class="smalltext"><b>$gtxt{'38'}</b></span></td>
  <td class="catbg" width="8%" align="center"><span class="smalltext"><b>$messageindex[11]</b></span></td>
EOT
	if($allowrate) { $ebout .= qq~<td class="catbg" align="center"><span class="smalltext"><b>$messageindex[19]</b></span></td>~; }
	$ebout .= qq~<td class="catbg" align="center" width="26%"><span class="smalltext"><b>$messageindex[12]</b></span></td>~;

	if($settings[4] eq 'Administrator' || $ismod) { $ebout .= qq~<form action="$scripturl,v=mod,a=mindex" method="post" name="msend"><td class="catbg" align="center" width="5"><span class="smalltext"><b>$messageindex[34]</b></span></td>~; }

	$ebout .= '</tr>';

	$end = $URL{'s'} eq 'all' ? $maxm : $tstart+$maxdis;

	for($h = 0; $h < @mlata2; $h++) {
		if($h >= $tstart && $h < $end) { push(@messages,$mlata2[$h]); }
	}

	foreach(@messages) {
		($messid,$messtitle,$posted,$date,$replies,$poll,$type,$micon,$date,$lastuser) = split(/\|/,$_);

		fopen(FILE,"$messages/$messid.view");
		@pgviews = <FILE>;
		fclose(FILE);

		$statuscng = 0;
		$status = FindStatus;
		if(!$stuck && $status =~ /sticky/) {
			$statuscng = $messageindex[24];
			$sticks = "$messageindex[26]: ";
			$stuck = 1;
		}
		if($stuck && $status !~ /sticky/) {
			$statuscng = $messageindex[25];
			$sticks = '';
			$stuck = 0;
		}

		if($statuscng) {
			$ebout .= <<"EOT";
 <tr>
  <td class="win2" colspan="$ratewid"><span class="smalltext"><b>$statuscng</b></span></td>
 </tr>
EOT
		}

		$poll = !$sticks && $status eq 'poll_icon' ? "$var{'67'}: " : '';

		if($username ne 'Guest') {
			$new = '';
			$notifys = '';
			$yay = 0;

			if(($logged{$messid}-$date) <= 0 && ($logged{"AllRead_$URL{'b'}"}-$date) <= 0) { $new = qq~<img src="$images/new.gif" alt="$messageindex[28]"> ~; }

			if(-e ("$messages/$messid.mail")) {
				fopen(FILE,"$messages/$messid.mail");
				while($notify = <FILE>) {
					chomp $notify;
					if($username eq $notify) { $notifys = qq~ <img src="$images/notify_sm.gif" alt="$messageindex[13]"> ~; }
				}
				fclose(FILE);
			}
		}

		loaduser($posted);
		$tstart = $userset{$posted}->[1] ne '' ? qq~<a href="$scripturl,v=memberpanel,a=view,u=$posted">$userset{$posted}->[1]</a>~ : CensorList($posted) || $var{'60'};
		loaduser($lastuser);
		$lastuser = $userset{$lastuser}->[1] ne '' ? qq~<a href="$scripturl,v=memberpanel,a=view,u=$lastuser">$userset{$lastuser}->[1]</a>~ : CensorList($lastuser) || $var{'60'};

		$messlinks = '';
		if($replies > 20000) { $replies = 0; }

		if($maxmess-1 < $replies) {
			$max = $replies+1;
			$pcnt = 0;
			$messlinks = qq~<div style="padding-top: 2px;"><b><img src="$images/board.gif"> $gtxt{'17'}:</b> ~;

			$beforeafter = int($totalpp/2);
			$counter = int(($max/$maxmess)+2) - $beforeafter;

			for($cnt = 0; $cnt < $max; $cnt += $maxmess) {
				++$pcnt;
				if(int($max/$maxmess) > $totalpp && $pcnt == $beforeafter+1) { $messlinks =~ s/, \Z/ /; $messlinks .= "... "; }
				if($pcnt > $beforeafter && $pcnt < $counter) { next; }
				$messlinks .= qq~<a href="$scripturl,m=$messid,s=$cnt">$pcnt</a>, ~;
			}

			$messlinks =~ s/, \Z/ /;
			if(!$sall) { $messlinks .= qq~: <a href="$scripturl,m=$messid,s=all">$messageindex[1]</a>~; }
			$messlinks .= "</div>";
		}

		$replies = MakeComma($replies);

		$date = get_date($date);
		$pviews = MakeComma($pgviews[0]) || 0;
		$startdate = get_date($messid,1);
		$messtitle = CensorList($messtitle);
		$icon = '';
		if($micon ne 'xx') { $icon = qq~<img src="$images/$micon.gif">~; }
		$messageurl = $surl."b=$URL{'b'}";
		$ebout .= <<"EOT";
<tr>
 <td class="win2" align="center" style="padding: 7px;">&nbsp;<img src="$images/$status.gif">&nbsp;</td>
 <td class="win" width="36%"><span class="smalltext"><div><b>$new$notifys$sticks$poll<a href="$messageurl,m=$messid" title="$messageindex[15]: $startdate">$messtitle</a></b></div>$messlinks</span></td>
 <td class="win2" align="center" width="18%"><span class="smalltext">$tstart</span></td>
 <td class="win" align="center" width="8%"><span class="smalltext">$replies</span></td>
 <td class="win" align="center" width="8%"><span class="smalltext">$pviews</span></td>
EOT
		if($allowrate) {
			$rcnt = 0;
			$rate = '';
			$ratecnt = 0;
			fopen(FILE,"$messages/$messid.rate");
			while(<FILE>) {
				chomp $_;
				if($rcnt == 0) { $rate = $_; $rcnt = 1; }
					else { ++$ratecnt; }
			}
			fclose(FILE);
			if($rate) { $rate = qq~<img src="$images/$rate\star.gif" alt="$ratecnt $messageindex[20]">~; }
			$ebout .= qq~<td class="win" align="center" width="5%"><span class="smalltext">$rate</span></td>~;
		}
		$ebout .= <<"EOT";
<td class="win2" width="26%">
 <table width="100%">
  <tr>
   <td width="20" align="center">$icon&nbsp;</td>
   <td><span class="smalltext"><div>$date</div><div style="padding-top: 2px;"><b>$messageindex[16]</b> $lastuser</div></span></td>
  </tr>
 </table>
</td>
EOT
	if($settings[4] eq 'Administrator' || $ismod) { ++$scounter; $ebout .= qq~<td class="win" align="center" width="5%"><input type="checkbox" class="checkboxinput" name="$scounter" value="$messid"></td>~; }
	$ebout .= "</tr>";
	}
	if(!$lastuser) {
		if($URL{'n'}) { $messageindex[17] = $messageindex[23]; }
		$ebout .= <<"EOT";
<tr>
 <td class="win" colspan="$ratewid" align="center"><br>$messageindex[17]<br><br></td>
</tr>
EOT
	}
	$GoBoards = &ListBoards;
	&BoardProperties;
	if($allowposts eq '' && $maread eq '') { $lck = qq~<img src="$images/locked.gif"> $messageindex[36]~; }

	if($settings[4] eq 'Administrator' || $ismod) {
		foreach(@catbase) {
			($t,$t,$access,$bhere) = split(/\|/,$_);
			@bdsh = split("/",$bhere);
			foreach $hh (@bdsh) {
				foreach $bbase (@boardbase) {
					($bid,$t,$t,$name) = split("/",$bbase);
					if($bid eq $URL{'b'} || $bid ne $hh) { next; }
					$ops .= qq~<option value="$bid">$name</option>\n~;
					last;
				}
			}
		}
		if($ops) { $move = qq~&nbsp; <input type="checkbox" class="checkboxinput" value="1" name="opt4"> $messageindex[29]: <select name="b">$ops</select>~; }
		$ratewid2 = $ratewid-1;
		$ebout .= <<"EOT";
<tr>
 <td class="catbg" colspan="$ratewid2" align="right"><span class="smalltext"><b><input type="checkbox" class="checkboxinput" value="1" name="opt1"> $messageindex[30] &nbsp; <input type="checkbox" class="checkboxinput" value="1" name="opt2"> $messageindex[31] &nbsp; <input type="checkbox" class="checkboxinput" value="1" name="opt3"> $messageindex[32] $move&nbsp;</b> <input type="submit" class="button" value="$messageindex[33]"></span></td>
 <td class="catbg" align="center"><input type="checkbox" class="checkboxinput" name="checkall" onclick="checkAll();"></td>
</form></tr>
EOT
	}
	$ebout .= <<"EOT";
 <tr>
  <td class="titlebg" colspan="$ratewid"><span class="smalltext"><div style="float: left; padding: 3px;"><img src="$images/$stata.gif" align="left">&nbsp;<b>$gtxt{'17'}:</b> $pagelinks</div><div style="float: right">$allowposts$maread</div></span></td>
 </tr>
</table>
<br>
<table cellpadding="0" width="100%" cellspacing="1" class="border">
 <tr>
  <td>
   <table cellpadding="4" cellspacing="0" width="100%"><tr><form action="$scripturl,v=search,p=bs" method="post" name="sform">
    <td class="win" align="center" width="33%"><div style="padding: 2px;"><input type="hidden" name="searchboards" value="$URL{'b'}"><input type="text" name="searchstring" class="textinput" style="vertical-align: center" size="30"> <input type="submit" class="button" value="$messageindex[37]" style="vertical-align: center"></div></td></form>
    <td class="win2" align="center" width="33%"><span class="smalltext"><a href="$surl">$gtxt{'44'}</a> &nbsp;<b>&#155;</b>&nbsp; <b>$boardnm</b>&nbsp; [ <a href="$scripturl">$messageindex[22]</a> | <a href="$scripturl,n=1">$messageindex[21]</a> ]</span></td>
    <td class="win" align="right" width="33%"><span class="smalltext"><b>$var{'70'}:</b> $GoBoards</span></td>
   </tr></table>
  </td>
 </tr>
</table><br>
<table width="100%" cellpadding="0" cellspacing="0">
 <tr>
  <td><table cellpadding="3" cellspacing="1" class="border">
   <tr>
    <td class="win">
    <table cellpadding="5" cellspacing="0" width="100%">
     <tr>
      <td align="center" width="15"><img src="$images/thread.gif"></td><td><span class="smalltext"><b>$var{'61'}</b></span></td>
      <td align="center" width="15"><img src="$images/locked.gif"></td><td><span class="smalltext"><b>$var{'62'}</b></span></td>
      <td align="center" width="15"><img src="$images/hotthread.gif"></td><td><span class="smalltext"><b>$var{'63'}</b> ($htdmax+)</span></td>
     </tr><tr>
      <td align="center"><img src="$images/veryhotthread.gif"></td><td><span class="smalltext"><b>$var{'64'}</b> ($vhtdmax+)</span></td>
      <td align="center"><img src="$images/sticky.gif"></td><td><span class="smalltext"><b>$var{'65'}</b></span></td>
      <td align="center"><img src="$images/sticky_lock.gif"></td><td><span class="smalltext"><b>$var{'66'}</b></span></td>
     </tr><tr>
      <td align="center"><img src="$images/poll_icon.gif"></td><td><span class="smalltext"><b>$var{'67'}</b></span></td>
      <td align="center"><img src="$images/poll_sticky.gif"></td><td><span class="smalltext"><b>$var{'68'}</b></span></td>
      <td align="center"><img src="$images/poll_lock.gif"></td><td><span class="smalltext"><b>$var{'69'}</b></span></td>
     </tr>
    </table>
    </td>
   </tr>
  </table>
  </td><td align="right"><table cellpadding="4" cellspacing="1" class="border" width="300">
   <tr>
    <td class="catbg" valign="top" colspan="2" align="center"><span class="smalltext"><b>$gtxt{'43'}</b></span></td></td>
   </tr><tr>
    <td class="win">
     <table width="100%" cellpadding="2" cellspacing="0"><tr>
      <td class="win" valign="top" width="65%"><span class="smalltext">$rules</span></td></td>
      <td class="win" valign="top" width="35%"><span class="smalltext">$rules2</span></td></td>
     </tr></table>
    </td>
   </tr>
  </table>
  </td>
 </tr>
</table>
EOT
	&footer;
	exit;
}
1;