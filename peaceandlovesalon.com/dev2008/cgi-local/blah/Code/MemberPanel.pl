################################################
# E-Blah Bulliten Board Systems       Platinum #
################################################
# Copyright (c) 2001 - 2004 e-blah!            #
# All Rights Reserved.                         #
################################################

CoreLoad('MemberPanel',1);

sub SecureMemberData {
	if($URL{'a'} eq 'view' && (!$proon && $URL{'u'} ne $username && $settings[4] ne 'Administrator')) { &PView; }
	if($URL{'a'} eq 'ban' || $URL{'a'} eq 'unban') { &Ban; }
	&is_member;
	if($URL{'u'} eq '' || $URL{'u'} eq $username) { $URL{'u'} = $username; }
	elsif($proon) { $profileonly = 1; }
	elsif($URL{'u'} ne $username) { &is_admin; $profileonly = 1; }
	if($username eq 'Guest') { &error($profiletxt[1]); }
	if($URL{'a'} eq 'remove') { &Remove; }

	loaduser($URL{'u'});
	if($userset{$URL{'u'}}->[1] eq '') { &error("$profiletxt[2] $URL{'u'}"); }
	fopen(FILE,"$members/$URL{'u'}.pm");
	@pmdata = <FILE>;
	fclose(FILE);
	chomp @pmdata;

	fopen(FILE,"$members/$URL{'u'}.prefs");
	@prefs = <FILE>;
	fclose(FILE);
	chomp @prefs;
	@folders = split(/\|/,$prefs[6]);
	foreach(@folders) {
		($fname,$fid) = split("/",$_);
		$fids{$fid} = $fname;
		++$foldercnt;
		$folderops .= qq~<option value="$fid">$fname</option>~;
	}
}

sub MemberPanel {
	&SecureMemberData;
	$perpic = '';
	if($userset{$URL{'u'}}->[5] ne '' && $apic) {
		$pic = $userset{$URL{'u'}}->[5];
		if($userset{$URL{'u'}}->[35]) { ($height,$width) = split(/\|/,$userset{$URL{'u'}}->[35]); }
			else { $width = $picwidth; $height = $picheight; }
		if($userset{$URL{'u'}}->[5] =~ /http:/ || $userset{$URL{'u'}}->[32]) { $perpic = qq!<img src="$pic" width="$width" height="$height"><br>!; }
			else { $perpic = qq!<img src="$avsurl/$userset{$URL{'u'}}->[5]"><br>!; }
	}

	if($activemembers{$URL{'u'}}) { $offonlines = $gtxt{'30'}; }
	elsif($offonline eq '') { $offonlines = $gtxt{'31'}; }
	if($settings[4] ne 'Administrator' && $userset{$URL{'u'}}->[18]) { $offonlines = $gtxt{'1'}; }

	if($URL{'a'} eq 'profile') {
		if($URL{'s'} eq 'contact') { $callt = qq~<img src="$images/pm2_sm.gif"> $profiletxt[177]~; CallProfile(1); }
		elsif($URL{'s'} eq 'pw') { $callt = qq~<img src="$images/keys.gif"> $profiletxt[178]~; CallProfile(2); }
		elsif($URL{'s'} eq 'sig') { $callt = qq~<img src="$images/xx.gif"> $profiletxt[179]~; CallProfile(3); }
		elsif($URL{'s'} eq 'avatar') { $callt = qq~<img src="$images/smiley.gif"> $profiletxt[180]~; CallProfile(4); }
			else { $callt = qq~<img src="$images/profile_sm.gif"> $profiletxt[181]~; CallProfile(5); }
	} elsif($URL{'a'} eq 'forum') {
		if($URL{'s'} eq 'lng') { $callt = qq~<img src="$images/open_thread.gif"> $profiletxt[182]~; CallProfile(8); }
		elsif($URL{'s'} eq 'time') { $callt = qq~<img src="$images/cal_sm.gif"> $profiletxt[183]~; CallProfile(7); }
		elsif($URL{'s'} eq 'messageblock') {  $callt = qq~<img src="$images/ban.gif"> $profiletxt[266]~; CallProfile(10); }
			else { $callt = qq~<img src="$images/thread.gif"> $profiletxt[184]~; CallProfile(6); }
	} elsif($URL{'a'} eq 'notify') {
		CoreLoad('Notify');
		if($URL{'s'} eq 'delete') { &NotifyDel; }
		elsif($URL{'m'} ne '') { &AddDelNotify2; }
			else { &View; }
	} elsif($URL{'a'} eq 'admin') {
		$callt = qq~<img src="$images/keys.gif"> $profiletxt[185]~; CallProfile(9);
	} elsif($URL{'a'} eq 'pm') {
		CoreLoad('PM');
		&PMOpen;
		if($URL{'s'} eq 'write') { &Write; }
		elsif($URL{'s'} eq 'prefs') { &Prefs; }
		elsif($URL{'s'} eq 'block') { &PMBlock; }
		elsif($URL{'s'} eq 'delete') { &PMDelete; }
		elsif($URL{'s'} eq 'mdelete') { &PMDelete2; }
		elsif($URL{'s'} eq 'folders') { &Folders; }
		elsif($URL{'s'} eq 'search') { &Search; }
		elsif($URL{'s'} eq 'blist') { &LoadBuddys; }
		elsif($URL{'m'} ne '') { &PMDisplay; }
			else { &PMStart; }
	} elsif($URL{'a'} eq 'save') { CoreLoad('ProfileEdit'); &SaveSettings; }
		else { $panel = 1; &PView;  $callt = qq~<img src="$images/profile_sm.gif"> $profiletxt[186]~; }

	if($perpic eq $var{60}) { $perpic = ''; }

	$title = $profiletxt[187];
	&header;
	$ebout .= <<"EOT";
<table cellpadding="0" cellspacing="0" width="950" align="center">
 <tr>
  <td valign="top">
   <table cellpadding="0" cellspacing="1" class="border" width="200" align="center">
    <tr>
     <td class="titlebg"><div style="padding: 5px;"><b><img src="$images/profile_sm.gif" style="vertical-align: middle"> $title</b></div></td>
    </tr><tr>
     <td width="100%" class="win" valign="top"><span class="smalltext">
      <div class="win2" style="padding: 10px;" align="center"><b>$profiletxt[282]:</b> <a href="$scripturl,v=memberpanel,u=$URL{'u'}">$userset{$URL{'u'}}->[1]</a><br>$offonlines</div>
EOT
	if(!$profileonly && !$pmdisable) {
		++$foldercnt;
		$ebout .= <<"EOT";
    <div class="catbg" style="padding: 5px;" align="center"><span class="smalltext"><b>$profiletxt[188]</b></span></div>
     <div style="padding: 5px; line-height: 140%;"><a href="$scripturl,v=memberpanel,a=pm,s=write">$profiletxt[189]</a><br>
     <a href="$scripturl,v=memberpanel,a=pm,s=start,f=1">$profiletxt[190]</a> ($pmcnt $gtxt{'11'})<br>
     <a href="$scripturl,v=memberpanel,a=pm,s=start,f=2">$profiletxt[192]</a> ($pmsent $gtxt{'11'})<br>
     <a href="$scripturl,v=memberpanel,a=pm,s=folders">$profiletxt[253]</a> ($foldercnt)<br>
     &nbsp;<img src="$images/pm_folder.gif"> <a href="$scripturl,v=memberpanel,a=pm,s=start,f=3">$profiletxt[193]</a> ($pmstore $gtxt{'11'})<br>
EOT
		foreach(@folders) {
			($fname,$fid) = split("/",$_);
			$cnt = $pmcnt{$fid} || 0;
			$ebout .= <<"EOT";
&nbsp;<img src="$images/pm_folder.gif"> <a href="$scripturl,v=memberpanel,a=pm,s=start,f=$fid">$fname</a> ($cnt $gtxt{'11'})<br>
EOT
		}
		$ebout .= <<"EOT";
     <a href="$scripturl,v=memberpanel,a=pm,s=search">$profiletxt[256]</a><br>
     <a href="$scripturl,v=memberpanel,a=pm,s=prefs">$profiletxt[194]</a><br>
     <a href="$scripturl,v=memberpanel,a=pm,s=blist">$profiletxt[257]</a>
     </div>
EOT
	}
	$ebout .= <<"EOT";
    <div class="catbg" style="padding: 5px;" align="center"><span class="smalltext"><b>$profiletxt[195]</b></span></div>
     <div style="padding: 5px; line-height: 140%;">
     <a href="$scripturl,v=memberpanel,a=profile,s=contact,u=$URL{'u'}">$profiletxt[177]</a><br>
     <a href="$scripturl,v=memberpanel,a=profile,s=profile,u=$URL{'u'}">$profiletxt[181]</a><br>
     <a href="$scripturl,v=memberpanel,a=profile,s=pw,u=$URL{'u'}">$profiletxt[178]</a><br>
     <a href="$scripturl,v=memberpanel,a=profile,s=sig,u=$URL{'u'}">$profiletxt[179]</a><br>
     <a href="$scripturl,v=memberpanel,a=profile,s=avatar,u=$URL{'u'}">$profiletxt[180]</a>
     </div>
    <div class="catbg" style="padding: 5px;" align="center"><span class="smalltext"><b>$profiletxt[196]</b></span></div>
     <div style="padding: 5px; line-height: 140%;">
     <a href="$scripturl,v=memberpanel,a=forum,s=messageblock,u=$URL{'u'}">$profiletxt[266]</a><br>
     <a href="$scripturl,v=memberpanel,a=forum,s=board,u=$URL{'u'}">$profiletxt[184]</a><br>
     <a href="$scripturl,v=memberpanel,a=forum,s=time,u=$URL{'u'}">$profiletxt[183]</a><br>
     <a href="$scripturl,v=memberpanel,a=forum,s=lng,u=$URL{'u'}">$profiletxt[182]</a><br>
     </div>
EOT
	if(!$profileonly && !$nonotify) {
		$ebout .= <<"EOT";
     <div class="catbg" style="padding: 5px;" align="center"><span class="smalltext"><b>$profiletxt[197]</b></span></div>
      <div style="padding: 5px; line-height: 140%;"><a href="$scripturl,v=memberpanel,a=notify">$profiletxt[198]</a></div>
EOT
	}
	if($settings[4] eq 'Administrator') {
		$ebout .= <<"EOT";
     <div class="catbg" style="padding: 5px;" align="center"><span class="smalltext"><b>$profiletxt[199]</b></span></div>
      <div style="padding: 5px; line-height: 140%;"><a href="$scripturl,v=memberpanel,a=admin,s=position,u=$URL{'u'}">$profiletxt[185]</a></div>
EOT
	}
	if($morecaller) {
		$morecaller = qq~<div style="padding: 5px;" class="win"><span class="smalltext">$morecaller</span></div>~;
	}
	$ebout .= <<"EOT";
     </td>
    </tr>
   </table>
  </td><td>&nbsp;</td><td valign="top" align="right" width="750">
   <table cellpadding="0" cellspacing="1" class="border" width="100%">
    <tr>
     <td class="titlebg"><div style="padding: 5px;"><b><img src="$images/profile_sm.gif"> $title</b></div></td>
    </tr><tr>
     <td class="win3" valign="top"><div style="padding: 5px;" class="catbg"><b>$callt</b></div>$morecaller<br>$displaycenter<br></td>
    </tr>
   </table>
  </td>
 </tr>
</table>
EOT
	&footer;
	exit;
}

sub CallProfile {
	CoreLoad('ProfileEdit');

	$displaycenter .= <<"EOT";
<table cellpadding="4" cellspacing="1" class="border" align="center" width="97%"><form action="$scripturl,v=memberpanel,a=save,as=$URL{'a'},s=$URL{'s'},u=$URL{'u'}" method="POST" name="msend" enctype="multipart/form-data">
EOT
	if($_[0] == 1)     { &Contact; } # Start Edit Profile
	elsif($_[0] == 2)  { &PW; }
	elsif($_[0] == 3)  { &Signature; }
	elsif($_[0] == 4)  { &AvatarSetup; }
	elsif($_[0] == 5)  { &ProfileInfo; }
	elsif($_[0] == 6)  { &BoardSettings; } # Start Forum Settings
	elsif($_[0] == 7)  { &TimeSettings; }
	elsif($_[0] == 8)  { &LngTheme; }
	elsif($_[0] == 9)  { &AdminLoad; } # Admin Settings
	elsif($_[0] == 10) { &MessageBlock; }
	if($disablecaller) { return; }
	$displaycenter .= <<"EOT";
 <tr>
  <td class="win2" align="center"><input type="hidden" name="caller" value="$_[0]"><input type="submit" class="button" name="save" value=" $profiletxt[200] "></td>
 </form></tr>
  </table>
EOT
}

sub PView {
	if($URL{'r'} ne '') { ReputationModify(); }

	# Error checking ...
	if(!$memguest) {
		if($username eq 'Guest') { error("$profiletxt[259]-register-"); }
		is_member();
	}
	$viewuser = $URL{'u'} || $username;
	loaduser($viewuser);
	if(!$userset{$viewuser}->[1]) { error($profiletxt[88]); }

	loaduserdisplay($viewuser);

	if($username ne 'Guest') {
		if(!$pmdisable) {
			$addbuddy = qq~<span class="smalltext"><b><a href="$scripturl,v=memberpanel,a=pm,s=blist,p=2,buddy=$viewuser">$profiletxt[258]</a></b></span>~;
			$pm = qq~<a href="$scripturl,v=memberpanel,a=pm,s=write,t=$URL{'u'}">$profiletxt[284]</a>~;
		}
	}

	$registered = get_date($userset{$viewuser}->[14],1);

	if($userset{$viewuser}->[19]) { $website = qq~<img src="$images/site_sm.gif" align="left">&nbsp;$profiletxt[285]: <b><a href="$userset{$viewuser}->[20]">$userset{$viewuser}->[19]</a></b><br><br>~; }

	$mailme = $hmail && $userset{$viewuser}->[12] && $settings[4] ne 'Administrator' ? '' : qq~<a href="mailto:$userset{$viewuser}->[2]" title="$gtxt{'28'}">$profiletxt[286]</a>~;

	$memberstat =~ s/<br>\Z//g;

	$title = "$profiletxt[93] $userset{$viewuser}->[1]";
	if(!$panel) { &header; }

	$displaycenter = '';

	$displaycenter .= <<"EOT";
<script language="javascript">
<!-- //
function confirmwin(mevalues) {
 if(mevalues == 1) { url = ",a=remove"; explain = "$profiletxt[150]: $userset{$URL{'u'}}->[1]?"; }
 if(mevalues == 2) { url = ",a=view,r=1"; explain = "$profiletxt[280] \\"$userset{$URL{'u'}}->[1]\\"$profiletxt[279]"; }
 if(mevalues == 3) { url = ",a=view,r=0"; explain = "$profiletxt[281] \\"$userset{$URL{'u'}}->[1]\\"$profiletxt[279]";}

 if(window.confirm(explain)) { location = "$scripturl,v=memberpanel,u=$URL{'u'}"+url; }
}
// -->
</script>
<table cellpadding="5" cellspacing="1" class="border" align="center" width="700">
 <tr>
  <td class="titlebg" colspan="2"><b>$profiletxt[93]: $userset{$viewuser}->[1]</b></td>
 </tr><tr>
  <td class="win" width="200" valign="top"><span class="smalltext">$memberstat</span></td>
  <td class="win2" valign="top">

<div style="font-size: 25px; font-weight: bold; float: left; padding-right: 10px;">$userset{$viewuser}->[1]</div>
<div class="smalltext">
EOT
	if($enablerep) {
		if($userset{$viewuser}->[43] eq '') { $userset{$viewuser}->[43] = "N/A"; $color=qq~class="grayrep"~; }
		elsif($userset{$viewuser}->[43] < 50) { $color = qq~class="redrep"~; } # Horrible
		elsif($userset{$viewuser}->[43] > 75) { $color = qq~class="greenrep"~; } # Best
			else { $color = qq~class="grayrep"~; } # Neut

		fopen(VLOG,"$members/$viewuser.vlog");
		@vlog = <VLOG>;
		fclose(VLOG);
		$totalcount = @vlog;
		if($username ne $viewuser && $username ne 'Guest') {
			chomp @vlog;
			foreach(@vlog) {
				($name,$t,$updown) = split(/\|/,$_);
				if($name eq $username) { $votelog = 1; last; }
			}
			$js = $updown == 1 ? '3' : '2';
			$updown = $updown == 1 ? 'minus' : 'add';
			if(!$votelog) { $finalrep = qq~<a href="javascript:confirmwin(2);"><img src="$images/add.gif" border="0" style="vertical-align: middle"></a> <a href="javascript:confirmwin(3);"><img src="$images/minus.gif" border="0" style="vertical-align: middle"></a>~; }
				else { $finalrep = qq~<a href="javascript:confirmwin($js);"><img src="$images/$updown.gif" border="0"></a>~; }
		}
		$displaycenter .= qq~<div style="padding-top: 10px; padding-bottom: 10px;"><b>$gtxt{'rep'}:</b>&nbsp; <font $color>$userset{$viewuser}->[43]%</font> ($totalcount) &nbsp;$finalrep</div>~;
	} else { $displaycenter .= qq~<div style="padding-top: 10px; padding-bottom: 10px;">&nbsp;</div>~; }
$displaycenter .= <<"EOT";
$addbuddy<br><b>$profiletxt[283]</b> $registered<br><br>
$website
<a href="$surl,v=search,p=user,by=$viewuser">$profiletxt[248]</a></div>

</td>
 </tr>
</table><br>
<table width="700" cellpadding="0" cellspacing="0" align="center">
 <tr>
  <td width="50%" valign="top">
   <table cellpadding="5" cellspacing="1" class="border" align="center" width="100%">
    <tr>
     <td class="catbg" width="100%"><b>$profiletxt[94]</b>
    </tr><tr>
     <td class="win2">
      <table cellpadding="4" cellspacing="0">
EOT
	$displaycenter .= !$hideposts ? qq~<tr><td class="smalltext" width="100"><b>$profiletxt[96]:</b></td><td class="smalltext">~.MakeComma($userset{$viewuser}->[3])."</td></tr>" : '';

	$displaycenter .= $userset{$viewuser}->[21] ? qq~<tr><td class="smalltext"><b>$ltxt[41]:</b></td><td class="smalltext">$userset{$viewuser}->[21]</td></tr>~ : '';

	if($userset{$viewuser}->[7]) {
		if($userset{$viewuser}->[7] == 1) { $gen = $profiletxt[35]; }
		elsif($userset{$viewuser}->[7] == 2) { $gen = $profiletxt[36]; }
		$displaycenter .= <<"EOT";
<tr>
 <td class="smalltext"><b>$profiletxt[33]:</b></td>
 <td class="smalltext">$gen</td>
</tr>
EOT
	}

	if($userset{$viewuser}->[16]) {
		$age = calage($userset{$viewuser}->[16]);
		$displaycenter .= <<"EOT";
<tr>
 <td class="smalltext"><b>$profiletxt[100]:</b></td>
 <td class="smalltext"><a href="$ageurl">$age</a></td>
</tr>
EOT
	}

	if($logactive) {
		$lastactive = $userset{$viewuser}->[28] && (!$userset{$viewuser}->[18] || $settings[4] eq 'Administrator') ? get_date($userset{$viewuser}->[28],1) : $gtxt{'1'};
		$displaycenter .= <<"EOT";
<tr>
 <td class="smalltext"><b>$profiletxt[113]:</b></td><td class="smalltext">$lastactive</td>
</tr>
EOT
	}

	if($activemembers{$viewuser}) { $offonline = $gtxt{'30'}; }
	elsif($offonline eq '') { $offonline = $gtxt{'31'}; }
	if($settings[4] ne 'Administrator' && $userset{$viewuser}->[18]) { $offonline = $gtxt{'1'}; }

	$displaycenter .= <<"EOT";
       <tr>
        <td class="smalltext"><b>$profiletxt[287]:</b></td><td class="smalltext">$offonline</td>
       </tr>
      </table>
     </td>
    </tr>
   </table>
  </td>
  <td>&nbsp;</td>
  <td width="50%" valign="top">
   <table cellpadding="5" cellspacing="1" class="border" align="center" width="100%">
    <tr>
     <td class="catbg" width="100%"><b>$profiletxt[288]</b>
    </tr><tr>
     <td class="win">
      <table cellpadding="4" cellspacing="0">
EOT
	if($userset{$viewuser}->[8] ne '') {
		$allcnt = 1;
		$displaycenter .= <<"EOT";
<tr>
 <td align="center" width="25"><img src="$images/icq_sm.gif" alt="$profiletxt[42]"></td>
 <td class="smalltext"><a href="http://wwp.icq.com/scripts/search.dll?to=$userset{$viewuser}->[8]" target="icq">$userset{$viewuser}->[8]</a></td>
</tr>
EOT
	}
	if($userset{$viewuser}->[9] ne '') {
		$allcnt = 1;
		$small = $userset{$viewuser}->[9];
		$small =~ s/\+/ /gsi;
		$displaycenter .= <<"EOT";
<tr>
 <td align="center" width="25"><img src="$images/aim_sm.gif" alt="$profiletxt[43]"></td>
 <td class="smalltext"><a href="aim:goim?screenname=$userset{$viewuser}->[9]&message=You+there?" title="$profiletxt[43]">$small</a></td>
</tr>
EOT
	}
	if($userset{$viewuser}->[27] ne '') {
		$allcnt = 1;
		$displaycenter .= <<"EOT";
<tr>
 <td align="center" width="25"><img src="$images/yim_sm.gif" alt="$profiletxt[44]" border="0"></td>
 <td class="smalltext"><a href="http://edit.yahoo.com/config/send_webmesg?.target=$userset{$viewuser}->[27]" target="yahoo" title="$profiletxt[44]">$userset{$viewuser}->[27]</a></td>
</tr>
EOT
	}
	if($userset{$viewuser}->[10] ne '') {
		$allcnt = 1;
		$displaycenter .= <<"EOT";
<tr>
 <td align="center" width="25"><img src="$images/msn_sm.gif" alt="$profiletxt[45]"></td>
 <td class="smalltext"><a href="http://members.msn.com/$userset{$viewuser}->[10]" title="$profiletxt[45]">$userset{$viewuser}->[10]</a></td>
</tr>
EOT
	}
	if($pm ne '') {
		$allcnt = 1;
		$displaycenter .= <<"EOT";
<tr>
 <td align="center" width="25"><img src="$images/pm_sm.gif"></td>
 <td class="smalltext">$pm</td>
</tr>
EOT
	}
	if($mailme ne '') {
		$allcnt = 1;
		$displaycenter .= <<"EOT";
<tr>
 <td align="center" width="25"><img src="$images/email_sm.gif"></td>
 <td class="smalltext">$mailme</td>
</tr>
EOT
	}

	$displaycenter .= !$allcnt ? qq~<tr><td colspan="2" class="smalltext">$profiletxt[110]</td></tr>~ : '';

	$displaycenter .= <<"EOT";
      </table>
     </td>
    </tr>
   </table>
  </td>
 </tr>
EOT
	if($userset{$viewuser}->[23] || ($settings[4] eq 'Administrator' || $URL{'u'} eq $username || $proon)) {
		$displaycenter .= <<"EOT";
 <tr>
  <td colspan="3"><br>
   <table cellpadding="5" cellspacing="1" class="border" width="100%">
    <tr>
     <td class="catbg" colspan="2"><b><img src="$images/lamp.gif">&nbsp;$profiletxt[111]</b></td>
    </tr><tr>
     <td class="win2" colspan="2">
      <table cellspacing="0" cellpadding="2" width="100%">
EOT
}
	if($userset{$viewuser}->[23]) {
		$type = $userset{$viewuser}->[23] eq 'ADMIN' ? $profiletxt[173] : $userset{$viewuser}->[23] eq 'EMAIL|ADMIN' ? $profiletxt[174] : $userset{$viewuser}->[23] eq 'EMAIL' ? $profiletxt[175] : $profiletxt[176];
		$displaycenter .= <<"EOT";
<tr>
 <td align="right"><span class="smalltext"><b>$profiletxt[172]:</b></span></td><td align="center" width="20"><img src="$images/mem_main.gif"></td><td><span class="smalltext" color="red"><b>$type</b></span></td>
</tr>
EOT
	}

	if($settings[4] eq 'Administrator' || $URL{'u'} eq $username || $proon) {
		$cnt = $settings[4] eq 'Administrator' ? 2 : 1;
		$displaycenter .= <<"EOT";
<tr>
 <td align="right" width="35%"><span class="smalltext"><b>$profiletxt[117]:</b></span></td>
 <td rowspan="$cnt" align="center" width="20"><img src="$images/keys.gif"></td>
 <td><span class="smalltext"><a href="javascript:confirmwin(1);">$profiletxt[119]</a></span></td>
</tr>
EOT
	}
	if($settings[4] eq 'Administrator') {
		fopen(FILE,"$prefs/BanList.txt");
		@banlist = <FILE>;
		fclose(FILE);
		chomp @banlist;
		foreach $banss (@banlist) {
			($_) = split(/\|/,$banss);
			if($URL{'u'} eq $_) { $unban = 1; }
		}
		if($logip) {
			$additional = qq~<a href="$scripturl,v=memberpanel,a=ban,u=$URL{'u'}">$profiletxt[120]</a>~;
		}
		$displaycenter .= <<"EOT";
    <tr>
     <td align="right" width="35%"><span class="smalltext"><b>&nbsp;$profiletxt[121]:</b></span></td>
     <td><span class="smalltext">
EOT
		if(!$unban) {
			$additional = $logip ? qq~, <a href="$scripturl,v=memberpanel,a=ban,u=$URL{'u'},s=super">$profiletxt[122] $profiletxt[120]</a>~ : '';
			$displaycenter .= qq~<a href="$scripturl,v=memberpanel,a=ban,u=$URL{'u'}">$profiletxt[120]</a>$additional~;
		} else {
			$additional = $logip ? qq~, <a href="$scripturl,v=memberpanel,a=unban,u=$URL{'u'},s=super">$profiletxt[122] $profiletxt[126]</a>~ : '';
			$displaycenter .= qq~<a href="$scripturl,v=memberpanel,a=unban,u=$URL{'u'}">$profiletxt[126]</a>$additional~;
		}
		$displaycenter .= <<"EOT";
, <a href="$scripturl,v=admin,a=advipsearch,u=$URL{'u'}">$profiletxt[128]</a></span></td>
    </tr>
EOT
	}
	if($userset{$viewuser}->[23] || ($settings[4] eq 'Administrator' || $URL{'u'} eq $username || $proon)) {
		$displaycenter .= <<"EOT";
      </table>
     </td>
    </tr>
   </table>
  </td>
 </tr>
EOT
	}

	if($message) {
		$displaycenter .= <<"EOT";
 <tr>
  <td colspan="3"><br>
   <table cellpadding="5" cellspacing="1" class="border" width="100%">
    <tr>
     <td class="catbg" colspan="2"><b><img src="$images/xx.gif">&nbsp;$profiletxt[55]</b></td>
    </tr><tr>
     <td class="win" colspan="2"><div class="postbody"><span class="smalltext">$message</span></div></td>
    </tr>
   </table>
  </td>
 </tr>
EOT
	}

	$displaycenter .= "</table>";
	if(!$panel) { $ebout .= $displaycenter; &footer; exit; }
}

sub Remove {
	if(!-e("$members/$URL{'u'}.dat")) { &error("$profiletxt[2] $URL{'u'}."); }

	$title = $profiletxt[152];
	&header;
	$ebout .= <<"EOT";
<table class="border" cellpadding="4" cellspacing="1" width="400" align="center">
 <tr>
  <td class="titlebg"><b><img src="$images/mem_main.gif"> $title</b></td>
 </tr><tr>
  <td class="win">$URL{'u'}, $profiletxt[153]</td>
 </tr>
</table>
EOT
	&footer;

	loaduser($URL{'u'});
	if($userset{$URL{'u'}}->[32]) {
		$userset{$URL{'u'}}->[5] =~ s/$uploadurl\///gsi;
		unlink("$uploaddir/$userset{$URL{'u'}}->[5]");
	}
	unlink("$members/$URL{'u'}.dat","$members/$URL{'u'}.vlog","$members/$URL{'u'}.lo","$members/$URL{'u'}.log","$members/$URL{'u'}.pm","$members/$URL{'u'}.prefs","$members/$URL{'u'}.msg");

	$settings[4] = 'Administrator';
	CoreLoad('Admin1');
	&Remem;

	exit;
}

sub Ban {
	&is_admin;
	if($URL{'u'} eq $username) { &error($profiletxt[154]); }
	if(!$logip || $URL{'s'} eq 'super') {
		foreach $use (@boardbase) {
			($board) = split('/',$use);
			fopen(FILE,"$boards/$board.msg");
			while(<FILE>) {
				chomp $_;
				($id,$msub,$t,$t,$replies) = split(/\|/,$_);
				fopen(MESSAGE,"$messages/$id.txt");
				while( $buffer = <MESSAGE> ) {
					chomp $buffer;
					($user,$t,$ip) = split(/\|/,$buffer);
					push(@IPLog,"$user||$ip");
				}
				fclose(MESSAGE);
			}
			fclose(FILE);
		}
	} else {
		fopen(FILE,"$prefs/IpLog.txt");
		@IPLog = <FILE>;
		fclose(FILE);
		chomp @IPLog;
	}
	foreach(@IPLog) {
		($user,$t,$ip) = split(/\|/,$_);
		if($URL{'u'} eq $user && !$cip{$ip}) {
			if($URL{'a'} eq 'ban') { push(@ipadd,"$ip|||$URL{'u'}"); }
			$cip{$ip} = 1;
		}
	}

	fopen(FILE,"$prefs/BanList.txt");
	@banlist = <FILE>;
	fclose(FILE);
	chomp @banlist;
	foreach $banzz (@banlist) {
		($_) = split(/\|/,$banzz);
		if($URL{'a'} eq 'unban') {
			if(!$cip{$_} && $URL{'u'} ne $_) {
				push(@ipadd,$banzz);
				$cip{$_} = 1;
			}
		}
		if($URL{'a'} eq 'ban') {
			if($cip{$_}) {
				push(@ipadd,"$_|||$URL{'u'}");
				$cip{$_} = 1;
			} else { push(@ipadd,"$banzz"); }
		}
	}
	if($URL{'a'} ne 'unban') { push(@ipadd,"$URL{'u'}|||$URL{'u'}"); }

	fopen(FILE,"+>$prefs/BanList.txt");
	foreach(@ipadd) { print FILE "$_\n"; }
	fclose(FILE);

	redirect("$scripturl,v=memberpanel,a=view,u=$URL{'u'}");
}

sub ReputationModify {
	if($username eq 'Guest') { error($gtxt{'noguest'}); }

	loaduser("$URL{'u'}");
	if($userset{$URL{'u'}}->[1] eq '') { return; } # User not found
	$userset{$URL{'u'}}->[43] = 0;

	fopen(VLOG,"$members/$URL{'u'}.vlog");
	@vlog = <VLOG>;
	fclose(VLOG);
	chomp @vlog;

	$rvar = $URL{'r'} ? 1 : 0;

	if($username ne $URL{'u'}) { push(@vlog,"$username||$rvar"); } else { $nevar = 2; }

	fopen(VLOG,">$members/$URL{'u'}.vlog");
	foreach(@vlog) {
		($vuser,$t,$vadd) = split(/\|/,$_);

		if($noadd && $vuser eq $username) { $nevar = 1; next; }

		if($username eq $vuser) {
			$noadd = 1;
			if($vadd != $rvar) { $vadd = $rvar; }
		}

		if($vadd == 1) { ++$pluses; ++$totals; }
			else { ++$totals; }

		print VLOG "$vuser||$vadd\n";
	}
	fclose(VLOG);

	if($totals > 0) { $userset{$URL{'u'}}->[43] = sprintf("%.2f",(($pluses/$totals)*100)); }
		else { $userset{$URL{'u'}}->[43] = ''; }

	fopen(FILE,">$members/$URL{'u'}.dat");
	for($q = 0; $q < $usersetcount; $q++) { print FILE "$userset{$URL{'u'}}->[$q]\n"; }
	fclose(FILE);

	return (1);
}
1;