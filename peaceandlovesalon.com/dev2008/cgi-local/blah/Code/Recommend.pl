################################################
# E-Blah Bulliten Board Systems       Platinum #
################################################
# Copyright (c) 2001 - 2004 e-blah!            #
# All Rights Reserved.                         #
################################################

CoreLoad('Recommend',1);

sub Recommend {
	if($username eq 'Guest') { &error($gtxt{'noguest'}); }
	&is_member;
	fopen(FILE,"$boards/$URL{'b'}.msg");
	while (<FILE>) {
		chomp $_;
		($id,$tit) = split(/\|/,$_);
		if($id eq $URL{'m'}) { $fnd = 1; $ttitle = $tit; last; }
	}
	fclose(FILE);
	if(!$fnd) { &error("$gtxt{error2}: $URL{'m'}"); }

	$title = CensorList($title);

	if($URL{'p'} eq 'send') { &Recommend2; }

	$title = $rectxt[5];
	&header;
	$ebout .= <<"EOT";
<table class="border" cellpadding="4" cellspacing="1" width="700" align="center">
 <tr><form action="$scripturl,v=recommend,m=$URL{'m'},p=send" method="post" name="msend">
  <td class="titlebg"><b><img src="$images/recommend_sm.gif"> $title</b></td>
 </tr><tr>
  <td class="win"><span class="smalltext">$rectxt[18]</span></td>
 </tr><tr>
  <td class="win2">
   <table cellpadding="2" cellspacing="0" width="100%">
    <tr>
     <td align="right" width="35%" valign="top"><b>$rectxt[6]:</b></td>
     <td width="65%">$ttitle</td>
    </tr>
   </table>
  </td>
 </tr><tr>
  <td class="win">
   <table cellpadding="2" cellspacing="0" width="100%">
    <tr>
     <td align="right" width="35%"><b>$rectxt[7]:</b></td>
     <td width="65%"><input type="text" class="textinput" size="30" name="name" value="$settings[1]"></td>
    </tr><tr>
     <td align="right" width="35%"><b>$rectxt[8]:</b></td>
     <td width="65%"><input type="text" class="textinput" size="30" name="email" value="$settings[2]"></td>
    </tr>
   </table>
  </td>
 </tr><tr>
  <td class="win2">
   <table cellpadding="2" cellspacing="0" width="100%">
    <tr>
     <td align="right" width="35%"><b>$rectxt[9]:</b></td>
     <td width="65%"><input type="text" class="textinput" size="30" name="fname" value=""></td>
    </tr><tr>
     <td align="right" width="35%"><b>$rectxt[10]:</b></td>
     <td width="65%"><input type="text" class="textinput" size="30" name="femail" value=""></td>
    </tr>
   </table>
  </td>
 </tr><tr>
  <td class="win">
   <table cellpadding="2" cellspacing="0" width="100%">
    <tr>
     <td align="right" valign="top" width="35%"><b>$rectxt[22]:</b></td>
     <td width="65%">$rectxt[2]</td>
    </tr><tr>
     <td align="right" valign="top" width="35%"><b>$rectxt[11]:</b><br><span class="smalltext">$rectxt[23]</span></td>
     <td width="65%"><textarea name="message" wrap="virtual" rows="7" cols="60">$rectxt[16]\n\n$rectxt[13], $settings[1], $rectxt[14] "$mbname".\n\n$rectxt[20], "$ttitle" $rectxt[21]:\n$rurl,v=display,b=$URL{'b'},m=$URL{'m'}\n\n$gtxt{'25'}</textarea></td>
    </tr>
   </table>
  </td>
 </tr><tr>
  <td class="win2" align="center"><input type="submit" class="button" value=" $rectxt[17] "></td>
 </tr>
</table>
EOT
	&footer;
	exit;
}

sub Recommend2 {
	my($to,$subject,$message,$from) = @_;

	if($URL{'he'}) { &error("[b]E-BLAH![/b]\n\nCreated by Justin\n\nCongrats, you've found a private area!"); }
	&error($rectxt[4]) if($FORM{'email'} eq '' || $FORM{'femail'} eq '');
	&error($rectxt[3]) if($FORM{'name'} eq '' || $FORM{'fname'} eq '');
	$message = Format($FORM{'message'});
	if($message eq '') { &error($gtxt{'bfield'}); }

	&smail($FORM{'femail'},$rectxt[2],$message,$FORM{'email'});
	redirect("$scripturl,v=display,m=$URL{'m'}");
}
1;