################################################
# E-Blah Bulliten Board Systems       Platinum #
################################################
# Copyright (c) 2001 - 2004 e-blah!            #
# All Rights Reserved.                         #
################################################

$iframes = 0; # 1 = iframes, 0 = frames (no iframes)

sub AdminList { # This file servers the purpose of quickly loading the admin center, with little wait and CPU
	my(%LoadAdminIndex,$core,$sub);

	is_admin();

	%LoadAdminIndex = (
		'bkup'      => 'Admin1,BoardBackup',
		'bkupstart' => 'Admin1,BKUPStart',
		'repop'     => 'Admin1,Repop',
		'temp'      => 'Admin1,Temp',
		'temp2'     => 'Admin1,Temp2',
		'iplog'     => 'Admin1,IPLog',
		'errorlog'  => 'Admin1,ErrorLog',
		'clicklog'  => 'Admin1,ClickLog',
		'remem'     => 'Admin1,Remem',
		'killsids'  => 'Admin1,KILLSID',
		'version'   => 'Admin1,Version',
		'news'      => 'Admin1,News',
		'reserve'   => 'Admin1,Reserve',
		'encrypt'   => 'Admin1,Encrypt',
		'main'      => 'Admin1,AdminMain',
		'welcome'   => 'Admin1,AdminWelcome',
		'links'     => 'Admin1,AdminLinks',

		'sets'     => 'Admin2,Settings',
		'smiley'   => 'Admin2,Smiley',
		'delspec'  => 'Admin2,RemoveSpectators',
		'banlog'   => 'Admin2,BanLog',
		'ban'      => 'Admin2,BanUser',
		'censor'   => 'Admin2,Censor',
		'posts'    => 'Admin2,PostRecount',

		'memgrps'       => 'Admin3,MemGrps',
		'advipsearch'   => 'Admin3,IPSearch',
		'mailing'       => 'Admin3,Mailing',
		'validate'      => 'Admin3,Validate',
		'removemembers' => 'Admin3,RemoveMembers',
		'removeelog'    => 'Admin3,RemoveELog',

		'remove'    => 'Moderate,OldThreads',

		'portal'    => 'Portal,PortalAdmin',

		'themesman' => 'Themes,ThemeManager',
		'sourcemod' => 'SourceCodeMod,SourceMod',

		'attlog'    => 'Attach,AttachLog',

		'reports'   => 'Report,AdminView',

		'referals' => 'Invite,Referals',

		'boards'    => 'ManageForum,ManageBoards',
		'cats'      => 'ManageForum,ManageBoards',
	);

	if($URL{'r'} ne '') { $URL{'a'} = 'main'; }

	$adminsloaded = 1;
	if($LoadAdminIndex{$URL{'a'}}) {
		($core,$sub) = split(',',$LoadAdminIndex{$URL{'a'}});
		CoreLoad($core);
		&$sub;
	} else {
		CoreLoad('Admin1');
		&AdminCenter;
	}
}

sub headerA {
	if($hdone) { return; }

	if($ENV{'HTTP_ACCEPT_ENCODING'} !~ /gzip/ && $gzipen) { $gzipen = 0; } # Browser doesn't support gzip =(
	if($gzipen) { print "Content-Encoding: gzip\n"; }
	print "Content-type: text/html\n\n";

	$hdone = 1; # This is so we get no errors!

	if($templatesu eq '') { $templatesu = "/blahdocs/template"; }

	$ebout .= <<"EOT";
<html>
<head>
<link rel="stylesheet" type="text/css" href="$templatesu/admintemplate.css">
<title>$title</title>
</head>
<body class="mainbg" topmargin="0" leftmargin="0" rightmargin="0" bottommargin="10">
EOT
}

sub footerA {
	$ebout .= <<"EOT";
</body>
</html>
EOT
	if($gzipen) {
		if($gzipen == 2) {
			open(GZIP,"| gzip -f");
			print GZIP $ebout;
			close(GZIP);
		} else {
			require Compress::Zlib;
			binmode STDOUT;
			print Compress::Zlib::memGzip($ebout);
		}
	} else { print $ebout; }
}
1;