################################################
# E-Blah Bulliten Board Systems       Platinum #
################################################
# Copyright (c) 2001 - 2004 e-blah!            #
# All Rights Reserved.                         #
################################################

CoreLoad('Admin1',1);

&is_admin;

sub LoadModifications {
	# When you make a modification to Blah, add it here in the following format:
	  # - push(@bdmod,"My Modification Name");

	$ypon = $yabbconver ? $admintxt[2] : $admintxt[3]; # Password Encryption
	push(@bdmod,"$admintxt[1] (<b>$ypon</b>)"); # Password Encryption

	# End of editing ...
	foreach(@bdmod) { $bdmods .= "$_<br>"; }
	$bdmods =~ s/<br>\Z//;
}

sub AdminCenter { # Frames version
	if($iframes) { AdminCenteriF(); }
	print "Content-type: text/html\n\n";

	print <<"EOT";
<html>
<head>
<title>$admintxt[187]</title>
</head>
<frameset cols="260,*" border="0" noresize="yes">
<frame src="$scripturl,v=admin,a=links" marginwidth="0" marginheight="0" scrolling="yes">
<frameset rows="65,*" noresize="yes">
<frame src="$scripturl,v=admin,a=welcome" name="top" scrolling="no" vspace="0" hspace="0" marginwidth="0" marginheight="0" noresize="yes">
<frame name="adminaction" src="$scripturl,v=admin,a=main" scrolling="auto" marginwidth="0" marginheight="0" noresize="no">
</frameset>
</frameset>
<noframes>
Frames are not avilable on your browser.  Get a new browser.
</noframes>
</HTML>
EOT
	exit;
}

sub AdminCenteriF { # iFrames version
	$title = $admintxt[187];
	&headerA;
	$ebout .= <<"EOT";
<script>
function resizeMe(obj){
obj.style.height = mm.document.body.scrollHeight + 30 + 'px'
}
function resizeMe2(obj){
obj.style.height = adminaction.document.body.scrollHeight + 30 + 'px'
}
</script>
<body>

<table width="100%" cellpadding="0" cellspacing="0">
 <tr>
  <td colspan="2"><iframe frameborder="0" scrolling="no" id="iframename" src="$scripturl,v=admin,a=welcome" width="100%" height="65"></iframe></td>
 </tr><tr>
  <td valign="top"><iframe frameborder=0 onLoad="resizeMe(this)" name="mm" scrolling="no" id="iframename" src="$scripturl,v=admin,a=links" width="250" height="300"></IFRAME></td>
  <td valign="top" width="100%"><iframe frameborder=0 onLoad="resizeMe2(this)" name="adminaction" scrolling="no" id="adminaction" src="$scripturl,v=admin,a=main" width="100%" height="250"></IFRAME></td>
 </tr>
</table>
EOT
	&footerA;
	exit;
}

sub AdminWelcome {
	$date_time = get_date(time,1);
	&headerA;
	$ebout .= <<"EOT";
<table cellpadding="4" cellspacing="0" width="100%" align="center">
 <tr>
  <td class="titlebg" colspan="2"><table cellpadding="0" cellspacing="0" width="100%"><tr class="titlebgtext"><td><b><img src="$images/admin_sm.gif" align="left"> $admintxt[187]</b></td><td align="right"><span class="smalltext">E-Blah $versioncr</span></tr></table></td>
 </tr><tr>
  <td class="win" colspan="2">
  <table cellpadding="2" cellspacing="0" width="100%">
   <tr>
    <td width="25%" align="left"><a href="$scripturl" target="_parent">$img{'home'}</a></td>
    <td width="50%" align="center"><img src="$images/keys.gif"> <b>$admintxt[8]:</b> $date_time</span></td>
    <td width="25%" align="right"><a href="$scripturl,v=login,p=3" target="_parent">$img{'logout'}</a></td>
   </tr>
  </table>
  </td>
 </tr>
</table>
EOT
	&footerA;
	exit;
}

sub AdminLinks {
	&headerA;
	$ebout .= <<"EOT";
<script language="javascript">
<!-- //
function clear(url,url2,explain) {
 if(!explain) { explain = "$admintxt[7]"; }
 if(window.confirm(explain)) { top.frames['adminaction'].location = url; }
  else {
   if(url2) { top.frames['adminaction'].location = url2; }
  }
}
// -->
</script>

<table cellpadding="5" cellspacing="0" width="100%">
 <tr>
  <td class="win2" align="center"><span class="smalltext"><img src="$images/eblahlogo.gif"><br><br><a href="$scripturl,v=admin,a=main" target="adminaction">$admintxt[187]</a><br><a href="$scripturl" target="adminaction">$admintxt[188]</a><br><br></span></td>
 </tr><tr>
  <td class="catbg"><b><img src="$images/open_thread.gif"> $admintxt[14]</b></td>
 </tr><tr>
  <td class="win" id="mm"><span class="smalltext">
<b><a href="$scripturl,v=admin,a=sets" target="adminaction">$admintxt[16]</a><br>
<a href="$scripturl,v=admin,a=boards" target="adminaction">$admintxt[43]</a><br>
<a href="$scripturl,v=admin,a=smiley" target="adminaction">$admintxt[44]</a><br>
<a href="$scripturl,v=admin,a=censor" target="adminaction">$admintxt[46]</a><br>
<a href="$scripturl,v=admin,a=portal" target="adminaction">$admintxt[168]</a><br>
<a href="$scripturl,v=admin,a=themesman" target="adminaction">$admintxt[47]</a><br>
<a href="$scripturl,v=admin,a=advipsearch" target="adminaction">$admintxt[48]</a><br>
<a href="$scripturl,v=admin,a=news" target="adminaction">$admintxt[49]</a><br>
<a href="$scripturl,v=admin,a=sourcemod" target="adminaction">$admintxt[50]</a></b></td>
 </tr><tr>
  <td class="catbg"><b><img src="$images/xx.gif"> $admintxt[51]</b></td>
 </tr><tr>
  <td class="win" id="mm"><span class="smalltext"><b><a href="$scripturl,v=admin,a=temp" target="adminaction">$admintxt[52]</a></b></td>
 </tr><tr>
  <td class="catbg"><b><img src="$images/profile_sm.gif"> $admintxt[54]</b></td>
 </tr><tr>
  <td class="win" id="mm"><span class="smalltext">
<b><a href="$scripturl,v=admin,a=memgrps" target="adminaction">$admintxt[55]</a><br>
<a href="$scripturl,v=admin,a=reports" target="adminaction">$admintxt[56]</a><br>
<a href="$scripturl,v=admin,a=mailing" target="adminaction">$admintxt[58]</a><br>
<a href="$scripturl,v=admin,a=removemembers" target="adminaction">$admintxt[62]</a><br>
<a href="$scripturl,v=admin,a=ban" target="adminaction">$admintxt[45]</a><br>
<a href="$scripturl,v=admin,a=validate" target="adminaction">$admintxt[63]</a><br>
<a href="$scripturl,v=register" target="adminaction">$admintxt[185]</a></b></td>
 </tr><tr>
  <td class="catbg"><b><img src="$images/mem_main.gif"> $admintxt[59]</b></td>
 </tr><tr>
  <td class="win" id="mm"><span class="smalltext">
<b><a href="javascript:clear('$scripturl,v=admin,a=remem')">$admintxt[60]</a><br>
<a href="$scripturl,v=admin,a=delspec" target="adminaction">$admintxt[61]</a><br>
<a href="$scripturl,v=admin,a=reserve" target="adminaction">$admintxt[171]</a><br>
<a href="javascript:clear('$scripturl,v=admin,a=posts','','$admintxt[159]')">$admintxt[160]</a><br>
<a href="javascript:clear('$scripturl,v=admin,a=encrypt','','$admintxt[183]')">$admintxt[184]</a></b></td>
 </tr><tr>
  <td class="catbg"><b><img src="$images/brd_main.gif"> $admintxt[64]</b></td>
 </tr><tr>
  <td class="win" id="mm"><span class="smalltext">
<b><a href="$scripturl,v=admin,a=repop" target="adminaction">$admintxt[65]</a><br>
<a href="$scripturl,v=admin,a=bkup" target="adminaction">$admintxt[66]</a><br>
<a href="$scripturl,v=admin,a=remove" target="adminaction">$admintxt[67]</a></b></td>
 </tr><tr>
  <td class="catbg"><b><img src="$images/clicklog_sm.gif"> $admintxt[68]</b></td>
 </tr><tr>
  <td class="win" id="mm"><span class="smalltext">
<b><a href="$scripturl,v=admin,a=attlog" target="adminaction">$admintxt[69]</a><br>
<a href="$scripturl,v=admin,a=banlog" target="adminaction">$admintxt[70]</a><br>
<a href="$scripturl,v=admin,a=clicklog" target="adminaction">$admintxt[71]</a><br>
<a href="$scripturl,v=admin,a=errorlog" target="adminaction">$admintxt[72]</a><br>
<a href="$scripturl,v=admin,a=iplog" target="adminaction">$admintxt[73]</a><br>
<a href="$scripturl,v=admin,a=referals" target="adminaction">$admintxt[74]</a></b></td>
 </tr>
</table>
EOT
	&footerA;
	exit;
}

sub AdminMain {
	foreach(@admins) { push(@adminlist,FindRanks($_)); }
	foreach(@adminlist) { $adminlist .= qq~<a href="$scripturl,v=memberpanel,a=view,u=$_">$userset{$_}->[1]</a>, ~; }
	$adminlist =~ s/, \Z//;

	&LoadModifications;
	fopen(FILE,"$prefs/ReportPM.txt");
	@reports = <FILE>;
	fclose(FILE);
	chomp @reports;
	$reportpm = 0;
	foreach(@reports) {
		($t,$t,$t,$t,$t,$t,$t,$t,$recieveuser,$t,$t) = split(/\|/,$_);
		if($recieveuser eq $username || $recieveuser eq 'all admin') { ++$reportpm; }
	}

	$banninglog = 0;
	$activelog = 0;
	$clicklog = 0;
	$banlog = 0;
	$felog = 0;
	$areferals = 0;
	$referals = 0;
	$activesids = 0;
	$inactivesids = 0;

	fopen(FILE,"$prefs/ELog.txt");
	while(<FILE>) { ++$felog; }
	fclose(FILE);
	fopen(FILE,"$prefs/BanList.txt");
	while(<FILE>) { ++$banlog; }
	fclose(FILE);
	fopen(FILE,"$prefs/ClickLog.txt");
	while(<FILE>) { ++$clicklog; }
	fclose(FILE);
	fopen(FILE,"$prefs/Active.txt");
	while(<FILE>) { ++$activelog; }
	fclose(FILE);
	fopen(FILE,"$prefs/NoAccess.txt");
	while(<FILE>) { ++$banninglog; }
	fclose(FILE);
	fopen(FILE,"$prefs/Refer.txt");
	while(<FILE>) { ++$areferals; }
	fclose(FILE);
	fopen(FILE,"$prefs/ReferLog.txt");
	while(<FILE>) { ++$referals; }
	fclose(FILE);

	opendir(DIR,"$theme/");
	@themes = readdir(DIR);
	closedir(DIR);
	CoreLoad("Themes");
	foreach(@themes) {
		if($_ =~ m/(.*)(.v2)/) {
			StopThemes("$theme/$_") || next;
			$themes .= qq~$name<br>~;
			++$thstoper;
		}
		if($thstoper == 3) { $themes .= qq~<a href="$scripturl,v=admin,a=themesman">......</a>~; last; }
	}
	$themes =~ s/<br>\Z//;
	if($themes eq '') { $themes = $admintxt[5]; }

	$systemstatus = $clicklog / $logcnt;
	if($activelog >= 0) { $systemstatus = $admintxt[166]; }
	if($activelog > 19 || ($clicklog / $logcnt) > 1.3) { $systemstatus = $admintxt[167]; }
	if($activelog > 39 || ($clicklog / $logcnt) > 5.5) { $systemstatus = $admintxt[165]; }
	if($felog) { $systemstatus .= " $admintxt[164]"; }

	$clicklog = MakeComma($clicklog);
	$logcnt = $logcnt > 59 ? sprintf("%.0f",($logcnt/60))." $gtxt{'3'}" : "$logcnt $gtxt{'2'}";

	if($URL{'r'} < 5 && $URL{'r'} > 0) {
		if($URL{'r'} == 1) { $r = $admintxt[77]; }
		if($URL{'r'} == 2) { $r = $admintxt[145]; }
		if($URL{'r'} == 3) { $r = $admintxt[169]; }
		if($URL{'r'} == 4) { $r = $admintxt[170]; }

		$message = "<b><center>$r</center></b>";
	} else {
		$message = qq~$admintxt[12] <a href="http://www.eblah.com/cgi-bin/bb/Blah.pl">$admintxt[13]</a>.~;
	}

	headerA();
	$ebout .= <<"EOT";
<table width="100%" cellpadding="3" cellspacing="0">
 <tr>
  <td colspan="2"><table class="border" cellpadding="4" cellspacing="1" width="100%" align="center">
   <tr>
    <td class="win" width="100%"><span class="smalltext">$message</span></td>
   </tr>
  </table><br></td>
 </tr><tr>
  <td width="50%" valign="top">
   <table width="100%" cellpadding="4" cellspacing="1" class="border">
    <tr>
     <td class="titlebg"><img src="$images/ip.gif"> $admintxt[161]</td>
    </tr><tr>
     <td class="win">
      <table cellpadding="2" cellspacing="1" width="100%">
       <tr>
        <td align="right" valign="top" colspan="2"><span class="smalltext"><b>$admintxt[163] ($logcnt):</b></span></td>
        <td valign="top" colspan="2"><span class="smalltext">$systemstatus</span></td>
       </tr><tr>
        <td colspan="4"><hr class="hr" size="1"></td>
       </tr><tr>
        <td width="15%" align="right"><span class="smalltext"><a href="$scripturl,v=admin,a=clicklog">$clicklog</a></span></td>
        <td width="35%"><span class="smalltext"><b>$admintxt[20]</b></span></td>
        <td width="10%" align="right"><span class="smalltext"><a href="$scripturl,v=admin,a=reports">$reportpm</a></span></td>
        <td width="40%"><span class="smalltext"><b>$admintxt[21]</b></span></td>
       </tr><tr>
        <td align="right"><span class="smalltext"><a href="$scripturl,v=stats,a=whereis" target="_parent">$activelog</a></span></td>
        <td><span class="smalltext"><b>$admintxt[22]</b></span></td>
        <td align="right"><span class="smalltext"><a href="$scripturl,v=admin,a=banlog">$banninglog</a></span></td>
        <td><span class="smalltext"><b>$admintxt[27]</b></span></td>
       </tr><tr>
        <td align="right"><span class="smalltext"><a href="$scripturl,v=admin,a=ban">$banlog</a></span></td>
        <td><span class="smalltext"><b>$admintxt[25]</b></span></td>
        <td align="right"><span class="smalltext"><a href="$scripturl,v=admin,a=errorlog">$felog</a></span></td>
        <td><span class="smalltext"><b>$admintxt[26]</b></span></td>
       </tr><tr>
        <td align="right"><span class="smalltext"><a href="$scripturl,v=admin,a=referals">$referals</a></span></td>
        <td><span class="smalltext"><b>$admintxt[29]</b></span></td>
        <td align="right"><span class="smalltext"><a href="$scripturl,v=admin,a=referals">$areferals</a></span></td>
        <td><span class="smalltext"><b>$admintxt[28]</b></span></td>
       </tr><tr>
        <td colspan="4"><hr class="hr" size="1"></td>
       </tr><tr>
        <td colspan="4" align="center"><span class="smalltext"><a href="$scripturl,v=admin,a=attlog"><b>$admintxt[24]</b></a></span></td>
       </tr>
      </table>
     </td>
    </tr><tr>
     <td class="titlebg"><img src="$images/thumbup.gif"> $admintxt[162]</td>
    </tr><tr>
     <td class="win">
      <table cellpadding="2" cellspacing="0" width="100%">
       <tr>
        <td align="right" width="45%" valign="top"><span class="smalltext"><b>$admintxt[32]:</b></span></td>
        <td valign="top"><span class="smalltext">$bdmods</span></td>
       </tr><tr>
        <td align="center" colspan="2"><span class="smalltext"><b><a href="$scripturl,v=admin,a=sourcemod">$admintxt[50]</a></b></span></td>
       </tr><tr>
        <td align="right" width="45%" valign="top"><span class="smalltext"><b>$admintxt[34]:</b></span></td>
        <td valign="top"><span class="smalltext">$themes</span></td>
       </tr><tr>
        <td align="center" colspan="2"><span class="smalltext"><b><a href="$scripturl,v=admin,a=themesman">$admintxt[47]</a></b></span></td>
       </tr>
      </table>
     </td>
    </tr>
   </table>
  </td>
  <td width="50%" valign="top">
   <table width="100%" cellpadding="4" cellspacing="1" class="border">
    <tr>
     <td class="titlebg"><img src="$images/lamp.gif"> $admintxt[15]</td>
    </tr><tr>
     <td class="win">
      <table cellpadding="2" cellspacing="0" width="100%">
       <tr>
        <td align="right" width="45%" valign="top"><span class="smalltext"><b>$admintxt[17]:</b></span></td>
        <td><span class="smalltext">$regto (<a href="mailto:$eadmin">e-mail</a>)</span></td>
       </tr><tr>
        <td colspan="2"><hr class="hr" size="1"></td>
       </tr><tr>
        <td align="right" width="45%" valign="top"><span class="smalltext"><b>$admintxt[18]:</b></span></td>
        <td valign="top"><span class="smalltext">$adminlist</span></td>
       </tr><tr>
        <td colspan="2" align="center"><span class="smalltext"><b><a href="$scripturl,v=members,a=groups">$admintxt[19]</a></b></span></td>
       </tr>
      </table>
     </td>
    </tr><tr>
     <td class="titlebg"><img src="$images/xx.gif"> <b>$admintxt[186]</b></td>
    </tr><tr>
     <td class="win">
      <table cellpadding="2" cellspacing="0" width="100%">
       <tr class="catbg">
        <td><b>$admintxt[180]</b></td>
        <td><b>$gtxt{18}</b></td>
        <td><b>$admintxt[130]</b></td>
       </tr>
EOT
	$curtime = time;
	fopen(FILE,"+<$prefs/AdminLog.txt");
	@log = <FILE>;
	chomp @log;
	truncate(FILE,0);
	seek(FILE,0,0);
	foreach(@log) {
		($timel,$userl,$ipl) = split(/\|/,$_);
		if($userl eq $username && $ipl eq $ENV{'REMOTE_ADDR'} && $timel+3600 > $curtime) { print FILE "$curtime|$userl|$ipl\n"; $alin = 1; }
			else { print FILE "$_\n"; }
	}
	if(!$alin) { print FILE "$curtime|$username|$ENV{'REMOTE_ADDR'}\n"; push(@log,"$curtime|$username|$ENV{'REMOTE_ADDR'}"); }
	fclose(FILE);

	foreach(reverse sort {$a <=> $b} @log) {
		($timel,$userl,$ipl) = split(/\|/,$_);
		$timel = get_date($timel);
		loaduser($userl);
		if($userset{$userl}->[1]) { $userl = qq~<a href="$scripturl,v=memberpanel,a=view,u=$userl" target="_parent">$userset{$userl}->[1]</a>~; }
		$ebout .= qq~<tr><td>$userl</td><td>$ipl</td><td>$timel</td></tr>~;
		++$counter;
		if($counter > 5) { last; }
	}
	$ebout .= <<"EOT";
      </table>
     </td>
    </tr><tr>
     <td class="titlebg"><img src="$images/question.gif"> $admintxt[146]</td>
    </tr><tr>
     <td class="win">
      <table cellpadding="2" cellspacing="0" width="100%"><tr>
        <td align="right" width="45%" valign="top"><span class="smalltext"><b>$admintxt[35]:</b></span></td>
        <td valign="top"><span class="smalltext">$version</span></td>
       </tr><td colspan="2" align="center"><span class="smalltext"><b><a href="$scripturl,v=admin,a=version">$admintxt[36]</a></span></td></tr>
      </table>
     </td>
    </tr>
   </table>
  </td>
 </tr>
</table>

<table align="center" width="600">
 <tr>
  <td valign="bottom" align="center" colspan="2"><br>
   <table class="border" cellpadding="0" cellspacing="1" width="100%">
    <tr>
     <td class="catbg"><table cellpadding="4" cellspacing="0" width="100%"><tr><td align="center" class="catbgtext"><span class="smalltext"><b>$admintxt[37]</b></span></td></tr></table></td>
    </tr><tr>
     <td class="win2" align="center">
      <table cellpadding="4" cellspacing="0" width="100%">
       <tr>
        <td colspan="2" class="win"><span class="smalltext"><div style="padding-bottom: 7px;">Obtaining <b>True Life</b> is as simple as <b>A - B - C</b></div><div style="float: left; padding-left: 5px; padding-right: 5px; line-height: 150%;"><b>A<br>B<br>C<br>D</b></div><div style="line-height: 150%">Admit that you are a <b>sinner</b> and that you need a <b>savior</b>.<br>Believe in your heart that <b>Jesus Christ is Lord</b>.<br><b>Confess</b> your sins and <b>commit your life to him</b>.<br><b>Do not wait</b> until tomorrow.</div><div style="padding-bottom: 7px;" align="right">"For GOD so <b>loved the world</b> that he <b>gave his only Son</b>, so that <b>everyone who believes</b> in him <b>will not perish</b> but have <b>eternal life</b>." - <b>John 3:16</b> NLT</div></span></td>
       </tr><tr>
        <td align="right" width="35%" valign="top"><span class="smalltext" style="cursor:default;"><b>$admintxt[38]:</b></span></td>
        <td valign="top"><span class="smalltext"><span style="cursor:hand;"><b>Jesus</b>, <a title="Allowing me to switch them from Gamers.com to Blah">JFA</a>, <a title="Their support of the Blah Project">FP Community</a>, <a title="Thanks for the eblah.com domain! :D">superfalon</a>, <a title="Justin's baby :D">Jennifer</a>, Beta Testers</span></span></td>
       </tr><tr>
        <td align="right" width="35%" valign="top" class="win"><span style="cursor:default;"><span class="smalltext"><b>$admintxt[39]:</b></span></span></td>
        <td valign="top" class="win"><span class="smalltext"><span style="cursor:default;">Justin</span></span></td>
       </tr><tr>
        <td align="right" width="35%" valign="top"><span style="cursor:default;"><span class="smalltext"><b>$admintxt[40]:</b></span></span></td>
        <td valign="top"><span class="smalltext"><span style="cursor:hand;"><a title="Helped a little with Attachments, various file opening routines, and text to URL.">Dave B.</a>, <a title="Did the help that I was too lazy to do. ;D">DeathBox</a>, <a title="Found some bugs for me. :) Thanks!!  Also, provides eblah.com hosting!">Tim Linden</a>, Tony, <a title="Very special thanks to Solid of FrontLevel.com for the new e-blah logo!">Solid</a></span></span></td>
       </tr>
      </table>
     </td>
    </tr>
   </table>
  </td>
 </tr><tr>
  <td class="smalltext" align="center"><br>$copyright</td>
 </tr>
</table>
EOT
	footerA();
	exit;
}

sub KILLSID {
	fopen(FILE,"$prefs/Sids.sid");
	while( $sid = <FILE> ) {
		chomp $sid;
		($tsid,$ip,$sidname,$t,$endtime) = split(/\|/,$sid);
		if($endtime > time) { $othersids .= "$sid\n"; }
	}
	fclose(FILE);
	fopen(FILE,"+>$prefs/Sids.sid");
	print FILE $othersids;
	fclose(FILE);
	redirect("$scripturl,v=admin,r=3");
}

sub Remem {
	opendir(DIR,"$members/");
	@list = readdir(DIR);
	closedir(DIR);
	$memcnt = 0;
	foreach (sort @list) {
		if($_ =~ s/.dat\Z//) {
			&loaduser($_);
			if($userset{$_}->[14] > $lucnt) {
				$luser = $_;
				$lucnt = $userset{$_}->[14];
			}
			if($userset{$_}->[1]) { ++$memcnt; $memlistc .= "$_\n"; }
		}
	}
	fopen(LIST,"+>$members/List.txt");
	print LIST "$memlistc";
	fclose(LIST);
	fopen(FILE,"+>$members/LastMem.txt");
	print FILE "$luser\n$memcnt";
	fclose(FILE);
	if($URL{'v'} eq 'memberpanel' || $URL{'a'} eq 'convert' || $URL{'a'} eq 'delspec' || $URL{'a'} eq 'removemembers') { return; }

	redirect("$scripturl,v=admin,r=1");
}

sub ClickLog {
	if($eclick != 1) { &error($admintxt[78]); }
	if($URL{'p'} eq 'delete') {
		fopen(FILE,">$prefs/ClickLog.txt");
		print FILE '';
		fclose(FILE);
		redirect("$scripturl,v=admin,r=3");
	}

	$title = $admintxt[79];
	&headerA;
	$ebout .= <<"EOT";
<script language="javascript">
<!-- //
function clear(url) {
 if(window.confirm("$admintxt[80]")) { location = url; }
}
// -->
</script>
<table class="border" cellpadding="4" cellspacing="1" align="center" width="98%">
 <tr>
  <td class="titlebg" colspan="6"><b><img src="$images/clicklog_sm.gif"> $title</b></td>
 </tr><tr>
  <td class="win" colspan="6"><span class="smalltext">$admintxt[81]<b><a href="$scripturl,v=admin,a=sets,l=log">$admintxt[82]</a></b>.</span></td>
 </tr><tr>
  <td class="catbg" colspan="6"><b>&#149; $admintxt[84]<span class="smalltext"> ($admintxt[83])</b></span></td>
 </tr><tr>
  <td class="win" colspan=6>
   <table cellpadding="3" cellspacing="0" width="100%">
EOT
	$totalips = 0;
	$counter = 0;
	fopen(FILE,"$prefs/ClickLog.txt");
	while(<FILE>) {
		chomp;
		($logtime,$ipaddy,$ref[$totalips],$page[$totalips],$info[$totalips]) = split(/\|/,$_);
		if($iplog{$ipaddy} eq '') {
			push(@ipaddyt,"$ipaddy");
			$iplog{$ipaddy} = 1;
		} else { ++$iplog{$ipaddy}; }
		if($page[$totalips] eq '') { $page[$totalips] = $admintxt[85]; }
		++$totalips;
	}
	fclose(FILE);
	if(!$totalips) { $totalips = 1; }

	foreach(@ipaddyt) { push(@ipaddy,"$iplog{$_}|$_"); }

	foreach(sort{$b <=> $a} @ipaddy) {
		($t,$ip) = split(/\|/,$_);
		$pper = ($iplog{$ip}/$totalips);
		$pper = sprintf("%.2f",($pper*100));
		$ebout .= <<"EOT";
    <tr>
     <td align=right width="25%"><b><a href="$scripturl,v=admin,a=advipsearch,s=advsearch,ip=$ip" title="$admintxt[86]">$ip</a></b> </td>
     <td width="50%"><img src="$images/bar.gif" width="$pper%" height="10" alt="$pper%"></td>
     <td width="25%"><span class="smalltext"> $iplog{$ip} $admintxt[87] ($pper%)</span></td>
    </tr>
EOT
		++$counter;
	}
		$ebout .= <<"EOT";
    <tr>
     <td width=75% colspan=2 align=right><b>$admintxt[88]: </b></td>
     <td width=25%><span class="smalltext"> $totalips $admintxt[87]</span></td>
    </tr><tr>
     <td width=75% colspan=2 align=right><b>$admintxt[89]: </b></td>
     <td width=25%><span class="smalltext"> $counter $admintxt[90]</span></td>
    </tr>
EOT
	$cntb = 0;
	$cnto = 0;
	for($i = 0; $i < @info; $i++) {
		$fndb = 0;
		$fndo = 0;
		$_ = $info[$i];
		$ost = $_;
		if($ost =~ s/Windows ([A-Za-z0-9. ]{2,8})//) { $os = "Windows $1"; }
		elsif($ost =~ s/Win([ 0-9.]{2,8})//) { $os = "Windows $1"; }
		elsif($ost =~ s/Linux([ 0-9.]{2,8})//) { $os = "Linux $1"; }
			else { $os = $var{'60'}; }
		if($ost =~ s/Opera ([0-9.A-Za-z]{2,9})// || $ost =~ s/Opera\/([0-9.A-Za-z]{2,9})//) { $browser = "Opera $1"; }
		elsif($ost =~ s/MSIE ([0-9.A-Za-z]{2,9})//) { $browser = "Microsoft Internet Explorer $1"; }
		elsif($ost =~ s/Netscape ([0-9.A-Za-z]{2,9})//) { $browser = "Netscape $1"; }
		elsif($ost =~ s/Gecko\/([0-9]{4})([0-9]{2})([0-9]{2})//) { $browser = "Gecko ($2.$3.$1)"; }
		elsif($ost =~ s/Gecko//) { $browser = "Gecko"; }
		elsif($ost =~ s/Mozilla\/([0-9.A-Za-z]{2,9})//) { $browser = "Mozilla $1"; }
		elsif($ost =~ s/PHP\/([0-9.A-Za-z]{2,9})//) { $browser = "PHP $1"; $os = "PHP $1"; }
		elsif($ost =~ s/bot//) { $browser = "Bot"; $os = "Bot"; }
			else { $browser = $var{'60'}; }
		if($os =~ s/NT 5.0//) { $os .= "2000"; }
		if($os =~ s/NT 5.1//) { $os .= "XP"; }
		if($os =~ s/NT 6.0//) { $os .= "Codename Longhorn"; }
		foreach (@osr) { if($os eq $_) { $fndo = 1; last; } }
		foreach (@tbro) { if($browser eq $_) { $fndb = 1; last; } }
		if(!$fndb) { push(@tbro,$browser); }
		if(!$fndo) { push(@osr,$os); }
		++$oscnt{$os};
		++$brcnt{$browser};
	}
	$oscnt = @osr;
	$browsercnt = @tbro;
	$ebout .= <<"EOT";
   </table>
  </td>
 </tr><tr>
  <td class="catbg" colspan="6"><b>&#149; $admintxt[91] ($oscnt)</b></td>
 </tr><tr>
  <td class="win2" colspan="6">
EOT
	foreach(sort @osr) {
		$percent = sprintf("%.2f",(($oscnt{$_}/$totalips)*100));
		$ebout .= qq~  <span class="smalltext">$oscnt{$_} $admintxt[87] ($percent%)  -&#155;  </span>$_<br>\n~;
	}
	$ebout .= <<"EOT";
  </td>
 </tr><tr>
  <td class="catbg" colspan="6"><b>&#149; $admintxt[92] ($browsercnt)</b></td>
 </tr><tr>
  <td class="win2" colspan="6">
EOT
	foreach(sort @tbro) {
		$percent = sprintf("%.2f",(($brcnt{$_}/$totalips)*100));
		$ebout .= qq~  <span class="smalltext">$brcnt{$_} $admintxt[87] ($percent%)  -&#155;  </span> $_<br>\n~;
	}
	for($i = 0; $i < @ref; $i++) {
		$fnd = 0;
		++$refers{$ref[$i]};
		foreach (@list) {
			if($_ eq "$ref[$i]") { $fnd = 1; last; }
		}
		if(!$fnd && $ref[$i] ne '') { push(@list,$ref[$i]); }
	}
	$refcnt = @list;
	$ebout .= <<"EOT";
  </td>
 </tr><tr>
  <td class="catbg" colspan="6"><b>&#149; $admintxt[93] ($refcnt)</b></td>
 </tr><tr>
  <td class="win2" colspan="6">
EOT
	$cnt = 0;
	foreach(@list) { $ebout .= qq~<span class="smalltext">$refers{$_} $admintxt[87] -&#155; </span><a href="$_">$_</a><br>~; }
	if($list[0] eq '') { $ebout .= "  $admintxt[94]\n"; }
	for($i = 0; $i < @page; $i++) {
		$fnd = 0;
		$page[$i] =~ s/^,//gsi;
		++$pv{"$page[$i]"};
		foreach (@nlist) {
			if($_ eq $page[$i]) { $fnd = 1; last; }
		}
		if(!$fnd && $page[$i] ne '') { push(@nlist,$page[$i]); }
	}
	$pagecnt = @nlist;
	$ebout .= <<"EOT";
  </td>
 </tr><tr>
  <td class="catbg" colspan="6"><b>&#149; $admintxt[95]<span class="smalltext"> ($pagecnt, $admintxt[83])</b></span></td>
 </tr><tr>
  <td class="win" colspan="6">
EOT
	foreach(@nlist) { push(@nlists,"$pv{$_}|$_"); }

	foreach (sort{$b <=> $a} @nlists) {
		($hits,$page) = split(/\|/,$_);
		$percent = sprintf("%.2f",(($hits/$totalips)*100));
		if($page eq $admintxt[85]) { $vurl = ''; $vname = $admintxt[85]; } else { $vurl = $page; $vname = $page; }
		$ebout .= qq~  <span class="smalltext">$hits $admintxt[87] ($percent%) -&#155; </span><a href="$rurl,$vurl">$vname</a><br>\n~;
	}
	if($page[0]) { $ebout .= <<"EOT";
 <tr>
  <td class="catbg" align="center" colspan="6"><span class="smalltext"><a href="javascript:clear('$scripturl,v=admin,a=clicklog,p=delete')"><b>$admintxt[96]</b></a></span></td>
 </tr>
EOT
	}
	$ebout .= <<"EOT";
  </td>
 </tr>
</table>
EOT
	&footerA;
	exit;
}

sub ErrorLog {
	if($URL{'p'} eq 'delete') {
		unlink("$prefs/ELog.txt");
		redirect("$scripturl,v=admin,r=3");
	}
	$title = $admintxt[117];
	&headerA;
	$ebout .= <<"EOT";
<script language="javascript">
<!-- //
function clear(url) {
 if(window.confirm("$admintxt[80]")) { location = url; }
}
// -->
</script>
<table class="border" cellpadding="4" cellspacing="1" width="98%" align="center">
 <tr>
  <td class="titlebg"><b><img src="$images/warning.gif"> $title</b></td>
 </tr><tr>
  <td class="win"><span class="smalltext">$admintxt[118]</span></td>
 </tr>
EOT

	fopen(FILE,"$prefs/ELog.txt");
	@elog = <FILE>;
	fclose(FILE);
	chomp @elog;
	if(!$elog[0] && $kelog) {
		$ebout .= <<"EOT";
 <tr>
  <td align="center" class="win2"><b>$admintxt[121]</b></td>
 </tr>
</table>
EOT
	} else {
		$ebout .= "</table>";
		$clog = 1;
		foreach(reverse @elog) {
			($desc,$reason,$logtime,$lusername,$url) = split(/\|/,$_);
			loaduser($lusername);
			if($userset{$lusername}->[1] ne '') { $lusername = qq~<a href="$scripturl,v=memberpanel,a=view,u=$lusername">$userset{$lusername}->[1]</a>~; }
				else { $lusername = $lusername; }
			++$counter;
			$timeoferror = &get_date($logtime);
			$reason = $reason ne '?' ? $reason = qq~<tr><td class="win" colspan="2"><span class="smalltext"><b>$reason</span></td></tr>~ : '';
			$ebout .= <<"EOT";
<br><table class="border" cellpadding="4" cellspacing="1" width="98%" align="center">
 <tr>
  <td width="5%" class="win" align="center"><b>$counter</b></td>
  <td class="win" width="95%">
   <table width="100%">
    <tr>
     <td width="25%"><span class="smalltext"><b>$admintxt[180]:</b> $lusername</span></td>
     <td width="40%"><span class="smalltext"><b>$admintxt[181]:</b> $timeoferror</span></td>
     <td width="35%"><span class="smalltext"><a href="$url">$url</a></b></span></td>
    </tr>
   </table>
  </td>
 </tr><tr>
  <td class="win2" colspan="2"><span class="smalltext">$desc</span></td>
 </tr>$reason
</table>
EOT
		}
	}


	if($clog) { $ebout .= <<"EOT";
<br><table class="border" cellpadding="4" cellspacing="1" width="98%" align="center">
 <tr>
  <td class="catbg" align="center"><span class="smalltext"><a href="javascript:clear('$scripturl,v=admin,a=errorlog,p=delete')"><b>$admintxt[96]</b></a></span></td>
 </tr>
</table>
EOT
	}

	&footerA;
	exit;
}

sub IPLog {
	if($URL{'p'} eq 'delete') {
		unlink("$prefs/IpLog.txt");
		redirect("$scripturl,v=admin,r=3");
	}
	$clog = 1;

	$title = $admintxt[125];
	&headerA;
	$ebout .= <<"EOT";
<script language="javascript">
<!-- //
function clear(url) {
 if(window.confirm("$admintxt[80]")) { location = url; }
}
// -->
</script>
<table class="border" cellpadding="4" cellspacing="1" width="700" align="center">
 <tr>
  <td class="titlebg" colspan="4"><b>$title</b></td>
 </tr><tr>
  <td class="win" colspan="4"><span class="smalltext">$admintxt[126]</span></td>
 </tr>
EOT

	if(!$logip) { $ebout .= <<"EOT";
 <tr>
  <td colspan="4" align="center" class="win"><b>$admintxt[120]</b></td>
 </tr>
EOT
		$clog = 0;
	}
		fopen(FILE,"$prefs/IpLog.txt");
		@iplog = <FILE>;
		fclose(FILE);
		if(!$iplog[0] && $logip == 1) { $ebout .= <<"EOT";
 <tr>
  <td colspan="4" align="center" class="win"><b>$admintxt[131]</b></td>
 </tr>
EOT
		$clog = 0;
	} else {
		# Get Pages
		$mupp = 50;
		$maxmessages = @iplog || 1;
		if($maxmessages < $mupp) { $URL{'p'} = 0; }
		$tstart = $URL{'p'} || 0;
		$link = "$scripturl,v=admin,a=iplog,s=$URL{'s'},p";
		if($tstart > $maxmessages) { $tstart = $maxmessages; }
		$tstart = (int($tstart/$mupp)*$mupp);
		if($tstart > 0) { $bk = ($tstart-$mupp); $pagelinks = qq~<a href="$link=$bk">&#171;</a> ~; }
		$counter = 1;
		for($i = 0; $i < $maxmessages; $i += $mupp) {
			if($i == $tstart || $maxmessages < $mupp) { $pagelinks .= qq~<b>$counter</b>, ~; $nxt = ($tstart+$mupp); }
				else { $pagelinks .= qq~<a href="$link=$i">$counter</a>, ~; }
			++$counter;
		}
		$pagelinks =~ s/, \Z//gsi;
		if(($tstart+$mupp) != $i) { $pagelinks .= qq~ <a href="$link=$nxt">&#187;</a>~; }
		$end = ($tstart+$mupp);
		$pagelinks .= " ($var{'92'} $tstart-$end $var{'93'} ".@iplog.")";

		$ebout .= <<"EOT";
 <tr>
  <td colspan="4" class="win2"><span class="smalltext"><b><img src="$images/board.gif"> $gtxt{'17'}:</b> $pagelinks</span></td>
 </tr><tr>
  <td class="catbg" align="center" width="25%"><span class="smalltext"><b>$admintxt[127]</b></span></td>
  <td class="catbg" align="center" width="20%"><span class="smalltext"><b>$admintxt[128]</b></span></td>
  <td class="catbg" align="center" width="25%"><span class="smalltext"><b>$gtxt{'18'}</b></span></td>
  <td class="catbg" align="center" width="30%"><span class="smalltext"><b>$admintxt[130]</b></span></td>
 </tr>
EOT
		@iplog = reverse @iplog;
		for($i = 0; $i < @iplog; ++$i) {
			if($i <= $end && $i > $tstart) { push(@log,$iplog[$i]); }
		}

		foreach(@log) {
			chomp;
			($lusername,$loginout,$ipaddr,$logtime) = split (/\|/,$_);
			loaduser($lusername);
			if($userset{$lusername}->[1] eq '') { $lusername = $lusername; }
				else { $lusername = qq~<a href="$scripturl,v=memberpanel,a=view,u=$lusername">$userset{$lusername}->[1]</a>~; }
			$loginout = $loginout == 1 ? $admintxt[132] : $admintxt[133];
			$loggedtime = &get_date($logtime);
			$ebout .= <<"EOT";
 <tr>
  <td class="win" align="center">$lusername</td>
  <td class="win2" align="center">$loginout</td>
  <td class="win" align="center">$ipaddr</td>
  <td class="win2" align="center"><span class="smalltext">$loggedtime</span></td>
 </tr>
EOT
		}
		$ebout .= <<"EOT";
 <tr>
  <td colspan="4" class="win"><span class="smalltext"><b><img src="$images/board.gif"> $gtxt{'17'}:</b> $pagelinks</span></td>
 </tr>
EOT
	}

	if($clog) { $ebout .= <<"EOT";
 <tr>
  <td class="catbg" align="center" colspan="4"><span class="smalltext"><b><a href="javascript:clear('$scripturl,v=admin,a=iplog,p=delete')">$admintxt[96]</a></b></span></td>
 </tr>
EOT
	}

	$ebout .= "</table>";
	&footerA;
	exit;
}

sub TempFormat {
	$_[0] =~ s/</&lt;/g;
	$_[0] =~ s/>/&gt;/g;
	$_[0] =~ s/"/\&quot;/g;
	$_[0] =~ s/\cM//g;
	return($_[0]);
}

sub Temp {
	fopen(FILE,"$templates/template.html") || fopen(FILE,"$root/template.html");
	while(<FILE>) { chomp; $temps1 .= $_."\n"; }
	fclose(FILE);
	$temps1 = TempFormat($temps1);

	fopen(FILE,"$templates/Smilies.html") || fopen(FILE,"$prefs/Smilies.html");
	while(<FILE>) { chomp; $temps2 .= $_."\n"; }
	fclose(FILE);
	$temps2 = TempFormat($temps2);

	fopen(FILE,"$templates/News.html") || fopen(FILE,"$prefs/News.html");
	while(<FILE>) { chomp; $temps3 .= $_."\n"; }
	fclose(FILE);
	$temps3 = TempFormat($temps3);

	fopen(FILE,"$templates/admintemplate.css");
	while(<FILE>) { chomp; $temps4 .= $_."\n"; }
	fclose(FILE);
	$temps4 = TempFormat($temps4);

	fopen(FILE,"$templates/template.css");
	while(<FILE>) { chomp; $temps5 .= $_."\n"; }
	fclose(FILE);
	$temps5 = TempFormat($temps5);

	fopen(FILE,"$prefs/RTemp.txt");
	while(<FILE>) { chomp; $temps6 .= $_."\n"; }
	fclose(FILE);
	$temps6 =~ s/<br>/\n/g;
	$temps6 = TempFormat($temps6);

	$title = $admintxt[135];
	&headerA;
	$ebout .= <<"EOT";
<script language="javascript">
<!-- //
function GetTemplate(gettemplate,tempdesc,tempdesc2) {
	oldputaway = document.poster.putaway;
	tempeditor = document.poster.tempedit;

	// Lets put away the old ones ... and get rdy for new ones!
	if(oldputaway.value == 1) { document.poster.temp1.value = tempeditor.value; }
	else if(oldputaway.value == 2) { document.poster.temp2.value = tempeditor.value; }
	else if(oldputaway.value == 3) { document.poster.temp3.value = tempeditor.value; }
	else if(oldputaway.value == 4) { document.poster.temp4.value = tempeditor.value; }
	else if(oldputaway.value == 5) { document.poster.temp5.value = tempeditor.value; }
	else if(oldputaway.value == 6) { document.poster.temp6.value = tempeditor.value; }

	if(gettemplate == 1) { tempeditor.value = document.poster.temp1.value; }
	else if(gettemplate == 2) { tempeditor.value = document.poster.temp2.value; }
	else if(gettemplate == 3) { tempeditor.value = document.poster.temp3.value; }
	else if(gettemplate == 4) { tempeditor.value = document.poster.temp4.value; }
	else if(gettemplate == 5) { tempeditor.value = document.poster.temp5.value; }
	else if(gettemplate == 6) { tempeditor.value = document.poster.temp6.value; }
	oldputaway.value = gettemplate;
	if(tempdesc) {
		document.getElementById('tempdesc').innerHTML = tempdesc;
		document.getElementById('tempdesc2').innerHTML = tempdesc2;
	}
}
// -->
</script>

<table width="98%" cellpadding="0" cellspacing="1" class="border" align="center">
 <tr>
  <td class="titlebg" style="padding: 5px;"><img src="$images/xx.gif"> $admintxt[135]</td>
 </tr><tr>
  <td class="win" style="padding: 5px;"><span class="smalltext">$admintxt[136]:<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>&lt;blah v="</i><b>\$variable</b><i>"&gt;</i></span></td>
 </tr><tr><form action="$scripturl,v=admin,a=temp2" method="post" name="poster" onSubmit="GetTemplate(0)">
  <td class="win2">
   <table cellpadding="0" cellspacing="0" width="100%">
    <tr>
     <td class="win" width="175" valign="top" rowspan="2"><input type="hidden" name="putaway">
     <div class="titlebg" style="padding: 5px;"><b>HTML Templates</b></div><div style="padding: 10px; line-height: 150%;">
     <a href="#" onClick="GetTemplate('1','$admintxt[197]','<b>$admintxt[194]:</b> <a href=$templatesu/template.html target=_blank>$templates/template.html</a>')">$admintxt[197]</a><input type="hidden" name="temp1" value="$temps1"><br>
     <a href="#" onClick="GetTemplate('2','$admintxt[138]','<b>$admintxt[194]:</b> <a href=$templatesu/Smilies.html target=_blank>$templates/Smilies.html</a>')">$admintxt[138]</a><input type="hidden" name="temp2" value="$temps2"><br>
     <a href="#" onClick="GetTemplate('3','$admintxt[182]','<b>$admintxt[194]:</b> <a href=$templatesu/News.html target=_blank>$templates/News.html</a>')">$admintxt[182]</a><input type="hidden" name="temp3" value="$temps3"></div>
     <div class="titlebg" style="padding: 5px;"><b>CSS Templates</b></div><div style="padding: 10px; line-height: 150%;">
     <a href="#" onClick="GetTemplate('4','$admintxt[193]','<b>$admintxt[194]:</b> <a href=$templatesu/admintemplate.css target=_blank>$templates/admintemplate.css</a>')">$admintxt[193]</a><input type="hidden" name="temp4" value="$temps4"><br>
     <a href="#" onClick="GetTemplate('5','$admintxt[192]','<b>$admintxt[194]:</b> <a href=$templatesu/template.css target=_blank>$templates/template.css</a>')">$admintxt[192]</a><input type="hidden" name="temp5" value="$temps5"></div>
     <div class="titlebg" style="padding: 5px;"><b>Various Templates</b></div><div style="padding: 10px; line-height: 150%;">
     <a href="#" onClick="GetTemplate('6','$admintxt[191]','$admintxt[190]')">$admintxt[191]</a><input type="hidden" name="temp6" value="$temps6"></div><br>
     </td>
     <td class="catbg" style="padding: 5px;"><b><span id="tempdesc">$admintxt[195]</span></b><div id="tempdesc2" class="smalltext" style="padding: 5px;"></div></td>
    </tr><tr>
     <td valign="top" style="padding: 5px;"><textarea name="tempedit" style="width: 100%; height: 200px;"></textarea><br></td>
    </tr>
   </table>
  </td>
 </tr><tr>
  <td class="win2" align="center" style="padding: 5px;"><input type="submit" class="button" name="submit" value=" $admintxt[139] "></td>
 </form></tr>
</table>
EOT
	&footerA;
	exit;
}

sub Temp2 {
	fopen(FILE,"+>$templates/template.html") || fopen(FILE,"+>$root/template.html");
	print FILE $FORM{'temp1'};
	fclose(FILE);

	fopen(FILE,"+>$templates/Smilies.html") || fopen(FILE,"+>$prefs/Smilies.html");
	print FILE $FORM{'temp2'};
	fclose(FILE);

	fopen(FILE,"+>$templates/News.html") || fopen(FILE,"+>$prefs/News.html");
	print FILE $FORM{'temp3'};
	fclose(FILE);

	fopen(FILE,"+>$templates/admintemplate.css");
	print FILE $FORM{'temp4'};
	fclose(FILE);

	fopen(FILE,"+>$templates/template.css");
	print FILE $FORM{'temp5'};
	fclose(FILE);

	fopen(FILE,"+>$prefs/RTemp.txt");
	print FILE Format($FORM{'temp6'});
	fclose(FILE);
	redirect("$scripturl,v=admin,r=3");
}

sub Repop {
	if($URL{'p'} eq '') {
		$title = $admintxt[65];
		&headerA;
		$ebout .= <<"EOF";
<table class="border" cellpadding="4" cellspacing="1" align="center" width="500">
 <tr>
  <td class="titlebg"><b><img src="$images/brd_main.gif"> $title</b></td>
 </tr><tr>
  <td class="win"><span class="smalltext">$admintxt[141]</span></td>
 </tr><tr>
  <td class="win2"><b><img src="$images/brd_main.gif"> <a href="$scripturl,v=admin,a=repop,p=start">$admintxt[142]</a></b><hr class="hr" size="1"><span class="smalltext"><img src="$images/warning_sm.gif"> $admintxt[143]</span></td>
 </tr>
</table>
EOF
		&footerA;
		exit;
	}

	fopen(FILE,">$root/Maintance.lock"); # Start MM
	print FILE "\$maintance = 1;\n\$maintancer = qq~$admintxt[144]~;\n1;";
	fclose(FILE);

	foreach(@boardbase) {
		($cgs) = split("/",$_);
		fopen(FILE,"$boards/$cgs.msg");
		@msg = <FILE>;
		fclose(FILE);
		chomp @msg;
		$number = 0;
		$replycount = 0;
		@output = '';
		$puser = 0;
		$ptime = 0;
		foreach $msgs (@msg) {
			($tmess,$a,$b,$c,$replies,$poll,$f,$g) = split(/\|/,$msgs);
			$curnumber = -1;
			if($inu{$tmess}) { next; }
			if(!-s "$messages/$tmess.txt") { next; }
			fopen(FILE,"$messages/$tmess.txt");
			while( $countz = <FILE> ) {
				chomp $countz;
				($posted,$t,$t,$t,$lp) = split(/\|/,$countz);
				++$curnumber;
			}
			fclose(FILE);
			$replycount = $curnumber+$replycount;
			++$number;
			$inu{$tmess} = 1;
			if($poll && !$polltop) { # Count the polls
				fopen(FILE,"$messages/$tmess.polled");
				@last = <FILE>;
				fclose(FILE);
				chomp @last;
				$lloc = @last-1;
				($poller,$ptime) = split(/\|/,$last[$lloc]);
				if($ptime && $ptime > $lp) { $posted = $poller; $lp = $ptime; }
			}
			push(@output,"$lp|$tmess|$a|$b|$c|$curnumber|$poll|$f|$g|$posted");
		}
		$find = 0;
		fopen(FILE,"+>$boards/$cgs.msg");
		foreach $last (sort{$b <=> $a} @output) {
			($lp,$tmess,$a,$b,$c,$curnumber,$e,$f,$g,$posted) = split(/\|/,$last);
			if($find == 0) { $puser = $posted; $ptime = $lp; ++$find; }
			if($tmess eq '') { next; }
			print FILE "$tmess|$a|$b|$c|$curnumber|$e|$f|$g|$lp|$posted\n";
		}
		fclose(FILE);
		$replycount = $replycount+$number;
		fopen(FILE,"+>$boards/$cgs.ino");
		print FILE "$number\n$replycount\n";
		fclose(FILE);
	}
	unlink("$root/Maintance.lock"); # End MM
	if($URL{'a'} eq 'remove') { return; } # If removing old threads
	redirect("$scripturl,v=admin,r=2");
}

sub Version {
	opendir(DIR,$code);
	@alldir = readdir(DIR);
	closedir(DIR);
	++$count;
	foreach(@alldir) {
		($good,$ext) = split(/\./,$_);
		$size += -s("$code/$_");
		fopen(FILE,"$code/$_");
		while(<FILE>) { ++$lines; }
		fclose(FILE);
		if($ext eq 'pl') { ++$count; }
	}
	fopen(FILE,"Blah.$scripttype");
	while(<FILE>) { ++$lines; }
	fclose(FILE);
	$size += -s("Blah.$scripttype");
	$lines = MakeComma($lines-(($count*11)-23));
	$size = sprintf("%.2f",($size/1024))." KB (".MakeComma($size)." bytes)";

	$bversion = $bversion < $theblahver ? qq~<span style="color: red;"><b>$admintxt[179]</b></span>~ : $bversion;

	CoreLoad('Themes');

	$lngversion = $lngversion ? $lngversion : '2';

	$title = $admintxt[146];
	&headerA;
	$ebout .= <<"EOT";
<table class="border" cellpadding="4" cellspacing="1" width="500" align="center">
 <tr><form action="http://www.eblah.com/?v=check" method="post" name="check" target="_parent">
  <td class="titlebg"><b><img src="$images/question.gif"> $title</b></td>
 </tr><tr>
  <td class="win"><span class="smalltext">$admintxt[147]</span></td>
 </tr><tr>
  <td class="catbg"><b><img src="$images/open_thread.gif"> e-blah! $admintxt[148]</b></td>
 </tr><tr>
  <td class="win2"><input type="hidden" name="version" value="$version"><table width="100%">
   <tr>
    <td align="right" width="150"><span class="smalltext"><b>$admintxt[153]:</b></span></td>
    <td><span class="smalltext">$size</span></td>
   </tr><tr>
    <td align="right" width="150"><span class="smalltext"><b>$admintxt[178]:</b></span></td>
    <td><span class="smalltext">$count</span></td>
   </tr><tr>
    <td align="right" width="150"><span class="smalltext"><b>$admintxt[177]:</b></span></td>
    <td><span class="smalltext">$lines</span></td>
   </tr></tr>
    <td colspan="2"><hr class="hr" size="1"></td>
   </tr><tr>
    <td align="right" width="150"><span class="smalltext"><b>$admintxt[149]:</b></span></td>
    <td><span class="smalltext">$version</span></td>
   </tr><tr>
    <td align="right" width="150"><span class="smalltext"><b>$admintxt[150]:</b></span></td>
    <td><span class="smalltext">$bversion</span></td>
   </tr><tr>
    <td align="right" width="150"><span class="smalltext"><b>$admintxt[151]:</b></span></td>
    <td><span class="smalltext">$themever &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <b>$admintxt[152]:</b> $themesupp</span></td>
   </tr><tr>
    <td align="right" width="150"><span class="smalltext"><b>Language Pack:</b></span></td>
    <td><span class="smalltext">$lngversion</span></td>
   </tr></tr>
    <td colspan="2"><hr class="hr" size="1"></td>
   </tr><tr>
    <td align="right" width="150"><span class="smalltext"><b>Perl:</b></span></td>
    <td><span class="smalltext">$]</span></td>
   </tr><tr>
    <td align="right" width="150"><span class="smalltext"><b>Operating System:</b></span></td>
    <td><span class="smalltext">$^O</span></td>
   </tr>
  </table></td>
 </tr><tr>
  <td class="win"><input type="submit" class="button" value=" $admintxt[155] "></td>
 </tr>
</table>
EOT
	&footerA;
	exit;
}

sub News {
	if($URL{'p'} == 2) {
		fopen(FILE,"+>$prefs/News.txt");
		print FILE $FORM{'news'};
		fclose(FILE);
		redirect("$scripturl,v=admin,r=3");
	}
	fopen(FILE,"$prefs/News.txt");
	@news = <FILE>;
	fclose(FILE);
	chomp @news;
	foreach(@news) { $news .= "$_\n"; }
	$news =~ s/\cM//g; # For non-ASCII files . . .
	$title = $admintxt[49];
	&headerA;
	$ebout .= <<"EOT";
<table class="border" cellspacing="1" cellpadding="4" width="550" align="center">
 <tr>
  <td class="titlebg"><b><img src="$images/news.gif"> $title</b></td>
 </tr><tr>
  <td class="win"><span class="smalltext">$admintxt[157]</span></td>
 </tr><tr><form action="$scripturl,v=admin,a=news,p=2" method="post" name="msend">
  <td class="win2" align="center"><textarea name="news" wrap="virtual" rows="8" cols="100">$news</textarea></td>
 </tr><tr>
  <td class="win"><input type="submit" class="button" value=" $admintxt[139] "></td>
 </form></tr>
</table>
EOT
	&footerA;
	exit;
}

sub Reserve {
	if($FORM{'cnt'}) {
		for($i = 1; $i <= $FORM{'max'}; ++$i) {
			if($FORM{"del_$i"}) { next; }
			$search = Format($FORM{"search_$i"});
			push(@pdata,qq~$search|$FORM{"box_$i"}~);
		}
		if($FORM{'search_new'}) { push(@pdata,"$FORM{'search_new'}|$FORM{'box_new'}"); }

		fopen(FILE,">$prefs/Names.txt");
		foreach(@pdata) { print FILE "$_\n"; }
		fclose(FILE);
		redirect("$scripturl,v=admin,a=reserve");
	}

	$title = $admintxt[171];
	&headerA;
	$ebout .= <<"EOT";
<table cellpadding="4" cellspacing="1" class="border" align="center" width="500">
 <tr>
  <td class="titlebg"><b><img src="$images/mem_main.gif"> $title</b></td>
 </tr><tr><form action="$scripturl,v=admin,a=reserve" method="POST">
  <td class="win"><span class="smalltext">$admintxt[172]</span></td>
 </tr><tr>
  <td class="win2">
   <table cellpadding="3" cellspacing="0" width="100%">
    <tr>
     <td width="44%"><span class="smalltext"><b>$admintxt[174]</b></span></td>
     <td width="44%" align="center"><span class="smalltext"><b>$admintxt[175]</b></span></td>
     <td width="12%" align="center"><span class="smalltext"><b>$admintxt[173]</b></span></td>
    </tr>
EOT
	$count = 0;
	fopen(FILE,"$prefs/Names.txt");
	while(<FILE>) {
		chomp $_;
		($searchme,$within) = split(/\|/,$_);
		$checked = $within ? ' checked' : '';
		++$count;
		$ebout .= <<"EOT";
    <tr>
     <td width="44%"><input type="text" class="textinput" name="search_$count" value="$searchme" size="30"></td>
     <td width="44%" align="center"><input type="checkbox" class="checkboxinput" name="box_$count" value="1"$checked></td>
     <td width="12%" align="center"><input type="checkbox" class="checkboxinput" name="del_$count" value="1"></td>
    </tr>
EOT
	}
	if($count <= 0) {
		$ebout .= <<"EOT";
    <tr>
     <td colspan="3" align="center"><span class="smalltext"><br>$admintxt[176]<br><br></span></td>
    </tr>
EOT
	}
	fclose(FILE);
	$ebout .= <<"EOT";
   </table>
  </td>
 </tr><tr>
  <td class="win">
   <table cellpadding="3" cellspacing="0" width="100%">
    <tr>
    <tr>
     <td width="44%"><input type="text" class="textinput" name="search_new" value="" size="30"></td>
     <td width="44%" align="center"><input type="checkbox" class="checkboxinput" name="box_new" value="1"></td>
     <td width="12%">&nbsp;</td>
    </tr>
   </table>
  </td>
 </tr><tr>
  <td class="win2" align="center"><input type="hidden" name="max" value="$count"><input type="hidden" name="cnt" value="1"><input type="submit" class="button" value=" $admintxt[139] "></td>
 </tr>
</table>
EOT
	&footerA;
	exit;
}

sub BoardBackup { # Backup Routines
	$title = $admintxt[66];
	&headerA;
	$ebout .= <<"EOT";
<script language="javascript1.2">
<!--
function togledisable() {
 for(i = 0; i < document.post.elements.length; i++) {
  if(document.post.elements[i].disabled == true || document.post.elements[i].name == "broot" || document.post.elements[i].name == "submit") { document.post.elements[i].disabled = false; }
   else { document.post.elements[i].disabled = true; }
 }
 document.post.fname.disabled = false;
}
-->
</script>
<table class="border" cellpadding="4" cellspacing="1" width="500" align="center">
 <tr>
  <td class="titlebg" colspan="2"><b><img src="$images/open_thread.gif"> $title</b></td>
 </tr><tr>
  <td class="win"><span class="smalltext">$backupt[2]</span></td>
 </tr><form action="$scripturl,v=admin,a=bkupstart" method="post" name="post"><tr>
  <td class="titlebg"><b>$backupt[10]</b></td>
 </tr><tr>
  <td class="win2">
   <table width="100%">
    <tr>
     <td align="right" width="50%" class="win2"><b>$backupt[22]:</b></td>
     <td class="win2" valign="top"><input type="text" class="textinput" value="backup" name="fname"></td>
    </tr><tr>
     <td align="right" width="50%" class="win2"><b>$backupt[20]</b></td>
     <td class="win2" valign="top"><input type="checkbox" class="checkboxinput" value="1" name="broot" onClick="togledisable();" checked></td>
    </tr>
   </table>
  </td>
 </tr>
 <tr>
  <td class="win">
   <table width="100%">
    <tr>
     <td width="50%" align="right"><b>Boards:</b></td>
     <td><input type="checkbox" class="checkboxinput" value="1" name="boards" disabled></td>
    </tr><tr>
     <td align="right"><b>Members:</b></td>
     <td><input type="checkbox" class="checkboxinput" value="1" name="members" disabled></td>
    </tr><tr>
     <td align="right"><b>Messages:</b></td>
     <td><input type="checkbox" class="checkboxinput" value="1" name="messages" disabled></td>
    </tr><tr>
     <td align="right"><b>Prefs:</b></td>
     <td><input type="checkbox" class="checkboxinput" value="1" name="prefs" disabled></td>
    </tr><tr>
     <td align="right"><b>Code:</b></td>
     <td><input type="checkbox" class="checkboxinput" value="1" name="code" disabled></td>
    </tr><tr>
     <td align="right"><b>Themes:</b></td>
     <td><input type="checkbox" class="checkboxinput" value="1" name="themes" disabled></td>
    </tr><tr>
     <td align="right"><b>Uploads:</b></td>
     <td><input type="checkbox" class="checkboxinput" value="1" name="uploads" disabled></td>
    </tr>
   </table>
  </td>
 </tr><tr>
  <td class="win2" align="right"><input type="submit" class="button" name="submit" value="$backupt[16]"></td>
 </form></tr>
</table>
EOT
	&footerA;
	exit;
}

sub BKUPStart {
	$FORM{'fname'} =~ s/ /_/g;
	$FORM{'fname'} =~ s/[#%+,\\\/:?"<>'|@^\$\&~'\)\(\]\[\;{}!`=-]//g;

	@backuparray = ("$root|$FORM{'broot'}|full", "$messages|$FORM{'messages'}|messages", "$code|$FORM{'code'}|code", "$boards|$FORM{'boards'}|boards", "$prefs|$FORM{'prefs'}|prefs", "$members|$FORM{'members'}|members", "$uploads|$FORM{'uploads'}|uploads", "$themes|$FORM{'themes'}|themes");

	foreach(@backuparray) {
		($arcdir,$doit,$arcname) = split(/\|/,$_);

		if($doit) {
			`tar -cf - "$arcdir" > "./$FORM{'fname'}_$arcname.tar"`;
			`gzip "./$FORM{'fname'}_$arcname.tar"`;
		}
	}

	redirect("$scripturl,v=admin,r=4");
}

sub Encrypt { # Password encryption .....
	if($pwcrypt eq '') { $pwcrypt = 'ya'; }

	fopen(FILE,"$members/List.txt");
	@membarlist = <FILE>;
	fclose(FILE);
	chomp @membarlist;

	foreach(@membarlist) {
		loaduser($_);
		if(substr($userset{$_}->[0],0,2) eq $pwcrypt && length($userset{$_}->[0]) == 13) { next; }
			else { # Encrypt me!
				$userset{$_}->[0] = crypt($userset{$_}->[0],$pwcrypt);
				fopen(FILE,">$members/$_.dat");
				for($q = 0; $q < $usersetcount; $q++) { print FILE "$userset{$_}->[$q]\n"; }
				fclose(FILE);
			}
	}
	CoreLoad('Admin2');
	&Settings3;
}
1;