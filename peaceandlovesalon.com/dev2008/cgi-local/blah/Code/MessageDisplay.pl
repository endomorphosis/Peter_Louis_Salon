################################################
# E-Blah Bulliten Board Systems       Platinum #
################################################
# Copyright (c) 2001 - 2004 e-blah!            #
# All Rights Reserved.                         #
################################################

CoreLoad('MessageDisplay',1);

sub MessageDisplay {
	$messid = $URL{'m'};
	fopen(FILE,"$boards/Stick.txt");
	@sticky = <FILE>;
	fclose(FILE);
	chomp @sticky;
	fopen(FILE,"$boards/$URL{'b'}.msg") || &error($ltxt[5]);
	while(<FILE>) {
		chomp;
		($messageid,$ptitle,$t,$t,$treplies,$tpoll,$type,$tmicon) = split(/\|/,$_);
		if($found) { $nextmessage = $messageid; last; }
		if($messid eq 'latest') { $messid = $messageid; $URL{'m'} = $messid; $URL{'s'} = $treplies; }
		if($messid == $messageid) {
			foreach $allstick (@sticky) {
				($trash,$m) = split(/\|/,$allstick);
				if($m eq $messid) { $stickme{$m} = 1; last; }
			}
			$poll = $tpoll; $replies = $treplies;
			$status = FindStatus($type,$poll);
			$title = $ptitle;
			$found = 1;
			$maxreplies = $treplies;
			$micon = $tmicon;
			$mlocked = $type;
		} else { $lastmessage = $messageid; }
	}
	fclose(FILE);
	if($found != 1 || !(-s("$messages/$messid.txt"))) { error("$messagedisplay[1]: $messid.txt",1); }

	if(!-e("$messages/$messid.view")) { # Create if need be ...
		fopen(XFILE,">$messages/$messid.view");
		fclose(XFILE);
	}
	fopen(ADD,"+<$messages/$messid.view");
	$curnumber = <ADD> || 0;
	seek(ADD,0,0);
	truncate(ADD,0);
	$viewcnt = $curnumber+1;
	print ADD $viewcnt,"\n";
	fclose(ADD);
	$viewcnt = MakeComma($viewcnt);

	# How many page links?
	$tmax = $totalpp*20;

	$treplies = ($maxreplies+1);
	if($treplies < $maxmess) { $URL{'s'} = 1; }
	$tstart = $URL{'s'} || 0;
	$link = "$scripturl,v=display,m=$messid,s";
	if($tstart > $treplies) { $tstart = $treplies; }
	$tstart = (int($tstart/$maxmess)*$maxmess);
	if($tstart > 0) { $bk = ($tstart-$maxmess); $pagelinks = qq~<a href="$link=$bk">&#171;</a> ~; }

	if((int($treplies/$maxmess) < $totalpp) < $totalpp) { $totalpp *= 2; }
	$beforeafter = int($totalpp/2);
	$counter = (($tstart/$maxmess)+1)-$beforeafter;
	if($counter < 1) { $counter = 1; }
	$mcnter = ($tstart-($maxmess*$beforeafter));
	if($mcnter < 1) { $mcnter = 0; }
	$counter2 = $counter;

	if($counter != 1) { $pagelinks .= qq~<a href="$link=0">...</a> ~; }

	for($i = $mcnter; $i < $treplies; $i += $maxmess) {
		if($URL{'s'} ne 'all' && $i == $tstart || $treplies < $maxmess) { $pagelinks .= qq~<b>$counter</b>, ~; $nxt = ($tstart+$maxmess); }
			else { $pagelinks .= qq~<a href="$link=$i">$counter</a>, ~; }

		++$counter;

		if($counter > $counter2+$beforeafter*2) {
			$gbk = (int($treplies/$maxmess)*$maxmess); # Last post in series
			$pagelinks =~ s/, \Z//gsi;
			$pagelinks .= qq~ <a href="$link=$gbk">...</a>~; last;
		}
	}
	$pagelinks =~ s/, \Z//gsi;
	if(($tstart+$maxmess) != $i && $URL{'s'} ne 'all') { $pagelinks .= qq~ <a href="$link=$nxt">&#187;</a>~; }
	if($treplies > $maxmess && !$sall) { $pagelinks .= $URL{'s'} ne 'all' ? qq~ : <a href="$link=all">$messagedisplay[2]</a>~ : qq~ : <b>$messagedisplay[2]</b>~; }

	$title = CensorList($title);
	&LogPage;
	&header;
	&Mods;
	if($modz) { $modz = qq~<div style="float: right"><b>$ltxt[7]:</b> $modz&nbsp;</div>~; }
	$ebout .= <<"EOT";
<script language="javascript">
<!-- //
function clear(o,url2) {
 if(o == 'all') { url = '$scripturl,v=mod,a=delthread,m=$messid'; }
 else if(o == '') { url = url2; }
  else { url = '$scripturl,v=mod,a=delnum,p=1,m=$messid,n='+o; }
 if(window.confirm('$messagedisplay[23]')) { location = url; }
}
// -->
</script>

<table cellspacing="1" width="100%" class="border">
 <tr>
  <td class="win" height="22"><span class="smalltext"><div style="float: left">&nbsp;<a href="$surl">$gtxt{'44'}</a> &nbsp;<b>&#155;</b> &nbsp;<a href="$surl,c=$catid">$catname</a> &nbsp;<b>&#155;</b> &nbsp;<a href="$scripturl">$boardnm</a> &nbsp;<b>&#155; &nbsp;$title</b></div>$modz</span></td>
 </tr>
EOT
	if($showactive) {
		BoardUsers();
		$ebout .= <<"EOT";
 <tr>
  <td class="win"><span class="smalltext">
   <div class="win2" style="padding: 5px"><b>$var{'90'}</b></div>
   <div style="padding: 5px">$activeusers $var{'97'} $gcnt $var{'91'}</div></span>
  </td>
 </tr>
EOT
	}
	$ebout .= '</table><br>';

	if($poll) { CoreLoad('Poll'); &PollDisplay; }
	if(($binfo[4] == 1 && $username ne 'Guest') || ($binfo[4] == 2 && $settings[4] eq 'Administrator') || ($binfo[4] == 4 && ($settings[4] eq 'Administrator' || $ismod)) || ($binfo[4] eq '' || $binfo[4] == 0) && !$mlocked && $binfo[4] != 3) { $allowposts = qq~<a href="$scripturl,v=post,m=$messid">$img{'reply'}</a> $msp~; }
	if($username ne 'Guest' && !$nonotify) { $notify = qq~<a href="$scripturl,v=memberpanel,a=notify,m=$messid">$img{'notify'}</a> $msp~; }
	$print_reply = qq~$allowposts$notify<a href="$scripturl,v=recommend,m=$messid">$img{'recommend'}</a> $msp<a href="$scripturl,v=print,m=$messid">$img{'print'}</a>~;
	if($sview) { $views = qq~<span class="smalltext">&nbsp; $messagedisplay[8] <b>$viewcnt</b> $messagedisplay[9]</span>~; }
	$ebout .= <<"EOT";
<table cellpadding="3" cellspacing="1" class="border" width="100%">
 <tr>
  <td class="titlebg" colspan="3"><span class="smalltext"><div style="float: left; padding: 3px;"><img src="$images/$status.gif" align="left"> &nbsp;<b>$gtxt{'17'}:</b> $pagelinks</div><div style="float: right">$print_reply</div></span></td>
 </tr><tr>
  <td bgcolor="$color{'catbg'}" class="catbg"><b>&nbsp;<img src="$images/profile_sm.gif"> $gtxt{'36'}</b></td>
  <td bgcolor="$color{'catbg'}" class="catbg" colspan="2">&nbsp;<img src="$images/$micon.gif">&nbsp;<b> $title</b>$views</td>
 </tr>
EOT
	$hr = qq~<hr color="$color{'border'}" class="hr" size="1">~;

	$stick = qq~<a href="$scripturl,v=mod,a=sticky,m=$messid,s=$URL{'s'}">$img{'stick'}</a>~;
	if($ismod) { $ipon = 1; }
	if($modon) {
		$ismod = 1;
		$stick = $ston ? " $msp $stick" : '';
	}
	elsif($ston) { $stick = <<"EOT";
     <td><span class="smalltext"><b>$messagedisplay[11]:</span></td>
     <td><span class="smalltext">$stick</span></td>
EOT
	}
		else { $stick = " $msp $stick"; }

	$counter = 0;
	$counter2 = 0;
	fopen(FILE,"$messages/$messid.txt");
	while($append = <FILE>) {
		++$counter;
		if($URL{'s'} eq 'all' || ($counter > $tstart && $counter2 < $maxmess)) {
			push(@msgs,$append);
			++$counter2;
		}
	}
	fclose(FILE);
	chomp @msgs;

	if($memgrpp[6]) { $team1 = qq~<img src="$images/team.gif" alt="$gtxt{'29'}"> ~; }

	$counter = $tstart;
	if($URL{'highlight'} && $URL{'highlight'} !~ /[#%,\\\/:?"<>'|@^\$\&~'\)\(\]\[\;{}!`=-]/) { @lights = split(/\+/,$URL{'highlight'}); } # Highlighter
	foreach(@msgs) {
		$extra = '';
		($user,$message,$ipaddr,$email,$fdate,$nosmile,$t,$t,$afile,$modified) = split(/\|/,$_);
		loaduser($user);
		if($userset{$user}->[1] eq '') { $guestuser = 1; $user2 = 'Guest'; } else { $user2 = $user; }

		# Show the cool bar ...
		if($showbar) { $ebout .= qq~<tr><td height="5" colspan="3" class="win3"></td></tr>~; } else { $showbar = 1; }

		# Block the users now ...
		$quit = 0;
		if($username ne 'Guest' && $URL{'override'} ne $counter && ($URL{'override2'} ne $user2)) {
			foreach(@blockedusers) {
				if($_ eq $user || ($guestuser && $_ eq 'Guest')) {
					loaduser($_);
					$ebout .= <<"EOT";
<tr>
 <td colspan="3" align="center" class="win2"><br><b>$userset{$_}->[1]</b> ($_) $messagedisplay[39] (<b><a href="$scripturl,v=memberpanel,a=save,as=forum,s=messageblock,caller=10,remove=$_">$messagedisplay[40]</a></b>).<br>$messagedisplay[41] <b><a href="$scripturl,v=$URL{'v'},b=$URL{'b'},m=$URL{'m'},s=$URL{'s'},override=$counter">$messagedisplay[42]</a></b>, $messagedisplay[43] <b><a href="$scripturl,v=$URL{'v'},b=$URL{'b'},m=$URL{'m'},s=$URL{'s'},override2=$_">$messagedisplay[42]</a></b>.<br><br></td>
</tr>
EOT
					$quit = 1;
					++$counter;
					last;
				}
			}
			if($quit) { next; }
		}

		$message =~ s/\&#124;/|/g;
		&BC;
		($message,$message2) = ($message2,$message);

		if($URL{'highlight'}) { Highlight($message2); }
		$nosmile = ($nosmile == 1 || $nosmile == 3) ? 1 : 0;
		loaduserdisplay($user);
		$date = get_date($fdate);

		$attachments = '';
		if($afile) {
			$attachments .= "<br><br><br>";
			foreach( split(/\//,$afile) ) {
				$attachments3 = '';
				if($_ && -e("$uploaddir/$_")) {
					fopen(FILE,"$prefs/Hits/$_.txt");
					@nump = <FILE>;
					fclose(FILE);
					chomp @nump;
					$nump = $nump[0];
					if($_ =~ /(jpg|jpeg|gif|art|bmp|png)\Z/) {
						++$nump;
						fopen(ADD,"+>$prefs/Hits/$_.txt");
						print ADD $nump,"\n";
						fclose(ADD);
						$attachments3 = qq~<tr><td class="win" colspan="2"><img src="$uploadurl/$_" border="0"></td></tr>~;
					}
					$size = sprintf("%.2f",(-s "$uploaddir/$_")/1024);
					$type = "KB";
					if($size > 1000) { $size = sprintf("%.2f",$size/1024); $type = "MB"; }
					if($settings[4] eq 'Administrator') { $attachments2 = qq~<a href="javascript:clear('','$scripturl,v=admin,a=attlog,p=del1,f=$_,m=$URL{'m'},s=$URL{'s'}');"><img src="$images/ban.gif" alt="$messagedisplay[45]" border="0"></a>~; }

					$attachments .= <<"EOT";
<table width="100%" cellpadding="5" cellspacing="0">
 <tr>
  <td class="win" width="40%"><table width="100%"><tr><td valign="top" width="18"><img src="$images/disk.gif"></td><td><b>$messagedisplay[12]:</b> <a href="$scripturl,v=download,f=$_" target="download">$_</a><br><b>$nump</b> $messagedisplay[46] &nbsp; - &nbsp; <b>$messagedisplay[47]:</b> $size $type</td><td align="center" valign="top">$attachments2</td></tr></table></td><td width="60%"></td>
 </tr>$attachments3
</table><br>
EOT
				}
			}
		}

		if($modified ne '') {
			@modified = split(/\>/,$modified);
			$total = @modified;
			$extra .= <<"EOT";
<tr>
 <td valign="bottom">$hr
  <table width="100%" cellpadding="4" cellspacing="0">
   <tr>
    <td class="catbg"><span class="smalltext"><b>$messagedisplay[48]</b> ($total $messagedisplay[49])</span></td>
   </tr><tr>
    <td class="win"><table cellpadding="4" cellspacing="0" width="100%">
EOT
			foreach(@modified) {
				($mod4,$mod2,$modreason) = split(/\//,$_);
				$modate = get_date($mod4);
				loaduser($mod2);
				if($userset{$mod2}->[1]) { $moduser = qq~<a href="$scripturl,v=memberpanel,u=$mod2,a=view">$userset{$mod2}->[1]</a>~; }
				$moduser = $moduser || $mod2;
				$extra .= qq~<tr><td class="smalltext">$moduser &nbsp;-&nbsp; $modate</td></tr>~;
				if($modreason) { $extra .= qq~<tr><td class="win2" valign="top"><span class="smalltext">$modreason</span></td></tr>~; }
			}
			$extra .= qq~</table></td></tr></table></td></tr>~;
		}

		if($settings[4] eq 'Administrator' || $ipon) { $ipaddr = qq~<a href="$scripturl,v=mod,a=ban,ip=$ipaddr,m=$messid">$ipaddr</a>~; }
			else { $ipaddr = $messagedisplay[32]; }
		if($counter > 0) { $replycnt = "<b>$gtxt{'37'}:</b> $counter - $maxreplies"; }

		$online = '';
		if($activemembers{$user} && !$userset{$user}->[18]) { $online = $gtxt{'30'}; }
		elsif($logactive && !$userset{$user}->[18] && $lastactive{$user}) { $online = qq~<a title="$messagedisplay[44]: $lastactive{$user}">$gtxt{'31'}</a>~; }

		$email = $guest ? qq~<a href="mailto:$email">$img{'email'}</a>~ : $memberemail;
		$email .= $pmdisable || $guest ? '' : qq~ <a href="$scripturl,v=memberpanel,a=pm,s=write,t=$user">$img{'pm'}</a>~;

		$modify = !$mlocked && (($settings[4] eq 'Administrator' || $ismod || $modifyon) || $username eq $user && (!$modifytime || $fdate+($modifytime*3600) > time)) ? qq~ $msp<a href="javascript:clear('$counter')">$img{'delete'}</a> $msp<a href="$scripturl,v=post,a=modify,m=$messid,n=$counter">$img{'modify'}</a>~ : '';

		$quote = !$mlocked ? qq~<a href="$scripturl,v=post,m=$messid,q=$counter">$img{'quote'}</a>~ : '';

		$homepage = ($userset{$user}->[19] && $userset{$user}->[20]) ? qq~<a href="$userset{$user}->[20]" title="$userset{$user}->[19]">$img{'site'}</a> ~: '';
		$message = $message2;
		$ebout .= <<"EOT";
<tr>
 <td class="win" width="180" align="center"><a name="num$counter"><b>$membername</b></a></td>
 <td class="win">
  <table cellpadding="0" cellspacing="2" width="100%">
   <tr>
    <td><span class="smalltext"><b>$gtxt{'34'}:</b> $date</span></td>
    <td align="right"><span class="smalltext">$quote$modify $msp<a href="$scripturl,v=report,m=$messid,n=$counter">$img{'report'}</a></span></td>
   </tr>
  </table>
 </td>
</tr><tr>
 <td class="win2" width="180" valign="top"><span class="smalltext">$mprofile</span></td>
 <td class="win2" height="100%" valign="top">
  <table cellspacing="0" cellpadding="3" width="100%" height="100%">
   <tr>
    <td valign="top" height="100%" class="postbody">$message$attachments</td>
   </tr>$extra
  </table>
 </td>
</tr><tr>
 <td class="win" width="180">
  <table width="100%">
   <tr>
    <td><span class="smalltext"><b><img src="$images/ip.gif"> $ipaddr</span></td>
    <td align="right"><span class="smalltext"><b>$online</b></span></td>
   </tr>
  </table>
 </td>
 <td class="win">
  <table width="100%">
   <tr>
    <td><span class="smalltext">$homepage$email$instmsg</span></td>
    <td align="right"><span class="smalltext">$replycnt</span></td>
   </tr>
  </table>
 </td>
</tr>
EOT
		++$counter;
	}

	$next = $nextmessage eq '' ? $messagedisplay[17] : qq~<b><a href="$scripturl,v=display,m=$nextmessage">$messagedisplay[17]</a></b>~;
	$back = $lastmessage eq '' ? $messagedisplay[19] : qq~<b><a href="$scripturl,v=display,m=$lastmessage">$messagedisplay[19]</a></b>~;

	if(!$binfo[5]) { $spoll = qq~$msp<a href="$scripturl,v=post,a=poll">$img{'newpoll'}</a>~; $vote = $messageindex[3]; }
	if(($binfo[3] == 1 && $username ne 'Guest') || ($binfo[3] == 2 && $settings[4] eq 'Administrator') || ($binfo[3] eq '' || $binfo[3] == 0) && $binfo[3] != 3) { $allowposts = qq~<a href="$scripturl,v=post">$img{'newthread'}</a> $spoll ~; } else { $allowposts = qq~<img src="$images/locked.gif"> $messagedisplay[29]~; }
	$GoBoards = ListBoards;
	&BoardProperties;

	if($settings[4] eq 'Administrator' || $ismod) {
		$moderate = <<"EOT";
<td><span class="smalltext"><b>$messagedisplay[11]:</b></span></td>
<td><span class="smalltext"><a href="javascript:clear('all')">$img{'remove'}</a> $msp <a href="$scripturl,v=mod,a=move,m=$messid">$img{'move'}</a>$stick $msp <a href="$scripturl,v=mod,a=lock,m=$messid">$img{'lock'}</a> $msp<a href="$scripturl,v=mod,a=split,m=$messid">$img{'split'}</a> $msp<a href="$scripturl,v=mod,a=merge,m=$messid">$img{'merge'}</a></span></td>
EOT
	}
	elsif($ston) { $moderate = $stick; }

	$ebout .= <<"EOT";
 <tr>
  <td class="titlebg" colspan="3"><span class="smalltext"><div style="float: left; padding: 3px;"><img src="$images/$status.gif" align="left"> &nbsp;<b>$gtxt{'17'}:</b> $pagelinks</div><div style="float: right">$print_reply</div></span></td>
 </tr>
</table>
<br>
<table cellpadding="0" cellspacing="1" class="border" width="100%" align="center">
 <tr>
  <td>
   <table cellpadding="4" cellspacing="0" width="100%">
    <tr>
     <td class="win" align="center"><span class="smalltext">$allowposts</span></td>
     <td class="win2" align="center"><span class="smalltext"><b><a href="$surl">$gtxt{'44'}</a> &nbsp;<b>&#155;</b>&nbsp; <a href="$scripturl,v=mindex">$boardnm</a></b>&nbsp; [ $back <b>|</b> $next ]</span></td>
     <td align="right" class="win"><span class="smalltext"><b>$var{'70'}:</b></span> $GoBoards</td>
    </tr>
   </table>
  </td>
 </tr>
</table>
<br>
EOT
	if(($quickreply && $username ne 'Guest') && (($binfo[4] == 1) || ($binfo[4] == 2 && $settings[4] eq 'Administrator') || ($binfo[4] == 4 && ($settings[4] eq 'Administrator' || $ismod)) || ($binfo[4] eq '' || $binfo[4] == 0) && !$mlocked && $binfo[4] != 3)) {
		$ebout .= <<"EOT";
<table cellspacing="1" cellpadding="5" width="100%" class="border" align="center">
 <tr><form action="$scripturl,v=post,m=$URL{'m'},post=1" method="post" name="msend">
  <td class="titlebg"><b><img src="$images/replied.gif"> $messagedisplay[36]</b></td>
 </tr><tr>
  <td class="win" align="center"><textarea name="message" tabindex="1" wrap="virtual" rows="12" cols="95"></textarea></td>
 </tr><tr>
  <td class="win2" align="center"><input type="hidden" name="xout" value="1"><input type="submit" class="button" tabindex="2" value=" $messagedisplay[37] " name="submit">&nbsp; <input type="submit" class="button" tabindex="3" name="preview" value=" $messagedisplay[38] "></td></form>
 </tr>
</table><br>
EOT
	}

	$ebout .= qq~<table cellspacing="0" cellpadding="0" width="100%"><tr><td valign="top">~;

	if($allowrate) {
		fopen(FILE,"$messages/$messid.rate");
		while(<FILE>) {
			chomp;
			if($rcnt == 0) { $rate = $_; $rcnt = 1; }
				else { ++$ratecnt; $rated{$_} = 1; }
		}
		fclose(FILE);
		if($rate) { $rate = qq~<img src="$images/$rate\star.gif"><br>$messagedisplay[30] <b>$ratecnt</b> $messagedisplay[35]~; }
			else { $rate = "<i>$messagedisplay[33]</i>"; }

		$ebout .= <<"EOT";
<table cellpadding="4" cellspacing="1" class="border" width="300">
 <tr>
  <td class="catbg" align="center"><span class="smalltext"><b>$messagedisplay[34]</b></span></td>
 </tr><tr>
  <td class="win" align="center"><span class="smalltext">$rate</span></td>
 </tr>
EOT
		if(!$rated{$username}) {
			$ebout .= <<"EOT";
 <tr>
  <td class="win2">
   <table width="100%">
    <tr><form action="$scripturl,v=ppoll,a=vote,m=$URL{'m'},s=$tstart,e=rate" method="post" name="msend">
     <td width="50%" align="center"><select name="rate">
      <option value="" selected>$messagedisplay[25]</option>
      <option value="5">5 - $messagedisplay[26]</option>
      <option value="4">4</option>
      <option value="3">3 - $messagedisplay[27]</option>
      <option value="2">2</option>
      <option value="1">1 - $messagedisplay[28]</option>
     </select> &nbsp; <input type="submit" class="button" value="$messagedisplay[24]"></td></form>
    </tr>
   </table>
  </td>
 </tr>
EOT
		}
		$ebout .= "</table>";
	}
	$ebout .= <<"EOT";
  </td><td align="right"><table cellpadding="4" cellspacing="1" class="border" width="300">
   <tr>
    <td class="catbg" valign="top" colspan="2" align="center"><span class="smalltext"><b>$gtxt{'43'}</b></span></td></td>
   </tr><tr>
    <td class="win">
     <table width="100%" cellpadding="2" cellspacing="0"><tr>
      <td class="win" valign="top" width="65%"><span class="smalltext">$rules</span></td></td>
      <td class="win" valign="top" width="35%"><span class="smalltext">$rules2</span></td></td>
     </tr></table>
    </td>
   </tr>
  </table>
  </td>
 </tr>
</table>
EOT
	if($moderate) {
		if($settings[4] eq 'Administrator') { $modlist = qq~<tr><td colspan="2" align="center"><span class="smalltext"><a href="$scripturl,v=mod,a=modlog,m=$URL{'m'}">$messagedisplay[50]</a></span></td></tr>~; }
		$ebout .= <<"EOT";
<br><table class="border" cellpadding="2" cellspacing="1" align="center">
 <tr>
  <td class="win2"><table cellpadding="2"><tr>$moderate</tr>$modlist</table></td>
 </tr>
</table>
EOT
	}
	&footer;
	exit;
}
1;