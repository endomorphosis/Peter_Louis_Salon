################################################
# E-Blah Bulliten Board Systems       Platinum #
################################################
# Copyright (c) 2001 - 2004 e-blah!            #
# All Rights Reserved.                         #
################################################

sub SaveSettings {
	$url = "$scripturl,v=memberpanel,a=$URL{'as'},s=$URL{'s'},u=$URL{'u'}";
	$INP{'message'} = $FORM{'message'};

	while(($iname,$ivalue) = each(%FORM)) {
		$ivalue =~ s~\A\s+~~;
		$ivalue =~ s~\s+\Z~~;
		$ivalue =~ s~[\n\r]~~g;
		$FORM{$iname} = $ivalue;
	}

	if($FORM{'caller'} == 1) {
		error($profiletxt[64]) if($FORM{'sn'} eq '');
		error($profiletxt[66]) if($FORM{'sn'} !~ /\A[0-9A-Za-z#%+,-\.@�^_ &nbsp;\&"']+\Z/);
		error($profiletxt[67]) if(length($FORM{'sn'}) > 30);
		error($profiletxt[65]) if($FORM{'email'} !~ /\A([0-9A-Za-z\._\-]{1,})@([0-9A-Za-z\._\-]{1,})+\.([0-9A-Za-z\._\-]{1,})+\Z/);
		error($profiletxt[68]) if(length($FORM{'email'}) > 60);

		fopen(FILE,"$members/List.txt");
		@membero = <FILE>;
		fclose(FILE);
		$bah = $FORM{'sn'};

		fopen(FILE,"$prefs/Names.txt");
		while(<FILE>) {
			chomp $_;
			($searchme,$within) = split(/\|/,$_);
			$searchme = lc($searchme);
			if($within) { error("$profiletxt[69]") if(lc($bah) =~ /\Q$searchme\E/gsi); }
				else { error("$profiletxt[69]") if($searchme eq lc($bah)); }
		}
		fclose(FILE);

		foreach $member (@membero) {
			chomp $member;
			if(lc($member) ne lc($URL{'u'})) {
				loaduser($member);
				if(lc($bah) eq lc($userset{$member}->[1]) || lc($member) eq lc($bah)) { &error($profiletxt[69]); }
				if(lc($FORM{'email'}) eq lc($userset{$member}->[2])) { &error($profiletxt[70]); }
			}
		}

		foreach (@banlist) {
			($banstring) = split(/\|/,$_);
			if($banstring eq $FORM{'email'}) { &error($profiletxt[70]); }
		}

		$FORM{'aim'} =~ s/ /+/gsi;

		$userset{$URL{'u'}}->[1] = $bah;
		$userset{$URL{'u'}}->[2] = lc($FORM{'email'});
		$userset{$URL{'u'}}->[9] = $FORM{'aim'};
		$userset{$URL{'u'}}->[10] = $FORM{'msn'};
		$userset{$URL{'u'}}->[8] = $FORM{'icq'};
		$userset{$URL{'u'}}->[27] = $FORM{'yim'};
	} elsif($FORM{'caller'} == 2) {
		if($FORM{'newpw'} ne $FORM{'newpwc'}) { &error($profiletxt[84]); }
		if(length($FORM{'newpw'}) > 8) { &error($profiletxt[85]); }
		if($yabbconver) {
			$FORM{'newpw'} = crypt($FORM{'newpw'},$pwcry);
			$FORM{'oldpw'} = crypt($FORM{'oldpw'},$pwcry);
		}

		if($FORM{'newpw'} eq '') { &error($profiletxt[86]); }
		if($userset{$URL{'u'}}->[0] ne $FORM{'oldpw'}) { &error($profiletxt[87]); }
		$userset{$URL{'u'}}->[0] = $FORM{'newpw'};

		if($username eq $URL{'u'}) { $url = "$scripturl,v=login"; }
	} elsif($FORM{'caller'} == 3) { # Signature
		if($maxsig && length($INP{'message'}) > $maxsig) { &error($profiletxt[74]); }
		$userset{$URL{'u'}}->[11] = Format($INP{'message'});
	} elsif($FORM{'caller'} == 4) {
		if($userset{$URL{'u'}}->[32] && ($FORM{'avcust'} || $FORM{'ulfile'} || $FORM{'avatartype'} == 3 || $FORM{'avatartype'} == 1)) {
			$userset{$URL{'u'}}->[5] =~ s/$uploadurl\///gsi;
			unlink("$uploaddir/$userset{$URL{'u'}}->[5]");
			$userset{$URL{'u'}}->[32] = '';
		}
		if($FORM{'avatartype'} == 1 && $FORM{'av'}) {
			if(!-e("$avdir/$FORM{'av'}")) { $FORM{'av'} = ''; }
			$userset{$URL{'u'}}->[5] = $FORM{'av'};
		}
		elsif($apic == 1 && $FORM{'avatartype'} == 2 && ($FORM{'avcust'} && $FORM{'avcust'} =~ /(http|ftp|mms|https):\/\/(.[^\s\n]+?)$/)) { $userset{$URL{'u'}}->[5] = $FORM{'avcust'}; }
		elsif($FORM{'avatartype'} == 2 && $FORM{'ulfile'}) {
			$allowedext = "gif,jpg,jpeg,png,bmp";
			$maxsize = $maxsize2;
			CoreLoad('Attach'); &Upload;
			$userset{$URL{'u'}}->[5] = "$uploadurl/$atturl";
			$userset{$URL{'u'}}->[32] = 1;
		} elsif($userset{$URL{'u'}}->[32]) { 1; }
			else { $userset{$URL{'u'}}->[5] = ''; }
		if($FORM{'picwidth'} && $FORM{'picwidth'} <= $picwidth) { $width = $FORM{'picwidth'}; } else { $width = $picwidth; }
		if($FORM{'picheight'} && $FORM{'picheight'} <= $picheight) { $height = $FORM{'picheight'}; } else { $height = $picheight; }
		$userset{$URL{'u'}}->[35] = "$height|$width";
	} elsif($FORM{'caller'} == 5) {
		$personaltext = Format($FORM{'pt'});
		if(length($personaltext) > 50) { &error($profiletxt[63]); }

		if($FORM{'dd'} && $FORM{'mm'} && $FORM{'yyyy'}) {
			if($FORM{'yyyy'} < 0 || $FORM{'mm'} > 12 || $FORM{'mm'} < 0 || $FORM{'dd'} > 31 || $FORM{'dd'} < 0) { &error($profiletxt[158]); }
			$birthday = "$FORM{'mm'}/$FORM{'dd'}/$FORM{'yyyy'}";
		}

		$siteurl = $FORM{'url'};
		if($siteurl && $siteurl !~ /http:\/\/(.*?)\Z/ && $1 eq '') { &error($profiletxt[75]); }
		if($siteurl eq 'http://') { $siteurl = ''; }
		if($siteurl eq '') { $FORM{'sname'} = ''; }
		if($FORM{'sname'} eq '') { $siteurl = ''; }

		$userset{$URL{'u'}}->[6] = $personaltext;
		$userset{$URL{'u'}}->[7] = $FORM{'sex'};
		$userset{$URL{'u'}}->[16] = $birthday;
		$userset{$URL{'u'}}->[19] = $FORM{'sname'};
		$userset{$URL{'u'}}->[20] = $siteurl;
		$userset{$URL{'u'}}->[21] = $FORM{'location'};
	} elsif($FORM{'caller'} == 6) {
		$userset{$URL{'u'}}->[12] = $FORM{'hidemail'};
		$userset{$URL{'u'}}->[13] = $FORM{'hidelogout'};
		$userset{$URL{'u'}}->[18] = $FORM{'hideonline'};
		$userset{$URL{'u'}}->[25] = $FORM{'mailing'};
		$userset{$URL{'u'}}->[29] = $FORM{'showsums'};
		$userset{$URL{'u'}}->[30] = $FORM{'notify'};
		$userset{$URL{'u'}}->[33] = $FORM{'onlynew'};
		$userset{$URL{'u'}}->[36] = $FORM{'signature'};
		$userset{$URL{'u'}}->[37] = $FORM{'avatar'};
		$userset{$URL{'u'}}->[39] = $FORM{'censor'};
		$userset{$URL{'u'}}->[41] = $FORM{'pmpop'};
	} elsif($FORM{'caller'} == 7) {
		$userset{$URL{'u'}}->[15] = $FORM{'toff'};
		$userset{$URL{'u'}}->[17] = $FORM{'dformat'};
		$userset{$URL{'u'}}->[22] = $FORM{'tformat'};
		$userset{$URL{'u'}}->[31] = $FORM{'tcng'};
	} elsif($FORM{'caller'} == 8) {
		if($FORM{'theme'} eq '|default|') { $FORM{'theme'} = ''; }
		if(!-e("$theme/$FORM{'theme'}.v2") && $FORM{'theme'} ne '') { &error($profiletxt[73]); }
		$userset{$URL{'u'}}->[26] = $FORM{'theme'};
		$userset{$URL{'u'}}->[34] = $FORM{'lng'};
	} elsif($FORM{'caller'} == 9) {
		&is_admin;
		use Time::Local 'timelocal';
		if(!$FORM{'posts'}) { $FORM{'posts'} = 0; }
		if($userset{$URL{'u'}}->[23] ne 'EMAIL' && $FORM{'status'} ne 'EMAIL') { $status = $FORM{'status'}; }

		if($FORM{'keytype'} ne '') {
			if($FORM{'keytype'} eq "TEMP") {
				($date2,$time2) = split(" at ",$FORM{'active'});
				($mm2,$dd2,$yy2) = split(/\//,$date2);
				($hh2,$mi2,$ss2) = split(":",$time2);
				$yy2 += 100;
				--$mm2;
				eval { $stime = timelocal($ss2,$mi2,$hh2,$dd2,$mm2,$yy2); };
				if($stime eq '') { $stime = time; }
				$extrakey = "$FORM{'max'}|$stime\n";
			}
			fopen(FILE,">$members/$URL{'u'}.lo");
			print FILE "$FORM{'keytype'}\n$extrakey";
			fclose(FILE);
		} else { unlink("$members/$URL{'u'}.lo"); }

		($date,$time) = split(" at ",$FORM{'regid'});
		($mm,$dd,$yy) = split(/\//,$date);
		($hh,$mi,$ss) = split(":",$time);
		$yy += 100;

		--$mm;
		eval { $registered = timelocal($ss,$mi,$hh,$dd,$mm,$yy); };

		$userset{$URL{'u'}}->[3] = $FORM{'posts'};
		$userset{$URL{'u'}}->[4] = $FORM{'pos'};
		$userset{$URL{'u'}}->[23] = $status;
		if($registered ne '') { $userset{$URL{'u'}}->[14] = $registered; }
		$userset{$URL{'u'}}->[38] = $FORM{'pm'};
	} elsif($FORM{'caller'} == 10 || $URL{'caller'} == 10) {
		@blockeduserlist = split(/\|/,$userset{$URL{'u'}}->[40]);

		if($URL{'remove'}) {
			$userset{$URL{'u'}}->[40] = '';
			foreach(@blockeduserlist) {
				if($URL{'remove'} ne $_) {
					$userset{$URL{'u'}}->[40] .= "$_|";
				}
			}
		} else {
			loaduser($FORUM{'1'});
			if($userset{$FORM{'1'}}->[1] eq '') {
				$FORM{'1'} =~ s/ //gsi;
				fopen(FILE,"$members/List.txt");
				@list = <FILE>;
				fclose(FILE);
				foreach(@list) {
					chomp $_;
					loaduser($_);
					$userset{$_}->[1] =~ s/ //gsi;
					if(lc($userset{$_}->[1]) eq lc($FORM{'1'}) || lc($_) eq lc($FORM{'1'})) { $FORM{'1'} = $_; $fnduser = 1; last; }
				}
			} else { $fnduser = 1; }
			if($FORM{'1'} eq 'Guest') { $fnduser = 1; }
			foreach(@blockeduserlist) {
				if($_ eq $FORM{'1'}) { $fnduser = 0; }
			}
			if($fnduser) { $userset{$URL{'u'}}->[40] .= "$FORM{'1'}|"; }
				else { &error($profiletxt[265]); }
		}
	} elsif($URL{'c'} eq 'reset') {
		&is_admin;
		$resetme .= sprintf("%.0f",rand(int(time/90)*9000));
		$resetme =~ tr/15973/MaZDnfJflkJHC/;
		smail($userset{$URL{'u'}}->[2],$profiletxt[231],qq~$profiletxt[232] <b>$resetme</b>\n\n$rurl\n\n$gtxt{'25'}~);
		if($yabbconver) { $resetme = crypt($resetme,$pwcry); }
		$userset{$URL{'u'}}->[0] = $resetme;
		if($username eq $URL{'u'}) { $url = "$scripturl,v=login"; }
	} else { &error($gtxt{'bfield'}); }
	fopen(FILE,">$members/$URL{'u'}.dat");
	for($q = 0; $q < $usersetcount; $q++) { print FILE "$userset{$URL{'u'}}->[$q]\n"; }
	fclose(FILE);
	&redirect;
}

sub AdminLoad {
	&is_admin;
	fopen(FILE,"$members/$URL{'u'}.lo");
	@key = <FILE>;
	fclose(FILE);
	chomp @key;
	$KEY{$key[0]} = " selected";

	# Key Time
	if($key[1] ne '') { ($max,$active) = split(/\|/,$key[1]); } else { $active = time; }
	($ss2,$mi2,$hh2,$dd2,$mm2,$yy2) = localtime($active);
	++$mm2;
	if($ss2 < 10) { $ss2 = "0$ss2"; }
	if($mi2 < 10) { $mi2 = "0$mi2"; }
	if($hh2 < 10) { $hh2 = "0$hh2"; }
	if($dd2 < 10) { $dd2 = "0$dd2"; }
	if($mm2 < 10) { $mm2 = "0$mm2"; }
	$yy2 -= 100;
	if($yy2 < 10) { $yy2 = "0$yy2"; }

	# Register Date
	($ss,$mi,$hh,$dd,$mm,$yy) = localtime($userset{$URL{'u'}}->[14]);
	++$mm;
	if($ss < 10) { $ss = "0$ss"; }
	if($mi < 10) { $mi = "0$mi"; }
	if($hh < 10) { $hh = "0$hh"; }
	if($dd < 10) { $dd = "0$dd"; }
	if($mm < 10) { $mm = "0$mm"; }
	$yy -= 100;
	if($yy < 10) { $yy = "0$yy"; }

	$status{$userset{$URL{'u'}}->[23]} = ' selected';
	$PM{$userset{$URL{'u'}}->[38]} = ' checked';

	$morecaller = $profiletxt[252];

	$displaycenter .= <<"EOT";
<tr>
 <td class="catbg" align="center"><span class="smalltext"><b>$profiletxt[242]</b></span></td>
</tr><tr>
 <td class="win">
  <table width="100%">
   <tr>
    <td width="35%" align="right"><b>$profiletxt[96]:</b></td>
    <td width="65%" colspan="2"><input type="text" class="textinput" name="posts" value="$userset{$URL{'u'}}->[3]" size="5"></td>
   </tr><tr>
    <td align="right" valign="top"><b>$profiletxt[136]:</b></td>
    <td align="center" width="4%"><img src="$images/register_sm.gif"></td>
    <td width="61%"><input type="text" class="textinput" name="regid" value="$mm/$dd/$yy at $hh:$mi:$ss" size="25"></td>
   </tr><tr>
    <td>&nbsp;</td>
    <td colspan="2"><span class="smalltext">$profiletxt[137]</span></td>
   </tr><tr>
    <td colspan="3">
     <table><tr>
      <td width="25" valign="top" align="center"><input type="checkbox" class="checkboxinput" name="pm" value="1"$PM{1}></td>
      <td width="25" align="center"><img src="$images/pm2_sm.gif"></td>
      <td><b>$profiletxt[250]</b><br><span class="smalltext">$profiletxt[251]</span></td>
     </tr></table>
    </td>
   </tr><tr>
   </tr>
  </table>
 </td>
</tr><tr>
 <td class="catbg" align="center"><span class="smalltext"><b>$profiletxt[204]</b></span></td>
</tr><tr>
 <td class="win">
  <table width="100%">
   <tr>
    <td width="35%" align="right"><b>$profiletxt[97]:</b></td>
    <td width="65%" colspan="2"><select name="pos">
EOT

	$C{"$userset{$URL{'u'}}->[4]"} = " selected";
	$displaycenter .= qq~<option value=""$C{''}>$profiletxt[202]</opition>\n~;
	$displaycenter .= qq~<option value=""></option>\n~;
	$displaycenter .= qq~<option value="Administrator"$C{'Administrator'}>$memgrps[0] ($profiletxt[203])</opition>\n~;
	for($i = 7; $i < @memgrps; $i++) {
		($t,$t,$t,$ty,$t,$t,$team) = split(',',$memgrpp[$i]);
		$team = $team ? " ($profiletxt[203])" : '';
		$displaycenter .= qq~<option value="$memgrps[$i]"$C{$memgrps[$i]}>$memgrps[$i]$team</opition>\n~;
	}

	$displaycenter .= <<"EOT";
    </select></td>
   </tr>
   <tr>
    <td align="right"><b>$profiletxt[131]:</b></td>
    <td valign="top"><select name="status"><option value=""$status{''}>$profiletxt[133]</option><option value="ADMIN"$status{'ADMIN'}>$profiletxt[134]</option><option value="EMAIL"$status{'EMAIL'}>$profiletxt[156]</option><option value="DISABLE"$status{'DISABLE'}>$profiletxt[135]</option></select></td>
   </tr>
  </table>
 </td>
</tr><tr>
 <td class="catbg" align="center"><span class="smalltext"><b>$profiletxt[243]</b></span></td>
</tr><tr>
 <td class="win">
  <table width="100%">
   <tr>
    <td align="center" width="25"><img src="$images/keys.gif"></td>
    <td><b>$profiletxt[244]</b><br><select name="keytype" onChange="OpenTemp(this.value);"><option value=""$KEY{''}>$profiletxt[142]</option><option value="ALLOW"$KEY{'ALLOW'}>$profiletxt[143]</option><option value="TEMP"$KEY{'TEMP'}>$profiletxt[144]</option></select></td>
   </tr><tr id="temp" style="display:none;">
    <td>&nbsp;</td>
    <td>
     <table>
      <tr>
       <td colspan="2"><b>$profiletxt[245]</b></td>
      </tr><tr>
       <td width="200" align="right"><span class="smalltext"><b>$profiletxt[246]:</b></span></td>
       <td><input type="text" class="textinput" name="active" value="$mm2/$dd2/$yy2 at $hh2:$mi2:$ss2" size="25"></td>
      </tr><tr><td>&nbsp;</td><td><span class="smalltext">$profiletxt[137]</span></td></tr><tr>
       <td width="200" align="right"><span class="smalltext"><b>$profiletxt[147]:</b></span></td>
       <td><input type="text" class="textinput" name="max" value="$max" size="4" maxlength="4"></td>
      </tr>
     </td>
    </table>
   </tr>
  </table>
 </td>
</tr>
<script language="javascript">
function OpenTemp(open) {
	if(document.getElementById) { openItem = document.getElementById('temp'); }
	else if (document.all){ openItem = document.all['temp']; }
	else if (document.layers){ openItem = document.layers['temp']; }

	if(open == 'TEMP') { ShowType = ""; }
		else { ShowType = "none"; }

	if(openItem.style) { openItem.style.display = ShowType; }
		else { openItem.visibility = "show"; }
}
OpenTemp(document.msend.keytype.value);
</script>
EOT
}

sub ProfileInfo {
	$userset{$URL{'u'}}->[6] =~ s~"~\&quot;~g;
	$SEL{$userset{$URL{'u'}}->[7]} = " selected";
	if($userset{$URL{'u'}}->[16]) { ($mm,$dd,$yyyy) = split("/",$userset{$URL{'u'}}->[16]); }

	$morecaller = $profiletxt[263];
	$displaycenter .= <<"EOT";
<tr>
 <td class="catbg" align="center"><span class="smalltext"><b>$profiletxt[224]</b></span></td>
</tr><tr><td class="win"><table width="100%"><tr>
 <td align="right" width="35%"><b>$profiletxt[33]: </b></td>
 <td width="65%" colspan="2"><select name="sex"><option value="">$profiletxt[34]</option><option value="1"$SEL{'1'}>$profiletxt[35]</option><option value="2"$SEL{'2'}>$profiletxt[36]</option></select></td>
</tr><tr>
 <td align="right"><b>$gtxt{'15'}:</b></td>
 <td colspan="2"><input type="text" class="textinput" name="location" value="$userset{$URL{'u'}}->[21]" size="40"></td>
</tr><tr>
 <td align="right" valign="top" width="35%"><b>$profiletxt[38]: </b></td>
 <td width="4%" align="center"><img src="$images/cal_sm.gif"></td>
 <td width="61%"><input type="text" class="textinput" name="mm" value="$mm" size="2" maxlength="2"> - <input type="text" class="textinput" name="dd" value="$dd" size="2" maxlength="2"> - <input type="text" class="textinput" name="yyyy" value="$yyyy" size="4" maxlength="4"></td>
 </tr><tr>
 <td>&nbsp;</td><td colspan="2"><span class="smalltext">$profiletxt[39]</span></td>
 </tr></table></td>
</tr><tr>
 <td class="catbg" align="center"><span class="smalltext"><b>$profiletxt[223]</b></span></td>
</tr><tr><td class="win"><table width="100%"><tr>
 <td align="right" valign="top" width="35%"><b>$profiletxt[40]:</b></td>
 <td width="4%" align="center" rowspan="2"><img src="$images/site_sm.gif"></td>
 <td width="61%"><input type="text" class="textinput" name="sname" value="$userset{$URL{'u'}}->[19]" size="30"></td>
</tr><tr>
 <td align="right"><b>$profiletxt[41]:</b></td>
 <td><input type="text" class="textinput" name="url" value="$userset{$URL{'u'}}->[20]" size="25"></td>
 </tr></table></td>
</tr><tr>
 <td colspan="2" class="catbg" align="center"><span class="smalltext"><b>$profiletxt[222]</b></span></td>
</tr><tr><td class="win"><table width="100%"><tr>
 <td align="right" width="35%"><b>$profiletxt[46]:</b><br><span class="smalltext">$profiletxt[47]</span></td>
 <td valign="top" width="65%"><input type="text" class="textinput" name="pt" value="$userset{$URL{'u'}}->[6]" size="50" maxlength="50"></td>
 </tr></table></td>
</tr>
EOT
}

sub TimeSettings {
	$DF{"$userset{$URL{'u'}}->[17]"} = " selected";
	$TZ{"$userset{$URL{'u'}}->[15]"} = " selected";
	$TCNG{"$userset{$URL{'u'}}->[31]"} = ' checked';
	$TF{"$userset{$URL{'u'}}->[22]"} = " selected";
	get_date(time);
	($sec,$min,$hour2,$dayz,$month,$year) = gmtime(time+(3600*$settings[15]+$settings[31]));
	$month2 = $month+1;
	if($hour == 0) { $hour = 12; }
	if($hour >= 13) { $hour = ($hour - 12); }
	++$hour2;
	if($hour2 == 24) { $hour2 = '00'; }
	if($min < 10) { $min = "0$min"; }
	if($sec < 10) { $sec = "0$sec"; }
	$year += 1900;
	$morecaller = $profiletxt[261];
	$displaycenter .= <<"EOT";
<tr>
 <td class="catbg" align="center"><span class="smalltext"><b>$profiletxt[226]</b></span></td>
</tr><tr><td class="win"><table width="100%"><tr>
 <td align="right" valign="top" width="40%"><b>$profiletxt[25]:</b></td>
 <td valign="top" width="60%"><select name="dformat">
  <option value="0"$DF{'0'}>$months[$month] $dayz, $year</option>
  <option value="1"$DF{'1'}>$days[$week], $months[$month] $dayz, $year</option>
  <option value="2"$DF{'2'}>$dayz $months[$month] $year</option>
  <option value="3"$DF{'3'}>$month2.$dayz.$year</option>
  <option value="4"$DF{'4'}>$dayz/$month2/$year</option>
  <option value="5"$DF{'5'}>$year/$month2/$dayz</option>
 </select></td>
</tr><tr>
 <td align="right" valign="top" width="40%"><b>$profiletxt[26]:</b></td>
 <td valign="top"><select name="tformat">
  <option value="0"$TF{'0'}>$hour:$min$ampm</option>
  <option value="1"$TF{'1'}>$hour:$min:$sec$ampm</option>
  <option value="2"$TF{'2'}>$hour2:$min</option>
  <option value="3"$TF{'3'}>$hour2:$min:$sec</option>
 </select></td></tr></td></tr></table>
</tr><tr>
 <td class="catbg" align="center"><span class="smalltext"><b>$profiletxt[227]</b></span></td>
</tr><tr>
 <td class="win"><table width="100%" cellspacing="0" cellpadding="4"><tr>
 <td colspan="2"><b>$profiletxt[159]</b><br>
  <select name="toff">
   <option value="-12"$TZ{'-12'}>(GMT-12:00) Eniwetok, Kwajalein</option>
   <option value="-11"$TZ{'-11'}>(GMT-11:00) Midway Island, Samoa</option>
   <option value="-10"$TZ{'-10'}>(GMT-10:00) Hawaii</option>
   <option value="-9"$TZ{'-9'}>(GMT-9:00) Alaska</option>
   <option value="-8"$TZ{'-8'}>(GMT-8:00) Pacific Time (US & Canada)</option>
   <option value="-7"$TZ{'-7'}>(GMT-7:00) Mountain Time (US & Canada)</option>
   <option value="-6"$TZ{'-6'}>(GMT-6:00) Central Time (US & Canada), Mexico City</option>
   <option value="-5"$TZ{'-5'}>(GMT-5:00) Eastern Time (US & Canada), Bogota, Lima, Quito</option>
   <option value="-4"$TZ{'-4'}>(GMT-4:00) Atlantic Time (Canada), Caracas, La Paz</option>
   <option value="-3.5"$TZ{'-3.5'}>(GMT-3:30) Newfoundland</option>
   <option value="-3"$TZ{'-3'}>(GMT-3:00) Brazil, Buenos Aires, Georgetown</option>
   <option value="-2"$TZ{'-2'}>(GMT-2:00) Mid-Atlantic</option>
   <option value="-1"$TZ{'-1'}>(GMT-1:00) Azores, Cape Verde Islands</option>
   <option value="0"$TZ{'0'}>(GMT) Western Europe Time, London, Lisbon, Casablanca, Monrovia</option>
   <option value="1"$TZ{'1'}>(GMT+1:00) Central Europe Time, Brussels, Copenhagen, Madrid, Paris</option>
   <option value="2"$TZ{'2'}>(GMT+2:00) Eastern Europe Time, Kaliningrad, South Africa</option>
   <option value="3"$TZ{'3'}>(GMT+3:00) Baghdad, Kuwait, Riyadh, Moscow, St. Petersburg, Volgograd, Nairobi</option>
   <option value="3.5"$TZ{'3.5'}>(GMT+3:30) Tehran</option>
   <option value="4"$TZ{'4'}>(GMT+4:00) Abu Dhabi, Muscat, Baku, Tbilisi</option>
   <option value="4.5"$TZ{'4.5'}>(GMT+4:30) Kabul</option>
   <option value="5"$TZ{'5'}>(GMT+5:00) Ekaterinburg, Islamabad, Karachi, Tashkent</option>
   <option value="5.5"$TZ{'5.5'}>(GMT+5:30) Bombay, Calcutta, Madras, New Delhi</option>
   <option value="6"$TZ{'6'}>(GMT+6:00) Almaty, Dhaka, Colombo</option>
   <option value="7"$TZ{'7'}>(GMT+7:00) Bangkok, Hanoi, Jakarta</option>
   <option value="8"$TZ{'8'}>(GMT+8:00) Beijing, Perth, Singapore, Hong Kong, Chongqing, Urumqi, Taipei</option>
   <option value="9"$TZ{'9'}>(GMT+9:00) Tokyo, Seoul, Osaka, Sapporo, Yakutsk</option>
   <option value="9.5"$TZ{'9.5'}>(GMT+9:30) Adelaide, Darwin</option>
   <option value="10"$TZ{'10'}>(GMT+10:00) East Australian Standard, Guam, Papua New Guinea, Vladivostok</option>
   <option value="11"$TZ{'11'}>(GMT+11:00) Magadan, Solomon Islands, New Caledonia</option>
   <option value="12"$TZ{'12'}>(GMT+12:00) Auckland, Wellington, Fiji, Kamchatka, Marshall Island</option>
  </select>
  </td>
 </tr><tr>
  <td align="center" valign="top"><input type="checkbox" class="checkboxinput" name="tcng" value="1"$TCNG{'1'}></td><td><b>$profiletxt[166]</b><br><span class="smalltext">$profiletxt[167]</span></td>
 </tr></table></td>
</tr>
EOT
}

sub LngTheme {
	opendir(DIR,"$languages/");
	@lngs = readdir(DIR);
	closedir(DIR);

	if($userset{$URL{'u'}}->[34] eq '') { $userset{$URL{'u'}}->[34] = $languagep; }
	foreach(@lngs) {
		($lng,$type) = split(/\./,$_);
		if($type ne 'lng') { next; }
		$check = '';
		if("$languages/$userset{$URL{'u'}}->[34]" eq "$languages/$lng") { $check = ' selected'; }
		$lngs .= qq~<option value="$lng"$check>$lng</option>\n~;
	}
	$morecaller = $profiletxt[260];
	$displaycenter .= <<"EOT";
<tr>
 <td class="catbg" align="center"><span class="smalltext"><b>$profiletxt[205] $profiletxt[225]</b></span></td>
</tr><tr><td class="win"><table width="100%"><tr>
 <td align="right" width="40%" class="win"><b>$profiletxt[205]:</b></td>
 <td valign="top" width="60%" class="win"><select name="lng">$lngs</select></td>
 </tr></table></td>
</tr>
EOT
	if($showtheme) {
		$displaycenter .= <<"EOF";
<tr>
 <td class="catbg" align="center"><span class="smalltext"><b>$profiletxt[29]</b></span></td>
</tr><tr><td class="win"><table width="100%"><tr>
 <td align="right" valign="top" width="40%"><b>$profiletxt[29]:</b><br><span class="smalltext">$profiletxt[30]</span></td>
 <td valign="top" width="30%"><select name="theme">
<option value="|default|">$profiletxt[31]</option>
EOF
		opendir(DIR,"$theme/");
		@themes = readdir(DIR);
		closedir(DIR);
		CoreLoad('Themes');
		if($name) { $sel2 = $name; }
		foreach(@themes) {
			if($_ =~ m/(.*)(.v2)/) {
				StopThemes("$theme/$_") || next;
				$sel = '';
				if("$settings[26].v2" eq $_) { $sel = " selected"; $name = $sel2; }
				$displaycenter2 .= qq~<option value="$1"$sel>$name</option>\n~;
			}
		}
		$displaycenter .= $displaycenter2 ? qq~<option value=""></option>~.$displaycenter2 : '';
		$displaycenter .= <<"EOF";
 </select></td><td width="30%"><input type="button" class="button" value="$profiletxt[239]" onClick="window.open('$scripturl,theme='+document.msend.theme.value,'themes','align=center,height=500,width=750,resizable=yes,scrollbars=yes,status=yes');"></td></tr></table></td>
</tr>
EOF
	}
}

sub BoardSettings {
	$HIDE{$userset{$URL{'u'}}->[18]} = " checked";
	$H{$userset{$URL{'u'}}->[12]} = " checked";
	$L{$userset{$URL{'u'}}->[13]} = " checked";
	$ML{$userset{$URL{'u'}}->[25]} = " checked";
	$SS{$userset{$URL{'u'}}->[29]} = " checked";
	$NOTIFY{$userset{$URL{'u'}}->[30]} = " checked";
	$ONEW{$userset{$URL{'u'}}->[33]} = ' checked';
	if($userset{$URL{'u'}}->[15] eq '') { $TZ{'0'} = ' selected'; }
	$SIGNATURE{$userset{$URL{'u'}}->[36]} = ' checked';
	$AVATAR{$userset{$URL{'u'}}->[37]} = ' checked';
	$CENSOR{$userset{$URL{'u'}}->[39]} = ' checked';
	$PMPOP{$userset{$URL{'u'}}->[41]} = ' checked';

	$morecaller = $profiletxt[228];

	$displaycenter .= <<"EOF";
<tr>
 <td class="catbg" align="center"><span class="smalltext"><b>$profiletxt[201]</b></span></td>
</tr><tr><td class="win"><table width="100%" cellspacing="0" cellpadding="4"><tr>
 <td align="center" valign="top"><input type="checkbox" class="checkboxinput" name="hidelogout" value="1"$L{'1'}></td>
 <td width="100%" colspan="2"><b>$profiletxt[17]</b><br><span class="smalltext">$profiletxt[18]</span></td>
</tr><tr>
 <td valign="top" align="center"><input type="checkbox" class="checkboxinput" name="showsums" value="1"$SS{'1'}></td>
 <td colspan="2"><b>$profiletxt[23]</b><br><span class="smalltext">$profiletxt[24]</span></td>
</tr><tr>
 <td valign="top" align="center"><input type="checkbox" class="checkboxinput" name="onlynew" value="1"$ONEW{'1'}></td>
 <td colspan="2"><b>$profiletxt[168]</b><br><span class="smalltext">$profiletxt[241]</span></td>
</tr><tr>
 <td valign="top" align="center"><input type="checkbox" class="checkboxinput" name="notify" value="1"$NOTIFY{'1'}></td>
 <td align="center" width="15"><img src="$images/notify_sm.gif"></td>
 <td><b>$profiletxt[155]</b><br><span class="smalltext">$profiletxt[240]</span></td>
</tr><tr>
 <td valign="top" align="center"><input type="checkbox" class="checkboxinput" name="censor" value="1"$CENSOR{'1'}></td>
 <td align="center" width="15"><img src="$images/report_sm.gif"></td>
 <td><b>$profiletxt[255]</b><br><span class="smalltext">$profiletxt[254]</span></td>
</tr><tr>
 <td valign="top" align="center"><input type="checkbox" class="checkboxinput" name="pmpop" value="1"$PMPOP{'1'}></td>
 <td align="center" width="15"><img src="$images/pm2_sm.gif"></td>
 <td><b>$profiletxt[274]</b><br><span class="smalltext">$profiletxt[275]</span></td>
</tr><tr>
 <td colspan="3"><hr color="$color{'border'}" class="hr" size="1"></td>
</tr><tr>
 <td valign="top" align="center"><input type="checkbox" class="checkboxinput" name="signature" value="1"$SIGNATURE{'1'}></td>
 <td colspan="2"><b>$profiletxt[229]</b></td>
</tr><tr>
 <td valign="top" align="center"><input type="checkbox" class="checkboxinput" name="avatar" value="1"$AVATAR{'1'}></td>
 <td colspan="2"><b>$profiletxt[230]</b></td>
 </tr></table></td>
</tr><tr>
 <td class="catbg" align="center"><span class="smalltext"><b>$profiletxt[206]</b></span></td>
</tr><tr><td class="win"><table width="100%" cellspacing="0" cellpadding="4">
EOF
	if($hmail || $hiddenmail == 2) {
		$displaycenter .= <<"EOF";
<tr>
 <td valign="top" align="center"><input type="checkbox" class="checkboxinput" name="hidemail" value="1"$H{'1'}></td>
 <td align="center"><img src="$images/lockmail.gif"></td>
 <td width="100%"><b>$profiletxt[15]</b><br><span class="smalltext">$profiletxt[16]</span></td>
</tr>
EOF
	}

	$displaycenter .= <<"EOF";
<tr>
 <td valign="top" align="center"><input type="checkbox" class="checkboxinput" name="hideonline" value="1"$HIDE{'1'}></td>
 <td colspan="2"><b>$profiletxt[19]</b><br><span class="smalltext">$profiletxt[20]</span></td>
</tr><tr>
 <td valign="top" align="center"><input type="checkbox" class="checkboxinput" name="mailing" value="1"$ML{'1'}></td>
 <td colspan="2"><b>$profiletxt[21]</b><br><span class="smalltext">$profiletxt[22]</span></td>
 </tr></table></td>
</tr>
EOF
}

sub Contact {
	$userset{$URL{'u'}}->[9] =~ s/\+/ /gsi;

	$morecaller = $profiletxt[264];
	$displaycenter .= <<"EOT";
<tr>
 <td colspan="2" class="catbg" align="center"><span class="smalltext"><b>$profiletxt[94]</b></span></td>
</tr><tr><td class="win"><table width="100%"><tr>
 <td align="right" width="40%"><b>$profiletxt[10]:</b><br><span class="smalltext">$profiletxt[11]</span></td>
 <td valign="top" width="60%" colspan="2"><input type="text" class="textinput" name="sn" value="$userset{$URL{'u'}}->[1]" maxlength="30"></td>
</tr><tr>
 <td align="right" width="40%"><b>$gtxt{'23'}: </b></td>
 <td width="4%" align="center"><img src="$images/email_sm.gif"></td>
 <td valign="top" width="56%"><input type="text" class="textinput" name="email" value="$userset{$URL{'u'}}->[2]" size="25" maxlength="80"></td>
 </tr></table></td>
</tr><tr>
 <td colspan="2" class="catbg" align="center"><span class="smalltext"><b>$profiletxt[102]</b></span></td>
</tr><tr><td class="win"><table width="100%"><tr>
 <td align="right" width="40%"><b>$profiletxt[42]:</b></td>
 <td align="center" width="4%"><img src="$images/icq_sm.gif"></td>
 <td width="56%"><input type="text" class="textinput" name="icq" value="$userset{$URL{'u'}}->[8]" maxlength="30" size="20"></td>
</tr><tr>
 <td align="right"><b>$profiletxt[43]:</b></td>
 <td align="center"><img src="$images/aim_sm.gif"></td>
 <td><input type="text" class="textinput" name="aim" value="$userset{$URL{'u'}}->[9]" maxlength="30"></td>
</tr><tr>
 <td align="right" width="40%"><b>$profiletxt[44]:</b></td>
 <td align="center"><img src="$images/yim_sm.gif"></td>
 <td><input type="text" class="textinput" name="yim" value="$userset{$URL{'u'}}->[27]" maxlength="30"></td>
</tr><tr>
 <td align="right" width="40%"><b>$profiletxt[45]:</b></td>
 <td align="center"><img src="$images/msn_sm.gif"></td>
 <td><input type="text" class="textinput" name="msn" value="$userset{$URL{'u'}}->[10]" maxlength="40"></td>
 </tr></table></td>
</tr>
EOT
}

sub MessageBlock {
	@blockeduserlist = split(/\|/,$userset{$URL{'u'}}->[40]);
	foreach(@blockeduserlist) {
		loaduser($_);
		if($userset{$_}->[1] eq '') { $userset{$_}->[1] = $gtxt{'0'}; } else { $userset{$_}->[1] = qq~<a href="$scripturl,v=memberpanel,u=$_">$userset{$_}->[1]</a>~; }
		$usersblocked .= qq~<a href="$scripturl,v=memberpanel,a=save,as=forum,s=messageblock,caller=10,u=$URL{'u'},remove=$_"><img src="$images/ban.gif" border="0"> $profiletxt[267]</a> -- $userset{$_}->[1] ($_)<br>~;
		++$cnt;
	}
	if($usersblocked eq '') { $usersblocked = $profiletxt[268]; }

	$morecaller = $profiletxt[269];
	$displaycenter .= <<"EOT";
<tr>
 <td colspan="2" class="catbg" align="center"><span class="smalltext"><b>$profiletxt[270]</b></span></td>
</tr><tr>
 <td class="win">
  <table width="100%">
   <tr>
    <td>$usersblocked</td>
   </tr>
  </table>
 </td>
</tr><tr>
 <td colspan="2" class="catbg" align="center"><span class="smalltext"><b>$profiletxt[271]</b></span></td>
</tr><tr>
 <td class="win">
  <table width="100%">
   <tr>
    <td>$profiletxt[272] </b><input type="text" class="textinput" size="30" name="1"><br><br>$profiletxt[273]</td>
   </tr>
  </table>
 </td>
</tr>
EOT
}

sub PW {
	if($settings[4] eq 'Administrator') {
		$admins = qq~<tr><td align="center" colspan="2"><span class="smalltext"><b><a href="javascript:reset();">$profiletxt[233]</a></b></span></td></tr>~;
		$displaycenter .= qq~<script language="javascript">
<!-- //
function reset() {
 if(window.confirm("$profiletxt[234]")) { location = "$scripturl,v=memberpanel,a=save,as=profile,s=pw,u=$URL{'u'},c=reset"; }
}
// -->
</script>~;
	}
	$morecaller = $profiletxt[78];
	$displaycenter .= <<"EOT";
<tr>
 <td class="catbg" align="center"><span class="smalltext"><b>$profiletxt[79] $profiletxt[80]</b></span></td>
</tr><tr><td class="win"><table width="100%"><tr>
 <td align="right" width="40%"><b>$profiletxt[79] $profiletxt[80]: </b></td>
 <td width="60%"><input type="password" class="textinput" name="oldpw" size="25"></td>
</tr></table></td></tr>
<tr>
 <td class="catbg" align="center"><span class="smalltext"><b>$profiletxt[81] $profiletxt[80]</b></span></td>
</tr><tr><td class="win"><table width="100%"><tr>
 <td align="right" width="40%"><b>$profiletxt[81] $profiletxt[80]: </b></td>
 <td width="60%"><input type="password" class="textinput" name="newpw" size="25" maxlength="8"></td>
</tr><tr>
 <td align="right"><b>$gtxt{'24'}: </b></td>
 <td><input type="password" class="textinput" name="newpwc" size="25" maxlength="8"></td>
</tr>$admins</table></td></tr>
EOT
}

sub Signature {
	$message = $userset{$URL{'u'}}->[11];
	&BC;
	$userset{$URL{'u'}}->[11] =~ s/<br>/\n/gi;
	if(!$maxsig) { $maxsig = $profiletxt[54]; }
	$morecaller = $profiletxt[56];

	if($message ne '') {
		$displaycenter .= <<"EOT";
<tr>
 <td class="catbg" align="center"><span class="smalltext"><b>$profiletxt[220]</b></span></td>
</tr><tr><td class="win"><table width="100%"><tr>
 <td><span class="smalltext">$message</span></td>
</tr></table></td></tr>
EOT
	}
	$displaycenter .= <<"EOT";
<tr>
 <td colspan="2" class="catbg" align="center"><span class="smalltext"><b>$profiletxt[221]</b></span></td>
</tr>
EOT
	CoreLoad('Post');
	if($BCLoad || $BCSmile) { $displaycenter .= &BCWait; }
	if($BCLoad) { $displaycenter .= &BCLoad; }
	if($BCSmile) { $smileys = &BCSmile; }

	$displaycenter .= <<"EOT";
<tr><td class="win"><table width="100%"><tr>
 <td align="right" valign="top" width="30%"><b>$profiletxt[55]:</b><br><br>$smileys</span></td>
 <td valign="top" width="70%"><span class="smalltext"><textarea name="message" rows="12" cols="90" wrap="virtual">$userset{$URL{'u'}}->[11]</textarea><br>$profiletxt[57]: <b>$maxsig</b>; $profiletxt[169]: <b><span id="remain">$maxsig</span></b></span></td>
</tr></table></td></tr>
EOT
	if($maxsig ne $profiletxt[54]) {
		$displaycenter .= <<"EOT";
<SCRIPT LANGUAGE="JavaScript">
<!-- Begin
function tick() {
 textCounter();
 timerID = setTimeout("tick()",300);
}

function textCounter() {
 if(document.msend.message.value.length > $maxsig) { document.msend.message.value = document.msend.message.value.substring(0,$maxsig); }
  else {
   if(document.all){ remain.innerHTML = $maxsig - document.msend.message.value.length; }
   if(document.getElementById && !document.all){ document.getElementById('remain').innerHTML = $maxsig - document.msend.message.value.length; }
  }
}
tick();
//  End -->
</script>
EOT
	}
}

sub AvatarSetup {
	$maxsize = $maxsize2;
	if(!$apic) {
		$disablecaller = 1;
		$displaycenter = <<"EOT";
<table cellpadding="4" cellspacing="1" class="border" align="center" width="97%">
 <tr>
  <td class="win" align="center">$profiletxt[216]</td>
 </tr>
</table>
EOT
		return;
	}
	$file = $userset{$URL{'u'}}->[5];
	$avup = $userset{$URL{'u'}}->[32];
	$avsize = $userset{$URL{'u'}}->[35];

	if($avsize) { ($height,$width) = split(/\|/,$userset{$URL{'u'}}->[35]); }
		else { $width = $picwidth; $height = $picheight; }

	($fdname,$ffname) = split(/\//,$userset{$URL{'u'}}->[5]);

	opendir(DIR,"$avdir/");
	@dlist = readdir(DIR);
	fclose(DIR);
	foreach (sort {lc($a) cmp lc($b)} @dlist) {
		if($fname =~ /[#%+,\\\/:?"<>'|@^\$\&~'\)\(\]\[\;{}!`=]/ || $_ eq 'nopic.gif') { next; }
		$selected = '';
		($fname,$fext) = split(/\./,$_);
		$fext = lc($fext);
		if($userset{$URL{'u'}}->[5] eq $_) { $show = $_; $selected = " selected"; $found = 1; }
		$fname2 = $fname;
		$fname =~ s/\_/ /gsi;
		if($fext eq 'jpg' || $fext eq 'gif' || $fext eq 'jpeg' && $fname ne 'nopic') { $list{'basic'} .= qq~<option value="$_"$selected>$fname</option>~; }
		if($fext eq '' && $fname ne '') {
			$sel = '';
			if($fname2 eq $fdname) { $sel = ' selected'; $fldfnd = 1; }
			if($fname eq 'valueopen' || $fname eq 'value' || $fname eq 'basic' || $fname eq 'valueclose') { next; }
			$avslist .= qq~<option value="$_"$sel>$fname</option>~;

			opendir(DIR,"$avdir/$fname2");
			@blist = readdir(DIR);
			fclose(DIR);
			$varlist .= "$fname2 = '";
			foreach (sort {lc($a) cmp lc($b)} @blist) {
				$selected = '';
				($fname,$fext) = split(/\./,$_);
				$fext = lc($fext);
				if(($fname2 eq $fdname) && $ffname eq $_) { $show = $userset{$URL{'u'}}->[5]; $selected = " selected"; $found = 1; $aval = "$fname2/$_"; }
				$fname =~ s/\_/ /gsi;
				if($fext eq 'jpg' || $fext eq 'gif' && $fname ne 'nopic') { $varlist .= qq~<option value="$fname2/$_"$selected>$fname</option>~; }
			}
			$varlist .= "';\nif(list == '$fname2') { list = $fname2; }\n";
		}
	}
	$sel = '';
	if(!$found && $userset{$URL{'u'}}->[5] ne '') { $selected = " selected"; $cvalue = 2; $avurl = $userset{$URL{'u'}}->[5]; }
	if(!$fldfnd) { $sel = ' selected'; }

	if($userset{$URL{'u'}}->[32]) {
		$cvalue = 2;
		$uploaded .= <<"EOT";
<tr>
 <td colspan="2" align="center"><span class="smalltext"><b><br>$profiletxt[207]<br><br></b></span></td>
</tr>
EOT
	}

	if($found) { $cvalue = 1; }
	if(!$cvalue) { $cvalue = 3; $checked = ' checked'; }
	if($show eq '') { $scr = "$avsurl/nopic.gif"; } else { $scr = "$avsurl/$show"; $userset{$URL{'u'}}->[5] = "$avsurl/".$userset{$URL{'u'}}->[5]; }

	if($avup) { $avurl = ''; }
	if(!$perpic) { $perpic = $var{'60'}; }

	$morecaller = $profiletxt[262];
	$displaycenter .= <<"EOT";
<script language="javascript">
<!-- //
function ShowImage(showtimage) {
	tmpdoc = document.msend.av;
	if(!document.images) return;
	if(tmpdoc) { tmpdoc.value = showtimage; }
	document.images.preview.src = '$avsurl/'+showtimage;
}
function Change(id) {
	if(id == 3) { cvalue = true; } else { cvalue = false; }
	document.msend.avcheckbox.checked = cvalue;
	document.msend.avatartype.value = id;
}
//-->
</script>
<tr>
 <td class="catbg" align="center"><span class="smalltext"><b>$profiletxt[218]</b></span></td>
</tr><tr><td class="win"><table width="100%"><tr>
 <td align="center"><span class="smalltext"><b>$perpic</b></span></td>
</tr></table></td>
</tr><tr>
 <td class="catbg" align="center"><span class="smalltext"><b>$profiletxt[209]</b></span></td>
</tr><tr><td class="win"><table width="100%"><tr>
 <td>
  <table>
   <tr>
    <td align="center"><span class="smalltext"><b>$profiletxt[235]</b></span></td>
    <td align="center"><span class="smalltext"><b>$profiletxt[236]</b></span></td>
    <td><span class="smalltext"><b>$profiletxt[237]</b></span></td>
   </tr><tr>
    <td valign="top" width="180" align="center"><select name="avlist" size="9" onChange="FunctionList(this.value);" onFocus="Change(1);">
<option value="basic"$sel>$profiletxt[238]</option>
$avslist
<option value="">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</option>
</select></td>
    <td align="center" valign="top" height="125" width="200"><span id="vis"></span></td>
    <td width="200">&nbsp; &nbsp; &nbsp;<img name="preview" alt="$profiletxt[51]" src="$scr"></td>
   </tr>
  </table>
 </td>
</tr></table></td>
<script language="javascript">
function FunctionList(list) {
	if(list == '') { return; }
	valueopen = '<select name="avf" size="9" onChange="ShowImage(this.value);" onFocus="Change(1);">';
	valueclose = '<option value="nopic.gif">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</option></select>';
	basic = '$list{'basic'}';
	if(list == 'basic') { list = basic; }
$varlist
	if(document.all) {
		vis.innerHTML = valueopen+list+valueclose;
		pic = document.msend.avf.value;
		if(!pic) { pic = 'nopic.gif'; }
	}
	if(document.getElementById && !document.all) {
		document.getElementById('vis').innerHTML = valueopen+list+valueclose;
		pic = 'nopic.gif';
	}
	ShowImage(pic);
}
FunctionList(document.msend.avlist.value);
</script>
</tr>
EOT
	if($avupload || $apic == 1) {
		$displaycenter .= <<"EOT";
<tr>
 <td class="catbg" align="center"><span class="smalltext"><b>$profiletxt[211]</b></span></td>
</tr><tr><td class="win"><table width="100%">
EOT
		if($apic == 1) {
			$displaycenter .= <<"EOT";
<tr>
 <td width="40%" align="right"><b>$profiletxt[53]:</b></td>
 <td><input type="text"class="textinput" name="avcust" value="$avurl" size="30" onFocus="Change(2);"></td>
</tr>
EOT
		}

		if($avupload) {
			$maxsize = $maxsize <= 0 ? $profiletxt[161] : "$maxsize MB";
			if($maxsize > 0 && $maxsize < 1) { $maxsize = 1024*$maxsize . " KB"; }
			$displaycenter .= <<"EOT";
<tr>
 <td width="40%" align="right" valign="top"><b>$profiletxt[162]:</b></td>
 <td><span class="smalltext"><b><input type="file" class="textinput" name="ulfile" size="30" onFocus="Change(2);"><br>$profiletxt[208]:</b> $maxsize</span></td>
</tr>
EOT
		}

		$displaycenter .= <<"EOT";
$uploaded<tr>
 <td colspan="2"><hr color="$color{'border'}" class="hr" size="1"></td>
</tr><tr>
 <td width="40%" align="right" valign="top"><b>$profiletxt[212]:</b></td>
 <td valign="top"><input type="text" class="textinput" name="picwidth" value="$width" size="4" onFocus="Change(2);"> <span class="smalltext">&#215;</span> <input type="text" class="textinput" name="picheight" value="$height" size="4" onFocus="Change(2);"> &nbsp; <span class="smalltext">($profiletxt[214])<br><b>$profiletxt[213]:</b> $picheight &#215; $picwidth</span></span></td>
</tr>
</table></td></tr>
EOT
	}
	$displaycenter .= <<"EOT";
<tr>
 <td class="catbg" align="center"><span class="smalltext"><b>$profiletxt[50]</b></span></td>
</tr><tr><td class="win"><table width="100%"><tr>
 <td><input type="hidden" name="avatartype" value="$cvalue"><input type="hidden" name="av" value="$show"><input type="checkbox" class="checkboxinput" value="3" name="avcheckbox" onClick="Change(3);"$checked><span style="cursor:default;" onClick="Change(3);"> <span class="smalltext">$profiletxt[217]</span></span></td>
</tr></table></td></tr>
EOT
}
1;