################################################
# E-Blah Bulliten Board Systems       Platinum #
################################################
# Copyright (c) 2001 - 2004 e-blah!            #
# All Rights Reserved.                         #
################################################

CoreLoad('Moderate',1);

sub Moderate {
	if($URL{'a'} eq 'calendar' && ($settings[4] eq 'Administrator' || $calmod)) { CoreLoad('Calendar'); CalendarEvents(); }
	$ipon = 1 if($ismod);
	$ismod = 1 if($modon);

	foreach(@boardbase) {
		($id,$t,$t,$btit) = split("/",$_);
		$boards{$id} = $btit;

		fopen(TEMPMSG,"$boards/$id.msg");
		while($temp = <TEMPMSG>) {
			($tid,$ttitle) = split(/\|/,$temp);
			$thread{$tid} = "$id|$ttitle";
		}
		fclose(TEMPMSG);
	}

	if($URL{'a'} eq 'delnum') { Delnum(); }
	if($URL{'a'} eq 'ban') { IPBan(); }
	if($URL{'a'} eq 'ban2') { IPBan2(); }
	if($URL{'a'} eq 'sticky' && ($ston || $ismod)) { $ismod = 1; }
	elsif($URL{'a'} eq 'sticky' && !$ston) { $ismod = 0; }
	is_mod();
	if($URL{'a'} eq 'delthread') { DelThread(); }
	if($URL{'a'} eq 'move') { Move(); }
	if($URL{'a'} eq 'move2') { Move2(); }
	if($URL{'a'} eq 'sticky') { Sticky(); }
	if($URL{'a'} eq 'lock') { Lock(); }
	if($URL{'a'} eq 'split') { Split(); }
	if($URL{'a'} eq 'merge') { Merge(); }
	if($URL{'a'} eq 'mindex') { QMIndex(); }
	if($URL{'a'} eq 'modlog') { ModLog(); }
	CoreLoad('MessageDisplay'); MessageDisplay(); # No mod needed
}

sub QMIndex {
	$noredirh = 1;
	fopen(FILE,"$boards/$URL{'b'}.ino");
	@brdinfor = <FILE>;
	fclose(FILE);
	chomp @brdinfor;
	for($i = 0; $i <= $brdinfor[0]; $i++) {
		if($FORM{$i} eq '') { next; }
		$URL{'m'} = $FORM{$i};
		if($FORM{'opt1'}) { &Sticky; $oR = 1; }
		if($FORM{'opt2'}) { &Lock; $oR = 1; }
		if($FORM{'opt4'}) { &Move2; $oR = 1; }
		if($FORM{'opt3'} && !$oR) { &DelThread; } # Only if others were not run!
	}
	redirect("$scripturl,v=mindex");
}

sub Delnum {
	$mu = 1;
	fopen(FILE,"<$boards/$URL{'b'}.msg");
	@msg = <FILE>;
	fclose(FILE);
	foreach (@msg) {
		chomp $_;
		($mid,$t,$t,$t,$repliecnt,$t,$locked) = split(/\|/,$_);
		if($mid == $URL{'m'}) { $fnd = 1; last; }
	}

	if($fnd != 1) { error("$moderate[1]: $URL{'m'}.txt"); }
	if($locked) { error($moderate[2]); }
	if($repliecnt < $URL{'n'}) { error("$moderate[1]."); }
	$counter = 0;
	$mu = 0;
	fopen(FILE,"$messages/$URL{'m'}.txt");
	@usethis = <FILE>;
	fclose(FILE);
	foreach(@usethis) {
		chomp $_;
		($whoposted,$t,$t,$t,$fdate,$t,$t,$t,$afile) = split(/\|/,$_);
		if($counter == $URL{'n'}) {
			if(($settings[4] ne 'Administrator' && !$ismod && !$modifyon) && $whoposted ne $username) { push(@newmess,$_); $mu = 1; ++$counter; next; }
			if($modifytime && ($settings[4] ne 'Administrator' && !$ismod && !$modifyon) && $fdate+($modifytime*3600) < time) { error($moderate[72]); }

			if($afile ne '') {
				foreach $deletefile (split(/\//,$afile)) { unlink("$uploaddir/$deletefile","$prefs/Hits/$deletefile.txt"); }
			}
			$pdu = $whoposted;
		} else { push(@newmess,$_); }
		++$counter;
	}
	if($mu) { error($moderate[4]); }

	fopen(FILE,"+>$messages/$URL{'m'}.txt");
	foreach(@newmess) { print FILE "$_\n"; }
	fclose(FILE);

	if($counter == 1) { DelThread(); }

	fopen(FILE,"+<$boards/$URL{'b'}.msg") || &error("$moderate[1]: $URL{'b'}.msg");
	@fdata = <FILE>;
	chomp @fdata;
	for($i = 0; $i < @fdata; $i++) {
		($mid,$c,$d,$e,$reply,$b,$locked,$a,$ff,$jj) = split(/\|/,$fdata[$i]);
		if($fdata[$i] =~ m/\A$URL{'m'}\|/) {
			$reply = @newmess-1;
			($jj,$t,$t,$t,$ff) = split(/\|/,$newmess[$reply]);
		}
		push(@writedata,"$ff|$mid|$c|$d|$e|$reply|$b|$locked|$a|$jj");
	}

	truncate(FILE,0);
	seek(FILE,0,0);

	foreach(sort{$b <=> $a} @writedata) {
		($q,$mid,$c,$d,$e,$reply,$b,$locked,$a,$z) = split(/\|/,$_);
		if($mid eq '') { next; }
		print FILE "$mid|$c|$d|$e|$reply|$b|$locked|$a|$q|$z\n";
		$totalreplys += $reply;
		++$mtotal;
	}
	fclose(FILE);
	$totalreplys += $mtotal;

	fopen(FILE,"+>$boards/$URL{'b'}.ino");
	print FILE "$mtotal\n$totalreplys\n";
	fclose(FILE);

	MLogging($URL{'m'},8,$username,time,"$URL{'n'}/$pdu",$URL{'b'});

	redirect("$scripturl,v=display,m=$URL{'m'}");
}

sub Lock {
	if($URL{'m'} eq '') { error($gtxt{'error2'}); }

	$fnd = 0;
	$counter = 0;
	fopen(FILE,"+<$boards/$URL{'b'}.msg") || &error("$moderate[1]: $URL{'b'}.msg",1);
	$readdata = 1;
	while($readdata) {
		$messageend = tell FILE; # Get location
		$readdata = <FILE>;
		($tmid) = split(/\|/,$readdata);
		if($tmid eq $URL{'m'}) { $fnd = 1; last; }
	}
	if($fnd) {
		seek(FILE,$messageend,0);
		chomp $readdata;
		($mid,$a,$b,$c,$d,$e,$lock,$f,$sd,$ds) = split(/\|/,$readdata);
		$lock = $lock ? 0 : 1;
		print FILE "$mid|$a|$b|$c|$d|$e|$lock|$f|$sd|$ds\n";
	}
	fclose(FILE);

	MLogging($URL{'m'},3,$username,time,$lock,$thread{$URL{'m'}});

	$url = $lock ? "$scripturl,v=mindex" : "$scripturl,v=display,m=$URL{'m'}";
	if(!$noredirh) { &redirect; }
}

sub Sticky {
	my($noadd,$added);
	if($access && $settings[4] ne 'Administrator') { &error($gtxt{'error'}); }

	fopen(FILE,"+<$boards/Stick.txt");
	@sticky = <FILE>;
	truncate(FILE,0);
	seek(FILE,0,0);
	foreach(@sticky) {
		chomp $_;
		($bdat,$tdat) = split(/\|/,$_);
		if($tdat eq $URL{'m'}) { $noadd = 1; next; }
		print FILE "$_\n";
	}
	if(!$noadd && !$unstickstick) { print FILE "$URL{'b'}|$URL{'m'}\n"; $added = 1; }
	fclose(FILE);

	MLogging($URL{'m'},2,$username,time,$added,$thread{$URL{'m'}});

	if(!$noredirh && !$unstickstick) { redirect("$scripturl,v=mindex"); }
}

sub DelThread {
	my($mtotal,$totalreplys,$wefnd);

	fopen(FILE,"+<$boards/$URL{'b'}.msg") || &error("$moderate[1]: $URL{'b'}.msg");
	@fdata = <FILE>;
	truncate(FILE,0);
	seek(FILE,0,0);
	foreach(@fdata) {
		($t,$mtitle,$t,$t,$replies) = split(/\|/,$_);
		if($_ =~ m/\A$URL{'m'}\|/) { $wefnd = 1; $tsub = $mtitle; next; }
		print FILE $_;
		$totalreplys += $replies;
		++$mtotal;
	}
	fclose(FILE);
	$totalreplys += $mtotal;

	fopen(FILE,"+>$boards/$URL{'b'}.ino");
	print FILE "$mtotal\n$totalreplys\n";
	fclose(FILE);

	if(!$noredirh && !$wefnd) { &error("$moderate[1]."); }

	fopen(FILE,"$messages/$URL{'m'}.txt");
	@messagedata = <FILE>;
	fclose(FILE);
	chomp @messagedata;
	foreach(@messagedata) {
		($t,$t,$t,$t,$t,$t,$t,$t,$afile) = split(/\|/,$_);
		if($afile ne '') {
			foreach $deletefile (split(/\//,$afile)) { unlink("$uploaddir/$deletefile","$prefs/Hits/$deletefile.txt"); }
		}
	}

	unlink("$messages/$URL{'m'}.txt","$messages/$URL{'m'}.rate","$messages/$URL{'m'}.txt.temp","$messages/$URL{'m'}.view","$messages/$URL{'m'}.polled","$messages/$URL{'m'}.mail","$messages/$URL{'m'}.poll","$messages/$URL{'m'}.log");

	$unstickstick = 1;
	Sticky();

	MLogging($URL{'m'},6,$username,time,$mtitle,$thread{$URL{'m'}});

	if(!$noredirh) { redirect("$scripturl,v=mindex"); }
}

sub Move {
	fopen(FILE,"$boards/$URL{'b'}.msg");
	while(<FILE>) {
		($mid) = split(/\|/,$_);
		if($mid eq $URL{'m'}) { $ok = 1; }
	}
	fclose(FILE);
	if(!$ok) { error($moderate[14]); }

	foreach(@catbase) {
		($t,$t,$access,$bhere) = split(/\|/,$_);
		@bdsh = split("/",$bhere);
		foreach $hh (@bdsh) {
			foreach $bbase (@boardbase) {
				($bid,$t,$t,$name) = split("/",$bbase);
				if($bid eq $URL{'b'} || $bid ne $hh) { next; }
				$ops .= qq~<option value="$bid">$name</option>\n~;
				last;
			}
		}
	}
	if($ops eq '') { error($moderate[69]); }

	if($showmove) {
		$totalmove = <<"EOT";
<tr>
 <td align="right" width="30%"><b>$moderate[70]</b><br><span class="smalltext">$moderate[71]</span></td>
 <td width="70%" valign="top"><input type="checkbox" class="checkboxinput" name="addmessage" value="1" checked></td>
</tr>
EOT
	}

	$title = $moderate[18];
	header();
	$ebout .= <<"EOT";
<table class="border" cellpadding="5" cellspacing="1" width="750" align=center>
 <tr><form action="$surl,v=mod,b=$URL{'b'},a=move2,m=$URL{'m'}" method="post" name="msend">
  <td class="titlebg"><b>$title</b></td>
 </tr><tr>
  <td class="win2"><span class="smalltext">$moderate[19]</span></td>
 </tr><tr>
  <td class="win">
   <table cellpadding="4" cellspacing="0" width="100%">
    <tr>
     <td align="right" width="30%"><b>$moderate[20]:</b></td>
     <td width="70%"><select name="b">$ops</select></td>
    </tr>$totalmove
   </table>
  </td>
 </tr>
EOT
	if($showmove) {
		CoreLoad("Post");
		if($BCLoad || $BCSmile) { &BCWait; }
		if($BCLoad) { &BCLoad; }
		$ebout .= <<"EOT";
 <tr>
  <td class="win">
   <table cellpadding="4" cellspacing="0" width="100%">
    <tr>
     <td align="right" width="30%" valign="top"><b>$moderate[16]:</b><br><span class="smalltext">$moderate[17]</span><br><br>
EOT
		if($BCSmile) { &BCSmile; }
		$ebout .= <<"EOT";
    </td>
     <td width="70%"><textarea name="message" wrap="virtual" rows="12" cols="95"></textarea></td>
    </tr>
   </table>
  </td>
 </tr>
EOT
	}
	$ebout .= <<"EOT";
 <tr>
  <td class="win2" align="center"><input type="submit" class="button" name="move" value="&nbsp;$moderate[21] "></td>
 </tr></form>
</table>
EOT
	footer();
	exit;
}

sub Move2 {
	my($totalreplys,$mtotal,$totalreplys2,$mtotal2,@savedata,$stuck);

	# Basics
	fopen(FILE,"$boards/$URL{'b'}.msg");
	while(<FILE>) {
		($mid,$sub) = split(/\|/,$_);
		if($mid eq $URL{'m'}) { $ok = 1; $tsub = $sub; last; }
	}
	fclose(FILE);
	if(!$ok) { error($moderate[14]); }

	if($FORM{'b'} eq '') { error($gtxt{'bfield'}); }
	if($FORM{'b'} eq $URL{'b'}) { error($gtxt{'bfield'}); }
	foreach(@boardbase) {
		($okay) = split("/",$_);
		if($okay eq $FORM{'b'}) { $error = 1; }
	}
	if(!$error) { error($gtxt{'bfield'}); }

	$movingid = $URL{'m'};

	# Remove from board ...
	fopen(FILE,"+<$boards/$URL{'b'}.msg") || &error("$moderate[1]: $URL{'b'}.msg");
	@fdata = <FILE>;

	truncate(FILE,0);
	seek(FILE,0,0);

	$message = Format($FORM{'message'});
	if($showmove && $FORM{'addmessage'} && $message ne '') { # Create a forwarding message
		$new = time;
		fopen(MESS,">$messages/$new.txt");
		print MESS qq~$username|$message<br><br>\[size=1\]$moderate[24] \[url=$rurl,b=$FORM{'b'},m=$URL{'m'},v=display\]$moderate[25]\[/url\] $moderate[26] \[url=$rurl,v=memberpanel,a=view,u=$username]$settings[1]\[/url\].\[/size\]|$ENV{'REMOTE_ADDR'}|$settings[2]|$new\n~;
		fclose(MESS);
		fopen(VIEW,">$messages/$new.view");
		print VIEW "0\n";
		fclose(VIEW);
		print FILE "$new|$moderate[52]: $sub|$username|$new|0|0|1|xx|$new|$username\n";

		# Ensure thread left is not marked as "new"
		$URL{'m'} = $new;
		&LogPage;
	}

	foreach(@fdata) {
		if($_ =~ m/\A$movingid\|/) { $movemessage = $_; next; }
		print FILE $_;
		($t,$t,$t,$t,$replies) = split(/\|/,$_);
		$totalreplys += $replies;
		++$mtotal;
	}
	fclose(FILE);
	$totalreplys += $mtotal;

	fopen(FILE,"+>$boards/$URL{'b'}.ino");
	print FILE "$mtotal\n$totalreplys\n";
	fclose(FILE);

	# Write to target board ...
	fopen(FILE,"+<$boards/$FORM{'b'}.msg") || &error("$moderate[1]: $FORM{'b'}.msg");
	my @tempdata = <FILE>;
	push(@tempdata,$movemessage);
	chomp @tempdata;

	foreach(@tempdata) {
		($mid,$sub,$poster,$pdate,$replies,$poll,$lock,$micon,$lposttime,$luserwhoposted) = split(/\|/,$_);
		push(@savedata,"$lposttime|$mid|$sub|$poster|$pdate|$replies|$poll|$lock|$micon|$luserwhoposted");
		$totalreplys2 += $replies;
		++$mtotal2;
	}

	truncate(FILE,0);
	seek(FILE,0,0);

	foreach(sort{$b <=> $a} @savedata) {
		($q,$mid,$c,$d,$e,$reply,$b,$locked,$a,$z) = split(/\|/,$_);
		if($mid eq '') { next; }
		print FILE "$mid|$c|$d|$e|$reply|$b|$locked|$a|$q|$z\n";
	}
	fclose(FILE);
	$totalreplys2 += $mtotal2;

	fopen(FILE,"+>$boards/$FORM{'b'}.ino");
	print FILE "$mtotal2\n$totalreplys2\n";
	fclose(FILE);

	# Check if it was sticky ...
	fopen(FILE,"+<$boards/Stick.txt");
	@sticky = <FILE>;
	truncate(FILE,0);
	seek(FILE,0,0);
	foreach(@sticky) {
		chomp $_;
		($bdat,$tdat) = split(/\|/,$_);
		if($tdat eq $movingid) { $stuck = 1; next; }
		print FILE "$_\n";
	}
	if($stuck) { print FILE "$FORM{'b'}|$movingid\n"; }
	fclose(FILE);

	MLogging($movingid,1,$username,time,$FORM{'b'},$FORM{'b'});

	if(!$noredirh) { redirect("$surl,b=$FORM{'b'},m=$movingid,v=display"); }
}

sub IPBan {
	&IPBanOk;
	foreach(@banlist) {
		($ips,$days) = split(/\|/,$_);
		if($URL{'ip'} eq $ips) { $ipfnd = 1; }
	}
	if($ipfnd) { $message = "$moderate[28] ($URL{'ip'}) $moderate[29]"; $what = $moderate[30]; }
		else { $message = <<"EOT";
$moderate[31]
<table cellpadding="2" width="75%">
 <tr>
  <td align="right"><b>$moderate[49]:</b></td>
  <td><select name="ban">
<option value="forever">$moderate[48]</option>
<option value="1">1 $moderate[47]</option>
<option value="3">3 $moderate[47]</option>
<option value="7">1 $gtxt{'39'}</option>
<option value="30">1 $gtxt{'40'}</option>
</select></td>
 </tr><tr>
  <td align="right"><b>$moderate[50]:</b></td>
  <td><input type="grp" name="grp"></td>
 </tr>
</table>
EOT
			$what = $moderate[33];
		}
	if($URL{'ip'} !~ /[1-9.]/ || $URL{'ip'} =~ /[A-Za-z]/) { &error($moderate[34]); }
	$title = $moderate[33];
	&header;
	$ebout .= <<"EOT";
<table class="border" cellspacing="1" cellpadding="4" width="450" align="center">
 <tr><form action="$scripturl,v=mod,a=ban2,ip=$URL{'ip'},m=$URL{'m'}" method="post" name="msend">
  <td class="titlebg"><b><img src="$images/ban.gif"> $title</b></td>
 </tr><tr>
  <td class="win">$message</td>
 </tr><tr>
  <td class="win2"><b><input type="submit" class="button" value=" $what "></b></td>
 </tr>
</table>
EOT
	&footer;
	exit;
}

sub IPBan2 {
	&IPBanOk;
	foreach(@banlist) {
		($ips,$days) = split(/\|/,$_);
		if($URL{'ip'} eq $ips) { $noadd = 1; next; }
			else { $readd .= "$_\n"; }
	}
	if($noadd != 1) {
		if($FORM{'ban'} ne 'forever') {
			$expire = time+($FORM{'ban'}*86400);
			$URL{'ip'} .="|$FORM{'ban'}|$expire";
		} else { $URL{'ip'} .= "||"; }
		$FORM{'grp'} =~ s/ /_/g;
		$FORM{'grp'} =~ s/[#%+,\\\/:?"<>'|@^\$\&~'\)\(\]\[\;{}!`=-]//g;
		$readd .= "$URL{'ip'}|$FORM{'grp'}\n";
	}
	fopen(FILE,"+>$prefs/BanList.txt");
	print FILE $readd;
	fclose(FILE);
	$url = "$scripturl,v=display,m=$URL{'m'}";
	if($URL{'tf'} ne '') { $url = "$scripturl,v=admin,a=reports"; }
	&redirect;
}

sub IPBanOk {
	if($URL{'ip'} eq $ENV{'REMOTE_ADDR'}) { &error($moderate[36]); }
	if($ipon != 1 && $settings[4] ne 'Administrator') { &error($gtxt{'error'}); }
	fopen(FILE,"$prefs/BanList.txt");
	@banlist = <FILE>;
	fclose(FILE);
	chomp @banlist;
	if($URL{'ip'} !~ /[1-9.]/ || $URL{'ip'} =~ /[A-Za-z]/) { &error($gtxt{'bfield'}); }
}

sub OldThreads {
	&is_admin;
	CoreLoad('AdminList');
	if($URL{'p'}) { &OldThreads2; }

	$title = $moderate[41];
	&headerA;
	$ebout .= <<"EOT";
<table class="border" cellpadding="4" cellspacing="1" width="500" align="center">
 <tr><form action="$scripturl,v=admin,a=remove,p=1" method="post" name="msend" enctype="multipart/form-data">
  <td class="titlebg"><b><img src="$images/brd_main.gif"> $title</b></td>
 </tr><tr>
  <td class="win"><span class="smalltext">$moderate[37]</span></td>
 </tr><tr>
  <td class="win2"><b>$moderate[38] <input type="text" class="textinput" name="days" value="30" size="4"> $moderate[39].</b></td>
 </tr><tr>
  <td class="catbg" align="center"><span class="smalltext"><b>$moderate[40]</b></span></td>
 </tr><tr>
  <td class="win"><select name="boards" size="6" multiple>
EOT
	foreach(@boardbase) {
		($obds,$t,$t,$name) = split("/",$_);
		$ebout .= qq~<option value="$obds" selected>$name</option>\n~;
		++$c;
		$next = '';
	}
	$ebout .= <<"EOT";
  </select></td>
 </tr><tr>
  <td class="win2" align="center"><input type="submit" class="button" value=" $moderate[41] "> <input type="reset" class="button" value=" $moderate[42] "></td>
 </form></tr>
</table>
EOT
	&footerA;
	exit;
}

sub OldThreads2 {
	# Lock the forum ...
	fopen(FILE,">$root/Maintance.lock");
	print FILE "\$maintance = 1;\n\$maintancer = qq~$moderate[51]~;\n1;";
	fclose(FILE);

	$threadold = time-($FORM{'days'}*86400);

	$counter = 0;
	@delbds = split(",",$FORM{'boards'});
	foreach(@delbds) { $temp{$_} = 1; }

	fopen(FILE,"$boards/Stick.txt");
	@sticky = <FILE>;
	fclose(FILE);
	chomp @sticky;
	foreach(@sticky) {
		($bdat,$tdat) = split(/\|/,$_);
		if(!$temp{$bdat}) { $other = "$_\n"; next; }
		$stick{$tdat} = $bdat;
	}

	fopen(MODLOG,">>$prefs/Moderator.log");
	foreach $open (@delbds) {
		unlink("$boards/$open.msg.bak","$boards/$open.msg.temp");
		rename("$boards/$open.msg","$boards/$open.msg.bak");
		fopen(FILE,"$boards/$open.msg.bak") || &error("$moderate[1]: $open.msg.bak");
		@messdata = <FILE>;
		fclose(FILE);
		chomp @messdata;
		fopen(FILE,">$boards/$open.msg.temp");
		foreach(@messdata) {
			($id,$mtitle) = split(/\|/,$_);
			if($id > $threadold || $stick{$id}) { # If thread is old, or is stuck, leave it ...
				print FILE "$_\n";
				next;
			}

			$keep = 0;

			fopen(MESS,"$messages/$id.txt");
			@mread = <MESS>;
			fclose(MESS);
			chomp @mread;
			foreach $read (@mread) {
				($t,$t,$t,$t,$oldtime,$t,$t,$t,$afile) = split(/\|/,$read);
				if($afile) {
					foreach $deletefile (split(/\//,$afile)) { unlink("$uploaddir/$deletefile","$prefs/Hits/$deletefile.txt"); }
				}
				if($oldtime > $threadold) { # Thread is old, but has been active within the past x days
					print FILE "$_\n";
					$keep = 1;
					last;
				}
			}
			if(!$keep) {
				print MODLOG "7|$username|".time."|$mtitle|$ENV{'REMOTE_ADDR'}|$id|$open|$mtitle\n";
				unlink("$messages/$id.txt","$messages/$id.rate","$messages/$id.txt.temp","$messages/$id.view","$messages/$id.polled","$messages/$id.mail","$messages/$id.poll");
				++$counter;
			}
		}
		fclose(FILE);
		unlink("$boards/$open.msg.bak");
		rename("$boards/$open.msg.temp","$boards/$open.msg");
	}
	fclose(MODLOG);

	CoreLoad('Admin1');
	Repop();

	$title = $moderate[62];
	&headerA;
	$ebout .= <<"EOT";
<table class="border" cellpadding="4" cellspacing="1" width="500" align="center">
 <tr>
  <td class="titlebg"><b><img src="$images/brd_main.gif"> $title</b></td>
 </tr><tr>
  <td class="win"><span class="smalltext"><b>$moderate[45]: $counter<br>$moderate[46]: $attcnt</b></span></td>
 </tr><tr>
  <td class="win2"><b><a href="javascript:history.back(-2)">&#171; $gtxt{'26'}</a></b></td>
 </tr>
</table>
EOT
	footerA();
	exit;
}

sub Split {
	$messid = $URL{'m'};
	fopen(FILE,"$boards/$URL{'b'}.msg") || error("$moderate[1]: $URL{'b'}.msg");
	while(<FILE>) {
		chomp;
		($messageid,$ptitle,$t,$t,$treplies) = split(/\|/,$_);
		if($messid == $messageid) { $fnd = 1; last; }
	}
	fclose(FILE);
	if(!$fnd) { error("$moderate[1]: $messageid.txt"); }

	fopen(FILE,"$messages/$messageid.txt") || error("$moderate[1]: $messageid.txt");
	@message = <FILE>;
	fclose(FILE);
	$message = @message;
	if($message == 1) { &error($moderate[53]); }
	if($URL{'p'}) { &Split2; }

	foreach(@catbase) {
		($t,$t,$access,$bhere) = split(/\|/,$_);
		@bdsh = split("/",$bhere);
		foreach $hh (@bdsh) {
			foreach $bbase (@boardbase) {
				($bid,$t,$t,$name) = split("/",$bbase);
				if($bid ne $hh) { next; }
				$check = $bid eq $URL{'b'} ? " selected" : '';
				$ops .= qq~     <option value="$bid"$check>$name</option>\n~;
				last;
			}
		}
	}

	$title = $moderate[54];
	&header;
	$ebout .= <<"EOT";
<table cellpadding="4" cellspacing="1" class="border" align="center" width="750">
 <tr><form action="$scripturl,v=mod,a=split,m=$URL{'m'},p=1" method="post">
  <td class="titlebg"><b>$title</b></td>
 </tr><tr>
  <td class="win"><span class="smalltext">$moderate[55]</span></td>
 </tr><tr>
  <td class="win2">
   <table cellpadding="4" cellspacing="0" width="100%">
    <tr>
     <td align="right" width="45%"><b>$moderate[56]:</b></td>
     <td width="55%">$ptitle</td>
    </tr><tr>
     <td colspan="2"><hr color="$color{'border'}" class="hr" size="1"></td>
    </tr><tr>
     <td align="right" width="45%"><b>$moderate[57]:</b></td>
     <td width="55%"><input type="text" class="textinput" name="title" size="30" maxlength="50"></td>
    </tr><tr>
     <td align="right" width="45%"><b>$moderate[58]:</b></td>
     <td width="55%"><select name="board">$ops</select></td>
    </tr>
   </table>
  </td>
 </tr>
</table>
<br>
<table class="border" cellpadding="4" cellspacing="1" width="750" align="center">
 <tr>
  <td valign="top" align="center" class="catbg"><span class="smalltext"><b>$moderate[59]</b></span></td>
  <td class="catbg" align="center"><span class="smalltext"><b>$moderate[60]</b></span></td>
 </tr>
EOT
	@bgcolors = ('win','win2');
	$count = 0;
	foreach(@message) {
		$bgcolor = $bgcolors[$count % 2];
		($postinguser,$message,$ip,$email,$date,$nosmile) = split(/\|/,$_);
		loaduser($postinguser);
		if($userset{$postinguser}->[1] eq '') { $postedby = $postinguser; }
			else { $postedby = qq~<a href="$scripturl,v=memberpanel,a=view,u=$postinguser">$userset{$postinguser}->[1]</a>~; }
		$datepost = get_date($date);

		if(length($message) > 200) {
			$message =~ s~\[table\](.*?)\[\/table\]~$var{'88'}~sgi;
			$message = substr($message,0,400);
			&BC;
			&MakeSmall;
			$message .= " ...";
		} else { &BC; }

		if($count > 0) { $split = qq~<input type="checkbox" class="checkboxinput" name="split_$count" value="1">~; }

		$ebout .= <<"EOT";
 <tr>
  <td align="center" class="$bgcolor">$split</td>
  <td class="$bgcolor">
  <table cellpadding="1" width="100%">
   <tr>
    <td><span class="smalltext"><b>$gtxt{'19'}:</b> $postedby</span></td>
    <td align="right"><span class="smalltext"><b>$gtxt{'21'}:</b> $datepost</span></td>
   </tr><tr>
    <td colspan="2"><hr color="$color{'border'}" class="hr" size="1"><span class="smalltext">$message</span></td>
   </tr>
  </table></td>
 </tr>
EOT
		++$count;
	}
	$bgcolor = $bgcolors[$count % 2];
	$ebout .= <<"EOT";
  </td>
 </tr><tr>
  <td class="$bgcolor" align="center" colspan="2"><input type="submit" class="button" value=" $moderate[54] "></td>
 </tr>
</table>
EOT
	&footer;
	exit;
}

sub Split2 {
	$sub = Format($FORM{'title'});
	if($sub eq '') { &error($gtxt{'bfield'}); }
	if(length($sub) > 50) { &error($gtxt{'bfield'}); }
	if($FORM{'board'} eq '') { &error($gtxt{'bfield'}); }

	if(!-e("$boards/$URL{'b'}.msg")) { &error("$moderate[1]: $URL{'b'}.msg"); }
	if(!-e("$boards/$FORM{'board'}.msg")) { &error("$moderate[1]: $FORM{'board'}.msg"); }

	# Lets split the message files first ...
	$count = 0;
	fopen(FILE,"+<$messages/$messageid.txt") || error("$moderate[1]: $messageid.txt");
	@tempopen = <FILE>;
	chomp @tempopen;
	foreach(@tempopen) {
		if(!$FORM{"split_$count"} || $count == 0) {
			($lopostinguser,$t,$t,$t,$lodate) = split(/\|/,$_);
			push(@rewrite,$_);
		} else {
			if(!$first) { ($postinguser,$message,$ip,$email,$date,$nosmile) = split(/\|/,$_); $first = 1; }
			($lpostinguser,$t,$t,$t,$ldate) = split(/\|/,$_);
			++$first4;
			push(@addto,$_);
		}
		++$count;
	}
	if(!$first4) {
		fclose(FILE);
		&error($moderate[61]);
	}
	truncate(FILE,0);
	seek(FILE,0,0);
	foreach(@rewrite) { print FILE "$_\n"; }
	fclose(FILE);
	--$first4;

	# Lets find a valid thread to write to ...
	for($e = 0; $e < 99; $e++) {
		if(-e "$messages/$date.txt") { ++$date; } else { last; }
	}

	# Write to it now
	fopen(FILE,">$messages/$date.txt");
	foreach(@addto) { print FILE "$_\n"; }
	fclose(FILE);
	fopen(FILE,">$messages/$date.view");
	fclose(FILE);

	$newboarddata = "$ldate|$date|$sub|$postinguser|$date|$first4|0|0|xx|$lpostinguser";

	# Now comes the tricky part, edit message data.
	fopen(FILE,"+<$boards/$URL{'b'}.msg") || &error("$moderate[1]: $URL{'b'}.msg");
	@tempopen = <FILE>;
	chomp @tempopen;
	foreach(@tempopen) {
		($mid,$sub,$poster,$pdate,$replies,$poll,$lock,$micon,$lposttime,$luserwhoposted) = split(/\|/,$_);
		if($mid eq $URL{'m'}) {
			$replies -= $first4+1;
			$repcnt += $replies;
			$lposttime = $lodate;
			$luserwhoposted = $lopostinguser;
		} else { $repcnt += $replies; }
		push(@tempwrite,"$lposttime|$mid|$sub|$poster|$pdate|$replies|$poll|$lock|$micon|$luserwhoposted");
	}
	if($FORM{'board'} eq $URL{'b'}) { push(@tempwrite,$newboarddata); $repcnt += $first4; $aadd = 1; }
	truncate(FILE,0);
	seek(FILE,0,0);
	foreach(sort{$b <=> $a} @tempwrite) {
		($q,$mid,$c,$d,$e,$reply,$b,$locked,$a,$z) = split(/\|/,$_);
		if($mid eq '') { next; }
		print FILE "$mid|$c|$d|$e|$reply|$b|$locked|$a|$q|$z\n";
		++$num;
	}
	fclose(FILE);
	$repcnt += $num;
	fopen(FILE,">$boards/$URL{'b'}.ino");
	print FILE "$num\n$repcnt\n";
	fclose(FILE);

	if(!$aadd) { # Move split message to another board ...
		$num = 0;
		fopen(FILE,"+<$boards/$FORM{'board'}.msg") || &error("$moderate[1]: $FORM{'board'}.msg");
		@tempopen = <FILE>;
		chomp @tempopen;
		foreach(@tempopen) {
			($mid,$sub,$poster,$pdate,$replies,$poll,$lock,$micon,$lposttime,$luserwhoposted) = split(/\|/,$_);
			$copyto += $replies;
			push(@tempwriteto,"$lposttime|$mid|$sub|$poster|$pdate|$replies|$poll|$lock|$micon|$luserwhoposted");
		}
		push(@tempwriteto,$newboarddata);
		$copyto += $first4;
		truncate(FILE,0);
		seek(FILE,0,0);
		foreach(sort{$b <=> $a} @tempwriteto) {
			($q,$mid,$c,$d,$e,$reply,$b,$locked,$a,$z) = split(/\|/,$_);
			if($mid eq '') { next; }
			print FILE "$mid|$c|$d|$e|$reply|$b|$locked|$a|$q|$z\n";
			++$num;
		}
		fclose(FILE);
		$copyto += $num;
		fopen(FILE,">$boards/$FORM{'board'}.ino");
		print FILE "$num\n$copyto\n";
		fclose(FILE);
	}

	MLogging($messageid,4,$username,time,$sub,$FORM{'board'});

	redirect("$surl,b=$FORM{'board'},v=display,m=$date");
}

sub Merge {
	if($FORM{'topic'} =~ s/http:\/\///gsi) {
		@url = split(/\,/,$FORM{'topic'});
		foreach (@url) {
			($action,$actiondo) = split("=",$_);
			if($action eq 'm') { $getfrom = $actiondo; }
		}
	} else { $getfrom = $FORM{'topic'}; }

	foreach(@boardbase) {
		($oboard,$desc,$t,$bnme) = split("/",$_);
		fopen(FILE,"$boards/$oboard.msg");
		while(<FILE>) {
			chomp $_;
			($mid,$t) = split(/\|/,$_);
			if($getfrom eq $mid) { $mergebrd = $oboard; }
			if($URL{'m'} eq $mid) { $ttitle = $t; $urlthread = $mid; }
			if($urlthread && $mergebrd) { last; }
		}
		fclose(FILE);
		if($urlthread && $mergebrd) { last; }
	}
	if($urlthread eq '') { &error("$moderate[1]: $urlthread"); }
	if($URL{'p'}) { &Merge2; }
	$title = $moderate[67];
	&header;
	$ebout .= <<"EOT";
<table cellpadding="4" cellspacing="1" class="border" width="700" align="center">
 <tr><form action="$scripturl,v=mod,a=merge,m=$URL{'m'},p=1" method="post">
  <td class="titlebg"><b>$title</b></td>
 </tr><tr>
  <td class="win"><span class="smalltext">$moderate[63]</span></td>
 </tr><tr>
  <td class="win2">
   <table width="100%" cellpadding="4" cellspacing="0">
    <tr>
     <td width="40%" align="right"><b>$moderate[64]:</b></td>
     <td width="60%" valign="top"><input type="text" class="textinput" name="subject" value="$ttitle" maxlength="50" size="40"></td>
    </tr><tr>
     <td width="40%" align="right"><b>$moderate[65]:</b><br><span class="smalltext">$moderate[66]</span></td>
     <td width="60%" valign="top"><input type="text" class="textinput" name="topic" size="50"></td>
    </tr>
   </table>
  </td>
 </tr><tr>
  <td class="win" align="center"><input type="submit" class="button" value="$moderate[67]"></td>
 </form></tr>
</table>
EOT
	&footer;
	exit
}

sub Merge2 {
	$subject = Format($FORM{'subject'});
	if(length($subject) > 50) { &error($gtxt{'bfield'}); }
	if($subject eq ' ' || $subject eq '&nbsp;' || $subject eq '') { &error($gtxt{'bfield'}); }
	if(!$mergebrd) { &error("$moderate[1]: $getfrom"); }
	if($getfrom eq $urlthread) { &error($moderate[68]); }

	fopen(FILE,"$messages/$getfrom.txt");
	while(<FILE>) {
		chomp $_;
		($user,$message,$ipaddr,$email,$fdate,$nosmile,$mod4,$mod2,$afile) = split(/\|/,$_);
		push(@cont,"$fdate|$user|$message|$ipaddr|$email|$nosmile|$mod4|$mod2|$afile");
	}
	fclose(FILE);

	$URLbackup = $URL{'m'};
	$URL{'m'} = $getfrom;
	$unstickstick = 1;
	&Sticky;
	$URL{'m'} = $URLbackup;

	unlink("$messages/$getfrom.poll","$messages/$getfrom.polled","$messages/$getfrom.view","$messages/$getfrom.txt","$messages/$getfrom.rate","$messages/$getfrom.txt.temp","$messages/$getfrom.mail","$messages/$getfrom.log");

	fopen(FILE,"$messages/$URL{'m'}.txt");
	while(<FILE>) {
		chomp $_;
		($user,$message,$ipaddr,$email,$fdate,$nosmile,$mod4,$mod2,$afile) = split(/\|/,$_);
		push(@cont,"$fdate|$user|$message|$ipaddr|$email|$nosmile|$mod4|$mod2|$afile");
	}
	fclose(FILE);
	fopen(FILE,">$messages/$URL{'m'}.txt");
	foreach(sort{$a <=> $b} @cont) {
		($fdate,$user,$message,$ipaddr,$email,$nosmile,$mod4,$mod2,$afile) = split(/\|/,$_);
		if(!$threadstart) { $threadstart = $user; $starttime = $fdate; }
		print FILE "$user|$message|$ipaddr|$email|$fdate|$nosmile|$mod4|$mod2|$afile\n";
		++$messagecnt;
	}
	fclose(FILE);

	if(-e("$messages/$starttime.txt")) { $starttime = $URL{'m'}; }

	# Keep the old stickys ...
	foreach (@sticky) {
		($bdat,$tdat) = split(/\|/,$_);
		if($tdat eq $URL{'m'}) { $restick = 1; }
	}
	if($restick) {
		fopen(FILE,"+>$boards/Stick.txt");
		foreach(@sticky) {
			($bdat,$tdat) = split(/\|/,$_);
			if($tdat eq $URL{'m'}) { $tdat = $starttime; }
			print FILE "$bdat|$tdat\n";
		}
		fclose(FILE);
	}
	rename("$messages/$URL{'m'}.txt","$messages/$starttime.txt");
	rename("$messages/$URL{'m'}.rate","$messages/$starttime.rate");
	rename("$messages/$URL{'m'}.polled","$messages/$starttime.polled");
	rename("$messages/$URL{'m'}.poll","$messages/$starttime.poll");
	rename("$messages/$URL{'m'}.mail","$messages/$starttime.mail");
	rename("$messages/$URL{'m'}.mail","$messages/$starttime.log");
	unlink("$messages/$URL{'m'}.txt.temp");
	$lastposted = $user;
	$lasttime = $fdate;

	# Remove old thread from board ...
	fopen(FILE,"+<$boards/$mergebrd.msg") || error("$moderate[1]: $mergebrd.msg");
	@fdata = <FILE>;
	truncate(FILE,0);
	seek(FILE,0,0);
	foreach(@fdata) {
		if($_ =~ m/\A$getfrom\|/) { $wefnd = 1; next; }
		print FILE $_;
		($t,$tsub,$t,$t,$replies) = split(/\|/,$_);
		$totalreplys += $replies;
		++$mtotal;
	}
	fclose(FILE);
	$totalreplys += $mtotal;

	fopen(FILE,"+>$boards/$mergebrd.ino");
	print FILE "$mtotal\n$totalreplys\n";
	fclose(FILE);

	# Readd to index ...
	$mtotal = 0;
	$totalreplys = 0;
	fopen(FILE,"+<$boards/$URL{'b'}.msg") || error("$moderate[1]: $URL{'b'}.msg");
	while( $readdata = <FILE> ) {
		chomp $readdata;
		($mid,$sub,$poster,$pdate,$replies,$poll,$lock,$micon,$lposttime,$luserwhoposted) = split(/\|/,$readdata);
		if($mid eq $URL{'m'}) { $sub = $subject; $replies = $messagecnt-1; $lposttime = $lasttime; $luserwhoposted = $lastposted; $mid = $starttime; $poster = $threadstart; }
		push(@mergedata,"$lposttime|$mid|$sub|$poster|$pdate|$replies|$poll|$lock|$micon|$luserwhoposted");
	}
	truncate(FILE,0);
	seek(FILE,0,0);
	foreach(sort{$b <=> $a} @mergedata) {
		($lposttime,$mid,$sub,$poster,$pdate,$replies,$poll,$lock,$micon,$luserwhoposted) = split(/\|/,$_);
		$totalreplys += $replies;
		print FILE "$mid|$sub|$poster|$pdate|$replies|$poll|$lock|$micon|$lposttime|$luserwhoposted\n";
		++$mtotal;
	}
	fclose(FILE);
	$totalreplys += $mtotal;

	fopen(FILE,"+>$boards/$URL{'b'}.ino");
	print FILE "$mtotal\n$totalreplys\n";
	fclose(FILE);

	MLogging($starttime,5,$username,time,$tsub,$URL{'b'});

	redirect("$scripturl,v=display,m=$starttime");
}

sub ModLog {
	is_admin();

	$title = $moderate[73];
	header();

	$FORM{'searchby'} = $URL{'s'} if(!$FORM{'searchby'});
	$FORM{'value'} =~ s/\+/ /gsi;	
	$FORM{'value'} = $URL{'value'} if(!$FORM{'value'});

	$ebout .= <<"EOT";
<table cellpadding="5" cellspacing="1" class="border" width="950" align="center">
 <tr>
  <td class="titlebg" colspan="6"><b>$title</b></td>
 </tr>
   <tr>
    <td class="catbg" width="120"><b>$moderate[74]</b></td><td class="catbg" align="center" width="175"><b>$moderate[75]</b></td><td class="catbg" width="150"><b>$moderate[87]</b></td><td class="catbg" width="170"><b>$moderate[86]</b></td><td class="catbg" width="175"><b>$moderate[76]</b></td><td class="catbg" width="100" align="center"><b>$gtxt{'18'}</b></td>
   </tr>
EOT
	fopen(FILE,"$prefs/Moderator.log");
	@modlog = <FILE>;
	fclose(FILE);
	chomp @modlog;

	$nolog = 1;

	$counter = $counter2 = 0;
	$npp = 25;
	foreach(reverse @modlog) {
		($id,$usr,$tm,$ext,$ipaddy,$mid,$board,$huh) = split(/\|/,$_);

		if(!$sbuser{$usr}) { push(@usrlog,$usr); $sbuser{$usr} = 1; }
			else { ++$sbuser{$usr}; }

		if(!$sbtask{$id}) { push(@tasklogs,$id); $sbtask{$id} = 1; }
			else { ++$sbtask{$id}; }

		($tb,$ttitle) = split(/\|/,$thread{$mid});

		if($URL{'u'} ne '' && $usr ne $URL{'u'}) { next; }
		elsif($URL{'m'} ne '' && $mid ne $URL{'m'}) { next; }
		elsif($URL{'t'} ne '' && $id ne $URL{'t'}) { next; }
		elsif($FORM{'searchby'} == 1 && $mid ne $FORM{'value'}) { next; }
		elsif($FORM{'searchby'} == 2 && $ttitle !~ /\Q$FORM{'value'}\E/sig && $huh !~ /\Q$FORM{'value'}\E/sig) { next; }
		elsif($FORM{'searchby'} == 3 && $board ne $FORM{'value'}) { next; }
		elsif($FORM{'searchby'} == 4 && $ipaddy !~ /\Q$FORM{'value'}/i) { next; }

		++$totallinks;
		if(($totallinks > $URL{'l'} && $counter2 < $npp)) { ++$counter2; } else { next; }

		$nolog = 0;

		if($id == 1) {
			if($boards{$ext} eq '') { $boards = $gtxt{1}; }
				else { $boards = qq~<a href="$surl,b=$ext">$boards{$ext}</a>~; }
			$function = "$moderate[77]: $boards";
		}
		elsif($id == 2) { $function = $ext ? $moderate[78] : $moderate[79]; }
		elsif($id == 3) { $function = $ext ? $moderate[80] : $moderate[81]; }
		elsif($id == 4) { $function = "$moderate[82] $ext"; }
		elsif($id == 5) { $function = "$moderate[83] $ext"; }
		elsif($id == 6) { $function = "$moderate[99]: $ext"; }
		elsif($id == 7) { $function = "$moderate[100]: $ext"; }
		elsif($id == 8) {
			($number,$pus) = split(/\//,$ext);
			loaduser($pus);
			$pus = $userset{$pus}->[1] ? qq~<a href="$scripturl,v=memberpanel,a=view,u=$pus">$userset{$pus}->[1]</a>~ : $pus;
			$function = "$gtxt{'37'} $number, $moderate[101] $pus $moderate[102]";
		}
			else { $function = ''; }

		$datetime = get_date($tm);
		loaduser($usr);
		$usr = $userset{$usr}->[1] ? qq~<a href="$scripturl,v=memberpanel,a=view,u=$usr">$userset{$usr}->[1]</a>~ : $usr;

		if($tb) { $dew = qq~<a href="$surl,m=$mid,b=$tb">$ttitle</a>~; }
		elsif($huh) { $dew = $huh; } else { $dew = ''; }

		if($boards{$board} eq '') { $board = $gtxt{1}; }
			else { $board = qq~<a href="$surl,b=$board">$boards{$board}</a>~; }

		$ebout .= <<"EOT";
<tr>
 <td class="win"><span class="smalltext">$usr</span></td><td align="center" class="win2"><span class="smalltext">$datetime</span></td><td class="win"><span class="smalltext">$board</span></td><td class="win2"><span class="smalltext">$dew<br>ID: $mid</span></td><td class="win"><span class="smalltext">$function</span></td><td class="win2" align="center"><span class="smalltext">$ipaddy</span></td>
</tr>
EOT
	}
	if($nolog) { $ebout .= qq~<tr><td colspan="6" class="win" align="center"><br><b>$moderate[84]</b><br><br></tr>~; }
		else {
			$counter = 1;
			$start = $URL{'l'};
			$mresults = $totallinks || 1;
			if($mresults < $npp) { $start = 0; }
			$tstart = $start || 0;

			# How many page links?
			$smax = $totalpp*20;
			$startdot = ($totalpp*2)/5;

			# Create the main link ...
			$FORM{'value'} =~ s/ /\+/gsi;
			$link = "$surl,v=mod,a=modlog,m=$URL{'m'},u=$URL{'u'},t=$URL{'t'},s=$FORM{'searchby'},value=$FORM{'value'},l";

			if($tstart > $mresults) { $tstart = $mresults; }
			$tstart = (int($tstart/$npp)*$npp);
			if($tstart > 0) { $bk = ($tstart-$npp); $pagelinks = qq~<a href="$link=$bk">&#171;</a> ~; }
			if($mresults > ($smax/2) && $tstart > $npp*($startdot+1) && $mresults > $smax) {
				$sbk = $tstart-$smax; $sbk = 0 if($sbk < 0); $pagelinks .= qq~<a href="$link=$sbk">...</a> ~;
			}
			for($i = 0; $i < $mresults; $i += $npp) {
				if($i < $bk-($npp*$startdot) && $mresults > $smax) { ++$counter; $final = $counter-1; next; }
				if($start ne 'all' && $i == $tstart || $mresults < $npp) { $pagelinks .= qq~<b>$counter</b>, ~; $nxt = ($tstart+$npp); }
					else { $pagelinks .= qq~<a href="$link=$i">$counter</a>, ~; }
				++$counter;
				if($counter > $totalpp+$final && $mresults > $smax) { $gbk = $tstart+$smax; if($gbk > $mresults) { $gbk = (int($mresults/$npp)*$npp); } $pagelinks =~ s/, \Z//gsi; $pagelinks .= qq~ <a href="$link=$gbk">...</a> ~; ++$i; last; }
			}
			if($counter > 2) { $pgs = 's'; }
			$pagelinks =~ s/, \Z//gsi;
			if(($tstart+$npp) != $i && $start ne 'all') { $pagelinks .= qq~ <a href="$link=$nxt">&#187;</a>~; }

			$onpage = ($tstart+1).' - '.($tstart+$npp);

			$mresults = 0 if($search[0] eq '');

			$ebout .= <<"EOT";
<tr>
 <td colspan="6" class="catbg"><span class="smalltext"><b>$gtxt{'17'}:</b> $pagelinks</span></td>
</tr>
EOT
		}
	$ebout .= <<"EOT";
</table><br>
<table cellpadding="5" cellspacing="1" class="border" width="950" align="center">
 <tr>
  <td class="titlebg"><b>$moderate[88]</b></td>
 </tr><tr>
  <td class="win">
   <table cellpadding="2" cellspacing="0" width="100%">
    <tr>
     <td width="50%" valign="top">
      <table width="100%" cellpadding="5" cellspacing="1" class="border">
       <tr>
        <td class="catbg"><b>$moderate[89]</b></td>
       </tr><tr>
        <td class="win2"><span class="smalltext"><div class="win" style="padding: 5px;"><div style="float: left"><b>$moderate[90]</b></div><div style="float: right"><b>$moderate[91]</b></div><br></div>
EOT
	foreach(@usrlog) {
		loaduser($_);
		$usr = $userset{$_}->[1] ? $userset{$_}->[1] : $_;
		$ebout .= qq~<div style="padding: 5px;"><div style="float: left"><a href="$surl,v=mod,a=modlog,u=$_">$usr</a></div><div style="float: right">$sbuser{$_}</div><br></div>~;
	}
	$ebout .= <<"EOT";
        </span></td>
       </tr>
      </table>
     </td><td></td>
     <td width="50%" valign="top">
      <table width="100%" cellpadding="5" cellspacing="1" class="border">
       <tr>
        <td class="catbg"><b>$moderate[92]</b></td>
       </tr><tr>
        <td class="win2"><span class="smalltext"><div class="win" style="padding: 5px;"><div style="float: left"><b>$moderate[76]</b></div><div style="float: right"><b>$moderate[91]</b></div><br></div>
EOT
	foreach(@tasklogs) {
		$ebout .= qq~<div style="padding: 5px;"><div style="float: left"><a href="$surl,v=mod,a=modlog,t=$_">$tasklog[$_-1]</a></div><div style="float: right">$sbtask{$_}</div><br></div>~;
	}
	$SEL{$FORM{'searchby'}} = ' selected';

	$ebout .= <<"EOT";
        </span></td>
       </tr>
      </table>
     </td>
    </tr><tr><form action="$surl,v=mod,a=modlog" method="post" name="lsearch">
     <td colspan="3">
      <table width="100%" cellpadding="5" cellspacing="1" class="border">
       <tr><td class="win2"><span class="smalltext"><b>$moderate[93] &nbsp; <select name="searchby" style="vertical-align: middle"><option value="1"$SEL{1}>$moderate[94]</option><option value="2"$SEL{2}>$moderate[95]</option><option value="3"$SEL{3}>$moderate[96]</option><option value="4"$SEL{4}>$moderate[97]</option></select> : &nbsp; </b><input type="text" class="textinput" name="value" value="$FORM{'value'}" style="vertical-align: middle"> &nbsp; <input type="submit" value="$moderate[98]" class="button" style="vertical-align: middle"></span></td></tr>
      </table>
     </td>
    </form></tr>
   </table>
  </td>
 </tr>
</table>
EOT

	footer();
	exit;
}

sub MLogging {
	my($mymid,$myid,$myusr,$mytm,$ext,$boardid,$previous) = @_;

	fopen(FILE,">>$prefs/Moderator.log");
	print FILE "$myid|$myusr|$mytm|$ext|$ENV{'REMOTE_ADDR'}|$mymid|$boardid|$previous\n";
	fclose(FILE);
}
1;