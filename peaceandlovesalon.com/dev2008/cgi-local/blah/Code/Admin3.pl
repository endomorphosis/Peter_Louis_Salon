################################################
# E-Blah Bulliten Board Systems       Platinum #
################################################
# Copyright (c) 2001 - 2004 e-blah!            #
# All Rights Reserved.                         #
################################################

&is_admin;
CoreLoad('Members',1);

sub MemGrps {
	if($URL{'p'} eq 'save') {
		if($FORM{'adminstar'} ne 'OTHER') { $adminstar = $FORM{'adminstar'}; }
			else { $adminstar = $FORM{'adminotherstar'}; }
		if($FORM{'modstar'} ne 'OTHER') { $modstar = $FORM{'modstar'}; }
			else { $modstar = $FORM{'modotherstar'}; }
		if($adminstar eq '') { $adminstar = 'nopic.gif'; }
		if($modstar eq '') { $modstar = 'nopic.gif'; }
		for($i = 0; $i < @memgrps; $i++) {
			if($i > 6) { $add .= "$memgrps[$i]|$memgrpp[$i]\n"; }
				else {
					$name = Format($FORM{$i});
					if($i == 0) { $add .= "$name|$FORM{'color'},$adminstar,$FORM{'admincount'}\n"; }
					elsif($i > 0 && $i < 6) { $add .= "$name|,,,,,".$FORM{$i."star"}.",,,,,".$FORM{"c_$i"}.",".$FORM{$i."count"}."\n"; ++$nu; }
					elsif($i == 6) { $add .= "$name|$FORM{'team'},$modstar,$FORM{'modcount'}\n"; }
						else { $add .= "$name\n"; }
				}
		}
		fopen(FILE,"+>$prefs/Ranks.txt");
		print FILE $add;
		fclose(FILE);
		redirect("$scripturl,v=admin,a=memgrps");
	}

	if($URL{'p'} eq 'edit2' || $URL{'p'} eq 'edit') { &EditGrps; }

	for($q = 0; $q < @memgrps; $q++) {
		($t,$t,$t,$t,$t,$star,$team,$color,$t,$t,$count,$starcount) = split(',',$memgrpp[$q]);
		if($q == 0) {
			($admincolor,$star,$admincount) = split(',',$memgrpp[0]);
			if($star ne 'stype1.gif' && $star ne '' && $star ne 'stype2.gif' && $star ne 'stype3.gif' && $star ne 'stype4.gif' && $star ne 'stype5.gif' && $star ne 'nopic.gif') { $oastar = $star; $star = 'OTHER'; }
			elsif($star eq '') { $star = 'stype2.gif'; }
			$adminstar{$star} = " selected";
			if($admincount eq '') { $admincount = '5'; }
		}
		if($q == 6) {
			($modteam,$star,$modcount) = split(',',$memgrpp[6]);
			if($star ne 'stype1.gif' && $star ne '' && $star ne 'stype2.gif' && $star ne 'stype3.gif' && $star ne 'stype4.gif' && $star ne 'stype5.gif' && $star ne 'nopic.gif') { $omstar = $star; $star = 'OTHER'; }
			elsif($star eq '') { $star = 'stype3.gif'; }
			$modstar{$star} = " selected";
			if($modcount eq '') { $modcount = '5'; }
			if($modteam) { $modteam = " checked"; } 
		}
		if($q > 0 && $q < 6) { $sc{$q} = $starcount; }
		if($count ne '') { $grpcnt{"$memgrps[$q]"} = $count; }
	}

	$maxcount  = $grpcnt{"$memgrps[1]"} || $maxcount;
	$maxstar   = $star{"$memgrps[1]"} || '5star.gif';
	$bigcount  = $grpcnt{"$memgrps[2]"} || $bigcount;
	$bigstar   = $star{"$memgrps[2]"} || '4star.gif';
	$medcount  = $grpcnt{"$memgrps[3]"} || $medcount;
	$medstar   = $star{"$memgrps[3]"} || '3star.gif';
	$mincount  = $grpcnt{"$memgrps[4]"} || $mincount;
	$minstar   = $star{"$memgrps[4]"} || '2star.gif';
	$babycount = $grpcnt{"$memgrps[5]"};
	$babystar  = $star{"$memgrps[5]"} || '1star.gif';

	$title = $memtext[160];
	&headerA;
	$ebout .= <<"EOT";
<script language="JavaScript">
<!--
function cin(incom) {
 previewvar = '';
 if(document.form.elements(incom+"count").value > 50) { document.form.elements(incom+"count").value = "50"; }
 for(i = 1; i <= document.form.elements(incom+"count").value; i++) {
	// previewvar = "<img src=$images/"+document.form.elements(incom+"star").value+">"+previewvar;

  if(document.form.elements(incom+"star").value != 'OTHER') { previewvar = "<img src=$images/"+document.form.elements(incom+"star").value+">"+previewvar; }
   else if(document.form.elements(incom+"otherstar").value != '') { previewvar = "<img src=$images/"+document.form.elements(incom+"otherstar").value+">"+previewvar; }
 }
 trash = String(incom+"junks");
 document.getElementById(trash).innerHTML = previewvar;
}

function ColorPreview() {
 if(document.form.color.value == '') { valy = ''; } else { valy = document.form.color.value; }
 document.getElementById('colorpreview').innerHTML = '<font color="'+valy+'">$memtext[180]</span>';
}
// -->
</script>
<body onLoad="cin('admin');cin('mod');cin('1');cin('2');cin('3');cin('4');cin('5');ColorPreview();"></body>
<table cellpadding="4" cellspacing="1" class="border" width="600" align="center">
 <tr><form action="$scripturl,v=admin,a=memgrps,p=save" method="post" name="form">
  <td class="titlebg"><img src="$images/profile_sm.gif"> $title</td>
 </tr><tr>
  <td class="catbg"><span class="smalltext"><b>$memtext[27]</b></span></td>
 </tr><tr>
  <td class="win">
   <table width="100%">
    <tr>
     <td width="200"><b>$memtext[173]:</b></td>
     <td><input type="text" class="textinput" name="0" value="$memgrps[0]" size="25"></td>
    </tr><tr>
     <td width="200"><b>$memtext[28]:</b></td>
     <td><input type="text" class="textinput" name="color" value='$admincolor' size="15" onChange="ColorPreview()"> &nbsp; &nbsp; <span id="colorpreview" class="win2" style="padding:5px"></span></td>
    </tr><tr>
     <td width="200" valign="top"><b>$memtext[174]:</b></td>
     <td><select name="adminstar" onChange="cin('admin')">
      <option value="stype1.gif"$adminstar{'stype1.gif'}>$memtext[175] 1</option>
      <option value="stype2.gif"$adminstar{'stype2.gif'}>$memtext[175] 2</option>
      <option value="stype3.gif"$adminstar{'stype3.gif'}>$memtext[175] 3</option>
      <option value="stype4.gif"$adminstar{'stype4.gif'}>$memtext[175] 4</option>
      <option value="stype5.gif"$adminstar{'stype5.gif'}>$memtext[175] 5</option>
      <option value="OTHER"$adminstar{'OTHER'}>$memtext[176]</option>
      <option value="nopic.gif"$adminstar{'nopic.gif'}>$memtext[177]</option>
     </select> x <input type="text" class="textinput" size="3" name="admincount" value="$admincount" onChange="cin('admin')"> &nbsp; &nbsp; <span id="adminjunks"></span><br><span class="smalltext"><b>$memtext[176]:</b> images / <input type="text" class="textinput" value="$oastar" name="adminotherstar" onChange="cin('admin')"></span></td>
    </tr>
   </table>
  </td>
 </tr><tr>
  <td class="catbg"><span class="smalltext"><b>$memtext[29]</b></span></td>
 </tr><tr>
  <td class="win">
   <table width="100%">
    <tr>
     <td width="200"><b>$memtext[173]:</b></td>
     <td><input type="text" class="textinput" name="6" value="$memgrps[6]" size="25"></td>
    </tr><tr>
     <td width="200"><b><img src="$images/team.gif"> $memtext[30]</b></td>
     <td><input type="checkbox" class="checkboxinput" name="team" value="1"$modteam></td>
    </tr><tr>
     <td width="200" valign="top"><b>$memtext[174]:</b></td>
     <td><select name="modstar" onChange="cin('mod')">
      <option value="stype1.gif"$modstar{'stype1.gif'}>$memtext[175] 1</option>
      <option value="stype2.gif"$modstar{'stype2.gif'}>$memtext[175] 2</option>
      <option value="stype3.gif"$modstar{'stype3.gif'}>$memtext[175] 3</option>
      <option value="stype4.gif"$modstar{'stype4.gif'}>$memtext[175] 4</option>
      <option value="stype5.gif"$modstar{'stype5.gif'}>$memtext[175] 5</option>
      <option value="OTHER"$modstar{'OTHER'}>$memtext[176]</option>
      <option value="nopic.gif"$modstar{'nopic.gif'}>$memtext[177]</option>
     </select> x <input type="text" class="textinput" size="3" name="modcount" value="$modcount" onChange="cin('mod')"> &nbsp; &nbsp; <span id="modjunks"></span><br><span class="smalltext"><b>$memtext[176]:</b> images / <input type="text" class="textinput" value="$omstar" name="modotherstar" onChange="cin('mod')"></span></td>
    </tr>
   </table>
  </td>
 </tr><tr>
  <td class="win2" align="center"><input type="submit" class="button" value=" $memtext[59] " name="submit"></td>
 </tr>
</table>
<br>
<table cellpadding="4" cellspacing="1" class="border" width="600" align="center">
 <tr>
  <td class="titlebg">$memtext[178]</td>
 </tr><tr>
  <td class="win">
   <table width="100%">
    <tr>
     <td width="200" valign="top"><b>$var{'79'}:</b></td>
     <td><span class="smalltext"><input type="text" class="textinput" name="5" value="$memgrps[5]" size="25"> $memtext[168] <input type="text" class="textinput" name="c_5" size="5" value="$babycount"> $gtxt{10}<br><b>Star:</b> images / <input type="text" class="textinput" name="5star" size="10" value="$babystar" onChange="cin('5')"> x <input type="text" class="textinput" name="5count" size="3" value="$sc{5}" onChange="cin('5')"><br><br></span></td>
     <td><span id="5junks"></span></td>
    </tr><tr>
     <td width="200" valign="top"><b>$var{'78'}:</b></td>
     <td><span class="smalltext"><input type="text" class="textinput" name="4" value="$memgrps[4]" size="25"> $memtext[168] <input type="text" class="textinput" name="c_4" size="5" value="$mincount"> $gtxt{10}<br><b>Star:</b> images / <input type="text" class="textinput" name="4star" size="10" value="$minstar" onChange="cin('4')"> x <input type="text" class="textinput" name="4count" size="3" value="$sc{4}" onChange="cin('4')"><br><br></span></td>
     <td><span id="4junks"></span></td>
    </tr><tr>
     <td width="200" valign="top"><b>$var{'77'}:</b></td>
     <td><span class="smalltext"><input type="text" class="textinput" name="3" value="$memgrps[3]" size="25"> $memtext[168] <input type="text" class="textinput" name="c_3" size="5" value="$medcount"> $gtxt{10}<br><b>Star:</b> images / <input type="text" class="textinput" name="3star" size="10" value="$medstar" onChange="cin('3')"> x <input type="text" class="textinput" name="3count" size="3" value="$sc{3}" onChange="cin('3')"><br><br></span></td>
     <td><span id="3junks"></span></td>
    </tr><tr>
     <td width="200" valign="top"><b>$var{'76'}:</b></td>
     <td><span class="smalltext"><input type="text" class="textinput" name="2" value="$memgrps[2]" size="25"> $memtext[168] <input type="text" class="textinput" name="c_2" size="5" value="$bigcount"> $gtxt{10}<br><b>Star:</b> images / <input type="text" class="textinput" name="2star" size="10" value="$bigstar" onChange="cin('2')"> x <input type="text" class="textinput" name="2count" size="3" value="$sc{2}" onChange="cin('2')"><br><br></span></td>
     <td><span id="2junks"></span></td>
    </tr><tr>
     <td width="200" valign="top"><b>$var{'75'}:</b></td>
     <td><span class="smalltext"><input type="text" class="textinput" name="1" value="$memgrps[1]" size="25"> $memtext[168] <input type="text" class="textinput" name="c_1" size="5" value="$maxcount"> $gtxt{10}<br><b>Star:</b> images / <input type="text" class="textinput" name="1star" size="10" value="$maxstar" onChange="cin('1')"> x <input type="text" class="textinput" name="1count" size="3" value="$sc{1}" onChange="cin('1')"></span></td>
     <td><span id="1junks"></span></td>
    </tr>
   </table>
  </td>
 </tr><tr>
  <td class="win2" align="center"><input type="submit" class="button" value=" $memtext[59] " name="submit"></td>
 </form></tr>
</table>
<br>
<table cellpadding="4" cellspacing="1" class="border" width="600" align="center">
 <tr>
  <td class="titlebg">$memtext[32] <span class="smalltext">(<b><a href="$scripturl,v=admin,a=memgrps,p=edit,e=new">$memtext[34]</a></b>)</span></td>
 </tr><tr>
  <td class="win">
EOT
	$counter = 1;
	for($i = 7; $i < @memgrps; $i++) {
		if($team{$memgrps[$i]}) { $team = qq~<img src="$images/team.gif"> ~; } else { $team = ''; }
		$customgrps .= qq~<b>$counter.</b> $team<a href="$scripturl,v=admin,a=memgrps,p=edit,e=$i">$memgrps[$i]</a><br>~;
		++$counter;
	}
	if($customgrps eq '') { $customgrps = 'Currently None'; }

	$ebout .= <<"EOT";
  $customgrps</td>
 </tr>
</table>
EOT
	&footerA;
	exit;
}

sub EditGrps {
	if($URL{'e'} ne 'new') {
		($ip,$mod,$sticky,$modify,$profile,$star,$team,$color,$calendar,$adminon,$count,$starcount) = split(',',$memgrpp[$URL{'e'}]);
		$pref = $memgrps[$URL{'e'}];
		if($star ne 'stype1.gif' && $star ne '' && $star ne 'stype2.gif' && $star ne 'stype3.gif' && $star ne 'stype4.gif' && $star ne 'stype5.gif' && $star ne 'nopic.gif') { $ostar = $star; $star = 'OTHER'; }
		$star{$star} = " selected";
		if($starcount ne '') { $maincount = $starcount; } else { $maincount = 5; }
		if($URL{'e'} < 7) { &error($memtext[35]); }
		if((@memgrps-1) < $URL{'e'}) { &error($memtext[36]); }
	}
	if($URL{'p'} eq 'edit2') { &EditGrps2; }
	if($URL{'e'} eq 'new') {
		$mgn = qq~<input type="text" class="textinput" name="name" value="$pref" size="30">~;
		$star{''} = " selected";
		$maincount = 1;
		$count = '';
	} else { $mgn = $pref; }

	$IP{$ip} = " checked";
	$MOD{$mod} = " checked";
	$ST{$sticky} = " checked";
	$M{$modify} = " checked";
	$PRO{$profile} = " checked";
	$CAL{$calendar} = " checked";
	$T{$team} = " checked";
	$ADMIN{$adminon} = " checked";
	if($URL{'e'} ne 'new') { $delete = qq~ <input type="submit" class="button" value=" $memtext[42] " name="delete">~; }

	$title = $memtext[37];
	&headerA;

	$ebout .= <<"EOT";
<script language="JavaScript">
<!--
function cin() {
 previewvar = '';
 if(document.form.maincount.value > 25) { document.form.maincount.value = "25"; }
 for(i = 1; i <= document.form.maincount.value; i++) {
  if(document.form.mainstar.value != 'OTHER' && document.form.mainstar.value != '') { previewvar = "<img src=$images/"+document.form.mainstar.value+">"+previewvar; }
   else if(document.form.otherstar.value != '') { previewvar = "<img src=$images/"+document.form.otherstar.value+">"+previewvar; }
 }
 document.getElementById('mainjunks').innerHTML = previewvar;
}

function ColorPreview() {
 if(document.form.color.value == '') { valy = ''; } else { valy = document.form.color.value; }
 document.getElementById('colorpreview').innerHTML = '<font color="'+valy+'">$memtext[180]</span>';
}

function PostsOnly() {
 if(document.getElementById) { openItem = document.getElementById('postsdisabled'); openItem2 = document.getElementById('postsdisabled2'); }
 else if (document.all){ openItem = document.all['postsdisabled']; openItem2 = document.all['postsdisabled2']; }
 else if (document.layers){ openItem = document.layers['postsdisabled']; openItem2 = document.layers['postsdisabled2']; }

 if(document.form.posts.value == '') { ShowType = ""; }
  else { ShowType = "none"; }
 if(openItem.style) { openItem.style.display = ShowType; openItem2.style.display = ShowType; }
  else { openItem.visibility = "show"; openItem2.visibility = "show"; }
}
// -->
</script>
<body onLoad="ColorPreview();cin();PostsOnly();"></body>
<table cellpadding="4" cellspacing="1" class="border" width="600" align="center">
 <tr><form action="$scripturl,v=admin,a=memgrps,p=edit2,e=$URL{'e'}" method="post" name="form" enctype="multipart/form-data">
  <td class="titlebg">$title</td>
 </tr><tr>
  <td class="win"><span class="smalltext">$memtext[38]</span></td>
 </tr><tr>
  <td class="catbg"><span class="smalltext"><b>$memtext[39]</b></span></td>
 </tr><tr>
  <td class="win2">
   <table width="100%" cellpadding="4" cellspacing="0">
    <tr>
     <td width="200"><b>$memtext[173]:</b></td>
     <td>$mgn</td>
    </tr><tr>
     <td width="200"><b>$memtext[179]</td>
     <td valign="top"><input type="text" class="textinput" name="posts" value="$count" size="3" onChange="PostsOnly()"> $memtext[181]</td>
    </tr><tr id="postsdisabled">
     <td colspan="3" class="win">
      <table width="100%">
       <tr>
        <td width="200"><img src="$images/team.gif"> <b>$memtext[57]</b></td>
        <td colspan="2"><input type="checkbox" class="checkboxinput" value="1" name="team"$T{'1'}></td>
       </tr><tr>
        <td width="200"><b>$memtext[28]:</b></td>
        <td><input type="text" class="textinput" value='$color' name="color" size="15" onChange="ColorPreview()"></td>
        <td align="center"><span id="colorpreview" class="win2" style="padding:5px"></span></td>
       </tr>
      </table>
     </td>
    </tr><tr>
     <td width="200" valign="top"><b>$memtext[174]:</b></td>
     <td><select name="mainstar" onChange="cin()">
      <option value="stype1.gif"$star{'stype1.gif'}>$memtext[175] 1</option>
      <option value="stype2.gif"$star{'stype2.gif'}>$memtext[175] 2</option>
      <option value="stype3.gif"$star{'stype3.gif'}>$memtext[175] 3</option>
      <option value="stype4.gif"$star{'stype4.gif'}>$memtext[175] 4</option>
      <option value="stype5.gif"$star{'stype5.gif'}>$memtext[175] 5</option>
      <option value="OTHER"$star{'OTHER'}>$memtext[176]</option>
      <option value="nopic.gif"$star{'nopic.gif'}>$memtext[177]</option>
      <option value=""$star{''}>$memtext[182]</option>
     </select> x <input type="text" class="textinput" size="3" name="maincount" value="$maincount" onChange="cin()"><br><span class="smalltext"><b>$memtext[176]:</b> images / <input type="text" class="textinput" value="$ostar" name="otherstar" onChange="cin()"></span></td>
     <td><span id="mainjunks"></span></td>
    </tr>
   </table>
  </td>
 </tr>
</table>
<br>
<table cellpadding="4" cellspacing="1" class="border" width="600" align="center">
 <tr>
  <td class="win" align="center"><input type="submit" class="button" value=" $memtext[59] " name="submit"> <input type="reset" class="button" value=" $memtext[60] " name="reset">$delete</td>
 </tr>
</table>
<br>
<table cellpadding="4" cellspacing="1" class="border" width="600" align="center" id="postsdisabled2">
 <tr>
  <td class="titlebg">$memtext[43]</td>
 </tr><tr>
  <td class="win"><span class="smalltext">$memtext[183]</span></td>
 </tr><tr>
  <td class="win2">
   <table width="100%" cellpadding="4" cellspacing="0">
    <tr>
     <td colspan="2" class="win"><span class="smalltext"><b>$memtext[184]</b></span></td>
    </tr><tr>
     <td><span class="smalltext"><b>$memtext[148]</span><br><br></td>
     <td valign="top" align="center"><input type="checkbox" class="checkboxinput" name="admin" value="1"$ADMIN{1}><br><br></td>
    </tr><tr>
     <td colspan="2" class="win"><span class="smalltext"><b>$memtext[185]</b></span></td>
    </tr><tr>
     <td><span class="smalltext"><b>$memtext[47]</b></span></td>
     <td align="center"><input type="checkbox" class="checkboxinput" name="ip" value="1"$IP{1}></td>
    </tr><tr>
     <td><span class="smalltext">$memtext[48]</span></td>
     <td align="center"><input type="checkbox" class="checkboxinput" name="mod" value="1"$MOD{1}></td>
    </tr><tr>
     <td><span class="smalltext"><b>$memtext[49]</b></span></td>
     <td align="center"><input type="checkbox" class="checkboxinput" name="st" value="1"$ST{1}></td>
    </tr><tr>
     <td><span class="smalltext"><b>$memtext[50]</b></span></td>
     <td align="center"><input type="checkbox" class="checkboxinput" name="m" value="1"$M{1}></td>
    </tr><tr>
     <td><span class="smalltext"><b>$memtext[51]</b></span></td>
     <td align="center"><input type="checkbox" class="checkboxinput" name="pro" value="1"$PRO{1}></td>
    </tr><tr>
     <td><span class="smalltext"><b>$memtext[147]</span><br><br></td>
     <td align="center"><input type="checkbox" class="checkboxinput" name="cal" value="1"$CAL{1}><br><br></td>
    </tr>
   </table>
  </td>
 </tr>
</table></form>
EOT
	&footerA;
	exit;
}

sub EditGrps2 {
	if($URL{'e'} eq 'new' && $FORM{'delete'} eq " $memtext[42] ") { &error($memtext[62]); }

	# Star Config
	if($FORM{'mainstar'} eq 'OTHER') { $star = $FORM{'otherstar'}; }
		else { $star = $FORM{'mainstar'}; }
	if($star =~ s/,/\&#44;/gsi) { &error($gtxt{'bfield'}); }
	if($FORM{'maincount'} > 25 || $FORM{'maincount'} < 0) { $FORM{'maincount'} = 5; }

	# Various
	$team = $FORM{'team'} || 0;


	fopen(FILE,"$prefs/Ranks.txt");
	@ranks = <FILE>;
	fclose(FILE);
	chomp @ranks;
	$name = Format($FORM{'name'});
	if($name eq 'member') { &error($gtxt{'bfield'}); }
	$color = Format($FORM{'color'});
	$color =~ s/,/\&#44;/gsi;
	if($URL{'e'} eq 'new') { &error($gtxt{'bfield'}) if($name eq ''); }
	for($i = 0; $i < @ranks; $i++) {
		if(($URL{'e'} ne 'new') && ($FORM{'delete'} ne " $memtext[42] ") && ($URL{'e'} == $i)) { $add .= qq~$memgrps[$i]|$FORM{'ip'},$FORM{'mod'},$FORM{'st'},$FORM{'m'},$FORM{'pro'},$star,$team,$color,$FORM{'cal'},$FORM{'admin'},$FORM{'posts'},$FORM{'maincount'}\n~; }
		elsif($URL{'e'} ne $i) { $add .= "$ranks[$i]\n"; }
	}
	if($URL{'e'} eq 'new') { $add .= qq~$name|$FORM{'ip'},$FORM{'mod'},$FORM{'st'},$FORM{'m'},$FORM{'pro'},$star,$team,$color,$FORM{'cal'},$FORM{'admin'},$FORM{'posts'},$FORM{'maincount'}\n~; }

	fopen(FILE,"+>$prefs/Ranks.txt");
	print FILE $add;
	fclose(FILE);
	redirect("$scripturl,v=admin,a=memgrps");
}

sub Mailing {
	if($URL{'p'} == 2) { &Mailing2; }
	fopen(FILE,"$members/List.txt");
	@mlist = <FILE>;
	fclose(FILE);
	chomp @mlist;
	foreach(@mlist) {
		loaduser($_);
		if($userset{$_}->[2] && (!$userset{$_}->[25] || $URL{'s'})) { $emails .= "$userset{$_}->[2];"; }
	}
	$emails =~ s/;\Z//;

	$title = $memtext[64];
	&headerA;
	$ebout .= <<"EOT";
<SCRIPT LANGUAGE="JavaScript">
<!-- Start
function Submit() {
 document.msend.submit.disabled = true;
}
// End -->
</script>
<table cellpadding="4" cellspacing="1" class="border" width="98%" align="center">
 <tr><form action="$scripturl,v=admin,a=mailing,p=2" method="post" name="msend" onSubmit="Submit()">
  <td class="titlebg"><b>$title</b></td>
 </tr><tr>
  <td class="win"><span class="smalltext">$memtext[65]</span></td>
 </tr><tr>
  <td class="win2">
   <table cellpadding="4" cellspacing="0" width="100%">
    <tr>
     <td valign="top" align="right" width="35%"><b>$memtext[66]:</b><br><span class="smalltext">$memtext[67]</span></td>
     <td width="65%"><textarea name="emails" wrap="virtual" rows="8" cols="60">$emails</textarea></td>
    </tr><tr>
     <td valign="top" align="right" width="35%"><b>$memtext[68]:</b></td>
     <td width="65%"><input type="text" class="textinput" name="subject" maxlength="50" size="35"></td>
    </tr>
   </table>
  </td>
 </tr><tr>
  <td class="win">
   <table cellpadding="4" cellspacing="0" width="100%">
    <tr><script language="JavaScript" src="$bcloc" type="text/javascript"></script>
     <td align="right" width="35%" valign="top"><b>$memtext[69]:</b></td>
     <td width="65%"><a href="javascript:use('[face=Verdana]','[/face]');"><img src="$images/face.gif" border="0" alt="$var{'8'}"></a>
      <a href="javascript:use('[size=2]','[/size]');"><img src="$images/size.gif" border="0" alt="$var{'9'}"></a>
      <a href="javascript:use('[b]','[/b]');"><img src="$images/bold.gif" border="0" alt="$var{'10'}"></a>
      <a href="javascript:use('[i]','[/i]');"><img src="$images/italics.gif" border="0" alt="$var{'11'}"></a>
      <a href="javascript:use('[u]','[/u]');"><img src="$images/underline.gif" border="0" alt="$var{'12'}"></a>
      <a href="javascript:use('[left]','[/left]');"><img src="$images/left.gif" border="0" alt="$var{'13'}"></a>
      <a href="javascript:use('[center]','[/center]');"><img src="$images/center.gif" border="0" alt="$var{'14'}"></a>
      <a href="javascript:use('[right]','[/right]');"><img src="$images/right.gif" border="0" alt="$var{'15'}t"></a>
      <a href="javascript:use('[pre]','[/pre]');"><img src="$images/pre.gif" border="0" alt="$var{'16'}"></a>
      <a href="javascript:use('[s]','[/s]');"><img src="$images/strike.gif" border="0" alt="$var{'17'}"></a>
      <a href="javascript:use('[url]','[/url]');"><img src="$images/url.gif" border="0" alt="$var{'21'}"></a>
      <a href="javascript:use('[mail]','[/mail]');"><img src="$images/email_click.gif" border="0" alt="$var{'22'}"></a>
      <a href="javascript:use('[img]','[/img]');"><img src="$images/img.gif" border="0" alt="$var{'23'}"></a>
      <a href="javascript:use('[hr]');"><img src="$images/hr.gif" border="0" alt="$var{'25'}"></a>
      <a href="javascript:use('[list]\\n[*]','\\n[/list]');"><img src="$images/list.gif" border="0" alt="$var{'18'}"></a>
     </td>
    </tr>
   </table>
  </td>
 </tr><tr>
  <td class="win2">
   <table cellpadding="4" cellspacing="0" width="100%">
    <tr>
     <td valign="top" align="right" width="35%"><b>$memtext[70]:</b><br><span class="smalltext">$memtext[71]</span></td>
     <td width="65%"><textarea name="message" wrap="virtual" rows="10" cols="90"></textarea></td>
    </tr>
   </table>
  </td>
 </tr><tr>
  <td class="win" align="center"><input type="submit" class="button" name="submit" value=" $memtext[72] "></td>
 </span></tr>
</table>
EOT

	&footerA;
	exit;
}

sub Mailing2 {
	@emails = split(/,|;/,$FORM{'emails'});
	$subject = $FORM{'subject'};
	if($subject eq '') { &error($gtxt{'bfield'}); }
	$message = Format($FORM{'message'});
	error($gtxt{'long'}) if(length($message) > $maxmesslth && $maxmesslth);

	if($message eq '') { &error($gtxt{'bfield'}); }
	if($al) {
		$message =~ s~([^\w\"\=\[\]]|[\n\b]|\A)\\*(\w+://[\w\~\.\;\:\$\-\+\!\*\?/\=\&\@\#\%]+\.[\w\~\;\:\$\-\+\!\*\?/\=\&\@\#\%,.]+[\w\~\;\:\$\-\+\!\*\?/\=\&\@\#\%])~$1<a href="$2" target="_blank">$2</a>~isg;
		$message =~ s~([^\"\=\[\]/\:\.(\://\w+)]|[\n\b]|\A)\\*(www\.[^\.][\w\~\.\;\:\$\-\+\!\*\?/\=\&\@\#\%]+\.[\w\~\;\:\$\-\+\!\*\?/\=\&\@\#\%\,]+[\w\~\;\:\$\-\+\!\*\?/\=\&\@\#\%])~$1<a href="http://$2" target="_blank">$2</a>~isg;
	}
	$message =~ s~\[b\](.*?)\[/b\]~<b>$1</b>~gsi;
	$message =~ s~\[i\](.*?)\[/i\]~<i>$1</i>~gsi;
	$message =~ s~\[u\](.*?)\[/u\]~<u>$1</u>~gsi;
	$message =~ s~\[s\](.*?)\[/s\]~<s>$1</s>~gsi;
	$message =~ s~\[size=(.*?)\](.*?)\[/size\]~<font size="$1">$2</span>~gsi;
	$message =~ s~\[img\]http://(.*?)\[/img\]~<img src="http://$1" border="0">~gsi;
	$message =~ s~\[url=http://(.*?)\](.*?)\[/url\]~<a href="http://$1">$2</a>~gsi;
	$message =~ s~\[url\]http://(.*?)\[/url\]~<a href="http://$1">$1</a>~gsi;
	$message =~ s~\[left\](.+?)\[/left\]~<div align="left">$1</div>~gsi;
	$message =~ s~\[right\](.+?)\[/right\]~<div align="right">$1</div>~gsi;
	$message =~ s~\[center\](.+?)\[/center\]~<center>$1</center>~gsi;
	$message =~ s~\[pre\](.+?)\[/pre\]~<pre>$1</pre>~gsi;

	$message =~ s~\[face=(.+?)\](.+?)\[/face\]~<font face="$1">$2</span>~gsi;
	$message =~ s~\[color=(.+?)\](.+?)\[/color\]~<font color="$1">$2</span>~gsi;
	$message =~ s~\[hr\]~<hr size="1" width="100%" color="$color{'border'}">~gsi;
	$message =~ s~\[mail\](.+?)\[/mail\]~<a href="mailto:$1">$1</a>~gsi;
	$message =~ s~\[mail=(.+?)\](.+?)\[/mail\]~<a href="mailto:$1">$2</a>~gsi;
	if($message =~ /\[list\]/) {
		$message =~ s~\[list\](.+?)\[/list\]~<ul>$1</ul>~gsi;
		$message =~ s~\[\*\]~<li>~gsi;
	}
	foreach $sendto (@emails) { smail($sendto,$subject,$message,$settings[1]); next; }
	redirect("$scripturl,v=admin,r=3");
}

sub Validate {
	if($URL{'p'} == 2) { &Validate2; }
	fopen(FILE,"$members/List.txt");
	@list = <FILE>;
	fclose(FILE);
	chomp @list;
	foreach(@list) {
		loaduser($_);
		if($userset{$_}->[23]) { $userset{$_}->[23] =~ s/\|/&#124;/gsi; push(@members,"$userset{$_}->[23]|$_"); ++$type{$userset{$_}->[23]}; }
	}
	@members = sort{$a cmp $b} @members;

	$title = $memtext[73];
	&headerA;
	$ebout .= <<"EOT";
<script language="javascript">
<!-- //
function deleteme(user) {
 if(window.confirm("$memtext[161]")) { location = "$scripturl,v=memberpanel,a=remove,u="+user; }
}
// -->
</script>
<table cellpadding="4" cellspacing="1" class="border" align="center" width="600">
 <tr>
  <td class="titlebg"><b><img src="$images/profile_sm.gif"> $title</b></td>
 </tr><tr>
  <td class="win"><span class="smalltext">$memtext[155]</span></td>
 </tr><tr>
  <td class="win2"><table cellpadding="5" cellspacing="0" width="98%" align="center">
EOT
	foreach(sort{$b <=> $a} @members) {
		($atype,$uname) = split(/\|/,$_);
		if(!$noshow{$atype}) {
			if($counter == 1) { $ebout .= "<td>&nbsp;</td></tr><tr>"; $counter = 0; }
			if($counter == 2) { $ebout .= "</tr><tr>\n"; $counter = 0; }
			if($atype eq 'ADMIN') { $type = $memtext[149]; }
			elsif($atype eq 'EMAIL&#124;ADMIN') { $type = $memtext[150]; }
			elsif($atype eq 'EMAIL') { $type = $memtext[151]; }
				else { $type = $memtext[152]; }
			$ebout .= <<"EOT";
$next<td colspan="2" class="win"><span class="smalltext"><b>$type</b> ($type{$atype})</span></td></tr><tr>
EOT
			++$add;
			$noshow{$atype} = 1;
		}
		$datereg = get_date($userset{$uname}->[14]);
		$ebout .= <<"EOT";
$next<td width="50%"><b><a href="$scripturl,v=memberpanel,a=view,u=$uname">$userset{$uname}->[1]</a></b> <a href="mailto:$userset{$uname}->[2]"><img src="$images/email_sm.gif" border="0"></a><br><span class="smalltext"><b>$memtext[153]:</b> $datereg<br>&nbsp;&#149; <a href="$scripturl,v=admin,a=validate,p=2,u=$uname">$memtext[83]</a><br>&nbsp;&#149; <a href="$scripturl,v=memberpanel,a=view,u=$uname" target="_blank">$memtext[154]</a><br>&nbsp;&#149; <a href="javascript:deleteme('$uname');">$memtext[162]</a></span><br><br></td>
EOT
		++$counter;
		if($counter == 2) { $next = "</tr><tr>\n"; $counter = 0; } else { $next = ''; }
	}
	if($counter == 1) { $ebout .= "<td>&nbsp;</td></tr>"; }
	if(!$add) {
		$ebout .= <<"EOT";
<td colspan="2">$memtext[84]</span></td>
EOT
	}
	$ebout .= <<"EOT";
  </table></td>
 </tr>
</table>
EOT
	&footerA;
	exit;
}

sub Validate2 {
	fopen(FILE,"$members/$URL{'u'}.dat") || &error("$gtxt{'error2'}: $URL{'u'}.dat");
	@sets = <FILE>;
	fclose(FILE);
	chomp @sets;
	$usersetcount = @sets;

	if($sets[23] eq 'ADMIN') {
		$subject = $memtext[156];
		$message = qq~$memtext[157] "$mbname".<br><br><a href="$rurl">$mbname</a><br><br>$gtxt{'25'}~;
	}

	$sets[23] = '';
	$sets[24] = '';
	fopen(FILE,"+>$members/$URL{'u'}.dat");
	for($q = 0; $q < $usersetcount; $q++) { print FILE "$sets[$q]\n"; }
	fclose(FILE);

	smail($sets[2],$subject,$message);

	redirect("$scripturl,v=admin,a=validate");
}

sub IPSearch {
	if($URL{'s'} eq 'advsearch') { &IPSearchCL; }
	if($FORM{'showclicklog'}) { $checked = " checked"; }
	if($FORM{'slowbogger'}) { $checkedb = " checked"; }
	if($URL{'u'} ne '') { $FORM{'finduser'} = $URL{'u'}; }
	$title = $memtext[90];
	&headerA;
	$ebout .= <<"EOT";
<br>
<table class="border" cellpadding="4" cellspacing="1" width="98%" align="center">
 <tr>
  <td class="titlebg"><b><img src="$images/ip.gif"> $title</b></td>
 </tr><tr>
  <td class="win"><span class="smalltext">$memtext[91]</span></td>
 </tr><tr>
  <td class="catbg"><span class="smalltext"><b>$memtext[164]</b></span></td>
 </tr><tr>
  <td class="win2">
   <table cellpadding="2" width="100%">
    <tr><form action="$scripturl,v=admin,a=advipsearch,s=start" method="post" name="sform">
     <td align="right" valign="top" width="14%"><span class="smalltext"><b>$memtext[92]:</b></span></td>
     <td><span class="smalltext"><input type="text" class="textinput" name="finduser" value="$FORM{'finduser'}"><br>$memtext[93]</span></td>
     <td align="right" valign="top" width="14%"><span class="smalltext"><b>$memtext[94]:</b></span></td>
     <td><span class="smalltext"><input type="text" class="textinput" name="findsn" value="$FORM{'findsn'}"><br>$memtext[93]</span></td>
     <td align="right" width="14%" valign="top"><span class="smalltext"><b>$gtxt{'18'}:</b></span></td>
     <td valign="top"><input type="text" class="textinput" name="findip" value="$FORM{'findip'}"></td>
    </tr>
   </table>
  </td>
 </tr><tr>
  <td class="catbg"><span class="smalltext"><b>$memtext[165]</b></span></td>
 </tr><tr>
  <td class="win">
   <table cellpadding="2" width="100%">
    <tr>
     <td valign="top"><input type="checkbox" class="checkboxinput" name="showclicklog" value="1"$checked></td>
     <td><span class="smalltext"><b>$memtext[97]</b><br>$memtext[98]</span></td>
    </tr><tr>
     <td valign="top"><input type="checkbox" class="checkboxinput" name="slowbogger" value="1"$checkedb></td>
     <td><span class="smalltext"><b>$memtext[99]</b><br>$memtext[100]</span></td>
    </tr>
   </table>
  </td>
 </tr><tr>
  <td class="win2" colspan="2" align="center"><input type="submit" class="button" value=" Search "></td>
 </form></tr>
</table><br>
<table class="border" cellpadding="4" cellspacing="1" width="98%" align="center">
 <tr>
  <td class="titlebg">
   <table cellpadding="2" cellspacing="0" width="100%">
    <tr>
     <td class="titlebgtext" width="33%"><span class="smalltext"><b>$memtext[94]</b></span></td>
     <td class="titlebgtext" width="33%" align="center"><span class="smalltext"><b>$memtext[15]</b></span></td>
     <td class="titlebgtext" width="33%" align="right"><span class="smalltext"><b>$gtxt{'18'}</b></span></td>
    </tr>
   </table>
  </td>
 </tr>
EOT
	if($URL{'s'} || $URL{'u'}) {
		$FORM{'finduser'} =~ s/[#%+,\\\/:?"<>'|@^\$\&~'\)\(\]\[\;{}!`=-]//g;
		$FORM{'finduser'} =~ s/(.*)\*(.*)\Z/\?/;
		if($1 && $2) { $FORM{'finduser'} = "$1?.$2"; }
		elsif($2 && !$1) { $FORM{'finduser'} = ".+?$2"; }
		elsif($1 && !$2) { $FORM{'finduser'} = "$1"; }
		$FORM{'finduser'} =~ s/\*(.*)\Z//gsi;
		$FORM{'findsn'} =~ s/[#%+,\\\/:?"<>'|@^\$\&~'\)\(\]\[\;{}!`=-]//g;
		$FORM{'findsn'} =~ s/(.*)\*(.*)\Z/\?/;
		if($1 && $2) { $FORM{'findsn'} = "$1.?.$2"; }
		elsif($2 && !$1) { $FORM{'findsn'} = ".+?$2"; }
		elsif($1 && !$2) { $FORM{'findsn'} = "$1"; }

		$FORM{'findsn'} =~ s/\*(.*)\Z//gsi;
		fopen(FILE,"$members/List.txt");
		@members = <FILE>;
		fclose(FILE);
		chomp @members;
		foreach(@members) {
			loaduser($_);
			if($FORM{'finduser'} && $_ =~ /($FORM{'finduser'})/i) { push(@aresults,$_); }
			if($FORM{'findsn'} && $userset{$_}->[1] =~ /($FORM{'findsn'})/i) { push(@bresults,$_); }
		}

		$iplength = length($FORM{'findip'});

		if($FORM{'slowbogger'}) {
			foreach $use (@boardbase) {
				($board) = split('/',$use);
				fopen(FILE,"$boards/$board.msg");
				while(<FILE>) {
					chomp $_;
					($id,$msub,$t,$t,$replies) = split(/\|/,$_);
					fopen(MESSAGE,"$messages/$id.txt");
					while( $buffer = <MESSAGE> ) {
						chomp $buffer;
						($user,$t,$ip) = split(/\|/,$buffer);
						push(@IPLog,"$user||$ip");
					}
					fclose(MESSAGE);
				}
				fclose(FILE);
			}
		} else {
			fopen(FILE,"$prefs/IpLog.txt");
			@IPLog = <FILE>;
			fclose(FILE);
			chomp @IPLog;
		}
		foreach(@IPLog) {
			($user,$t,$ip) = split(/\|/,$_);
			$ipsearch = substr($ip,0,$iplength);
			if($FORM{'findip'} && $ipsearch =~ /\Q$FORM{'findip'}/i) { push(@cresults,"$user|$ip"); }
			if(!$nneed{$ip,$user}) {
				$userip{$user} .= "$ip, ";
				$nneed{$ip,$user} = 1;
			}
			$list{$ip} = $user;
		}

		foreach(@aresults) {
			$userip{$_} =~ s/, \Z//gsi;
			if(!$userip{$_}) { $userip{$_} = $gtxt{'1'}; }
			$aresults .= qq~<tr><td width="33%" valign="top"><span class="smalltext"><a href="$scripturl,v=memberpanel,a=view,u=$_">$userset{$_}->[1]</a></span></td><td width="34%" valign="top" align="center"><span class="smalltext">$_</span></td><td width="33%" align="right"><span class="smalltext">$userip{$_}</span></td></tr>~;
		}

		foreach(@bresults) {
			$userip{$_} =~ s/, \Z//gsi;
			if(!$userip{$_}) { $userip{$_} = $gtxt{'1'}; }
			$bresults .= qq~<tr><td width="33%" valign="top"><span class="smalltext"><a href="$scripturl,v=memberpanel,a=view,u=$_">$userset{$_}->[1]</a></span></td><td width="34%" valign="top" align="center"><span class="smalltext">$_</span></td><td width="33%" align="right"><span class="smalltext">$userip{$_}</span></td></tr>~;
		}

		foreach(@cresults) {
			($tuser,$tip) = split(/\|/,$_);
			if($fnd{$tuser} eq $tip) { next; }
			$fnd{$tuser} = $tip;
			if($userset{$tuser}->[1]) { $user = qq~<a href="$scripturl,v=memberpanel,a=view,u=$tuser">$userset{$tuser}->[1]</a>~; $un = $tuser; }
				else { $user = $tuser; $un = ''; }
			$cresults .= qq~<tr><td width="33%"><span class="smalltext">$user</span></td><td width="34%" align="center"><span class="smalltext">$un</span></td><td width="33%" align="right"><span class="smalltext">$tip</span></td></tr>~;
		}

		if($FORM{'showclicklog'}) {
			fopen(FILE,"$prefs/ClickLog.txt");
			@clicklog = <FILE>;
			fclose(FILE);
			chomp @clicklog;
			foreach(@clicklog) {
				($lastactive,$tip) = split(/\|/,$_);
				if($al{$tip}) { next; }
				$tuser = $list{$tip};
				if($userset{$tuser}->[1]) { $user = qq~<a href="$scripturl,v=memberpanel,a=view,u=$tuser">$userset{$tuser}->[1]</a>~; $un = $tuser; }
					else { $user = $tip." ($gtxt{'0'})"; $un = ''; }
				$dresults .= qq~<tr><td width="33%"><span class="smalltext">$user</span></td><td width="34%" align="center"><span class="smalltext">$un</span></td><td width="33%" align="right"><span class="smalltext"><a href="$scripturl,v=admin,a=advipsearch,s=advsearch,ip=$tip">$tip</a></span></td></tr>~;
				$al{$tip} = 1;
			}
		}
	}

	if($aresults eq '') { $aresults = qq~<tr><td><span class="smalltext">$memtext[109]</span></td></tr>~; }
	if($bresults eq '') { $bresults = qq~<tr><td><span class="smalltext">$memtext[109]</span></td></tr>~; }
	if($cresults eq '') { $cresults = qq~<tr><td><span class="smalltext">$memtext[109]</span></td></tr>~; }
	if($dresults eq '') { $dresults = qq~<tr><td><span class="smalltext">$memtext[109]</span></td></tr>~; }

	$ebout .= <<"EOT";
 <tr>
  <td class="catbg"><span class="smalltext"><b>&#155; $memtext[15] $memtext[114]</b></span></td>
 </tr><tr>
  <td class="win2">
   <table cellpadding="2" cellspacing="0" width="100%">
    $aresults
   </table>
  </td>
 </tr><tr>
  <td class="catbg"><span class="smalltext"><b>&#155; $memtext[94] $memtext[114]</b></span></td>
 </tr><tr>
  <td class="win2">
   <table cellpadding="2" cellspacing="0" width="100%">
    $bresults
   </table>
  </td>
 </tr><tr>
  <td class="catbg"><span class="smalltext"><b>&#155; $gtxt{'18'} $memtext[114]</b></span></td>
 </tr><tr>
  <td class="win2">
   <table cellpadding="2" cellspacing="0" width="100%">
    $cresults
   </table>
  </td>
 </tr>
EOT
	if($FORM{'showclicklog'}) {
		$ebout .= <<"EOT";
 <tr>
  <td class="catbg"><span class="smalltext"><b>&#155; $memtext[119] $memtext[114]</b></span></td>
 </tr><tr>
  <td class="win2">
   <table cellpadding="2" cellspacing="0" width="100%">
    $dresults
   </table>
  </td>
 </tr>
EOT
	}
	$ebout .= "</table>";
	&footerA;
	exit;
}

sub IPSearchCL {
	if($URL{'ip'} ne '' && $URL{'ip'} !~ /[1-9.]/ || $URL{'ip'} =~ /[A-Za-z]/) { &error($memtext[121]); }

	# We need to find the username that the IP address we are looking at may be ...
	if($URL{'slow'} || !$logip) {
		foreach $use (@boardbase) {
			($board) = split('/',$use);
			fopen(FILE,"$boards/$board.msg");
			while(<FILE>) {
				chomp $_;
				($id,$msub,$t,$t,$replies) = split(/\|/,$_);
				fopen(MESSAGE,"$messages/$id.txt");
				while( $buffer = <MESSAGE> ) {
					chomp $buffer;
					($user,$t,$ip) = split(/\|/,$buffer);
					push(@IPLog,"$user||$ip");
				}
				fclose(MESSAGE);
			}
			fclose(FILE);
		}
	} else {
		fopen(FILE,"$prefs/IpLog.txt");
		@IPLog = <FILE>;
		fclose(FILE);
		chomp @IPLog;
	}
	$iplength = length($URL{'ip'});
	foreach(@IPLog) {
		($user,$t,$ip) = split(/\|/,$_);
		$list{$ip} = $user;
	}

	$couldbe = $list{$URL{'ip'}};
	loaduser($couldbe);

	if($userset{$couldbe}->[1]) { $uncbe = qq~<a href="$scripturl,v=memberpanel,a=view,u=$couldbe">$userset{$couldbe}->[1]</a>~; }
	elsif($couldbe) { $uncbe = $couldbe; }
		else { $uncbe = $memtext[122]; }

	fopen(FILE,"$prefs/ClickLog.txt");
	@clicklog = <FILE>;
	fclose(FILE);
	chomp @clicklog;
	foreach(@clicklog) {
		($lastactive,$tip,$refer,$clicks,$info) = split(/\|/,$_);
		$ipsearch = substr($tip,0,$iplength);
		if($ipsearch !~ /\Q$URL{'ip'}/i) { next; }
		if($refer) { $referals .= qq~<a href="$refer">$refer</a><br>~; }
		if($lastactive > $lasttime) { $lasttime = $lastactive; }
		if($hits{$clicks} eq '') { push(@clickx,$clicks); $hits{$clicks} = 1; }
			else { ++$hits{$clicks}; }
		push(@info,$info);

	}
	foreach(@clickx) { push(@clickz,"$hits{$_}|$_"); }

	@clickx = ();

	foreach(sort{$b <=> $a} @clickz) {
		($hitcnt,$hiturl) = split(/\|/,$_);
		if($hiturl eq '') { $hit = $memtext[123]; } else { $hit = $hiturl; }
		$hits .= qq~<a href="$surl$hiturl">$hit</a> ($hitcnt $var{'89'})<br>~;
	}

	if($referals eq '') { $referals = $memtext[124]; }

	for($i = 0; $i < @info; $i++) {
		$fndb = 0;
		$fndo = 0;
		$_ = $info[$i];
		$ost = $_;
		if($ost =~ s/Windows ([A-Za-z0-9. ]{2,8})//) { $os = "Windows $1"; }
		elsif($ost =~ s/Win([ 0-9.]{2,8})//) { $os = "Windows $1"; }
		elsif($ost =~ s/Linux([ 0-9.]{2,8})//) { $os = "Linux $1"; }
			else { $os = $gtxt{'1'}; }
		if($ost =~ s/Opera ([0-9.A-Za-z]{2,9})// || $ost =~ s/Opera\/([0-9.A-Za-z]{2,9})//) { $browser = "Opera $1"; }
		elsif($ost =~ s/MSIE ([0-9.A-Za-z]{2,9})//) { $browser = "Microsoft Internet Explorer $1"; }
		elsif($ost =~ s/Netscape ([0-9.A-Za-z]{2,9})//) { $browser = "Netscape $1"; }
		elsif($ost =~ s/Gecko\/([0-9]{4})([0-9]{2})([0-9]{2})//) { $browser = "Gecko ($2.$3.$1)"; }
		elsif($ost =~ s/Gecko//) { $browser = "Gecko"; }
		elsif($ost =~ s/Mozilla\/([0-9.A-Za-z]{2,9})//) { $browser = "Mozilla $1"; }
			else { $browser = $gtxt{'1'}; }
		if($os =~ s/NT 5.1//) { $os .= "XP"; }
		elsif($os =~ s/NT 5.0//) { $os .= "2000"; }
		elsif($os =~ s/NT 6.0//) { $os .= "Longhorn"; }
		if(!$found{$browser}) { $browserl .= $browser.'<br>'; $found{$browser} = 1; }
		if(!$found{$os}) { $osl .= $os.'<br>'; $found{$os} = 1; }
	}

	$title = "$memtext[90] - $memtext[119]";
	&headerA;
	if($lasttime) { $lastactive = get_date($lasttime); }
		else { &error($memtext[166]); }
	$ebout .= <<"EOT";
<br>
<table class="border" cellpadding="4" cellspacing="1" width="98%" align="center">
 <tr>
  <td class="titlebg"><b><img src="$images/ip.gif"> $title</b></td>
 </tr><tr>
  <td class="catbg"><span class="smalltext"><b>&#155; $memtext[125]</b></span></td>
 </tr><tr>
  <td class="win2">
   <table cellpadding="2" cellspacing="0" width="100%">
    <tr>
     <td width="45%" align="right"><span class="smalltext"><b>$memtext[15]:</b><br>$memtext[127] <a href="$scripturl,v=admin,a=advipsearch,s=advsearch,ip=$URL{'ip'},slow=on">$memtext[141]</a>.</span></td>
     <td valign="top" width="55%"><span class="smalltext">$uncbe</span></td>
    </tr><tr>
     <td width="45%" align="right"><span class="smalltext"><b>$gtxt{'18'}:</b></span></td>
     <td valign="top" width="55%"><span class="smalltext">$URL{'ip'}</span></td>
    </tr><tr>
     <td width="45%" align="right"><span class="smalltext"><b>$memtext[128]:</b></span></td>
     <td valign="top" width="55%"><span class="smalltext">$lastactive</span></td>
    </tr>
   </table>
  </td>
 </tr><tr>
  <td class="catbg"><span class="smalltext"><b>&#155; $memtext[129]</b></span></td>
 </tr><tr>
  <td class="win2"><span class="smalltext">$osl</span></td>
 </tr><tr>
  <td class="catbg"><span class="smalltext"><b>&#155; $memtext[130]</b></span></td>
 </tr><tr>
  <td class="win2"><span class="smalltext">$browserl</span></td>
 </tr><tr>
  <td class="catbg"><span class="smalltext"><b>&#155; $memtext[131]</b> (<i>$memtext[132]</i>)</span></td>
 </tr><tr>
  <td class="win2"><span class="smalltext">$referals</span></td>
 </tr><tr>
  <td class="catbg"><span class="smalltext"><b>&#155; $memtext[133]</b> (<i>$memtext[134]</i>)</span></td>
 </tr><tr>
  <td class="win2"><span class="smalltext">$hits</span></td>
 </tr>
</table>
EOT
	&footerA;
	exit;
}

sub RemoveMembers {
	if($URL{'p'}) { &RemoveMembers2; }
	$title = $memtext[146];
	&headerA;
	$ebout .= <<"EOT";
<script language="javascript">
<!-- //
function check() {
 for(i = 0; i < document.remove.elements.length; i++) {
  if(document.remove.c.checked) { document.remove.elements[i].checked = true; }
   else { document.remove.elements[i].checked = false; }
 }
}
function ConfirmSubmit() {
	if(window.confirm('$memtext[172]')) { return true; } else { return false; }
}
// -->
</script>
<table class="border" cellpadding="4" cellspacing="1" width="98%" align="center">
 <tr><form action="$scripturl,v=admin,a=removemembers,p=2" method="post" name="remove" onSubmit="return ConfirmSubmit()">
  <td class="titlebg"><b><img src="$images/profile_sm.gif"> $title</b></td>
 </tr><tr>
  <td class="win"><span class="smalltext">$memtext[135]</span></td>
 </tr><tr>
  <td class="win2">
   <table cellpadding="4" cellspacing="0" width="100%">
    <tr>
     <td width="225" class="catbg"><span class="smalltext"><b>$memtext[115]</b></span></td>
     <td width="200" class="catbg"><span class="smalltext"><b>$memtext[163]</b></span></td>
     <td width="200" class="catbg"><span class="smalltext"><b>$memtext[171]</b></span></td>
     <td width="20" class="catbg"><span class="smalltext"><b>$memtext[2]</b></span></td>
    </tr>
EOT
	fopen(FILE,"$members/List.txt");
	@list = <FILE>;
	fclose(FILE);
	chomp @list;

	foreach(@list) {
		loaduser($_);
		if(!$userset{$_}->[1]) { next; }
		if($FORM{'email'} && $FORM{'email'} ne $userset{$_}->[2]) { next; }
		if($FORM{'name'} && ($FORM{'name'} ne $userset{$_}->[1] && $FORM{'name'} ne $_)) { next; }

		push(@tlist,$_);
	}

	$thelist = @tlist || 1;
	$tstart = $URL{'s'} || 0;
	$end = $URL{'s'}+50;
	$tstart = (int($tstart/50)*50);

	$pagecount = 1;
	for($i = 0; $i < $thelist; $i += 50) {
		if($i == $tstart || $thelist < 50) { $pagelinks .= "<b>$pagecount</b>, "; }
			else { $pagelinks .= qq~<a href="$scripturl,v=admin,a=removemembers,s=$i">$pagecount</a>, ~; }
		++$pagecount;
	}
	$pagelinks =~ s/, \Z//gsi;

	@list = ();
	for($i = 0; $i < @tlist; ++$i) {
		if($i <= $end && $i >= $tstart) { push(@list,$tlist[$i]); }
	}

	foreach(@list) {
		$check = $_ ne $username ? qq~<input type="checkbox" class="checkboxinput" name="$_" value="1">~ : '';

		if($userset{$_}->[28]) { $lastactive = get_date($userset{$_}->[28]); }
			else { $lastactive = "Unknown"; }
		$ebout .= <<"EOT";
    <tr>
     <td><span class="smalltext">$check <a href="$scripturl,v=memberpanel,u=$_,a=view">$userset{$_}->[1]</a> &nbsp; (<i>$_</i>)</span></td>
     <td class="win"><span class="smalltext"><a href="mailto:$userset{$_}->[2]"><img src="$images/email_sm.gif" border="0">&nbsp; $userset{$_}->[2]</a></span></td>
     <td><span class="smalltext">$lastactive</span></td>
     <td class="win" align="right"><span class="smalltext">$userset{$_}->[3]</span></td>
    </tr>
EOT
	}
	if(@list == 0) {
		$ebout .= <<"EOT";
<tr>
 <td colspan="4" align="center"><br><b>$memtext[170]</b><br><br></td>
</tr>
EOT
	}

	$ebout .= <<"EOT";
   </table>
  </td>
 </tr><tr>
  <td class="win">
   <table cellpadding="0" cellspacing="0" width="100%"><tr>
    <td width="25"><input type="checkbox" class="checkboxinput" name="c" value="1" onClick="check();"></td>
    <td><span class="smalltext"><b>$gtxt{17}:</b> $pagelinks</span></td>
    <td align="right" width="270"><input type="submit" class="button" value="$memtext[137]"></td>
   </tr></table>
  </td>
 </form></tr>
<table><br>
<table class="border" cellpadding="4" cellspacing="1" width="500" align="center">
 <tr>
  <td class="catbg"><b><img src="$images/search_sm.gif"> $memtext[144]</b></td>
 </tr><tr><form action="$scripturl,v=admin,a=removemembers" method="post" name="srch">
  <td class="win2">
   <table cellpadding="4" cellspacing="0">
    <tr>
     <td width="200" align="right"><span class="smalltext"><b>$memtext[145]:</b></span></td>
     <td><input type="text" class="textinput" name="name"></b></span></td>
    </tr><tr>
     <td align="right"><span class="smalltext"><b>$memtext[163]:</b></span></td>
     <td><input type="text" class="textinput" name="email"></td>
    </tr><tr>
     <td>&nbsp;</td>
     <td><input type="submit" class="button" value=" $memtext[143] "></td>
    </tr>
   </table>
  </td>
 </form></tr>
</table>
EOT
	&footerA;
	exit;
}

sub RemoveMembers2 {
	while(($deluser,$t) = each(%FORM)) {
		if($deluser eq $username) { next; }
		loaduser($deluser);
		if($userset{$deluser}->[32]) {
			$userset{$deluser}->[5] =~ s/$uploadurl\///gsi;
			unlink("$uploaddir/$userset{$deluser}->[5]");
		}
		unlink("$members/$deluser.dat","$members/$deluser.vlog","$members/$deluser.lo","$members/$deluser.log","$members/$deluser.pm","$members/$deluser.prefs","$members/$deluser.msg");
		$good = 1;
	}
	if(!$good) { &error($gtxt{'bfield'}); }
	CoreLoad('Admin1');
	&Remem; # Recount all members and return
	redirect("$scripturl,v=admin,r=3");
}
1;