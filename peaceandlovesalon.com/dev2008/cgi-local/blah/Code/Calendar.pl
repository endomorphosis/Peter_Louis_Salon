################################################
# E-Blah Bulliten Board Systems       Platinum #
################################################
# Copyright (c) 2001 - 2004 e-blah!            #
# All Rights Reserved.                         #
################################################

use Time::Local 'timelocal';

CoreLoad('Calendar',1);

sub CalendarLoad {
	my($next,$last);
	# We need to get a few globls for the Calendar, here they are:
	get_date(time);

	$kday = $day; $kmon = $curmonth+1; $kyear = $year;

	# Lets get the varibles in, and check for errors
	$gmonth = $gmonth2 = ($URL{'month'} || $kmon);
	--$gmonth;
	$gyear = $URL{'year'} || $kyear;
	if($gyear > 2015 || $gyear < 1995) { $gyear = $kyear; }
	if($gmonth < 0 || $gmonth > 11) { $gmonth = $kmon; }
	$gyear2 = $gyear - 1900;
	$keepyear = $gyear;

	if($URL{'v'} ne 'mod') { &Month; }
}

sub Month {
	if($gyear eq '' && $gmonth eq '') { &error("Please reupload Blah.$scripttype!"); }
	eval { $mstart = timelocal(1,1,1,1,$gmonth,$gyear,1); };
	($t,$t,$t,$qmday,$qmon,$qyear,$qwday,$qyday) = localtime($mstart);

	if($URL{'id'} ne '') { &ShowID; }

	$title = "$months[$gmonth] $keepyear";
	&header;

	$ebout .= <<"EOT";
<table width="100%" cellpadding="0" cellspacing="0">
 <tr>
  <td width="76%" valign="top">
EOT
	if($URL{'id'} ne '') { $ebout .= $event; }
	elsif($URL{'week'}) { &WeekView; }
		else { &MonthView; }

	$ebout .= <<"EOT";
  </td><td>&nbsp;</td><td valign="top">
EOT
	if($URL{'week'} eq '' && $URL{'id'} eq '') {
		($t,$t,$t,$t,$qmon,$qyear) = localtime($mstart-86400); # Get last month
		eval { $mstart2 = timelocal(1,1,1,1,$qmon,$qyear,1); };
		&SimpleCalendar($mstart2, $calendar[29]);
		$ebout .= "<br>";

		($t,$t,$t,$t,$qmon,$qyear) = localtime($mstart+2937600); # Get next month (36 days ahead)
		eval { $mstart2 = timelocal(1,1,1,1,$qmon,$qyear,1); };
		&SimpleCalendar($mstart2, $calendar[31]);
	}
		else {
			if($nonoyr == 1990) {
				eval { $mstart = timelocal(1,1,1,1,$kmon,$kyear,1); };
			}
			&SimpleCalendar($mstart, $calendar[32]);
		}
	&OtherOptions;
	$ebout .= <<"EOT";
  </td>
 </tr>
</table>
EOT

	&footer;
	exit;
}

sub WeekView {
	$ebout .= <<"EOT";
   <table cellpadding="5" cellspacing="1" width="100%" class="border">
    <tr>
     <td class="titlebg" colspan="2"><b>$calendar[33] - $title</b></td>
    </tr>
EOT
	$days = CheckDays($gmonth);

	$daycnt = 1;
	$qwday2 = $qwday;
	++$qwday;
	$numweeks = 1;
	&GetBirthdays;
	&GetEvents;
	for($i = $qwday; $i <= 7; $i++) {
		if($numweeks == $URL{'week'}) {
			if($BirthDay{"$gmonth|$daycnt"}) {
				$BirthDay{"$gmonth|$daycnt"} =~ s/, \Z//;
				$birthdays = qq~<b>Birthdays:</b> $BirthDay{"$gmonth|$daycnt"}~;
			} else { $birthdays = ''; }
			if($Events{"$gmonth|$daycnt|$gyear2"} ne '' || $Events{"$gmonth|$daycnt|90"} ne '') {
				$events = $Events{"$gmonth|$daycnt|$gyear2"}.$Events{"$gmonth|$daycnt|90"};
				$events =~ s/, \Z//;
				$events .= "<br>";
			} else { $events = ''; }
			if($birthdays eq '' && $events eq '') { $events = $calendar[34]; }
			$ebout .= <<"EOT";
    <tr>
     <td class="catbg" colspan="2"><b>$days[($i-1)]</b></td>
    </tr><tr>
     <td class="win" width="50" align="center"><font size="5"><b>$daycnt</b></font></td>
     <td class="win2" valign="top" width="93%">$events $birthdays</td>
    </tr>
EOT
			$weekshown = 1;
		}
		if($days == $daycnt) { last; }
		if($i == 7 && $days > $daycnt) {
			++$numweeks;
			$i = 0;
		}
		++$daycnt;
	}
	if(!$weekshown) {
		$ebout .= <<"EOT";
    <tr>
     <td class="win" colspan="2">$calendar[35]</td>
    </tr>
EOT
	}

	$ebout .= "</table>";
}

sub OtherOptions {
	$sel{$gmonth} = ' selected';
	for($x = 0; $x < 12; ++$x) { $current .= '<option value="'.($x+1).qq~"$sel{$x}>$months[$x]</option>\n~; }
	$yrs{$gyear} = " selected";
	for($ey = -2; $ey < 3; $ey++) {
		$curyr = $gyear+$ey;
		if($curyr < 1995 || $curyr > 2015) { next; }
		$yearzz .= qq~<option value="$curyr"$yrs{$curyr}>$curyr</option>~;
	}

	if($settings[4] eq 'Administrator' || $calmod) { $adminlook = qq~<hr class="hr" color="$color{'border'}" size="1"><a href="$scripturl,v=mod,a=calendar">$calendar[36]</a>~; }

	$ebout .= <<"EOT";
<script language="JavaScript">
<!--
function JumpTo(ghere,year) {
 location = "$scripturl,v=cal,month="+ghere+",year="+year;
}
//-->
</script>
   <br><table cellpadding="5" cellspacing="1" class="border" width="100%">
    <tr>
     <td class="titlebg"><b>$calendar[37]</b></td>
    </tr><tr>
     <td class="win">
      <table width="100%">
       <tr>
        <td align="center" colspan="2" class="smalltext">$months[$gmonth] $keepyear</td>
       </tr><tr>
        <td class="smalltext"><b>$calendar[38]:</b></td>
        <td align="right"><select name="JumpZ" onChange="JumpTo(this.value,$gyear);">$current</select></td>
       </tr><tr>
        <td class="smalltext"><b>$calendar[39]:</b></td>
        <td align="right"><select name="JumpZ" onChange="JumpTo($gmonth+1,this.value);">$yearzz</select></td>
       </tr><tr>
        <td colspan="2" align="center" class="smalltext">$adminlook</td>
       </tr>
      </td>
     </table>
    </tr>
   </table>
EOT
}

sub SimpleCalendar {
	my($thisyear,$tempmonth2);
	my($mstart,$toptitle) = @_;

	($t,$t,$t,$t,$tempmonth,$tempyear,$mindent) = localtime($mstart);

	$thisyear = $tempyear+1900;
	$tempmonth2 = $tempmonth+1;

	$print = <<"EOT";
<table cellpadding="5" cellspacing="1" class="border" width="100%">
 <tr>
  <td colspan="8" class="titlebg"><table cellpadding="0" cellspacing="0" width="100%"><tr><td class="titlebgtext"><b><a href="$scripturl,v=cal,month=$tempmonth2,year=$thisyear">$months[$tempmonth] $thisyear</a></b></td><td align="right" class="titlebgtext"><span class="smalltext">$toptitle</span></td></tr></table></td>
 </tr><tr>
  <td class="catbg" width="13">&nbsp;</td>
EOT
	for($i = 0; $i < 7; ++$i) {
		$print .= <<"EOT";
  <td class="catbg" align="center"><b>$smdlist[$i]</b></td>
EOT
	}
	$print .= "</tr>";

	if($mindent > 0) {
		$print .= <<"EOT";
 <tr>
  <td class="win" align="center"><b><a href="$scripturl,v=cal,month=$tempmonth2,year=$thisyear,week=1">&#155;</a></b></td>
  <td class="win" colspan="$mindent">&nbsp;</td>
EOT
	}
	$days = CheckDays($tempmonth);

	$daycnt = 1;
	++$mindent;
	$numweeks = 1;
	for($i = $mindent; $i <= 7; $i++) {
		if($kday == $daycnt && $kmon == $tempmonth && ($kyear == $tempyear+1900)) { $style = qq~ style="border:2px solid red; font-weight: bold"~; } else { $style = ''; }
		if($numweeks == $URL{'week'} && $style eq '' && $URL{'v'} eq 'cal') { $style2 = 'win3'; } else { $style2 = 'win2'; }
		if($i == 1) {
			$print .= qq~<td class="win" align="center"><b><a href="$scripturl,v=cal,month=$tempmonth2,year=$thisyear,week=$numweeks">&#155;</a></b></td>~;
		}
		$print .= qq~<td class="$style2" align="center"$style>$daycnt</td>~;
		if($i == 7 && $days > $daycnt) {
			$print .= "</tr>";
			++$numweeks;
			$i = 0;
		}
		if($days == $daycnt) { last; }
		++$daycnt;
	}

	if($i != 7) {
		$alldays = 7-$i;
		$print .= <<"EOT";
<td class="win" valign="top" colspan="$alldays">&nbsp;</td>
EOT
	}
	$print .= "</tr></table>";
	if($URL{'v'} eq cal) { $ebout .= $print; } else { return($print); }
}

sub MonthView {
	$ebout .= <<"EOT";
<table cellpadding="5" cellspacing="1" width="100%" class="border">
 <tr>
  <td class="titlebg" colspan="8"><b>$months[$gmonth] $keepyear</b></td>
 </tr><tr>
  <td class="catbg" width="20">&nbsp;</td>
EOT
	for($i = 0; $i < 7; ++$i) {
		$ebout .= <<"EOT";
  <td class="catbg" align="center" width="14%"><b>$days[$i]</b></td>
EOT
	}
	$ebout .= "</tr>";

	if($qwday > 0) {
		$ebout .= <<"EOT";
 <tr>
  <td class="win"><font size="2"><b><a href="$scripturl,v=cal,month=$gmonth2,year=$gyear,week=1">&nbsp;&#155;&nbsp;<br>&nbsp;&#155;&nbsp;<br>&nbsp;&#155;&nbsp;</a></b></font></td>
  <td class="win" height="70" colspan="$qwday">&nbsp;</td>
EOT
		 $lweek = 1;
	}
	$days = CheckDays($gmonth);

	$qwday2 = $qwday;
	$daycnt = 1;
	++$qwday;
	&GetBirthdays;
	&GetEvents;
	$week = 1;
	for($i = $qwday; $i <= 7; $i++) {
		$BirthDay{"$gmonth|$daycnt"} =~ s/, \Z//;
		if($BirthDayC{"$gmonth|$daycnt"}) {
			$BirthDayL{"$gmonth|$daycnt"} =~ s/\n\Z//;
			$birthdays = qq~<a href="$scripturl,v=cal,month=$gmonth2,year=$gyear,week=$week" title="$BirthDayL{"$gmonth|$daycnt"}">$BirthDayC{"$gmonth|$daycnt"} $calendar[40]</a>~;
		}
			else { $birthdays = ''; }
		if($Events{"$gmonth|$daycnt|$gyear2"} ne '' || $Events{"$gmonth|$daycnt|90"} ne '') {
			$events = $Events{"$gmonth|$daycnt|$gyear2"}.$Events{"$gmonth|$daycnt|90"};
			$events =~ s/, \Z//;
			$events .= "<br>";
		} else { $events = ''; }
		if($week != $lweek) {
			$ebout .= qq~  <td class="win"><font size="2"><b><a href="$scripturl,v=cal,month=$gmonth2,year=$gyear,week=$week">&nbsp;&#155;&nbsp;<br>&nbsp;&#155;&nbsp;<br>&nbsp;&#155;&nbsp;</a></b></font></td>~;
			$lweek = $week;
		}

		if($kday == $daycnt && $kmon == $gmonth && $kyear == $gyear) { $style = qq~ style="border:2px solid red; font-weight: bold"~; } else { $style = ''; }

		$ebout .= qq~  <td class="win2" height="70" valign="top"$style><b>$daycnt</b><br><span class="smalltext">$events$birthdays</span></td>~;
		if($i == 7 && $days > $daycnt) {
			++$week;
			$ebout .= "</tr>"; $i = 0;
		}
		if($days == $daycnt) { last; }
		++$daycnt;
	}

	if($i != 7) {
		$alldays = 7-$i;
		$ebout .= <<"EOT";
<td class="win" height="90" valign="top" colspan="$alldays">&nbsp;</td>
EOT
	}

	$ebout .= <<"EOT";
 </tr>
</table>
EOT
}

sub GetBirthdays {
	fopen(FILE,"$members/List.txt") || &error($calendar[1]);
	@loadlist = <FILE>;
	fclose(FILE);
	chomp @loadlist;
	foreach(@loadlist) {
		loaduser($_);
		if(!$userset{$_}->[16]) { next; }
		($bmon,$day,$year) = split("/",$userset{$_}->[16]);

		$bmon = sprintf("%.0f",$bmon)-1;
		$day = sprintf("%.0f",$day);

		$BirthDay{"$bmon|$day"} .= qq~<a href="$scripturl,v=memberpanel,a=view,u=$_">$userset{$_}->[1]</a>, ~;
		$BirthDayL{"$bmon|$day"} .= "$userset{$_}->[1]\n";
		++$BirthDayC{"$bmon|$day"};
	}
}

sub GetEvents {
	my($eventz);

	fopen(FILE,"$prefs/Events.txt");
	@events = <FILE>;
	fclose(FILE);
	chomp @events;
	foreach(@events) {
		($id,$evdate,$title,$message,$author,$spanrange,$spancolor) = split(/\|/,$_);

		if($spancolor eq '') { $spancolor = "clear"; }

		$eventz = qq~<span style="background: $spancolor; padding: 0px; font-weight: bold"><a href="$scripturl,v=cal,id=$id">$title</a></span>, ~;

		++$spanrange;
		if($spanrange) {
			for($za = 1; $za < $spanrange; $za++) {
				($t,$t,$t,$eday,$emon,$eyear) = localtime($evdate+(86400*$za));
				$Events{"$emon|$eday|$eyear"} .= $eventz;
			}
		}

		($t,$t,$t,$eday,$emon,$eyear) = localtime($evdate);
		$Events{"$emon|$eday|$eyear"} .= $eventz;
		push(@useonce,$eventz);
	}
}

sub ShowID {
	my($evtitled,$id,$evdate,$evtitle,$author,$spanrange,$spancolor,$topic);

	fopen(FILE,"$prefs/Events.txt");
	@events = <FILE>;
	fclose(FILE);
	chomp @events;
	foreach(@events) {
		($id,$evdate,$evtitle,$mess,$author,$spanrange,$spancolor,$topic) = split(/\|/,$_);
		if($id == $URL{'id'}) {
			$message = $mess;
			($t,$t,$t,$eday,$emon,$eyear) = localtime($evdate);
			eval { $mstart = timelocal(1,1,1,1,$emon,$eyear,1); };
			$eyear += 1900;
			$nonoyr = $eyear;
			if($eyear != 1990) { $eyear = ", $eyear"; } else { $eyear = ''; }
			$datestart = "$months[$emon] $eday$eyear";
			if($spanrange) {
				($t,$t,$t,$eday,$emon,$eyear) = localtime($evdate+(86400*$spanrange));
				$eyear += 1900;
				if($eyear != 1990) { $eyear = ", $eyear"; } else { $eyear = ''; }
				$datestart .= " - $months[$emon] $eday$eyear";
			}
			if($URL{'id'} eq $id) { $evtitled = $evtitle; }
			&loaduser($author);
			if($userset{$author}->[1]) { $user = qq~<b>$calendar[41]:</b> <a href="$scripturl,v=memberpanel,a=view,u=$author">$userset{$author}->[1]</a><br>~; } else { $user = ''; }
			last;
		}
	}
	if($evtitled eq '') { &error($calendar[42]); }

	$title = $evtitled;

	if($settings[4] eq 'Administrator' || $calmod) { $modify = qq~<a href="$scripturl,v=mod,a=calendar,edit=$URL{'id'}">$img{'modify'}</a>~; }
	&BC;

	$event = <<"EOT";
<table width="100%" cellpadding="5" cellspacing="1" class="border">
 <tr>
  <td class="titlebg">$evtitled</td>
 </tr><tr>
  <td class="win">
   <table cellpadding="0" cellspacing="0" width="100%">
    <tr>
     <td>$user<b>$calendar[43]:</b> $datestart</td>
     <td align="right">$modify</td>
    </tr>
   </table>
  </td>
 </tr><tr>
  <td class="win2" class="postbody">$message</td>
 </tr>
</table>
EOT
}

sub CalendarEvents {
	fopen(FILE,"$prefs/Events.txt");
	@events = <FILE>;
	fclose(FILE);
	chomp @events;
	if($URL{'p'} eq 'save') { &CalendarSave; }

	foreach(@events) {
		($number,$edate2,$sum,$f,$author,$span,$spancolor,$type) = split(/\|/,$_);
		($t,$t,$t,$qday,$qmon,$qyear) = localtime($edate);
		if($URL{'edit'} && $URL{'edit'} eq $number) { $edate = $edate2; $showuser = $author eq '' ? 'checked' : ''; $summary = $sum; $full = Unformat($f); $remove = qq~ <input type="submit" class="button" value=" $calendar[13] " name="remove">~; $fnd = 1; last; }
	}
	if(!$fnd && $URL{'edit'}) { &error($gtxt{'error2'}); }

	$edate = $edate || time;

	if($URL{'edit'}) { $title = $calendar[53]; } else {
		$number = $sum = $author = $span = $spancolor = $type = '';
		$title = $calendar[54];
	}

	&header;
	$ebout .= <<"EOT";
<table cellpadding="4" cellspacing="1" class="border" width="750" align="center">
 <tr><form action="$scripturl,v=mod,a=calendar,edit=$URL{'edit'},p=save" method="post" name="msend">
  <td class="titlebg"><b>$title</b></td>
 </tr><tr>
  <td class="win">
   <table width="100%">
    <tr>
     <td width="30%" align="right"><b>$calendar[44]:</b></td>
     <td><input type="text" class="textinput" name="evtitle" value="$summary" size="25"></td>
    </tr><tr>
     <td width="30%" align="right"><b>$calendar[45]:</b></td>
     <td><select name="month">
EOT
	($t,$t,$t,$qday,$qmon,$qyear) = localtime($edate);

	$mon{$qmon} = ' selected';
	for($i = 0; $i < 12; ++$i) {
		$ebout .= qq~<option value="$i"$mon{$i}>$months[$i]</option>~;
	}

	$ebout .= qq~</select> <select name="day">~;

	$day{$qday} = ' selected';
	for($i = 1; $i < 32; ++$i) { $ebout .= qq~<option value="$i"$day{$i}>$i</option>~; }
	$ebout .= qq~</select> <select name="year">~;
	$qyear += 1900;
	$yrs{$qyear} = " selected";
	if($qyear == 1990) { $qyear = 2005; }
	for($ey = -2; $ey < 3; $ey++) {
		$curyr = $qyear+$ey;
		$ebout .= qq~<option value="$curyr"$yrs{$curyr}>$curyr</option>~;
	}

	$ebout .= <<"EOT";
     <option value="1990"$yrs{1990}>$calendar[46]</option></select></td>
    </tr><tr>
     <td width="30%" align="right"><b>$calendar[47]:</b></td>
     <td><input type="text" class="textinput" name="span" value="$span" size="5"></td>
    </tr></tr>
     <td>&nbsp;</td><td><span class="smalltext">$calendar[48]</span></td>
    </tr><tr>
     <td width="30%" align="right"><b>$calendar[49]:</b></td>
     <td><input type="text" class="textinput" name="spancolor" value="$spancolor" size="10"></td>
    </tr></tr>
     <td>&nbsp;</td><td><span class="smalltext">$calendar[50]</span></td>
    </tr><tr>
     <td width="30%" align="right"><b>$calendar[51]</b></td>
     <td><input type="checkbox" class="checkboxinput" name="showuser" value="1"$showuser></td>
    </tr></tr>
     <td>&nbsp;</td><td><span class="smalltext">$calendar[52]</span></td>
    </tr>
   </table>
  </td>
 </tr>
EOT
	CoreLoad('Post');
	if($BCLoad || $BCSmile) { &BCWait; }
	if($BCLoad) { &BCLoad; }
	$ebout .= <<"EOT";
 <tr>
  <td class="win">
   <table cellpadding="4" cellspacing="0" width="100%">
    <tr>
     <td align="right" width="30%" valign="top"><b>$calendar[24]:</b><br><br>
EOT

	if($BCSmile) { &BCSmile; }

	$ebout .= <<"EOT";
    </td>
     <td><textarea name="message" wrap="virtual" rows="12" cols="70">$full</textarea></td>
    </tr>
   </table>
  </td>
 </tr><tr>
  <td align="center" class="win2"><input type="submit" class="button" value=" $calendar[28] " name="submit">$remove</td>
 </form></tr>
</table>
EOT
	&footer;
	exit;
}

sub CalendarSave {
	$FORM{'year'} -= 1900;
	eval { $mstart = timelocal(1,1,1,$FORM{'day'},$FORM{'month'},$FORM{'year'},1); };
	$sum = Format($FORM{'evtitle'});
	$full = Format($FORM{'message'});
	error($gtxt{'long'}) if(length($full) > $maxmesslth && $maxmesslth);

	if($sum eq '' || $full eq '' || !$mstart || ($FORM{'span'} > 50)) { &error($gtxt{'bfield'}); }

	if($FORM{'showuser'}) { $author = ''; } else { $author = $username; }

	if(!$URL{'edit'}) {
		$count = time;
		fopen(FILE,"+>>$prefs/Events.txt");
		print FILE qq~$count|$mstart|$sum|$full|$author|$FORM{'span'}|$FORM{'spancolor'}|0\n~;
		fclose(FILE);
	} else {
		foreach(@events) {
			($count,$odate,$osum,$ofull) = split(/\|/,$_);
			if($count eq $URL{'edit'} && $FORM{'remove'}) { next; }
			elsif($count eq $URL{'edit'}) { $all .= "$count|$mstart|$sum|$full|$author|$FORM{'span'}|$FORM{'spancolor'}|0\n"; }
				else { $all .= "$_\n"; }
		}

		fopen(FILE,"+>$prefs/Events.txt");
		print FILE $all;
		fclose(FILE);
	}
	$url = "$scripturl,v=cal";
	if(!$FORM{'remove'}) { $url .= ",id=$URL{'edit'}"; }
	&redirect;
}
1;