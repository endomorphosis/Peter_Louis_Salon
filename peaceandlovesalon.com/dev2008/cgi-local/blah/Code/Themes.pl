################################################
# E-Blah Bulliten Board Systems       Platinum #
################################################
# Copyright (c) 2001 - 2004 e-blah!            #
# All Rights Reserved.                         #
################################################

CoreLoad('Themes',1);

$themever = 'Platinum';
$themesupp = 2;

sub ThemeManager {
	&is_admin;
	$url = "$scripturl,v=admin,a=themesman";
	$themeid = time;

	if($URL{'p'} eq 'create') { &Create; }
	elsif($URL{'p'} eq 'cdefault') { &CDefault; }
	elsif($URL{'p'} eq 'delete') { &Delete; }
	elsif($URL{'p'} eq 'convert') { &Convert; }

	$title = $themetxt[1];
	&headerA;
	$ebout .= <<"EOT";
<script language="javascript">
<!-- //
function clear(name,name2) {
 if(window.confirm("$themetxt[3]\\n\\n"+(name2))) { location = '$url,p=delete,m='+name; }
}
// -->
</script>
<table cellpadding="4" cellspacing="1" class="border" width="98%" align="center">
 <tr>
  <td class="titlebg"><b>$title</b></td>
 </tr>
EOT

	opendir(DIR,"$theme/");
	@themes = readdir(DIR);
	closedir(DIR);
	foreach(@themes) {
		$themename = $_;
		$themename =~ s/ /\+/gsi;
		if($_ =~ m/(.*)(.v1)/) { push(@one,"1|$_"); ++$one{1}; }
		if($_ =~ m/(.*)(.v2)/) { push(@one,"2|$_"); ++$one{2}; }
	}

	$ebout .= <<"EOT";
 <tr>
  <td class="win">
   <table width="100%">
    <tr>
EOT
	$counter = 0;
	if($settings[26]) { $bkup = $name; }
	foreach(sort{$b <=> $a} @one) {
		($o,$themen) = split(/\|/,$_);
		$themen =~ m/(.*)(.v1|.v2)/; $nme = $1;
		if(!$noshow{$o}) {
			if($counter == 1) { $ebout .= "<td>&nbsp;</td></tr><tr>"; $counter = 0; }
			if($counter == 2) { $ebout .= "</tr><tr>\n"; $counter = 0; }
			$ebout .= <<"EOT";
$next<td colspan="2"><span class="smalltext"><b>$themetxt[4] $o $themetxt[5]</b> ($one{$o})</span></td></tr><tr><td colspan="2" height="2" class="border"><img src="$images/Avatars/nopic.gif" height="2"></td></tr><tr>
EOT
			$noshow{$o} = 1;
		}
		StopThemes("$theme/$themen") || next;
		$themename = $name;
		if($o == 1) {
 			$themename = $themen;
			$themename =~ s/ /\+/gsi;
			$actions = qq~&nbsp;&#149; <a href="$url,p=convert,m=$nme">$themetxt[22]</a>~;
		} else {
			if($settings[26] eq $nme) { $themename = $bkup; }
			$themename2 = $themename;
			$themename2 =~ s/\'/\\\'/gsi;
			$actions = qq~&nbsp;&#149; <span onClick="javascript:window.open('$surl,theme=$nme','smiles','align=center,height=500,width=750,resizable=yes,scrollbars=yes,status=yes');" style="cursor:hand;">$themetxt[25]</span><br>&nbsp;&#149; <a href="$url,p=create,m=$nme">$themetxt[6]</a><br>&nbsp;&#149; <a href="javascript:clear('$nme','$themename2');">$themetxt[7]</a>~;
		}
		$ebout .= <<"EOT";
$next<td width="50%"><b>$themename</b><span class="smalltext"><br>$actions<br><br></span></td>
EOT
		++$counter;
		if($counter == 2) { $next = "</tr><tr>\n"; $counter = 0; } else { $next = ''; }
	}
	if($counter == 1) { $ebout .= "<td>&nbsp;</td></tr>"; }
	$counter = @one;
	if(!$counter) { $ebout .= <<"EOT";
<td>$themetxt[2]</td>
EOT
	}
	if(!$settings[26]) { $created = qq~<a href="$url,p=cdefault">$themetxt[9]</a>~; }
	$ebout .= <<"EOT";
   $next</table>
  </td>
 </tr><tr>
  <td class="win2"><span class="smalltext"><b><a href="$url,p=create">$themetxt[8]</a><br>$created</b></span></td>
 </tr><tr>
  <td class="win"><span class="smalltext"><b>$themetxt[21]:</b> $themever<br><b>$themetxt[10]:</b> $themesupp</span></td>
 </tr>
</table>
EOT
	&footerA;
	exit;
}

sub Create {
	&is_admin;
	if($FORM{'theme'}) { &Save; }



	if($URL{'m'}) {
		if(!-e("$theme/$URL{'m'}.v2")) { error($themetxt[26]); }
		$use1 = "$theme/$URL{'m'}.html";
		$use2 = "$theme/s_$URL{'m'}.html";
		StopThemes("$theme/$URL{'m'}.v2") || error($themetxt[26]);
		if($temp ne $use1) { $use1 = $temp; }
		if($stemp ne $use2) { $use2 = $stemp; }
	} else {
		$use1 = "$root/template.html";
		$use2 = "$prefs/smilies.html";
	}

	$title = $themetxt[11];
	headerA();

	fopen(FILE,$use1);
	while (<FILE>) { chomp $_; $templatez .= "$_\n"; }
	fclose(FILE);
	fopen(FILE,$use2);
	while (<FILE>) { chomp $_; $stemplatez .= "$_\n"; }
	fclose(FILE);
	$preview = $preview ? ' checked' : '';
	if(!$preview) { $change = qq~ onLoad="change()"~; }
	if($URL{'m'}) { $nameedit = $name; }
	$ebout .= <<"EOT";
<table cellpadding="4" cellspacing="1" class="border" width="98%" align="center">
 <tr>
  <td class="titlebg"><b>$title</b></td>
 </tr><tr><form action="$url,p=create,m=$URL{'m'}" method="post" name="post">
  <td class="win">
   <table cellpadding="3" cellspacing="0" width="100%">
    <tr>
     <td width="35%" align="right"><b>$themetxt[12]:</b></td>
     <td width="65%"><input type="text" class="textinput" class="textinput" name="theme" value="$nameedit" size="40"></td>
    </tr>
   </table>
  </td>
 </tr><tr>
  <td class="win2">
   <table cellpadding="3" cellspacing="0" width="100%">
    <tr>
     <td width="35%" align="right" valign="top"><b>$themetxt[13]:</b></td>
     <td width="65%"><textarea name="template" rows="10" cols="90">$templatez</textarea></td>
    </tr><tr>
     <td width="35%" align="right" valign="top"><b>$themetxt[14]:</b></td>
     <td width="65%"><textarea name="stemplate" rows="10" cols="90">$stemplatez</textarea></td>
    </tr>
   </table>
  </td>
 </tr><tr>
  <td class="win">
   <table cellpadding="3" cellspacing="0" width="100%">
    <tr>
     <td width="35%" align="right"><b>$themetxt[24]:</b></td>
     <td width="65%"><input type="text" class="textinput" class="textinput" name="images" value='$imgz' size="30"></td>
    </tr><tr>
     <td width="35%" align="right"><b>$themetxt[23]:</b></td>
     <td width="65%"><input type="text" class="textinput" class="textinput" name="simages2" value='$simages2' size="30"></td>
    </tr>
   </table>
  </td>
 </tr><tr>
  <td class="win2">
   <table cellpadding="3" cellspacing="0" width="100%">
    <tr>
     <td colspan="2" align="center"><input type="submit" class="button" value=" $themetxt[20] " name="submit"></td>
    </form></tr>
   </table>
  </td>
 </tr>
</table>
EOT
	footerA();
	exit;
}

sub Save {
	&is_admin;
	if($URL{'m'} ne '') { $themeid = $URL{'m'}; }
	fopen(FILE,">$theme/$themeid.html");
	print FILE $FORM{'template'};
	fclose(FILE);
	fopen(FILE,">$theme/s_$themeid.html");
	print FILE $FORM{'stemplate'};
	fclose(FILE);

	if($FORM{'images'}) {
		$extras = <<"EOT";
\n\$images = qq*$FORM{'images'}*;
EOT
	}
	if($FORM{'simages2'}) {
		$extras .= <<"EOT";
\n\$simages2 = qq*$FORM{'simages2'}*;
EOT
	}

	fopen(FILE,">$theme/$themeid.v2");
	print FILE <<"EOT";
\$name = "$FORM{'theme'}";
\$template = qq*$theme/$themeid.html*;
\$smiliestemplate = qq*$theme/s_$themeid.html*;$extras
1;
EOT
	fclose(FILE);
	&redirect;
}

sub CDefault {
	&is_admin;
	use File::Copy;
	copy("$root/template.html","$theme/$themeid.html");
	copy("$prefs/smilies.html","$theme/s_$themeid.html");
	fopen(FILE,">$theme/$themeid.v2");
	print FILE <<"EOT";
\$name = "Default Theme";
\$template = qq*$theme/$themeid.html*;
\$smiliestemplate = qq*$theme/s_$themeid.html*;
1;
EOT
	fclose(FILE);
	&redirect;
}

sub StopThemes {
	($whatfile) = $_[0];
	$tmp = $template;
	$stmp = $smiliestemplate;
	$imgs = $images;
	# Get theme colors
	CoreLoad($whatfile,2) || return(0);
	$temp = $template;
	$stemp = $smiliestemplate;
	$imgz = $images;
	# Restore Colors ...
	$template = $tmp;
	$smiliestemplate = $stmp;
	$images = $imgs;
	return(1);
}

sub Delete {
	&is_admin;
	unlink("$theme/$URL{'m'}.v2","$theme/$URL{'m'}.html","$theme/s_$URL{'m'}.html");
	redirect();
}

sub Convert { # Platinum 2 Convertor ...
	&is_admin;
	StopThemes("$theme/$URL{'m'}.v1") || error($themetxt[26]);
	use File::Copy;
	if(-e("$template")) { copy("$template","$theme/$themeid.html"); }
		else { copy("$root/template.html","$theme/$themeid.html"); }
	if(-e("$smiliestemplate")) { copy("$smiliestemplate","$theme/s_$themeid.html"); }
		else { copy("$prefs/smilies.html","$theme/s_$themeid.html"); }
	$name = $URL{'m'};
	$name =~ s/_/ /gsi;
	fopen(FILE,">$theme/$themeid.v2");
	print FILE <<"EOT";
\$name = "$name";
\$template = qq*$theme/$themeid.html*;
\$smiliestemplate = qq*$theme/s_$themeid.html*;
1;
EOT
	fclose(FILE);
	unlink("$theme/$URL{'m'}.v1");
	redirect();
}
1;