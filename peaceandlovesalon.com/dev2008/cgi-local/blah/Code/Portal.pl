################################################
# E-Blah Bulliten Board Systems       Platinum #
################################################
# Copyright (c) 2001 - 2004 e-blah!            #
# All Rights Reserved.                         #
################################################

CoreLoad('Portal',1);

if(-e("$prefs/PSettings.pl")) { require("$prefs/PSettings.pl"); } else { &Defaults; }

&VerifyBoard; # Run this once -- it verifies the boards

sub Defaults {
	# Global
	$lastmemcnt = "5";
	$showlastpostsn = "5";
	$topmemcnt = "10";

	# Left Vars
	$lenable = 1;
	# Left Variables
	$lmembers = 1;
	$lmembersn = 1;
	$lnewmem = 1;
	$lnewmemn = 3;
	$lboard = 0;
	$lboardn = 4;
	$lmemstats = 1;
	$lmemstatsn = 2;
	$lcalendar = 0;
	$lcalendarn = 5;
	# Center Variables
	$showlastposts = 1;
	# Right Vars
	$renable = 1;
	# Right variables
	$rmembers = 0;
	$rmembersn = 4;
	$rnewmem = 0;
	$rnewmemn = 3;
	$rboard = 1;
	$rboardn = 1;
	$rmemstats = 0;
	$rmemstatsn = 5;
	$rcalendar = 1;
	$rcalendarn = 2;
}

sub PortalAdmin {
	&is_admin;
	if($URL{'p'}) { &PortalSave; }

	if($lenable) { $lenablec = " checked"; }
	if($lmembers) { $lmembersc = " checked"; }
	$lmembers{$lmembersn} = " selected";
	if($lnewmem) { $lnewmemc = " checked"; }
	$lnewmem{$lnewmemn} = " selected";
	if($lboard) { $lboardc = " checked"; }
	$lboard{$lboardn} = " selected";
	if($lmemstats) { $lmemstatsc = " checked"; }
	$lmemstats{$lmemstatsn} = " selected";
	if($lcalendar) { $lcalendarc = " checked"; }
	$lcalendar{$lcalendarn} = " selected";

	if($renable) { $renablec = " checked"; }
	if($rmembers) { $rmembersc = " checked"; }
	$rmembers{$rmembersn} = " selected";
	if($rnewmem) { $rnewmemc = " checked"; }
	$rnewmem{$rnewmemn} = " selected";
	if($rboard) { $rboardc = " checked"; }
	$rboard{$rboardn} = " selected";
	if($rmemstats) { $rmemstatsc = " checked"; }
	$rmemstats{$rmemstatsn} = " selected";
	if($rcalendar) { $rcalendarc = " checked"; }
	$rcalendar{$rcalendarn} = " selected";

	$showlastpostsc{$showlastposts} = " selected";

	if($portalversion != 'Platinum') { $portalversion = "$portalversion $portal[39]"; } else { $portalversion = $portalversion; }

	$title = "$mbname $portal[1]";
	&headerA;
	$ebout .= <<"EOT";
<table cellpadding="4" cellspacing="1" class="border" width="600" align="center">
 <tr><form action="$scripturl,v=admin,a=portal,p=1" method="POST">
  <td class="titlebg"><b><img src="$images/xx.gif"> $title</b></td>
 </tr><tr>
  <td class="win"><span class="smalltext">$portal[41]</span></td>
 </tr><tr>
  <td class="catbg"><b>$portal[2]</b></td>
 </tr><tr>
  <td class="win2" align="100%">
   <table width="100%">
    <tr>
     <td width="50%" align="right"><b>$portal[3]:</b></td>
     <td width="50%"><input type="text" class="textinput" name="lastmemcnt" value="$lastmemcnt"></td>
    </tr><tr>
     <td width="50%" align="right"><b>$portal[38]:</b></td>
     <td width="50%"><input type="text" class="textinput" name="topmemcnt" value="$topmemcnt"></td>
    </tr><tr>
     <td width="50%" align="right"><b>$portal[37]:</b></td>
     <td width="50%"><span class="smalltext"><b>&nbsp;&nbsp;&nbsp;<img src="$images/question.gif"> $portalversion</b></span></td>
    </tr>
   </table>
  </td>
 </tr><tr>
  <td class="catbg"><b>$portal[4]</b></td>
 </tr><tr>
  <td class="win" align="100%">
   <table width="100%">
    <tr>
     <td width="50%" align="right"><b>$portal[5]</b></td>
     <td width="50%"><input type="checkbox" class="checkboxinput" name="lenable" value="1"$lenablec></td>
    </tr><tr>
     <td width="50%" align="right"><b>$portal[6]</b></td>
     <td width="50%"><input type="checkbox" class="checkboxinput" name="lmembers" value="1"$lmembersc><span class="smalltext"><b>&nbsp; &nbsp;&nbsp; &nbsp;$portal[9]: </span><select name="lmembersn">
     <option value="1"$lmembers{'1'}>1</option>
     <option value="2"$lmembers{'2'}>2</option>
     <option value="3"$lmembers{'3'}>3</option>
     <option value="4"$lmembers{'4'}>4</option>
     <option value="5"$lmembers{'5'}>5</option>
     </select></b></td>
    </tr><tr>
     <td width="50%" align="right"><b>$portal[7]</b></td>
     <td width="50%"><input type="checkbox" class="checkboxinput" name="lnewmem" value="1"$lnewmemc><span class="smalltext"><b>&nbsp; &nbsp;&nbsp; &nbsp;$portal[9]: </span><select name="lnewmemn">
     <option value="1"$lnewmem{'1'}>1</option>
     <option value="2"$lnewmem{'2'}>2</option>
     <option value="3"$lnewmem{'3'}>3</option>
     <option value="4"$lnewmem{'4'}>4</option>
     <option value="5"$lnewmem{'5'}>5</option>
     </select></b></td>
    </tr><tr>
     <td width="50%" align="right"><b>$portal[8]</b></td>
     <td width="50%"><input type="checkbox" class="checkboxinput" name="lboard" value="1"$lboardc><span class="smalltext"><b>&nbsp; &nbsp;&nbsp; &nbsp;$portal[9]: <select name="lboardn">
     <option value="1"$lboard{'1'}>1</option>
     <option value="2"$lboard{'2'}>2</option>
     <option value="3"$lboard{'3'}>3</option>
     <option value="4"$lboard{'4'}>4</option>
     <option value="5"$lboard{'5'}>5</option>
     </select></span></b></td>
    </tr><tr>
     <td width="50%" align="right"><b>$portal[32]</b></td>
     <td width="50%"><input type="checkbox" class="checkboxinput" name="lmemstats" value="1"$lmemstatsc><span class="smalltext"><b>&nbsp; &nbsp;&nbsp; &nbsp;$portal[9]: <select name="lmemstatsn">
     <option value="1"$lmemstats{'1'}>1</option>
     <option value="2"$lmemstats{'2'}>2</option>
     <option value="3"$lmemstats{'3'}>3</option>
     <option value="4"$lmemstats{'4'}>4</option>
     <option value="5"$lmemstats{'5'}>5</option>
     </select></span></b></td>
    </tr><tr>
     <td width="50%" align="right"><b>$portal[34]</b></td>
     <td width="50%"><input type="checkbox" class="checkboxinput" name="lcalendar" value="1"$lcalendarc><span class="smalltext"><b>&nbsp; &nbsp;&nbsp; &nbsp;$portal[9]: <select name="lcalendarn">
     <option value="1"$lcalendar{'1'}>1</option>
     <option value="2"$lcalendar{'2'}>2</option>
     <option value="3"$lcalendar{'3'}>3</option>
     <option value="4"$lcalendar{'4'}>4</option>
     <option value="5"$lcalendar{'5'}>5</option>
     </select></span></b></td>
    </tr>
   </table>
  </td>
 </tr><tr>
  <td class="catbg"><b>$portal[10]</b></td>
 </tr><tr>
  <td class="win2" align="100%">
   <table width="100%">
    <tr>
     <td width="50%" align="right"><b>$portal[11]</b></td>
     <td width="50%"><select name="showlastposts">
     <option value="0"$showlastpostsc{'0'}>$portal[29]</option>
     <option value="1"$showlastpostsc{'1'}>$portal[30]</option>
     <option value="2"$showlastpostsc{'2'}>$portal[31]</option>
     </select></td>
    </tr><tr>
     <td width="50%" align="right"><b>$portal[12]</b></td>
     <td width="50%"><input type="text" class="textinput" name="showlastpostsn" value="$showlastpostsn"></td>
    </tr>
   </table>
  </td>
 </tr><tr>
  <td class="catbg"><b>$portal[13]</b></td>
 </tr><tr>
  <td class="win" align="100%">
   <table width="100%">
    <tr>
     <td width="50%" align="right"><b>$portal[5]</b></td>
     <td width="50%"><input type="checkbox" class="checkboxinput" name="renable" value="1"$renablec></td>
    </tr><tr>
     <td width="50%" align="right"><b>$portal[6]</b></td>
     <td width="50%"><input type="checkbox" class="checkboxinput" name="rmembers" value="1"$rmembersc><span class="smalltext"><b>&nbsp; &nbsp;&nbsp; &nbsp;$portal[9]: <select name="rmembersn">
     <option value="1"$rmembers{'1'}>1</option>
     <option value="2"$rmembers{'2'}>2</option>
     <option value="3"$rmembers{'3'}>3</option>
     <option value="4"$rmembers{'4'}>4</option>
     <option value="5"$rmembers{'5'}>5</option>
     </select></span></b></td>
    </tr><tr>
     <td width="50%" align="right"><b>$portal[7]</b></td>
     <td width="50%"><input type="checkbox" class="checkboxinput" name="rnewmem" value="1"$rnewmemc><span class="smalltext"><b>&nbsp; &nbsp;&nbsp; &nbsp;$portal[9]: <select name="rnewmemn">
     <option value="1"$rnewmem{'1'}>1</option>
     <option value="2"$rnewmem{'2'}>2</option>
     <option value="3"$rnewmem{'3'}>3</option>
     <option value="4"$rnewmem{'4'}>4</option>
     <option value="5"$rnewmem{'5'}>5</option>
     </select></span></b></td>
    </tr><tr>
     <td width="50%" align="right"><b>$portal[8]</b></td>
     <td width="50%"><input type="checkbox" class="checkboxinput" name="rboard" value="1"$rboardc><span class="smalltext"><b>&nbsp; &nbsp;&nbsp; &nbsp;$portal[9]: <select name="rboardn">
     <option value="1"$rboard{'1'}>1</option>
     <option value="2"$rboard{'2'}>2</option>
     <option value="3"$rboard{'3'}>3</option>
     <option value="4"$rboard{'4'}>4</option>
     <option value="5"$rboard{'5'}>5</option>
     </select></span></b></td>
    </tr><tr>
     <td width="50%" align="right"><b>$portal[32]</b></td>
     <td width="50%"><input type="checkbox" class="checkboxinput" name="rmemstats" value="1"$rmemstatsc><span class="smalltext"><b>&nbsp; &nbsp;&nbsp; &nbsp;$portal[9]: <select name="rmemstatsn">
     <option value="1"$rmemstats{'1'}>1</option>
     <option value="2"$rmemstats{'2'}>2</option>
     <option value="3"$rmemstats{'3'}>3</option>
     <option value="4"$rmemstats{'4'}>4</option>
     <option value="5"$rmemstats{'5'}>5</option>
     </select></span></b></td>
    </tr><tr>
     <td width="50%" align="right"><b>$portal[34]</b></td>
     <td width="50%"><input type="checkbox" class="checkboxinput" name="rcalendar" value="1"$rcalendarc><span class="smalltext"><b>&nbsp; &nbsp;&nbsp; &nbsp;$portal[9]: <select name="rcalendarn">
     <option value="1"$rcalendar{'1'}>1</option>
     <option value="2"$rcalendar{'2'}>2</option>
     <option value="3"$rcalendar{'3'}>3</option>
     <option value="4"$rcalendar{'4'}>4</option>
     <option value="5"$rcalendar{'5'}>5</option>
     </select></span></b></td>
    </tr>
   </table>
  </td>
 </tr><tr>
  <td class="win2" align="center"><input type="submit" class="button" name="submit" value="$portal[14]"></td>
 </form></tr>
</table>
EOT
	&footerA;
	exit;
}

sub PortalSave {
	&is_admin;
	@checks = ('lenable','lmembers','lnewmem','lboard','renable','rmembers','rnewmem','rboard','lmemstats','rmemstats','rcalendar','lcalendar');
	foreach(@checks) { $PORTAL{$_} = $FORM{$_} || 0; }

	$printtofile = <<"EOT";
####################################################
# Blah Software Code   |   Software Version: Final #
####################################################
# Portal Setup File                                #
####################################################

\$portalversion = "Platinum";

# Global
\$lastmemcnt = "$FORM{'lastmemcnt'}";
\$showlastpostsn = "$FORM{'showlastpostsn'}";
\$topmemcnt = "$FORM{'topmemcnt'}";

# Left Vars
\$lenable = $PORTAL{'lenable'};
# Left Variables
\$lmembers = $PORTAL{'lmembers'};
\$lmembersn = $FORM{'lmembersn'};
\$lnewmem = $PORTAL{'lnewmem'};
\$lnewmemn = $FORM{'lnewmemn'};
\$lboard = $PORTAL{'lboard'};
\$lboardn = $FORM{'lboardn'};
\$lmemstats = $PORTAL{'lmemstats'};
\$lmemstatsn = $FORM{'lmemstatsn'};
\$lcalendar = $PORTAL{'lcalendar'};
\$lcalendarn = $FORM{'lcalendarn'};
# Center Variables
\$showlastposts = $FORM{'showlastposts'};
# Right Vars
\$renable = $PORTAL{'renable'};
# Right variables
\$rmembers = $PORTAL{'rmembers'};
\$rmembersn = $FORM{'rmembersn'};
\$rnewmem = $PORTAL{'rnewmem'};
\$rnewmemn = $FORM{'rnewmemn'};
\$rboard = $PORTAL{'rboard'};
\$rboardn = $FORM{'rboardn'};
\$rmemstats = $PORTAL{'rmemstats'};
\$rmemstatsn = $FORM{'rmemstatsn'};
\$rcalendar = $PORTAL{'rcalendar'};
\$rcalendarn = $FORM{'rcalendarn'};
1;
EOT
	fopen(FILE,"+>$prefs/PSettings.pl",1);
	print FILE "$printtofile";
	fclose(FILE);
	redirect("$scripturl,v=admin,a=main");
}

sub Portal {
	CoreLoad('Calendar');
	($t,$t,$t,$kday,$kmon,$kyear) = localtime(time+(3600*($settings[15]+$settings[31]+$gtoff)));
	eval { $gtime = timelocal(1,1,1,1,$kmon,$kyear,1); };
	$kyear += 1900;

	for($i = 0; $i < @memgrps; $i++) {
		if($i > 6) { ($t,$t,$t,$t,$t,$t,$t,$memgrpp[$i]) = split(",",$memgrpp[$i]); }
		$gcolors{$memgrps[$i]} = $memgrpp[$i];
	}

	$percent = 100;
	# Gather information to display
	if($lenable) { # Build the left panel
		if($lmembers) { $leftdata{$lmembersn} = &MembersOnline; }
		if($lnewmem) { $leftdata{$lnewmemn} = &LastMember; }
		if($lboard) { $leftdata{$lboardn} = &BoardData; }
		if($lmemstats) { $leftdata{$lmemstatsn} = &Stats; }
		if($lcalendar) { $leftdata{$lcalendarn} = SimpleCalendar($gtime,$portal[35])."<br>"; }
		$left = <<"EOT";
  <td width="25%" valign="top"><span class="smalltext">$leftdata{1}$leftdata{2}$leftdata{3}$leftdata{4}$leftdata{5}</span></td>
EOT
		$percent -= 25;
	}
	if($renable) { # Build the right panel
		if($rmembers) { $rightdata{$rmembersn} = &MembersOnline; }
		if($rnewmem) { $rightdata{$rnewmemn} = &LastMember; }
		if($rboard) { $rightdata{$rboardn} = &BoardData; }
		if($rmemstats) { $rightdata{$rmemstatsn} = &Stats; }
		if($rcalendar) { $rightdata{$rcalendarn} = SimpleCalendar($gtime,$portal[35])."<br>"; }
		$right = <<"EOT";
  <td width="25%" valign="top"><span class="smalltext">$rightdata{1}$rightdata{2}$rightdata{3}$rightdata{4}$rightdata{5}</span></td>
EOT
		$percent -= 25;
	}

	if($showlastposts == 1) { &LatestThreads; }
	&News;
	if($showlastposts == 2) { &LatestThreads; }

	$title = "$mbname $portal[47]";
	&header;
	$ebout .= <<"EOT";
<table cellpadding="1" cellspacing="0" width="100%">
 <tr>$left
  <td width="$percent%" valign="top">$lastthread</td>
 $right</tr>
</table>
EOT
	&footer;
	exit;
}

sub Stats {
	if(!$mlistload) { &LoadMemberList; }
	foreach (@list) { loaduser($_); }
	$team{'Administrator'} = 1;
	$c = 1;
	$returnto = <<"EOT";
 <table width="100%" cellpadding="4" cellspacing="1" class="border"><tr>
  <td class="titlebg" colspan="2"><span class="smalltext"><b>$portal[33]</b></span></td>
 </tr><tr>
  <td class="win2" colspan="2"><span class="smalltext">
EOT
	for($g = 0; $g < $topmemcnt; ++$g) {
		$team = '';
		($posts,$member) = split(/\|/,$maxmem[$g]);
		$posts = MakeComma($posts);
		if($member eq '') { next; }
		if($team{$userset{$member}->[4]}) { $team = qq~ <img src="$images/team.gif" alt="$gtxt{'29'}"> ~; }
		$returnto .= qq~<b>$c.</b>$team <a href="$surl,v=memberpanel,a=view,u=$member">$userset{$member}->[1]</a> <span class="smalltext">($posts $gtxt{'10'})</span><br>~;
		++$c;
	}
	
	return($returnto."</span></td></tr></table><br>");
}

sub LoadMO {
	fopen(FILE,"$prefs/Active.txt");
	@activelist = <FILE>;
	fclose(FILE);
	$activecnt = @activelist;
	chomp @activelist;
	$hidec = 0;
	$memcnt = 0;
	$gcnt = 0;
	$morun = 1;

	foreach(@activelist) {
		($luser,$ltime,$hidden,$t,$bview) = split(/\|/,$_);
		loaduser($luser);
		if($userset{$luser}->[1] eq '') { $fndu = $luser; } else { $fndu = $userset{$luser}->[1]; }
		push(@quicksort,"$fndu|$luser|$hidden|$bview");
	}

	&LoadColors;
	foreach(sort{lc($a) cmp lc($b)} @quicksort) {
		($t,$luser,$hidden,$bview) = split(/\|/,$_);
		if($userset{$luser}->[1] ne '') {
			$lostuser = $gcolors{$userset{$luser}->[4]} ? qq~<font color="$gcolors{$userset{$luser}->[4]}"><b>$userset{$luser}->[1]</b></font>~ : qq~$userset{$luser}->[1]~;
			if($hidden) {
				++$hidec;
				if($settings[4] eq 'Administrator') { $memberson .= qq~<a href="$rurl,v=memberpanel,a=view,u=$luser">$lostuser</a> &#149; ~; }
					else { next; }
			} else {
				++$memcnt;
				$memberson .= qq~<a href="$rurl,v=memberpanel,a=view,u=$luser">$lostuser</a> &#149; ~;
			}
		} else { ++$gcnt; }
		++$B{$bview};
		++$allview;
	}
	$memberson =~ s/ &#149; \Z//i;
	if($memberson eq '') { $memberson = qq~<i>$portal[40]</i>~; }
}

sub MembersOnline {
	if(!$morun) { &LoadMO; }
	$returnto = <<"EOT";
 <table width="100%" cellpadding="4" cellspacing="1" class="border"><tr>
  <td class="win2" width="25" align="center"><img src="$images/online.gif"></td>
  <td class="win"><span class="smalltext">
<b>$portal[45] $allview $portal[46]</b><br>
$memcnt $portal[42] ($hidec $portal[43]), $gcnt $portal[44]<br>
$memberson</span></td>
 </tr></table><br>
EOT
	return($returnto);
}

sub MembersOnline3 {
	if(!$morun) { &LoadMO; }
	$returnto = <<"EOT";
 <table width="100%" cellpadding="4" cellspacing="1" class="border"><tr>
  <td class="win2" width="25" align="center"><img src="$images/online.gif"></td>
  <td class="win"><span class="smalltext">$portal[15]<br>$var{'83'} ($memcnt): $memberson<br>$gcnt $var{'91'}</span></td>
 </tr></table><br>
EOT
	return($returnto);
}

sub BoardData {
	if(!$bdrun) {
		foreach(@boardbase) {
			($board,$t,$t,$bname,$t,$t,$t,$t,$t,$t,$brdmemgrps) = split("/",$_);
			if($boardallow{$board} != 1) { next; }
			fopen(FILE,"$boards/$board.ino");
			@ino = <FILE>;
			fclose(FILE);
			if($brdmemgrps) {
			}
			if(!$ino[1]) { $ino[1] = 0; }
			push(@tbds,"$ino[1]|$bname|$board");
			chomp @ino;
			$threads = $threads+$ino[0];
			$message = $message+$ino[1];
		}
		$catcnt = $catcounter;
		$bdscnt = $boardcounter;

		@tbds = sort{$b <=> $a} @tbds;

		$c = 1;
		for($g = 0; $g < 10; ++$g) {
			($mcnt,$boardnm,$bid) = split(/\|/,$tbds[$g]);
			$mcnt = MakeComma($mcnt);
			if($boardnm) { $topboards .= qq~<b>$c.</b> <a href="$surl,v=mindex,b=$bid">$boardnm</a> <span class="smalltext">($mcnt $gtxt{'11'})</span><br>~; }
			++$c;
		}
		$bdrun = 1;
	}

	$returnto = <<"EOT";
 <table width="100%" cellpadding="4" cellspacing="1" class="border"><tr>
  <td class="titlebg" colspan="2"><span class="smalltext"><b>$portal[17]</b></span></td>
 </tr><tr>
  <td class="win2" width="25" align="center"><img src="$images/boardinfo.gif"></td>
  <td class="win"><span class="smalltext"><b>$gtxt{'6'}:</b> $catcnt<br><b>$gtxt{'7'}:</b> $bdscnt<br><b>$gtxt{'8'}:</b> $threads<br><b>$gtxt{'9'}:</b> $message</span></td>
 </tr><tr>
  <td class="win2" width="25" align="center"><img src="$images/boardinfo.gif"></td>
  <td class="win"><span class="smalltext">$topboards</span></td>
 </tr></table><br>
EOT
	return($returnto);
}

sub LatestThreads {
	$lastthread .= <<"EOT";
<table class="border" cellpadding="4" cellspacing="1" width="100%">
 <tr>
  <td class="titlebg" colspan="4"><b>$portal[18]</b></td>
 </tr><tr>
  <td class="catbg" width="20" align="center">&nbsp;</td>
  <td class="catbg"><span class="smalltext"><b>$portal[19]</b></span></td>
  <td class="catbg" align="center" width="50"><span class="smalltext"><b>$gtxt{'38'}</b></span></td>
  <td class="catbg" width="200"><span class="smalltext"><b>$portal[21]</b></span></td>
 </tr>
EOT
	foreach $use (@boardbase) {
		($board,$t,$t,$t,$t,$t,$t,$pass) = split('/',$use);
		if(!$boardallow{$board}) { next; }
		fopen(FILE,"$boards/$board.msg");
		$counts = 0;
		while(<FILE>) {
			chomp $_;
			($id,$title,$posted,$date,$replies,$poll,$type,$micon,$date,$lastuser) = split(/\|/,$_);
			push(@data,"$date|$id|$title|$posted|$replies|$poll|$type|$micon|$lastuser|$board");
			++$counts;
			if($counts > 11+$showlastpostsn) { last; }
		}
		fclose(FILE);
	}
	$counter = 1;

	if($username ne 'Guest') {
		fopen(FILE,"$members/$username.log");
		@log = <FILE>;
		fclose(FILE);
		chomp @log;
	}

	foreach(sort{$b <=> $a} @data) {
		if($counter == $showlastpostsn+1) { last; }
		($date,$id,$title,$posted,$replies,$poll,$type,$micon,$lastuser,$board) = split(/\|/,$_);
		if($username ne 'Guest') {
			$new = '';
			$isnew = 0;
			foreach $logged (@log) {
				($mbah,$lmtime) = split(/\|/,$logged);
				if($mbah eq "AllRead_$board" || $mbah eq $id) {
					$isnew = $lmtime-$date;
					last;
				}
			}
			if($isnew <= 0) { $new = qq~<img src="$images/new.gif" alt="New"> ~; }
		}

		$status = FindStatus;
		&loaduser($lastuser);
		&loaduser($posted);
		if($lastuser eq '') { $lastuser = $posted; }
			else { $lastuser = $userset{$lastuser}->[1] ne '' ? qq~<a href="$surl,v=memberpanel,a=view,u=$lastuser">$userset{$lastuser}->[1]</a>~ : $lastuser; }
		$title = &CensorList($title);
		$date = get_date($date);

		$lastthread .= <<"EOT";
 <tr>
  <td class="win" width="20" align="center"><img src="$images/$status.gif"></td>
  <td class="win2"><span class="smalltext">$new <a href="$surl,v=display,b=$board,m=$id" title="$gtxt{'19'} $userset{$posted}->[1]">$title</a></span></td>
  <td class="win" align="center" width="50"><span class="smalltext">$replies</span></td>
  <td class="win2" width="200"><table>
   <tr>
    <td width="20" align="center"><img src="$images/$micon.gif"></td>
    <td><span class="smalltext">$date $portal[23] $lastuser</span></td>
   </tr>
  </table></td>
 </tr>
EOT
		++$counter;
	}

	$lastthread .= "</table><br>";
}

sub News {
	$lastthread .= <<"EOT";
<table class="border" cellpadding="4" cellspacing="1" width="100%">
 <tr>
  <td class="titlebg"><b><img src="$images/news.gif"> $var{'4'}</b></td>
 </tr>
EOT
	@open = split(",",$newsboard);
	foreach(@open) {
		@news1 = ();
		fopen(FILE,"$boards/$_.msg");
		while($curforumread = <FILE>) {
			chomp $curforumread;
			push(@news1,"$curforumread|$_");
		}
		fclose(FILE);
		push(@news,@news1);
	}	$counter = 0;

	$newslength = $newslength || 2000;

	foreach(sort{$b <=> $a} @news) {
		if($counter eq $newsshow) { last; }
		($messid,$messtitle,$posted,$date,$replies,$poll,$type,$micon,$date,$t,$curforumread) = split(/\|/,$_);
		if($posted eq '') { next; }
		fopen(FILE,"$messages/$messid.txt");
		while($temp = <FILE>) {
			($t,$message) = split(/\|/,$temp);
			last;
		}
		fclose(FILE);
		if(length($message) > $newslength) {
			$message =~ s~\[table\](.*?)\[\/table\]~$var{'88'}~sgi;
			$message = substr($message,0,$newslength);

			$message =~ s~([^\w\"\=\[\]]|[\n\b]|\A)\\*(\w+://[\w\~\.\;\:\$\-\+\!\*\?/\=\&\@\#\%]+\.[\w\~\;\:\$\-\+\!\*\?/\=\&\@\#\%,.]+[\w\~\;\:\$\-\+\!\*\?/\=\&\@\#\%]\Z)~~eisg;
			$message =~ s~([^\"\=\[\]/\:\.(\://\w+)]|[\n\b]|\A|[\<\n\b\>])\\*(www\.[^\.][\w\~\.\;\:\$\-\+\!\*\?/\=\&\@\#\%]+\.[\w\~\;\:\$\-\+\!\*\?/\=\&\@\#\%\,]+[\w\~\;\:\$\-\+\!\*\?/\=\&\@\#\%]\Z)~~eisg;

			&BC;
			&MakeSmall;
			$message .= qq~ <a href="$rurl,v=display,b=$curforumread,m=$messid">$portal[48]</a>~;
		} else { &BC; }
		$sdate = &get_date($messid);
		&loaduser($posted);
		if($userset{$posted}->[1] eq '') { $userpost = $posted; }
			else { $userpost = qq~<a href="$rurl,v=memberpanel,a=view,u=$posted">$userset{$posted}->[1]</a>~; }
		$lastthread .= <<"EOT";
 <tr>
  <td class="win">
   <table cellpadding="0" cellspacing="0" width="100%">
    <tr>
     <td><b><img src="$images/$micon.gif">&nbsp;&nbsp;<a href="$rurl,v=display,b=$curforumread,m=$messid">$messtitle</b></td>
     <td align="right"><span class="smalltext">$sdate<br>$gtxt{'36'}: $userpost</span></td>
    </tr>
   </table>
  </td>
 </tr><tr>
  <td class="win2"><span class="smalltext">$message<hr color="$color{'border'}" class="hr" size="1"><a href="$rurl,v=display,b=$newsboard,m=$messid">$portal[26]</a> ($replies)</span></td>
 </tr>
EOT
		++$counter;
	}
	$lastthread .= "</table><br>";
}

sub LoadMemberList {
	fopen(FILE,"$members/List.txt");
	@list = <FILE>;
	fclose(FILE);
	chomp @list;
	foreach(@list) {
		loaduser($_);
		push(@maxmem,"$userset{$_}->[3]|$_");
		push(@newmembers,"$userset{$_}->[14]|$_");
	}
	@maxmem = sort{$b <=> $a} @maxmem;
	@newmembers = sort{$b <=> $a} @newmembers;
	$mlistload = 1;
}

sub LastMember {
	if(!$mlistload) { &LoadMemberList; }
	$returnto = <<"EOT";
 <table width="100%" cellpadding="4" cellspacing="1" class="border"><tr>
  <td class="titlebg" colspan="2"><span class="smalltext"><b>$portal[27]</b></span></td>
 </tr><tr>
  <td class="win2" colspan="2"><span class="smalltext">
EOT
	$counter = 1;
	for($g = 0; $g < $lastmemcnt; ++$g) {
		$team = '';
		($regged,$member) = split(/\|/,$newmembers[$g]);
		$regged = get_date($regged);
		if($member eq '') { next; }
		if($team{$userset{$member}->[4]}) { $team = qq~ <img src="$images/team.gif" alt="$gtxt{'29'}"> ~; }
		$returnto .= qq~  <b>$counter.</b>$team <a href="$surl,v=memberpanel,a=view,u=$member">$userset{$member}->[1]</a> $portal[28] $regged<br>~;
		$regnew .= qq~<b>$c.</b>$team <a href="$surl,v=memberpanel,a=view,u=$member">$userset{$member}->[1]</a> ($regged)<br>~;
		++$counter;
	}
	return($returnto."</span></td></tr></table><br>");
}

sub Shownews {
	print "Content-type: text/html\n\n";
	if($URL{'a'} eq 'lastposts') { LatestThreads(); print $lastthread; exit; }
	elsif($URL{'a'} eq 'online') { &Online; exit; }

	@open = split(",",$newsboard);
	foreach(@open) {
		@news1 = ();
		fopen(FILE,"$boards/$_.msg");
		while($curforumread = <FILE>) {
			chomp $curforumread;
			push(@news1,"$curforumread|$_");
		}
		fclose(FILE);
		push(@news,@news1);
	}	$counter = 0;

	$newslength = $newslength || 2000;

	$usertxt = $gtxt{'36'}; $commenttxt = $portal[26];

	fopen(FILE,"$templates/News.html") || fopen(FILE,"$prefs/News.html");
	@temp = <FILE>;
	fclose(FILE);
	chomp @temp;

	if(!@temp) { &News; print $lastthread; exit; }

	foreach(sort{$b <=> $a} @news) {
		if($counter eq $newsshow) { last; }
		($messid,$messtitle,$posted,$date,$replies,$poll,$type,$micon,$date,$t,$curforumread) = split(/\|/,$_);
		if($posted eq '') { next; }
		fopen(FILE,"$messages/$messid.txt");
		while($temp = <FILE>) {
			($t,$message,$t,$t,$t,$nosmile) = split(/\|/,$temp);
			last;
		}
		fclose(FILE);
		if(length($message) > $newslength) {
			$message =~ s~\[table\](.*?)\[\/table\]~$var{'88'}~sgi;
			$message = substr($message,0,$newslength);

			$message =~ s~([^\w\"\=\[\]]|[\n\b]|\A)\\*(\w+://[\w\~\.\;\:\$\-\+\!\*\?/\=\&\@\#\%]+\.[\w\~\;\:\$\-\+\!\*\?/\=\&\@\#\%,.]+[\w\~\;\:\$\-\+\!\*\?/\=\&\@\#\%]\Z)~~eisg;
			$message =~ s~([^\"\=\[\]/\:\.(\://\w+)]|[\n\b]|\A|[\<\n\b\>])\\*(www\.[^\.][\w\~\.\;\:\$\-\+\!\*\?/\=\&\@\#\%]+\.[\w\~\;\:\$\-\+\!\*\?/\=\&\@\#\%\,]+[\w\~\;\:\$\-\+\!\*\?/\=\&\@\#\%]\Z)~~eisg;

			&BC;
			&MakeSmall;
			$message .= qq~<br><br><a href="$rurl,v=display,b=$curforumread,m=$messid">$portal[48]</a>~;
		} else { &BC; }
		$sdate = &get_date($messid);
		&loaduser($posted);
		if($userset{$posted}->[1] eq '') { $userpost = $posted; }
			else { $userpost = qq~<a href="$rurl,v=memberpanel,a=view,u=$posted">$userset{$posted}->[1]</a>~; }
		$totalurl = "$rurl,v=display,b=$curforumread,m=$messid";

		foreach(@temp) {
			$pdata = $_;
			$pdata =~ s/<blah v="\$(.+?)">/${$1}/gsi;
			print "$pdata\n";
		}
		++$counter;
	}
	exit;
}

sub Online {
	&LoadMO;
	print qq~$var{'83'} ($memcnt): $memberson<br>$gcnt $var{'91'}~;
	exit;
}
1;