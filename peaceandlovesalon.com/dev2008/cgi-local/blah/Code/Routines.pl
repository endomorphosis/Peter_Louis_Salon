################################################
# E-Blah Bulliten Board Systems       Platinum #
################################################
# Copyright (c) 2001 - 2004 e-blah!            #
# All Rights Reserved.                         #
################################################

require("$language/Routines.lng");

# DO NOT REMOVE OR EDIT THE FOLLOWING LINE (unless licensed) ...
$copyright = qq~Powered by <a href="http://www.eblah.com" target="_new">e-blah</a> $versioncr &#169; 2001-2004~;
%botsearch = ('googlebot<>'=>'Googlebot','slurp<>'=>'Yahoo! Bot','msnbot<>'=>'MSN Bot','inktomi<>'=>'Hot Bot','ask jeeves<>'=>'AskJeeves.com','lycos<>'=>'Lycos','ia_archiver<>'=>'Archive.org'); # <> - makes it not open .dat files

sub header {
	my($sfoot,$sep);
	if($hdone) { return; } # Headers have already been completed
	$new = 0;

	if($ENV{'HTTP_ACCEPT_ENCODING'} !~ /gzip/ && $gzipen) { $gzipen = 0; } # Browser doesn't support gzip
	if($gzipen) { print "Content-Encoding: gzip\n"; }
	print "Content-type: text/html\n\n";

	$hdone = 1;

	$date_time = get_date(time,1);
	$kday = $day; $kmon = $curmonth; $kyear = $year;

	# Time of day ...
	if($thour > 11 && $thour < 17)  { $wmessage = $rtxt[46]; } # Afternoon
	elsif($ampm eq 'am') { $wmessage = $rtxt[45]; } # Morning
		else { $wmessage = $rtxt[47]; } # Evening

	if($username eq 'Guest') {
		$userwelcome = $displayuser = qq~$rtxt[1], <b>$rtxt[2]</b>.~;
		$displayuser .= $userpm = qq~$rtxt[3] <a href="$surl,v=login">$rtxt[4]</a> $rtxt[44] <a href="$surl,v=register">$rtxt[5]</a>.~; &ShowGuest; }
	elsif($settings[1] eq '') {
		$userwelcome = $displayuser = qq~$rtxt[1] $rtxt[9] <b>$username</b>.~;
		$displayuser .= $userpm = $rtxt[7];
	} else { $pmcnt = 0; $new = 0; $pmsent = 0; $pmstore = 0;
		fopen(FILE,"$members/$username.pm");
		while(<FILE>) {
			($med,$t,$t,$t,$t,$t,$n) = split(/\|/,$_);
			if($med == 1) {
				if($n) { ++$new; }
				++$pmcnt;
			} elsif($med == 2) { ++$pmsent; }
			elsif($med == 3) { ++$pmstore; }
				else { ++$pmcnt{$med}; }
		}
		fclose(FILE);
		$userwelcome = $displayuser = "$wmessage <b>$settings[1]</b>.";
		if(!$pmdisable) { $displayuser .= $userpm = qq~ $rtxt[10] <b>$pmcnt</b> (<b>$new $rtxt[11]</b>) <a href="$surl,v=memberpanel,a=pm" title="$rtxt[12]">$rtxt[13]</a>.~; }
			else { $displayuser .= $userpm = " $rtxt[54]"; }
		if($settings[4] eq 'Administrator' && ($maintance || $lockout)) {
			if($maintance) { $on = $rtxt[14]; }
			if($maintance && $lockout) { $on .= $rtxt[15]; }
			if($lockout) { $on .= $rtxt[16]; }
			$displayuser = $userpm .= "<br><b><img src='$images/warning_sm.gif'> $rtxt[17] $on <img src='$images/warning_sm.gif'></b>";
		}
	}

	if($helpdesk) { $help = qq~<a href="$helpdesk" target="_new">$img{'help'}</a>~; $sep = $msp; }

	$homemenu = qq~<a href="$surl">$img{'home'}</a>~;
	$calmenu = qq~<a href="$surl,v=cal">$img{'cal'}</a>~;
	$searchmenu = qq~<a href="$surl,v=search">$img{'search'}</a>~;
	$menubar = "$homemenu $sep$help $msp$calmenu $msp$searchmenu $msp";

	if($settings[4] eq 'Administrator') {
		$adminmenu = qq~<a href="$surl,v=admin">$img{'admin'}</a>~;
		$menubar .= $adminmenu." $msp";
	}

	if($username ne 'Guest') {
		$profilemenu = qq~<a href="$surl,v=memberpanel">$img{'profile'}</a>~;
		$loginmenu = qq~<a href="$surl,v=login,p=3">$img{'logout'}</a>~;
		$menubar .= $profilemenu." $msp";
	} else {
		$registermenu = qq~<a href="$surl,v=register">$img{'register'}</a>~;
		$loginmenu = qq~<a href="$surl,v=login">$img{'login'}</a>~;
		$menubar .= $registermenu." $msp";
	}
	$menubar .= $loginmenu;

	$template = $template || "$templates/template.html"; # Get the template to use

	fopen(FILE,"$template") || fopen(FILE,"$root/template.html") or $ebout .= "Template not found!<br><br>";
	@temp = <FILE>;
	fclose(FILE);
	$footer = '';
	foreach(@temp) {
		$_ =~ s/<blah v="\$(.+?)">/${$1}/gsi;
		if($_ =~ /(.*?)<blah main>(.*?)\Z/) { $sfoot = 1; $header .= $1; $footer .= $2; next; }
		if(!$sfoot) { $header .= $_; } else { $footer .= $_; }
	}

	$ebout .= $header;
	if($username eq 'Guest') {
		$new = '';
		if(!$gdisable) { $ebout .= $guestlogin; }
	}
	$ebout .= $URL{'v'} ne 'memberpanel' && $settings[41] && $new > 0 ? qq~<script language="Javascript1.3">if(window.confirm("$rtxt[59] $new $rtxt[58]")) { location = "$scripturl,v=memberpanel,a=pm"; }</script>~ : '';
}

sub footer {
	if($uextlog) { ExtClose(); }

	if($settings[1] ne '' && $logactive) {
		@temp = ();
		fopen(FILE,"+<$members/$username.dat"); # Get data into memory again to avoid errors
		@temp = <FILE>;
		chomp @temp;
		truncate(FILE,0);
		seek(FILE,0,0);
		$temp[28] = time;
		for($q = 0; $q < $usersetcount; $q++) { print FILE "$temp[$q]\n"; }
		fclose(FILE);
	}

	$ebout .= $footer;

	if($gzipen) {
		if($gzipen == 2) {
			open(GZIP,"| gzip -f");
			print GZIP $ebout;
			close(GZIP);
		} else {
			require Compress::Zlib;
			binmode STDOUT;
			print Compress::Zlib::memGzip($ebout);
		}
	} else { print $ebout; }
}

sub AL {
	my(@activelog,$buser,$luser,$ltime,@maxlog,$totactive);
	if($username eq 'Guest') {
		if($ENV{'HTTP_USER_AGENT'} =~ /(googlebot|inktomi|ask jeeves|lycos|ia_archiver|slurp|msnbot)/i) { $buser = lc($1).'<>'; }
			else { $buser = $ENV{'REMOTE_ADDR'}; }
	} else { $buser = $username; }

	fopen(FILE,"$prefs/Active.txt");
	@activelog = <FILE>;
	fclose(FILE);
	chomp @activelog;

	fopen(FILE,"$prefs/MaxLog.txt");
	@maxlog = <FILE>;
	fclose(FILE);
	chomp @maxlog;
	if($maxlog[0] < @activelog) {
		fopen(FILE,">$prefs/MaxLog.txt");
		print FILE @activelog."\n".time;
		fclose(FILE);
	}

	foreach(@activelog) {
		($luser,$ltime) = split(/\|/,$_);
		if(time-$ltime < (60*$activeuserslog) && lc($luser) ne lc($buser)) { $fileprint .= "$_\n"; }
		if(lc($luser) eq lc($buser)) { $keepon = 1; }
		$activemembers{$luser} = 1; # Keep user in memory for checking online status quickly
		++$totactive;
	}

	# Check server load ...
	if($serload && !$keepon && $totactive > $serload && $settings[4] ne 'Administrator') { return() if $URL{'v'} eq 'login'; error($rtxt[55]); }

	fopen(FILE,"+>$prefs/Active.txt");
	print FILE "$buser|".time."|$settings[18]|$URL{'v'}|$URL{'b'}|$URL{'m'}\n$fileprint";
	fclose(FILE);

	if($uextlog) { $ExtLog[7] = @activelog; }
}

sub error {
	my($ecode,$elog) = @_;
	my($ttime,$etype,$icon);
	if($ecode eq 'OPEN_ERROR'){ $ecode = $rtxt[56]; }
	$error = Format($ecode);
	$message = $error;
	&BC;
	$error = $message;
	if($error =~ /-admin-/ || $error =~ /-register-/) { &SpecialErrors; }
	$icon = 'ban';
	if($elog && $elog < 3) {
		$icon = 'warning';
		if($elog == 2) { $rtxt[18] = $rtxt[51]; $rtxt[19] = "<b>$rtxt[51] $rtxt[41]:</b> $rtxt[53]"; }
		$ewhat = "$rtxt[18] "; $esig = qq~</td></tr><tr><td height="2" class="border"></td></tr><tr><td><span class="smalltext"><img src="$images/warning_sm.gif"> $rtxt[19]</span>~;
		if($kelog) {
			chomp $!; chomp $error; chomp $errorurl;
			$errorurl = "Blah.pl?$ENV{'QUERY_STRING'}";
			$ttime = time;
			$specify = $! || '?';
			fopen(FILE,"+>>$prefs/ELog.txt");
			print FILE "$error|$specify|$ttime|$username|$errorurl\n";
			fclose(FILE);
		}
	}
	$ewhat .= $rtxt[41];
	if($elog == 3) {
		$icon = 'mem_main';
		$ewhat = $boardlock[33];
	}
	$error =~ s/$code|$messages|$members|$prefs|$boards/./gsi;  # Security: Change full paths to local
	if($root ne '.') { $error =~ s/$root/./gsi; }
	$title = $ewhat;
	$date_time = get_date(time);
	if($adminsloaded) { headerA(); } else { header(); }
	$ebout .= <<"EOT";
<table class="border" cellpadding="4" cellspacing="1" width="600" align="center">
 <tr>
  <td class="titlebg"><b><img src="$images/$icon.gif"> $title</b></td>
 </tr><tr>
  <td class="win"><span class="smalltext">$rtxt[49]</span></td>
 </tr><tr>
  <td class="win2"><table cellpadding="4"><tr><td>$error$esig</td></tr></table></td>
 </tr><tr>
  <td class="win"><table cellpadding="0" width="100%"><tr><td class="smalltext"><b><a href="javascript:history.go(-1)">&#171; $gtxt{'22'}</a></b></td><td align="right" class="smalltext"><a href="mailto:$eadmin">$rtxt[50]</a></td></tr></table></td>
 </tr>
</table>
EOT
	if($adminsloaded) { footerA(); } else { footer(); }
	exit;
}

sub redirect {
	($url2,$noexit) = $_[0];

	if($url eq '') { $url = $scripturl; }
	if($url2) { $url = $url2; }
	if($redirectfix == 1) { print qq~Content-type: text/html\n\n<html><head><meta http-equiv="refresh" content="0;URL=$url"></head></html>~; } 
	elsif($redirectfix == 2) { print "Refresh: 0;url=$url\n\n"; }
		else { print "Location: $url\n\n"; }
	if(!$noexit) { exit; }
}

sub get_date {
	my($date,$unbld,$personalized,$hourcnt) = @_;

	if($date eq '') { $date = time; }
	if($settings[1] eq '') { $settings[15] = $gtzone; }
	if($personalized) { ($sec,$min,$hour,$day,$month,$year,$week,$ydays,$dst) = localtime($date); }
		else { ($sec,$min,$hour,$day,$month,$year,$week,$ydays,$dst) = gmtime($date+(3600*($settings[15]+$settings[31]+$gtoff))); }
	$curmonth = $month;
	$ampm = "am";
	$year = 1900 + $year;
	if($min < 10) { $min = "0$min"; }
	$thour = $hour;
	if($hour == 12) { $ampm = "pm"; }
	if($hour == 0) { $hour = 12; }
	if($hour >= 13) {
		$hour = ($hour - 12);
		$ampm = "pm";
	}

	date_dis($unbld);
	return($ddis);
}

sub date_dis {
	my($unbld) = $_[0];
	my($ttdate,$tttime,$day1);
	if(!$settings[17]) { $settings[17] = $datedis; }
	if(!$settings[22]) { $settings[22] = $timedis; }
	$day1 = $day;

	$day = datest($day);

	if($settings[17] == 1) { $ttdate = "$days[$week], $months[$month] $day, $year"; }
	elsif($settings[17] == 2) { $ttdate = "$day1 $months[$month] $year"; }
	elsif($settings[17] == 3) { ++$month; $ttdate = "$month.$day1.$year"; }
	elsif($settings[17] == 4) { ++$month; $ttdate = "$day1/$month/$year"; }
	elsif($settings[17] == 5) { ++$month; $ttdate = "$year/$month/$day1"; }
		else { $ttdate = "$months[$month] $day, $year"; }
	if($settings[22] > 1) {
		if($hour < 10) { $hour = "0$hour"; }
		if($ampm eq 'pm' && $hour != 12) { $hour = $hour+12; }
		if($hour == 12 && $ampm eq 'am') { $hour = "00"; }
		if($sec < 10) { $sec = "0$sec"; }
		if($settings[22] == 2) { $tttime = "$hour:$min"; }
			else { $tttime = "$hour:$min:$sec"; }
	} elsif($settings[22] == 1) {
		if($sec < 10) { $sec = "0$sec"; }
		$tttime = "$hour\:$min\:$sec$ampm";
	} else { $tttime = "$hour:$min$ampm"; }
	if(!$unbld && $btod && ($kday == $day && $kmon == $month && $kyear == $year)) { $ttdate = "<b>$rtxt[20]</b>"; }

	$ddis = "$ttdate, $tttime";
	if($notime_date) { $ddis = "$months[$month] $day, $year"; }
	$notime_date = 0;
}

sub datest {
	my($day) = $_[0];
	if($day < 20 && $day > 10) { $day .= 'th'; }
	elsif($day % 10 == 1) { $day .= 'st'; }
	elsif($day % 10 == 2) { $day .= 'nd'; }
	elsif($day % 10 == 3) { $day .= 'rd'; }
		else { $day .= 'th'; }
	return($day);
}

sub ClickLog {
	my($clt,$lgtm,$maxtime,$maxed,$final,$rawprint,$nlog);
	if($uextlog) { ++$ExtLog[0]; }

	return() if !$eclick;

	$clt = time;
	$ref = $ENV{'HTTP_REFERER'} =~ /$realurl\/Blah.pl/ ? '' : $ENV{'HTTP_REFERER'};

	$maxtime = ($logcnt*60);
	$maxx = ($clt-$maxtime);

	fopen(RAWLOG,"+<$prefs/ClickLog.txt");
	while( <RAWLOG> ) {
		chomp;
		($lgtm) = split(/\|/,$_);
		$maxed = ($lgtm-$maxx);
		if($maxed > 0) { $rawprint .= "$_\n"; } else { last; }
	}
	seek(RAWLOG,0,0);
	truncate(RAWLOG,0);
	print RAWLOG "$clt|$ENV{'REMOTE_ADDR'}|$ref|$ENV{'QUERY_STRING'}|$ENV{'HTTP_USER_AGENT'}\n$rawprint";
	fclose(RAWLOG);
}

sub BCSmileys {
	my($smilecnt,$temper);
	if($simages2 ne '') { $temper = $simages; $simages = $simages2; }
	while($message =~ s~\Q::)\E~<img src="$simages/roll.gif" border="0">~ && $smilecnt < $gmaxsmils) { ++$smilecnt; }
	while($message =~ s~\Q:)\E~<img src="$simages/smiley.gif" border="0">~ && $smilecnt < $gmaxsmils) { ++$smilecnt; }
	while($message =~ s~\Q:-)\E~<img src="$simages/smiley.gif" border="0">~ && $smilecnt < $gmaxsmils) { ++$smilecnt; }
	while($message =~ s~\Q:D\E~<img src="$simages/lol.gif" border="0">~ && $smilecnt < $gmaxsmils) { ++$smilecnt; }
	while($message =~ s~\Q:-D\E~<img src="$simages/lol.gif" border="0">~ && $smilecnt < $gmaxsmils) { ++$smilecnt; }
	while($message =~ s~\Q:B\E~<img src="$simages/blush.gif" border="0">~ && $smilecnt < $gmaxsmils) { ++$smilecnt; }
	while($message =~ s~\Q??)\E~<img src="$simages/huh.gif" border="0">~ && $smilecnt < $gmaxsmils) { ++$smilecnt; }
	while($message =~ s~\Q:'(\E~<img src="$simages/cry.gif" border="0">~ && $smilecnt < $gmaxsmils) { ++$smilecnt; }
	while($message =~ s~\Q:X\E~<img src="$simages/lipsx.gif" border="0">~ && $smilecnt < $gmaxsmils) { ++$smilecnt; }
	while($message =~ s~\Q:o\E~<img src="$simages/shock.gif" border="0">~ && $smilecnt < $gmaxsmils) { ++$smilecnt; }
	while($message =~ s~\Q:K)\E~<img src="$simages/kiss.gif" border="0">~ && $smilecnt < $gmaxsmils) { ++$smilecnt; }
	while($message =~ s~(\A|\W);D~$1<img src="$simages/grin.gif" border="0">~ && $smilecnt < $gmaxsmils) { ++$smilecnt; }
	while($message =~ s~\Q;-D\E~<img src="$simages/grin.gif" border="0">~ && $smilecnt < $gmaxsmils) { ++$smilecnt; }
	while($message =~ s~\Q8)\E~<img src="$simages/cool.gif" border="0">~ && $smilecnt < $gmaxsmils) { ++$smilecnt; }
	while($message =~ s~\Q:P\E~<img src="$simages/tongue.gif" border="0">~ && $smilecnt < $gmaxsmils) { ++$smilecnt; }
	while($message =~ s~\Q\&gt;:(\E~<img src="$simages/angry.gif" border="0">~ && $smilecnt < $gmaxsmils) { ++$smilecnt; }
	while($message =~ s~\Q:-/\E~<img src="$simages/undecided.gif" border="0">~ && $smilecnt < $gmaxsmils) { ++$smilecnt; }
	while($message =~ s~\Q:(\E~<img src="$simages/sad.gif" border="0">~ && $smilecnt < $gmaxsmils) { ++$smilecnt; }
	while($message =~ s~\Q:-(\E~<img src="$simages/sad.gif" border="0">~ && $smilecnt < $gmaxsmils) { ++$smilecnt; }
	while($message =~ s~(\A|\W)\Q;)\E~$1<img src="$simages/wink.gif" border="0">~ && $smilecnt < $gmaxsmils) { ++$smilecnt; }
	while($message =~ s~(\A|\W)\Q;-)\E~$1<img src="$simages/wink.gif" border="0">~ && $smilecnt < $gmaxsmils) { ++$smilecnt; }
	if($upbc) {
		if($temper ne '') { $simages = $temper; }
		if(!$smileysopen) {
			fopen(FILE,"$prefs/smiley.txt");
			@smiley = <FILE>;
			fclose(FILE);
			chomp @smiley;
			$smileysopen = 1;
		}

		foreach(@smiley) {
			($smile,$slink) = split(/\|/,$_);
			$message =~ s~\Q$smile\E~<img src="$simages/$slink" border="0">~g;
		}
	}
}

sub Quote {
	my($qdate,$qmessage,$repaste,$qname);
	if($2) {
		$qdate = get_date($3);
		$userset{$1}->[1] = '';
		loaduser($1);
		if($userset{$1}->[1] ne '') { $qname = qq~<a href="$scripturl,v=memberpanel,a=view,u=$1">$userset{$1}->[1]</a>~; }
			else { $qname = $1; }
		$quotedfrom = qq~$rtxt[22] <b>$qname</b>, $rtxt[23] <b>$qdate</b> $rtxt[21] <b><a href="$2" title="$rtxt[25]">$rtxt[26]</a></b>~;
		$qmessage = $4;
	} else {
		$quotedfrom = "<b>$rtxt[27]</b>";
		$qmessage = $1;
	}

	$repaste = qq~
<br><table width="95%" class="border" cellpadding="0" cellspacing="1" align="center">
 <tr>
  <td><span class="smalltext"><div class="win" style="padding: 8px">$quotedfrom</div><div class="win2" style="padding: 8px">$qmessage</div></span></td>
 </tr>
</table>
~;
	return($repaste);
}

sub SizedURL {
	$url = $_[1];
	if(length($url) > 100 && $URL{'v'} ne 'print') { $url = substr($url,0,30).'.....'.substr($url,length($url)-20); }
	return(qq~$_[0]<a href="$_[1]" target="_blank">$url</a>~);
}

sub Code {
	if($BCLoad == 0) { return; }

	%translation = (
		'('  => '&#040;',
		'D'  => '&#068;',
		')'  => '&#041;',
		'-'  => '&#045;',
		'/'  => '&#047;',
		':'  => '&#058;',
		'?'  => '&#063;',
		'['  => '&#091;',
		'\\' => '&#092;',
		']'  => '&#093;',
		'.'  => '&#046;'
	);
	$codelander = $1;
	# $codelander =~ s/&amp;/&/g;
	$codelander =~ s/([\:\(\)\-\/\?\[\]\\\.D])/$translation{$1}/g;
	if(split(/<br>/,$codelander) > 20) { $height = "200px"; } else { $height = 'none'; }

	return(qq~<table class="border" cellpadding="5" cellspacing="1" width="95%" align="center"><tr><td class="win2" align="center"><span class="smalltext"><b>$rtxt[57]</b></span></td></tr><tr><td class="win"><div style="overflow: auto; width: 100%; height: $height;"><span class="smalltext" face="Courier New">$codelander</span></div></td></tr></table>~);
}

sub BC {
	if($html && $nosmile < 2) {
		$message =~ s/&quot;/"/g;
		if($htmls[0] ne '') {
			foreach(@htmls) {
				$close = $open = 0;
				while($message =~ s~\&lt;$_(\w? || .*?)\&gt;~<$_$1$2>~) { ++$open; }
				while($message =~ s~\&lt;/$_(.*?)\&gt;~</$_$1>~) { ++$close; }
				while($open > $close) { $message .= "</$_>"; ++$close; } # Close open tags (security)
			}
		} else { $message =~ s~\&lt;(.*?)\&gt;~<$1>~gsi; }
	}
	$message =~ s~\[code\](.+?)\[/code\]~&Code~esgi;

	$message =~ s~\[quote by=(.+?) link=(.+?) date=(.+?)\](.+?)\[/quote\]~&Quote~gsie;
	$message =~ s~\[quote\](.+?)\[/quote\]~&Quote~gsie;

	if($al) {
		$message =~ s~([^\w\"\=\[\]]|[\n\b]|\A)\\*(\w+://[\w\~\.\;\:\$\-\+\!\*\?/\=\&\@\#\%]+\.[\w\~\;\:\$\-\+\!\*\?/\=\&\@\#\%,.]+[\w\~\;\:\$\-\+\!\*\?/\=\&\@\#\%])~SizedURL($1,$2)~eisg;
		$message =~ s~([^\"\=\[\]/\:\.(\://\w+)]|[\n\b]|\A|[\<\n\b\>])\\*(www\.[^\.][\w\~\.\;\:\$\-\+\!\*\?/\=\&\@\#\%]+\.[\w\~\;\:\$\-\+\!\*\?/\=\&\@\#\%\,]+[\w\~\;\:\$\-\+\!\*\?/\=\&\@\#\%])~SizedURL($1,"http://$2")~eisg;
	}
	if($BCSmile && ($nosmile != 1 && $nosmile != 3)) { &BCSmileys; }

	if(!$BCLoad) { $message = CensorList($message); return(); } # BC is disabled

	$message =~ s~\[s\](.*?)\[/s\]~<s>$1</s>~gsi;
	$message =~ s~\[b\](.*?)\[/b\]~<b>$1</b>~gsi;
	$message =~ s~\[i\](.*?)\[/i\]~<i>$1</i>~gsi;
	$message =~ s~\[u\](.*?)\[/u\]~<u>$1</u>~gsi;
	$message =~ s~\[size=([1-9][^\s\n<>]*?)\](.*?)\[/size\]~<font size="$1" style="line-height: normal">$2</font>~gsi;

	$message =~ s~\[img\](http|ftp|mms|https)://(.[^\s\n<>]+?)\[/img\]~<img src="$1://$2" border="0">~gsi;
	$message =~ s~\[img width=([1-9][^\s]*?) height=([1-9][^\s]*?)\](http|ftp|mms|https)://(.[^\s\n<>]+?)\[/img\]~<img src="$3://$4" width="$1" height="$2" border="0">~gsi;
	$message =~ s~\[img width=([1-9][^\s]*?) height=([1-9][^\s]*?) align=(right|left)\](http|ftp|mms|https)://(.[^\s\n<>]+?)\[/img\]~<img src="$4://$5" width="$1" height="$2" border="0" style="float:$3" align="float:$3">~gsi;
	$message =~ s~\[img align=(right|left)\](http|ftp|mms|https)://(.[^\s\n<>]+?)\[/img\]~<img src="$2://$3" border="0" style="float:$1" align="float:$1">~gsi;

	$message =~ s~\[url=(http|ftp|mms|https)://(.[^\s\n<>]+?)\](.+?)\[/url\]~<a href="$1://$2" title="$2" target="_new">$3</a>~gsi;
	$message =~ s~\[url=www\.(.[^\s\n<>]+?)\](.+?)\[/url\]~<a href="http://www.$1" title="www.$1" target="_new">$2</a>~gsi;
	$message =~ s~\[url\](http|ftp|mms|https)://(.[^\s\n<>]+?)\[/url\]~SizedURL('',"$1://$2")~egsi;
	$message =~ s~\[url\]www\.(.[^\s\n<>]+?)\[/url\]~SizedURL('',"http://www.$1")~egsi;

	$message =~ s~\[flash width=([0-9]*?) height=([0-9]*?) url=(http|ftp|mms|https)://(.[^\s\n<>]+?)\]~<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="$1" height="$2"><param name="movie" value="$3://$4"><param name="play" value="true"><param name="loop" value="true"><param name="quality" value="high"><embed src="$3://$4" width="$1" height="$2" play="true" loop="true" quality="high"></embed></object>~gsi;

	$message =~ s~\[pre\](.+?)\[/pre\]~<pre>$1</pre>~gsi;
	if($message =~ /\[list(.*?)\](.*?)\[\/list\]/) {
		$message =~ s~\[list\](.+?)\[/list\]~<ul>$1</ul>~gsi;
		$message =~ s~\[list=([a-zA-Z|1]+?)\](.+?)\[/list\]~<ol type="$1">$2</ol>~gsi;
		$message =~ s~\[\*\]~<li>~gsi;
	}

	$message =~ s~\[left\](.+?)\[/left\]~<div align="left">$1</div>~gsi;
	$message =~ s~\[right\](.+?)\[/right\]~<div align="right">$1</div>~gsi;
	$message =~ s~\[center\](.+?)\[/center\]~<center>$1</center>~gsi;
	$message =~ s~\[justify\](.+?)\[/justify\]~<div align="justify">$1</div>~gsi;
	$message =~ s~\[face=(.+?)\](.+?)\[/face\]~<font face="$1">$2</font>~gsi;
	$message =~ s~\[color=(.+?)\](.+?)\[/color\]~<font color="$1">$2</font>~gsi;

	$message =~ s~\[glow=(.+?) strength=(.+?)\](.*?)\[/glow\]~<span style="filter:glow(color=$1, strength=$2);height: 1px;">$3</span>~gsi;
	$message =~ s~\[glow=(.+?)\](.*?)\[/glow\]~<span style="filter:glow(color=$1, strength=5);height: 1px;">$2</span>~gsi;
	$message =~ s~\[shadow=(.+?) strength=(.+?)\](.*?)\[/shadow\]~<span style="filter:shadow(color=$1, strength=$2);height: 1px;">$3</span>~gsi;
	$message =~ s~\[shadow=(.+?)\](.*?)\[/shadow\]~<span style="filter:shadow(color=$1, strength=5);height: 1px;">$2</span>~gsi;

	$message =~ s~\[hr\]~<hr size="1" width="100%" color="$color{'border'}">~gsi;
	$message =~ s~\[sub\](.+?)\[/sub\]~<sub>$1</sub>~gsi;
	$message =~ s~\[sup\](.+?)\[/sup\]~<sup>$1</sup>~gsi;
	$message =~ s~\[mail\](.+?)\[/mail\]~<a href="mailto:$1">$1</a>~gsi;
	$message =~ s~\[mail=(.+?)\](.+?)\[/mail\]~<a href="mailto:$1">$2</a>~gsi;
	$message =~ s~\[move\](.+?)\[/move\]~<marquee>$1</marquee>~gsi;

	if($message =~ /\[table\](.+?)\[\/table\]/) {
		$message =~ s/<br>/\t/gsi;
		while($message =~ s~\t{0,1}\[table\]\t{0,1}(.+?)\t{0,1}\[\/table\]\t{0,1}~<table>$1</table>~sgi) {
			while($message =~ s~<table>\t{0,1}(.*?)\t{0,1}\[tr\]\t{0,1}(.*?)\t{0,1}\[/tr\]\t{0,1}(.*?)\t{0,1}</table>~<table>$1<tr>$2</tr>$3</table>~sgi) { } # Loop until rows are done.
			while($message =~ s~<table>(.*?)<tr>\t{0,1}(.*?)\t{0,1}\[td\]\t{0,1}(.*?)\t{0,1}\[/td\]\t{0,1}(.*?)\t{0,1}</tr>(.*?)</table>~<table>$1<tr>$2<td>$3</td>$4</tr>$5</table>~sig) { } # Loop until cols are done.
		}
		$message =~ s/\t/<br>/gsi;
	}
	$message = CensorList($message);
}

sub smail {
	my($subject,$message,$wholemessage,$tothis);
	my($to,$subject,$message,$from) = @_;
	if(!$to || !$subject || !$message) { return(-1); }
	if($to !~ /\A([0-9A-Za-z\._\-]{1,})@([0-9A-Za-z\._\-]{1,})+\.([0-9A-Za-z\._\-]{1,})+\Z/) { return(-1); }

	$message =~ s/\cM//g;
	$message =~ s/\n/<br>/gsi;
	$message =~ s/  /&nbsp; /gsi;
	$message =~ s/&lt;br&gt;/<br>/gsi;
	$message =~ s/<br>/<br>\n/gsi;
	$message =~ s/bgcolor="(.+?)"/bgcolor="#FFFFFF"/gsi;
	$emailsig = Unformat($emailsig);
	if($emailsig) { $message .= qq~<span class="smalltext"><br><br><hr size="1" color="#C0C0C0">$emailsig</span>~; }

	$wholemessage = <<"EOT";
<html>
<body bgcolor="#FFFFFF">
<font face="Verdana,Arial" color="#000000" size="2">$message</font>
</body>
</html>
EOT
	if($settings[2] ne '' && $from eq '') { $eadmin = $settings[2]; }
	if($from eq '') { $from = $regto; }
	if($eadmin eq '') { $eadmin = "noreply\@noreply.noreply"; }
	if($mailuse == 1) {
		open(MAIL,"| $smaill -t");
		print MAIL "MIME-Version: 1.0\n";
		print MAIL "Content-Type: text/html; charset=\"ISO-8859-1\"\n";
		print MAIL "Content-Transfer-Encoding: 7bit\n";
		print MAIL "X-Forum-System: e-blah, $version, copyright 2001-2003\n";
		print MAIL "X-Username: $username\n";
		print MAIL "X-Sent-From-IP: $ENV{'REMOTE_ADDR'}\n";
		print MAIL "To: $to\n";
		print MAIL "From: \"$from\" <$eadmin>\n";
		print MAIL "Subject: $subject\n\n";
		print MAIL "$wholemessage";
		print MAIL "\n\n";
		close(MAIL);
	} elsif($mailuse == 2) {
		eval q{
			use Net::SMTP;
			my $smtp = Net::SMTP->new($mailhost);
			if($mailauth) { $smtp->auth($mailusername,$mailpassword); }
			$smtp->mail($eadmin);
			$smtp->to($to);
			$smtp->data();
			$smtp->datasend("MIME-Version: 1.0\n");
			$smtp->datasend("Content-Type: text/html; charset=ISO-8859-1\n");
			$smtp->datasend("Content-Transfer-Encoding: 7bit\n");
			$smtp->datasend("X-Forum-System: e-blah, $version, copyright 2001-2003\n");
			$smtp->datasend("X-Username: $username\n");
			$smtp->datasend("X-Sent-From-IP: $ENV{'REMOTE_ADDR'}\n");
			$smtp->datasend("To: $to\n");
			$smtp->datasend("From: \"$from\" <$eadmin>\n");
			$smtp->datasend("Subject: $subject\n\n");
			$smtp->datasend("$wholemessage");
			$smtp->datasend("\n");
			$smtp->dataend();
			$smtp->quit;
		};
		if($@) { &error("[b]Net::SMTP Error[/b]\n\n$@",2); }
	}

	if($maildebug) {
		$time = time;
		fopen(OPENFILE,">$root/$time\_mailed.txt");
		print OPENFILE "To: $to\nFrom: \"$from\" <$eadmin>\nSubject: $subject\n\n$wholemessage";
		fclose(OPENFILE);
	}

	return(1); # Clean return
}

sub ExtClose { # Save extensive log ..
	my($save,$u,$day,$month,$year);

	($t,$t,$t,$day,$month,$year) = localtime(time);
	$save = $day.$month.$year;

	fopen(WRITEEXT,"+<$prefs/BHits/$save.txt");
	@ExtensiveLogs = <WRITEEXT>;
	chomp @ExtensiveLogs;
	if($ExtensiveLogs[0] eq '') {
		fclose(WRITEEXT);
		fopen(WRITEEXT,"+>$prefs/BHits/$save.txt");
	}
	seek(WRITEEXT,0,0);
	truncate(WRITEEXT,0);
	for($u = 0; $u < @ExtensiveLogs; $u++) {
		if($ExtensiveLogs[$u] eq '') { $ExtensiveLogs[$u] = 0; }
		if($u != 7) { $PLog[$u] = ($ExtLog[$u]+$ExtensiveLogs[$u]) || 0; }
	}
	if($ExtensiveLogs[7] < $ExtLog[7]) { $PLog[7] = $ExtLog[7]; } else { $PLog[7] = $ExtensiveLogs[7]; }
	print WRITEEXT "$PLog[0]\n$PLog[1]\n$PLog[2]\n$PLog[3]\n$PLog[4]\n$PLog[5]\n".time."\n$PLog[7]";
	fclose(WRITEEXT);
}

sub Mark {
	if($username eq 'Guest') { &error("$rtxt[28]-register-"); }
	if($URL{'l'} eq 'bindex') { $l = "AllBoards"; }
	if($URL{'l'} eq 'bdis') { $l = "AllRead_$URL{'b'}"; $url = $surl; }

	fopen(FILE,"$members/$username.log");
	@log = <FILE>;
	fclose(FILE);
	chomp @log;
	fopen(FILE,"+>$members/$username.log");
	print FILE "$l|".time."\n";
	foreach(@log) {
		($data) = split(/\|/,$_);
		if($data ne $l) { print FILE "$_\n"; }
	}
	fclose(FILE);
	redirect();
}

sub MakeComma {
	$ctobe = $_[0];
	if(!$nocomma) { return($ctobe); }
	if(!$ctobe) { return(0); }
	while($ctobe =~ s/^([-+]?\d+)(\d{3})/$1$nocomma$2/) { } # Loop and make commas
	return($ctobe);
}

sub SpecialErrors {
	$loginout = $username ne 'Guest' ? ",v=memberpanel,a=view,u=" : ",v=login,u=";

	if($error =~ s/(-admin-|-register-)//) {
		if($1 eq '-register-') { $reg = 1; }
	}

	for($x = 1; $x < 3; ++$x) {
		$tryuser = $Blah{"$cooknme\Logout$x"};
		if($tryuser) {
			loaduser($tryuser);
			if($userset{$tryuser}->[4] eq 'Administrator') { $adminerror .= qq~<a href="$scripturl$loginout$tryuser">$userset{$tryuser}->[1]</a><br>~; }
			if($userset{$tryuser}->[1] ne '') { $addreg .= qq~<a href="$scripturl$loginout$tryuser">$rtxt[29] $userset{$tryuser}->[1]</a><br>~; }
		}
	}
	$adminerror =~ s/<br>\Z//;
	$addreg =~ s/<br>\Z//;
	if($adminerror && !$reg) { $error .= qq~<br><br><span class="smalltext"><b>$rtxt[30]:</b><br>$adminerror</span>~; }
	if($reg) {
		if($addreg) { $addreg = qq~<br><br><b>$rtxt[30]:</b><br>$addreg~; }
		$error .= qq~<br><br><span class="smalltext"><b>$rtxt[32]:</b><br><a href="$scripturl,v=register">$rtxt[33]</a>.<br><a href="$scripturl,v=login">$rtxt[34]</a>.$addreg</span>~;
	}
}

sub ShowGuest {
	CoreLoad('Login',1);

	$guestlogin = <<"EOT";
<table cellpadding="4" cellspacing="1" class="border" width="450" align="center">
 <tr>
  <td class="titlebg"><span class="smalltext"><b>$rtxt[35]</b></span></td>
 </tr><tr><form action="$surl,v=login,p=2" method="POST">
  <td class="win">
   <table cellpadding="2" cellspacing="0" width="100%">
    <tr>
     <td><span class="smalltext"><b>$rtxt[36]:</b></span></td>
     <td colspan="2"><input type="text" class="textinput" name="username" size="20" tabindex="56"></td>
     <td><span class="smalltext"><b><a href="$scripturl,v=register">$logintxt[3]</a></b></span></td>
    </tr><tr>
     <td><span class="smalltext"><b>$rtxt[37]:</b></span></td>
     <td><input type="password"  class="textinput" name="password" size="20" tabindex="57"></td>
     <td>&nbsp; &nbsp;<input type="hidden" name="days" value="forever"><input type="hidden" name="redirect" value="$URL{'b'},m=$URL{'m'}"><input type="submit" class="button" value="&nbsp;&nbsp;$rtxt[38]&nbsp;&nbsp;" tabindex="58"></td>
     <td><span class="smalltext"><b><a href="$scripturl,v=login,p=forgotpw">$logintxt[5]</a></b></span></td>
    </tr>
   </table>
  </td>
 </form></tr>
</table><br>
EOT
}

sub calage {
	($bday) = @_;
	($t,$t,$t,$td,$tm,$ty) = localtime(time);
	($m,$d,$y) = split("/",$bday);
	$ty += 1900;
	if($y < 1900) { $y += 1900; }
	$age = ($ty-$y)-1;
	if((($tm+1) == $m && $td >= $d) || ($tm+1) > $m) { ++$age; }
	if(!$bday) { $age = 0; } else { --$m; $ageurl = "$scripturl,v=cal,month=".($m+1); }
	return($age);
}

sub CheckDays {
	($qmon) = $_[0];
	$number = (int($qyear/4)*4);
	if($qmon eq 0 || $qmon eq 2 || $qmon eq 4 || $qmon eq 6 || $qmon eq 7 || $qmon eq 9 || $qmon eq 11) { $days = 31; }
	elsif($qmon eq 3 || $qmon eq 5 || $qmon eq 8 || $qmon eq 10) { $days = 30; }
	elsif($qmon eq 1) { $days = $qyear == $number ? 29 : 28; }
	return($days);
}

sub Highlight {
	$_[0] =~ s/<br>/<br>\n/gsi;
	@smallm = split(/(<.+?>)\s/,$_[0]);
	$_[0] = '';

	foreach(@smallm) {
		if($_ !~ /[ht|f]tp/ && $_ !~ /<(.*?)>/) {
			foreach $hl (@lights) {
				if($hl eq '') { next; }
				$_ =~ s~(\Q$hl\E)~<span style="background-color:yellow; color: black;"><b>$1</b></span>~isg;
			}
		}
		$_[0] .= "$_ ";
	}
}

sub VerifyBoard { # This simply verifies that the user requesting, has authorization to get the data requested
	my($id,$nme,$grps,$input,$memgrp,$catbase,@binfo,$grp);

	foreach $catbase (@catbase) {
		($nme,$id,$grps,$input) = split(/\|/,$catbase);

		if($grps ne '') {
			$con = '';
			@mgrps = split(",",$grps);
			foreach $grp (@mgrps) {
				if($grp eq $settings[4]) { $con = 1; }
				if($grp eq 'member' && $username ne 'Guest') { $con = 1; }
			}
			if(!$con && $settings[4] ne 'Administrator') { next; }
		}
		@input2 = split("/",$input);
		foreach $onlist (@input2) {
			$boardson{$onlist} = 1;
		}

		$catallow{$id} = 1;
		++$catcounter;
	}

	foreach $boardindex (@boardbase) {
		($id2,$t,$t,$t,$t,$t,$t,$binfo[6],$t,$t,$binfo[9]) = split("/",$boardindex);

		if(!$boardson{$id2}) { next; }
		if($binfo[6] ne '') { next; }

		if($binfo[9] ne '') {
			$con = '';
			@mgrps = split(",",$binfo[9]);
			foreach $grp (@mgrps) {
				if($grp eq $settings[4]) { $con = 1; }
				if($grp eq 'member' && $username ne 'Guest') { $con = 1; }
			}
			if($con eq '' && $settings[4] ne 'Administrator') { next; }
		}
		$boardallow{$id2} = 1;
		++$boardcounter;
	} # Command outputs: $[cat|board]allow{id} and $[cat|board]counter
}

sub SetCookie { # Gets cookie data, and prints it
	my($cookname,$cookvalue,$cookexp) = @_;
	my($cookprint);

	print "Set-Cookie: $cookname=$cookvalue; path=/; expires=$cookexp GMT\n";
	return(1);
}
1;