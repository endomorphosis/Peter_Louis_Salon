################################################
# E-Blah Bulliten Board Systems       Platinum #
################################################
# Copyright (c) 2001 - 2004 e-blah!            #
# All Rights Reserved.                         #
################################################

CoreLoad('Invite',1);

sub Invite {
	if($invfri != 1) {
		if($username eq 'Guest') { &error($gtxt{'noguest'}); }
		&is_member;
	}
	elsif($URL{'id'} ne '') { &LinkMeUp; }
	elsif($URL{'p'} eq '') { &InviteLong; }
		else { &Finalize; }
}

sub InviteBI { # This is only for the board index!
	$ebout .= <<"EOT";
    <tr>
     <td class="win" align="center"><img src="$images/invite.gif"></td>
     <td class="win">
      <table cellspacing="0" cellpadding="1" width="100%">
       <tr><form action="$scripturl,v=invite" method="post" name="post">
        <td align="right"><span class="smalltext"><b>$invite[3]:</b></span></td>
        <td><input type="text" class="textinput" name="friendname" size="25"></td>
        <td rowspan="2"><input type="submit" class="button" value=" $invite[6] "></td>
       </tr><tr>
        <td align="right"><span class="smalltext"><b>$gtxt{'23'}:</b></span></td>
        <td><input type="text" class="textinput" name="friendmail" size="25"></td>
       </form></tr>
      </table>
     </td>
    </tr>
EOT
}

sub InviteLong {
	if($username eq 'Guest') { &error("$invite[1]-register-"); }
	$title = $invite[6];
	&header;

	if($boardbase[0] ne '') { $dirops .= qq~<option value="|boardindex|">--------------------</option>~; }
	foreach(@catbase) {
		($t,$t,$access,$bhere) = split(/\|/,$_);
		if($access) { next; }
		@bdsh = split("/",$bhere);
		foreach $hh (@bdsh) {
			foreach $bbase (@boardbase) {
				($bid,$t,$t,$name) = split("/",$bbase);
				if($bid eq $URL{'b'} || $bid ne $hh) { next; }
				$dirops .= qq~<option value="$bid">&nbsp;-&#187; $name</option>~;
				last;
			}
		}
	}

	$ebout .= <<"EOT";
<table cellpadding="4" cellspacing="1" class="border" width="750" align="center">
 <tr>
  <td class="titlebg"><b><img src="$images/recommend_sm.gif"> $title</b></td>
 </tr><tr>
  <td class="catbg"><span class="smalltext"><b>$invite[8]</b></span></td>
 </tr><tr>
  <td class="win">
   <table cellspacing="0" cellpadding="4" width="100%">
    <tr><form action="$scripturl,v=invite,p=2" method="post" name="post">
     <td align="right" width="25%"><b>$invite[3]:</b></td>
     <td width="25%"><input type="text" class="textinput" name="friendname" size="25" value="$FORM{'friendname'}"></td>
     <td align="right" width="25%"><b>$invite[4]:</b></td>
     <td width="25%"><input type="text" class="textinput" name="yourname" value="$settings[1]" size="25"></td>
    </tr><tr> 
     <td align="right"><b>$invite[5] $gtxt{'23'}:</b></td>
     <td><input type="text" class="textinput" name="friendmail" value="$FORM{'friendmail'}" size="25"></td>
     <td align="right"><b>$gtxt{'23'}:</b></td>
     <td><input type="text" class="textinput" name="yourmail" value="$settings[2]" size="25"></td>
    </tr>
   </table>
  </td>
 </tr><tr>
  <td class="catbg"><span class="smalltext"><b>$invite[12]</b></span></td>
 </tr><tr>
  <td class="win2">
   <table cellspacing="0" cellpadding="2" width="100%">
    <tr>
      <td width="5%" align="center"><input type="checkbox" class="checkboxinput" name="emailme" value="1" checked></td>
      <td width="95%" colspan="2"><b>$invite[13]</b><br><span class="smalltext">$invite[14]</span></td>
    </tr><tr>
     <td></td><td><b>$invite[15]</b><br><span class="smalltext">$invite[16]</span></td>
     <td><select name="board"><option value="|boardindex|" selected>-&#187; $invite[17]</option>$dirops</select></td>
    </tr>
   </table>
  </td>
 </tr><tr>
  <td class="win">
   <table cellspacing="0" cellpadding="4" width="100%" align="center">
    <tr> 
     <td align="right" width="40%"><b>$invite[18]:</b></td>
     <td width="60%"><input type="text" class="textinput" name="subject" value="$invite[19]" size="30" maxlength="30"></td>
    </tr><tr>
     <td align="right" valign="top" width="40%"><b>$invite[20]:</b></td>
     <td width="60%"><textarea name="permessage" wrap="virtual" rows="4" cols="45">$invite[21]</textarea><br>$invite[44]</td>
    </tr>
   </table>
  </td>
 </tr><tr>
  <td class="win2" align="center"><input type="submit" class="button" value=" $invite[22] "></td>
 </form></tr>
</table>
EOT
	&footer;
	exit;
}

sub Finalize {
	if($username eq 'Guest') { &error("$invite[1]-register-"); }
	&error($invite[32]) if($FORM{'friendmail'} eq '');
	&error($invite[33]) if($FORM{'friendname'} eq '');
	&error($invite[34]) if($FORM{'permessage'} eq '');
	&error($invite[35]) if($FORM{'subject'} eq '');
	&error($invite[36]) if($FORM{'board'} eq '');

	if(length($FORM{'permessage'}) > 499) { &error($gtxt{'bfield'}); }
	if(length($FORM{'subject'}) > 29) { &error($gtxt{'bfield'}); }

	if($FORM{'yourname'} eq '') { $FORM{'yourname'} = $settings[1]; }
	if($FORM{'yourmail'} eq '') { $FORM{'yourmail'} = $settings[2]; }

	$referid = time;
	$nofnd = 1;
	foreach(@boardbase) {
		($boardids) = split("/",$_);
		if($FORM{'board'} eq $boardids) { $referboard = qq~$surl,b=$boardids,v=mindex~; $nofnd = 0; last; }
	}
	if($nofnd) { $referboard = qq~$surl~; }
	error($invite[32]) if($FORM{'friendmail'} !~ /\A([0-9A-Za-z\._\-]{1,})@([0-9A-Za-z\._\-]{1,})+\.([0-9A-Za-z\._\-]{1,})+\Z/);
	if($FORM{'yourmail'} !~ /\A([0-9A-Za-z\._\-]{1,})@([0-9A-Za-z\._\-]{1,})+\.([0-9A-Za-z\._\-]{1,})+\Z/) { $FORM{'yourmail'} = $settings[2]; }

	# Write to file
	$friendmail = Format($FORM{'yourmail'});
	$friendemail = $FORM{'emailme'} ? 1 : 0;
	$friendname = Format($FORM{'friendname'});
	fopen(FILE,"+>>$prefs/Refer.txt");
	print FILE "$referid|$referboard|$friendmail|$friendemail|$friendname|$username\n";
	fclose(FILE);

	# Send message
	$message2 = Format($FORM{'permessage'});
	$subject = Format($FORM{'subject'});
	if(!$subject) { $subject = $invite[37]; }
	$sendto = Format($FORM{'friendmail'});
	$message = <<"EOT";
$message2

$invite[24]:
<a href="$rurl,v=invite,id=$referid">$rurl,v=invite,id=$referid</a>
EOT
	smail($sendto,$subject,$message,$friendmail);

	&redirect;
}

sub LinkMeUp {
	fopen(FILE,"$prefs/Refer.txt");
	while(<FILE>) {
		chomp;
		($followid,$returnto,$emailof,$send,$thisname) = split(/\|/,$_);
		if($followid eq $URL{'id'}) {
			if($send) {
				$timedate = get_date(time);
				$message = qq~$invite[31] "$thisname" $invite[26]~;
				smail($emailof,$invite[27],$message);
			}
			$addto = "$_\n";
			$url = $returnto;
		} else { $readd .= "$_\n"; }
	}
	fclose(FILE);
	if($url eq '') { redirect($scripturl); } # May have already been invited
	fopen(FILE,"+>>$prefs/ReferLog.txt");
	print FILE $addto;
	fclose(FILE);

	fopen(FILE,"+>$prefs/Refer.txt");
	print FILE $readd;
	fclose(FILE);
	redirect();
}

sub Referals {
	if($URL{'p'} eq 'remove') {
		if($URL{'pp'}) { unlink("$prefs/Refer.txt"); } else { unlink("$prefs/ReferLog.txt"); }
		redirect("$scripturl,v=admin,r=3");
	}
	fopen(FILE,"$prefs/Refer.txt");
	while(<FILE>) { chomp; push(@refer,"1|$_"); }
	fclose(FILE);

	fopen(FILE,"$prefs/ReferLog.txt");
	while(<FILE>) { chomp; push(@refer,"2|$_"); }
	fclose(FILE);

	$title = $invite[38];
	&headerA;
	$ebout .= <<"EOT";
<script language="javascript">
<!-- //
function clear(url) {
 if(window.confirm("$invite[39]")) { location = url; }
}
// -->
</script>
<table class="border" cellpadding="4" cellspacing="1" width="700" align="center">
 <tr>
  <td class="titlebg" colspan="5"><b><img src="$images/recommend_sm.gif"> $title</b></td>
 </tr><tr>
  <td class="win">
   <table cellpadding="4" cellspacing="0" width="100%">
EOT
	foreach(sort{$a <=> $b} @refer) {
		($t,$followid,$returnto,$emailof,$send,$thisname,$inviteusername) = split(/\|/,$_);
		if($t ne $current) {
			if($counter == 1) { $ebout .= "</td><td>&nbsp;</td></tr><tr>"; $counter = 0; }
			if($counter == 2) { $ebout .= "</tr><tr>\n"; $counter = 0; }
			$current = $t;
			if($t == 1) { $t = $invite[40]; $remove = 1; }
			if($t == 2) { $t = $invite[41]; $remove = 1; }
			$ebout .= <<"EOT";
$next<td colspan="2" class="win2"><span class="smalltext"><b>$t</b></span></td><tr>
EOT
			$c = 0;
			$next = '';
		}
		++$c;
		$getdate = get_date($followid);
		if($c > 2) { $o = "<br>"; }
		$serviceen = '';
		if($send) { $serviceen = qq~<b>$invite[43]</b>~; }

		loaduser($inviteusername);
		if($userset{$inviteusername}->[1] ne '') { $inviteusername = qq~<a href="$scripturl,v=memberpanel,a=view,u=$inviteusername" target="_parent">$userset{$inviteusername}->[1]</a>~; }
		elsif($inviteusername) { $inviteusername = "$inviteusername"; }
			else { $inviteusername = qq~<a href="mailto:$emailof">$emailof</a>~; }

		$ebout .= <<"EOT";
$next<td width="50%">$o
 <table>
  <tr>
   <td rowspan="2" valign="top"><b>$c.</b></td>
   <td><b>$invite[45]:</b> $thisname</td>
  </tr><tr>
   <td><span class="smalltext"><b>$invite[46]:</b> $getdate<br><b>$invite[47]:</b> $inviteusername</span></td>
  </tr><tr>
   <td colspan="2"><span class="smalltext">$serviceen</span></td>
  </tr>
 </table>
</td>
EOT
		$next = '';
		++$counter;
		if($counter == 2) { $next = "</tr><tr>\n"; $counter = 0; }
	}
	if($counter == 1) { $ebout .= "</td><td>&nbsp;</td>"; }
	if($remove) {
		$ebout .= <<"EOT";
<tr><td colspan="2" align="center" class="win2"><span class="smalltext"><b><a href="javascript:clear('$scripturl,v=admin,a=referals,p=remove,pp=2')">$invite[49]</a> | <a href="javascript:clear('$scripturl,v=admin,a=referals,p=remove')">$invite[48]</a></b></span></td></tr>
EOT
	} else {
		$ebout .= <<"EOT";
<tr><td colspan="2">$invite[50]</td></tr>
EOT
	}
	$ebout .= <<"EOT";
   </table>
  </td>
 </tr>
</table>
EOT
	&footerA;
	exit;
}
1;