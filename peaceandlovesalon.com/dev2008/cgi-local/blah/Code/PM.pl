################################################
# E-Blah Bulliten Board Systems       Platinum #
################################################
# Copyright (c) 2001 - 2004 e-blah!            #
# All Rights Reserved.                         #
################################################

CoreLoad('PM',1);

if($pmdisable) { &error($pmtxt[133]); }

sub PMOpen {
	$fids{'3'} = $pmtxt[27];
	if($URL{'f'} == 2) { $load = 2; $select = $pmtxt[139]; $folder = $pmtxt[25]; }
	elsif($fids{$URL{'f'}} ne '') { $load = $URL{'f'}; $select = $pmtxt[5]; $folder = $fids{$URL{'f'}}; }
		else { $load = 1; $select = $pmtxt[7]; $folder = $pmtxt[23]; }
}

sub PMStart {
	if($pmmaxquota > 0) { # Quota
		$size = -s("$members/$username.pm");
		$size = sprintf("%.2f",($size/1024));
		$remain = $size;
		$left = $pmmaxquota-$remain;
		if($remain < 0) { $remain = "0.00"; }
		if($size eq '') { $size = 0; }
		$width = "70%";

		$size = sprintf("%.2f",(($size/$pmmaxquota)*100));
		$type = $typeb = $typec = 'KB';
		if($remain > 1000) { $remain = sprintf("%.2f",$remain/1024); $type = "MB"; }
		$total = $pmmaxquota;
		if($total > 1000) { $total = sprintf("%.2f",$total/1024); $typeb = "MB"; }
		$left = sprintf("%.2f",(($left/100)*100));
		if($left > 1000) { $left = sprintf("%.2f",$left/1024); $typec = "MB"; }
		if($size > 100) { $size = 100; }
		if($left < 0) { $left = 0; $error = $pmtxt[140]; }
		$quota = <<"EOT";
<table class="border" cellpadding="4" cellspacing="1" width="97%" align="center">
 <tr>
  <td class="win2" colspan="3"><span class="smalltext"><b>$size%</b> $pmtxt[185] <b>$total $typeb</b> $pmtxt[186] <b>$left $typec</b> $pmtxt[10].$error</span></td>
 </tr><tr>
  <td class="win" colspan="3"><img src="$images/bar.gif" width="$size%" height="10" alt="$size%"></td>
 </tr><tr>
  <td class="win2" width="33%"><span class="smalltext"><b>0%</b></span></td>
  <td class="win2" width="33%" align="center"><span class="smalltext"><b>50%</b></span></td>
  <td class="win2" width="34%" align="right"><span class="smalltext"><b>100%</b></span></td>
 </tr>
</table>
<br>
EOT
	} # END QUOTA

	if($URL{'start'} ne 'title') { $stitle = qq~<a href="$scripturl,v=memberpanel,a=pm,f=$URL{'f'},start=title">$pmtxt[12]</a>~; }
		else { $stitle = "&#155; $pmtxt[12]"; $ta = 1; }
	if($URL{'start'} ne 'user') { $suser = qq~<a href="$scripturl,v=memberpanel,a=pm,f=$URL{'f'},start=user">$select</a>~; }
		else { $suser = "&#155; $select"; $ta = 1; }
	if($ta) { $sgot = qq~<a href="$scripturl,v=memberpanel,a=pm,f=$URL{'f'}">$pmtxt[14]</a>~; }
		else { $sgot = "&#155; $pmtxt[14]"; }

	$callt = qq~<img src="$images/pm2_sm.gif"> $folder~;

	$displaycenter = <<"EOT";
<SCRIPT LANGUAGE="JavaScript">
<!-- Start
function checkAll() {
 for(i = 0; i < document.dform.elements.length; i++) {
  if(document.dform.del.checked) { document.dform.elements[i].checked = true; }
   else { document.dform.elements[i].checked = false; }
 }
}
// End -->
</script>
$quota
<table cellpadding="0" cellspacing="0" width="97%" align="center">
 <tr><form action="$scripturl,v=memberpanel,a=pm,s=delete,f=$URL{'f'},start=$URL{'start'}" method="post" name="dform">
  <td valign="top" align="right" width="80%">
   <table class="border" cellpadding="4" cellspacing="1" width="100%">
EOT
	$sort = $URL{'start'};

	foreach(@pmdata) {
		($med,$pmid,$title,$tof,$ta,$tb,$mnew,$service,$beep,$flag) = split(/\|/,$_);
		if($med ne $load || $_ eq '') { next; }
		if($sort eq 'user') { push(@pmz,"$tof|$pmid|$title|$med|$ta|$tb|$mnew|$service|$beep|$flag"); }
		elsif($sort eq 'title') { push(@pmz,"$title|$pmid|$med|$tof|$ta|$tb|$mnew|$service|$beep|$flag"); }
			else { push(@pmz,"$pmid|$med|$title|$tof|$ta|$tb|$mnew|$service|$beep|$flag"); }
	}
	if($sort eq 'user' || $sort eq 'title') { @pmz = sort{lc($a) cmp lc($b)} @pmz; }
		else { @pmz = sort{$b <=> $a} @pmz; }

	# Get Pages
	$mupp = 15;
	$maxmessages = @pmz || 1;
	if($maxmessages < $mupp) { $URL{'p'} = 0; }
	$tstart = $URL{'p'} || 0;
	$link = "$scripturl,v=memberpanel,a=pm,f=$URL{'f'},start=$URL{'start'},p";
	if($tstart > $maxmessages) { $tstart = $maxmessages; }
	$tstart = (int($tstart/$mupp)*$mupp);
	if($tstart > 0) { $bk = ($tstart-$mupp); $pagelinks = qq~<a href="$link=$bk">&#171;</a> ~; }
	$counter = 1;
	for($i = 0; $i < $maxmessages; $i += $mupp) {
		if($i == $tstart || $maxmessages < $mupp) { $pagelinks .= qq~<b>$counter</b>, ~; $nxt = ($tstart+$mupp); }
			else { $pagelinks .= qq~<a href="$link=$i">$counter</a>, ~; }
		++$counter;
	}
	$pagelinks =~ s/, \Z//gsi;
	if(($tstart+$mupp) != $i) { $pagelinks .= qq~ <a href="$link=$nxt">&#187;</a>~; }
	$end = ($tstart+$mupp);
	$pagelinks .= " ($var{'92'} $tstart-$end $var{'93'} ".@pmz." $gtxt{'11'})";

	$displaycenter .= <<"EOT";
    <tr>
     <td class="catbg" width="25" align="center"><img src="$images/flag_off.gif" alt="$pmtxt[30]"></td>
     <td class="catbg"><b>$stitle</b></td>
     <td class="catbg" align="center" width="20%"><b>$suser</b></td>
     <td class="catbg"><b>$sgot</b></td>
     <td class="catbg" width="30" align="center"><input type="checkbox" class="checkboxinput" name="del" onclick="checkAll();"></td>
    </tr>
EOT
	if(@pmz > 0) {
		for($c = 0; $c < $maxmessages; $c++) {
			if($c >= $tstart && $c < $end) { push(@pmlist,$pmz[$c]); }
		}
		$counter = $tstart;
	} else { $counter = 0; }

	foreach(@pmlist) {
		$pflag = qq~<img src="$images/flag_off.gif">~;
		if($sort eq 'user') { ($tof,$pmid,$title,$med,$trash,$trash,$mnew,$service,$beep,$flag) = split(/\|/,$_); }
		elsif($sort eq 'title') { ($title,$pmid,$med,$tof,$trash,$trash,$mnew,$service,$beep,$flag) = split(/\|/,$_); }
			else { ($pmid,$med,$title,$tof,$trash,$trash,$mnew,$service,$beep,$flag) = split(/\|/,$_); }
		$date = get_date($pmid);
		&loaduser("$tof");
		if($userset{$tof}->[1] eq '') { $userset{$tof}->[1] = "$tof"; }
			else { $userset{$tof}->[1] = qq!<a href="$scripturl,v=memberpanel,a=view,u=$tof">$userset{$tof}->[1]</a>!; }
		if($mnew) { $new = qq!<img src="$images/new.gif" alt="$pmtxt[20]"> !; } else { $new = ''; }
		if($flag == 1) { $pflag = qq~<img src="$images/flag.gif" alt="$pmtxt[30]">~; }
		elsif($flag == 2) { $pflag = qq~<img src="$images/replied.gif" alt="$pmtxt[31]"> ~; }
		if($service == 1) { $snote = qq~<br><span class="smalltext"><b>&nbsp; - $pmtxt[32]</b></span>~; }
		elsif($service == 2) { $snote = qq~<br><span class="smalltext"><b>&nbsp; - $pmtxt[33]</b></span>~; }
		elsif($service == 3) { $snote = qq~<br><span class="smalltext"><b>&nbsp; - $beep</b></span>~; }
			else { $snote = ''; }
		$displaycenter .= <<"EOT";
    <tr>
     <td class="win2" align="center" id="$counter">$pflag</td>
     <td class="win" id="$counter"><span class="smalltext"><b>$new<a href="$scripturl,v=memberpanel,a=pm,f=$URL{'f'},m=$pmid">$title</a></b>$snote</span></td>
     <td class="win2" align="center" id="$counter"><span class="smalltext">$userset{$tof}->[1]</span></td>
     <td class="win" id="$counter"><span class="smalltext">$date</span></td>
     <td class="win2" align="center" id="$counter"><input type="checkbox" class="checkboxinput" name="d_$counter" value="$pmid"></td>
    </tr>
EOT
		++$counter;
	}
	if(!$counter) { $displaycenter .= <<"EOT";
<tr>
 <td class="win2" align="center" colspan="5"><br>$pmtxt[34]<br><br></td>
</tr>
EOT
	} else {
		$move = <<"EOT";
<select name="moveto"><option value="3">$pmtxt[27]</option>$folderops</select> <input type="submit" class="button" name="move" value="$pmtxt[35]">
EOT
		$displaycenter .= <<"EOT";
    <tr>
     <td class="catbg" colspan="5" align="right">$move <input type="submit" class="button" value="$pmtxt[132]"></td>
    </tr>
EOT
	}
	$displaycenter .= <<"EOT";
   <tr>
    <td class="win" colspan="5"><span class="smalltext"><img src="$images/board.gif"> <b>$gtxt{'17'}:</b> $pagelinks</span></td>
   </tr></table>
  </td>
 </form></tr><tr>
  <td align="center"><br>
   <table cellpadding="5" cellspacing="0">
    <tr>
     <td width="15" align="center"><img src="$images/flag_off.gif"></td><td><span class="smalltext"><b>$pmtxt[145]</b></span></td>
     <td width="15" align="center"><img src="$images/flag.gif"></td><td><span class="smalltext"><b>$pmtxt[30]</b></span></td>
     <td width="15" align="center"><img src="$images/replied.gif"></td><td><span class="smalltext"><b>$pmtxt[31]</b></span></td>
    </tr>
   </table>
  </td>
 </tr>
</table>
EOT
}

sub Move {
	if($FORM{'moveto'} < 3) { &error($pmtxt[37]); }
	if(($FORM{'moveto'} != 3 && $fids{$FORM{'moveto'}} eq '') || $FORM{'moveto'} eq $load) { &error($gtxt{'bfield'}); }
	fopen(FILE,"$members/$username.pm");
	@message = <FILE>;
	fclose(FILE);
	$max = @message;
	chomp @message;

	for($p = 0; $max > $p; $p++) {
		if($FORM{"d_$p"} ne '') { push(@search,$FORM{"d_$p"}); }
	}
	foreach(@message) {
		$fnd = 0;
		($med,$pmid,$title,$tof,$ta,$tb,$mnew,$service,$other,$flag) = split(/\|/,$_);
		if($load eq $med) {
			foreach $find (@search) {
				if($find eq $pmid) { $fnd = 1; }
			}
		}
		$move .= !$fnd ? "$_\n" : "$FORM{'moveto'}|$pmid|$title|$tof|$ta|$tb|$mnew|$service|$other|$flag\n";
	}

	fopen(FILE,">$members/$username.pm");
	print FILE $move;
	fclose(FILE);

	redirect("$scripturl,v=memberpanel,a=pm,f=$URL{'f'}");
}

sub PMDelete {
	if($FORM{'move'}) { &Move; }
	fopen(FILE,"$members/$username.pm");
	@message = <FILE>;
	fclose(FILE);
	$max = @message;

	for($p = 0; $max > $p; $p++) {
		if($FORM{"d_$p"} ne '') { push(@search,$FORM{"d_$p"}); }
	}
	foreach (@message) {
		$fnd = 0;
		chomp;
		($med,$pmid,$title,$tof,$trash,$trash,$mnew,$service) = split(/\|/,$_);
		if($load eq $med) {
			foreach $find (@search) {
				if($find eq $pmid) { $fnd = 1; }
			}
		}
		if($fnd != 1) { $deleted .= "$_\n"; }
	}

	fopen(FILE,">$members/$username.pm");
	print FILE $deleted;
	fclose(FILE);

	# Reset the PM over message
	$prefs[7] = 0;
	fopen(FILE,">$members/$username.prefs");
	for($i = 0; $i < 9; $i++) { print FILE "$prefs[$i]\n"; }
	fclose(FILE);

	redirect("$scripturl,v=memberpanel,a=pm,f=$URL{'f'}");
}

sub Write {
	if($URL{'m'} ne '') {
		fopen(FILE,"$members/$username.pm");
		while (<FILE>) {
			chomp;
			($folder,$mid,$ttitle,$replyuser,$message) = split(/\|/,$_);
			if($folder == $load) {
				if($URL{'m'} == $mid) { $fnd = 1; last; }
			}
		}
		fclose(FILE);
	}
	if($URL{'p'} == 2 && !$FORM{'preview'}) { PMSend2(); return(1); }
	elsif($FORM{'preview'}) {
		$ttitle = Format($FORM{'title'});
		$messageedit = Format($FORM{'message'});
		$replyuser = Format($FORM{'to'});
		$other = Format($FORM{'other'});
		$toall{$FORM{'toall'}} = ' selected';
		$smile{$FORM{'smile'}} = ' checked';
		$flag{$FORM{'flag'}} = ' checked';
		$sendmeth{$FORM{'sendmeth'}} = ' selected';
	}
	$message2 = $message;
	if($FORM{'message'}) {
		$message = $messageedit;
		BC();
		$displaycenter .= <<"EOT";
<table cellpadding="5" cellspacing="1" width="700" align="center" class="border">
 <tr>
  <td class="catbg"><span class="smalltext"><b>$pmtxt[184]</b></span></td>
 </tr><tr>
  <td class="win">$message</td>
 </tr>
</table><br>
EOT
	}
	if($fnd) {
		$title = $pmtxt[38];
		$ttitle =~ s/Re: //gsi;
		$ttitle = "Re: $ttitle";
		$fndinp = qq~<input type="hidden" name="ron" value="1">~;
	} else { $title = $pmtxt[39]; }

	if($URL{'t'} ne '') { $replyuser = $URL{'t'}; }

	$callt = qq~<img src="$images/pm2_sm.gif"> $title~;

	$displaycenter .= <<"EOT";
<script language="JavaScript">
function AddBuddy() {
	if(document.msend.to.value != '') { front = ','; } else { front = ''; }
	document.msend.to.value += front+document.msend.addbud.value;
	document.msend.addbud.value = '';
}
</script>
<table cellpadding="2" cellspacing="1" class="border" width="700" align="center">
 <tr><form action="$scripturl,v=memberpanel,a=pm,f=$URL{'f'},s=write,p=2,m=$URL{'m'}" method="post" name="msend">
  <td class="win">
   <table cellpadding="4" cellpadding="0" width="100%">
    <tr>
     <td width="30%" valign="top" align="right" rowspan="2"><b>$pmtxt[42]:</b></td>
     <td width="150"><input type="text" class="textinput" name="to" value="$replyuser" size="30" tabindex="1"></td><td>
EOT
	LoadBuddys(1);
	if(@buddylist > 0) {
		$displaycenter .= qq~<select name="addbud" tabindex="1" onChange="AddBuddy();"><option value=''>$pmtxt[167]</option>~;

		foreach(@buddylist) {
			($name,$myname) = split("/",$_);
			loaduser($name);
			if(!$userset{$name}->[1] || $blocked{$name}) { next; }
			$online = $activemembers{$name} && !$userset{$name}->[18] ? " ($gtxt{'30'})" : '';
			if($myname) { $userset{$name}->[1] = $myname; }
			$displaycenter .= qq~<option value="$name">$userset{$name}->[1]$online</option>~;
		}
		$displaycenter .= qq~</select>~;
	}

	$displaycenter .= qq~</td></tr><tr><td colspan="2"><span class="smalltext">$pmtxt[43]</span></td></tr>~;

	if($settings[4] eq 'Administrator') {
		$displaycenter .= <<"EOT";
    <tr>
     <td align="right" valign="top" width="30%" rowspan="2"><b>$pmtxt[44]:</b></td>
     <td valign="top" colspan="2"><select name="toall" tabindex="2"><option value="0"$toall{0}>$pmtxt[46]</option><option value="1"$toall{1}>$pmtxt[47]</option></select></td>
    </tr><tr>
     <td colspan="2"><span class="smalltext">$pmtxt[45]</span></td>
    </tr>
EOT
	}
	$displaycenter .= <<"EOT";
    <tr>
     <td align="right" width="30%"><b>$pmtxt[48]:</b></td>
     <td colspan="2"><input type="text" class="textinput" tabindex="3" name="title" value="$ttitle" size="40" maxlength="50"></td>
    </tr>
   </table>
  </td>
 </tr>
EOT
	CoreLoad('Post');
	if($BCLoad || $BCSmile) { $displaycenter .= BCWait(); }
	if($BCLoad) { $displaycenter .= BCLoad(); }
	$displaycenter .= <<"EOT";
 <tr>
  <td class="win">
   <table cellpadding="4" cellspacing="0" width="100%">
    <tr>
     <td align="right" width="30%" valign="top"><b>$pmtxt[51]:</b><br><br>
EOT
	if($BCSmile) { $displaycenter .= &BCSmile; }

	$messageedit =~ s/<br>/\n/g;

	$displaycenter .= <<"EOT";
    </td>
     <td width="70%"><textarea name="message" tabindex="4" wrap="virtual" rows="12" cols="95" style="width: 100%">$messageedit</textarea></td>
    </tr><tr>
     <td align="right" width="30%"><b>$pmtxt[54]:</b></td>
     <td width="70%" valign="top"><input type="checkbox" class="checkboxinput" tabindex="5" name="smile" value="1"$smile{1}> &nbsp; <span class="smalltext">$posttxt[120]</span></td>
    </tr><tr>
     <td align="right" valign="top" width="30%"><b>$pmtxt[55]:</b></td>
     <td width="70%" valign="top"><input type="checkbox" class="checkboxinput" tabindex="6" name="flag" value="1"$flag{1}> &nbsp; <span class="smalltext">$pmtxt[56]</span></td>
    </tr>
   </table>
  </td>
 </tr><tr>
  <td class="catbg" style="padding: 5px"><span class="smalltext"><b>$pmtxt[57]</b></span></td>
 </tr><tr>
  <td class="win">
   <table cellpadding="4" cellspacing="0" width="100%">
    <tr>
     <td colspan="2"><b>$pmtxt[58]</b></td>
    </tr><tr>
     <td width="25%"><select name="sendmethod" tabindex="6"><option value="0"$sendmeth{0}>$pmtxt[60]</option><option value="2"$sendmeth{2}>$pmtxt[33]</option><option value="1"$sendmeth{1}>$pmtxt[32]</option><option value="3"$sendmeth{3}>$pmtxt[64] $pmtxt[63]</option></select></td>
     <td><span class="smalltext"><b>$pmtxt[64]: </b></span><input type="text" class="textinput" tabindex="7" name="other" value="$other" maxlength="50"></td>
    </tr><tr>
     <td colspan="2"><span class="smalltext">$pmtxt[59]</span></td>
    </tr>
   </table>
  </td>
 </tr><tr>
  <td class="win2" align="center" style="padding: 5px"><input type="submit" class="button" tabindex="8" value=" $pmtxt[66] " name="submit"> &nbsp;<input type="submit" class="button" tabindex="9" value=" $pmtxt[184] " name="preview"></td>
 </form></tr>
</table>
EOT
	$message = FormatOld($message2,$replyuser);
	PMCODE();
	if($fnd) { $displaycenter .= $messagez; }
}

sub FormatOld {
	($input,$ruser) = @_;
	$temp1 = $input;
	$temp2 = $input;
	$temp1 =~ s~{REPLY: (.+?)}(.+?){/REPLY}~&ReFormat~eisg;
	$temp3 = qq~\{REPLY: $ruser\}$temp1\{/REPLY\}~;
	$return = $temp3.$add;
	return ($return);
}

sub ReFormat { $add .= "\{REPLY: $1\}$2\{/REPLY\}"; return ""; }

sub PMSend2 {
	if($pmmaxquota && 0 > $pmmaxquota-(sprintf("%.2f",(-s("$members/$username.pm"))/1024)) && $settings[4] ne 'Administrator') { error($pmtxt[141]); }

	$mess = $message;

	$subject = Format($FORM{'title'});
	$message = Format($FORM{'message'});

	if($subject eq '') { error($pmtxt[68]); }
	if($message eq '') { error($pmtxt[69]); }

	error($gtxt{'long'}) if(length($message) > $maxmesslth && $maxmesslth);
	$adminoverride = 0;
	$message =~ s~\{R~\{<!>R~gi;
	$message =~ s~\Y}~Y<!>\}~gi;

	if($settings[4] eq 'Administrator' && $FORM{'toall'}) {
		fopen(FILE,"$members/List.txt") || error($pmtxt[70],1);
		@to = <FILE>;
		fclose(FILE);
		chomp @to;
		$adminoverride = 1;
	} else {
		if($FORM{'to'} eq '') { error($pmtxt[71]); }
		@to = split(/,/,$FORM{'to'});
	}
	if($settings[4] eq 'Administrator') {
		if(length($FORM{'other'}) > 50) { error($pmtxt[72]); }
		if($FORM{'sendmethod'} == 1) { $adminadd = 1; $FORM{'other'} = ''; }
		elsif($FORM{'sendmethod'} == 2) { $adminadd = 2; $FORM{'other'} = ''; }
		elsif($FORM{'sendmethod'} == 3) { $adminadd = 3; }
			else { $FORM{'other'} = ''; }
	}

	$counter = 0;
	$ttime = time;

	fopen(FILE,"$members/List.txt");
	@members = <FILE>;
	fclose(FILE);
	chomp @members;

	GMS();

	foreach (@to) {
		$_ =~ s/ //gi;
		$found = 0;
		if(!-e "$members/$_.dat") {
			foreach $find (@members) {
				loaduser($find);
				$userset{$find}->[1] =~ s/ //gi;
				if(lc($userset{$find}->[1]) eq lc($_)) {
					$found = 1;
					$_ = $find;
					last;
				}
			}
		} else { $found = 1; }
		if($found == 0 && $adminoverride) { error("$_ $pmtxt[73]"); }
		if($found == 0 && !$adminoverride) { error("$_ $pmtxt[74]"); }
		loaduser($_);

		if($userset{$_}->[38]) { error("$pmtxt[146] $userset{$_}->[1]."); }

		$cnt = 0;
		fopen(FILE,"$members/$_.prefs");
		while( $value = <FILE> ) {
			chomp $value;
			$prefs{$_}->[$cnt] = $value;
			++$cnt;
		}
		fclose(FILE);

		if(!$adminoverride && $settings[4] ne 'Administrator') {
			@blocked = split(/\|/,$prefs{$_}->[0]);
			foreach $block (@blocked) {
				if($username eq $block) { error("$pmtxt[75] $_ $pmtxt[76]"); }
			}
		}

		# Check the PM quota
		&loaduser($_);
		$pmdis = 0;
		if($pmmaxquota && !$adminoverride && $userset{$_}->[4] ne 'Administrator' && !$admin{$userset{$_}->[4]}) {
			$size = -s("$members/$_.pm");
			if($size > 0) { $size = sprintf("%.2f",($size/1024)); }
			$remain = $pmmaxquota-$size;
			if($remain < 0) { # PM limit has been reached
				$remain = "0.00";
				if(!$prefs{$_}->[7]) {
					$prefs{$_}->[7] = 1;
					fopen(FILE,">$members/$_.prefs");
					for($i = 0; $i < 9; $i++) { print FILE "$prefs{$_}->[$i]\n"; }
					fclose(FILE);

					fopen(FILE,">>$members/$_.pm");
					print FILE "1|$ttime|$pmtxt[79]|admin|$pmtxt[80]|$ENV{'REMOTE_ADDR'}|1|1\n";
					fclose(FILE);
					if($prefs{$_}->[1]) { smail($userset{$_}->[2],$pmtxt[81],$pmtxt[80]); }
				}
				error("$_ $pmtxt[83]");
				$pmdis = 1;
			}
		}
		if($prefs{$_}->[1] && (!$pmdis)) { $emailm[$counter] = $userset{$_}->[2]; ++$counter; }
		if(!$adminoverride && !$pmdis) { $sentto .= "$userset{$_}->[1]<br>"; }

		# Send message ...
		if(!$prefs{$_}->[7]) { push(@confirmed,$_); }
	}

	if(@errors) { Write(1); }

	foreach(@confirmed) {
		if($prefs{$_}->[4]) {
			($tsub,$tmess) = split(/\|/,$prefs{$_}->[5]);
			++$ttime;
			push(@esend,"1|$ttime|$tsub|$_|$tmess|$ENV{'REMOTE_ADDR'}|1|3|$pmtxt[99]");
			fopen(FILE,">>$members/$_.pm");
			++$ttime;
			print FILE "2|$ttime|$tsub|$username|$tmess|$ENV{'REMOTE_ADDR'}||3|$pmtxt[99]\n";
			fclose(FILE);
		}
	}
	$ttime = time;
	$date_time = get_date(time);
	$mcut = $message;
	$BCSmile = 0;
	&BC;
	$sendmessage = qq~$settings[1] $pmtxt[84] ($date_time):<hr size="1" color="#000000"><br>$message<br><br><hr size="1" color="#000000"><span class="smalltext"><a href="$rurl,v=memberpanel,a=pm">$pmtxt[85]</a>.</span>~;
	$message = $mcut;
	foreach(@emailm) {
		if($NOEM{$_}) { next; }
		smail($_,"$pmtxt[86]: $subject",$sendmessage);
		$NOEM{$_} = 1;
	}
	fopen(FILE,"$members/$username.prefs");
	@perprefs = <FILE>;
	fclose(FILE);
	chomp @perprefs;

	if($URL{'m'} ne '') { $message .= FormatOld($mess,$replyuser); }
	foreach(@confirmed) {
		if($NO{$_}) { next; }
		++$ttime;
		fopen(FILE,">>$members/$_.pm");
		print FILE "1|$ttime|$subject|$username|$message|$ENV{'REMOTE_ADDR'}|1|$adminadd|$FORM{'other'}|$FORM{'flag'}|$FORM{'smile'}\n";
		fclose(FILE);
		if($adminoverride == 0 && $perprefs[2] != 1) {
			++$ttime;
			fopen(FILE,">>$members/$username.pm");
			print FILE "2|$ttime|$subject|$_|$message|$ENV{'REMOTE_ADDR'}||$adminadd|$FORM{'other'}||$FORM{'smile'}\n";
			fclose(FILE);
		}
		$NO{$_} = 1;
	}
	if($adminoverride) {
		if($perprefs[2] != 1) {
			++$ttime;
			fopen(FILE,">>$members/$username.pm");
			print FILE "2|$ttime|$subject|$pmtxt[87]|$message|$ENV{'REMOTE_ADDR'}||$adminadd|$FORM{'other'}||$FORM{'smile'}\n";
			fclose(FILE);
		}
		$sentto = "$pmtxt[47]<br>";
	}
	fopen(FILE,">>$members/$username.pm");
	foreach(@esend) { print FILE "$_\n"; }
	fclose(FILE);

	# Mark as replied
	if($URL{'m'}) {
		fopen(FILE,"$members/$username.pm");
		@replied = <FILE>;
		fclose(FILE);
		chomp @replied;

		fopen(FILE,"+>$members/$username.pm");
		foreach(@replied) {
			($folder,$mid,$ttitle,$replyuser,$message,$ip,$t1,$t2,$t3,$t4,$t5) = split(/\|/,$_);
			if($folder == $load && $URL{'m'} == $mid) { print FILE "$folder|$mid|$ttitle|$replyuser|$message|$ip|$t1|$t2|$t3|2|$t5\n"; }
				else { print FILE "$_\n"; }
		}
		fclose(FILE);
	}

	if($perprefs[3]) { redirect("$scripturl,v=memberpanel,a=pm,f=$URL{'f'}"); }

	$callt = qq~<img src="$images/pm2_sm.gif"> $pmtxt[144]</b>~;
	$morecaller = $pmtxt[182];
	$displaycenter = <<"EOT";
<meta http-equiv="refresh" content="5;url=$scripturl,v=memberpanel,a=pm,f=$URL{'f'}">
<table class="border" align="center" width="425" cellpadding="4" cellspacing="1">
 <tr>
  <td class="win"><span class="smalltext"><b>$pmtxt[88]</b></span></td>
 </tr><tr>
  <td class="win2"><span class="smalltext"><br><center>$sentto<br><input type="button" class="button" onClick="location='$scripturl,v=memberpanel,a=pm,f=$URL{'f'}'" value="&nbsp; $gtxt{'26'} &nbsp;"></center></span><br></td>
 </tr>
</table>
EOT
}

sub Prefs {
	if($URL{'p'} == 2) { &Prefs2; }

	$CHECK1["$prefs[1]"] = " checked";
	$CHECK2["$prefs[2]"] = " checked";
	$CHECK3["$prefs[3]"] = " checked";
	$CHECK4["$prefs[4]"] = " checked";
	($sub,$mess) = split(/\|/,$prefs[5]);
	$subject = Unformat($sub);
	$message = Unformat($mess);

	$callt = qq~<img src="$images/pm2_sm.gif"> $pmtxt[177]~;

	$displaycenter = <<"EOT";
<table class="border" cellspacing="1" cellpadding="4" width="97%" align="center">
 <tr><form action="$scripturl,v=memberpanel,a=pm,s=prefs,f=$URL{'f'},p=2" method="post" name="post">
  <td class="catbg" align="center"><span class="smalltext"><b>$pmtxt[143]</b></span></td>
 </tr><tr>
  <td class="win">
   <table cellpadding="3" cellspacing="0" width="100%">
    <tr>
     <td>
      <table cellpadding="3" cellspacing="0">
       <tr>
        <td width="5%" align="center"><input type="checkbox" class="checkboxinput" name="email" value="1"$CHECK1[1]></td>
        <td><b>$pmtxt[93]</b></td>
       </tr><tr>
        <td align="center"><input type="checkbox" class="checkboxinput" name="outbox" value="1"$CHECK2[1]></td>
        <td><b>$pmtxt[95]</b></td>
       </tr><tr>
        <td align="center"><input type="checkbox" class="checkboxinput" name="sent" value="1"$CHECK3[1]></td>
        <td><b>$pmtxt[97]</b></td>
       </tr>
      </table>
     </td>
    </tr>
   </table>
  </td>
 </tr><tr>
  <td class="catbg" align="center"><span class="smalltext"><b>$pmtxt[99]</b></span></td>
 </tr><tr>
  <td class="win2"><span class="smalltext">$pmtxt[100]</span></td>
 </tr><tr>
  <td class="win">
   <table cellpadding="3" cellspacing="0" width="100%">
    <tr>
     <td>
      <table cellpadding="3" cellspacing="0">
       <tr>
        <td width="4%" align="center"><input type="checkbox" class="checkboxinput" name="auto" value="1" onClick="OpenTemp()"$CHECK4[1]></td>
        <td width="96%"><b>$pmtxt[77]</b></td>
       </tr>
      </table>
     </td>
    </tr><tr>
     <td colspan="2" id="temp" style="display:none;">
      <table cellpadding="3" cellspacing="0">
       <tr>
        <td align="right" valign="top" width="200"><b>$pmtxt[102]:</b></td>
        <td valign="top"><input type="text" class="textinput" value="$subject" name="autosub" size="30"></td>
       </tr><tr>
        <td align="right" valign="top"><b>$pmtxt[103]:</b></td>
        <td><textarea name="automessage" wrap="virtual" cols="50" rows="6">$message</textarea></td>
       </tr>
      </table>
     </td>
    </tr>
   </table>
  </td>
 </tr><tr>
  <td class="win2" align="center"><input type="submit" class="button" value=" $pmtxt[104] "></td>
 </form></tr>
</table>
<script language="javascript">
function OpenTemp() {
	if(document.getElementById) { openItem = document.getElementById('temp'); }
	else if (document.all){ openItem = document.all['temp']; }
	else if (document.layers){ openItem = document.layers['temp']; }

	if(document.post.auto.checked == true) { ShowType = ""; }
		else { ShowType = "none"; }

	if(openItem.style) { openItem.style.display = ShowType; }
		else { openItem.visibility = "show"; }
}
OpenTemp();
</script>
EOT
}

sub Prefs2 {
	$FORM{'blocked'} =~ s/\cM//g;
	$FORM{'blocked'} =~ s/\n/\|/g;
	$email = $FORM{'email'} || 0;
	$outbox = $FORM{'outbox'} || 0;
	$sent = $FORM{'sent'} || 0;
	$auto = $FORM{'auto'} || 0;
	$autosub = Format($FORM{'autosub'});
	$automessage = Format($FORM{'automessage'});
	if($auto && $automessage eq '') { &error($pmtxt[105]); }
	if(length($automessage) > 800) { &error($pmtxt[106]); }
	fopen(FILE,">$members/$username.prefs");
	print FILE "$prefs[0]\n$email\n$outbox\n$sent\n$auto\n$autosub|$automessage\n$prefs[6]\n$prefs[7]\n$prefs[8]\n";
	fclose(FILE);
	redirect("$scripturl,v=memberpanel,a=pm,f=$URL{'f'}");
}

sub PMBlock {
	if($URL{'u'} eq 'All Members') { &error($pmtxt[107]); }
	fopen(FILE,"$members/$username.prefs");
	@prefs = <FILE>;
	fclose(FILE);
	chomp @prefs;
	@blocks = split(/\|/,$prefs[0]);
	foreach (@blocks) {
		chomp;
		if($_ eq $URL{'u'}) { $alreadyon = 1; }
	}

	loaduser($URL{'u'});
	if($userset{$URL{'u'}}->[1] eq '') { $userset{$URL{'u'}}->[1] = $URL{'u'}; }
		else { $userset{$URL{'u'}}->[1] = qq~<a href="$scripturl,v=memberpanel,a=view,u=$URL{'u'}">$userset{$URL{'u'}}->[1]</a>~; }

	if($alreadyon != 1) {
		fopen(FILE,"+>$members/$username.prefs");
		print FILE "$prefs[0]$URL{'u'}|\n";
		for($q = 1; $q < @prefs; $q++) {
			print FILE "$prefs[$q]\n";
		}
		fclose(FILE);
		$title = $pmtxt[110];
		$message = "$userset{$URL{'u'}}->[1] $pmtxt[111]";
	} else {
		$title = $pmtxt[112];
		$message = "$userset{$URL{'u'}}->[1] $pmtxt[113]";
	}

	$callt = qq~<img src="$images/ban.gif"> $title~;

	$displaycenter = <<"EOT";
<table class="border" cellpadding="6" cellspacing="1" width="500" align="center">
 <tr>
  <td class="win">$message<br><br><span class="smalltext">$pmtxt[114]</span></td>
 </tr>
</table>
EOT
}

sub PMDisplay {
	fopen(FILE,"$members/$username.pm");
	@pm = <FILE>;
	fclose(FILE);
	chomp @pm;
	foreach(@pm) {
		($med,$pmid,$tm,$tof,$message,$ip,$new) = split(/\|/,$_);
		if($med eq $load && $pmid == $URL{'m'}) { $found = 1; last; }
	}
	if($found != 1) { error($pmtxt[115]); }
	loaduser($tof);
	if($userset{$tof}->[1] eq '') { $tofrom = $tof; }
		else { $tofrom = qq!<a href="$scripturl,v=memberpanel,a=view,u=$tof">$userset{$tof}->[1]</a><br><b><a href="$scripturl,v=memberpanel,a=pm,s=blist,p=2,buddy=$tof">$pmtxt[169]</a></b>!; }
	$dategot = get_date($pmid);

	if($load == 1 || $load == 3) {
		if($new == 1) {
			fopen(FILE,"+>$members/$username.pm");
			foreach(@pm) {
				($tmed,$tpmid,$ttm,$ttof,$tmessage,$tip,$tnew,$tmore,$tmore2,$flag,$smile) = split(/\|/,$_);
				if($tpmid == $URL{'m'} && $tmed == $load) {
					if($flag) { $fl = 1; }
					print FILE "$tmed|$tpmid|$ttm|$ttof|$tmessage|$tip|0|$tmore|$tmore2||$smile\n";
				} else { print FILE "$_\n"; }
			}
			fclose(FILE);
		}
	}
	if($fl == 1) {
		$curforumtime = time;
		$thisdate = get_date(time);
		fopen(USER,"+>>$members/$tof.pm");
		print USER "1|$curforumtime|$pmtxt[55]: $ttm|$username|$settings[1] ($username) $pmtxt[117] $thisdate.|$ENV{'REMOTE_ADDR'}|1|2\n";
		fclose(USER);
		$cntme = 0;
		fopen(FILE,"$members/$tof.prefs");
		@prefs = <FILE>;
		fclose(FILE);
		chomp @prefs;
		if($prefs[1]) { smail($_,"$pmtxt[86]: $subject",$sendmessage); }
	}

	if(!$smile) { $smiley = $pmtxt[138]; } else { $smiley = $pmtxt[120]; }

	$callt = qq~<img src="$images/pm2_sm.gif"> $pmtxt[121]~;

	$tm = CensorList($tm);

	$displaycenter = <<"EOT";
<script language="javascript">
<!-- //
function clear(url) {
 if(window.confirm("$pmtxt[136]")) { location = url; }
}
// -->
</script>
<table class="border" cellpadding="4" cellspacing="1" width="97%" align="center">
 <tr>
  <td class="win">
   <table cellpadding="4" cellspacing="0" width="100%">
    <tr>
     <td width="30%" align="right"><span class="smalltext"><b>$pmtxt[12]:</b></span></td>
     <td width="70%"><span class="smalltext">$tm</span></td>
    </tr><tr>
     <td align="right" valign="top"><span class="smalltext"><b>$select:</b></span></td>
     <td><span class="smalltext">$tofrom</span></td>
    </tr><tr>
     <td align="right"><span class="smalltext"><b>$pmtxt[14]:</b></span></td>
     <td><span class="smalltext">$dategot</span></td>
    </tr>
   </table>
  </td>
 </tr><tr>
  <td class="win2">
   <table cellpadding="4" cellspacing="0" width="100%">
    <tr><tr>
     <td width="30%" align="right" valign="top"><span class="smalltext"><b>$pmtxt[126]:</b></span></td>
     <td width="70%"><span class="smalltext"><a href="javascript:clear('$scripturl,v=memberpanel,a=pm,s=block,u=$tof')">$pmtxt[137]</a><br>
     <a href="$scripturl,v=report,a=pmreport,m=$URL{'m'},f=$load">$pmtxt[128]</a></span></td>
    </tr><tr>
     <td align="right" valign="top"><span class="smalltext"><b>$pmtxt[129]:</b></span></td>
     <td><span class="smalltext">$smiley</span></td>
    </tr>
   </table>
  </td>
 </tr><tr>
  <td class="catbg">
   <table cellpadding="0" cellspacing="0" width="100%">
    <tr><form action="$scripturl,v=memberpanel,a=pm,s=delete,f=$URL{'f'}" method="post" name="dform">
     <td><span class="smalltext"><b>&#155; <a href="$scripturl,v=memberpanel,a=pm,s=write,f=$URL{'f'},m=$URL{'m'}">$pmtxt[130]</a> | <a href="javascript:clear('$scripturl,v=memberpanel,a=pm,s=mdelete,f=$URL{'f'},m=$URL{'m'}')">$pmtxt[131]</a> | <a href="$scripturl,v=print,s=pm,f=$URL{'f'},m=$URL{'m'}">$pmtxt[166]</a></b></span></td>
     <td align="right"><input type="hidden" name="d_0" value="$URL{'m'}"><select name="moveto"><option value="3">$pmtxt[27]</option>$folderops</select> <input type="submit" class="button" name="move" value="$pmtxt[35]"></td>
    </form></tr>
   </table>
  </td>
 </tr>
</table><br>
EOT
	if($smile) { $BCSmile = 0; }
	&PMCODE;
	&BC;
	$message2 = $message;
	if($load == 2) { $tof = $username; }
	LoadColors();
	loaduserdisplay($tof);
	$message = $message2;

	$email = $guest ? qq~<a href="mailto:$email">$img{'email'}</a>~ : $memberemail;
	$email .= $pmdisable ? '' : qq~ <a href="$scripturl,v=memberpanel,a=pm,s=write,t=$user">$img{'pm'}</a>~;

	$homepage = ($userset{$tof}->[19] && $userset{$tof}->[20]) ? qq~<a href="$userset{$tof}->[20]" title="$userset{$tof}->[19]">$img{'site'}</a> ~: '';

	$online = '';
	if($activemembers{$tof} && !$userset{$tof}->[18]) { $online = $gtxt{'30'}; }
	elsif($logactive && !$userset{$tof}->[18] && $lastactive{$tof}) { $online = qq~<a title="$lastactive{$tof}">$gtxt{'31'}</a>~; }

	$showip = $settings[4] eq 'Administrator' ? qq~<a href="$scripturl,v=admin,a=advipsearch,s=advsearch,ip=$ip">$ip</a>~ : $pmtxt[183];
	$displaycenter .= <<"EOT";
<table class="border" cellpadding="4" cellspacing="1" width="97%" align="center">
 <tr>
  <td class="titlebg"><b>&nbsp;<img src="$images/profile_sm.gif"> $gtxt{'36'}</b></td>
  <td class="titlebg"><b>&nbsp;<img src="$images/xx.gif">&nbsp; $tm</b></td>
 </tr><tr>
  <td class="win" width="180" align="center"><b>$membername</b></td>
  <td class="win">
   <table cellpadding="0" cellspacing="2" width="100%">
    <tr><td><span class="smalltext"><b>$pmtxt[14]:</b> $dategot</span></td><td align="right"><span class="smalltext"><a href="$scripturl,v=memberpanel,a=pm,s=write,f=$URL{'f'},m=$URL{'m'}">$img{'reply'}</a> $msp<a href="$scripturl,v=memberpanel,a=pm,s=mdelete,f=$URL{'f'},m=$URL{'m'}">$img{'remove'}</a></span></td></tr>
   </table>
  </td>
 </tr><tr>
  <td class="win2" width="180" valign="top"><span class="smalltext">$mprofile</span></td>
  <td class="win2" height="100%" valign="top">
   <table cellspacing="0" cellpadding="0" width="100%" height="100%">
    <tr>
     <td valign="top" height="100%" class="postbody">$message</td>
    </tr>$extra
   </table>
  </td>
 </tr><tr>
  <td class="win" width="180">
   <table width="100%">
    <tr>
     <td><span class="smalltext"><b><img src="$images/ip.gif"> $showip</span></td>
     <td align="right"><span class="smalltext"><b>$online</b></span></td>
    </tr>
   </table>
  </td><td class="win"><span class="smalltext">$homepage$email$instmsg</span></td>
 </tr>
</table>$messagez
EOT
}

sub PMDelete2 {
	if($URL{'p'} eq 'move') { $FORM{"d_0"} = $URL{'m'}; $FORM{'moveto'} = 3; &Move; }

	fopen(FILE,"$members/$username.pm");
	@message = <FILE>;
	fclose(FILE);
	chomp @message;
	foreach (@message) {
		$fnd = 0;
		($med,$pmid) = split(/\|/,$_);
		if($load eq $med) {
			if($URL{'m'} eq $pmid) { $fnd = 1; }
		}
		if(!$fnd) { $deleted .= "$_\n"; }
	}
	fopen(FILE,">$members/$username.pm");
	print FILE $deleted;
	fclose(FILE);

	# Reset the PM over message
	$prefs[7] = 0;
	fopen(FILE,">$members/$username.prefs");
	for($i = 0; $i < 9; $i++) { print FILE "$prefs[$i]\n"; }
	fclose(FILE);

	redirect("$scripturl,v=memberpanel,a=pm,f=$URL{'f'}");
}

sub PMCODES {
	++$replies;
	loaduser($1);
	if($userset{$1}->[1] eq '') { $userset{$1}->[1] = $1; }
		else { $userset{$1}->[1] = qq!<a href="$scripturl,v=memberpanel,a=view,u=$1">$userset{$1}->[1]</a>!; }
	$messagew = $message;
	$message = $2;
	&BC;
	$messagea .= <<"TEMP";
 <tr>
  <td class="win2">
  <table cellpadding="4" cellspacing="0">
   <tr>
    <td><span class="smalltext"><b>$gtxt{'37'} $replies:</b> </td>
    <td><span class="smalltext">$userset{$1}->[1]</span></td>
   </tr>
  </table>
  </td>
 </tr><tr>
  <td class="win" class="postbody">$message</td>
 </tr>
TEMP
	$message = $messagew;
	return($messagea);
}

sub PMCODE {
	$replies = 0;
	if($message =~ /{REPLY: (.*?)}(.*?){\/REPLY}/) { $on = 1; }
		else { return; }
	$messagez = $message;

	$message =~ s~\{REPLY: (.+?)\}(.+?)\{\/REPLY\}~~isg;
	$messagez =~ s~{REPLY: (.+?)}(.+?){\/REPLY}~&PMCODES~eisg;

	$width = $URL{'s'} eq 'write' ? 700 : '98%';
	$messagez = <<"EOT";
<br><table class="border" cellpadding="4" cellspacing="1" width="$width" align="center">
 <tr>
  <td class="titlebg" align="center"><span class="smalltext"><b>$gtxt{'38'}</b></span></td>
 </tr>$messagea
</table>
EOT
}

sub Folders {
	if($URL{'p'}) { &Folders2; }

	$callt = qq~<img src="$images/thread.gif"> $pmtxt[147]~;
	$morecaller = $pmtxt[149];
	$displaycenter = <<"EOT";
<table cellpadding="4" cellspacing="1" class="border" width="97%" align="center">
 <tr><form action="$scripturl,v=memberpanel,a=pm,s=folders,p=1" method="post" name="msend">
  <td class="catbg" align="center"><span class="smalltext"><b>$pmtxt[150]</b></span></td>
 </tr><tr>
  <td class="win2"><span class="smalltext">$pmtxt[151]<br><img src="$images/warning_sm.gif"> $pmtxt[155]</span></td>
 </tr><tr>
  <td class="win">
   <table cellpadding="5" cellspacing="0">
    <tr>
     <td width="150"><b>$pmtxt[23]</b></td><td><span class="smalltext">$pmtxt[148]</span></td>
    </tr><tr>
     <td width="150"><b>$pmtxt[25]</b></td><td><span class="smalltext">$pmtxt[148]</span></td>
    </tr><tr>
     <td width="150"><b>$pmtxt[27]</b></td><td><span class="smalltext">$pmtxt[148]</span></td>
    </tr>
EOT
	foreach(@folders) {
		($fname,$fid) = split("/",$_);
		$fids{$fid} = $fname;
		$displaycenter .= <<"EOT";
<tr>
 <td colspan="2"><input type="text" class="textinput" name="$fid" value="$fname" size="30" maxlength="30"></td>
</tr>
EOT
	}

	$displaycenter .= <<"EOT";
   </table>
  </td>
 </tr><tr>
  <td class="catbg" align="center"><span class="smalltext"><b>$pmtxt[152]</b></span></td>
 </tr><tr>
  <td class="win2"><span class="smalltext">$pmtxt[153]</span></td>
 </tr><tr>
  <td class="win">
   <table cellpadding="5" cellspacing="0">
EOT
	$folderucnt = $foldercnt;
	for($i = $folderucnt+4; $i < $folderucnt+7; $i++) {
		while($fids{$i} ne '') { ++$i; ++$folderucnt; }
		$displaycenter .= qq~<tr><td><input type="text" class="textinput" name="new_$i" size="30" maxlength="30"></td></tr>~;
	}
	$displaycenter .= <<"EOT";
   </table>
  </td>
 </tr><tr>
  <td class="win2" align="center"><input type="submit" class="button" value="$pmtxt[154]"></td>
 </form></tr>
</table>
EOT
}

sub Folders2 {
	while(($iname,$ivalue) = each(%FORM)) {
		push(@data,"$iname|$ivalue");
	}
	foreach(@data) {
		($iname,$ivalue) = split(/\|/,$_);
		$ivalue =~ s/\n//gsi;
		$ivalue =~ s~\/~&#47;~gsi;
		$iname =~ s/\n//gsi;
		$iname =~ s~\/~&#47;~gsi;
		$current = Format($ivalue);
		if($iname =~ /new_(.*?)\Z/) {
			if($current eq '') { next; }
			if($fids{$1} ne '') { next; } # It's already a folder! Don't overwrite!
			push(@print,"$1|$current");
			next;
		}
		if($current eq '') {
			$nodel = '';
			fopen(FILE,"$members/$username.pm");
			@message = <FILE>;
			fclose(FILE);
			chomp @message;
			foreach (@message) {
				$fnd = 0;
				($med) = split(/\|/,$_);
				if($iname eq $med) { $fnd = 1; }
				if(!$fnd) { $nodel .= "$_\n"; }
			}
			fopen(FILE,">$members/$username.pm");
			print FILE $nodel;
			fclose(FILE);
		} else { push(@print,"$iname|$current"); }
	}

	foreach(sort{$a <=> $b} @print) {
		($t1,$t2) = split(/\|/,$_);
		$printdata .= "$t2/$t1|";
	}

	fopen(FILE,">$members/$username.prefs");
	print FILE "$prefs[0]\n$prefs[1]\n$prefs[2]\n$prefs[3]\n$prefs[4]\n$prefs[5]\n$printdata\n$prefs[7]\n$prefs[8]\n";
	fclose(FILE);

	redirect("$scripturl,v=memberpanel,a=pm,s=folders");
}

sub Search {
	if($URL{'p'}) { &Search2; return; }

	$callt = qq~<img src="$images/search_sm.gif"> $pmtxt[163]~;
	$displaycenter = <<"EOT";
<table cellpadding="4" cellspacing="1" class="border" width="97%" align="center">
 <tr><form action="$scripturl,v=memberpanel,a=pm,s=search,p=1" method="post" name="msend">
  <td class="catbg" align="center"><span class="smalltext"><b>$pmtxt[162]</b></span></td>
 </tr><tr>
  <td class="win">
   <table width="100%">
    <tr>
     <td width="35%" align="right"><b>$pmtxt[161]:</b></td>
     <td><input type="text" class="textinput" name="search" size="35"></td>
    </tr><tr>
     <td width="35%" align="right"><b>$pmtxt[178]:</b></td>
     <td><input type="text" class="textinput" name="touser" size="30"></td>
    </tr><tr>
     <td width="35%" align="right"><b>$pmtxt[160]:</b></td>
     <td><select name="inside"><option value="0">$pmtxt[159]</option><option value="1">$pmtxt[23]</option><option value="2">$pmtxt[25]</option><option value="3">$pmtxt[27]</option>$folderops</select></td>
    </tr><tr>
     <td width="35%" align="right"><b>$pmtxt[179]:</b></td>
     <td><input type="text" class="textinput" name="fresults" value="5" size="5" maxlength="4"></td>
    </tr><tr>
     <td width="35%" align="right"><b>$pmtxt[180]:</b></td>
     <td><input type="text" class="textinput" name="results" value="30" size="5" maxlength="4"></td>
    </tr>
   </table>
  </td>
 </tr><tr>
  <td class="win2" align="center"><input type="submit" class="button" value="$pmtxt[157]"></td>
 </form></tr>
</table>
EOT
}

sub Search2 {
	if($FORM{'search'} eq '') { &error($gtxt{'bfield'}); }
	$callt = qq~<img src="$images/search_sm.gif"> $pmtxt[156]~;

	$displaycenter = qq~<table cellpadding="4" cellspacing="1" class="border" width="97%" align="center">~;
	$fids{'1'} = $pmtxt[23];
	$fids{'2'} = $pmtxt[25];

	foreach(sort {$a <=> $b} @pmdata) {
		($folder,$mid,$sub,$puser,$message) = split(/\|/,$_);
		if($FORM{'inside'} && $FORM{'inside'} != $folder) { next; }
		while($message =~ s~{REPLY: (.*?)}(.*?){\/REPLY}~~gsi) {}
		if($message !~ /\Q$FORM{'search'}\E/sig && $sub !~ /\Q$FORM{'search'}\E/sig) { next; }
		if($FORM{'touser'} && $puser !~ /\Q$FORM{'touser'}\E/sig) { next; }
		&BC;
		loaduser($puser);

		if($userset{$puser}->[1] eq '') { $puser = $puser; }
			else { $puser = qq~<a href="$scripturl,v=memberpanel,a=view,u=$puser">$userset{$puser}->[1]</a>~; }
		++$counter;
		if($folder != $current) {
			$current = $folder;
			$displaycenter .= <<"EOT";
<tr>
 <td class="catbg" align="center" colspan="2"><span class="smalltext"><b>$fids{$folder}</b></span></td>
</tr>
EOT
			$foldercnt = 1;
		} elsif($FORM{'fresults'} && $foldercnt > $FORM{'fresults'}) { next; }
		$displaycenter .= <<"EOT";
<tr>
 <td class="win" rowspan="2" align="center" width="25"><b>$counter</b></td>
 <td class="win2">
  <table width="100%">
   <tr>
    <td><span class="smalltext"><b><a href="$scripturl,v=memberpanel,a=pm,s=display,f=$folder,m=$mid">$sub</a></b></span></td>
    <td align="right"><span class="smalltext"><b>$pmtxt[165]:</b> $puser</span></td>
   </tr>
  </table>
 </td>
</tr><tr>
 <td class="win"><span class="smalltext">$message</span></td>
</tr>
EOT
		if($FORM{'results'} && $counter > $FORM{'results'}-1) { last; }
		++$foldercnt;
	}
	if(!$counter) {
		$displaycenter .= <<"EOT";
<tr>
 <td class="win" align="center" colspan="2"><b>$pmtxt[164]</b></td>
</tr>
EOT
	}
	$displaycenter .= "</table>";
}

sub LoadBuddys {
	@blocklist = split(/\|/,$prefs[0]);
	foreach(@blocklist) {
		$blocked{$_} = 1;
	}
	@buddylist = split(/\|/,$prefs[8]);
	foreach(@buddylist) {
		($name) = split("/");
		$buddy{$name} = 1;
	}
	foreach(@blocklist) {
		if(!$buddy{$_}) { push(@buddylist,$_); }
	}
	if($_[0] == 1) { return; }
	if($URL{'p'}) { &SaveBuddys; return; }
	$callt = qq~<img src="$images/profile_sm.gif"> $profiletxt[257]~;
	$morecaller = $pmtxt[170];

	$displaycenter = <<"EOT";
<table width="97%" cellpadding="4" cellspacing="1" class="border" align="center">
 <tr><form action="$scripturl,v=memberpanel,a=pm,s=blist,p=1" method="post" name="msend">
  <td class="catbg" align="center"><span class="smalltext"><b>$pmtxt[171]</b></span></td>
 </tr><tr>
  <td class="win2"><span class="smalltext">$pmtxt[176]</span></td>
 </tr><tr>
  <td class="win"><table cellpadding="5" cellspacing="0" align="center">
   <tr>
    <td align="center" valign="top" width="150"><span class="smalltext"><b>$pmtxt[172]</b></span></td>
    <td align="center" width="200"><span class="smalltext"><b>$pmtxt[173]</b><br>$pmtxt[174]</span></td>
    <td align="center" width="200" valign="top"><span class="smalltext"><b>$pmtxt[181]</span></td>
   </tr>
EOT
	foreach(@buddylist) {
		($name,$display) = split("/");
		++$counter;
		if(!-e("$members/$name.dat")) { next; }
		$sel{''} = '';
		$sel{$blocked{$name}} = ' selected';
		$displaycenter .= <<"EOT";
<tr>
 <td align="center"><input type="text" class="textinput" name="name_$counter" value="$name" size="25"></td>
 <td align="center"><input type="text" class="textinput" name="display_$counter" value="$display" size="30"></td>
 <td align="center"><select name="block_$counter"><option value="1"$sel{1}>$gtxt{'33'}</option><option value="0"$sel{''}>$gtxt{'32'}</option></select></td>
</tr>
EOT
	}
	$displaycenter .= <<"EOT";
  </table></td>
 </tr><tr>
  <td class="catbg" align="center"><span class="smalltext"><b>$pmtxt[175]</b></span></td>
 </tr><tr>
  <td class="win"><table cellpadding="5" cellspacing="0" align="center">
   <tr>
    <td align="center" valign="top" width="150"><input type="text" class="textinput" name="name_new"size="25"></td>
    <td align="center" width="200"><input type="text" class="textinput" name="display_new" size="30"></td>
    <td align="center" width="200"><select name="block_new"><option value="1">$gtxt{'33'}</option><option value="0" selected>$gtxt{'32'}</option></select></td>
   </tr>
  </table></td>
 </tr><tr>
  <td class="win2" align="center"><input type="hidden" name="cnt" value="$counter"><input type="submit" class="button" value=" $pmtxt[104] "></td>
 </form></tr>
</table>
EOT
}

sub SaveBuddys {
	my($o);

	if($URL{'p'} == 2) {
		LoadBuddys(1);
		foreach(@buddylist) {
			($name,$display) = split("/");
			$printdata .= "$name/$display|";
			$onlist{$name} = 1;
		}
		if(!$onlist{$URL{'buddy'}} && -e("$members/$URL{'buddy'}.dat")) { $printdata .= "$URL{'buddy'}/|"; }

		fopen(FILE,">$members/$username.prefs");
		print FILE "$prefs[0]\n$prefs[1]\n$prefs[2]\n$prefs[3]\n$prefs[4]\n$prefs[5]\n$prefs[6]\n$prefs[7]\n$printdata\n";
		fclose(FILE);
	} else {
		for($o = 1; $o <= $FORM{'cnt'}; ++$o) {
			$FORM{"display_$o"} =~ s/\n//gsi;
			$FORM{"display_$o"} =~ s~\/~&#47;~gsi;

			$findname = $FORM{"name_$o"};
			loaduser($findname);

			$fndun = 0;

			if($userset{$findname}->[1] eq '') {
				$FORM{"name_$o"} =~ s/ //gsi;
				fopen(FILE,"$members/List.txt");
				@list = <FILE>;
				fclose(FILE);
				foreach(@list) {
					chomp $_;
					loaduser($_);
					$userset{$_}->[1] =~ s/ //gsi;
					if(lc($userset{$_}->[1]) eq lc($findname) || lc($_) eq lc($findname)) { $findname = $_; $fndun = 1; last; }
				}
			} else { $fndun = 1; }

			if(!$fndun || $findname eq '' || $onlist{$findname}) { next; }

			$onlist{$findname} = 1;
			$printdata .= qq~$findname/$FORM{"display_$o"}|~;
			if($FORM{"block_$o"}) { $blockdata .= qq~$findname|~; }
		}

		$findname = $FORM{'name_new'};
		loaduser($findname);

		$fndun = 0;

		if($userset{$findname}->[1] eq '') {
			$findname =~ s/ //gsi;
			fopen(FILE,"$members/List.txt");
			@list = <FILE>;
			fclose(FILE);
				foreach(@list) {
				chomp $_;
				loaduser($_);
				$userset{$_}->[1] =~ s/ //gsi;
				if(lc($userset{$_}->[1]) eq lc($findname) || lc($_) eq lc($findname)) { $findname = $_; $fndun = 1; last; }
			}
		} else { $fndun = 1; }

		if($fndun || $findname eq '' || $onlist{$findname}) {
			$FORM{'display_new'} =~ s/\n//gsi;
			$FORM{'display_new'} =~ s~\/~&#47;~gsi;
			$printdata .= qq~$findname/$FORM{'display_new'}|~;
			if($FORM{'block_new'}) { $blockdata .= "$findname|"; }
		}

		fopen(FILE,">$members/$username.prefs");
		print FILE "$blockdata\n$prefs[1]\n$prefs[2]\n$prefs[3]\n$prefs[4]\n$prefs[5]\n$prefs[6]\n$prefs[7]\n$printdata\n";
		fclose(FILE);
	}

	redirect("$scripturl,v=memberpanel,a=pm,s=blist");
}
1;