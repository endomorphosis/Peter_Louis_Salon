################################################
# E-Blah Bulliten Board Systems       Platinum #
################################################
# Copyright (c) 2001 - 2004 e-blah!            #
# All Rights Reserved.                         #
################################################

CoreLoad('BoardIndex',1);

sub LoadIndex {
	if($URL{'a'} eq 'tog') { &Tog; }
	my($moderate,$postsandtops);
	$gdisable = 1;
	$title = $mbname;
	&header;
	foreach (@catbase) {
		($nme,$id) = split(/\|/,$_);
		if($id eq $URL{'c'}) { $catid = $id; $catname = $nme; last; }
	}
	if($URL{'c'} ne '') { $dirlist = qq~<a href="$surl">$gtxt{'44'}</a> &nbsp;<b>&#155; &nbsp;$catname~; }
		else { $dirlist = qq~<b>$gtxt{'44'}~; }

	$ebout .= <<"EOT";
<table cellspacing="1" width="100%" class="border">
 <tr>
  <td class="win" height="22"><span class="smalltext">&nbsp;$dirlist</b></span></td>
 </tr>
</table><br>
EOT
	if($enews) {
		$news = <<"EOT";
 <tr>
  <td class="titlebg"  colspan="5"><b><img src="$images/site_sm.gif"> $mbname $var{'4'}</b></td>
 </tr><tr>
  <td class="win" colspan="5" height="40" align="center">
  <script language="JavaScript" type="text/javascript" src="$newsloc"></script>
  <div align="center" onLoad="start()">
  <ilayer id="ns4slider" width="&{swidth};" height="&{sheight};">
  <layer id="ns4slider1" height="&{sheight};" onmouseover="sspeed=0;" onmouseout="sspeed=332;">
  <script language="JavaScript">
EOT
		fopen(FILE,"$prefs/News.txt");
		@newsdisplay = <FILE>;
		fclose(FILE);
		chomp(@newsdisplay);
		$number = 0;
		foreach $message (@newsdisplay) {
			&BC;
			$message =~ s/\r//;
			$message =~ s/'/\\'/;
			$news .= "singletext[$number]='$message'\n";
			$temp = $message;
			++$number;
		}
		if($number == 1) { $news .= "singletext[$number]='$temp'\n"; }
		elsif(!$number) {
			$mbname =~ s/'/\\'/;
			$news .= "  singletext[0]='$mbname $var{'4'}'\n  singletext[1]='$mbname $var{'4'}'\n";
		}
		$news .= <<"EOT";
  </script>
  </layer></ilayer>
  <script language="JavaScript">
  if(document.all){
   document.writeln('<div style="cursor:default;position:relative;overflow:hidden;width:'+swidth+';height:'+sheight+';clip:rect(0 '+swidth+' '+sheight+' 0);" onmouseover="sspeed=0;" onmouseout="sspeed=2">')
   document.writeln('<div id="ieslider1" style="position:relative;width:'+swidth+';">')
   document.write(singletext[0])
   document.writeln('</div></div>')
  }
  if(document.getElementById&&!document.all){
   document.writeln('<div style="cursor:default;position:relative;overflow:hidden;width:'+swidth+';height:'+sheight+';clip:rect(0 '+swidth+' '+sheight+' 0);" onmouseover="sspeed=0;" onmouseout="sspeed=2">')
   document.writeln('<div id="ns6slider1" style="position:relative;width:'+swidth+';">')
   document.write(singletext[0])
   document.writeln('</div></div>')
  }
  window.onload = start;
  </script></span>
  </div>
  </td>
 </tr>
EOT
	}

	$ebout .= <<"EOT";
<table cellpadding="5" cellspacing="1" class="border" width="100%">
 $news<tr>
  <td class="titlebg" colspan="2" align="center"><b>$boardindex[2]</b></td>
  <td class="titlebg" width="55" align="center"><span class="smalltext"><b>$boardindex[3]</span></b></td>
  <td class="titlebg" width="55" align="center"><span class="smalltext"><b>$boardindex[4]</span></b></td>
  <td class="titlebg" width="200" align="center"><b>$boardindex[5]</b></td>
 </tr>
EOT
	fopen(FILE,"$prefs/Active.txt");
	@activelist = <FILE>;
	fclose(FILE);
	$activecnt = @activelist;
	chomp @activelist;
	$hidec = 0;
	$memcnt = 0;
	$gcnt = 0;

	&LoadColors;

	foreach(@activelist) {
		($luser,$ltime,$hidden,$t,$bview) = split(/\|/,$_);
		loaduser($luser);
		if($userset{$luser}->[1] eq '') { $fndu = $luser; } else { $fndu = $userset{$luser}->[1]; }
		push(@quicksort,"$fndu|$luser|$hidden|$bview");
	}

	foreach(sort{lc($a) cmp lc($b)} @quicksort) {
		($t,$luser,$hidden,$bview) = split(/\|/,$_);
		++$B{$bview};
		if($userset{$luser}->[1] ne '') {
			$lostuser = $gcolors{$userset{$luser}->[4]} ? qq~<font color="$gcolors{$userset{$luser}->[4]}"><b>$userset{$luser}->[1]</b></font>~ : qq~$userset{$luser}->[1]~;
			if($hidden) {
				++$hidec;
				if($settings[4] eq 'Administrator') { $memberson .= qq~<a href="$scripturl,v=memberpanel,a=view,u=$luser">$lostuser</a>, ~; }
					else { next; }
			} else {
				++$memcnt;
				$memberson .= qq~<a href="$scripturl,v=memberpanel,a=view,u=$luser">$lostuser</a>, ~;
			}
		} elsif($botsearch{$luser}) {
			$memberson .= qq~<span class="onlinebots">$botsearch{$luser}</span>, ~;
			++$memcnt;
		} else { ++$gcnt; }
	}

	$bcnt = 0; $bp = 0; $bt = 0; $catcnt = 0;

	if($username ne 'Guest') {
		fopen(FILE,"$members/$username.log");
		@logged = <FILE>;
		fclose(FILE);
		chomp @logged;
	}
	if($settings[33]) { $link = "$surl,v=mindex,n=1,b="; } else { $link = "$surl".'b='; }

	foreach(split(/\|/,$Osettings[42])) { $catshow{$_} = 1; }

	foreach(@boardbase) {
		($id) = split("/",$_);
		$board{$id} = $_;
	}

	foreach(@catbase) {
		($t,$boardid,$t,$t,$t,$subcats) = split(/\|/,$_);
		$catbase{$boardid} = $_;
		if(!$URL{'c'} && $subcats) {
			foreach(split(/\//,$subcats)) { $noshow{$_} = 1; }
		}
		push(@cats,$boardid);
	}

	foreach(@cats) {
		if($noshow{$_} || $URL{'c'} && $URL{'c'} ne $_) { next; }
		($name,$boardid,$memgroups,$boardlist,$message,$subcats) = split(/\|/,$catbase{$_});

		if(GetMemberAccess($memgroups) == 0) { next; }

		&BC; $catdesc = $message ? qq~<span class="smalltext"><div style="padding-left: 3px; line-height: 130%;">$message</div></span>~ : '';

		$rollup = !$catshow{$boardid} ? 'minimize' : 'expand';
		$rollup = $username ne 'Guest' ? qq~<a href="$scripturl,a=tog,cat=$boardid"><img src="$images/$rollup.gif" border="0"></a>~ : '';
		$ebout .= <<"EOT";
<tr>
 <td colspan="6" class="catbg">
  <table cellpadding="1" cellspacing="0" width="100%">
   <tr>
    <td class="catbgtext"><b><a href="$scripturl,c=$boardid">$name</a></b>$catdesc</td>
    <td align="right" valign="top">$rollup</td>
   </tr>
  </table>
 </td>
</tr>
EOT
		if($catshow{$boardid}) { $catdisabled = 1; next; }
		if($boardlist eq '') { GetSubCats(); next; }
		foreach $bid (split("/",$boardlist)) {
			if($board{$bid} eq '') { next; } # Invalid board data
			($t,$message,$binfo[1],$binfo[2],$binfo[3],$binfo[4],$binfo[5],$binfo[6],$t,$t,$binfo[9],$t,$t,$redir) = split("/",$board{$bid});

			$lastuser = $lastdate = $icon = $bstat = $postsandtops = $infrm = '';

			if(GetMemberAccess($binfo[9]) == 0) { next; }

			# Get the post totals
			fopen(FILE,"$boards/$bid.ino");
			@postinfo = <FILE>;
			fclose(FILE);
			chomp @postinfo;
			$posts  = $postinfo[1] > 0 ? MakeComma($postinfo[1]) : 0;
			$topics = $postinfo[0] > 0 ? MakeComma($postinfo[0]) : 0;

			# Compile the mods list
			@mods = split(/\|/,$binfo[1]);
			&Mods;
			if($modz) { $modz = qq~<div class="smalltext" style="line-height: 200%;"><b>$ltxt[7]:</b> $modz</div>~; }

			# Users browsing
			if($sauser && $B{$bid}) { $infrm = qq~ ($B{$bid} $boardindex[45])~; }

			# Get last thread info (and look in log for new threads)
			fopen(FILE,"$boards/$bid.msg");
			while(<FILE>) {
				chomp;
				($t,$mtitle,$t,$t,$t,$t,$t,$icon,$lastdate,$lastuser) = split(/\|/,$_);
				$icon = $icon ne 'xx' ? qq~<img src="$images/$icon.gif">~ : '';
				last;
			}
			fclose(FILE);

			GetBoardData($mtitle,$lastdate,$lastuser,$bid);

			if($foundnew) { $new = 'off'; $alt = $boardindex[9]; }
				else { $new = 'on'; $alt = $boardindex[10]; }

			# Restricted posting?
			if($binfo[3] > 1 || $binfo[4] > 1) { $bstat = qq~<div class="smalltext" style="line-height: 200%;"><b>$boardindex[6]</b></div>~; }
			if($binfo[3] == 3 && $binfo[4] == 3) { $new = "locked"; }

			# Basic board info (like description)
			$message =~ s/&#47;/\//gsi;
			&BC;

			# Info blocked by permissions?
			if($binfo[6]) {
				$bstat .= qq~<div class="smalltext" style="line-height: 200%;"><b>$boardindex[7]</b></div>~;
				if(($Blah{"$bid\_pw"} ne $binfo[6] && $settings[4] ne 'Administrator') || $username eq 'Guest') {
					$icon = '';
					$lastpost = $gtxt{'13'};
					$bt -= $postinfo[0];
					$bp -= $postinfo[1];
					$topics = $posts = '?';
					$new = 'locked';
					$lastdate = 1;
				}
			}

			# Redirect forum, or regular?
			if($redir) {
				fopen(ADD,"$boards/$bid.hits");
				$nump = MakeComma( <ADD> ) || 0;
				fclose(ADD);

				$postsandtops = <<"EOT";
  <td class="win" align="center" colspan="2"><span class="smalltext"><b>$boardindex[71]:</b> $nump</span></td>
EOT
				$icon = '';
				$lastpost = $boardindex[72];
				$new = 'redirect';
				$bid = $bid.'" target="_new"';
			} else {
				$postsandtops = <<"EOT";
  <td class="win" align="center"><b>$topics</b></td>
  <td class="win" align="center"><b>$posts</b></td>
EOT
				$bp += $postinfo[1];
				$bt += $postinfo[0];
				++$bcnt;
			}

			$ebout .= <<"EOT";
 <tr>
  <td class="win" align="center" width="25" valign="top" style="padding: 7px;"><img src="$images/$new.gif" alt="$alt"></td>
  <td class="win2" valign="top"><b><a href="$link$bid">$binfo[2]</a></b><span class="smalltext">$infrm<div style="line-height: 130%; padding-left: 2px;">$message$bstat$modz</div></span></td>
$postsandtops
  <td class="win2" width="205"><table cellpadding="2" cellspacing="0">
   <tr>
    <td width="20" align="center">$icon</td>
    <td valign="top" width="190"><span class="smalltext">$lastpost</span></td>
   </tr>
  </table></td>
 </tr>
EOT
		}
		if($subcats) { GetSubCats(); }
		++$catcnt;
	}

	if($bcnt == 0 && !$catdisabled) {
		$ebout .= <<"EOT";
 <tr>
  <td class="win" align="center" colspan="5"><br>$boardindex[14]<br><br></td>
 </tr>
EOT
	} else {
		if($username ne 'Guest' && $amar && $URL{'c'} eq '') { $maread = qq~<span class="smalltext"><a href="$scripturl,v=mark,l=bindex">$img{'markread'}</a></span>~; }
		if($settings[15] > 0) { $settings[15] = "+$settings[15]"; }
		if($settings[15]) { $timezone = " $settings[15] $gtxt{'3'}"; }
		$ebout .= <<"EOT";
<tr>
 <td colspan="5" class="catbg">
  <table cellpadding="0" cellspacing="0" width="100%">
   <tr>
    <td class="catbgtext"><span class="smalltext">$boardindex[67]$timezone</span></td>
    <td class="catbgtext" align="right">$maread</td>
   </tr>
  </table>
 </td>
</tr>
EOT
	}
	if($bcnt == 1) { $r = $boardindex[49]; } else { $r = $boardindex[50]; }
	$bp = MakeComma($bp);
	$bt = MakeComma($bt);
	if(!$lasttopic) { $lasttopic = $boardindex[24]; }
	$ebout .= <<"EOT";
</table>
<br>
<table cellpadding="0" cellspacing="0" width="100%">
 <tr>
  <td align="left" valign="top" width="50%">
   <table cellpadding="4" cellspacing=1 class="border" width="100%">
    <tr>
     <td class="titlebg"  colspan="2"><b><img src="$images/open_thread.gif" style="vertical-align: middle"> $boardindex[18]</b></td>
    </tr><tr>
     <td class="win2" align="center" width="33"><img src="$images/boardinfo.gif"></td>
     <td class="win2">
      <table cellpadding="2" cellspacing="0" width="100%">
       <tr><td colspan="2"><span class="smalltext"><b>$boardindex[19]</b></span></td><td align="right" colspan="2"><span class="smalltext"><b><a href="$scripturl,v=portal">$boardindex[48]</a></b></span></td></tr>
       <tr><td colspan="4" height="2" class="border"></td></tr>
       <tr><td width="150"><span class="smalltext"><b>$gtxt{'6'}:</b></span></td><td><span class="smalltext">$catcnt</span></td><td width="150"><span class="smalltext"><b>$gtxt{'7'}:</b></span></td><td><span class="smalltext">$bcnt</span></td></tr>
       <tr><td><span class="smalltext"><b>$gtxt{'8'}:</b></span></td><td><span class="smalltext">$bt</span></td><td><span class="smalltext"><b>$gtxt{'9'}:</b></span></td><td><span class="smalltext">$bp</span></td></tr>
       <tr><td colspan="2" align="center"><span class="smalltext">$lasttopic</span></td><td colspan="2" align="center"><span class="smalltext"><a href="$scripturl,v=search,p=topten">$boardindex[25]</a></span></td></tr>
      </table></td>
    </tr>
EOT
	if(($eclick && (!$hideclog || $settings[4] eq 'Administrator')) || ($uextlog && (!$hideelog || $settings[4] eq 'Administrator'))) {
		fopen(FILE,"$prefs/ClickLog.txt");
		while (<FILE>) { ++$clcnt; }
		fclose(FILE);
		$logcnt = $logcnt > 59 ? sprintf("%.0f",($logcnt/60))." $gtxt{'3'}" : "$logcnt $gtxt{'2'}";
		$clcnt = MakeComma($clcnt);
		$ebout .= <<"EOT";
    <tr>
     <td class="win" align="center"><img src="$images/clicklog.gif"></td>
     <td class="win">
      <table cellpadding="2" cellspacing="0" width="100%">
EOT
		if($eclick && (!$hideclog || $settings[4] eq 'Administrator')) {
			$ebout .= <<"EOT";
     <tr><td><span class="smalltext">$boardindex[51] <b>$logcnt</b> $boardindex[52] <b>$clcnt $boardindex[53]</b>.</b></span></td></tr>
EOT
		}
		if($uextlog  && (!$hideelog || $settings[4] eq 'Administrator')) { $ebout .= qq~<tr><td><span class="smalltext"><b><a href="$scripturl,v=stats">$boardindex[22] $boardindex[21]</a></b></span></td></tr>~; }
		$ebout .= <<"EOT";
      </table>
     </td>
    </tr>
EOT
		$dacolor = 'win2';
	} else { $dacolor = 'win'; }
	if($showevents) {
		CoreLoad('Calendar');
		($t,$t,$t,$tday,$thismon,$qyear) = gmtime(time+(3600*($settings[15]+$settings[31]+$gtoff)));
		if($enbdays) { &GetBirthdays; }
		&GetEvents;

		if($upevents) { # Upcoming eventz
			for($c = 1; $c <= $upevents; ++$c) {
				($t,$t,$t,$ttday,$tthismon,$tyear) = gmtime((time+(3600*($settings[15]+$settings[31]+$gtoff)))+(86400*$c));
				$temp = $Events{"$tthismon|$ttday|$tyear"}.$Events{"$tthismon|$ttday|90"};
				foreach(@useonce) {
					if($temp =~ /\Q$_\E/) {
						if($used{$_}) { $temp =~ s~\Q$_\E~~gis; }
						$used{$_} = 1;
					}
				}
				$tom .= $temp;
			}
		}
		$tom =~ s/, \Z//;

		if($enbdays) {
			$bdays = $BirthDay{"$thismon|$tday"};
			$bdays =~ s/, \Z//;
			$bdays = $bdays || "<i>$boardindex[46]</i>"; # None!
		}

		$events = $Events{"$thismon|$tday|$qyear"}.$Events{"$thismon|$tday|90"};
		$events =~ s/, \Z//;
		$events = $events || "<i>$boardindex[46]</i>"; # None!

		$ebout .= <<"EOT";
    <tr>
     <td class="$dacolor" align="center"><img src="$images/cal_sm.gif"></td>
     <td class="$dacolor">
      <table cellpadding="2" cellspacing="0" width="100%">
EOT
		if($Events{"$thismon|$tday|$qyear"} || $Events{"$thismon|$tday|90"} || $BirthDay{"$thismon|$tday"}) {
			$ebout .= <<"EOT";
<tr><td colspan="2"><span class="smalltext"><b>$boardindex[27]</b></span></td></tr>
<tr><td colspan="2" height="2" class="border"></td></tr>
EOT
			if($enbdays) { $ebout .= qq~<tr><td width="80" valign="top"><span class="smalltext"><b>$bdaysc $boardindex[28]:</b></span></td><td><span class="smalltext">$bdays</span></td></tr>~; }
			$ebout .= <<"EOT";
<tr><td width="80" valign="top"><span class="smalltext"><b>$eventsc $boardindex[29]:</b></span></td><td><span class="smalltext">$events</span></td></tr>
EOT
		} else { $ebout .= qq~<tr><td><span class="smalltext"><b>$boardindex[30] $boardindex[31]</b></span></td></tr>~; }

		if($tom) {
			$ebout .= <<"EOT";
<tr><td colspan="2"><span class="smalltext"><br><b>$boardindex[70]</b></span></td></tr>
<tr><td colspan="2" height="2" class="border"></td></tr>
<tr><td colspan="2"><span class="smalltext">$tom</span></td></tr>
EOT
		}
		$ebout .= <<"EOT";
      </table>
     </td>
    </tr>
EOT
	}
	$ebout .= <<"EOT";
   </table>
  </td>
  <td>&nbsp;</td>
  <td align="right" valign="top" width="50%">
   <table cellpadding="4" cellspacing="1" class="border" width="100%">
    <tr>
     <td class="titlebg" colspan="2"><b><img src="$images/profile_sm.gif" style="vertical-align: middle"> $boardindex[32]</b></td>
    </tr>
EOT
	fopen(FILE,"$members/LastMem.txt");
	@lm = <FILE>;
	fclose(FILE);
	chomp @lm;

	$memberson =~ s/, \Z//i;
	if($memberson eq '') { $memberson = "<i>$boardindex[69]</i>"; }

	fopen(FILE,"$prefs/MaxLog.txt");
	@maxlogged = <FILE>;
	fclose(FILE);
	chomp @maxlogged;
	$maxtime = get_date($maxlogged[1]);

	loaduser($lm[0]);
	$allcnt = MakeComma($gcnt+$hidec+$memcnt);
	$lm[1] = MakeComma($lm[1]);
	$ebout .= <<"EOT";
    <tr>
     <td class="win" align="center" width="33"><img src="$images/online.gif"></td>
     <td class="win">
      <table cellpadding="2" cellspacing="0" width="100%">
       <tr><td><span class="smalltext"><b>$allcnt $boardindex[54] $activeuserslog $gtxt{'2'}</b></span></td></tr>
       <tr><td height="2" class="border"></td></tr>
       <tr><td><span class="smalltext"><b>$memcnt $boardindex[56]</b> ($boardindex[66] $hidec $boardindex[57]) $boardindex[68] <b>$gcnt $boardindex[59]</b> $boardindex[58]</b></span></td></tr>
       <tr><td><span class="smalltext">$memberson</span></td></tr>
EOT
	if($whereis) { $ebout .= qq~<tr><td><span class="smalltext"><b><a href="$scripturl,v=stats,a=whereis">$boardindex[36]</a></b></span></td></tr>~; }
	$ebout .= <<"EOT";
      </table>
     </td>
    </tr>
    <tr>
     <td class="win2" align="center"><img src="$images/profile_lg.gif"></td>
     <td class="win2">
      <table cellpadding="2" cellspacing="0" width="100%">
       <tr><td><span class="smalltext">$boardindex[64] $lm[1] $boardindex[65].
       <br>$boardindex[63] "<b><a href="$scripturl,v=memberpanel,a=view,u=$lm[0]">$userset{$lm[0]}->[1]</a></b>".
       <br><a href="$scripturl,v=members"><b>$boardindex[60]</b></a>.
       <br>$boardindex[61] <b>$maxlogged[0]</b> $boardindex[62] <b>$maxtime</b>.</span></td></tr>
      </table>
     </td>
    </tr>
EOT
	if($invfri && $username ne 'Guest') { CoreLoad('Invite'); &InviteBI; }
	$ebout .= <<"EOT";
   </table>
  </td>
 </tr><tr>
  <td colspan="3">&nbsp;</td>
</tr>
EOT
	if($username eq 'Guest') { $ebout .= qq~<tr><td colspan="3"><br>$guestlogin</td></tr>~; }
	$ebout .= "</table>";

	&footer;
	exit;
}

sub Tog {
	if($username eq 'Guest') { &error($boardindex[73].'-register-'); }
	is_member();
	VerifyBoard(); # So someone can't overload the datafile ...
	my($delMZ);
	$URL{'cat'} =~ s~\A\s+~~; # As always ... hack protection
	$URL{'cat'} =~ s~\s+\Z~~;
	$URL{'cat'} =~ s~[\n\r]~~g;

	$Osettings[42] = '';
	foreach(split(/\|/,$settings[42])) {
		if($URL{'cat'} eq $_) { $delMZ = 1; next; }
		$Osettings[42] .= "$_|";
	}
	if(!$delMZ && $catallow{"$URL{'cat'}"}) { $Osettings[42] .= "$URL{'cat'}"; }

	fopen(FILE,"+>$members/$username.dat");
	for($q = 0; $q < $usersetcount; $q++) { print FILE "$Osettings[$q]\n"; }
	fclose(FILE);
}

sub GetBoardData {
	my($mtitle,$lastdate,$lastuser,$board) = @_;
	my($found,$isnew,$tdate);

	$tdate = get_date($lastdate);
	if($username ne 'Guest') {
		foreach $log (@logged) {
			($lboard,$lasttime) = split(/\|/,$log);
			if($lboard eq $board || $lboard eq 'AllBoards') {
				$isnew = $lasttime-$lastdate;
				if($isnew >= 0) { $found = 1; }
				last;
			}
		}
	}
	$foundnew = $username eq 'Guest' || $found || $lastdate eq '' ? 1 : 0;

	# Get last post info
	loaduser($lastuser);
	if($userset{$lastuser}->[1]) { $lpby = qq~<a href="$scripturl,v=memberpanel,a=view,u=$lastuser">$userset{$lastuser}->[1]</a>~; }
		else { $lpby = CensorList($lastuser); }
	$mtitle = CensorList($mtitle);
	if(length($mtitle) > 24) { $mtitle = substr($mtitle,0,22)."..."; }
	$lpin = $mtitle ne '' ? qq~<a href="$surl,b=$board,v=display,m=latest">~.$mtitle."</a>" : $gtxt{'13'};
	$lastpost = $lpby ne '' ? qq~<div><b>$lpin</b></div><div><b>$boardindex[11]:</b> $lpby</div><div align="right">$tdate</div>~ : $gtxt{'13'};

	if($max < $lastdate) { $max = $lastdate; $lasttopic = qq~<a href="$surl,b=$board,v=display,m=latest" title="$boardindex[13]">$mtitle</a>~; }
}

sub GetMSubs {
	my($msubcats,@boards);
	$noloop{$_[0]} = 1;
	($t,$t,$t,$boardlist,$t,$msubcats) = split(/\|/,$catbase{$_[0]});
	push(@boardlists, split(/\//,$boardlist) );

	if($msubcats) { foreach $subcat (split(/\//,$msubcats)) { GetMSubs($subcat); } }
}

sub GetSubCats {
	my($cats,$msubcats);
	@boardlists = ();
	foreach $nohere (split(/\//,$subcats)) {
		if($catbase{$nohere} eq '') { next; }
		$subboards = $maxlastdate = 0;
		$icon = $sflist = $lastpost = $posts = $topics = '';
		($name,$t,$memgroups,$boardlist,$message,$msubcats) = split(/\|/,$catbase{$nohere});
		&BC;
		if(!GetMemberAccess($memgroups)) { next; }
		@boardlists = split(/\//,$boardlist);

		foreach $subcat (split(/\//,$msubcats)) { GetMSubs($subcat); }
		foreach $board (@boardlists) {
			++$subboards;
			($daid,$t,$t,$sflname,$t,$t,$t,$binfo[6],$t,$t,$t,$t,$t,$redir) = split("/",$board{$board});
			if($redir || ($binfo[6] ne '' && (($Blah{"$board\_pw"} ne $binfo[6] && $settings[4] ne 'Administrator') || $username eq 'Guest'))) { next; }

			if($subboards <= 4) { $sflist .= qq~<a href="$surl,b=$daid">$sflname</a>, ~; }

			# Get the post totals
			fopen(FILE,"$boards/$board.ino");
			@postinfo = <FILE>;
			fclose(FILE);
			chomp @postinfo;
			$posts  += $postinfo[1];
			$topics += $postinfo[0];
			$bp += $postinfo[1];
			$bt += $postinfo[0];
			++$bcnt;

			fopen(FILE,"$boards/$board.msg");
			while(<FILE>) {
				chomp;
				($t,$mtitle,$t,$t,$t,$t,$t,$icon,$lastdate,$lastuser) = split(/\|/,$_);
				if($lastdate > $maxlastdate) { $maxlastdate = $lastdate; $lastpost = "$board|$mtitle|$icon|$lastuser"; }
				last;
			}
			fclose(FILE);
		}
		$sflist =~ s/, \Z//g;
		if($sflist eq '') { $sflist = $boardindex[46]; }

		if($lastpost) {
			($board,$mtitle,$icon,$lastuser) = split(/\|/,$lastpost);
			GetBoardData($mtitle,$maxlastdate,$lastuser,$board);

			$icon = $icon ne 'xx' && $icon ne '' ? qq~<img src="$images/$icon.gif">~ : '';

			if($foundnew) { $new = 'cat_off'; $alt = $boardindex[9]; }
				else { $new = 'cat_on'; $alt = $boardindex[10]; }
		} else { $new = 'cat_off'; $alt = $boardindex[9]; $lastpost = $gtxt{'13'}; }

		$posts  = MakeComma($posts);
		$topics = MakeComma($topics);

		$ebout .= <<"EOT";
 <tr>
  <td class="win" align="center" width="25" valign="top" style="padding: 7px;"><img src="$images/$new.gif" alt="$alt"></td>
  <td class="win2" valign="top"><b><a href="$scripturl,c=$nohere">$name</a></b><span class="smalltext"><div style="line-height: 130%; padding-left: 2px;">$message<div style="line-height: 200%;"><b>$boardindex[74]:</b> $sflist</div></div></span></td>
  <td class="win" align="center"><b>$topics</b></td>
  <td class="win" align="center"><b>$posts</b></td>
  <td class="win2" width="205"><table cellpadding="2" cellspacing="0">
   <tr>
    <td width="20" align="center">$icon</td>
    <td valign="top" width="190"><span class="smalltext">$lastpost</span></td>
   </tr>
  </table></td>
 </tr>
EOT
		$catdisabled = 1;
	}
}

sub GetMemberAccess {
	my($grpaccess) = @_;
	if($grpaccess eq '') { return(1); }
	$boot = 1;
	foreach $group (split(",",$grpaccess)) {
		if($group eq $settings[4]) { $boot = 0; }
		if($group eq 'member' && $username ne 'Guest') { $boot = 0; }
	}
	if($boot && $settings[4] ne 'Administrator') { return(0); }
	return(1);
}
1;