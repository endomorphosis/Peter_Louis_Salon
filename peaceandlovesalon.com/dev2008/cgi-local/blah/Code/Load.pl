################################################
# E-Blah Bulliten Board Systems       Platinum #
################################################
# Copyright (c) 2001 - 2004 e-blah!            #
# All Rights Reserved.                         #
################################################

require("$language/Load.lng");

# Quick errors for restricted areas
sub is_admin  { error("$ltxt[1]-admin-",1) if $settings[4] ne 'Administrator'; }
sub is_mod    { error($ltxt[2],1) if($settings[4] ne 'Administrator' && !$ismod); }
sub is_member {
	if(!$settings[23]) { return(); }
	elsif($settings[23] eq 'ADMIN') { error("$ltxt[24] $ltxt[26]"); }
	elsif($settings[23] eq 'EMAIL') { error("$ltxt[24] $ltxt[25]"); }
	elsif($settings[23] eq 'EMAIL|ADMIN') { error("$ltxt[24] $ltxt[27]"); }
		else { error($ltxt[23]); }
}

sub BoardCheck {
	my($bfnd,$nump);

	fopen(FILE,"$boards/bdscats.db");
	@catbase = <FILE>;
	fclose(FILE);
	chomp @catbase;

	fopen(FILE,"$boards/bdindex.db");
	@boardbase = <FILE>;
	fclose(FILE);
	chomp @boardbase;

	if($URL{'b'} eq '') { return; }

	foreach (@boardbase) {
		($boardid,$binfo[0],$binfo[1],$binfo[2],$binfo[3],$binfo[4],$binfo[5],$binfo[6],$binfo[7],$binfo[8],$binfo[9],$binfo[10],$binfo[11],$binfo[12]) = split("/",$_);
		if($URL{'b'} eq $boardid) { $bfnd = 1; last; }
	}

	error($ltxt[5]) if !$bfnd;

	if($binfo[12] ne '') { # This board redirects, increase hits and exit
		fopen(ADD,"+<$boards/$URL{'b'}.hits");
		$nump = <ADD> || 0;
		seek(ADD,0,0);
		truncate(ADD,0);
		print ADD $nump+1,"\n";
		fclose(ADD);

		$binfo[12] =~ s/&#47;/\//g;
		redirect($binfo[12]);
	}

	# Lets start applying this boards permissions!
	@mods = split(/\|/,$binfo[1]);
	foreach (@mods) {
		if($username eq $_) { $ismod = 1; last; }
	}

	foreach (@catbase) {
		($nme,$id,$grps,$input) = split(/\|/,$_);
		if($input ne '') { @cats = split("/",$input); } else { next; }
		foreach (@cats) {
			if($_ eq $URL{'b'}) { $catid = $id; $catname = $nme; $memgrp = $grps; last; }
		}
	}

	if($memgrp ne '' || $binfo[9] ne '') {
		$con = '';
		$totalmemgrps = "$binfo[9],$memgrp";
		@mgrps = split(",",$totalmemgrps);

		foreach $grp (@mgrps) {
			if($grp eq $settings[4]) { $con = 1; }
			if($grp eq 'member' && $username ne 'Guest') { $con = 1; }
		}

		if($con eq '' && $settings[4] ne 'Administrator') { error($ltxt[5]); }
	}
	if($binfo[6] ne '') { CoreLoad('BoardLock'); Password(); }
	if($binfo[10]) { $allowrate = 1; }
	if($binfo[11]) { $uallow = 0; }

	$boardnm = $binfo[2];
}

sub Mods {
	$modz = '';
	foreach(@mods) {
		loaduser($_);
		if($userset{$_}->[1] eq '') { next; }
		$modz .= qq~<a href="$scripturl,v=memberpanel,a=view,u=$_">$userset{$_}->[1]</a>, ~;
	}
	if($modz eq '') { return(); }
	$modz =~ s/, \Z//gi;
}

sub loaduser {
	my($user) = $_[0];   # DO NOT ADD LOADUSER TO MY STATEMENT!
	if($userset{$user}->[1] || $user eq '') { return; } # User is already loaded

	fopen(LOADUSER,"$members/$user.dat");
	@loaduser = <LOADUSER>;
	fclose(LOADUSER);

	for($i = 0; $i < @loaduser; $i++) {
		chomp $loaduser[$i];
		if($i == 12 && (($hiddenmail == 2 && $username eq 'Guest') || ($hiddenmail == 1))) { $userset{$user}->[$i] = 1; next; }
		$userset{$user}->[$i] = $loaduser[$i];
	}

	$userset{$user}->[1] = CensorList($userset{$user}->[1]);
}

sub LMG {
	fopen(FILE,"$prefs/Ranks.txt");
	@memgrps = <FILE>;
	fclose(FILE);
	chomp @memgrps;
	$cnt = 0;
	foreach(@memgrps) {
		($grpname,$grpper) = split(/\|/,$_);
		$memgrps[$cnt] = $grpname;
		$memgrpp[$cnt] = $grpper;
		++$cnt;
	}
	&GMS;
	&LMGS;
}

sub StarCreator {
	my($starr);
	my($input) = @_;
	if($starcount{$input} < 1 || $starcount{$input} > 51) { $starcount{$input} = 1; }
	if($star{$input} eq '') { return(); }
	for($g = 0; $g < $starcount{$input}; $g++) { $starr .= qq~<img src="$images/$star{$input}">~; }
	return($starr);
}

sub loaduserdisplay {
	my($user) = $_[0];
	my($memberstar,$memberpic,$membericq,$memberaim,$membermsn,$memberpost,$ppd,$memberyim,$stpuser);
	if(!$LMG) { LMG(); $LMG = 1; }

	loaduser($user);
	$guest = 0;
	if($userset{$user}->[1] eq '') {
		$membername = CensorList($user);
		$membername =~ s/ \(Guest\)\Z//gis;
		$mprofile = qq~<div align="center">$gtxt{'0'}<br><br><br><br></div>~;
		$instmsg = '';
		$guest = 1;
		return;
	}

	$lastactive{$user} = get_date($userset{$user}->[28],1);
	$membername = qq~<a href="$scripturl,v=memberpanel,a=view,u=$user"><font color="$gcolors{$userset{$user}->[4]}">$userset{$user}->[1]</font></a>~;

	$memberstar = $memberstat = $team = '';
	# Member groups counter ...
	foreach(@grpsposts) {
		($num,$memgrp) = split(/\|/,$_);
		if(($userset{$user}->[3]+1) >= $num) {
			$memberstat = $memgrp;
			$memberstar = StarCreator($memgrp);
		}
	}

	# Get the group apart of, and stars ...
	if($userset{$user}->[4] ne '') {
		if($team{$userset{$user}->[4]}) { $team = 1; }
		$memberstat = $userset{$user}->[4];
		if($star{$userset{$user}->[4]}) { $memberstar = StarCreator($userset{$user}->[4]); }
	}
	foreach(@mods) {
		if($user eq $_) {
			$memberstat = $memgrps[6];
			$memberstar = StarCreator($memgrps[6]);
			last;
		}
	}
	if($userset{$user}->[4] eq 'Administrator') {
		$team = 1;
		$memberstat = $memgrps[0];
		$memberstar = StarCreator($memgrps[0]);
	}

	# Avatar
	if(!$settings[37] && $userset{$user}->[5] ne '' && $apic) {
		$pic = $userset{$user}->[5];
		if($userset{$user}->[35]) { ($height,$width) = split(/\|/,$userset{$user}->[35]); }
			else { $width = $picwidth; $height = $picheight; }
		if($userset{$user}->[5] =~ /http:/ || $userset{$user}->[32]) { $memberpic = qq~<img src="$pic" width="$width" height="$height">~; }
			else { $memberpic = qq~<img src="$avsurl/$userset{$user}->[5]">~; }
	}

	# E-mail address
	$memberemail = $userset{$user}->[12] && $hmail && $settings[4] ne 'Administrator' ? '' : qq~<a href="mailto:$userset{$user}->[2]">$img{'email'}</a> $msp~;

	# Post count
	if(!$hideposts) { $memberpost = "<b>$ltxt[9]:</b> ".MakeComma($userset{$user}->[3])."<br>"; }

	# Instant Messanging
	if($userset{$user}->[9])  { $memberaim = qq~ $msp<a href="aim:goim?screenname=$userset{$user}->[9]&message=You+there?" title="$ltxt[10] ($userset{$user}->[9])">$img{'aim'}</a>~; }
	if($userset{$user}->[10]) { $membermsn = qq~ $msp<a href="http://members.msn.com/$userset{$user}->[10]" target="msn" title="$userset{$user}->[10]">$img{'msn'}</a>~; }
	if($userset{$user}->[8])  { $membericq = qq~ $msp<a href="http://wwp.icq.com/scripts/search.dll?to=$userset{$user}->[8]" target="icq" title="$userset{$user}->[8]">$img{'icq'}</a>~; }
	if($userset{$user}->[27]) { $memberyim = qq~ $msp<a href="http://profiles.yahoo.com/$userset{$user}->[27]" target="yim" title="$userset{$user}->[27]">$img{'yim'}</a>~; }

	if(!$settings[36] && $userset{$user}->[11]) { # Signature
		$message = $userset{$user}->[11];
		&BC;
		$extra = qq~<tr><td valign="botton" class="postbody"><hr color="$color{'border'}" class="hr" size="1"><span class="smalltext">$message</span></td></tr>~;
	}
	if($showgender) { # Gender
		if($userset{$user}->[7] == 1) { $pic = "male"; $gen = $ltxt[11]; }
		elsif($userset{$user}->[7] == 2) { $pic = "female"; $gen = $ltxt[12]; }
		$gender = qq~<b>$ltxt[22]:</b> <img src="$images/$pic.gif"> $gen<br>~;
		if(!$userset{$user}->[7]) { $gender = ''; }
	}
	if($sppd) { # Posts Per Day
		($t,$t,$t,$lday,$lmonth,$lyear,$lweek) = localtime($userset{$user}->[14]);
		($t,$t,$t,$cday,$cmonth,$cyear,$cweek) = localtime(time);
		$lyear = substr($lyear,-2);
		$cyear = substr($cyear,-2);
		++$lmonth; ++$cmonth;
		$reg = (365*$lyear)+(30.42*$lmonth)+$lday;
		$now = (365*$cyear)+(30.42*$cmonth)+$cday;
		$days = ($now-$reg) || 1;
		$ppd = sprintf("<b>$ltxt[13]: </b> %.2f", $userset{$user}->[3]/$days,"<br>");
	}
	if($enablerep) { # Reputation
		$reputation = '';
		if($userset{$user}->[43] ne '') {
			$color = '';
			if($userset{$user}->[43] < 50) { $color = 'redrep'; } # Horrible
			elsif($userset{$user}->[43] > 75) { $color = 'greenrep'; } # Best
				else { $color = 'grayrep'; } # Neut

			$reputation = qq~<br><b>$gtxt{'rep'}:</b> <span class="$color">$userset{$user}->[43]%</span>~;
		}
	}

	$avatar = $memberpic || $userset{$user}->[6] ? "$memberpic<br>$userset{$user}->[6]" : '';
	$showlocationu = $showlocation && $userset{$user}->[21] ne '' ? "<b>$ltxt[41]:</b> $userset{$user}->[21]<br>" : '';

	$memberstar = qq~<div>$memberstar</div>~ if $memberstar;

	if($team) { $memberstat = "<b>$memberstat</b>"; }

	$memberstat = qq~<div style="line-height: 200%" align="center">$memberstat<br>$memberstar<br>$avatar</div><br>~ if($memberstat || $memberstar || $avatar);

	$mprofile = qq~$memberstat<div style="line-height: 140%; padding: 3px;" align="left">$gender$showlocationu$memberpost$ppd$reputation</div></span>~;
	$instmsg = "$membericq$memberaim$memberyim$membermsn";
}

sub LogPage {
	if($username eq 'Guest') { return; }
	my($lid,$ltime,$found,$lltime);

	$lltime = (time+2); # +2 prevents "Your New Post == New".


	fopen(ULOG,"$members/$username.log");
	@log = <ULOG>;
	fclose(ULOG);
	chomp @log;
	fopen(ULOG,"+>$members/$username.log");
	foreach $log (@log) {
		($lid,$ltime) = split(/\|/,$log);
		if($lid ne $URL{'m'} && $lid ne $URL{'b'} && $ltime+($logdays*86400) > time) { $tempwrite .= "$log\n"; }
	}
	if($URL{'m'} ne '') { print ULOG "$URL{'m'}|$lltime\n"; }
	print ULOG "$URL{'b'}|$lltime\n$tempwrite";
	fclose(ULOG);
}

sub LMGS {
	if($settings[4] eq 'Administrator') { return; }
	for($i = 7; $i < @memgrps; $i++) {
		if($settings[4] eq $memgrps[$i]) {
			($ip,$mod,$st,$m,$pro,$t,$t,$t,$cal,$admin) = split(',',$memgrpp[$i]);
			if($admin) { $settings[4] = 'Administrator'; return; } # User is an admin
			$ipon = $ip; $modon = $mod; $ston = $st; $modifyon = $m; $proon = $pro; $calmod = $cal;
			last;
		}
	}
}

sub GMS {
	push(@admins,'Administrator');
	for($q = 0; $q < @memgrps; $q++) {
		($t,$t,$t,$t,$t,$star,$team,$t,$t,$admin,$count,$starcount) = split(',',$memgrpp[$q]);
		if($q == 0) { ($t,$star,$starcount) = split(',',$memgrpp[0]); }
		if($q == 6) { ($team,$star,$starcount) = split(',',$memgrpp[6]); }
		$star{$memgrps[$q]} = $star;
		$starcount{$memgrps[$q]} = $starcount;
		$team{$memgrps[$q]} = $team;
		if($admin) { $admin{$memgrps[$q]} = 1; push(@admins,$memgrps[$q]); }
		if($count ne '') { push(@grpsposts,"$count|$memgrps[$q]"); }
	}
	@grpsposts = sort{$a <=> $b} @grpsposts;
}

sub Ban {
	fopen(FILE,"$prefs/BanList.txt");
	@banlist = <FILE>;
	fclose(FILE);
	chomp @banlist;
	foreach (@banlist) {
		($banstring,$banlimit,$bantime) = split(/\|/,$_);
		$length = length($banstring);
		$ipsearch = substr($ENV{'REMOTE_ADDR'},0,$length);
		if($ipsearch =~ /\Q$banstring/i || $username eq $banstring || $banstring eq $settings[2]) { CoreLoad('BoardLock'); Banned($banlimit,$bantime); }
	}
}

sub FindRanks {
	my(@rdata);
	($frank) = $_[0];
	fopen(FILE,"$members/List.txt");
	@list = <FILE>;
	fclose(FILE);
	chomp @list;
	foreach(@list) {
		loaduser($_);
		if($userset{$_}->[4] eq $frank) { push(@rdata,$_); }
	}
	return(@rdata);
}

sub Format {
	($temp) = $_[0];
	# $temp =~ s/&#8217/\'/g;
	$temp =~ s/&/\&amp;/g;
	$temp =~ s/</&lt;/g;
	$temp =~ s/>/&gt;/g;
	$temp =~ s/\cM//g;
	$temp =~ s/\n/<br>/g;
	$temp =~ s/\|/\&#124;/g;
	$temp =~ s/"/\&quot;/g;
	$temp =~ s/  / &nbsp;/gi;
	$temp =~ s/\t/ &nbsp; &nbsp; /gi;
	return $temp;
}

sub Unformat {
	($temp) = $_[0];
	$temp =~ s/<br>/\n/g;
	$temp =~ s/&lt;/</g;
	$temp =~ s/&gt;/>/g;
	$temp =~ s/  /&nbsp; /gi;
	return $temp;
}

sub BoardProperties {
	$tallow = 0; # Thread rules start
	if($noguestp == 0 && $username eq 'Guest') { $rules .= $ltxt[30]; }
	elsif($username eq 'Guest' && $binfo[3] > 0) { $rules .= $ltxt[30]; }
	elsif($binfo[3] == 2 && $settings[4] eq 'Administrator') { $rules .= $ltxt[31]; $tallow = 1; }
	elsif($binfo[3] == 2 || $binfo[3] == 3) { $rules .= $ltxt[30]; }
	elsif($binfo[3] == 4 && ($settings[4] eq 'Administrator' || $ismod)) { $rules .= $ltxt[31]; $tallow = 1; }
	elsif($binfo[3] == 4) { $rules .= $ltxt[30]; }
		else { $rules .= $ltxt[31]; $tallow = 1; }
	if($noguestp == 0 && $username eq 'Guest') { $rules .= $ltxt[32]; }
	elsif($username eq 'Guest' && $binfo[4] == 1) { $rules .= $ltxt[32]; }
	elsif($binfo[4] == 2 && $settings[4] eq 'Administrator') { $rules .= $ltxt[33]; $repallow = 1; }
	elsif($binfo[4] == 2 || $binfo[4] == 3) { $rules .= $ltxt[32]; }
	elsif($binfo[4] == 4 && ($settings[4] eq 'Administrator' || $ismod)) { $rules .= $ltxt[33]; $repallow = 1; }
	elsif($binfo[4] == 4) { $rules .= $ltxt[30]; }
		else { $rules .= $ltxt[33]; $repallow = 1; }

	$rules .= ($binfo[5] || !$tallow) ? $ltxt[35] : $ltxt[34]; # Polls

	$rules .= ($uallow == 1 || ($uallow == 2 && $username ne 'Guest') || ($uallow == 3 && $settings[4] eq 'Administrator')) && ($tallow || $repallow) ? $ltxt[29] : $ltxt[28]; # Attachments

	$rules2 = $ltxt[36];
	$rules2 .= $html ? $ltxt[37] : $ltxt[38];
	$rules2 .= $ltxt[39];
	$rules2 .= $BCLoad ? $ltxt[37] : $ltxt[38];
	$rules2 .= $ltxt[40];
	$rules2 .= $BCSmile ? $ltxt[37] : $ltxt[38];
}

sub ListBoards {
	if(!$boardbase[1]) { return(qq~<span class="smalltext">$boardnm ($ltxt[19])</span>~); }
	my($temp,@bdsh,$hh,$bbase,$name,$bid,$bhere,$access);
	$temp = qq~<script language="JavaScript">
<!--
function JumpTo(ghere) {
 if(ghere != '') { location = "$surl,b="+ghere; }
}
//-->
</script>
<select name="JumpZ" onChange="JumpTo(this.value);">~;
	foreach(@catbase) {
		($viewer,$selector,$access,$bhere) = split(/\|/,$_);
		if($access && ($access ne $settings[4] && $settings[4] ne 'Administrator')) { next; }
		@bdsh = split("/",$bhere);
		if(length($viewer) > 25) { $viewer = substr($viewer,0,25)." ..."; }

		$temp .= qq~<option value=",c=$selector">$viewer</option>~;
		foreach $hh (@bdsh) {
			foreach $bbase (@boardbase) {
				($bid,$t,$t,$name,$t,$t,$t,$t,$t,$t,$access) = split("/",$bbase);
				if($access && ($access ne $settings[4] && $settings[4] ne 'Administrator')) { next; }
				if($bid ne $hh) { next; }
				$select = $bid eq $URL{'b'} ? ' selected' : '';
				if(length($name) > 25) { $name = substr($name,0,25)." ..."; }
				$temp .= qq~<option value="$bid"$select>&nbsp; - $name</option>~;
				last;
			}
		}
	}
	$temp .= qq~</select>~;
	return($temp);
}

sub FindStatus {
	my($temp);
	if($type) { $temp = 'locked'; }
	elsif($replies >= $vhtdmax) { $temp = 'veryhotthread'; }
	elsif($replies >= $htdmax) { $temp = 'hotthread'; }
		else { $temp = 'thread'; }
	if($poll && $type) { $temp = 'poll_lock'; }
	elsif($poll) { $temp = 'poll_icon'; }
	if($stickme{$messid} && $type && $temp ne 'poll_icon') { $temp = 'sticky_lock'; }
	elsif($stickme{$messid} && $temp eq 'poll_icon') { $temp = 'poll_sticky'; }
	elsif($stickme{$messid}) { $temp = 'sticky'; }
	return($temp);
}

sub CensorList {
	if($settings[39]) { return($_[0]); }
	my($takenin) = $_[0];
	if(!$CensorLoad) {
		fopen(CENSOR,"$prefs/Censor.txt");
		@censor = <CENSOR>;
		fclose(CENSOR);
		chomp @censor;
		$CensorLoad = 1;
	}
	foreach(@censor) {
		($censor,$change) = split(/\|/,$_);
		if($censor eq '' || $change eq '') { next; }
		$takenin =~ s~\b\Q$censor\E\b~$change~ig;
	}
	return($takenin);
}

sub LoadColors {
	for($i = 0; $i < @memgrps; $i++) {
		if($i > 6) { ($t,$t,$t,$t,$t,$t,$t,$memgrpp[$i]) = split(",",$memgrpp[$i]); }
		if($i == 0) { ($memgrpp[$i]) = split(",",$memgrpp[$i]); $memgrps[$i] = 'Administrator'; }
		$gcolors{$memgrps[$i]} = $memgrpp[$i];
	}
}

sub BoardUsers {
	my(@usersonb,@userson);
	$cnt = 0;
	$gcnt = 0;
	fopen(FILE,"$prefs/Active.txt");
	@userson = <FILE>;
	fclose(FILE);
	chomp @userson;

	&LoadColors;

	foreach(@userson) {
		($luser,$ltime,$hidden,$t,$bview) = split(/\|/,$_);
		loaduser($luser);
		if($userset{$luser}->[1] eq '') { $fndu = $luser; } else { $fndu = $userset{$luser}->[1]; }
		push(@usersonb,"$fndu|$luser|$hidden|$bview");
	}
	foreach(sort {lc($a) cmp lc($b)} @usersonb) {
		($trash,$luser,$hidden,$cbrd) = split(/\|/,$_);
		if($cbrd ne $URL{'b'}) { next; }
		if($userset{$luser}->[1] ne '') {
			if($gcolors{$userset{$luser}->[4]}) { $duser = qq~<font color="$gcolors{$userset{$luser}->[4]}"><b>$userset{$luser}->[1]</b></font>~; }
				else { $duser = $userset{$luser}->[1]; }
			if($hidden && $settings[4] ne 'Administrator') { next; }

			$activeusers .= qq~<a href="$scripturl,v=memberpanel,a=view,u=$luser">$duser</a>, ~;
			++$cnt;
		} elsif($botsearch{$luser}) {
			$activeusers .= qq~<span class="onlinebots">$botsearch{$luser}</span>, ~;
			++$cnt;
		} else { ++$gcnt; }
	}

	$activeusers =~ s/, \Z//;
	if(!$activeusers) { $activeusers = $ltxt[20]; }
}

sub MakeSmall { # To make long code look like it's suppose to, do this ...
	if(!$BCLoad) { return; }
	$message =~ s/<(.[^">]*?)\Z//gsi;
	$message =~ s~\[(.[^\]]*?)\Z~~gsi;
	$message =~ s/<a href="(.[^">]*?)\Z//gsi;
	$message =~ s~\[code\](.*?)\Z~&Code~esgi;
	$message =~ s~\[quote by=(.+?) link=(.+?) date=(.+?)\](.+?)\Z~&Quote~gsie;
	$message =~ s~\[quote\](.+?)\Z~&Quote~gsie;
	$message =~ s~\[s\](.*?)\Z~<s>$1</s>~gsi;
	$message =~ s~\[b\](.*?)\Z~<b>$1</b>~gsi;
	$message =~ s~\[i\](.*?)\Z~<i>$1</i>~gsi;
	$message =~ s~\[u\](.*?)\Z~<u>$1</u>~gsi;
	$message =~ s~\[size=([1-9][^\s\n<>]*?)\](.*?)\Z~<font size="$1">$2</font>~gsi;
	$message =~ s~\[pre\](.+?)\Z~<pre>$1</pre>~gsi;
	$message =~ s~\[justify\](.+?)\Z~<div align="justify">$1</div>~gsi;
	$message =~ s~\[left\](.+?)\Z~<div align="left">$1</div>~gsi;
	$message =~ s~\[right\](.+?)\Z~<div align="right">$1</div>~gsi;
	$message =~ s~\[center\](.+?)\Z~<center>$1</center>~gsi;
	$message =~ s~\[face=(.+?)\](.+?)\Z~<font face="$1">$2</font>~gsi;
	$message =~ s~\[color=(.+?)\](.+?)\Z~<font color="$1">$2</font>~gsi;
	$message =~ s~\[sub\](.+?)\Z~<sub>$1</sub>~gsi;
	$message =~ s~\[sup\](.+?)\Z~<sup>$1</sup>~gsi;
	$message =~ s~\[img(.+?)\Z~~gsi;
	if($message =~ /\[list(.*?)\](.+?)\Z/) {
		$message =~ s~\[list\](.+?)\Z~<ul>$1</ul>~gsi;
		$message =~ s~\[list=([a-zA-Z|1]+?)\](.+?)\Z~<ol type="$1">$2</ol>~gsi;
		$message =~ s~\[\*\]~<li>~gsi;
	}
}
1;