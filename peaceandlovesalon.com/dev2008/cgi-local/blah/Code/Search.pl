################################################
# E-Blah Bulliten Board Systems       Platinum #
################################################
# Copyright (c) 2001 - 2004 e-blah!            #
# All Rights Reserved.                         #
################################################

CoreLoad('Search',1);

sub Search {
	my($cnter,$searchID,$file,$counter);
	if(!$gsearch) { # Guests are not allowed ...
		error("$searchtxt[1]-register-") if($username eq 'Guest');
		is_member();
	}

	# Find where we should go:
	if($URL{'p'} ne '') { Results(); }

	$title = $searchtxt[2];
	&header;
	$ebout .= <<"EOT";
<script language="JavaScript">
<!--
function Submit() { document.sform.submit.disabled = true; }
// -->
</script>
<table cellpadding="5" cellspacing="1" width="700" class="border" align="center">
 <tr><form action="$scripturl,v=search,p=2" method="post" name="sform" enctype="multipart/form-data" onSubmit="Submit()">
  <td class="titlebg"><img src="$images/search_sm.gif" style="vertical-align: middle"> <b>$title</b></td>
 </tr><tr> 
  <td class="catbg"><span class="smalltext"><b>$searchtxt[35]</b></span></td>
 </tr><tr>
  <td class="win">
   <table cellpadding="4" cellspacing="0" width="100%">
    <tr>
     <td class="win2" width="50%"><b>$searchtxt[48]</b></td>
     <td class="win2" width="50%">$searchtxt[49]</td>
    </tr><tr>
     <td valign="top"><input type="text" class="textinput" name="searchstring" size="35"></td>
     <td><input type="text" class="textinput" name="searchuser" size="30"><div class="smalltext" style="padding-top: 5px; padding-bottom: 5px;"><input type="checkbox" class="checkboxinput" name="searchmtype" value="1"> $searchtxt[67]</div></td>
    </tr>
EOT
	if($settings[4] eq 'Administrator') {
		$ebout .= <<"EOT";
    <tr>
     <td colspan="2" class="win2"><b>$searchtxt[55]</b></td>
    </tr><tr>
     <td colspan="2"><input type="text" class="textinput" name="searchip" size="20" maxlength="15"><span class="smalltext"> &nbsp; $searchtxt[66]</span></td>
    </tr>
EOT
	}
	$ebout .= <<"EOT";
   </table>
  </td>
 </tr><tr> 
  <td class="catbg"><span class="smalltext"><b>$searchtxt[56]</b></span></td>
 </tr><tr>
  <td class="win">
   <table cellpadding="4" cellspacing="0" width="100%">
    <tr>
     <td class="win2" width="50%"><b>$searchtxt[57]</b></td>
     <td class="win2" width="50%"><b>$searchtxt[58]</b></td>
    </tr><tr>
     <td valign="top"><span class="smalltext">$searchtxt[59] <input type="text" class="textinput" name="searchdate" value="30" size="4" maxlength="4"> $searchtxt[60] <select name="searcholder"><option value="0">$searchtxt[40]</option><option value="1" selected>$searchtxt[41]</option></select></span></td>
     <td valign="top"><span class="smalltext"><select name="searchresults"><option value="5">5</option><option value="10">10</option><option value="15">15</option><option value="20" selected>20</option><option value="25">25</option><option value="30">30</option></select> $searchtxt[61]<div style="padding-top: 5px;">$searchtxt[62] <select name="searchnm"><option value="1">$searchtxt[63]</option><option value="0" selected>$searchtxt[64]</option></select></div></span></td>
    </tr>
   </table>
  </td>
 </tr><tr> 
  <td class="catbg"><span class="smalltext"><b>$searchtxt[65]</b></span></td>
 </tr><tr> 
  <td class="win" align="center"><br><select size="10" style="width: 300px" name="searchboards" multiple>
EOT

	foreach(@catbase) {
		($t,$boardid,$memgrps,$t,$t,$subcats) = split(/\|/,$_);
		$catbase{$boardid} = $_;
		if(!GetMemberAccess($memgrps)) { next; }
		foreach(split(/\//,$subcats)) { $noshow{$_} = 1; }

		$cats .= "$boardid/";
	}

	foreach(@boardbase) {
		($id,$t,$t,$t,$t,$t,$t,$passed,$t,$t,$boardgood,$t,$t,$redir) = split("/",$_);
		if(($settings[4] ne 'Administrator') && (!GetMemberAccess($boardgood) || $passed ne '' || $redir ne '')) { next; }
		$board{$id} = $_;
	}

	SearchBoardsGet($cats,1);

	$ebout .= <<"EOT";
  </select><br><br></td>
 </tr><tr> 
  <td class="catbg" align="center"><input type="submit" class="button" value=" $searchtxt[24] " name="submit"></td>
 </tr></form>
</table>
EOT
	# Recent Searches ... =D

	$ebout .= <<"EOT";
<br>
<table cellpadding="0" cellspacing="1" width="700" class="border" align="center">
 <tr>
  <td class="titlebg" align="center" style="padding: 5px;"><span class="smalltext"><b>$searchtxt[68]</b></span></td>
 </tr><tr>
  <td class="win"><table cellpadding="8" cellspacing="0" width="100%">
EOT

	opendir(DIR,"$prefs/BHits");
	@list = readdir(DIR);
	closedir(DIR);

	@colors = ('win2','win');

	foreach(reverse @list) { # After 2 hours, search expires
		if($_ =~ /.search\Z/) { $file = $_; $file =~ s/.search\Z//g; } else { next; }
		$time = time;

		if($file+6200 > $time) {
			$cnter = 0;

			fopen(FILE,"$prefs/BHits/$file.search");
			while( $read = <FILE> ) {
				++$cnter;
				if($cnter == 1) { $searchID = $read; }
				if($cnter == 5) { last; }
			}
			fclose(FILE);
			if($cnter == 4) { next; }

			++$counter;
			$window = $colors[$counter % 2];

			$searchdate = get_date($file);
			$ebout .= <<"EOT";
<tr>
 <td class="$window" width="50%"><span class="smalltext"><a href="$surl,v=search,p=2,s=$file">$searchID</a></span></td>
 <td class="$window" width="50%"><span class="smalltext">$searchdate</span></td>
</tr>
EOT
		}
	}

	$ebout .= qq~<tr><td class="win2"><span class="smalltext"><div style="padding: 7px; width: 100%;" align="center">$searchtxt[69]</div></span></td></tr>~ if $searchdate eq '';

	$ebout .= "</table></td></tr></table>";

	footer();
}

sub SearchBoardsGet {
	foreach $catbased (split(/\//,$_[0])) {
		if($arshown{$catbased} || $catbase{$catbased} eq '') { next; }
		if($noshow{$catbased} && $_[1]) { next; }
		$arshown{$catbased} = 1;

		($b,$bid,$t,$input,$t,$subcats) = split(/\|/,$catbase{$catbased});

		$ebout .= qq~<optgroup label="$_[2]$b">~;
		if($input ne '') {
			foreach $boardbased (split(/\//,$input)) {
				if($board{$boardbased} eq '') { next; }
				($obds,$t,$t,$info) = split("/",$board{$boardbased});
				$ebout .= qq~<option value="$obds" selected>$_[2]$info</option>~;
			}
		}

		if($subcats ne '') { SearchBoardsGet($subcats,0,"$_[2]&nbsp; &nbsp; "); }

		$ebout .= "</optgroup>";
	}
}

sub Results { # Search Results
	if($URL{'s'} eq '') {
		Clean();
		DirtySearch();
	} else { $searchtime = $URL{'s'}; }

	if($URL{'p'} ne 'topten' && $URL{'p'} ne 'user') {
		error($searchtxt[36]) if !-e("$prefs/BHits/$searchtime.search");
		fopen(SEARCH,"$prefs/BHits/$searchtime.search");
		@search = <SEARCH>;
		fclose(SEACH);
		chomp @search;
		$searchterms = $search[0];
		$searchnm    = $search[3];
		$npp         = $search[2] || 1;
		shift(@search); shift(@search); shift(@search); shift(@search);
	} else {
		$npp = $searchresults;
	}

	$title = $searchtxt[27];
	header();

	$start = $URL{'l'};
	$mresults = @search || 1;
	if($mresults < $npp) { $start = 0; }
	$tstart = $start || 0;
	$counter = 1;

	# How many page links?
	$smax = $totalpp*20;
	$startdot = ($totalpp*2)/5;

	# Create the main link ...
	$search =~ s/ /\+/gsi;
	$hllink = "highlight=$searchterms";
	$link = "$scripturl,v=search,p=2,s=$searchtime,l";

	if($tstart > $mresults) { $tstart = $mresults; }
	$tstart = (int($tstart/$npp)*$npp);
	if($tstart > 0) { $bk = ($tstart-$npp); $pagelinks = qq~<a href="$link=$bk">&#171;</a> ~; }
	if($mresults > ($smax/2) && $tstart > $npp*($startdot+1) && $mresults > $smax) {
		$sbk = $tstart-$smax; $sbk = 0 if($sbk < 0); $pagelinks .= qq~<a href="$link=$sbk">...</a> ~;
	}
	for($i = 0; $i < $mresults; $i += $npp) {
		if($i < $bk-($npp*$startdot) && $mresults > $smax) { ++$counter; $final = $counter-1; next; }
		if($start ne 'all' && $i == $tstart || $mresults < $npp) { $pagelinks .= qq~<b>$counter</b>, ~; $nxt = ($tstart+$npp); }
			else { $pagelinks .= qq~<a href="$link=$i">$counter</a>, ~; }
		++$counter;
		if($counter > $totalpp+$final && $mresults > $smax) { $gbk = $tstart+$smax; if($gbk > $mresults) { $gbk = (int($mresults/$npp)*$npp); } $pagelinks =~ s/, \Z//gsi; $pagelinks .= qq~ <a href="$link=$gbk">...</a> ~; ++$i; last; }
	}
	if($counter > 2) { $pgs = 's'; }
	$pagelinks =~ s/, \Z//gsi;
	if(($tstart+$npp) != $i && $start ne 'all') { $pagelinks .= qq~ <a href="$link=$nxt">&#187;</a>~; }

	$onpage = ($tstart+1).' - '.($tstart+$npp);

	$mresults = 0 if($search[0] eq '');

	$ebout .= <<"EOT";
<table cellpadding="5" cellspacing="1" class="border" width="100%">
 <tr>
  <td style="padding: 7px;" class="titlebg"><img src="$images/search_sm.gif" align="left"> <b>$title</b></td>
 </tr><tr>
  <td class="catbg" style="padding: 10px;"><span class="smalltext">
   <div style="float: left"><b>$gtxt{'17'}:</b> $pagelinks</div>
   <div style="float: right"><b>$searchtxt[28] $onpage</b> (<b>$mresults</b> $searchtxt[29])</div>
  </span></td>
 </tr>
EOT



	$counter = 1;
	$counter2 = 0;
	if($search2 !~ /[#%,\\\/:?"<>'|@^\$\&~'\)\(\]\[\;{}!`=-]/) { @lights = split(/\+/,$search2); } # Highlighter
	foreach(@search) {
		if(($counter > $tstart && $counter2 < $npp)) {
			($ptime,$board,$id,$msub,$message,$ip,$user,$count,$nosmile) = split(/\|/,$_);
			$msub = CensorList($msub);

			$date = get_date($ptime);
			loaduser($user);
			if($userset{$user}->[1]) { $user = qq~<a href="$scripturl,v=memberpanel,a=view,u=$user">$userset{$user}->[1]</a>~; }

			$msub .= $count > 0 ? qq~</a> <span class="smalltext">&nbsp; ($searchtxt[31])</span>~ : '</a>';
			if($ipadd) { $showip = qq~<br><b>$gtxt{'18'}:</b> $ip~; }

			if($counter != 1) { $ebout .= qq~<tr><td class="win3" height="5"></td></tr>~; }
			$ebout .= <<"EOT";
 <tr>
  <td class="win">
   <div style="float: left; padding-top: 5px; padding-left: 3px;"><b><a href="$surl,b=$board,v=display,m=$id,s=$count,$hllink#num$count">$msub</b></div>
   <div class="smalltext" style="float: right" align="right"><b>$gtxt{'19'}:</b> $user<br><b>$gtxt{'21'}:</b> $date</div>
  </td>
 </tr>
EOT

			if(!$searchnm) {
				if(length($message) > 750) {
					$message =~ s~\[table\](.*?)\[\/table\]~$var{'88'}~sgi;
					$message = substr($message,0,750);

					$message =~ s~([^\w\"\=\[\]]|[\n\b]|\A)\\*(\w+://[\w\~\.\;\:\$\-\+\!\*\?/\=\&\@\#\%]+\.[\w\~\;\:\$\-\+\!\*\?/\=\&\@\#\%,.]+[\w\~\;\:\$\-\+\!\*\?/\=\&\@\#\%]\Z)~~eisg;
					$message =~ s~([^\"\=\[\]/\:\.(\://\w+)]|[\n\b]|\A|[\<\n\b\>])\\*(www\.[^\.][\w\~\.\;\:\$\-\+\!\*\?/\=\&\@\#\%]+\.[\w\~\;\:\$\-\+\!\*\?/\=\&\@\#\%\,]+[\w\~\;\:\$\-\+\!\*\?/\=\&\@\#\%]\Z)~~eisg;

					BC();
					MakeSmall();
					$message .= qq~...<br><br><a href="$surl,b=$board,v=display,m=$id,s=$count,$hllink#num$count">$searchtxt[70] ...</a>~;
				} else { BC(); }
				Highlight($message);

				$ebout .= <<"EOT";
 <tr>
  <td class="win2" style="padding: 7px;"><span  class="smalltext">$message</td>
 </tr>
EOT
			}
			++$counter2;
		}
		++$counter;
	}

	if($search[0] eq '') {
		$ebout .= <<"EOT";
 <tr>
  <td class="win" style="padding: 15px" align="center"><b>$searchtxt[32]</b></td>
 </tr>
EOT
	}

	$ebout .= <<"EOT";
 <tr>
  <td class="catbg" style="padding: 10px;"><span class="smalltext">
   <div style="float: left"><b>$gtxt{'17'}:</b> $pagelinks</div>
   <div style="float: right"><b>$searchtxt[28] $onpage</b> (<b>$mresults</b> $searchtxt[29])</div>
  </span></td>
 </tr>
</table>
EOT
	footer();
	exit;
}

sub DirtySearch { # We need to create a Search ID for use =(
	# Create Variables, first:
	if($URL{'p'} eq 'topten') { # Topten
		$searchmaxresults = 10;
		$searchresults    = 10;
		$searchnm         = 0;
	} elsif($URL{'p'} eq 'user') { # By username
		$searchuser       = $URL{'by'} || $username;
		$searchresults    = 30;
		$searchmaxresults = 30;
		$searchnm         = 0;
	} elsif($URL{'p'} eq 'bs33') {
		$searchstring     = $FORM{'searchstring'};
		$searchresults    = 60;
		$searchmaxresults = 30;
		$searchnm         = 0;
	} else { # Search using the form
		$searchstring  = $FORM{'searchstring'};
		$searchuser    = $FORM{'searchuser'};

		error($searchtxt[4]) if($searchstring eq '' && $searchuser eq '');

		$searchmtype   = $FORM{'searchmtype'};
		$searcholder   = $FORM{'searcholder'};
		$searchnm      = $FORM{'searchnm'};
		$searchresults = $FORM{'searchresults'} || 20;
		$searchdate    = time-($FORM{'searchdate'}*86400);
		if($settings[4] eq 'Administrator') { $searchip = $FORM{'searchip'}; }

		foreach $selbrd (split(",",$FORM{'searchboards'})) { $searchboards{$selbrd} = 1; }
	}

	# Find boards to search this go around =)
	foreach(@catbase) {
		($t,$t,$memgrps,$thelist,$t,$subcats) = split(/\|/,$_);
		GetMemberAccess($memgrps) || next;
		foreach( split(/\//,$thelist) ) { $goodboard{$_} = 1; }
	}

	foreach(@boardbase) {
		($id,$t,$t,$t,$t,$t,$t,$passed,$t,$t,$boardgood) = split("/",$_);
		next if !$goodboard{$id} || $passed ne '';
		GetMemberAccess($boardgood) || next;
		if($FORM{'searchboards'} ne '' && !$searchboards{$id}) { next; }
		push(@searchboards,$id);
	}

	# It's search time!
	$searchtime = time;
	foreach $board (@searchboards) {
		$deeplvl = 0;
		fopen(MSGDB,"$boards/$board.msg");
		while( $msgdb = <MSGDB> ) {
			chomp $msgdb;
			($id,$msub,$t,$t,$t,$t,$t,$t,$date) = split(/\|/,$msgdb);

			if($searchmaxresults && $deeplvl > $searchmaxresults+30) { last; }

			if(($searcholder && $searchdate) && ($date-$searchdate) < 0) { next; }
			elsif((!$searcholder && $searchdate) && ($date-$searchdate) > 0) { next; }

			$count = -1;
			fopen(MESSAGE,"$messages/$id.txt");
			while( $buffer = <MESSAGE> ) {
				chomp $buffer;
				($user,$message,$ip,$t,$ptime,$smiley) = split(/\|/,$buffer);
				if($searchmtype && $count == 0) { last; }
				++$count;

				if(($searcholder && $searchdate) && ($ptime-$searchdate) < 0) { next; }
				elsif((!$searcholder && $searchdate) && ($ptime-$searchdate) > 0) { next; }

				$ptime =~ s/\n//g;

				if($user eq '' || $msub eq '' || $message eq '' || $msub eq '') { next; }

				if($searchip) {
					$ipsearch = substr($ip,0, length($searchip) );
					if($ipsearch !~ /\Q$searchip/i) { next; }
				}

				if($searchuser && $searchuser ne $user) { next; }
					else { ++$deeplvl; } # Number found by user
				if($searchstring && ($message !~ /\Q$searchstring\E/sig && $msub !~ /\Q$searchstring\E/sig)) { next; }

				# We found one!
				push(@searchresults,"$ptime|$board|$id|$msub|$message|$ip|$user|$count|$smiley");
			}
			fclose(MESSAGE);
			if($URL{'p'} eq 'topten') { ++$deeplvl; }
		}
		fclose(MSGDB);
	}

	$count = 0;
	if($URL{'p'} eq 'user' || $URL{'p'} eq 'topten') {
		foreach(sort{$b <=> $a} @searchresults) {
			if($searchmaxresults && $count == $searchmaxresults) { last; }
			push(@search,$_);
			++$count;
		}
		$mresults = $searchresults;
	} else {
		fopen(FILE,">$prefs/BHits/$searchtime.search");
		print FILE "$searchstring\n$searchuser\n$searchresults\n$searchnm\n";
		foreach(sort{$b <=> $a} @searchresults) {
			print FILE "$_\n";
			++$count;
		}
		fclose(FILE);
	}
}

sub GetMemberAccess { # Adapted from the BoardIndex =P
	my($grpaccess) = @_;
	if($grpaccess eq '') { return(1); }
	my $boot = 1;
	foreach $group (split(",",$grpaccess)) {
		if($group eq $settings[4]) { $boot = 0; }
		if($group eq 'member' && $username ne 'Guest') { $boot = 0; }
	}
	if($boot && $settings[4] ne 'Administrator') { return(0); }
	return(1);
}

sub Clean {
	opendir(DIR,"$prefs/BHits");
	@list = readdir(DIR);
	closedir(DIR);
	foreach(@list) { # After 2 hours, search expires
		if($_ =~ /.search\Z/) { $file = $_; $file =~ s/.search\Z//g; } else { next; }
		$time = time;
		if($file+7200 < $time) { unlink("$prefs/BHits/$_"); }
			elsif($file > $max) { $max = $file; }
	}
}
1;