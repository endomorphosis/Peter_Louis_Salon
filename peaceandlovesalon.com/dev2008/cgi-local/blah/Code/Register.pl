################################################
# E-Blah Bulliten Board Systems       Platinum #
################################################
# Copyright (c) 2001 - 2004 e-blah!            #
# All Rights Reserved.                         #
################################################

CoreLoad('Register',1);

sub Register {
	if($URL{'a'} eq 'validate') { &Validate; }
	if($username ne 'Guest' && $settings[4] ne 'Administrator') { &error($registertxt[1]); }
	if($settings[4] eq 'Administrator') {
		$quickreg = 0;
		$oldform = ' checked';
		$vradmin = 0;
		$creg = 0;
		CoreLoad('AdminList');
	}
	$gdisable = 1;
	if($creg) { &error($registertxt[2]); }
	elsif($URL{'p'} == 1) { &Register2; }
	elsif($URL{'p'} == 2) { &Register3; }
	elsif($URL{'p'} eq 'finish') { &Finish; }
		else { &RegisterOkay; }
}

sub RegisterOkay {
	if(!$quickreg) { &Register2; }
	use Time::Local 'timelocal';
	($sec,$min,$hour,$day,$month,$year) = localtime(time);
	$yearz = $year-13;
	eval { $oldie = timelocal($sec,$min,$hour,$day,$month,$yearz); };
	($sec,$min,$hour,$day,$mu,$year,$week) = localtime($oldie);
	$date = "$months[$mu] $day, ".(1900+$year);
	$title = $registertxt[3];
	&header;
	$ebout .= <<"EOT";
<table cellspacing="1" cellpadding="5" class="border" width="600" align="center">
 <tr>
  <td colspan="2" class="titlebg"><b><img src="$images/register_sm.gif"> $title</b></td>
 </tr><tr>
  <td class="win"><span class="postbody">$registertxt[4]</span></td>
 </tr><tr>
  <td class="win2" align="center" style="padding: 10px"><p style="float: left"><b><a href="$scripturl">$registertxt[6] $date</a></p><p style="float: right"><a href="$surl,v=register,p=1">$registertxt[5] $date</a></p></b></td>
 </tr>
</table>
EOT
	&footer;
	exit;
}

sub Register2 {
	$title = $registertxt[3];
	if($settings[4] eq 'Administrator') { &headerA; } else { &header; }
	$ebout .= <<"EOT";
<script language="JavaScript"><!--
function check() {
 box = eval('document.post.agree');
 box.checked = !box.checked;
}
// --></script>
$error<table cellspacing="1" cellpadding="4" class="border" width="700" align="center">
 <tr><form action="$scripturl,v=register,p=2" method="POST" name="post">
  <td class="titlebg"><b><img src="$images/register_sm.gif"> $title</b></td>
 </tr><tr>
  <td class="win"><span class="smalltext">$registertxt[8]</span></td>
 </tr><tr>
  <td class="catbg"><span class="smalltext"><b>$registertxt[9]</b></span></td>
 </tr><tr>
  <td class="win2">
   <table cellpadding="4" cellspacing="0" width="100%">
    <tr>
     <td align="right" width="45%"><b>$registertxt[10]:</b></td>
     <td width="55%"><input type="text" class="textinput" name="username" value="$FORM{'username'}" size="30" maxlength="30"></td>
    </tr><tr>
     <td align="right"><b>$gtxt{'23'}:</b></td>
     <td><img src="$images/email_sm.gif"> <input type="text" class="textinput" name="email" value="$FORM{'email'}" size="25" maxlength="40"></td>
    </tr><tr>
     <td>&nbsp;</td><td><span class="smalltext">$registertxt[11]</span></td>
    </tr><tr>
     <td align="right"><b>$registertxt[12]:</b></td>
     <td><input type="password"  class="textinput" name="pw" size="20" maxlength="8"></td>
    </tr><tr>
     <td align="right"><b>$gtxt{'24'}:</b></td>
     <td><input type="password"  class="textinput" name="cpw" size="20" maxlength="8"></td>
    </tr>
   </table>
  </td>
 </tr>
EOT
	if($showreg) {
		if($FORM{'username'} && $FORM{'agree'}) { $oldform = ' checked'; }
		fopen(FILE,"$prefs/RTemp.txt");
		@rtemp = <FILE>;
		fclose(FILE);
		chomp @rtemp;
		foreach(@rtemp) { $message .= $_; }
		&BC;
		$ebout .= <<"EOT";
 <tr>
  <td class="catbg"><span class="smalltext"><b>$registertxt[14]</b></span></td>
 </tr><tr>
  <td class="win"><table cellpadding="4" cellspacing="0"><tr><td><span class="smalltext">$message<br><br><div class="win2" align="center" style="padding: 3px;"><input type="checkbox" class="checkboxinput" name="agree" value="1"$oldform><span style="cursor:default;" onClick="check();"> <b>$registertxt[15]</b></span></div></span></td></tr></table></td>
 </tr>
EOT
	} else { $ebout .= qq~<input type="hidden" value="1" name="agree">~; }
	$ebout .= <<"EOT";
 <tr>
  <td align="center" class="win" style="padding: 8px"><input type="submit" class="button" value="&nbsp;&nbsp;$registertxt[3]&nbsp;&nbsp;"></td></form>
 </tr>
</table>
EOT
	if($settings[4] eq 'Administrator') { &footerA; } else { &footer; }
	exit;
}

sub error_reg {
	my($error1) = $_[0];
	if(!$errorbuild) {
		$error = <<"EOT";
<table class="border" cellpadding="4" cellspacing="1" width="700" align="center">
 <tr>
  <td class="titlebg"><b><img src="$images/ban.gif"> $registertxt[55]</b></td>
 </tr><tr>
  <td class="win"><b>$registertxt[56]</b><div style="line-height: 140%"><ul>
EOT
	}
	if($error1 ne 'Finish') {
		if($ersal{$error1}) { return; }
		$ersal{$error1} = 1;
		$error .= "<li> $error1</li>";
	} else { $error .= qq~<ul></div><br></td></tr></table><br>~;
		&Register2;
	}
	$errorbuild = 1;
}

sub Register3 {
	if(!$FORM{'agree'}) { error_reg($registertxt[65]); }

	while(($name,$value) = each(%FORM)) {
		$value =~ s/[\n\r]//g;
		$value =~ s/\A\s+//;
		$value =~ s/\s+\Z//;
		$FORM{$name} = $value;
	}

	# Yawn ... validate the user ... make sure they do not be bad ... (this was boring to code)
	error_reg($registertxt[18]) if($FORM{'username'} eq '');
	error_reg($registertxt[19]) if($FORM{'pw'} eq '');
	error_reg($registertxt[20]) if($FORM{'cpw'} eq '');
	error_reg($registertxt[21]) if($FORM{'email'} eq '');
	error_reg($registertxt[22]) if($FORM{'cpw'} ne $FORM{'pw'});

	error_reg($registertxt[23]) if(length($FORM{'username'}) > 30);
	error_reg($registertxt[24]) if(length($FORM{'pw'}) > 8);
	error_reg($registertxt[25]) if(length($FORM{'email'}) > 40);

	$wantedname = $FORM{'username'};
	$formusername = lc($FORM{'username'});
	$FORM{'username'} =~ s/ /_/gi;
	error_reg("$registertxt[26] '$FORM{'username'}'.") if($formusername eq 'guest' || $formusername eq 'mods' || $formusername eq 'ma' || $formusername eq 'admin');
	fopen(FILE,"$prefs/Names.txt");
	while(<FILE>) {
		chomp $_;
		($searchme,$within) = split(/\|/,$_);
		$searchme = lc($searchme);
		if($within) { error_reg("$registertxt[26] '$FORM{'username'}'.") if($formusername =~ /\Q$searchme\E/gsi); }
			else { error_reg("$registertxt[26] '$FORM{'username'}'.") if($searchme eq $formusername); }
	}
	fclose(FILE);

	error_reg($registertxt[27]) if($FORM{'username'} !~ /\A[0-9A-Za-z%+,-\.@�^_ &nbsp;]+\Z/);
	error_reg("$FORM{'username'} $registertxt[28]") if(-e ("$members/$FORM{'username'}.dat"));
	error_reg($registertxt[21]) if($FORM{'email'} !~ /\A([0-9A-Za-z\._\-]{1,})@([0-9A-Za-z\._\-]{1,})+\.([0-9A-Za-z\._\-]{1,})+\Z/);

	foreach (@banlist) {
		($banstring) = split(/\|/,$_);
		if($banstring eq $FORM{'email'}) { error_reg($registertxt[21]); }
	}

	$formusername =~ s/ //gsi;
	fopen(FILE,"$members/List.txt");
	@membero = <FILE>;
	fclose(FILE);
	foreach $member (@membero) {
		chomp $member;
		fopen(FILE,"$members/$member.dat");
		@memberset = <FILE>;
		fclose(FILE);
		chomp $memberset[1];
		chomp $memberset[2];
		$mymail = lc $FORM{'email'};
		$mail = lc $memberset[2];
		$memset = lc $memberset[1];
		if($memset eq $formusername) { error_reg($registertxt[31]); }
		if($mymail eq $mail) { error_reg($registertxt[32]); }
	}
	if($errorbuild) { error_reg('Finish'); }

	fopen(FILE,">>$members/List.txt");
	print FILE "$FORM{'username'}\n";
	fclose(FILE);
	fopen(FILE,"$members/LastMem.txt");
	@latestmems = <FILE>;
	fclose(FILE);
	chomp @latestmems;
	++$latestmems[1];
	fopen(FILE,">$members/LastMem.txt");
	print FILE "$FORM{'username'}\n$latestmems[1]\n";
	fclose(FILE);

	$curtime = time;
	if($yabbconver) { $FORM{'pw'} = crypt($FORM{'pw'},$pwcry); }

	if($vradmin) {
		$formid = sprintf("%.0f",rand(int(time/9)*7000));
		if($vradmin == 2) { $extra = "$registertxt[33]\n"; }
			else { $extra = $registertxt[34]; }
		$message = <<"EOT";
$registertxt[35] $mbname!

$registertxt[36] $mbname, $registertxt[54] $extra

$registertxt[37]:
<a href="$rurl,v=register,a=validate,id=$formid,u=$FORM{'username'}">$rurl,v=register,a=validate,id=$formid,u=$FORM{'username'}</a>

$gtxt{'25'}!
EOT
		smail($FORM{'email'},$registertxt[38],$message,$registertxt[39]);
	}

	if($emailadmin) { smail($eadmin,$registertxt[64],$registertxt[63]); }

	if($vradmin == 1) { $vradmin = "EMAIL"; }
	elsif($vradmin == 2) { $vradmin = "EMAIL|ADMIN"; }
		else { $vradmin = 0; }

	$newuser->[0]  = $FORM{'pw'};
	$newuser->[1]  = $wantedname;
	$newuser->[2]  = lc($FORM{'email'});
	$newuser->[3]  = 0;
	$newuser->[14] = $curtime;
	$newuser->[15] = $gtzone || 0; # Sets the GMT to the "guest" GMT
	$newuser->[23] = $vradmin;
	$newuser->[24] = $formid;

	fopen(FILE,">$members/$FORM{'username'}.dat");
	for($q = 0; $q < 25; $q++) { print FILE "$newuser->[$q]\n"; }
	fclose(FILE);

	if($uextlog) { ++$ExtLog[3]; &ExtClose; }

	if($settings[4] eq 'Administrator') { $url = "$scripturl,v=admin,r=3"; }
		else { $url = "$scripturl,v=register,p=finish,u=$FORM{'username'}"; }

	&redirect;
}

sub Finish {
	if($vradmin) { $extra = qq~$registertxt[59]~; }
	if($vradmin == 2) { $extra .= qq~<hr color="$color{'border'}" class="hr" size="1">$registertxt[60]~; }
	$title = $registertxt[45];
	&header;
	$ebout .= <<"EOT";
<table class="border" cellpadding="4" cellspacing="1" width="500" align="center">
 <tr>
  <td class="titlebg"><b><img src="$images/register_sm.gif"> $title</b></td>
 </tr><tr>
  <td class="win"><table cellspacing="0" cellpadding="5"><tr><td><span class="smalltext">$registertxt[58] $extra</span></td></tr></table></td>
 </tr><tr>
  <td class="win2"><b>&nbsp;<a href="$scripturl,v=login,u=$URL{'u'}">$registertxt[46]</a></b></td>
 </tr>
</table>
EOT
	&footer;
	exit;
}

sub Validate {
	$usernameb = $URL{'u'};
	fopen(FILE,"$members/$usernameb.dat");
	@settingsa = <FILE>;
	fclose(FILE);
	chomp @settingsa;
	$usersetcount = @settingsa;

	if($settingsa[23] eq 'ADMIN') { error($registertxt[48]); }
	elsif($settingsa[23] != 0) { error($registertxt[49]); }
	if($settingsa[24] ne $URL{'id'}) { error($registertxt[50]); }

	if($settingsa[23] eq "EMAIL|ADMIN") { $settingsa[23] = 'ADMIN'; }
		else { $settingsa[23] = "0"; }

	$settingsa[24] = '';
	fopen(FILE,"+>$members/$usernameb.dat");
	for($q = 0; $q < $usersetcount; $q++) { print FILE "$settingsa[$q]\n"; }
	fclose(FILE);

	if($vradmin == 2) { $message = $registertxt[62]; }
		else { $message = $registertxt[61]; }

	$title = $registertxt[52];
	&header;
	$ebout .= <<"EOT";
<table class="border" cellpadding="4" cellspacing="1" width="500" align="center">
 <tr>
  <td class="titlebg"><b><img src="$images/register_sm.gif"> $title</b></td>
 </tr><tr>
  <td class="win"><table cellspacing="0" cellpadding="5"><tr><td><span class="smalltext">$message</span></td></tr></table></td>
 </tr><tr>
  <td class="win2"><b>&nbsp;<a href="$scripturl">$gtxt{'26'}</a></b></td>
 </tr>
</table>
EOT
	&footer;
	exit;
}
1;