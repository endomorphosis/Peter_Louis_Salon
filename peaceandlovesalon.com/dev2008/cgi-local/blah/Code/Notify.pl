################################################
# E-Blah Bulliten Board Systems       Platinum #
################################################
# Copyright (c) 2001 - 2004 e-blah!            #
# All Rights Reserved.                         #
################################################

CoreLoad('Notify',1);

sub AddDelNotify2 {
	if($URL{'m'} ne 'brd') {
		&GetMessages;
		fopen(FILE,"$messages/$URL{'m'}.mail");
		while(<FILE>) {
			chomp $_;
			if($_ eq $username) { $remove = 1; } else { $print .= "$_\n"; }
		}
		fclose(FILE);
		if($remove != 1) { $print .= "$username\n"; }
		fopen(FILE,"+>$messages/$URL{'m'}.mail");
		print FILE "$print";
		fclose(FILE);
		if(-s("$messages/$URL{'m'}.mail") == 0) { unlink("$messages/$URL{'m'}.mail"); }
		$url = "$scripturl,v=mindex";
	} else {
		fopen(FILE,"$boards/$URL{'b'}.mail");
		while(<FILE>) {
			chomp $_;
			if($_ eq $username) { $fnd = 1; } else { push(@bdata,$_); }
		}
		fclose(FILE);
		if(!$fnd) { push(@bdata,$username); }
		fopen(FILE,">$boards/$URL{'b'}.mail");
		foreach(@bdata) { print FILE "$_\n"; }
		fclose(FILE);
		$url = $surl;
	}
	&redirect;
}

sub GetMessages {
	fopen(FILE,"$boards/$URL{'b'}.msg");
	@msg = <FILE>;
	fclose(FILE);
	chomp(@msg);
	foreach(@msg) {
		($mid,$mtitle,$t,$t,$t,$t,$locked) = split(/\|/,$_);
		if($mid == $URL{'m'}) { $fnd = 1; last; }
	}
	if($fnd != 1) { &error("$notify[8]: $URL{'m'}.txt"); }
}

sub View {
	foreach(@boardbase) {
		($oboard,$desc,$t,$bnme) = split("/",$_);
		fopen(FILE,"$boards/$oboard.mail");
		while(<FILE>) { chomp $_;
			if($_ eq $username) { push(@bdata,"$oboard|$bnme|$desc"); }
		}
		fclose(FILE);
		fopen(FILE,"$boards/$oboard.msg");
		while(<FILE>) { chomp $_;
			($id,$msub) = split(/\|/,$_);
			push(@mdata,"$id|$msub|$oboard");
		}
		fclose(FILE);
	}

	opendir(DIR,"$messages/");
	while($show = readdir(DIR)) {
		if($show =~ /\.mail/) { push(@messages,$show); }
	}
	closedir(DIR);

	foreach(@messages) {
		($open,$t) = split(/\./,$_);
		fopen(FILE,"$messages/$open.mail");
		while( $show = <FILE> ) { chomp $show;
			if($username eq $show) { push(@allmessages,$open); }
		}
		fclose(FILE);
	}

	$callt = qq~<img src="$images/notify_sm.gif"> $notify[17]~;
	$morecaller = $notify[5];

	$displaycenter = <<"EOT";
<script language="javascript">
<!-- //
function check(what) {
 for(i = 0; i < document.notify.elements.length; i++) { document.notify.elements[i].checked = what; }
}
// -->
</script>
<table class="border" cellpadding="4" cellspacing="1" width="97%" align="center">
 <tr><form action="$scripturl,v=memberpanel,a=notify,s=delete" method="post" name="notify">
  <td class="catbg"><span class="smalltext"><b>$notify[19]</b></span></td>
 </tr><tr>
  <td class="win">
   <table width="100%"><tr>
EOT
	$cnt = 0;
	foreach(@bdata) {
		($id,$name,$message) = split(/\|/,$_);
		$message =~ s/&#47;/\//gsi;
		&BC;
		++$cnt;
		$displaycenter .= qq~$next<td valign="top" width="10"><input type="checkbox" class="checkboxinput" name="b_$id" value="1"></td><td width="50%" valign="top"><b><a href="$surl,v=mindex,b=$id">$name</a></b><br><span class="smalltext">$message</span></td>~;
		if($cnt == 2) { $next = "</tr><tr>"; $cnt = 0; } else { $next = ''; }
	}
	if($cnt == 1) { $displaycenter .= "<td>&nbsp;</td><td>&nbsp;</td>"; }
	if(!$bdata[0]) { $displaycenter .= qq~<td align="center"><br>$notify[20]<br><br></td>~; }
	$displaycenter .= <<"EOT";
  </tr></table></td>
 </tr><tr>
  <td class="catbg"><span class="smalltext"><b>$notify[11]</b></span></td>
 </tr><tr>
  <td class="win"><b>
EOT
	foreach(@mdata) {
		($id,$msub,$oboard,$rcnt) = split(/\|/,$_);
		foreach $viewid (@allmessages) {
			if($id eq $viewid) { $displaycenter .= qq~&nbsp;<input type="checkbox" class="checkboxinput" name="$id" value="1"> <a href="$surl,v=display,m=$id,b=$oboard">$msub</a><br>~; }
		}
	}
	if(!$allmessages[0]) { $displaycenter .= "</b><br><center>$notify[12]</center><br><b>"; }

	$displaycenter .= "</b></td></tr>";
	if($allmessages[0] || $bdata[0]) {
		$displaycenter .= <<"EOT";
 <tr>
  <td class="win2" align="center">
   <table cellpadding="0" cellspacing="0" width="100%">
    <tr>
     <td><input type="submit" class="button" value=" $notify[13] "></td>
     <td align="right"><span class="smalltext"><a href="javascript:check(true)">$notify[15]</a> <b>::</b> <a href="javascript:check(false)">$notify[16]</a></span></td>
    </tr>
   </table>
  </td>
 </tr>
EOT
	}
	$displaycenter .= "</form></table>";
}

sub NotifyDel {
	while(($mdat,$on) = each(%FORM)) {
		if($mdat =~ /b_(.+?)\Z/gsi) {
			fopen(FILE,"$boards/$1.mail");
			@bdat = <FILE>;
			fclose(FILE);
			chomp @bdat;
			fopen(FILE,">$boards/$1.mail");
			foreach(@bdat) { unless($username eq $_) { print FILE $_; } else { $found = 1; } }
			fclose(FILE);
			if(-s("$boards/$1.mail") == 0) { unlink("$boards/$1.mail"); }
		}
		if(-e("$messages/$mdat.mail")) {
			fopen(FILE,"$messages/$mdat.mail");
			@mdat = <FILE>;
			fclose(FILE);
			chomp @mdat;
			fopen(FILE,">$messages/$mdat.mail");
			foreach(@mdat) { unless($username eq $_) { print FILE $_; } else { $found = 1; } }
			fclose(FILE);
			if(-s("$messages/$mdat.mail") == 0) { unlink("$messages/$mdat.mail"); }
		}
	}
	if(!$found) { &error($notify[14]); }
	redirect("$scripturl,v=memberpanel,a=notify");
}
1;