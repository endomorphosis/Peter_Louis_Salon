################################################
# E-Blah Bulliten Board Systems       Platinum #
################################################
# Copyright (c) 2001 - 2004 e-blah!            #
# All Rights Reserved.                         #
################################################

CoreLoad('Report',1);

sub PMReport {
	fopen(FILE,"$members/$username.pm");
	@pms = <FILE>;
	fclose(FILE);
	chomp @pms;
	if($URL{'f'} != 1) { &error($reporttxt[1]); }
		else { $pmfolder = $reporttxt[2]; $ofold = 1; }
	foreach (@pms) {
		($folder,$pmnum,$subject,$sender,$message,$uip) = split(/\|/,$_);
		if($folder eq $ofold && $pmnum eq $URL{'m'}) {
			$pmsub = $subject;
			$pmsender = $sender;
			$pmmessage = $message;
			$pmip = $uip;
			if($sender eq $username) { &error($reporttxt[1]); }
			last;
		}
	}
	if($pmsub eq '') { &error($reporttxt[3]); }
	fopen(FILE,"$prefs/ReportPM.txt");
	@reporter = <FILE>;
	fclose(FILE);
	chomp @reporter;
	foreach(@reporter) {
		($t,$t,$t,$t,$t,$t,$t,$useme,$t,$useuser) = split(/\|/,$_);
		if($useme eq $URL{'m'} && $useuser eq $username) { &error($reporttxt[4]); }
	}
	if($URL{'s'} eq 'yes') { &PMReport2; }
	$title = $reporttxt[5];
	&header;
	$ebout .= <<"EOT";
<table cellpadding="4" cellspacing="1" class="border" width="700" align="center">
 <tr><form action="$scripturl,v=report,m=$URL{'m'},a=pmreport,s=yes,f=$URL{'f'}" method="post" name="post">
  <td class="titlebg"><b><img src="$images/report_sm.gif"> $title</b></td>
 </tr><tr>
  <td class="win"><span class="smalltext">$reporttxt[6]<hr size="1" color="$color{'border'}" class="hr">$reporttxt[44]</span></td>
 </tr><tr>
  <td class="win2">
   <table width="100%">
    <tr>
     <td align="right" width="35%"><b>$reporttxt[7]:</b></td>
     <td width="65%">$pmsub</td>
    </tr><tr>
     <td align="right" width="35%"><b>$reporttxt[8]:</b></td>
     <td width="65%">$pmfolder</td>
    </tr>
   </table>
  </td>
 </tr><tr>
  <td class="win">
   <table width="100%">
    <tr>
     <td align="right" valign="top" width="35%"><b>$reporttxt[9]:</b></td>
     <td width="65%"><select name="sendto"><option value="all admin">$reporttxt[10]</option>
EOT
	@adminindex = FindRanks('Administrator');
	foreach(@adminindex) { $ebout .= qq~<option value="$_">$userset{$_}->[1]</option>~; }
	$ebout .= <<"EOT";
     </select></td>
    </tr>
   </table>
  </td>
 </tr><tr>
  <td class="win2">
   <table width="100%">
     <tr>
     <td align="right" valign="top" width="35%"><b>$reporttxt[11]:</b></td>
     <td width="65%"><textarea name="reason" rows="7" cols="60" wrap="virtual"></textarea></td>
    </tr>
   </table>
  </td>
 </tr><tr>
  <td class="win" align="center"><input type="submit" class="button" name="submit" value=" $reporttxt[12] "></td>
 </form></tr>
</table>
EOT
	&footer;
	exit;
}

sub PMReport2 {
	$reason = &Format($FORM{'reason'});
	$sendto = &Format($FORM{'sendto'});
	$reporttime = time;
	fopen(FILE,"+>>$prefs/ReportPM.txt");
	print FILE "$reporttime|$pmsub|$pmsender|$pmmessage|$reason|$pmip|$ENV{'REMOTE_ADDR'}|$URL{'m'}|$sendto|$username\n";
	fclose(FILE);
	redirect("$scripturl,v=memberpanel,a=pm");
}

sub Report {
	&is_member;
	if($username eq 'Guest') { error($gtxt{'noguest'}); }
	if($URL{'a'} eq 'pmreport') { &PMReport; }
	fopen(FILE,"$boards/$URL{'b'}.msg");
	while (<FILE>) {
		chomp;
		($mid, $subject) = split(/\|/,$_);
		if($mid == $URL{'m'}) { $msub = $subject; $fnd = 1; last; }
	}
	fclose(FILE);
	if($fnd != 1) { &error($reporttxt[3]); }
	if($URL{'a'} eq 'r2') { &Report2; }

	$title = $reporttxt[14];
	&header;
	$ebout .= <<"EOT";
<table cellpadding="4" cellspacing="1" class="border" width="700" align="center">
 <tr><form action="$scripturl,v=report,m=$URL{'m'},n=$URL{'n'},a=r2" method="post" name="post">
  <td class="titlebg"><b><img src="$images/report_sm.gif"> $title</b></td>
 </tr><tr>
  <td class="win"><span class="smalltext">$reporttxt[44]</span></td>
 </tr><tr>
  <td class="win2">
   <table width="100%">
    <tr>
     <td align="right" width="35%"><b>$reporttxt[7]:</b></td>
     <td width="65%">$msub</td>
    </tr>
   </table>
  </td>
 </tr><tr>
  <td class="win">
   <table width="100%">
    <tr>
     <td align="right" valign="top" width="35%"><b>$reporttxt[49]:</b></td>
     <td width="65%"><select name="report"><option value="$URL{'n'}">$reporttxt[17]</option><option value="all">$reporttxt[18]</option></select></td>
    </tr><tr>
     <td align="right" valign="top" width="35%"><b>$reporttxt[9]:</b></td>
     <td width="65%"><select name="sendto"><option value="all admin">$reporttxt[10]</option>
EOT
	if(@mods) {
		$ebout .= qq~<option value="mods">$reporttxt[21]</option><option value="ma" selected>$reporttxt[22]</option>~;
		foreach (@mods) {
			&loaduser($_);
			if($userset{$_}->[1] eq '') { next; }
			$ebout .= qq~<option value="$_">$userset{$_}->[1]</option>~;
		}
	}
	$ebout .= <<"EOT";
     </select></td>
    </tr>
   </table>
  </td>
 </tr><tr>
  <td class="win2">
   <table width="100%">
     <tr>
     <td align="right" valign="top" width="35%"><b>$reporttxt[11]:</b></td>
     <td width="65%"><textarea name="reason" rows="7" cols="60" wrap="virtual"></textarea></td>
    </tr>
   </table>
  </td>
 </tr><tr>
  <td class="win" align="center"><input type="submit" class="button" name="submit" value=" $reporttxt[12] "></td>
 </form></tr>
</table>
EOT
	&footer;
	exit;
}

sub Report2 {
	&error($reporttxt[25]) if($FORM{'reason'} eq '');

	# Get all the admins into an array
	@adminslist = FindRanks('Administrator');

	# Find who we should send this report to
	if($FORM{'sendto'} eq 'all admin' || $FORM{'sendto'} eq 'ma') { push(@sendto,@adminslist); }
	if($FORM{'sendto'} eq 'mods' || $FORM{'sendto'} eq 'ma') { push(@sendto,@mods); }
	if(@sendto == 0) { push(@sendto,$FORM{'sendto'}); }

	foreach(@sendto) {
		if($senta{$_}) { next; } # Already sent
		$senta{$_} = 1;
		push(@sendsto,$userset{$_}->[2]);
	}

	if($settings[2] eq '') { $settings[2] = "$ENV{'REMOTE_ADDR'}"; }

	$sendmessage = <<"EOT";
"$settings[2]" ($username) $reporttxt[26]

<a href="$rurl,v=display,m=$URL{'m'},b=$URL{'b'},s=$URL{'n'}#$URL{'n'}">$rurl,v=display,m=$URL{'m'},b=$URL{'b'},s=$URL{'n'}#$URL{'n'}</a>

$reporttxt[27]:
$FORM{'reason'}
EOT
	foreach(@sendsto) { smail($_,$reporttxt[28],$sendmessage,$settings[2]); }
	redirect("$scripturl,v=display,m=$URL{'m'},s=$URL{'n'}");
}

sub AdminView {
	&is_admin;
	fopen(FILE,"$prefs/ReportPM.txt");
	@reports = <FILE>;
	fclose(FILE);
	chomp @reports;
	foreach(@reports) {
		($vrtime,$vsub,$vpmsender,$vmessage,$vreason,$vip,$vrip,$vmid,$recieveuser,$vsender,$new) = split(/\|/,$_);
		if($recieveuser eq 'all admin' || $username eq $recieveuser) { push(@vreports,"$vrtime|$vsub|$vpmsender|$vmessage|$vreason|$vip|$vrip|$vmid|$vsender|$new"); }
	}
	if($URL{'p'} eq 'admin') { &AdminDelete; }
	if($URL{'p'} ne '') { &AdminView2; }
	$title = $reporttxt[29];
	&headerA;
	$ebout .= <<"EOT";
<table class="border" cellpadding="4" cellspacing="1" width="98%" align="center">
 <tr>
  <td class="titlebg" colspan="5"><b><img src="$images/report_sm.gif"> $title</b></td>
 </tr><tr>
  <td class="catbg"><span class="smalltext"><b>$reporttxt[30]</b></span></td>
  <td class="catbg" align="center"><span class="smalltext"><b>$reporttxt[31]</b></span></td>
  <td class="catbg" align="center"><span class="smalltext"><b>$reporttxt[48]</b></span></td>
  <td class="catbg" align="center"><span class="smalltext"><b>$reporttxt[32]</b></span></td>
  <td class="catbg" align="center"><span class="smalltext"><b>$reporttxt[33]</b></span></td>
 </tr>
EOT
	foreach(reverse @vreports) {
		($vrtime,$vsub,$vpmsender,$vmessage,$vreason,$vip,$vrip,$vmid,$vsender,$new) = split(/\|/,$_);
		$reporttime = get_date($vrtime);
		loaduser($vsender);
		if($userset{$vsender}->[1] eq '') { $sender = $vsender; }
			else { $sender = qq~<a href="$scripturl,v=memberpanel,a=view,u=$vsender">$userset{$vsender}->[1]</a>~; }
		loaduser($vpmsender);
		if($userset{$vpmsender}->[1] eq '') { $pmsender = $vpmsender; }
			else { $pmsender = qq~<a href="$scripturl,v=memberpanel,a=view,u=$vpmsender">$userset{$vpmsender}->[1]</a>~; }
		$msenttime = get_date($vmid);
		if($new eq '') { $new = qq~ <img src="$images/new.gif">~; } else { $new = ''; }
		$ebout .= <<"EOT";
 <tr>
  <td class="win2"><span class="smalltext"><b><a href="$scripturl,v=admin,a=reports,p=$vrtime">$vsub</a>$new</b></span></td>
  <td class="win" align="center"><span class="smalltext">$sender</span></td>
  <td class="win2" align="center"><span class="smalltext">$pmsender</span></td>
  <td class="win"><span class="smalltext">$reporttime</span></td>
  <td class="win2"><span class="smalltext">$msenttime</span></td>
 </tr>
EOT
		$gotone = 1;
	}
	if($gotone eq '') {
		$ebout .= <<"EOT";
 <tr>
  <td class="win" colspan="5"><b>$reporttxt[34]</b></td>
 </tr>
EOT
	}
	$ebout .= "</table>";
	&footerA;
	exit;
}

sub AdminView2 {
	&is_admin;
	$title = $reporttxt[35];
	&headerA;
	$ebout .= <<"EOT";
<table class="border" cellpadding="4" cellspacing="1" width="98%" align="center">
 <tr>
  <td class="titlebg"><b><img src="$images/report_sm.gif"> $title</b></td>
 </tr>
EOT
	foreach(@vreports) {
		($vrtime,$vsub,$vpmsender,$vmessage,$vreason,$vip,$vrip,$vmid,$vsender) = split(/\|/,$_);
		if($URL{'p'} ne $vrtime) { next; }
		$reporttime = get_date($vrtime);
		loaduser($vsender);
		if($userset{$vsender}->[1] eq '') { $sender = $vsender; }
			else { $sender = qq~<a href="$scripturl,v=memberpanel,a=view,u=$vsender">$userset{$vsender}->[1]</a>~; }
		loaduser($vpmsender);
		if($userset{$vpmsender}->[1] eq '') { $pmsender = $vpmsender; }
			else { $pmsender = qq~<a href="$scripturl,v=memberpanel,a=view,u=$vpmsender">$userset{$vpmsender}->[1]</a>~; }
		$msenttime = get_date($vmid);
		if($vreason eq '') { $vreason = $reporttxt[45]; }
		$message = $vmessage;
		&BC;
		CoreLoad('PM');
		&PMCODE;
		$message2 = $message;
		$message = $vreason;
		&BC;
		$ebout .= <<"EOT";
 <tr>
  <td class="win" align="center">
   <table width="100%" cellpadding="4" cellspacing="0">
    <tr>
     <td align="right" width="200"><span class="smalltext"><b>$reporttxt[30]:</b></span></td>
     <td><span class="smalltext">$vsub</span></b></span></td>
    </tr><tr>
     <td align="right" width="200"><span class="smalltext"><b>$reporttxt[31]:</b></span></td>
     <td><span class="smalltext">$sender</span></td>
    </tr><tr>
     <td align="right" width="200"><span class="smalltext"><b>$reporttxt[48]:</b></span></td>
     <td><span class="smalltext">$pmsender</span></td>
    </tr>
   </table>
  </td>
 </tr><tr>
  <td class="win2" align="center">
   <table width="100%" cellpadding="4" cellspacing="0">
    <tr>
     <td align="right" width="200"><span class="smalltext"><b>$reporttxt[32]:</b></span></td>
     <td><span class="smalltext">$reporttime</span></td>
    </tr><tr>
     <td align="right" width="200"><span class="smalltext"><b>$reporttxt[33]:</b></span></td>
     <td><span class="smalltext">$msenttime</span></td>
    </tr>
   </table>
  </td>
 </tr><tr>
  <td class="win" align="center">
   <table width="100%" cellpadding="4" cellspacing="0">
    <tr>
     <td align="right" width="200"><span class="smalltext"><b>$gtxt{'18'} $reporttxt[40]:</b></span></td>
     <td><span class="smalltext"><a href="$scripturl,v=mod,a=ban,ip=$vrip,tf=report">$vrip</span></td>
    </tr><tr>
     <td align="right" width="200"><span class="smalltext"><b>$gtxt{'18'} $reporttxt[41]:</b></span></td>
     <td><span class="smalltext"><a href="$scripturl,v=mod,a=ban,ip=$vip,tf=report">$vip</span></td>
    </tr>
   </table>
  </td>
 </tr><tr><form action="$scripturl,v=admin,a=reports,p=admin" method="post" name="post">
  <td class="win2"><input type="hidden" name="delete" value="$URL{'p'}"><input type="submit" class="button" value="$reporttxt[42]" name="submit"></td>
 </form></tr>
</table><br>
<table class="border" cellpadding="4" cellspacing="1" width="750" align="center">
 <tr>
  <td class="catbg"><b>$reporttxt[46]</b></td>
 </tr><tr>
  <td class="win">$message</td>
 </tr>
</table><br>
<table class="border" cellpadding="4" cellspacing="1" width="750" align="center">
 <tr>
  <td class="catbg"><b>$reporttxt[47]</b></td>
 </tr><tr>
  <td class="win2">$message2</td>
 </tr>
</table>
EOT
		$fnd = 1;
		last;
	}
	if($fnd eq '') { $ebout .= "</table><br>"; &error($reporttxt[43]); }
	fopen(FILE,"+>$prefs/ReportPM.txt");
	foreach(@reports) {
		($vrtime,$vsub,$vpmsender,$vmessage,$vreason,$vip,$vrip,$vmid,$recieveuser,$vsender,$new) = split(/\|/,$_);
		if($vrtime eq $URL{'p'} && $new eq '') {
			print FILE "$vrtime|$vsub|$vpmsender|$vmessage|$vreason|$vip|$vrip|$vmid|$recieveuser|$vsender|1\n";
		} else { print FILE "$_\n"; }
	}
	fclose(FILE);
	$ebout .= "</table>";
	&footerA;
	exit;
}

sub AdminDelete {
	&is_admin;
	fopen(FILE,"+>$prefs/ReportPM.txt");
	foreach(@reports) {
		($vrtime,$vsub,$vpmsender,$vmessage,$vreason,$vip,$vrip,$vmid,$recieveuser,$vsender,$new) = split(/\|/,$_);
		if($recieveuser ne $username && $recieveuser ne 'all admin') { print FILE "$_\n"; next; }
		if($vrtime ne $FORM{'delete'}) { print FILE "$_\n"; next; }
	}
	fclose(FILE);
	redirect("$scripturl,v=admin,a=reports");
}
1;
