################################################
# E-Blah Bulliten Board Systems       Platinum #
################################################
# Copyright (c) 2001 - 2004 e-blah!            #
# All Rights Reserved.                         #
################################################

CoreLoad('Post',1);

sub Post {
	if($URL{'a'} eq 'smilies') { &Smilies; }
	if($username eq 'Guest' && !$noguestp) { &error($gtxt{'noguest'}); }
	is_member();
	if($URL{'a'} eq 'modify' || $URL{'q'} ne '') {
		if($URL{'q'} ne '') { $URL{'n'} = $URL{'q'}; }
		if($username eq 'Guest' && $URL{'a'} eq 'modify') { &error($posttxt[2]); }
		&GetMessages;
		&GetMessage;
		$counter = 0;
		$wefnd = 0;
		foreach(@messagez) {
			if($counter != $URL{'n'}) { ++$counter; next; }
			($postinguser,$message,$ip,$email,$date,$smile,$t,$t,$t,$modsource) = split(/\|/,$_);
			$wefnd = 1;
			last;
		}
		if($wefnd != 1) { &error($posttxt[3]); }
		if($URL{'a'} eq 'modify' && ($settings[4] ne 'Administrator' && !$ismod && !$modon && $username ne $postinguser && !$modifyon)) { &error($posttxt[4]); }
		$qmodify = Unformat($message);
		if($URL{'a'} eq 'modify') {
			if($modifytime && ($settings[4] ne 'Administrator' && !$ismod && !$modon && !$modifyon) && $date+($modifytime*3600) < time) { &error($posttxt[129]); }

			$title = $posttxt[119];
			if($URL{'n'} eq '0') {
				$titleed = $mtitle; $sel{$micon} = " selected";

				fopen(FILE,"$messages/$URL{'m'}.poll");
				@polldata = <FILE>;
				fclose(FILE);
				chomp @polldata;
				$psub = $polldata[0];

				if(!$preview) {
					$res{1} = '';
					$res2{1} = '';
					$i = 1;
					foreach $pollops (@polldata) {
						$data .= $pollops."<br>";
						($type,$value,$opvalue) = split(/\|/,$pollops);
						if($type eq 'op') { $value{$i} = $value; $currentvalue{$i} = $opvalue; ++$i; }
						elsif($type eq 'res') { $res{$value} = " checked"; }
						elsif($type eq 'res2') { $res2{$value} = " checked"; }
					}
				}
			}
		} else { $title = "$posttxt[6] '$mtitle' $posttxt[5]"; $qmodify =~ s/\[quot(.*?)\](.*?)\[\/quote\]//gsi; $qmodify = "\[quote by=$postinguser link=$scripturl,m=$URL{'m'},s=$URL{'q'} date=$date\]$qmodify\[/quote\]\n\n"; }
		$psmiley{$smile} = " checked";
	} elsif($URL{'a'} eq 'poll') {
		if($binfo[5] == 1) { &error($posttxt[7]); }
		$poll = 1;
		$title = $posttxt[8];
	} elsif($URL{'m'} ne '') {
		&GetMessages;
		&GetMessage;
		fopen(FILE,"$messages/$URL{'m'}.mail");
		@curlist = <FILE>;
		fclose(FILE);
		chomp @curlist;
		if(!$FORM{'preview'} || $FORM{'xout'}) {
			foreach(@curlist) {
				if($username eq $_) { $notify = ' checked'; }
			}
			if($notify ne ' checked') { $notify = ''; }
		}

		$title = "$posttxt[6] '$mtitle'";
	}
		else { $title = $posttxt[10]; }
	if($URL{'post'} == 1) { &PostThread; }

	$res{'1'} = " checked";
	$results = " checked";

	&PostRights;
	if($micon eq '') { $micon = 'xx'; }
	&Post2;
}

sub Smilies {
	if(!$upbc) { &error($gtxt{'error'}); }
	print "Content-type: text/html\n\n";
	if($smiliestemplate eq '') {
		$smiliestemplate = "$templates/Smilies.html";
		$smiliestemplate = "$prefs/Smilies.html" if !(-e $smiliestemplate);
	}

	$title = $posttxt[11];

	if($smiliestemplate =~ /.html/) {
		fopen(FILE,"$smiliestemplate");
		@temp = <FILE>;
		fclose(FILE);
		chomp @temp;
		foreach(@temp) {
			$_ =~ s/<blah v="\$(.+?)">/${$1}/gsi;
			if($_ =~ /<blah main>/) { $sfoot = 1; next; }
			elsif(!$sfoot) { $header .= $_; }
				else { $footer .= $_; }
		}
	}

	print $header;
	print <<"EOT";
<table class="border" cellpadding="4" cellspacing="1" width="95%" align="center">
 <tr><script language="JavaScript" src="$bcloc" type="text/javascript"></script>
  <td class="titlebg"><b><img src="$images/smiley.gif"> $title</b></td>
 </tr><tr>
  <td class="win">
   <table cellpadding="2" cellspacing="0" width="100%">
EOT
	fopen(FILE,"$prefs/smiley.txt");
	@smilies = <FILE>;
	fclose(FILE);
	chomp @smilies;
	foreach(@smilies) {
		($smilie,$url) = split(/\|/,$_);
		if(!$show) {
			$ebout .= qq~<tr>~;
			$show = 1;
		}
		++$show;
		$smilie =~ s/'/\\'/gsi;
		print qq~<td align="center"><img src="$simages/$url" border="0" onClick="openuse('$smilie');" onMouseOver="this.style.cursor='hand';"></td>~;
		if($show > 8) { print "</tr>"; $show = 0; next; }
	}
	if($show && $show < 8) {
		$span = 9-$show;
		print qq~<td colspan="$span">&nbsp;</td></tr>~;
	}
	print <<"EOT";
   </table>
  </td>
 </tr>
</table>
EOT
	print $footer;
	exit;
}

sub PostRights {
	if($URL{'m'} eq '') {
		if($binfo[3] == 0 || $binfo[3] eq '') { return; }
		elsif($binfo[3] == 1 && $username eq 'Guest') { &error($posttxt[12]); }
		elsif($binfo[3] == 2 && $settings[4] ne 'Administrator') { &error($posttxt[13]); }
		elsif($binfo[3] == 3) { &error($posttxt[14]); }
		elsif($binfo[3] == 4 && (!$ismod && $settings[4] ne 'Administrator')) { &error($posttxt[13]); }
	} else {
		if($binfo[4] == 0 || $binfo[4] eq '') { return; }
		elsif($binfo[4] == 1 && $username eq 'Guest') { &error($posttxt[15]); }
		elsif($binfo[4] == 2 && $settings[4] ne 'Administrator') { &error($posttxt[16]); }
		elsif($binfo[4] == 3) { &error($posttxt[17]); }
		elsif($binfo[4] == 4 && (!$ismod && $settings[4] ne 'Administrator')) { &error($posttxt[16]); }
	}
}

sub GetMessages {
	fopen(FILE,"$boards/$URL{'b'}.msg");
	@msg = <FILE>;
	fclose(FILE);
	chomp(@msg);
	foreach(@msg) {
		($mid,$mtitle,$t,$t,$t,$modifypoll,$locked,$micon) = split(/\|/,$_);
		if($mid == $URL{'m'}) { $fnd = 1; last; }
	}
	if($fnd != 1) { &error("$posttxt[18] $URL{'m'}"); }
	if($locked == 1 || $locked == 3) { &error($posttxt[19]); }
}

sub GetMessage {
	fopen(FILE,"$messages/$URL{'m'}.txt");
	@messagez = <FILE>;
	fclose(FILE);
	chomp @messagez;
}

sub StartPoll {
	$ebout .= <<"EOT";
<table class="border" cellspacing="1" cellpadding="3" width="750" align="center">
 <tr>
  <td class="titlebg"><b><img src="$images/poll_icon.gif"> $posttxt[20]</b></td>
 </tr><tr>
  <td class="win">
   <table cellpadding="4" cellspacing="0" width="100%">
    <tr>
     <td align="right" width="30%" valign="top"><b>$var{'67'} $posttxt[125]:</b></td>
     <td width="70%"><input type="text" class="textinput" name="psubject" value="$psub" maxlength="50" size="40"></td>
    </tr>
   </table>
  </td>
 </tr><tr>
  <td class="win2">
   <table cellpadding="2" cellspacing="0" width="100%">
EOT
	for($i = 1; $i <= $pollops; ++$i) {
		$ebout .= <<"EOT";
    <tr>
     <td align="right" width="30%"><span class="smalltext"><b>$posttxt[117] $i.</b></span></td>
     <td width="70%"><input type="text" class="textinput" name="$i" value="$value{$i}" maxlength="100" size="30"></td>
    </tr>
EOT
		if($settings[4] eq 'Administrator') {
			$ebout .= <<"EOT";
    <tr>
     <td>&nbsp;</td>
     <td><span class="smalltext">$posttxt[127]: <input type="text" class="textinput" name="currentvalue_$i" value="$currentvalue{$i}" size="4" maxlength="4"></span></td>
    </tr>
EOT
		}
	}
	$ebout .= <<"EOT";
    <tr>
     <td colspan="2" align="center"><span class="smalltext"><b>$posttxt[23]</b></span></td>
    </tr>
   </table>
  </td>
 </tr><tr>
  <td class="win">
   <table cellpadding="4" cellspacing="0" width="100%">
    <tr>
     <td align="right" width="40%"><b>$posttxt[24]</b></td>
     <td width="60%" valign="top"><input type="checkbox" class="checkboxinput" name="results" value="1"$res{'1'}> &nbsp; <span class="smalltext">$posttxt[25]</span></td>
    </tr><tr>
     <td align="right" width="40%"><b>$posttxt[123]</b></td>
     <td width="60%" valign="top"><input type="checkbox" class="checkboxinput" name="multi" value="1"$res2{'1'}> &nbsp; <span class="smalltext">$posttxt[124]</span></td>
    </tr>
EOT
	if($URL{'m'}) {
		$ebout .= <<"EOT";
    <tr>
     <td colspan="2"><hr size="1" color="$color{'border'}" class="hr"></td>
    </tr><tr>
     <td align="right" width="40%"><b>$posttxt[128]</b></td>
     <td width="60%" valign="top"><input type="checkbox" class="checkboxinput" name="deletepoll" value="1"$res3{'1'}></td>
    </tr>
EOT
	}
	$ebout .= <<"EOT";
   </table>
  </td>
 </tr>
</table><br>
EOT
}

sub Post2 {
	$tempopen = $FORM{'tempopen'};
	if($BCLoad) { $spanclose = "</span>"; }
	$title = CensorList($title);
	&header;
	&Mods;
	if($modz) { $modz = qq~<p style="float: right"><b>$ltxt[7]:</b> $modz</p>~; }

	$ebout .= <<"EOT";
<script language="JavaScript">
<!--
function ShowImage(valy) {
 document.images.icon.src = "$images/"+valy+".gif";
}
// -->
</script>

<table cellspacing="1" width="100%" class="border">
 <tr>
  <td class="win" height="22"><span class="smalltext"><p style="float: left">&nbsp;<a href="$surl">$mbname</a> &nbsp;<b>&#155;</b> &nbsp;<a href="$surl,c=$catid">$catname</a> &nbsp;<b>&#155;</b> &nbsp;<a href="$scripturl,v=mindex">$boardnm</a> &nbsp;<b>&#155; &nbsp;$title</b></p>$modz</span></td>
 </tr>
EOT
	if($showactive) {
		&BoardUsers;
		$ebout .= <<"EOT";
 <tr>
  <td class="win"><span class="smalltext">
   <div class="win2" style="padding: 5px"><b>$var{'90'}</b></div>
   <div style="padding: 5px">$activeusers $var{'97'} $gcnt $var{'91'}</div></span>
  </td>
 </tr>
EOT
	}
	$ebout .= qq~</table><br><form action="$scripturl,v=post,m=$URL{'m'},a=$URL{'a'},q=$URL{'q'},post=1,n=$URL{'n'}" method="post" name="msend" enctype="multipart/form-data">~;

	if($error) {
	$ebout .= <<"EOT";
<table class="border" cellpadding="5" cellspacing="1" width="750" align="center">
 <tr>
  <td class="titlebg"><b><img src="$images/ban.gif"> $rtxt[41]</b></td>
 </tr><tr>
  <td class="win2">$error</td>
 </tr>
</table><br>
EOT
	}

	if($FORM{'preview'}) { $ebout .= $preview; }
	if($URL{'a'} eq 'poll' || ($titleed && $modifypoll)) { &StartPoll; }
	if($BCLoad) { $color = 'win'; }
		else { $color = 'win2'; }

	$tempopen = $tempopen ? $tempopen : time;
	$ebout .= <<"EOT";
<table class="border" cellspacing="1" cellpadding="4" width="750" align="center">
 <tr><input type="hidden" value="$tempopen" name="tempopen">
  <td class="titlebg"><b><img src="$images/thread.gif"> $title</b></td>
 </tr>
EOT
	if($modifytime && $username ne 'Guest' && ($settings[4] ne 'Administrator' && !$ismod && !$modon && !$modifyon) && $URL{'a'} ne 'modify') {
		$ebout .= <<"EOT";
 <tr>
  <td class="win2"><span class="smalltext">$posttxt[130]</span></td>
 </tr>
EOT
	}
	$ebout .= <<"EOT";
 <tr>
  <td class="$color">
   <table cellpadding="4" cellspacing="0" width="100%">
EOT
	if($URL{'m'} eq '' || $titleed) {
		if($BCLoad) { $onLoad6 = qq~<span onMouseOver="funclu('subject')" onMouseOut="funclu('wait')">~; }
		if($BCLoad) { $onLoad7 = qq~<span onMouseOver="funclu('icon')" onMouseOut="funclu('wait')">~; }
		$ebout .= <<"EOT";
    <tr>
     <td align="right" width="30%"><b>$posttxt[28] $posttxt[22]:</b></td>
     <td width="70%">$onLoad6<input type="text" class="textinput" name="subject" value="$titleed" size="40" maxlength="50" tabindex="1">$spanclose</td>
    </tr><tr>
     <td align="right" width="30%"><b>$posttxt[28] $posttxt[31]:</b></td>
     <td width="70%">$onLoad7<img src="$images/$micon.gif" name="icon"> &nbsp;<select name="micon" tabindex="2" onChange="ShowImage(this.value);">
       <option value="xx"$sel{'xx'}>$var{'0'}</option>
       <option value="lamp"$sel{'lamp'}>$var{'6'}</option>
       <option value="question"$sel{'question'}>$var{'5'}</option>
       <option value="news"$sel{'news'}>$var{'4'}</option>
       <option value="thumbup"$sel{'thumbup'}>$var{'2'}</option>
       <option value="thumbdown"$sel{'thumbdown'}>$var{'3'}</option>
       <option value="ban"$sel{'ban'}>$var{'7'}</option>
       <option value="smiley"$sel{'smiley'}>$var{'1'}</option>
       <option value="angry"$sel{'angry'}>$var{'49'}</option>
       <option value="lol"$sel{'lol'}>$var{'57'}</option>
       <option value="open_thread"$sel{'open_thread'}>$posttxt[78]</option>
       <option value="warning"$sel{'warning'}>$posttxt[79]</option>
      </select>$spanclose</td>
    </tr>
EOT
	}
	if($URL{'a'} eq 'modify') {
		loaduser($postinguser);
		if($userset{$postinguser}->[1] eq '') { $postedby = $postinguser; }
			else { $postedby = $userset{$postinguser}->[1]; }
	} else {
		if($username eq 'Guest') {
			$postedby = qq~<input type="text" class="textinput" tabindex="3" name="username" value="$unpost" size="25" maxlength="25">~;
			$guestemail = <<"EOT";
    <tr>
     <td align="right" width="30%"><b>$gtxt{'23'}:</b></td>
     <td width="70%"><input type="text" class="textinput" tabindex="4" name="email" size="30" value="$epost" maxlength="100"></td>
    </tr>
EOT
		} else { $postedby = $settings[1]; }
	}
	$ebout .= <<"EOT";
    <tr>
     <td align="right" width="30%"><b>$gtxt{'19'}:</b></td>
     <td width="70%">$postedby</td>
    </tr>$guestemail
   </table>
  </td>
 </tr>
EOT
	if($BCLoad || $BCSmile) { &BCWait; }
	if($BCLoad) { &BCLoad; }
	$qmodify =~ s/</&lt;/g;
	$qmodify =~ s/>/&gt;/g;
	$ebout .= <<"EOT";
 <tr>
  <td class="win">
   <table cellpadding="4" cellspacing="0" width="100%">
    <tr>
     <td align="right" width="30%" valign="top"><b>$posttxt[28]:</b><br><br>
EOT
	if($BCSmile) { &BCSmile; }
	if($BCLoad) { $onLoad3 = qq~<span onMouseOver="funclu('message')" onMouseOut="funclu('wait')">~; }
	$ebout .= <<"EOT";
     </td>
     <td width="70%">$onLoad3<textarea name="message" tabindex="5" wrap="virtual" rows="12" cols="80" style="width: 100%">$qmodify</textarea>$spanclose</td>
    </tr>
EOT

	if($URL{'a'} ne 'modify' && (($uallow eq 2 && $username ne 'Guest') || ($uallow eq 3 && $settings[4] eq 'Administrator') || ($uallow eq 1))) {
		$maxsize = $maxsize <= 0 ? $posttxt[36] : "$maxsize MB"; # MB
		if($maxsize > 0 && $maxsize < 1) { $maxsize = 1024*$maxsize . " KB"; } # KB
		if($allowedext ne '') {
			$aup = "<b>$posttxt[39]:</b> ";
			@au = split(/,/,$allowedext);
			foreach (@au) { $aup .= "$_, "; }
			$aup =~ s/, \Z/<br>/i;
		}

		if($tempopen && -e("$prefs/Hits/$tempopen.temp")) { # Yeah, we're going to find the current temp, and load his currently uploaded files
			fopen(TEMPFILE,"$prefs/Hits/$tempopen.temp");
			while(<TEMPFILE>) {
				chomp;
				if(!-e("$uploaddir/$_") || $alreadythere{$_}) { next; }
				$alreadythere{$_} = 1;
				$size = sprintf("%.3f",((-s("$uploaddir/$_"))/1024/1024));
				$size = "$size MB"; # MB
				if($size > 0 && $size < 1) { $size = 1024*$size . " KB"; } # KB
				$attached .= qq~<tr><td width="25" align="center"><img src="$images/disk.gif"></td><td class="smalltext">$_</td><td class="smalltext">$size</td><td><input type="submit" class="button" value="$posttxt[134]" name="del_$_"></td></tr>~;
			}
			fclose(TEMPFILE);
		}
		$ebout .= <<"EOT";
   </table>
  </td>
 </tr><tr>
  <td class="win2">
   <table cellpadding="4" cellspacing="0" width="100%">
    <tr>
     <td align="right" width="30%" valign="top"><b>$posttxt[38]:</b></td>
     <td width="70%"><input type="file" name="ulfile" size="30" class="upload"> <input type="submit" class="button" value="$posttxt[133]" name="uploadonly"><table cellpadding="4" cellspacing="0" width="100%">$attached<tr><td colspan="4" class="smalltext">$aup<b>$posttxt[40]:</b> $maxsize</td></tr></table></td>
    </tr>
   </table>
  </td>
 </tr><tr>
  <td class="win">
   <table cellpadding="4" cellspacing="0" width="100%">
    <tr>
EOT
	}
	if($URL{'a'} eq 'modify') {
		$ebout .= <<"EOT";
    <tr>
     <td align="right" width="30%"><b>$posttxt[135]:</b></td>
     <td width="70%"><input type="text" class="textinput" style="width: 100%" name="editreason" maxlength="150"></td>
    </tr>
EOT
	}
	if($URL{'a'} eq 'modify' && $modsource ne '' && $settings[4] eq 'Administrator') {
		$ebout .= <<"EOT";
    <tr>
     <td align="right" width="30%" valign="top"><b>$posttxt[136]:</b></td>
     <td width="70%" class="smalltext"><textarea name="modifyedit" wrap="off" rows="4" style="width: 100%">
EOT

		foreach(split(/\>/,$modsource)) { $ebout .= "$_\n"; }

		$ebout .= qq~</textarea>$posttxt[137]</td></tr>~;

	}

	if($username ne 'Guest') {
		if($settings[30] && ($notify ne ' unchecked' && $URL{'m'} eq '')) { $notify = ' checked'; }
		$ebout .= <<"EOT";
    <tr>
     <td align="right" width="30%"><b>$posttxt[43]</b></td>
     <td width="70%"><input type="checkbox" class="checkboxinput" name="notify" value="1"$notify> &nbsp; <span class="smalltext">$posttxt[42]</span></td>
    </tr>
EOT
	}
	if($psmiley{1} || $psmiley{3}) { $smilechecked = " checked"; }
	$ebout .= <<"EOT";
    <tr>
     <td align="right" width="30%"><b>$posttxt[44]</b></td>
     <td width="70%"><input type="checkbox" class="checkboxinput" name="smile" value="1"$smilechecked> &nbsp; <span class="smalltext">$posttxt[120]</span></td>
    </tr>
EOT
	if($html) {
		if($psmiley{2} || $psmiley{3}) { $htmlc = " checked"; }
		$ebout .= <<"EOT";
    <tr>
     <td align="right" width="30%"><b>$posttxt[81]</b></td>
     <td width="70%"><input type="checkbox" class="checkboxinput" name="html" value="2"$htmlc> &nbsp; <span class="smalltext">$posttxt[121]</span></td>
    </tr>
EOT
	}
	if($URL{'m'} eq '' && ($settings[4] eq 'Administrator' || $ismod)) {
		$ebout .= <<"EOT";
    <tr>
     <td align="right" width="30%"><b>$posttxt[45]:</b></td>
     <td width="70%"><input type="checkbox" class="checkboxinput" name="sticky" value="1" $stick{'1'}> &nbsp; <span class="smalltext">$posttxt[122]</span></td>
    </tr>
EOT
	}
	$ebout .= <<"EOT";
   </table>
  </td>
 </tr><tr>
  <td class="win2" align="center"><input type="submit" class="button" tabindex="6" value=" $posttxt[46] " name="submit">&nbsp; <input type="submit" class="button" tabindex="7" name="preview" value=" $posttxt[47] "></td>
 </form></tr>
</table>
EOT
	if($URL{'m'} ne '' && $URL{'a'} ne 'modify') { &ThreadSummary; }
	&footer;
	exit;
}

sub PostThread { # Start posting now ...
	&Mods;
	$mtime = time;
	$found = 0;
	fopen(FILE,"$prefs/ips.txt");
	@log = <FILE>;
	fclose(FILE);
	chomp @log;
	fopen(FILE,"+>$prefs/ips.txt");
	foreach (@log) {
		($remoteip,$logtime) = split(/\|/,$_);
		$plus = $mtime-$iptimeout;
		$minus = $plus-$logtime;
		if($minus < 0) {
			print FILE "$_\n";
			if($remoteip eq $ENV{'REMOTE_ADDR'}) { $found = 1; }
		}
	}
	fclose(FILE);
	if($found && $settings[4] ne 'Administrator' && $URL{'a'} ne 'modify') { &error("$posttxt[48] $iptimeout $posttxt[49]"); }
	$message = Format($FORM{'message'});
	$smiley = $FORM{'smile'}+$FORM{'html'};
	if($username eq 'Guest') {
		$tuser = Format($FORM{'username'}) || 'Guest';
		$temail = Format($FORM{'email'});
		if(-e "$members/$tuser.dat") { &error($posttxt[50]); }
			else {
				fopen(FILE,"$members/List.txt");
				@memlist = <FILE>;
				fclose(FILE);
				chomp @memlist;
				foreach (@memlist) {
					loaduser($_);
					if($tuser eq $settings{$_}->[1]) { &error($posttxt[50]); }
				}
			}

		$uscheck = lc($tuser);
		fopen(FILE,"$prefs/Names.txt");
		while(<FILE>) {
			chomp $_;
			($searchme,$within) = split(/\|/,$_);
			$searchme = lc($searchme);
			if($within) { error($posttxt[50]) if($uscheck =~ /\Q$searchme\E/gsi); }
				else { error($posttxt[50]) if($searchme eq $uscheck); }
		}
		fclose(FILE);
		$tuser .= " (Guest)";
	} else { $tuser = $username; $temail = $settings[2]; }
	error($posttxt[52]) if($tuser eq '');
	error($posttxt[53]) if($temail eq '');
	error($posttxt[80]) if($temail !~ /\A([0-9A-Za-z\._\-]{1,})@([0-9A-Za-z\._\-]{1,})+\.([0-9A-Za-z\._\-]{1,})+\Z/);

	$FORM{'tempopen'} = $FORM{'tempopen'} || time;
	if($FORM{'tempopen'}) {
		fopen(TEMPFILE,"$prefs/Hits/$FORM{'tempopen'}.temp");
		while(<TEMPFILE>) {
			chomp;
			if($FORM{"del_$_"} ne '') { unlink("$uploaddir/$_","$prefs/Hits/$_.txt"); $deleteatts = 1; next; }
			$addupload{$_} = 1;
			$totalusize += -s("$uploaddir/$_");
			if(-e("$uploaddir/$_")) { $atturl .= "$_/"; }
		}
		fclose(TEMPFILE);
		$maxsize -= sprintf("%.2f",($totalusize/1024/1024)); # With other attachments, total size goes down.  ;)
	}

	if($message eq '') { $error = $posttxt[54]; }
	if(length($message) > $maxmesslth && $maxmesslth) { $error = $gtxt{'long'}; }

	if($URL{'m'} eq '') {
		if(length($FORM{'subject'}) > 50) { $error = $posttxt[55]; }
		$subject = Format($FORM{'subject'});
		if($subject eq ' ' || $subject eq '&nbsp;' || $subject eq '') { $error = $posttxt[56]; }
	}

	if($uallow && $FORM{'ulfile'} ne '') { CoreLoad('Attach'); &Upload; }
	if($FORM{'preview'} || $FORM{'uploadonly'} || $deleteatts || $error) { &Preview; }

	for($e = 0; $e < 99; $e++) { # Find a correct time to use (for heavy boards)
		if(-e "$messages/$mtime.txt") { ++$mtime; } else { last; }
	}

	if($URL{'a'} eq 'poll') { PollSave(); PostTopic(); }
	elsif($URL{'a'} eq 'modify') { PostModify(); }
	elsif($URL{'m'} ne '') { ReplyThread(); }
		else { PostTopic(); }

	if($FORM{'tempopen'}) { # Delete the temp file now that all errors have been cleared!  =D
		$atturl =~ s/\/\Z//g;
		unlink("$prefs/Hits/$FORM{'tempopen'}.temp");
	}

	if($URL{'a'} ne 'modify') {
		if($username ne 'Guest' && $binfo[8] eq '') { # Add a post count
			++$Osettings[3];
			fopen(FILE,"+>$members/$username.dat");
			for($q = 0; $q < $usersetcount; $q++) { print FILE "$Osettings[$q]\n"; }
			fclose(FILE);
		}

		fopen(FILE,"+<$boards/$URL{'b'}.ino");
		@binfo = <FILE>;
		seek(FILE,0,0);
		truncate(FILE,0);
		chomp @binfo;
		$posts = $binfo[0]+$bi[0];
		$replys = $binfo[1]+$bi[1];
		print FILE "$posts\n$replys\n";
		fclose(FILE);

		fopen(FILE,"+>>$prefs/ips.txt");
		print FILE "$ENV{'REMOTE_ADDR'}|$mtime\n";
		fclose(FILE);
	}

	if($emails[0]) {
		foreach $mails (@emails) {
			if($mails eq $username) { next; }
			loaduser($mails);
			if($userset{$mails}->[1] ne '') {
				$norun = 0;
				fopen(FILE,"$members/$mails.log"); # We will ensure this user has only been mailed ONCE (we don't need to fill up their inbox!)
				while( <FILE> ) {
					($lstview,$lsttime) = split(/\|/,$_);
					if($lstview eq $msid && $lsttime < $lastmessagetime) { $norun = 1; last; }
				}
				fclose(FILE);
				if(!$norun) { smail($userset{$mails}->[2],$sendsubject,$sendmessage,$eadmin); }
			}
		}
	}

	redirect();
}

sub PostTopic {
	fopen(FILE,"+>$messages/$mtime.txt") || &error("$posttxt[58]: $mtime.txt",1);
	print FILE "$tuser|$message|$ENV{'REMOTE_ADDR'}|$temail|$mtime|$smiley|||$atturl\n";
	fclose(FILE);

	fopen(FILE,">$messages/$mtime.view");
	print FILE "0\n";
	fclose(FILE);

	if($FORM{'notify'} && $username ne 'Guest') {
		fopen(FILE,">$messages/$mtime.mail");
		print FILE "$username\n";
		fclose(FILE);
	}

	$micon = ValIcon($FORM{'micon'});

	if($URL{'a'} eq 'poll') { $poll = 1; } else { $poll = 0; }

	fopen(FILE,"+<$boards/$URL{'b'}.msg",1) || &error("$posttxt[58]: $URL{'b'}.msg",1);
	@fdump = <FILE>;
	foreach $mdump (@fdump) {
		($lastmessagetime) = split(/\|/,$mdump);
		push(@lastmessagetime,$lastmessagetime);
	}
	truncate(FILE,0);
	seek(FILE,0,0);
	print FILE "$mtime|$subject|$tuser|$mtime|0|$poll|0|$micon|$mtime|$tuser\n";
	print FILE @fdump;
	fclose(FILE);

	$url = "$scripturl,m=$mtime,s=0";
	if($uextlog) { ++$ExtLog[1]; &ExtClose; }
	++$bi[0];
	++$bi[1];
	if(($FORM{'sticky'} && $URL{'m'} eq '') && ($settings[4] eq 'Administrator' || $ismod)) { &DoSticky; }

	if($binfo[7]) { # Start sending everyone an e-mail ...
		foreach(@mgrps) { $onlygrps{$_} = 1; }

		fopen(FILE,"$members/List.txt");
		@mlist = <FILE>;
		fclose(FILE);
		chomp @mlist;
		foreach(@mlist) {
			loaduser($_);
			if(!$onlygrps{"$userset{$_}->[4]"} && $mgrps[0]) { next; } # Should we send e-mail?
			if($userset{$_}->[2] && !$userset{$_}->[25]) { push(@emails,$_); }
		}
	}
	if(-e("$boards/$URL{'b'}.mail")) {
		$msid = $URL{'b'};
		@lastmessagetime = sort {$b <=> $a} @lastmessagetime;
		$lastmessagetime = $lastmessagetime[0];

		fopen(FILE,"$boards/$URL{'b'}.mail");
		while(<FILE>) { chomp $_; push(@emails,$_); }
		fclose(FILE);
	}

	if($emails[0]) {
		$sendsubject = "$posttxt[10]: $subject";
		$sendmessage = qq~$posttxt[59] "$boardnm" $posttxt[76] "$mbname".\n\n$posttxt[60] <a href="$rurl,b=$URL{'b'},m=$mtime,s=0">$rurl,b=$URL{'b'},m=$mtime,s=0</a>.\n\n\n$gtxt{'25'}~;
	}
}

sub ReplyThread {
	my($addtolist,$activated,$resave);
	$msid = $URL{'m'};

	# Lets load the mail database, which needs reading twice this run
	fopen(FILE,"$messages/$msid.mail");
	@emails = <FILE>;
	fclose(FILE);
	chomp @emails;

	fopen(FILE,"+>>$messages/$msid.txt") || &error("$posttxt[58]: $msid.txt",1);
	print FILE "$tuser|$message|$ENV{'REMOTE_ADDR'}|$temail|$mtime|$smiley|||$atturl\n";
	fclose(FILE);

	# Notify tick on/off add/remove
	if($username ne 'Guest') {
		if($FORM{'xout'}) { $FORM{'notify'} = $notify; }

		foreach(@emails) {
			if($_ ne $username) { $addtolist .= "$_\n"; }
			elsif($_ eq $username && $FORM{'notify'}) { $addtolist .= "$_\n"; $activated = 1; }
			elsif($_ eq $username && !$FORM{'notify'}) { $resave = 1; }
		}

		if($resave || (!$activated && $FORM{'notify'})) {
			fopen(FILE,">$messages/$msid.mail");
			print FILE $addtolist;
			if($FORM{'notify'} && !$activated) { print FILE "$username\n"; }
			fclose(FILE);
		}
	}

	if($URL{'a'} eq 'poll') { $poll = 1; } else { $poll = 0; }

	fopen(FILE,"+<$boards/$URL{'b'}.msg",1) || &error("$posttxt[58]: $URL{'b'}.msg",1);
	@fdump = <FILE>;
	foreach(@fdump) {
		if($_ =~ m/\A$msid\|/) { $receive = $_; last; }
	}
	truncate(FILE,0);
	seek(FILE,0,0);
	chomp $receive;
	($miduse,$subject,$tempposted,$trdate,$replies,$poll,$type,$micon) = split(/\|/,$receive);

	$start = $replies+1;

	print FILE "$msid|$subject|$tempposted|$trdate|$start|$poll|$type|$micon|$mtime|$tuser\n";

	foreach(@fdump) {
		if($_ =~ m/\A$msid\|/) { next; }
		print FILE $_;
	}
	fclose(FILE);

	# Notify uses in the database
	if(@emails) {
		($t,$t,$t,$t,$lastmessagetime) = split(/\|/,$messagez[@messagez-1]);

		$postedonthisdate = get_date($mtime,1);
		if($username eq 'Guest') { $usersentthis = $tuser; } else { $usersentthis = $settings[1]; }

		$sendsubject = "$posttxt[64] $subject";
		$sendmessage = qq~$posttxt[62]\n\n$posttxt[116] $postedonthisdate, $usersentthis $posttxt[63], "<a href="$rurl,m=$msid,s=$start,b=$URL{'b'}">$subject</a>".\n\n\n$gtxt{'25'}~;
	}

	$url = "$scripturl,m=$msid,s=$start,#num$start";
	if($uextlog) { ++$ExtLog[2]; &ExtClose; }
	++$bi[1];
}

sub PostModify {
	my($tempeditreason);
	$counter = 0;

	if($URL{'n'} == 0) {
		if(length($FORM{'subject'}) > 50) { &error($posttxt[55]); }
		$subject = Format($FORM{'subject'});
		$micon = Format($FORM{'micon'}) || 'xx';
		$micon = ValIcon($FORM{'micon'});

		if($titleed && $modifypoll && !$FORM{'deletepoll'}) { PollSave(); }

		fopen(FILE,"+<$boards/$URL{'b'}.msg",1) || error("$posttxt[58]: $URL{'b'}.msg",1);
		@fdump = <FILE>;
		for($i = 0; $i < @fdump; $i++) {
			if($fdump[$i] =~ m/\A$URL{'m'}\|/) {
				chomp $fdump[$i];
				($miduse,$t,$tempposted,$trdate,$replies,$poll,$type,$t,$ttime,$tuser) = split(/\|/,$fdump[$i]);
				if($FORM{'deletepoll'}) { unlink("$messages/$URL{'m'}.poll","$messages/$URL{'m'}.polled"); $poll = 0; }
				$fdump[$i] = "$miduse|$subject|$tempposted|$trdate|$replies|$poll|$type|$micon|$ttime|$tuser\n";
				last;
			}
		}
		truncate(FILE,0);
		seek(FILE,0,0);
		foreach(@fdump) { print FILE $_; }
		fclose(FILE);
	}

	fopen(FILE,"$messages/$URL{'m'}.txt");
	while( $mline = <FILE> ) {
		chomp $mline;
		($uuser,$t,$oldipsave,$uemail,$ptime,$t,$oldedits,$oldedits2,$atturl,$saveedits) = split(/\|/,$mline);
		if($counter == $URL{'n'}) {
			if($oldedits) { $saveedits = "$oldedits/$oldedits2/>"; }
			$FORM{'editreason'} =~ s/(\/|\>|\n)//g;
			if($settings[4] eq 'Administrator') {
				$saveedits = '';
				foreach(split(/\n/,$FORM{'modifyedit'})) { $_ =~ s/\cM//g; $saveedits .= "$_>"; }
			}
			$curtime = time;
			$saveedits .= "$curtime/$username/".Format($FORM{'editreason'}).'>';
			$writedata .= "$uuser|$message|$oldipsave|$uemail|$ptime|$smiley|||$atturl|$saveedits\n";
		} else { $writedata .= "$mline\n"; }
		++$counter;
	}
	fclose(FILE);
	fopen(WRITE,"+>$messages/$URL{'m'}.txt");
	print WRITE $writedata;
	fclose(WRITE);
	$url = "$scripturl,m=$URL{'m'},s=$URL{'n'}";
}

sub PollSave {
	if(length($FORM{'psubject'}) > 50) { error($posttxt[55]); }
	$psubject = Format($FORM{'psubject'});
	if($psubject eq '' || $psubject eq ' ' || $psubject eq '&nbsp;') { error($posttxt[68]); }
	$ecnts = 0;
	for($e = 1; $e <= $pollops; $e++) {
		$form = Format($FORM{"$e"});
		if(length($form) > 250) { error("$posttxt[70] $e $posttxt[71]"); }
		if($settings[4] eq 'Administrator') { $currentvalue{$e} = $FORM{"currentvalue_$e"}; }
		$votecount += ($pollvcnt = $currentvalue{$e} || 0);
		if($form ne '') { push(@pollops,"$form|$pollvcnt"); }
		if($titleed) { $mtime = $URL{'m'}; }
	}
	if(@pollops < 2) { error($posttxt[72]); }

	$res = $FORM{'results'} ? 1 : 0;
	$res2 = $FORM{'multi'} ? 1 : 0;

	fopen(FILE,">$messages/$mtime.poll");
	print FILE "$psubject\n";
	print FILE "vc|$votecount\n";
	foreach(@pollops) {
		if($_ ne '') { print FILE qq~op|$_\n~; }
	}
	print FILE "res|$res\n";
	print FILE "res2|$res2\n";
	fclose(FILE);
}

sub ThreadSummary {
	if($settings[29]) { return; }
	$ebout .= <<"EOT";
<br><table class="border" cellpadding="4" cellspacing="1" width="750" align="center">
 <tr>
  <td class="titlebg" align="center"><span class="smalltext"><b>$posttxt[73]</b></span></td>
 </tr>
EOT
	foreach(@messagez) {
		($postinguser,$message,$ip,$email,$date,$nosmile) = split(/\|/,$_);
		push(@message,"$date|$postinguser|$message|$ip|$email|$nosmile");
	}
	if($reversesum) { @message = sort{$b <=> $a} @message; }

	foreach(@message) {
		if($maxsumc && $junkcounter == $maxsumc) { last; }
		($date,$postinguser,$message,$ip,$email,$nosmile) = split(/\|/,$_);

		loaduser($postinguser);
		if($userset{$postinguser}->[1] eq '') { $guestuser = 1; }

		if($username ne 'Guest') {
			foreach(@blockedusers) {
				if($postinguser eq $_ || ($guestuser && $_ eq 'Guest')) { $quit = 1; }
			}
		}
		if($quit) { $ebout .= qq~<tr><td class="win2"><span class="smalltext">$posttxt[126]</span></td></tr>~; next; }

		if($userset{$postinguser}->[1] eq '') { $postedby = $postinguser; }
			else { $postedby = qq~<a href="$scripturl,v=memberpanel,a=view,u=$postinguser">$userset{$postinguser}->[1]</a>~; }
		$datepost = get_date($date);

		if(length($message) > 1000) {
			$message =~ s~\[table\](.*?)\[\/table\]~$var{'88'}~sgi;
			$message = substr($message,0,1000);
			&BC;
			&MakeSmall;
			$message .= " ...";
		} else { &BC; }

		$ebout .= <<"EOT";
 <tr>
  <td class="win"><table cellpadding="1" width="100%">
   <tr>
    <td><span class="smalltext"><b>$gtxt{'19'}:</b> $postedby</span></td>
    <td align="right"><span class="smalltext"><b>$gtxt{'21'}:</b> $datepost</span></td>
   </tr>
  </table></td>
 </tr><tr>
  <td class="win2"><span class="smalltext">$message</span></td>
 </tr>
EOT
		++$junkcounter;
	}
	$ebout .= "</table>";
}

sub Preview {
	while(($iname,$ivalue) = each(%FORM)) {
		$ivalue =~ s/\"/&#034;/g;
		$FORM{$iname} = $ivalue;
	}

	$nosmile = $smiley;
	%psmiley = (1 => '',2 => '',3 => '');
	$qmodify = Unformat($message);
	&BC;

	$titleed = $FORM{'subject'};
	$sel{"$FORM{'micon'}"} = ' selected';
	$micon = $FORM{'micon'};

	for($i = 1; $i <= $pollops; ++$i) {
		$currentvalue{$i} = $FORM{"currentvalue_$i"};
		$value{$i} = $FORM{$i};
	}

	$psub = $FORM{'psubject'};
	$results = $FORM{'results'};
	$stick{"$FORM{'sticky'}"} = " checked";
	if($FORM{'notify'}) { $notify = " checked"; } else { $notify = ''; }
	$psmiley{$smiley} = " checked";
	$res{"$FORM{'results'}"} = " checked";
	$res2{"$FORM{'multi'}"} = " checked";
	$res3{"$FORM{'deletepoll'}"} = " checked";

	$unpost = $FORM{'username'};
	$epost = $FORM{'email'};
	if($message eq '') { $message = qq~<span class="smalltext"><i>$gtxt{'13'}</i></span>~; }

	$preview = <<"EOT";
<table class="border" cellpadding="4" cellspacing="1" width="750" align="center">
 <tr>
  <td height="100%" valign="top" class="catbg"><span class="smalltext"><b>$posttxt[47]</b></span></td>
 </tr><tr>
  <td height="100%" valign="top" class="win">$message</td>
 </tr>
</table><br>
EOT
	&Post2;
}

sub ValIcon {
	my($micon) = $_[0];
	if($micon ne 'open_thread' && $micon ne 'warning' && $micon ne 'angry' && $micon ne 'xx' && $micon ne 'lol' && $micon ne 'smiley' && $micon ne 'thumbup' && $micon ne 'thumbdown' && $micon ne 'news' && $micon ne 'question' && $micon ne 'lamp' && $micon ne 'ban') { &error($posttxt[57]); }
	return($micon);
}

sub DoSticky {
	fopen(FILE,"+>>$boards/Stick.txt");
	print FILE "$URL{'b'}|$mtime\n";
	fclose(FILE);
}

sub BCSmile {
	my($temp,$temper);
	if($BCLoad) { $onLoad = qq~<span onMouseOver="funclu('smiley')" onMouseOut="funclu('wait')">~; $onLoad2 = "</span>"; }
	if($simages2 ne '') { $temper = $simages; $simages = $simages2; }
	$temp = <<"EOT";
<table cellpadding="2" cellspacing="1" width="180" align="center" class="border">
 <tr>
  <td class="win2">
   <table cellpadding="2" cellspacing="0" width="100%">
    <tr>
     <td colspan="8"><span class="smalltext"><b>&#155; $posttxt[34]</b></span><td>
    </tr><tr>$onLoad
     <td><img src="$simages/smiley.gif" border="0" alt="$var{'44'}" onClick="use(' :)');" onMouseOver="$ghand"></td>
      <td><img src="$simages/wink.gif" border="0" alt="$var{'45'}" onClick="use(' ;)');" onMouseOver="$ghand"></td>
      <td><img src="$simages/tongue.gif" border="0" alt="$var{'46'}" onClick="use(' :P');" onMouseOver="$ghand"></td>
      <td><img src="$simages/grin.gif" border="0" alt="$var{'47'}" onClick="use(' ;D');" onMouseOver="$ghand"></td>
      <td><img src="$simages/sad.gif" border="0" alt="$var{'48'}" onClick="use(' :(');" onMouseOver="$ghand"></td>
      <td><img src="$simages/angry.gif" border="0" alt="$var{'49'}" onClick="use(' >:(');" onMouseOver="$ghand"></td>
      <td><img src="$simages/cry.gif" border="0" alt="$var{'50'}" onClick="use(' :\\'(');" onMouseOver="$ghand"></td>
      <td><img src="$simages/lipsx.gif" border="0" alt="$var{'51'}" onClick="use(' :X');" onMouseOver="$ghand"></td>
     </tr><tr>
      <td><img src="$simages/undecided.gif" border="0" alt="$var{'52'}" onClick="use(' :-/');" onMouseOver="$ghand"></td>
      <td><img src="$simages/shock.gif" border="0" alt="$var{'53'}" onClick="use(' :o');" onMouseOver="$ghand"></td>
      <td><img src="$simages/blush.gif" border="0" alt="$var{'54'}" onClick="use(' :B');" onMouseOver="$ghand"></td>
      <td><img src="$simages/cool.gif" border="0" alt="$var{'55'}" onClick="use(' 8)');" onMouseOver="$ghand"></td>
      <td><img src="$simages/kiss.gif" border="0" alt="$var{'56'}" onClick="use(' :K)');" onMouseOver="$ghand"></td>
      <td><img src="$simages/lol.gif" border="0" alt="$var{'57'}" onClick="use(' :D');" onMouseOver="$ghand"></td>
      <td><img src="$simages/roll.gif" border="0" alt="$var{'58'}" onClick="use(' ::)');" onMouseOver="$ghand"></td>
      <td><img src="$simages/huh.gif" border="0" alt="$var{'59'}" onClick="use(' ??)');" onMouseOver="$ghand"></td>
     </tr>
EOT
	if($upbc) {
		$temp .= <<"EOT";
<tr>
 <td colspan="8" align="center"><span class="smalltext"><a href="$scripturl,v=post,a=smilies" onClick="window.open('$scripturl,v=post,a=smilies','smiles','height=400,width=750,resizable=yes,scrollbars=yes'); return false;" target="smiles">$posttxt[11]</a></span></td>
</tr>
EOT
	}
	$temp .= <<"EOT";
   </table>
  </td>$onLoad2
 </tr>
</table>
EOT
	if($temper) { $simages = $temper; }
	if($URL{'v'} eq 'memberpanel') { return($temp); }
		else { $ebout .= $temp; }
}

sub BCLoad {
	my($temp);
	$temp = <<"EOT";
<tr>
 <td class="win2">
  <table cellpadding="4" cellspacing="0" width="100%">
   <tr><span onMouseOut="funclu('wait')">
    <td align="right" width="30%" valign="top"><b>$posttxt[32]:</b></td>
    <td width="70%">
     <table cellpadding="0" cellspacing="0">
      <tr>
       <td>
        <table cellpadding="0" cellspacing="0">
         <tr>
         <td><img src="$images/bold.gif" border="0" alt="$var{'10'}" onClick="use('[b]','[/b]');" onMouseOver="funclu('b');$ghand">
         <img src="$images/italics.gif" border="0" alt="$var{'11'}" onClick="use('[i]','[/i]');" onMouseOver="funclu('i');$ghand">
         <img src="$images/underline.gif" border="0" alt="$var{'12'}" onClick="use('[u]','[/u]');" onMouseOver="funclu('u');$ghand">
         <img src="$images/strike.gif" border="0" alt="$var{'17'}" onClick="use('[s]','[/s]');" onMouseOver="funclu('s');$ghand"></td>
         <td>&nbsp;<span onMouseOver="funclu('face')"><select style="width:75px;" name="face" onChange="AddNewValue('face',this.value);"><option value="">$var{'8'}</option><option value="Arial">Arial</option><option value="Times">Times</option><option value="Courier">Courier</option><option value="Geneva">Geneva</option><option value="Sans-Serif">Sans-Serif</option><option value="Verdana">Verdana</option></select></span>
         <span onMouseOver="funclu('size')"><select style="width:75px;" name="size" onChange="AddNewValue('size',this.value);"><option value="">$var{'9'}</option><option value="1">$var{'94'}</option><option value="8">$var{'95'}</option><option value="14">$var{'96'}</option></select></td>
         <td>&nbsp;<img src="$images/center.gif" border="0" alt="$var{'14'}" onClick="use('[center]','[/center]');" onMouseOver="funclu('center');$ghand">
         <img src="$images/right.gif" border="0" alt="$var{'15'}" onClick="use('[right]','[/right]');" onMouseOver="funclu('right');$ghand">
         <img src="$images/justify.gif" border="0" alt="$var{'16'}" onClick="use('[justify]','[/justify]');" onMouseOver="funclu('justify');$ghand">
         <img src="$images/list.gif" border="0" alt="$var{'18'}" onClick="use('[list]\\n[*]','\\n[/list]');" onMouseOver="funclu('list');$ghand"></td>
         </tr>
        </table>
       </td>
      </tr><tr>
       <td>
        <table cellpadding="0" cellspacing="0">
         <tr>
         <td>
         <img src="$images/sub.gif" border="0" alt="$var{'19'}" onClick="use('[sub]','[/sub]');" onMouseOver="funclu('sub');$ghand">
         <img src="$images/sup.gif" border="0" alt="$var{'20'}" onClick="use('[sup]','[/sup]');" onMouseOver="funclu('sup');$ghand">
         <img src="$images/hr.gif" border="0" alt="$var{'25'}" onClick="use('[hr]');" onMouseOver="funclu('hr');$ghand">
         <img src="$images/code.gif" border="0" alt="$var{'87'}" onClick="use('[code]','[/code]');" onMouseOver="funclu('code');$ghand">
         <img src="$images/url.gif" border="0" alt="$var{'21'}" onClick="use('[url]','[/url]');" onMouseOver="funclu('url');$ghand">
         <img src="$images/email_click.gif" border="0" alt="$var{'22'}" onClick="use('[mail]','[/mail]');" onMouseOver="funclu('mail');$ghand">
         <img src="$images/img.gif" border="0" alt="$var{'23'}" onClick="use('[img]','[/img]');" onMouseOver="funclu('img');$ghand">
         <img src="$images/quote_click.gif" border="0" alt="$var{'24'}" onClick="use('[quote]','[/quote]');" onMouseOver="funclu('quote');$ghand">
         <img src="$images/table.gif" border="0" alt="$var{'27'}" onClick="use('[table]\\n','[/table]');" onMouseOver="funclu('table');$ghand">
         <img src="$images/tr.gif" border="0" alt="$var{'28'}" onClick="use('[tr]\\n','[/tr]');" onMouseOver="funclu('tr');$ghand">
         <img src="$images/td.gif" border="0" alt="$var{'29'}" onClick="use('[td]\\n','[/td]');" onMouseOver="funclu('td');$ghand">
         </td><td>&nbsp;<span onMouseOver="funclu('color')"><select style="width:75px;" name="color" onChange="AddNewValue('color',this.value);"><option value="">$var{'30'}</option><option value="green" style="color:green">$var{'39'}</option><option value="blue" style="color:blue">$var{'37'}</option><option value="purple" style="color:purple">$var{'34'}</option><option value="orange" style="color:orange;">$var{'41'}</option><option value="yellow" style="color: yellow">$var{'40'}</option><option value="red" style="color:red">$var{'42'}</option></span><option value="black" style="color: black">$var{'33'}</option></select></span></td>
         </tr>
        </table>
       </td>
      </tr><tr><td><span class="smalltext"><span id="about" onLoad="funclu('wait')">$posttxt[111]</span></span></td></tr>
     </table>
    </td>
   </tr></span>
  </table>
 </td>
</tr>
EOT
	if($URL{'v'} eq 'memberpanel') { return($temp); }
		else { $ebout .= $temp; }
}

sub BCWait {
	my($temp);
	$ghand = "this.style.cursor='hand';";
	$temp = <<"EOT";
<script language="JavaScript" src="$bcloc" type="text/javascript"></script>
<script language="javascript1.2">
<!--
 var et = '$posttxt[82] <i>';
 var et2 = '$posttxt[83] <i>';
 var al = '$posttxt[84] <i>';
 var cr = '$posttxt[85] ... <i>';
 var c = ".</i>";
 var func_b = et+'$posttxt[86]'+c;
 var func_face = et2+'$posttxt[87]'+c;
 var func_size = et2+'$posttxt[88]'+c;
 var func_i = et+'$posttxt[89]'+c;
 var func_u = et+'$posttxt[90]'+c;
 var func_left = al+'$posttxt[91]'+c;
 var func_right = al+'$posttxt[92]'+c;
 var func_center = et+'$posttxt[93]'+c;
 var func_pre = et+'$posttxt[94]'+c;
 var func_justify = et+'$posttxt[94]'+c;
 var func_s = et+'$posttxt[95]'+c;
 var func_list = cr+'$posttxt[96]'+c;
 var func_code = cr+'$posttxt[97]'+c;
 var func_sub = et+'$posttxt[98]'+c;
 var func_sup = et+'$posttxt[99]'+c;
 var func_url = cr+'$posttxt[100]'+c;
 var func_mail = cr+'$posttxt[101]'+c;
 var func_img = cr+'$posttxt[102]'+c;
 var func_quote = cr+'$posttxt[103]'+c;
 var func_hr = cr+'$posttxt[104]'+c;
 var func_flash = cr+'$posttxt[105]'+c;
 var func_upload = cr+'$posttxt[106]'+c;
 var func_table = cr+'$posttxt[107]'+c;
 var func_tr = cr+'$posttxt[108]'+c;
 var func_td = cr+'$posttxt[109]'+c;
 var func_color = et2+'$posttxt[110]'+c;
 var func_icon = '$posttxt[112]';
 var func_subject = '$posttxt[113]';
 var func_message = '$posttxt[114]';
 var func_smiley = '$posttxt[115]';
 var func_shadow = et+'$posttxt[131]'+c;
 var func_glow = et+'$posttxt[132]'+c;
 var func_wait = "$posttxt[111]";
-->
</script>
EOT
	if($URL{'v'} eq 'memberpanel') { return($temp); }
		else { $ebout .= $temp; }
}
1;