################################################
# E-Blah Bulliten Board Systems       Platinum #
################################################
# Copyright (c) 2001 - 2004 e-blah!            #
# All Rights Reserved.                         #
################################################

&is_admin;
CoreLoad('ManageBoards',1);

sub ManageBoards {
	foreach(@catbase) {
		($cattitle,$catid,$t,$input,$desc,$subcats) = split(/\|/,$_);
		$catname{$catid} = "$cattitle|$input|$desc|$subcats";
		foreach $nohere (split(/\//,$subcats)) { $noshow{$nohere} = 1; }
	}
	if($URL{'a'} eq 'cats') { &EditCatsR; }
		else {
			if($URL{'g'} eq '2') { &EditBoards2; }
			elsif($URL{'g'} eq '3') { &EditBoards3; }
			if($URL{'p'} eq 'move') { &MoveBoards; }
			if($URL{'p'} eq 'moveboard') { &MoveToCat; }
			&StartBoards;
		}
	exit;
}

sub StartBoards {
	$title = $manageboards[1];
	headerA();

	$ebout .= <<"EOT";
<script language="JavaScript">
<!-- //
function Removebrd(cateid) {
 if(window.confirm("$managecats[18]")) { location = "$scripturl,v=admin,a=cats,p=2,id="+cateid+",remove=remove"; }
}
// -->
</script>
<table cellpadding="5" cellspacing="1" width="650" align="center" class="border">
 <tr>
  <td class="titlebg"><b><img src="$images/cat.gif"> $title</b></td>
 </tr><tr>
  <td class="win" colspan="2"><span class="smalltext">$manageboards[51]</span></td>
 </tr><tr>
  <td class="win2">
   <table width="100%" cellpadding="5" cellspacing="0">
EOT
	foreach(@catbase) {
		($t,$nohere) = split(/\|/,$_);
		if(!$noshow{$nohere}) { push(@catlist,$nohere); }
	}

	foreach(@boardbase) {
		($bid,$bdisc[0],$bdisc[1],$bdisc[2],$bdisc[3],$bdisc[4],$bdisc[5],$bdisc[6]) = split("/",$_);
		$boardname{$bid} = "$bdisc[0]/$bdisc[1]/$bdisc[2]/$bdisc[3]/$bdisc[4]/$bdisc[5]/$bdisc[6]";
	}

	$totallinks = @catlist;
	$counter = 0;
	foreach(@catlist) { ++$counter; SubCat($_,'',$totallinks,$counter); }

	$ebout .= <<"EOT";
    <tr>
     <td class="catbg" colspan="3" align="center"><b>$subcat<a href="$surl,v=admin,a=cats,n=1">$managecats[3]</a></b></td>
    </tr>
   </table>
  </td>
 </tr>
</table>
EOT

	footerA();
	exit;
}

sub SubCat { # This puts together all sub cats ...
	my($catid,$subcat,$total,$curcount) = @_;
	my($curcounter);
	if($subcat eq '') { $depth = ''; }

	if($catname{$catid} eq '') { return(-1); }

	if($alreadyshown{$catid}) { return(-1); }
	$alreadyshown{$catid} = 1;

	($cattitle,$input,$desc,$subcats) = split(/\|/,$catname{$catid});

	@forumlist = split(/\//,$input);
	$totalforums{$catid} = @forumlist;
	@subcats = split(/\//,$subcats);
	$totalscats{$catid} = @subcats;

	$updown = '';
	if($total > 1) {
		$down = qq~<a href="$surl,v=admin,a=cats,id=$catid,p=move,s=down">$manageboards[2]</a>~;
		$up = '';
		if($curcount != 1) { $up = qq~<a href="$surl,v=admin,a=cats,id=$catid,p=move,s=up">$manageboards[3]</a> | ~; }
		if($total == $curcount) { $down = ''; $up =~ s/ \| //gi; }
		if($total > 0) { $updown = qq~ <span class="smalltext">$up$down</span>~; }
		if($input ne '') { @randoms = split("/",$input); }
		$incnt = $input ne '' ? @randoms : 0;
	}
	if(@catbase > 1) { $updown .= qq~ : <a href="$scripturl,v=admin,a=boards,p=moveboard,cid=$catid">$manageboards[48]</a>~; }
	$updown =~ s/^ \: //g;

	$ebout .= <<"EOT";
<tr>
 <td class="catbg"><span class="smalltext"><b>$subcat<img src="$images/cat_sm.gif"> <a href="$surl,v=admin,a=cats,id=$catid">$cattitle</a></b></span></td>
 <td class="catbg" align="center"><span class="smalltext"><b>$totalforums{$catid}</b> $manageboards[61], <b>$totalscats{$catid}</b> $managecats[27]</span></td>
 <td class="catbg" align="right"><span class="smalltext">$updown | <a href="javascript:Removebrd('$catid');">$managecats[2]</a></span></td>
</tr><tr>
 <td class="win">$depth&nbsp;<img src="$images/subdown.gif" width="10">&nbsp;<img src="$images/board_sm.gif"> <a href="$surl,v=admin,a=boards,g=2,c=$catid,n=1">$manageboards[11]</a></td>
 <td class="win" colspan="2" align="right"><a href="$surl,v=admin,a=cats,n=1,l=$catid">$managecats[29]</a></td>
</tr>
EOT

	foreach(@forumlist) {
		if($_ eq '') { next; }
		SubForums($_,$catid,$depth,$totalforums{$catid},$curcounter); ++$curcounter;
	}
	$curcounter = 0;
	foreach(split(/\//,$subcats)) {
		$depth = qq~<img src="$images/nopic.gif" width="10">~.$subcat;
		++$curcounter; SubCat($_,$depth,$totalscats{$catid},$curcounter);
	}
}

sub SubForums { # This puts together all sub forums ...
	my($boardid,$catid,$depth,$max,$counter) = @_;
	($bdisc[0],$bdisc[1],$bdisc[2],$bdisc[3],$bdisc[4],$bdisc[5],$bdisc[6]) = split("/",$boardname{$boardid});

	$bmoveup = '';
	if($counter >= 0) { $bmovedown = qq~<a href="$scripturl,v=admin,a=boards,p=move,s=down,id=$boardid">$manageboards[2]</a>~; }
	if($counter > 0) { $bmoveup = qq~<a href="$scripturl,v=admin,a=boards,p=move,s=up,id=$boardid">$manageboards[3]</a> | ~; }
	if($counter == $max-1) { $bmovedown = ''; $bmoveup =~ s/ \| \Z//gsi; }
	if($bmovedown ne '' || $bmoveup ne '') { $bmovedown .= " : "; }
	if(@catbase > 1) { $moveto = qq~<a href="$scripturl,v=admin,a=boards,p=moveboard,id=$boardid">$manageboards[48]</a> | ~; }
	if($bmoveup || $bmovedown || $moveto) { $moveops = qq~ $bmoveup$bmovedown$moveto~; }

	$ebout .= <<"EOT";
<tr>
 <td>$depth&nbsp;<img src="$images/subdown.gif" width="10">&nbsp;<img src="$images/board_sm.gif"> <a href="$surl,v=admin,a=boards,g=2,c=$catid,bd=$boardid">$bdisc[2]</a></td>
 <td align="right" colspan="2"><span class="smalltext">$moveops<a href="$scripturl,v=admin,a=boards,g=3,c=$catid,bd=$boardid,remove=1">$managecats[2]</a></span></td>
</tr>
EOT
}

sub EditBoards2 {
	$catid = $URL{'c'};
	$board = $URL{'bd'};
	if($URL{'n'} != 1) {
		foreach (@catbase) {
			($t,$t,$t,$input) = split(/\|/,$_);
			if($input ne '') { @randoms = split("/",$input); } else { next; }
			foreach(@randoms) {
				if($_ eq $board) { $fnd = 1; last; }
			}
		}
		if($fnd != 1) { &error("$gtxt{'error2'}: $board"); }
		foreach(@boardbase) {
			($bdid,$bdisc[0],$bdisc[1],$bdisc[2],$bdisc[3],$bdisc[4],$bdisc[5],$bdisc[6],$bdisc[7],$bdisc[8],$bdisc[9],$bdisc[10],$bdisc[11],$bdisc[12]) = split("/",$_);
			if($bdid eq $board) { last; }
		}
		@mod = split(/\|/,$bdisc[1]);
		foreach (@mod) {
			$mods .= "$_\n";
		}
		$mods =~ s/,\Z//;
		$boardid = "$board";
		$remove = qq~&nbsp;&nbsp;<input type="submit" class="button" name="remove" value=" $manageboards[10] ">~;
	} else { $boardid = qq~<input type="text" class="textinput" name="bid">~; }
	$S{$bdisc[3]}     = " selected";
	$S2{$bdisc[4]}    = " selected";
	$P{$bdisc[5]}     = " checked";
	$E{$bdisc[7]}     = " checked";
	$PC{$bdisc[8]}    = " checked";
	$V{$bdisc[10]}    = ' checked';
	$ATTS{$bdisc[11]} = ' checked';
	$desc = Unformat($bdisc[0]);
	if($URL{'n'}) { $title = $manageboards[11]; }
		else { $title = "$manageboards[12]: $bdisc[2]"; }

	@groups = split(",",$bdisc[9]);
	foreach(@groups) {
		$a{$_} = " selected";
	}
	$count = 0;
	$membergroups = qq~<select name="memgrp" size="4" multiple>~;
	foreach(@memgrps) {
		if($count > 0 && $count < 7) { ++$count; next; }
		$membergroups .= qq~<option$a{$_}>$_</option>~;
		++$count;
	}
	$membergroups .= qq~<option></option><option value='member'$a{'member'}>$manageboards[62]</option></select>~;
	&headerA;
	$ebout .= <<"EOT";
<table cellpadding="5" cellspacing="1" class="border" width="700" align="center">
 <tr><form action="$scripturl,v=admin,a=boards,g=3,c=$URL{'c'},bd=$URL{'bd'},n=$URL{'n'}" method="post" name="save" enctype="multipart/form-data">
  <td class="titlebg"><b><img src="$images/cat.gif"> $title</b></td>
 </tr><tr>
  <td class="win"><span class="smalltext">$manageboards[54]</span></td>
 </tr><tr>
  <td class="catbg" align="center"><span class="smalltext"><b>$manageboards[52]</b></span></td>
 </tr><tr>
  <td class="win2">
   <table cellpadding="5" cellspacing="0" width="100%">
    <tr>
     <td align="right" width="50%"><b>$manageboards[13]:</b></td>
     <td width="50%" valign="top"><input type="text" class="textinput" name="bname" value="$bdisc[2]" size="25"></td>
    </tr><tr>
     <td align="right" width="50%"><b>$manageboards[14]:</b></td>
     <td width="50%" valign="top">$boardid</td>
    </tr><tr>
     <td align="right" width="50%" valign="top"><b>$manageboards[15]:</b></td>
     <td width="50%" valign="top"><textarea name="bdisc" rows=3 cols=50 wrap=virtual>$desc</textarea></td>
    </tr><tr>
     <td align="right" width="50%" valign="top"><b>$manageboards[16]:</b><br><span class="smalltext">$manageboards[17]</span></td>
     <td width="50%" valign="top"><textarea name="mods" rows="5" cols="35" wrap="virtual">$mods</textarea></td>
    </tr>
   </table>
  </td>
 </tr><tr>
  <td class="catbg" align="center"><span class="smalltext"><b>$manageboards[53]</b></span></td>
 </tr><tr>
  <td class="win">
   <table cellpadding="5" cellspacing="0" width="100%">
    <tr>
     <td align="right" width="50%"><b>$var{'72'}:</b></td>
     <td width="50%" valign="top"><select name="start">
      <option value="0"$S{'0'}>$manageboards[18]</option>
      <option value="1"$S{'1'}>$manageboards[19]</option>
      <option value="4"$S{'4'}>$manageboards[50]</option>
      <option value="2"$S{'2'}>$var{'84'}</option>
      <option value="3"$S{'3'}>$manageboards[20]</option>
     </select></td>
    </tr><tr>
     <td align="right" width="50%"><b>$var{'73'}:</b></td>
     <td width="50%" valign="top"><select name="reply">
      <option value="0"$S2{'0'}>$manageboards[18]</option>
      <option value="1"$S2{'1'}>$manageboards[19]</option>
      <option value="4"$S2{'4'}>$manageboards[50]</option>
      <option value="2"$S2{'2'}>$var{'84'}</option>
      <option value="3"$S2{'3'}>$manageboards[20]</option>
     </select></td>
    </tr><tr>
     <td align="right" width="50%" valign="top"><b>$manageboards[65]:</b><br>$manageboards[66]<br>$managecats[7]</td>
     <td width="50%">$membergroups</td>
    </tr><tr>
     <td align="right" width="50%"><b>$manageboards[24]:</b></td>
     <td width="50%" valign="top"><input type="password"  class="textinput" name="password" value="$bdisc[6]" size="15"></td>
    </tr>
   </table>
  </td>
 </tr><tr>
  <td class="catbg" align="center"><span class="smalltext"><b>$managecats[20]</b></span></td>
 </tr><tr>
  <td class="win">
   <table cellpadding="5" cellspacing="0" width="100%">
    <tr>
     <td align="right" width="50%"><b>$manageboards[25]:</b></td>
     <td width="50%" valign="top"><input type="checkbox" class="checkboxinput" name="polls" value="1"$P{'1'}></td>
    </tr><tr>
     <td align="right" width="50%"><b>$manageboards[63]:</b><br><span class="smalltext">$manageboards[64]</span></td>
     <td width="50%" valign="top"><input type="checkbox" class="checkboxinput" name="pcnt" value="1"$PC{'1'}></td>
    </tr><tr>
     <td align="right" width="50%"><b>$manageboards[26]</b><br><span class="smalltext">$manageboards[27]</span></td>
     <td width="50%" valign="top"><input type="checkbox" class="checkboxinput" name="email" value="1"$E{'1'}></td>
    </tr><tr>
     <td align="right" width="50%"><b>$managecats[21]</b></td>
     <td width="50%" valign="top"><input type="checkbox" class="checkboxinput" name="voting" value="1"$V{'1'}></td>
    </tr><tr>
     <td align="right" width="50%"><b>$managecats[22]</b><br><span class="smalltext">$managecats[23]</span></td>
     <td width="50%" valign="top"><input type="checkbox" class="checkboxinput" name="atts" value="1"$ATTS{'1'}></td>
    </tr><tr>
     <td align="right" width="50%"><b>$managecats[24]:</b><br><span class="smalltext">$managecats[25]</span></td>
     <td width="50%" valign="top"><input type="text" class="textinput" name="redirurl" value="$bdisc[12]" size="40"></td>
    </tr>
   </table>
  </td>
 </tr><tr>
  <td class="win2" align="center"><input type="submit" class="button" name="submit" value=" $manageboards[28] ">$remove</td>
 </form></tr>
</table>
EOT
	&footerA;
	exit;
}

sub EditBoards3 {
	$catid = $URL{'c'};
	$board = $URL{'bd'};
	if($URL{'n'} != 1) {
		$fnd = 1;
		foreach (@catbase) {
			($t,$t,$t,$input) = split(/\|/,$_);
			if($input ne '') { @randoms = split("/",$input); } else { next; }
			foreach(@randoms) {
				if($_ eq $board) { $fnd = 1; last; }
			}
		}
		if($fnd != 1) { &error("$gtxt{'error2'}: $board"); }
		foreach(@boardbase) {
			($bdid,$t,$t,$t,$t,$t,$t,$oldpass) = split("/",$_);
			if($bdid eq $board) { $fnd = 0; last; }
		}
		if($fnd) { &error("$gtxt{'error2'}: $board"); }
	} else {
		$idbud = 0;
		if($FORM{'bid'} eq 'AllBoards' || $FORM{'bid'} =~ s/AllRead// || $FORM{'bid'} =~ s/Cat// || $FORM{'bid'} eq 'All') { &error("$gtxt{'bfield'} :: \"AllBoards\", \"AllRead\", \"All\", and \"Cat\""); }
		$FORM{'bid'} =~ s/[#%+,\\\/:?"<>'| @^\$\&~'\)\(\]\[\;{}!`=-]//g;
		if($FORM{'bid'} eq '') { &error($gtxt{'bfield'}); }
		foreach(@boardbase) {
			($bdid) = split("/",$_);
			if(lc($bdid) eq lc($FORM{'bid'})) { $idbud = 1; last; }
		}
		if($idbud) { &error($manageboards[29]); } else { $board = $FORM{'bid'}; }
		fopen(FILE,">$boards/$FORM{'bid'}.msg");
		fclose(FILE);
		fopen(FILE,">$boards/$board.hits");
		fclose(FILE);
		fopen(FILE,">$boards/$FORM{'bid'}.ino");
		fclose(FILE);
	}

	if(!-e("$boards/$board.hits") && $FORM{'bid'} == '') {
		fopen(FILE,">$boards/$board.hits");
		fclose(FILE);
	}

	if($FORM{'remove'} || $URL{'d'} || $URL{'remove'} == 1) { &DeleteBoards; }
		else {
			&error($gtxt{'bfield'}) if($FORM{'bdisc'} eq '');
			&error($gtxt{'bfield'}) if($FORM{'bname'} eq '');
		}

	$desc = Format($FORM{'bdisc'});
	$desc =~ s/\//&#47;/g;
	$bname = Format($FORM{'bname'});
	$bname =~ s/\//&#47;/g;
	if($FORM{'password'} ne $oldpass) {
		if($FORM{'password'} =~ s/[#|%^\\{}\/?~'\)\(\]\[\;]//) { &error($manageboards[30]); }
		$password = Format($FORM{'password'});
		if($password ne '') {
			$password = crypt($password,$pwcry);
			$password =~ s/\///g;
		}
	} else { $password = $oldpass; }

	if($FORM{'mods'} ne '') {
		fopen(FILE,"$members/List.txt");
		@members = <FILE>;
		fclose(FILE);
		chomp @members;
		@mods = split(/\n/,$FORM{'mods'});

		foreach(@mods) {
			$_ =~ s/ //gi;
			$_ =~ s/\cM//g;
			if($_ eq '') { next; }
			loaduser($_);
			if($userset{$_}->[1] eq '') {
				foreach $findme (@members) {
					loaduser($findme);
					$userset{$findme}->[1] =~ s/ //gi;
					if(lc($userset{"$findme"}->[1]) eq lc($_)) {
						push(@modz,$findme);
						$found = 1;
						last;
					}
				}
				if($found != 1) { error("$_ $manageboards[31]"); }
			} else { push(@modz,$_); }
		}
		foreach(@modz) { $modss .= "$_|"; }
	}

	$polls = $FORM{'polls'} || 0;
	$redirurl = $FORM{'redirurl'};
	$redirurl =~ s/\//&#47;/g;
	fopen(FILE,"+>$boards/bdindex.db");
	foreach (@boardbase) {
		($usethis) = split("/",$_);
		if($usethis ne $board) { print FILE "$_\n"; }
	}

	$memgrp = Format($FORM{'memgrp'});
	print FILE "$board/$desc/$modss/$bname/$FORM{'start'}/$FORM{'reply'}/$polls/$password/$FORM{'email'}/$FORM{'pcnt'}/$memgrp/$FORM{'voting'}/$FORM{'atts'}/$redirurl\n";
	fclose(FILE);

	if($URL{'n'}) {
		fopen(FILE,"+>$boards/bdscats.db");
		foreach(@catbase) {
			($ll,$cid,$aa,$bds,$cdat1,$desc,$subcats) = split(/\|/,$_);
			if($cid eq $catid) {
				$bds =~ s/\/\Z//g;
				if($bds eq '') { $bds = $board; } else { $bds .= "/$board"; }
				print FILE "$ll|$cid|$aa|$bds|$cdat1|$desc|$subcats\n";
			} else { print FILE "$_\n"; }
		}
		fclose(FILE);
	}
	redirect("$surl,v=admin,a=boards");
}

sub DeleteBoards {
	if(($FORM{'remove'} || $URL{'remove'}) && $URL{'d'} eq '') {
		foreach(@boardbase) {
			($bdid,$bdisc[0],$bdisc[1],$bdisc[2],$bdisc[3],$bdisc[4],$bdisc[5],$bdisc[6],$bdisc[7]) = split("/",$_);
			if($bdid eq $board) { last; }
		}
		$title = $manageboards[32];
		&headerA;
		$ebout .= <<"EOT";
<table cellpadding="5" cellspacing="1" class="border" align="center" width="600">
 <tr>
  <td class="titlebg"><b><img src="$images/ban.gif"> $title</b></td>
 </tr><tr>
  <td class="win"><span class="smalltext">$manageboards[55] "$bdisc[2]".</span></td>
 </tr><tr>
  <td class="win2">
   <table width="100%" cellpadding="5" cellspacing="0">
    <tr>
     <td class="catbg"><b><a href="$surl,v=admin,a=boards,g=3,c=$URL{'c'},bd=$URL{'bd'},d=1,m=1">$manageboards[56]</a></b></td>
    </tr><tr>
     <td><span class="smalltext"><br>&nbsp; $manageboards[57]<br><br></span></td>
    </tr><tr>
     <td class="catbg"><b><a href="$surl,v=admin,a=boards,g=3,c=$URL{'c'},bd=$URL{'bd'},d=1">$manageboards[58]</a></b></td>
    </tr><tr>
     <td><span class="smalltext"><br>&nbsp; $manageboards[59]<br><br></span></td>
    </tr><tr>
     <td class="catbg"><b><a href="javascript:history.back(-1)">$manageboards[60]</a></b></td>
    </tr><tr>
     <td><span class="smalltext"><br>&nbsp; $managecats[30]<br><br></span></td>
    </tr>
   </table>
  </td>
 </tr>
</table>
EOT
		&footerA;
		exit;
	} else {
		fopen(FILE,"$boards/$URL{'bd'}.msg");
		@messages = <FILE>;
		fclose(FILE);
		chomp @messages;
		$title = $manageboards[32];
		&headerA;
		$ebout .= <<"EOT";
<meta http-equiv="refresh" content="2;url=$scripturl,v=admin,a=boards">
<table cellpadding="4" cellspacing="1" class="border" align="center" width="500">
 <tr>
  <td class="titlebg"><b><img src="$images/ban.gif"> $title</b></td>
 </tr><tr>
  <td class="win"><span class="smalltext">
EOT
		$count = 0;
		$replycnt = 0;
		foreach (@messages) {
			($mid,$t,$t,$t,$replys) = split(/\|/,$_);
			unlink("$messages/$mid.txt","$messages/$mid.rate","$messages/$mid.view","$messages/$mid.mail","$messages/$mid.poll","$messages/$mid.polled");
			$ebout .= "$manageboards[38]: <b>$mid</b><br>\n";
			++$count;
			$replycnt = $replycnt+$replys;
		}
		unlink("$boards/$URL{'bd'}.msg","$boards/$URL{'bd'}.hits","$boards/$URL{'bd'}.ino","$boards/$URL{'bd'}.mail");
		if($URL{'m'}) {
			fopen(FILE,">$boards/$URL{'bd'}.msg");
			print FILE "";
			fclose(FILE);
			fopen(FILE,">$boards/$URL{'bd'}.ino");
			print FILE "";
			fclose(FILE);
		}
		$ebout .= "$manageboards[38]: <b>$URL{'bd'}.msg, $URL{'bd'}.ino, $URL{'bd'}.mail</b><br>";
		if(!$URL{'m'}) {
			$gottcha = 1;
			fopen(FILE,"+>$boards/bdscats.db");
			foreach(@catbase) {
				($cname,$cid,$cmemgrps,$input,$desc,$subcats) = split(/\|/,$_);
				@ball = split("/",$input);
				$pbdata = "$cname|$cid|$cmemgrps|";
				foreach $useme (@ball) {
					if($useme ne $URL{'bd'}) { $pbdata .= "$useme/"; }
						else { $gottcha = 0; }
				}
				$pbdata =~ s/\/\Z//gsi;
				print FILE "$pbdata|$desc|$subcats\n";
			}
			fclose(FILE);
			fopen(FILE,"+>$boards/bdindex.db");
			foreach (@boardbase) {
				($boardid) = split("/",$_);
				if($boardid ne $URL{'bd'}) { print FILE "$_\n"; }
			}
			fclose(FILE);
			if($gottcha) { $thereserror = qq~<br> - $error{'error2'} :: $URL{'bd'}</font>~; }
			$ebout .= "$manageboards[38]: <b>$URL{'bd'} in <i>bdscats.db</i>$thereserror</b><br>";
		}
		$total = $replycnt+$count;
		$ebout .= <<"EOT";
  </span></td>
 </tr><tr>
  <td class="win2">$manageboards[38]: <b>$count</b><br>$manageboards[38]: <b>$replycnt</b><br>$manageboards[38]: <b>$total</b></td>
 </tr>
</table>
EOT
		&footerA;
		exit;
	}
}

sub MoveBoards {
	foreach (@catbase) {
		($t,$t,$t,$input) = split(/\|/,$_);
		@output = split("/",$input);
		foreach $add (@output) {
			foreach $test (@boardbase) {
				($uid) = split("/",$test);
				if($uid eq $add) { push(@baseboard,"$test\n"); }
			}
		}
	}
	@boardbase = @baseboard;
	for($i = 0; $i < @boardbase; $i++) {
		($id) = split("/",$boardbase[$i]);
		if($id eq $URL{'id'}) { $move = $i; last; }
	}
	if($URL{'s'} eq 'up') {
		$counter = 0;
		for($e = 0; $e < @boardbase; $e++) {
			if($e == $move) {
				if($move == 0) { $add[$e] = $boardbase[$e]; } else {
					$add[--$e] = $boardbase[$move]; $add[++$e] = $boardbase[--$move];
				}
			}
				else { $add[$e] = $boardbase[$e]; }
		}
	} elsif($URL{'s'} eq 'down') {
		$max = @boardbase-1;
		$counter = 0;
		for($e = 0; $e < @boardbase; $e++) {
			if($e == $move) {
				if($move == $max) { $add[$e] = $boardbase[$e]; } else {
					$add[$e] = $boardbase[++$move]; $add[++$e] = $boardbase[--$move];
				}
			}
				else { $add[$e] = $boardbase[$e]; }
		}
	}
	chomp @add;
	fopen(FILE,"+>$boards/bdindex.db");
	foreach(@add) { print FILE "$_\n"; }
	fclose(FILE);

	fopen(FILE,"+>$boards/bdscats.db");
	foreach $cats(@catbase) {
		($name,$other,$otherb,$oldinput,$desc,$subcats) = split(/\|/,$cats);
		$update = "$name|$other|$otherb|";
		@outputs = split("/",$oldinput);
			foreach $outs (@add) {
				($id) = split("/",$outs);
				foreach $argh (@outputs) {
					if($argh eq $id) { $update .= "$id/"; }
				}
			}
		print FILE "$update|$desc|$subcats\n";
	}
	fclose(FILE);

	redirect("$surl,v=admin,a=boards");
}

sub FindCrosslinks { # This finds all the cats that CANNOT be used when moving this cat!
	my($catid) = @_;

	if($catname{$catid} eq '') { return(-1); }

	if($alreadyshown{$catid}) { return(-1); }
	$alreadyshown{$catid} = 1;

	($t,$t,$t,$subcats) = split(/\|/,$catname{$catid});

	foreach(split(/\//,$subcats)) { FindCrosslinks($_); }
}

sub MoveToCat {
	$board = $URL{'id'} || '';
	$cat   = $URL{'cid'} || '';
	FindCrosslinks($cat);
	foreach(@catbase) {
		($name,$useid,$t,$input,$t,$subscat) = split(/\|/,$_);
		if($input ne '') { @randoms = split("/",$input); }
		elsif(!$alreadyshown{$useid}) { $selection .= qq~<option value="$useid">$name</option>\n~; next; }
		if($cat eq '') {
			foreach(@randoms) {
				if($_ eq $board) { $other = 1; $fnd = 1; }
			}
		} else {
			if($useid eq $cat) { $fnd = 1; $other = 1; }
			$addmore = qq~<option value=""></option><option value="">$managecats[28]</option>~;
		}
		if($other eq '' && !$alreadyshown{$useid}) { $selection .= qq~<option value="$useid">$name</option>\n~; }
		$other = '';
	}
	if($fnd != 1) { &error($manageboards[44]); }
	if($URL{'o'} == 2) { &MoveToCat2; }
	if(!$selection) { &error($manageboards[45]); }
	$title = $manageboards[46];
	&headerA;
	$ebout .= <<"EOT";
<table class="border" cellspacing="1" cellpadding="5" align="center" width="400">
 <tr><form action="$scripturl,v=admin,a=boards,p=moveboard,id=$URL{'id'},cid=$URL{'cid'},o=2" method="post" name="post">
  <td class="titlebg"><b>$title</b></td>
 </tr><tr>
  <td class="win2"><span class="smalltext">The following action will move this board or category to the specified category below.</span></td>
 </tr><tr>
  <td class="win"><b>$manageboards[47] ...</b><br><br><center><select name="cat" size="7">
$selection$addmore
  </select><br><br></td>
 </tr><tr>
  <td class="win2" align="center"><input type="submit" class="button" value=" $manageboards[48] " name="submit"></td>
 </form></tr>
</table>
EOT
	&footerA;
	exit;
}

sub MoveToCat2 {
	foreach(@catbase) {
		($name,$useid,$ts,$input,$desc,$subcats) = split(/\|/,$_);
		$pushed = "$name|$useid|$ts|";

		if($board ne '') {
			if($input ne '') { @randoms = split("/",$input); }
				else {
					if($FORM{'cat'} eq $useid) { $pushed .= "$board/"; }
					push(@catbase2,$pushed);
					next;
				}
			foreach(@randoms) {
				if($_ ne $board) { $pushed .= "$_/"; }
			}
			if($FORM{'cat'} eq $useid) { $pushed .= "$board/"; }
			$pushed =~ s/\/\Z//;
		} else {
			$resubcats = '';
			$pushed .= $input; # Do not change boards
			foreach $subcatz (split(/\//,$subcats)) {
				if($cat ne $subcatz) { $resubcats .= "$subcatz/"; }
			}
			if($FORM{'cat'} eq $useid && !$alreadyshown{$useid}) { $resubcats .= $cat; }
			$subcats = $resubcats;
			$subcats =~ s/\/\Z//g;
		}

		push(@catbase2,"$pushed|$desc|$subcats");
	}

	fopen(FILE,"+>$boards/bdscats.db");
	foreach(@catbase2) { print FILE "$_\n"; }
	fclose(FILE);

	redirect("$surl,v=admin,a=boards");
}

sub EditCatsR {
	if($URL{'remove'} eq 'remove' || $URL{'p'} == 3) { &DelCats; }
	if($URL{'p'} == 2) { &EditCats2; }
	if($URL{'p'} eq 'move') { &MoveCat; }
	if($URL{'n'} != 1) {
		$title = "$managecats[1]: ";
		foreach (@catbase) {
			($name,$id,$membergrp,$t,$catdesc) = split(/\|/,$_);
			if($id eq $URL{'id'}) {
				$title .= $name;
				last;
			}
		}
		if($URL{'n'} eq '') { $remove = qq~&nbsp;&nbsp;<input type="button" class="button" name="remove" value=" $managecats[2] " onClick="Removebrd();">~; }
		fopen(FILE,"$prefs/Ranks.txt");
		@ranks = <FILE>;
		fclose(FILE);
		chomp @ranks;
		$id = $URL{'id'};
	} else {
		$id = qq~<input type="text" class="textinput" name="id" size=4>~;
		$title = "$managecats[3]";
	}
	@groups = split(",",$membergrp);
	foreach(@groups) {
		$a{$_} = " selected";
	}
	$count = 0;
	$membergroups = qq~<select name="memgrp" size="4" multiple>~;
	foreach(@memgrps) {
		if($count > 0 && $count < 7) { ++$count; next; }
		$membergroups .= qq~<option$a{$_}>$_</option>~;
		++$count;
	}
	$membergroups .= qq~<option></option><option value='member'$a{'member'}>$manageboards[62]</option></select>~;
	&headerA;
	$ebout .= <<"EOT";
<script language="JavaScript">
<!-- //
function Removebrd() {
 if(window.confirm("$managecats[18]")) { location = "$scripturl,v=admin,a=cats,p=2,id=$URL{'id'},n=$URL{'n'},remove=remove"; }
}
// -->
</script>
<table cellpadding="5" cellspacing="1" class="border" width="650" align="center">
 <tr><form action="$scripturl,v=admin,a=cats,p=2,id=$URL{'id'},l=$URL{'l'},n=$URL{'n'}" method="post" name="post" name="save" enctype="multipart/form-data">
  <td class="titlebg"><b><img src="$images/cat.gif"> $title</b></td>
 </tr><tr>
  <td class="win"><span class="smalltext">$managecats[19]</span></td>
 </tr><tr>
  <td class="win2">
   <table cellpadding="5" cellspacing="0" width="100%">
    <tr>
     <td width="50%" align="right"><b>$managecats[4]:</b></td>
     <td width="50%" valign="top"><input type="text" class="textinput" size="25" name="name" value="$name"></td>
    </tr><tr>
     <td align="right" width="50%" valign="top"><b>$managecats[26]:</b></td>
     <td width="50%" valign="top"><textarea name="catdesc" rows=3 cols=50 wrap=virtual>$catdesc</textarea></td>
    </tr><tr>
     <td width="50%" align="right"><b>$managecats[5]:</b></td>
     <td width="50%" valign="top">$id</td>
    </tr><tr>
     <td width="50%" align="right" valign="top"><b>$managecats[6]:</b><br><span class="smalltext">$managecats[7]</span></td>
     <td width="50%" valign="top">$membergroups</td>
    </tr>
   </table>
  </td>
 </tr><tr>
  <td class="win" align="center"><input type="submit" class="button" name="submit" value=" $managecats[8] ">$remove</td>
 </form></tr>
</table>
EOT
	&footerA;
	exit;
}

sub EditCats2 {
	if($FORM{'name'} eq '') { &error($gtxt{'bfield'}); }
	$name = Format($FORM{'name'});
	$memgrp = Format($FORM{'memgrp'});
	$catdesc = Format($FORM{'catdesc'});
	$ids = Format($FORM{'id'});
	$ids =~ s/[#%+,\\\/:?"<>'| @^\$\&~'\)\(\]\[\;{}!`=-]//g;
	if($ids eq '' && $URL{'n'}) { &error($gtxt{'bfield'}); }

	foreach(@catbase) {
		($xname,$id,$xmemgrp,$bds,$xcatdesc,$subcats) = split(/\|/,$_);
		if($URL{'n'} && (lc($id) eq lc($FORM{'id'}))) { &error($managecats[17]); }
		if($URL{'n'} && $URL{'l'} eq $id) {
			$subcats =~ s/\/\Z//g;
			if($subcats) { $subcats .= "/$ids"; } else { $subcats = $ids; }
			$update .= "$xname|$id|$xmemgrp|$bds|$xcatdesc|$subcats\n";
		}
		elsif($id eq $URL{'id'}) { $update .= "$name|$id|$memgrp|$bds|$catdesc|$subcats\n"; } else { $update .= "$_\n"; }
	}
	if($URL{'n'}) { $update .= "$name|$ids|$memgrp||$catdesc|\n"; }

	fopen(FILE,"+>$boards/bdscats.db");
	print FILE $update;
	fclose(FILE);

	redirect("$surl,v=admin,a=boards");
}

sub DelCats {
	foreach(@catbase) {
		($name,$id,$t,$input) = split(/\|/,$_);
		if($id eq $URL{'id'}) { $delete = $name; @delete = split("/",$input); last; }
	}
	if($delete eq '') { &error("$gtxt{'error2'}: $delete"); }

	$title = $managecats[9];
	&headerA;
	$count = 0;
	$ebout .= <<"EOT";
<meta http-equiv="refresh" content="2;url=$scripturl,v=admin,a=boards">
<table cellpadding="4" cellspacing="1" class="border" align="center" width="500">
 <tr>
  <td class="titlebg"><b><img src="$images/ban.gif"> $title</b></td>
 </tr><tr>
  <td class="win"><span class="smalltext">
EOT
	foreach (@delete) {
		fopen(FILE,"$boards/$_.msg");
		@messages = <FILE>;
		fclose(FILE);
		fopen(FILE,"$boards/bdindex.db");
		@boardbase = <FILE>;
		fclose(FILE);
		chomp @boardbase;
		chomp @messages;
		foreach (@messages) {
			($mid) = split(/\|/,$_);
			unlink("$messages/$mid.txt","$messages/$mid.rate","$messages/$mid.view","$messages/$mid.mail","$messages/$mid.poll","$messages/$mid.polled");
			$ebout .= "$managecats[12]: <b>$mid</b><br>\n";
			++$count;
		}
		unlink("$boards/$_.msg","$boards/$_.hits","$boards/$_.ino","$boards/$_.mail");
		fopen(FILE,"+>$boards/bdindex.db");
		foreach $fix (@boardbase) {
			($boardid) = split("/",$fix);
			if($boardid ne $_) { print FILE "$fix\n"; }
		}
		fclose(FILE);
		$ebout .= "<b>$managecats[12]: $_.msg, $_.ino, $_.mail</b><br>";
		$ebout .= qq~<hr size="1" width="100%">~;
	}
	$ebout .= "$managecats[12]: <b>$id.cg</b><br>";
	fopen(FILE,"+>$boards/bdscats.db");
	foreach(@catbase) {
		($name,$id,$tg1,$tg2,$tg3,$subcats) = split(/\|/,$_);
		if($subcats) {
			$subcatz = '';
			foreach $subcat (split(/\//,$subcats)) {
				if($URL{'id'} ne $subcat) { $subcatz .= $subcat."/"; }
			}
			$subcatz =~ s/\/\Z//g;
			print FILE "$name|$id|$tg1|$tg2|$tg3|$subcatz\n";
		}
		elsif($id ne $URL{'id'}) { print FILE "$_\n"; }
	}
	$ebout .= "$managecats[12]: <b>$id $managecats[16] <i>bdscats.db</i></b><br>";
	fclose(FILE);
	$ebout .= <<"EOT";
 <tr>
  <td class="win2">$managecats[12]: <b>$count messages</b></td>
 </tr>
</table>
EOT
	&footerA;
	exit;
}

sub MoveCat {
	foreach(@catbase) {
		($t,$id,$t,$t,$t,$subcats) = split(/\|/,$_);
		foreach $nohere (split(/\//,$subcats)) {
			if($URL{'id'} eq $nohere) {
				@subcats = split(/\//,$subcats);
				$subcats{$id} = 1;
				$noshow{$nohere} = 1;
				last;
			}
		}
	}

	$readd = '';
	$count = 0;
	if($noshow{$URL{'id'}} && $URL{'s'} eq 'up') {
		foreach(@catbase) {
			($cattitle,$catid,$temp1,$input,$desc,$subcats) = split(/\|/,$_);
			if($subcats{$catid}) {
				foreach(@subcats) {
					if($URL{'id'} eq $_) { $movecnt = $count; }
					++$count; last;
				}

				for($e = 0; $e < @subcats; $e++) {
					if($e == $movecnt) {
						if($movecnt == 0) { $add[$e] = $subcats[$e]; } else { $add[--$e] = $subcats[$movecnt]; $add[++$e] = $subcats[--$movecnt]; }
					} else { $add[$e] = $subcats[$e]; }
				}
				foreach(@add) { $add .= "$_/"; }
				$add =~ s/\/\Z//g;
				$readd .= "$cattitle|$catid|$temp1|$input|$desc|$add\n";
			} else { $readd .= "$_\n"; }
		}
	} elsif($noshow{$URL{'id'}} && $URL{'s'} eq 'down') {
		foreach(@catbase) {
			($cattitle,$catid,$temp1,$input,$desc,$subcats) = split(/\|/,$_);
			if($subcats{$catid}) {
				foreach(@subcats) {
					if($URL{'id'} eq $_) { $movecnt = $count; }
					++$count;
				}
				for($e = 0; $e < @subcats; $e++) {
					if($e == $movecnt) {
						if($movecnt == $count) { $add[$e] = $subcats[$e]; } else { $add[$e] = $subcats[++$movecnt]; $add[++$e] = $subcats[--$movecnt]; }
					} else { $add[$e] = $subcats[$e]; }
				}
				foreach(@add) { $add .= "$_/"; }
				$add =~ s/\/\Z//g;
				$readd .= "$cattitle|$catid|$temp1|$input|$desc|$add\n";
			} else { $readd .= "$_\n"; }
		}
	} elsif(!$noshow{$URL{'id'}} && $URL{'s'} eq 'up') {
		$counter = 0;
		for($i = 0; $i < @catbase; $i++) {
			($name,$id) = split(/\|/,$catbase[$i]);
			if($id eq $URL{'id'}) { $move = $i; last; }
		}
		for($e = 0; $e < @catbase; $e++) {
			if($e == $move) {
				if($move == 0) { $add[$e] = $catbase[$e]; } else {
					$add[--$e] = $catbase[$move]; $add[++$e] = $catbase[--$move];
				}
			}
				else { $add[$e] = $catbase[$e]; }
		}
	} elsif(!$noshow{$URL{'id'}} && $URL{'s'} eq 'down') {
		$max = @catbase-1;
		$counter = 0;
		for($i = 0; $i < @catbase; $i++) {
			($name,$id) = split(/\|/,$catbase[$i]);
			if($id eq $URL{'id'}) { $move = $i; last; }
		}
		for($e = 0; $e < @catbase; $e++) {
			if($e == $move) {
				if($move == $max) { $add[$e] = $catbase[$e]; } else {
					$add[$e] = $catbase[++$move]; $add[++$e] = $catbase[--$move];
				}
			}
				else { $add[$e] = $catbase[$e]; }
		}
	}

	fopen(FILE,"+>$boards/bdscats.db");
	if(!$noshow{$URL{'id'}}) { foreach(@add) { print FILE "$_\n"; } }
		else { print FILE $readd; }
	fclose(FILE);

	redirect("$surl,v=admin,a=boards");
}
1;