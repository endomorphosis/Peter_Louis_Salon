################################################
# E-Blah Bulliten Board Systems       Platinum #
################################################
# Copyright (c) 2001 - 2004 e-blah!            #
# All Rights Reserved.                         #
################################################

&is_admin;
CoreLoad('SourceCodeMod',1);

%Actions = (
	1 => 'After',
	2 => 'Replace',
	3 => 'Before'
);

sub SourceMod {
	if($URL{'p'} eq 'install') { &InstallMod; }
	elsif($URL{'p'} eq 'create') { &CreateMod; }
	elsif($URL{'p'} eq 'browser') { &Browser; }
	elsif($URL{'p'} eq 'remove') { unlink("$modsdir/$URL{'m'}.v1m","$modsdir/$URL{'m'}.installed"); $sourceremove = qq~<table class="border" cellpadding="4" cellspacing="1" width="700" align="center"><tr><td class="win" align="center"><span class="smalltext"><b>$URL{'m'} $sourcecode[86]</b></span></td></tr></table><br>~; }

	$title = $sourcecode[1];
	&headerA;
	$ebout .= <<"EOT";
<script language="javascript">
<!-- //
function clear(mod) {
 if(window.confirm("$sourcecode[85]")) { location = "$scripturl,v=admin,a=sourcemod,p=remove,m="+mod; }
}
// -->
</script>
$sourceremove<table class="border" cellpadding="4" cellspacing="1" width="700" align="center">
 <tr>
  <td class="titlebg"><b><img src="$images/open_thread.gif"> $title</b></td>
 </tr><tr>
  <td class="catbg"><b>$sourcecode[2]</b></td>
 </tr>
EOT
	@bgcolor = ('win2','win');
	opendir(DIR,"$modsdir/");
	@themes = readdir(DIR);
	closedir(DIR);
	foreach(@themes) {
		if($_ =~ m/(.*)(.v1m)/) {
			$color = $bgcolor[$count % 2];
			if($modlist) { $hr = qq~<hr class="hr" size="1">~; };
			$install = -e("$modsdir/$1.installed") ? $sourcecode[3] : $sourcecode[4];
			$about = '';
			$author = '';
			$msite = '';
			$mver = '';
			$bver = '';
			require("$modsdir/$1.v1m");
			$name = $1;
			$name2 = $name;
			$name =~ s/ /\+/gsi;
			$explain = '';
			if($about) { $explain = qq~<tr><td><span class="smalltext"><b>$sourcecode[5]:</b></span></td><td><span class="smalltext">$about</span></td></tr>~; }
			if($author) { $explain .= qq~<tr><td><span class="smalltext"><b>$gtxt{'36'}:</b></span></td><td><span class="smalltext">$author</span></td></tr>~; }
			if($msite) { $explain .= qq~<tr><td><span class="smalltext"><b>$sourcecode[7]:</b></span></td><td><span class="smalltext"><a href="$msite">$msite</a></span></td></tr>~; }
			if($mver) { $explain .= qq~<tr><td><span class="smalltext"><b>$sourcecode[8]:</b></span></td><td><span class="smalltext">$mver</span></td></tr>~; }
			if($bver) { $explain .= qq~<tr><td><span class="smalltext"><b>$sourcecode[9]:</b></span></td><td><span class="smalltext">$bver</span></td></tr>~; }
			if(!$explain) { $explain = qq~<tr><td><span class="smalltext">$sourcecode[80]</span></td></tr>~; }

			$name2 = $mname ? $mname : $name2;
			$modlist .= <<"EOT";
 <tr>
  <td class="$color">
   <table width="100%"><tr>
    <td>$hr<b>$name2</b><span class="smalltext"><br>&nbsp; <b><a href="$scripturl,v=admin,a=sourcemod,p=install,m=$name">$install</a></b> &#149; <a href="$scripturl,v=admin,a=sourcemod,p=install,m=$name,t=1">$sourcecode[11] $install</a> &#149; <a href="$scripturl,v=admin,a=sourcemod,p=create,m=$name">$sourcecode[10]</a> &#149; <a href="javascript:clear('$name');">$sourcecode[84]</a><hr class="hr" size="1"></span></td>
   </tr><tr>
    <td><table>$explain</table></td>
   </tr></table>
  </td>
 </tr>
EOT
			++$count;
			$name2 = ''; $mname = '';
		}
	}
	if(!$modlist) { ++$count; $modlist = qq~<tr><td class="win2"><i>$sourcecode[80]</i></td></tr>~; }
	$color = $bgcolor[$count % 2];
	$color2 = $bgcolor[($count+1) % 2];
	$ebout .= <<"EOT";
 <tr>
  <td class="win"><span class="smalltext">$sourcecode[12]</span></td>
 </tr>
$modlist
 <tr>
  <td class="$color"><span class="smalltext"><b>-&#187; <a href="$scripturl,v=admin,a=sourcemod,p=create">$sourcecode[13]</a></b></span></td>
 </tr><tr>
  <td class="$color2"><span class="smalltext"><b>$sourcecode[14]:</b> Version 1<br><b>$sourcecode[15]:</b> Platinum</span></td>
 </tr>
</table>
EOT
	&footerA;
	exit;
}

sub InstallMod {
	$URL{'m'} =~ s/\+/ /gsi;
	if(!-e("$modsdir/$URL{'m'}.v1m")) { &error("$sourcecode[16] $URL{'m'}.v1m"); }
	require("$modsdir/$URL{'m'}.v1m");

	$title = $sourcecode[17];
	&headerA;
	$ebout .= <<"EOT";
<table class="border" cellpadding="4" cellspacing="1" width="700" align="center">
 <tr>
  <td class="titlebg"><b><img src="$images/open_thread.gif"> $title</b></td>
 </tr><tr>
  <td class="win">
   <table class="border" cellpadding="4" cellspacing="1" align="center" width="650">
    <tr>
     <td class="titlebg"><span class="smalltext"><b>&#187; $sourcecode[18]</b></span></td>
    </tr><tr>
     <td class="win2"><span class="smalltext">
EOT
	foreach(@files) {
		($file,$keys) = split(/\|/,$_);
		++$keys;
		$filemod = $file;
		$filemod =~ s/Code\//$code\//gsi;
		$filemod =~ s/Root\//$root\//gsi;
		if($i) { $br = "<br>"; }
		fopen(FILE,"$filemod");
		@modify = <FILE>;
		fclose(FILE);
		chomp @modify;
		$ferror = '';
		if($modify[0] eq '') { $ferror = qq~<font color="red" size="1"> <b>:: $sourcecode[19]</b></font>~; $error = 1; $e{$filemod} = 1; }
		$ebout .= qq~$br<b>$sourcecode[20]:</b> $file$ferror<hr size="1" class="hr">~;
		$onestring = '';
		foreach(@modify) { $onestring .= "$_\\n"; }
		for($i = 1; $i < $keys; $i++) {
			$action = '';
			for($z = 0; $z < 4; $z++) {
				if($change{$file,$Actions{$z}}->[$i]) { $action = $Actions{$z}; }
			}
			if($action eq '') { $action = "$sourcecode[21]"; }

			if(!-e("$modsdir/$URL{'m'}.installed")) {
				$test = Format($change{$file,'s'}->[$i]);
				$test =~ s/\t/&nbsp; /g;
				$ebout .= "<b>Source Code:</b><blockquote>$test</blockquote>";
				$test = Format($change{$file,$action}->[$i]);
				$test =~ s/\t/&nbsp; /g;
				$ebout .= "<b>$action:</b><blockquote>$test</blockquote>";
				$find = $change{$file,'s'}->[$i];
				$replace = $change{$file,$action}->[$i];
				$install = 1;
			} else {
				$test = Format($change{$file,$action}->[$i]);
				$test =~ s/\t/&nbsp; /g;
				$ebout .= "<b>Source Code:</b><blockquote>$test</blockquote>";
				$test = Format($change{$file,'s'}->[$i]);
				$test =~ s/\t/&nbsp; /g;
				$ebout .= "<b>$action:</b><blockquote>$test</blockquote>";
				$find = $change{$file,$action}->[$i];
				$replace = $change{$file,'s'}->[$i];
			}
			$find =~ s~\n~\\n~gsi;
			$find =~ s/\cM//g;
			$replace =~ s~\n~\\n~gsi;
			$replace =~ s/\cM//g;
			$ok = 0;
			if($action eq 'Replace') {
				if($onestring =~ s~\Q$find\E~$replace~g) { $ebout .= "<b>--&#187; $sourcecode[22]</b><br>"; }
					else { $ebout .= qq~<b><font color="red">--&#187; $sourcecode[23]</font></b><br>~; $error = 1; $e{$filemod} = 1; }
			}
			elsif($action eq 'After' && $install) {
				if($onestring =~ s~\Q$find\E~$find\n$replace~g) { $ebout .= "<b>--&#187; $sourcecode[22]</b><br>"; }
					else { $ebout .= qq~<b><font color="red">--&#187; $sourcecode[23]</font></b><br>~; $error = 1; $e{$filemod} = 1; }
			}
			elsif($action eq 'Before' && $install) {
				if($onestring =~ s~\Q$find\E~$replace\n$find~g) { $ebout .= "<b>--&#187; $sourcecode[22]</b><br>"; }
					else { $ebout .= qq~<b><font color="red">--&#187; $sourcecode[23]</font></b><br>~; $error = 1; }
			}
			elsif($action eq 'Before' && !$install) {
				if($onestring =~ s~\Q$find\n$replace\E~$replace~g) { $ebout .= "<b>--&#187; $sourcecode[22]</b><br>"; }
					else { $ebout .= qq~<b><font color="red">--&#187; $sourcecode[23]</font></b><br>~; $error = 1; }
			}
			elsif($action eq 'After' && !$install) {
				if($onestring =~ s~\Q$replace\n$find\E~$replace~g) { $ebout .= "<b>--&#187; $sourcecode[22]</b><br>"; }
					else { $ebout .= qq~<b><font color="red">--&#187; $sourcecode[23]</font></b><br>~; $error = 1; }
			} else { $ebout .= qq~<b><font color="red">--&#187; $sourcecode[24] '$file'!</font></b><br>~; $error = 1; $e{$filemod} = 1; }
			$modify{$filemod} = $onestring; # This is a protection against errors
		}
	}
	if($URL{'s'}) { $error = 0; } # Turn off error checks and install
	$ebout .= <<"EOT";
    <tr>
     <td class="titlebg"><span class="smalltext"><b>&#187; $sourcecode[25]</b></span></td>
    </tr><tr>
     <td class="win2"><span class="smalltext">
EOT
	if($error && $URL{'t'}) { $ebout .= qq~$sourcecode[26]<hr class="hr" size="1">~; }
	elsif($error) { $ebout .= qq~<img src="$images/warning_sm.gif"> $sourcecode[27] <a href="$scripturl,v=admin,a=sourcemod,p=install,m=$URL{'m'},s=1">$sourcecode[28]</a>, $sourcecode[29]<hr class="hr" size="1">~; }
	elsif($URL{'t'}) { $ebout .= qq~$sourcecode[30] <a href="$scripturl,v=admin,a=sourcemod,p=install,m=$URL{'m'},s=1">$sourcecode[31]</a>.<hr class="hr" size="1">~; $error = 1; }
	while(($filename,$write) = each(%modify)) {
		if($error) { last; }
		$postinstalle = '';
		$time = time;
		rename($filename,"$filename.bkup.$time");
		$write =~ s/\\n/\n/gsi;
		fopen(FILE,">$filename");
		print FILE $write;
		fclose(FILE);
		if($e{$filename}) { $postinstalle = qq~ :: <font color="red">$sourcecode[32]</font>~; }
		$ebout .= qq~<b>$sourcecode[33]:</b> $filename$postinstalle<br>~;
		delete($modify{$file});
	}
	if($error) { $ebout .= qq~<b>Files Modified:</b> <i>None</i>~; }

	if(-e("$modsdir/$URL{'m'}.installed") && !$error) { unlink("$modsdir/$URL{'m'}.installed"); }
	elsif(!$error) {
		fopen(FILE,">$modsdir/$URL{'m'}.installed");
		fclose(FILE);
	}
	$ebout .= <<"EOT";
     </span></td>
    </tr>
   </table>
  </td>
 </tr><tr>
  <td class="titlebg"><a href="$scripturl,v=admin,a=sourcemod"><b>$gtxt{'26'}</b></a></td>
 </tr>
</table>
EOT
	&footerA;
	exit;
}

sub CreateMod {
	if($URL{'m'} eq '') {
		$title = $sourcecode[13];
		&headerA;
		$ebout .= <<"EOT";
<SCRIPT LANGUAGE="JavaScript">
<!-- Start
function Submit() {
 document.sform.submit.disabled = true;
}
// End -->
</script>
<table class="border" cellpadding="4" cellspacing="1" width="400" align="center">
 <tr><form action="$scripturl,v=admin,a=sourcemod,p=create,m=new" method="post" name="sform" onSubmit="Submit()">
  <td class="titlebg"><b><img src="$images/open_thread.gif"> $title</b></td>
 </tr><tr>
  <td class="win"><span class="smalltext">$sourcecode[35]</span></td>
 </tr><tr>
  <td class="win2">
   <table cellpadding="4" cellspacing="0" width="100%">
    <tr>
     <td align="right"><b>$sourcecode[36]:</b></td>
     <td><input type="text" class="textinput" name="name" value=""></td>
    </tr>
   </table></td>
 </tr><tr>
  <td class="win"><input type="submit" class="button" value=" $sourcecode[37] " name="submit"></td>
 <form></tr>
</table>
EOT
		&footerA;
		exit;
	} elsif($URL{'m'} eq 'new') {
		$FORM{'name'} =~ s/[#%+,\\\/:?"<>'|@^\$\&~'\]\[\;{}!`=-]//gsi;
		$FORM{'name'} =~ s/ //gsi;
		if($FORM{'name'} eq '') { &error($sourcecode[39]); }
		if(-e("$modsdir/$FORM{'name'}.v1m")) { &error($sourcecode[38]); }
		fopen(FILE,">$modsdir/$FORM{'name'}.v1m");
		print FILE "1;";
		fclose(FILE);
		redirect("$scripturl,v=admin,a=sourcemod,p=create,m=$FORM{'name'}");
	}
	if(!-e("$modsdir/$URL{'m'}.v1m")) { &error($sourcecode[40]); }
	if($FORM{'spec'}) { $URL{'f'} = "$FORM{'folder'}/$FORM{'spec'}"; }
	if($URL{'s'} || ($URL{'d'} && $URL{'f'} && $FORM{'action'} && $FORM{'searchfor'})) { &Save; }
	@ftm = split('/',$URL{'f'});
	if($ftm[@ftm-1] eq '') { $URL{'f'} = ''; }
	if($URL{'f'} =~ s/\/\Z//) { $URL{'f'} = ''; }
	$modifing = $URL{'f'} ? $URL{'f'} : "<i>$sourcecode[41]</i>";
	$title = $sourcecode[42];
	&headerA;
	if($URL{'d'} ne '' && $URL{'f'} eq '') { $ebout .= qq~<table class="border" cellpadding="4" cellspacing="1" width="700" align="center"><tr><td class="win" align="center"><span class="smalltext"><b><img src="$images/warning_sm.gif"> &nbsp; $sourcecode[81] &nbsp; <img src="$images/warning_sm.gif"></b></span></td></tr></table><br>~; }
	$ebout .= <<"EOT";
<table class="border" cellpadding="4" cellspacing="1" width="98%" align="center">
 <tr><form action="$scripturl,v=admin,a=sourcemod,p=create,m=$URL{'m'},f=$URL{'f'},d=$URL{'d'},n=$URL{'n'}" method="post" name="save">
  <td class="titlebg" colspan="2"><b><img src="$images/open_thread.gif"> $title</b></td>
 </tr><tr>
  <td class="win" colspan="2"><span class="smalltext">$sourcecode[43]</span></td>
 </tr><tr>
  <td class="win2" colspan="2"><b><img src="$images/open_thread.gif"> $sourcecode[44]:</b> "$modifing" <b>$sourcecode[45]:</b> "$URL{'m'}"<br><span class="smalltext">&nbsp; &nbsp; &nbsp;<b>- <a href="$scripturl,v=admin,a=sourcemod,p=browser,m=$URL{'m'}">$sourcecode[46]</a></b></span></td>
 </tr>
EOT
	require("$modsdir/$URL{'m'}.v1m");
	foreach(@files) {
		($file,$keys) = split(/\|/,$_);
		$filemod .= qq~<b><a href="$scripturl,v=admin,a=sourcemod,p=create,f=$file,m=$URL{'m'}">$file</a></b> ($keys $sourcecode[47])<br>~;
		++$keys;
		if($file ne $URL{'f'}) { next; }
		for($i = 1; $i < $keys; $i++) {
			for($z = 0; $z < 4; $z++) {
				if($change{$file,$Actions{$z}}->[$i]) { $action = $Actions{$z}; }
			}
			$acts .= qq~<b><a href="$scripturl,v=admin,a=sourcemod,p=create,m=$URL{'m'},f=$URL{'f'},d=$action,n=$i">$action</a></b> -&#155; $sourcecode[48] $i<br>~;
			if($URL{'n'} == $i) {
				$search = $change{$file,'s'}->[$i];
				$changes = $change{$file,$action}->[$i];
				$delete = qq~ <input type="submit" class="button" value=" $sourcecode[49] " name="delete">~;
			}
		}
	}
	if(!$filemod) { $filemod = $sourcecode[50]; }
	if($URL{'d'} && $URL{'f'}) {
		if($URL{'d'} eq 'After') { $action = $sourcecode[51]; }
		elsif($URL{'d'} eq 'Before') { $action = $sourcecode[52]; }
			else { $action = $sourcecode[53]; }
		$preform = <<"EOT";
$sourcecode[54]
<textarea name="searchfor" wrap="virtual" rows="5" cols="112">$search</textarea><br><br>

$action
<textarea name="action" wrap="virtual" rows="5" cols="112">$changes</textarea><br><br>
<input type="submit" class="button" value=" Save Action " name="submit">$delete<br>
<br><span class="smalltext"><b>$sourcecode[55]:</b> (\\t) $sourcecode[56]</span>
EOT
	} else { $preform = $sourcecode[57]; }

	$version =~ s/(.+?) \((.+?)\)//gsi;
	$version = $1;
	$bver = $bver ? $bver : $version;
	$mname = $mname ? $mname : $URL{'m'};
	$about = Unformat($about);

	if(!$acts) { $acts = $sourcecode[58]; }
	$ebout .= <<"EOT";
 <tr>
  <td class="catbg"><b>&#149; $sourcecode[59]</b></td>
  <td class="catbg" width="20%"><b>&#149; $sourcecode[60]</b></td>
 </tr><tr>
  <td class="win2" valign="top"><span class="smalltext">$preform</span></td>
  <td class="win" width="20%" valign="top"><span class="smalltext">$acts</span></td>
 </form></tr><tr>
  <td class="catbg" colspan="2"><span class="smalltext"><b>&#149; $sourcecode[61]</b></span></td>
 </tr><tr>
  <td class="win2" colspan="2"><span class="smalltext"><b>- <a href="$scripturl,v=admin,a=sourcemod,p=create,m=$URL{'m'},f=$URL{'f'},d=After">$sourcecode[63]</a><br>- <a href="$scripturl,v=admin,a=sourcemod,p=create,m=$URL{'m'},f=$URL{'f'},d=Before">$sourcecode[62]</a><br>- <a href="$scripturl,v=admin,a=sourcemod,p=create,m=$URL{'m'},f=$URL{'f'},d=Replace">$sourcecode[64]</a></b></span></td>
 </tr><tr>
  <td class="catbg" colspan="2"><span class="smalltext"><b>&#149; $sourcecode[65]</b></span></td>
 </tr><tr>
  <td class="win" colspan="2"><span class="smalltext">$filemod</span></td>
 </tr><tr><form action="$scripturl,v=admin,a=sourcemod,p=create,m=$URL{'m'},f=$URL{'f'},d=$URL{'d'},n=$URL{'n'},s=1" method="post" name="save">
  <td class="catbg" colspan="2"><span class="smalltext"><b>&#149; $sourcecode[66]</b></span></td>
 </tr><tr>
  <td class="win2" colspan="2">
   <table cellpadding="3" cellspacing="0">
    <tr>
     <td align="right"><span class="smalltext"><b>$sourcecode[67]:</b></span></td>
     <td><input type="text" class="textinput" name="title" value="$mname" size="50"></td>
    </tr><tr>
     <td align="right"><span class="smalltext"><b>$gtxt{'36'}:</b></span></td>
     <td><input type="text" class="textinput" name="author" value="$author" size="40"></td>
    </tr><tr>
     <td align="right"><span class="smalltext"><b>$sourcecode[69]:</b></span></td>
     <td><input type="text" class="textinput" name="msite" value="$msite" size="35"></td>
    </tr><tr>
     <td align="right"><span class="smalltext"><b>$sourcecode[70]:</b></span></td>
     <td><input type="text" class="textinput" name="mver" value="$mver" size="13"></td>
    </tr><tr>
     <td align="right"><span class="smalltext"><b>$sourcecode[71]:</b></span></td>
     <td><input type="text" class="textinput" name="bver" value="$bver" size="13"></td>
    </tr><tr>
     <td align="right" valign="top"><span class="smalltext"><b>$sourcecode[72]:</b></span></td>
     <td><textarea name="about" wrap="virtual" rows="3" cols="50">$about</textarea></td>
    </tr><tr>
     <td colspan="2"><input type="submit" class="button" name="submit" value=" $sourcecode[73] "></td>
    </tr>
   </table>
  </td>
 </form></tr>
</table>
EOT
	&footerA;
	exit;
}

sub QuickFormat {
	($format) = $_[0];
	$format =~ s/\(\\t\)/\t/gsi;
	$format =~ s/\'/\\\'/gsi;
	return($format);
}

sub Save {
	$key = 0;
	require("$modsdir/$URL{'m'}.v1m");

	$sfor = QuickFormat($FORM{'searchfor'});
	$act = QuickFormat($FORM{'action'});

	fopen(FILE,">$modsdir/$URL{'m'}.v1m");
	foreach(@files) {
		($file,$keys) = split(/\|/,$_);
		if($file eq $URL{'f'} && !$URL{'s'}) {
			$key = $keys;
			if(!$URL{'n'} && !$FORM{'delete'}) { ++$key; }
			elsif($URL{'n'} && $FORM{'delete'}) { --$key; }
			if($key == 0) { next; }
			$all .= qq~"$file|$key",~;
		} else { $all .= qq~"$file|$keys",~; }

		++$keys;
		$sds = 1;
		for($i = 1; $i < $keys; $i++) {
			$action = '';
			for($z = 0; $z < 4; $z++) {
				if($change{$file,$Actions{$z}}->[$i]) { $action = $Actions{$z}; }
			}

			if($action) {
				$change{$file,'s'}->[$i] = QuickFormat($change{$file,'s'}->[$i]);
				$change{$file,$action}->[$i] = QuickFormat($change{$file,$action}->[$i]);
				if($file eq $URL{'f'} && $i == $URL{'n'} && !$URL{'s'}) {
					$change{$file,'s'}->[$i] = $sfor;
					$change{$file,$action}->[$i] = $act;
				}
				if($FORM{'delete'} && $URL{'n'} == $i && $URL{'f'} eq $file) { next; }
				$topmods .= "# $sds\n\$change{'$file','s'}->[$sds] = '$change{$file,'s'}->[$i]';\n";
				$topmods .= "\$change{'$file','$action'}->[$sds] = '$change{$file,$action}->[$i]';\n";
				++$sds;
			}
		}
	}
	if(!$key && !$URL{'n'} && !$URL{'s'}) { ++$key; $all .= qq~"$URL{'f'}|$key",~; }
	$all =~ s/,\Z//gsi;
	print FILE "\@files = ($all);\n$topmods";
	if(!$URL{'n'} && !$FORM{'delete'} && !$URL{'s'}) {
		print FILE "# $key\n\$change{'$URL{f}','s'}->[$key] = '$sfor';\n";
		print FILE "\$change{'$URL{f}','$URL{d}'}->[$key] = '$act';\n";
	}
	if($URL{'s'}) {
		while(($in,$out) = each(%FORM)) {
			$out = Format($out);
			$out = QuickFormat($out);
			$FORM{$in} = $out;
		}
		if($FORM{'title'} ne $URL{'m'}) { print FILE qq~\$mname = '$FORM{title}';\n~; }
		if($FORM{'author'}) { print FILE qq~\$author = '$FORM{author}';\n~; }
		if($FORM{'about'}) { print FILE qq~\$about = '$FORM{about}';\n~; }
		if($FORM{'mver'}) { print FILE qq~\$mver = '$FORM{mver}';\n~; }
		if($FORM{'bver'}) { print FILE qq~\$bver = '$FORM{bver}';\n~; }
		if($FORM{'msite'}) { print FILE qq~\$msite = '$FORM{msite}';\n~; }
	} else {
		$mname = QuickFormat($mname);
		$author = QuickFormat($author);
		$about = QuickFormat($about);
		$mver = QuickFormat($mver);
		$bver = QuickFormat($bver);
		$msite = QuickFormat($msite);
		print FILE qq~\$mname = '$mname';\n\$author = '$author';\n\$about = '$about';\n\$mver = '$mver';\n\$bver = '$bver';\n\$msite = '$msite';\n~;
	}
	print FILE "1;";
	fclose(FILE);

	&redirect("$scripturl,v=admin,a=sourcemod,p=create,m=$URL{'m'},f=$URL{'f'}");
}

sub Browser {
	opendir(DIR,"$code/");
	@directory = readdir(DIR);
	closedir(DIR);
	foreach(@directory) {
		if(-d("$root/$_") || $_ eq '.' || $_ eq '.htaccess' || $_ eq '..') { next; }
		$codedir .= qq~<a href="$scripturl,v=admin,a=sourcemod,p=create,f=Code/$_,m=$URL{'m'}">$_</a><br>~;
	}
	opendir(DIR,"$root/");
	@directory = readdir(DIR);
	closedir(DIR);
	foreach(@directory) {
		if(-d("$root/$_") || $_ eq '.' || $_ eq '.htaccess' || $_ eq '..' || $_ eq 'Boards' || $_ eq 'Code' || $_ eq 'Members' || $_ eq 'Messages' || $_ eq 'Prefs' || $_ eq 'Themes' || $_ eq 'Mods') { next; }
		$rootdir .= qq~<a href="$scripturl,v=admin,a=sourcemod,p=create,f=Root/$_,m=$URL{'m'}">$_</a><br>~;
	}
	opendir(DIR,"$language/");
	@directory = readdir(DIR);
	closedir(DIR);
	$lngdir = qq~<a href="$scripturl,v=admin,a=sourcemod,p=create,f=Languages/$languagep.lng,m=$URL{'m'}">$languagep.lng</a><br>~;
	foreach(@directory) {
		if(-d("$root/$_") || $_ eq '.' || $_ eq '.htaccess' || $_ eq '..' || $_ eq 'Boards' || $_ eq 'Code' || $_ eq 'Members' || $_ eq 'Messages' || $_ eq 'Prefs' || $_ eq 'Themes' || $_ eq 'Mods') { next; }
		$lngdir .= qq~<a href="$scripturl,v=admin,a=sourcemod,p=create,f=Languages/$languagep/$_,m=$URL{'m'}">$languagep/$_</a><br>~;
	}	opendir(DIR,"$language/");
	$title = $sourcecode[74];
	&headerA;
	$ebout .= <<"EOT";
<table class="border" cellpadding="4" cellspacing="1" width="400" align="center">
 <tr><form action="$scripturl,v=admin,a=sourcemod,p=create,m=$URL{'m'}" method="post" name="save">
  <td class="titlebg"><b><img src="$images/open_thread.gif"> $title</b></td>
 </tr><tr>
  <td class="catbg"><b>&#149; $sourcecode[75]: Root</b></td>
 </tr><tr>
  <td class="win"><span class="smalltext">$rootdir</span></td>
 </tr><tr>
  <td class="catbg"><b>&#149; $sourcecode[75]: Code</b></td>
 </tr><tr>
  <td class="win"><span class="smalltext">$codedir</span></td>
 </tr><tr>
  <td class="catbg"><b>&#149; $sourcecode[75]: Languages/$languagep</b></td>
 </tr><tr>
  <td class="win"><span class="smalltext">$lngdir</span></td>
 </tr><tr>
  <td class="catbg"><b>&#149; $sourcecode[76]</b></td>
 </tr><tr>
  <td class="win2">
   <table cellpadding="4" cellspacing="0">
    <tr>
     <td align="right"><b>$sourcecode[77]:</b></td>
     <td><select name="folder"><option value="Root">Root</option><option value="Code">Code</option><option value="Languages">Languages</option><option value="">$sourcecode[82]</option></select></td>
    </tr><tr>
     <td align="right"><b>$sourcecode[78]:</b></td>
     <td><input type="text" class="textinput" name="spec" value=""></td>
    </tr><tr>
     <td align="right" colspan="2"><input type="submit" class="button" value=" $sourcecode[79] " name="dir"></td>
    </tr>
   </table>
  </td>
 </form></tr>
</table>
EOT
	&footerA;
	exit;
}
1;