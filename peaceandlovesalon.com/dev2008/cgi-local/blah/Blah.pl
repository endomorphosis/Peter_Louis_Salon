#!/usr/bin/perl

############################################################
# E-Blah Bulliten Board Systems                   Platinum #
############################################################
# Software Version  : Platinum Release 8                   #
# Project started   : December 2001 by Justin              #
# Distrobuted by    : http://www.eblah.com                 #
# License Agreement : http://www.eblah.com/?v=license      #
############################################################
# Copyright (c) 2001-2004 e-blah!  -  All Rights Reserved  #
############################################################

# Global version information:
$theblahver = 15;
$version    = 'Platinum 8.1 BETA';
$versioncr  = 'Platinum 8.1';

$scripttype = -e('Blah.cgi') ? 'cgi' : 'pl';

# Once you have a working install, you can comment this out so that it will not require the CGI module
use CGI::Carp 'fatalsToBrowser';
use Fcntl ':flock';

$languagep = "English";
$languages = "./Languages";

require('Settings.pl');

require("$code/QuickCore.pl");
UFS();
CheckCookies();

if($URL{'theme'}) { $settings[26] = $URL{'theme'}; } # Preview
if($settings[26]) { require("$theme/$settings[26].v2") if -e("$theme/$settings[26].v2"); }

$language = "$languages/$languagep";
require("$language.lng");
require("$code/Routines.pl");
require("$code/Load.pl");

# Load basic features that we can use later
LMG();
BoardCheck();
ClickLog();
AL();

# Lets see if this user should have access ...
if(($maintance || $noguest) || $lockout || -e("$root/Maintance.lock")) { CoreLoad('BoardLock'); MainLO(); }
Ban();

sub UFS {
	my($query);
	@url = split(/\,/,$ENV{'QUERY_STRING'});
	foreach (@url) {
		($action,$actiondo) = split(/=/,$_);
		$URL{$action} = $actiondo;

		if($action =~ /\&/) { $blockform = 1; } # Hack attemp, block forms!!
	}

	if(!$blockform) {
		if($ENV{'CONTENT_TYPE'} =~ /multipart\/form-data/) { # If it's an upload (uses CGI library) ...
			require CGI || &error("CGI Load error"); import CGI qw(:standard);
			$form = new CGI;
			foreach $var ($form->param) {
				$output = join(',',$form->param($var));
				if(!$nouselist{$var}) { $FORM{$var} = $output; }
			}
		} else { # If it's not ...
			read(STDIN, my $temp, $ENV{'CONTENT_LENGTH'});
			@pairs = split(/&/,$temp);

			foreach (@pairs) {
				($key,$content) = split(/=/,$_);
				$content =~ tr/+/ /;
				$content =~ s/%(..)/pack("c",hex($1))/ge;
				$key =~ tr/+/ /;
				$key =~ s/%(..)/pack("c",hex($1))/ge;
				chomp($content);
				$FORM{$key} = $content;
			}
		}
	}
}

sub CoreLoad {
	my($temp1,$temp2) = @_;
	if($CoreLoaded{$temp1,$temp2}) { return; }
	$CoreLoaded{$temp1,$temp2} = 1;
	if($temp2 == 1) { $load = "$language/$temp1.lng"; }
	elsif($temp2) { $load = $temp1; }
		else { $load = "$code/$temp1.pl"; }
	eval { require($load) };

	if($_[1] == 2) {
		if($@) { return(0); }
		return(1);
	}
	if($@) { error(qq~$rtxt[52]\n\n$load\n\n\[size=1\]$@\[/size\]~,2); }
}

{
	%LoadBoard = (
		'memberpanel' => 'MemberPanel,MemberPanel',
		'login'       => 'Login,Login',
		'mod'         => 'Moderate,Moderate',
		'register'    => 'Register,Register',
		'admin'       => 'AdminList,AdminList',
		'post'        => 'Post,Post',
		'ppoll'       => 'Poll,PPoll',
		'display'     => 'MessageDisplay,MessageDisplay',
		'mindex'      => 'MessageIndex,MessageIndex',
		'print'       => 'Print,PrintDisplay',
		'members'     => 'Members,Members',
		'report'      => 'Report,Report',
		'cal'         => 'Calendar,CalendarLoad',
		'download'    => 'Attach,Download',
		'stats'       => 'Stats,Stats',
		'search'      => 'Search,Search',
		'invite'      => 'Invite,Invite',
		'recommend'   => 'Recommend,Recommend',
		'mark'        => ',Mark',
		'shownews'    => 'Portal,Shownews',
		'portal'      => 'Portal,Portal'
	);

	if($LoadBoard{$URL{'v'}}) {
		($core,$sub) = split(',',$LoadBoard{$URL{'v'}});
		CoreLoad($core) if $core ne '';
		&$sub;
	} elsif($URL{'m'} && $URL{'b'}) { CoreLoad('MessageDisplay'); MessageDisplay(); }
	elsif($URL{'b'}) { CoreLoad('MessageIndex'); MessageIndex(); }
		else { CoreLoad('BoardIndex'); LoadIndex(); }
	exit;
}
1;