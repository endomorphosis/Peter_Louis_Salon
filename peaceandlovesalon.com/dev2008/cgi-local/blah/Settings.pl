####################################################
# E-Blah Bulliten Board Systems           Platinum #
####################################################
# This file contains setup information vital to    #
# running e-blah. Read Blah.pl for full headers.   #
# You should not edit this file manualy. Use Admin #
# Center on your Forum to change these settings.   #
####################################################

$bversion = 12;      # Version

####### Directories Sets #######
$root = ".";
$code = "./Code";
$boards = "./Boards";
$prefs = "./Prefs";
$members = "./Members";
$messages = "./Messages";
$theme = "./Themes";
$modsdir = "./Mods";
$images = "/blahdocs/images";
$simages = "/blahdocs/Smilies";
$avsurl = "/blahdocs/Avatars";
$avdir = "$ENV{'DOCUMENT_ROOT'}/blahdocs/Avatars";
$realurl = "http://mysite.domain.dom/cgi-bin/MB";
$bcloc = "/blahdocs/bc.js";
$helpdesk = "";
$languages = "./Languages";
$newsloc = "/blahdocs/news.js";
$templates = "$ENV{'DOCUMENT_ROOT'}/blahdocs/template";
$templatesu = "/blahdocs/template";

#######   Colors Prefs   #######
$color{'titlebg'} = qq**;
$color{'win'}     = qq**;
$color{'win2'}    = qq**;
$color{'catbg'}   = qq**;
$color{'border'}  = qq**;

#######  Essential Sets  #######
$regto = qq*admin*;
$eadmin = q*admin@mysite.com*;
$cookpw = qq*blahpass*;
$cooknme = qq*blahname*;
$mailuse = 1;
$smaill = "/usr/sbin/sendmail";
$mailhost = "";
$emailsig = q*This e-mail was sent via eblah.&nbsp; It is not in any way related to eblah or eblah.com.*;
$mailauth = 0;
$mailusername = '';
$mailpassword = '';

#######   Board Locks    #######
$maintance = 0;
$maintancer = qq~This board is being upgraded.~;
$lockout = 0;
$noguest = 0;
$creg = 0;
$serload = 0;

#######   Upload Prefs   #######
$uallow = 0;
$avupload = 0;
$ntsys = 0;
$uploaddir = "$ENV{'DOCUMENT_ROOT'}/blahdocs/uploads";
$uploadurl = "/blahdocs/uploads";
$allowedext = "";
$maxsize = "1";
$maxsize2 = ".25";

#######    Main Prefs    #######
$upbc = 1;
$noguestp = 1;
$invfri = 0;
$sppd = 0;
$al = 1;
$whereis = 1;
$showmove = 1;
$apic = 1;
$hmail = 1;
$slpoller = 1;
$BCSmile = 1;
$BCLoad = 1;
$showreg = 1;
$pmmaxquota = "0";
$maxsig = "500";
$gtoff = '0';
$datedis = 0;
$timedis = 0;
$maxmess = 15;
$maxdis = 30;
$mbname = q*My New e-blah! Board*;
$mincount = "5";
$medcount = "50";
$bigcount = "100";
$maxcount = "500";
$picheight = "65";
$picwidth = "65";
$btod = 1;
$mtext = 0;
$amar = 1;
$sauser = 1;
$mmpp = 25;
$iptimeout = 5;
$vhtdmax = 50;
$htdmax = 25;
$enews = 0;
$showgender = 0;
$totalpp = 10;
$memguest = 0;
$gsearch = 0;
$showdes = 1;
$sall = 0;
$vradmin = 0;
$showtheme = 1;
$sview = 1;
$quickreg = 0;
$showactive = 1;
$logactive = 1;
$showevents = 0;
$reversesum = 1;
$gdisable = 0;
$redirectfix = 0;
$pmdisable = 0;
$languagep = "English";
$allowrate = 0;
$nocomma = ',';
$html = 0;
@htmls = ();
$hiddenmail = 0;
$nonotify = 0;
$polltop = 1;
$pollops = "7";
$gtzone = "0";
$quickreply = 1;
$gmaxsmils = 200;
$maxmesslth = 50000;
$maxsumc = "40";
$upevents = 7;

#######     Logging      #######
$eclick = 1;
$uextlog = 1;
$kelog = 1;
$logip = 1;
$logcnt = "60";
$logdays = 365;
$activeuserslog = 15;

#######   File Locking   #######
$fopen = 2;
$fclose = 8;
$uflock = 1;

#######       News       #######
$newsboard = "";
$newsshow = 5;
$newslength = 200;

#######       MODS       #######
$yabbconver = 0;
1;
