<?php
	include("cart.php");
	
	class Basket extends Controller
	{
		function Basket()
		{
			parent::Controller();
		}
		
		
		function baskets()
		{
			$this->load->view('basketview');
			BasketView::topbody();
			BasketView::baskets();
			BasketView::buttombody();
		}
		
		function shipadd()
		{
			if($this->session->userdata('user_email')==''){
				redirect("login/signin");
			}
			$this->load->view('shippingadd_view');
			ShippingAddView::topbody();
			ShippingAddView::shipadd();
			ShippingAddView::buttombody();
			
		}
		
		
		function addprevship()
		{
			$_SESSION['shipping_id']	=	$_POST['address_id'];
			redirect('billing/pickship');
		}
		
		
	}//end of class Basket
?>