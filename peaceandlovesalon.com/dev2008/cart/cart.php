<?
class CartItem
	{
		var $ProductId;
		var $Price;
		var $UnitPrice;
		var $Quantity;
		var $Title;
		
		function CartItem($pid, $qty, $price, $title)
		{
			$this->ProductId = $pid;
			$this->UnitPrice = $price;
			$this->Quantity = $qty;
			$this->Price = ($qty * $this->UnitPrice);
			$this->Title = $title;
		}
		function Update($qty)
		{
			$this->Quantity = $qty;
			$this->Price = ($qty * $this->UnitPrice);
		}
	} //end of class CartItem
	
	
	class Cart
	{
		var $Items;
		var $TotalItem;
		var $PriceTotal;
		var $PriceSubtotal;
		var $PriceShipping;
		var $TypenameShipping;
		var $PriceTax;
		var $PriceDiscount;
		var $PriceDiscount_descr;
		var $PriceDiscount_code;
		var $PriceDiscount_rate;
		var $CartName = "ShoppingCart";
		function Cart()
		{
			$cart = NULL;
			if(isset($_SESSION[$this->CartName])){
				$cart = $_SESSION[$this->CartName];
			}
			
			if($cart==NULL){
				$this->Items 				= NULL;
				$this->TotalItem 			= 0;
				$this->PriceTotal 			= 0;
				$this->PriceSubtotal 		= 0;
				$this->PriceShipping 		= 0;
				$this->TypenameShipping 	= '';
				$this->PriceTax 			= 0;
				$this->PriceDiscount 		= 0;
				$this->PriceDiscount_descr	= '';
				$this->PriceDiscount_code	= '';
				$this->PriceDiscount_rate	= 0;
			}
			else{
				$this->Items 				= $cart->Items;
				$this->TotalItem 			= $cart->TotalItem;
				$this->PriceTotal 			= $cart->PriceTotal;
				$this->PriceSubtotal 		= $cart->PriceSubtotal;
				$this->PriceShipping 		= $cart->PriceShipping;
				$this->TypenameShipping 	= $cart->TypenameShipping;
				$this->PriceTax 			= $cart->PriceTax;
				$this->PriceDiscount 		= $cart->PriceDiscount;
				$this->PriceDiscount_descr	= $cart->PriceDiscount_descr;
				$this->PriceDiscount_code	= $cart->PriceDiscount_code;
				$this->PriceDiscount_rate	= $cart->PriceDiscount_rate;
			}
			
		}
		function GetPriceSubtotal()
		{
			$this->PriceSubtotal = 0;
			if($this->Items != NULL){
				foreach($this->Items as $key=>$item){
					$this->PriceSubtotal += $item->Price;
				}
			}
			$this->SetCart();
			return $this->PriceSubtotal;
		}
		function CalculateTax($percentage)
		{
			$this->GetPriceSubtotal();
			$this->PriceTax = (($this->PriceSubtotal * $percentage) / 100);
			$this->SetCart();
			return $this->PriceTax;
		}
		function CalculateShipping($percentage)
		{
			/*
			$this->GetPriceSubtotal();
			$this->PriceSubtotal = $this->PriceSubtotal
			*/
		}
		function GetPriceTotal()
		{
			$this->GetPriceSubtotal();
			$this->PriceTotal = $this->PriceSubtotal + $this->PriceShipping + $this->PriceTax - $this->PriceDiscount;
			
			return $this->PriceTotal;
		}
		function GetTotalItem()
		{
			$this->TotalItem = sizeof($this->Items);
			
			return $this->TotalItem;
		}
		function CheckItem($pid)
		{
			if($this->Items != NULL){
				foreach($this->Items as $key=>$item){
					if($item->ProductId == $pid){
						return $key;						
					}
				}
			}
			return -1;
		}
		function Add($pid=0, $qty=1, $price=0.0, $title="")
		{
		
			$key = $this->CheckItem($pid);
			if($key==-1){
				$item = new CartItem($pid, $qty, $price, $title);
				$items =  array_merge ( (array)$this->Items, array($item));
				$this->Items = $items;

			}
			
		}
		function Update($pid=0, $qty=1)
		{
		
			if($qty<=0){
			
				$this->Remove($pid);
			}
			else{
				$key = $this->CheckItem($pid);				
				  if($key>=0){
					$item = $this->Items[$key];
					$item->Update($qty);
					$this->Items[$key] = $item;
				}
			}
			$this->SetCart();
		}
		function Remove($pid=0)
		{
			
			$key = $this->CheckItem($pid);
			
			if(isset($this->Items[$key]))
			{
				unset($this->Items[$key]);
			}
			$this->SetCart();
			
			
		}
		function Validate()
		{
			$flag = NULL;
			$total = $this->GetTotalItem();
			if($total>0){
				$flag = $this;
			}
			return $flag;
		}
		function SetCart()
		{
			$_SESSION[$this->CartName] = $this;
		}
		
		
		function Update_s()
		{
			foreach($_REQUEST['product_id'] as $keys => $value)
			{
				
				$this->Update($keys, $value);
				$this->SetCart();			
				
			}
			
		}
		
		function CalculateDiscount($discount_code,$dis_desr,$rate)
		{
			$this->GetPriceSubtotal();
			$this->PriceDiscount = $this->PriceSubtotal*$rate;
			$this->PriceDiscount_code = $discount_code;
			$this->PriceDiscount_descr = $dis_desr;
			$this->PriceDiscount_rate=$rate;
			$this->SetCart();
			return $this->PriceDiscount;
		}//end of CalculateDiscount($row->discount_code,$row->dis_desr,$row->rate)
	} //end of class Cart
?>