<?php
	include("cart.php");
	
	class Shoppingcart extends Controller
	{
		function Shoppingcart()
		{
			parent::Controller();
		}
				
		
		function additems($pid=0,$qty,$price,$title)
		{				
			$pid = round($pid);
			
			$cart = new Cart();
			
			if($pid>0){			
				$cart->Add($pid,$qty,$price,$title);
			}
			
			$cart = $cart->Validate();
			
			$cart->SetCart();
			
			Shoppingcart::Display();
			
			
		}
		
		function update($pid=0, $qty)		
		{
			$pid = round($pid);
			$qty = round($qty);

			$cart = new Cart();
			if($pid>0){
				$cart->Update($pid, $qty);
			}
			
			$cart = $cart->Validate();
			
			$cart->SetCart();
			
			Shoppingcart::Display();
			
		}
		
		function remove($pid=0, $qty)		
		{
			//Shoppingcart::update($pid, 0);
			$cart = new Cart();
			
			$cart->Remove($pid);
			
			Shoppingcart::Display();			
		}
		
		//start of function
		function additem()
		{
			$pid = 0;
			if(isset($_REQUEST['variant_id'])){
				$pid = round($_REQUEST['variant_id']);
			}			
			// DB
			$this->db->where('variant_id',$pid);
			$query = $this->db->get('itemdesc');
				
			if($query->num_rows() > 0)
			{
				foreach($query->result() as $rows){
					$price	=	$rows->price;
					$title	=	$rows->descr;
				}				
			}
			
			Shoppingcart::additems($pid,1,$price,$title);
		}	
		
		
		function delitem()
		{
			$did = 0;
			if($this->uri->segment(3) != '' || $this->uri->segment(3) !=0 ){
				$did = round($this->uri->segment(3));
				$qty = round($this->uri->segment(4));
			}
			Shoppingcart::remove($did, $qty);	
		}
		
		function editQty()
		{
			$cart = new Cart();
			if(isset($_POST['variant_id']))
				$cart->Update_s();
			
			$cart = $cart->Validate();
			
			//$cart->SetCart();
			
			Shoppingcart::Display();
		}
		
		
		function Display()
		{
			redirect('basket/baskets');
		}		
		
		
		function promcode()
		{
		 	$cart = new Cart();
			
			if($cart->Items !=NULL)
			{
				$pcode	=	trim($_POST['discount_code']);
				$this->db->where('discount_code',$pcode);
				$query	=	$this->db->get('discounts');
				if($query->num_rows() > 0)
				{
					$row	=	$query->row();				
					$cart->CalculateDiscount($row->discount_code,$row->dis_desr,$row->rate);
					
					$cart	=	$cart->Validate();
				
					$cart->SetCart();
				
					Shoppingcart::Display();
				}//end if
			}
			else
			{
				Shoppingcart::Display();
			}//end if	
		}		
		
 		
	}//end of function Shoppingcart
?>