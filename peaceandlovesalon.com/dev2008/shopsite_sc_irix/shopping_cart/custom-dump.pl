#!/usr/bin/perl




print "Content-type: text/html\n\n";

if ($ENV{'REQUEST_METHOD'} =~ /get/i) { 
    $buffer = $ENV{'QUERY_STRING'};
    $method = "Get";
}
else {
    read(STDIN, $buffer, $ENV{'CONTENT_LENGTH'});
    $method = "Post";
}

print "<HTML><HEAD>\n";
print "<TITLE>Query Results</TITLE>\n";
print "</HEAD><BODY>\n";
print "<h1>Query Results</h1>\n";
print "<h2>(Spill my Guts)</h2>\n";
print "<h3>Method: $method</h3>\n";
print "<PRE>\n";

@nvpairs = split(/&/, $buffer);
foreach $pair (@nvpairs)
{
    ($name, $value) = split(/=/, $pair);

    $name  =~ tr/+/ /;
    $name  =~ s/%([a-fA-F0-9][a-fA-F0-9])/pack("C", hex($1))/eg;
    $value =~ tr/+/ /;
    $value =~ s/%([a-fA-F0-9][a-fA-F0-9])/pack("C", hex($1))/eg;

    print "$name = $value\n";
}
print "</PRE>\n";
print "</BODY></HTML>\n";
