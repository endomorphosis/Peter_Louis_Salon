





require "dbapi.pl";


$UHEADER = "uheader.dat";
$UFOOTER = "ufooter.dat";





sub Emit {
	local( *db, *rec, $TYPE ) = @_;
	local( $hohum );


	(defined(%db) && defined(%rec)) || return undef;

	{
		local( %CUR_DB ) = %db;
		local( %CUR_REC ) = %rec;
		local( @FILESTACK ) = undef;
		local( @DBSTACK ) = undef;
		local( $template, $template_file ) = undef;
		local( $template_call, $function_call );

		$template_call = &GetTemplateCall(*db, *rec);

		if ($template_call eq "CustomParser") {
		    &CustomParser(*db,*rec);
		} else {
		   $function_call = "&$template_call; 1;";

		   $hohum=eval($function_call);
		   if (!defined($hohum)) {

		       $template = &GetTemplate(*db,*rec);
		       $template || return undef;
		       $template_code = "sub $template_call {$template } 1;";
		       undef($template);

		       $hohum=eval($template_code);
		       if (!defined($hohum)) {
			   printf STDOUT &plSSMsgCatGet(SSMSGTemplCompError, "Template compilation error: %s"), $@;
		       } else {
			   $hohum=eval($function_call);
			   if (!defined($hohum)) {
			       printf STDOUT &plSSMsgCatGet(SSMSGFcnCallCompError, "Function call compilation error: %s"), $@;
			   }
		       }
		   }
	       }
	}
	&NewHandle( &EndNewFile2() );

	return 1;
}


sub EmitFile {

	local($fn) = @_;
	local($handle);

	$handle = $fn;

	open( $handle, $fn ) || return undef;

	while(<$handle>)
	{
		s/\000//g;
		print;
	}
	close( $handle );
	return 1;
}




sub GetTemplate {
	local( *db, *rec ) = @_;
	local( $template );
	local( $templatetype, $templatefile );

	$templatefile = &GetField( *db, *rec, "Template" );
	chop $templatefile;
	$templatetype = $db{"name"};



	if (!(open TEMPFILE,"$BO_DIR/templates/$templatetype/$templatefile"))
	{

            opendir TEMPDIR, "$BO_DIR/templates/$templatetype"
                or &throw("Can't find template directory!");

            @allfiles = readdir TEMPDIR;
            closedir TEMPDIR;

            foreach $allfiles (@allfiles) {
                if ($allfiles =~ /^$templatefile.*/) {
                    $templatefile = $allfiles;
                    last;
                }
            }
	    if (!(open TEMPFILE,"$BO_DIR/templates/$templatetype/$templatefile"))
            {#Try the user's data directory.

		 if (!(open TEMPFILE,
		       "$DATA_DIR/templates/$templatetype/$templatefile"))
		 {

		     opendir TEMPDIR, "$DATA_DIR/templates/$templatetype"
			 or &throw("Can't find template!");

		     @allfiles = readdir TEMPDIR;
		     closedir TEMPDIR;

		     foreach $allfiles (@allfiles) {
			 if ($allfiles =~ /^$templatefile.*/) {
			     $templatefile = $allfiles;
			     last;
			 }
		     }

		     open TEMPFILE,"$DATA_DIR/templates/$templatetype/$templatefile"
			 or &throw("Can't find template!");
		 }
	     }
	}
	
	while (<TEMPFILE>) {
            $template .= $_;
        }
	
	close TEMPFILE;

	return $template;
}

sub GetTemplateCall {
    local( *db, *rec ) = @_;
    local( $final );
    local( $templatenum, $subnum);

    $templatenum = &GetField( *db, *rec, "Template" );
    chop($templatenum);
    if ($templatenum =~ /([0-9][0-9]).*/) {
	$final = $db{"name"};
	chop($final);
	$final .= $1;
    } else {
	$final = "CustomParser";
    }

    return ($final);
}


sub SingleField {
	local( $dbname, $recnum, $field ) = @_;
	local( %db, %rec );

	(%db = &InitDatabase( $dbname )) || return undef;

	(%rec = &GetRecord(*db, $recnum)) || return undef;

	&GetField( *db, *rec, $field );

	&CloseDatabase( *db );
}


sub LoadFields {
	local( *rec ) = @_;

	die(&plSSMsgCatGet(SSMSGLoadFieldsNotImpl, "LoadFields is not yet implemented.\n"));
}


sub IgnoreHTML {

	local( $str ) = @_;

	$str =~ s/<[^\>]*>//g;

	return $str;
}


sub NewDB {
	local( $fn ) = @_;

	if (!defined($fn)) {
	    print STDOUT (&plSSMsgCatGet(SSMSGUndefFilename, "Undefined filename!  <br>\n"));
            return undef;
        }

	if( defined($CUR_DB{'name'}) )
	{
		push( @DBSTACK, $CUR_DB{'name'} );
		push( @DBSTACK, $CUR_REC{'number'} );
		push( @DBSTACK, $CUR_REC{'cur_item'} );
		&CloseDatabase(*CUR_DB);
	}

	%CUR_DB = &InitDatabase($fn);
	undef(%CUR_REC);

        if (!defined(%CUR_DB)) {
	    print STDOUT &plSSMsgCatGet(SSMSGUndefDatabase, "AUGH!  Undefined database!\n");
        }
	
	return 1;
}	

sub EndNewDB {
	local( $dbname, $recnum, $itemnum );

	$itemnum = pop(@DBSTACK);
	$recnum = pop(@DBSTACK);
	$dbname = pop(@DBSTACK);

	defined( $CUR_DB{'name'} ) && &CloseDatabase( *CUR_DB );

	if(defined($dbname))
	{
 
		%CUR_DB = &InitDatabase($dbname); 

		%CUR_REC = &GetRecord( *CUR_DB, $recnum );

		$CUR_REC{'cur_item'} = $itemnum;

		return 1;
	}
	else
	{
		undef(%CUR_DB);
		undef(%CUR_REC);

		return undef;
	}
}


sub NewHandle {
	local($handle) = @_;

	defined($handle) || return undef;

	push( @Filestack, select );
	select($handle);
}

sub NewFile {
	local($fn) = @_;
	local($handle);
	
	defined( $fn ) || die(&plSSMsgCatGet(SSMSGBadCall2NewFile, "Bad call to NewFile!"));

	$handle = $fn;


	if (!open($handle, "> $fn")){
	    printf STDOUT (&plSSMsgCatGet(SSMSGErrOpeningFile, "<p><b>ERROR</b> Unable to open file %s"), $fn);
	    print STDOUT (&plSSMsgCatGet(SSMSGCheckPermOwn, "<br>Check to see if the directory or file
has the appropriate permissions and ownership."));
	}
	&NewHandle($handle);
}



sub EndNewFile {
	local( $foo );

	close(select);

	$foo = pop(@Filestack);
	if(defined($foo)) {
		select($foo);
	}
}

sub EndNewFile2 {
    local( $foo );

    $foo = pop(@Filestack);
    if(defined($foo)) {
	select($foo);
    }
}



sub ResetDB {
	$CUR_REC = undef;
}

sub ResetRecord {

    $CUR_REC{'cur_item'}= undef;
}

sub SetRecord {
	local( $num ) = @_;

	defined( $num ) || return undef;
	($num<0 || $num>=$CUR_DB{'numrecs'}) && return undef;

	%CUR_REC = &GetRecord( *CUR_DB, $num );

	defined( $CUR_REC{'number'} ) || return undef;

	$CUR_REC{'cur_item'} = undef;

	return 1;
}
		
sub NextRecord {
	local( $num );

	defined(%CUR_DB) || return undef;

	if(defined(%CUR_REC)) 
	{
		$num = $CUR_REC{'number'} + 1;
	}
	else
	{
		$num = 0;
	}

        %CUR_REC = &GetRecord( *CUR_DB, $num ); 


	while(($num<$CUR_DB{'numrecs'}) && !defined($CUR_REC{'number'})) 
	{
		$num++;

                %CUR_REC = &GetRecord( *CUR_DB, $num );

	}

	($num >= $CUR_DB{'numrecs'}) && return undef;

	$CUR_REC{'cur_item'} = undef;

	return 1;
}

sub NextItem {

	local( $num );
	local($numitems);

	defined(%CUR_DB) || &throw("Can't NextItem before NewDB() has been called!");
	defined(%CUR_REC) || &throw("Can't NextItem without a current record!");

	$numitems = &GetItem( *CUR_DB, *CUR_REC );

	defined($numitems) || return undef;

	if( defined($CUR_REC{'cur_item'}) )
	{
		$CUR_REC{'cur_item'}++;
		$num = $CUR_REC{'cur_item'};
	}
	else
	{
		$num = $CUR_REC{'cur_item'} = 0;
	}

	($num >= $numitems) && return undef;


	local( $foo, $bar ) = &GetItem( *CUR_DB, *CUR_REC, $num );
	return ($foo, $bar);
}

sub Field {
	local( $name ) = @_;

	return &GetField( *CUR_DB, *CUR_REC, $name );
}

sub FieldType {
	local( $name ) = @_;

	return &GetType( *CUR_DB, &GetFieldNum($name) );
}

1;








