#!/usr/local/bin/perl







require "i18n.pl";

plSSMsgCatOpen(USERCLASS_MERCHANT);


print "Content-type: text/html\n";

if ($ENV{'REQUEST_METHOD'} eq "POST") {
    read(STDIN, $buffer, $ENV{'CONTENT_LENGTH'});
} else {
    print &plSSMsgCatGet(SSMSG00031, "\n\nInvalid CGI method!\n");
    exit(0);
}

if (!(open (DOH,"> ./ssm_tmp/tmpul$$")))
{
    print STDOUT sprintf(&plSSMsgCatGet(SSMSG00033, "\n\nError opening %s for writing: %s\n"), "./ssm_tmp/tmpul$$", "$!");
    exit;
}

if (!(print DOH $buffer))
{
   print STDOUT &plSSMsgCatGet(SSMSG00034, "\n\n<h3>ERROR</h3>\n\
<hr>\n\
An error occurred uploading your file.  This\n\
could be due to insufficient disk space.");

   close (DOH);
   unlink ("./ssm_tmp/tmpul$$");

   exit;

}

close DOH;

$ENV{'CONTENT_TYPE'} =~ /boundary=(.*$)/;
$boundary=$1;

if (!(open (DOH,"./ssm_tmp/tmpul$$")))
{
    print STDOUT sprintf(&plSSMsgCatGet(SSMSG00035, "\n\nError opening %s for reading: %s\n"),
	"./ssm_tmp/tmpul$$", "$!");
    exit;
}

local($isfname) = 0;
local($isuserfile) = 0;

PIECE:
while (<DOH>) {
  if (/rnum/) { 
      <DOH>; $rnum=<DOH>; 
      chop $rnum; chop $rnum;
    }
  if (/fname/) { 
      <DOH>; $fname=<DOH>; 
      chop $fname; chop $fname;
      if ($fname) { $isfname = 1;}
    }
  if (/userfile/) { 
      $origpath = $_;
      $origpath =~ s/.*filename=\"(.*)\".*/$1/;
      <DOH>; <DOH>; 
      while ($mhi = <DOH>) {
	  if ($mhi =~ /$boundary/) 
	  { next PIECE; }
	  $data .= $mhi; 
      }
  }
}

chop $data; chop $data; 

close (DOH);
unlink ("./ssm_tmp/tmpul$$");

$size = length($origpath);

if ($size < 2) {
    print STDOUT &plSSMsgCatGet(SSMSGFileToUpld, "\n\n<h3>Error</h3>
<hr>
You forgot to choose a file to upload.<br>
Please press <a href=\"javascript:history.go(-1)\">here</a> to return and
select a file for uploading.<hr>
");
    exit;
}

$origname = $origpath;

$origname =~ s/.*\\([^\\]*)/$1/;
$size=length($origname);

if ($size < 2) {
    $origname = $origpath;
    $origname =~ s/.*\/([^\/]*)/$1/;
    $size = length($origname);
}



if (length($size < 2))
{
    print STDOUT &plSSMsgCatGet(SSMSGFileToUpld, "\n\n<h3>Error</h3>
<hr>
You forgot to choose a file to upload.<br>
Please press <a href=\"javascript:history.go(-1)\">here</a> to return and
select a file for uploading.<hr>
");
    exit;
}

if ($isfname != 1) {$fname = $origname;}

$fname =~ y/a-zA-Z0-9.-_//cds;

if (!(open (NEWFILE,">./ssm_tmp/$fname")))
{
    printf STDOUT &plSSMsgCatGet(SSMSG00209, "\n\nError opening ./ssm_tmp/%s for writing: %s\n"), $fname, $!;
    print STDOUT &plSSMsgCatGet(SSMSG00210, "Press <a href=\"javascript:history.go(-1)\">here</a> to return.");
    exit;
			 
    exit;
}

if (!(print NEWFILE $data))
{
   print STDOUT &plSSMsgCatGet(SSMSG00034, "\n\n<h3>ERROR</h3>\n\
<hr>\n\
An error occurred uploading your file.  This\n\
could be due to insufficient disk space.");
   print STDOUT &plSSMsgCatGet(SSMSG00210, "Press <a href=\"javascript:history.go(-1)\">here</a> to\n\
return.");

   close (NEWFILE);
   unlink ("./ssm_tmp/$fname");

   exit;

}

close (NEWFILE);
if (-z "./ssm_tmp/$fname")
{
    print STDOUT &plSSMsgCatGet(SSMSGEmptyFile, "\n\n<h3>Error</h3>\n
<hr>
You have attempted to upload an empty or non-existent file.<br>\n");

    print STDOUT &plSSMsgCatGet(SSMSG00210, "Press <a href=\"javascript:history.go(-1)\">here</a> to return.");
    exit;
}


$Host = $ENV{'SERVER_NAME'};
$Path = $ENV{'SCRIPT_NAME'};
$Port = $ENV{'SERVER_PORT'}; 

if ($Port == 80) { $Port = ""; }
else { $Port = ':'.$Port; }

$Path =~ s/^(.*)\/[^\/]*$/$1/;

print "Status: 302 Redirected\n";
print "Location: http://$Host$Port$Path/smarthtml.cgi?fcn_upload_upload=upload&fname=$fname\n\n";


