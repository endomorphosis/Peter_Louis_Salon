sub plMsgCatOpen {

	($msgcat_fname) = @_;

	if ( ! open(CAT, $msgcat_fname) ) {
		return;
	}

	while (<CAT>) {
		($msgcat_msgid,$msgcat_msgtxt) = split(/%%%/); 
		($msgcat_msgid,$msgcat_msgtxt) =~ s/__NL__/\n/g;
		$msgcat_msgarr{$msgcat_msgid} = $msgcat_msgtxt; 
	}

}

sub plMsgCatGet {

	($msgcat_msgid) = @_[0];
	($msgcat_defmsg) = @_[1];
	$msgcat_msgtxt = $msgcat_msgarr{$msgcat_msgid};
	if ($msgcat_msgtxt eq "") {
		return($msgcat_defmsg);
	}
	return($msgcat_msgtxt);
}

1;
