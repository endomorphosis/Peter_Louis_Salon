#!/usr/bin/perl







print <<EndBlock01;
Content-type: text/html


<html>
<head>
<title>
INFO.CGI
</title>
</head>
<body bgcolor = "#ffffff">
<center>
<h2>
Display info: what do I know?
</h2>
</center>
<h3>Environment:</h3>
EndBlock01

    while(($ekey, $eval) = each(%ENV)){
	print "$ekey : $eval<br>\n";
	if ($ekey eq 'PATH') {
	    $path = $eval;
	}
    }
print <<EndOtherBlock;
<hr>
<h3>Other Interesting Info:</h3>
<tt>
EndOtherBlock
    $uname = `uname -a`;
print "Uname info : $uname<br>\n";
$mydir = `ls -ld .`;
print "Working directory : $mydir<br>\n";
$mypath = `pwd`;
print "Path to current directory : $mypath<br>\n";
$myid = `id`;
print "Ids : $myid<br>\n";

$tarfound = $perlfound = $smfound = 0;
@paths = split(/:/, $path);
foreach (@paths) {
    if ( -e "$_/tar" ) {
	$tarfound = 1;
	print "Tar found : $_/tar<br>\n";
    }
    if ( -e "$_/perl" ) {
	$perlfound = 1;
	print "Perl found : $_/perl<br>\n";
        $perlversion = `$_/perl -v`;
        print "Perl version : $perlversion<br>\n";
    }
    if ( -e "$_/sendmail" ) {
	$smfound = 1;
	print "Sendmail found : $_/sendmail<br>\n";
    }
}
if ($tarfound == 0) {
    print "Could be TROUBLE: tar not found on server PATH<br>\n";
}
if ($perlfound == 0) {
    print "Could be TROUBLE: perl not found on server PATH<br>\n";
}
if ($smfound == 0) {
    print "Could be TROUBLE: sendmail not found on server PATH<br>\n";
}
print <<EndBlock02;
</tt>
</body>
</html>
EndBlock02

