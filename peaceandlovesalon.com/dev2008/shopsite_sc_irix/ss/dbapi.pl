



















$SIZEOF_LONG = length(pack("l", 12345));
$SIZEOF_INT = length(pack("i", 1234));

$VER_STR = "IDB 2.1"; 
$OLD_VER_STR = "IDB 2.0"; 

$FTYPE{128} = "_INT";
$FTYPE{129} = "_LONG";
$FTYPE{130} = "_FLOAT";
$FTYPE{131} = "_CHAR";
$FTYPE{12} = "IMGLIBIMAGE";
$FTYPE{9} = "RELATIONAL";

$FTYPE{132} = "_ITEMS";


$SIZEOF_RDT = 8;
$RDT_TEMPLATE = "cxxxl";
$RDT_OK = 1;
$RDT_DELETED = -1;

$SIZEOF_FLT = $SIZEOF_LONG * 3;
$FLT_TEMPLATE = "lll";

$ITEM_NAME = "_items";





sub throw {
	die( @_ );
}



sub InitDatabase {
	local( $fn ) = @_;

	local $full_path = "$DATA_DIR/$fn.db"; 

	if(defined($OPEN_DATABASES{"ref_$fn"}))
	{
		$OPEN_DATABASES{"ref_$fn"}++;

		return %{$OPEN_DATABASES{"glob_$fn"}};
	}

	local( $db_ver, $headerinfo );  

	local $hash_name = "DBS_CACHE_$fn";

	local $dbs_handle = "DB_HANDLE_$full_path";

	${$hash_name}{'handle'} = $dbs_handle;

	$OPEN_DATABASES{"ref_$fn"} = 1;

	if(! open($dbs_handle, "< $full_path" )) {
		&throw("Can't open database $full_path");
		return();
	}

	binmode($dbs_handle);

	seek( $dbs_handle, 0, 0 );
	read( $dbs_handle, $db_ver, length($VER_STR) );

	if($db_ver ne $VER_STR && $db_ver ne $OLD_VER_STR) {
	    print STDOUT "<h3>dbapi: no match for $db_ver</h3>\n";
	    &throw("$full_path: Incorrect database version ");
	    return();
	}


	${$hash_name}{ "name" } = $fn;
	${$hash_name}{ "dir" } = $DATA_DIR;

if ($db_ver eq $VER_STR) {
	read( $dbs_handle, $junk, 1);	
}
	read( $dbs_handle, $headerinfo, $SIZEOF_LONG * 6);	

	(${$hash_name}{"numfields"}, ${$hash_name}{"numrecs"},
	 ${$hash_name}{"numnondel"}, ${$hash_name}{"rdtloc"},
	 ${$hash_name}{"fsmloc"},    ${$hash_name}{"fdtloc"} )
		= unpack( "l6", $headerinfo );

	&LoadFDT( *{$hash_name} );

	$OPEN_DATABASES{"glob_$fn"} = *{$hash_name};

	return %{$hash_name};
}



sub CloseDatabase {
	local( *dbs ) = @_;
	local( $name );

	$name = $dbs{'name'};

	if( (!defined($OPEN_DATABASES{"ref_$name"})) || 
		($OPEN_DATABASES{"ref_$name"} <=0 ))
	{
	   &throw("Attempt to close database $name.  (It wasn't open!)");
	}

	$OPEN_DATABASES{"ref_$name"}--;
}



sub LoadFDT {

	local(*dbs) = @_;
	local( $i ) = 0;
	local( $fields ) = $dbs{"numfields"};
	local( $handle ) = $dbs{"handle"};
	local( $bytes ) = 0;

	unless( defined($dbs{"fdtloc"}) && 
		defined($dbs{"numfields"}) &&
		defined($dbs{"handle"}) )
	{
		&throw("LoadFDT: Bad database structure passed!");
		return;
	}
 
	seek($handle, $dbs{"fdtloc"}, 0);

	for($i=0; $i< $fields ; $i++)
	{
		local($ss);
		local($pp);
		local($ns);
		read($handle, $bytes , $SIZEOF_LONG);
		$dbs{"f",$i,"type"} = unpack("l" ,$bytes);

		read( $handle, $bytes, $SIZEOF_LONG); 

		$bytes = unpack("l", $bytes);
		if($bytes>0) {
			local($fname);

			read($handle, $fname, $bytes);
			chop $fname;

			$ss = $fname;
			$pp = index($ss, "_i18n");
			if ($pp > -1) {
				$ns = substr($ss, 0, $pp);
				$ss = $ns;
			}

			$fname = $ss;
			$dbs{"f",$i,"name"} = $fname;

			$dbs{"fnum", $fname} = $i;
		}
			
		read( $handle, $bytes, $SIZEOF_LONG); 
		$bytes = unpack("l", $bytes);
		if($bytes>0) { 
			read($handle, $fname, $bytes);
			chop $fname;
			$dbs{"f",$i,"def"} = $fname;
		}

		read( $handle, $bytes, $SIZEOF_LONG);
		$bytes = unpack("l", $bytes);
		if($bytes>0) {
			read($handle, $fname, $bytes);
			chop $fname;
			$dbs{"f",$i,"m1"} = $fname;
		}

		read( $handle, $bytes, $SIZEOF_LONG);
		$bytes = unpack("l", $bytes);
		if($bytes>0) {
			read($handle, $fname, $bytes);
			chop $fname;
			$dbs{"f",$i,"m2"} = $fname;
		}

		read( $handle, $bytes, $SIZEOF_LONG); 
		$bytes = unpack("l", $bytes);
		if($bytes>0) {
			read($handle, $fname, $bytes);
			chop $fname;
			$dbs{"f",$i,"m3"} = $fname;
		}

		if( defined( $DEBUG ) ) {
		
			print( "Field $i type: $dbs{'f',$i,'type'}\n");
			print( "\tName: $dbs{'f',$i,'name'}\n");
			print( "\tDefault: $dbs{'f',$i,'def'}\n");
			print( "\tMore info 1: $dbs{'f',$i,'m1'}\n");
			print( "\tMore info 2: $dbs{'f',$i,'m2'}\n");
			print( "\tMore info 3: $dbs{'f',$i,'m3'}\n");
		}
	}
}


sub GetRecord {
	local( *dbs, $recnum ) = @_;
	local( %rec );
	local( $handle ) = $dbs{'handle'};
	local( $data );
	local( $status, $pos );
	local( $i, $foo );
	local( $numfields ) = $dbs{'numfields'};
	local( $recordname ) = $dbs{'name'}.$;.$recnum;


	return(())
		if( (!defined(%dbs)) || (!defined($recnum)) || 
		 ($recnum<0) || ($recnum>=$dbs{'numrecs'}) );


	    

	    seek( $handle, $dbs{'rdtloc'} + ($recnum*$SIZEOF_RDT), 0 );
	    read( $handle, $data, $SIZEOF_RDT );
	    ($status, $pos) = unpack($RDT_TEMPLATE, $data);
	    

	    ($status==$RDT_OK)  ||  return(());
	    
	    seek( $handle, $pos, 0 );
	    
	    $rec{'number'} = $recnum;



	    read( $handle, $data, $SIZEOF_FLT * $numfields );
	    for( $i = 0; $i < $numfields ; $i++ )
	    {
		($rec{$i,"loc"}, $rec{$i,"size"}, $foo) =
                   unpack( $FLT_TEMPLATE, 
			substr($data, ($i * $SIZEOF_FLT) + $[, $SIZEOF_FLT));
            }




	return %rec;
}

sub GetField {
	local( *dbs, *rec, $field ) = @_;
	local( $num );


	$field =~ /^_/  && return undef;

	$num = $dbs{"fnum",$field};


	defined($num) ? &GetFieldByNumber(*dbs,$num,*rec) : undef;
}	


sub GetFieldNum {
	local(*dbs, $field) = @_;


	return $dbs{"fnum",$field};
}


sub GetFieldByNumber {
	local( *dbs, $field, *rec ) = @_;
	local( $data );
	local( $handle ) = $dbs{'handle'};
	local( $type );

	defined($rec{$field,"data"}) && return $rec{$field,"data"};



	unless(defined( $rec{$field,"loc"} ))
	{
		defined($DEBUG) && print("Bad record!\n");
		return undef;
	}

	if( !seek( $handle, $rec{$field,"loc"}, 0)) { 
		&throw("Error seeking to $rec{$field,'loc'} while reading field $field of $db{'dir'}/$db{'name'}.db\n");
		return undef;
	}
	read( $handle, $data, $rec{$field,'size'} );
		
	$type = $dbs{'f', $field, 'type'};


	return &UnpackTypes( *dbs, *rec, $field, $type, $data);
}


sub GetType {
	local( *dbs, $field ) = @_;

	(defined( $field) && defined(%dbs)) || return undef;

	return $FTYPE{$dbs{'f', $field, 'type'}};
}

sub UnpackTypes {
	local(*dbs, *rec, $field, $type, $data ) = @_;
	local( $typename );


	$typename = $rec{$field,'type'} = $FTYPE{$type};


	if ($typename eq '_ITEMS' && $OS_TYPE eq 'SOLARIS') {      
            local(@elements, $total, $i, $dbname, $rnum);
            @elements = split("\t", $data);
            $total = shift(@elements);
            $rec{$field,"data"} = $total;
 
            for( $i=0; $i < $total; $i++ ) {
                $dbname = shift(@elements);
                $rnum = shift(@elements);
                $rec{$field,'data',$i,'db'} = $dbname;
                $rec{$field,'data',$i,'rec'} = $rnum;
            }
            return $rec{$field,'data'};
        }
	elsif ($typename eq '_ITEMS') {
		local($size, $items, $i, $index);
		$index = 0;

		$rec{$field,"data"} = unpack('l',$data);
		$items = $rec{$field,"data"};
		$index += $SIZEOF_LONG;

		for( $i=0; $i< $items; $i++ ) {

		   $size = ord(substr($data,$index,1));
		   $index += 1;

		   if($size > 0) {
		      $rec{$field,'data',$i,'db'}=substr($data,$index,$size-1);
		      $index += $size;
		   }

		   $rec{$field,'data',$i,'rec'}=
			unpack('l',substr($data,$index,$SIZEOF_LONG));
		   $index += $SIZEOF_LONG;

		   ($index > length($data)) && &throw("Corrupted item field!\n");
		}
		return $rec{$field,'data'};
	}
	elsif ($typename eq 'IMGLIBIMAGE') {


		$rec{$field,"data"} = &grokimage(*dbs,*rec,$field,$data);
		return $rec{$field,'data'};
	}
	elsif($typename eq 'RELATIONAL') {
		$rec{$field,"data"} = $data;
		$rec{$field,"data","db"} = $dbs{"f",$field,"m3"};

		return $rec{$field,'data'};
	}
	elsif( $typename eq '_INT' ){
		$rec{$field,"data"} = unpack('i',$data);
		return $rec{$field,'data'};
	}
	elsif ( $typename eq '_LONG' ) {
		$rec{$field,"data"} = unpack('l',$data);
		return $rec{$field,'data'};
	}
	elsif ( $typename eq '_FLOAT' ) {
		$rec{$field,"data"} = unpack('d',$data);
		return $rec{$field,'data'};
	}
	elsif ( $typename eq '_CHAR') {


		$rec{$field,"data"} = unpack('c',$data);

		return $rec{$field,'data'};
	}
	else {


		$rec{$field,'type'} = "_NORMAL";
		$rec{$field,"data"} = $data;
		chop( $rec{$field,"data"} );

		return $rec{$field,'data'};
	}

	&throw("Impossible state in UnpackTypes!");
}

sub grokimage {
	local(*dbs,*rec,$field,$data) = @_;

	local( $i );		

	local( $imagename );
	local( $idstring );


	local( $imgurl ); 	
	local( $imgpath );	

	local( $extrainfo ); 	


	$imagename = $data;
	chop $imagename;

	if ($imagename =~ /http:\/\/.*/) {
            return "src=\"$imagename\" hspace=3 vspace=3";
        }

        if ($imagename eq "none") {
	    return "none";
        }

	$idstring = $dbs{'f',$field,'m3'};





	



	$imgpath = $OUTPUT_DIR."/media";
	$imgurl = $OUTPUT_URL."/media";

	$extrainfo = &grokinf("$imgpath/.inf.$imagename");
	
	if ($extrainfo)
	{
	    return $extrainfo;
	}
	else
	{
	    return "src=\"$imgurl/$imagename\" hspace=3 vspace=3";
	}
}

sub grokinf {
	local( $fn ) = @_;


	local( $data );
	local( $type, $width, $height, $bordersize,
		$align, $ismap, $altsize, $lowsrcsize, $moresize,
		$alt, $lowsrc, $more );




	if (!open(GROKINF_FH, $fn)) {
            return undef;
        }


	$returnval = <GROKINF_FH>;

	$returnval =~ s/\<\s*img(.*)\>/$1/;

	close(GROKINF_FH);

	if (!$returnval)
	{
	    print STDOUT &plSSMsgCatGet(SSMSGNoInfo,"\n<br>no .inf info\n");
	}

	chop $returnval;
	return $returnval;




}		




sub GetItem {
	local( *db, *rec, $i ) = @_;
	local( $field );
	local( $numitems );

	$field = &GetFieldNum( *db, $ITEM_NAME );
	$field || return undef;


	$numitems = &GetFieldByNumber( *db, $field, *rec );

	defined($i) || return $numitems;
	if (($i >= $numitems) || ($i < 0)) {
          return undef;
        }

	return ($rec{$field,'data',$i,'db'}, $rec{$field,'data',$i,'rec'});
}

1;




