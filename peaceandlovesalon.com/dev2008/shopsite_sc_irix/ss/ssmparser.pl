





require "template.pl";


sub FindTag {
  local($TagName, $InString) = @_;
  local($ReturnValue);
  
  ($ReturnValue = $InString) =~ s/<\s*($TagName)[^>]*>/$1/i;
  return $ReturnValue;
}

sub GetQualifier {
  local($QualifierName, $InString) = @_;
  local($ReturnValue);
  
  $ReturnValue = $InString;
  if($ReturnValue =~  s/\<.*$QualifierName=((\"([^\"]*)\")|(\w*)).*\>/$1/i)
    {
      $ReturnValue =~ s/\"?([^\"]*)\"?/$1/i;
    }
  else
    {
      $ReturnValue = "";
    }
  return $ReturnValue;
}


sub SmartParser {
    local($IN_NAME, $OUT_NAME) = @_; 

    printf STDOUT &plSSMsgCatGet(SSMSGGenPg,"\n<p><b>Generating Page:[%s]</b>"),
	$IN_NAME;
    $OUT_NAME = "> ".$OUT_NAME;

    if (!open(SRC, $IN_NAME))
    {
	printf STDOUT &plSSMsgCatGet(SSMSGNoOpenSrcFile,"\n<br><b>ERROR!</b>\n<br>Unable to open source file:[%s]"),$IN_NAME;
	die &plSSMsgCatGet(SSMSGDieOpenFile,"error opening in file: $!\n");
    }

    if (!open(DEST, $OUT_NAME))
    {
        printf STDOUT &plSSMsgCatGet(SSMSGNoOpenOutFile,"\n<br><b>ERROR!</b>\n<br>Unable to open destination file:[%s]"),$OUT_NAME;
        die &plSSMsgCatGet(SSMSGDieOpenOutFile,"error opening out file: $!\n");
    }
    
    local($orderlink) = "\<a href=$CGI_URL/order.cgi?storeid=$STOREID&dbname=DBNAME&itemnum=ITEMNUM&function=add\>";
    local($colink) = "\<a href=$CGI_URL/order.cgi?storeid=$STOREID&function=show\>";
    
    local($CURALIGN) = "left";
    
    
    while(<SRC>) {
	select(DEST);
	
	if (/<\s*uheader\s*>/gi)
	{
	    &EmitFile("$DATA_DIR/uheader.dat");
	}
	
	if (/<\s*ufooter\s*>/gi)
	{
	    &EmitFile("$DATA_DIR/ufooter.dat");
	}
	
	
	if (/<\s*curdb=[^>]*>/gi)
	{
	    local($dbname) = $&;
	    
	    if ($dbname =~ s/\<\s*curdb=\"?(\w*)\"?\s*\>/$1/gi)
	    {
		$curdb = $dbname;
	    }
	    s/<\s*curdb=[^>]*>//gi;
	}
	
	
	if (/<\s*currec=[^>]*>/gi)
	{
	    local($recname) = $&;
	    
	    if ($recname =~ s/\<\s*currec=\"?(\w*)\"?\s*\>/$1/i)
	    {
		$currec = $recname;
	    }
	    s/<\s*currec=[^>]*>//gi;
	}


        if (/<\s*recname=[^>]*>/gi)
        {
            local($recname) = $&;

            if ($recname =~ s/\<\s*recname=\"(.*)\"\s*\>/$1/i)
            {
		if ($curdb eq "pages")
                {
		    $currec = $PageList{$recname}; 
		}
		elsif ($curdb eq "products")
		{
		    $currec = $ProductsList{$recname};
		}

            }
            s/<\s*currec=[^>]*>//gi;
        }
	
	if (/<\s*order[^>]*>/i)
	{
	    local($ordermatch) = $&;
	    
	    
	    local($dbname) = $ordermatch;
	    ($dbname =~ s/\<.*db=\"?(\w*)\"?.*\>/$1/i) || ($dbname = $curdb);
	    
	    
	  local($recnum) = $ordermatch;
	  if(!($recnum =  &GetQualifier("rnum", $ordermatch)))
            {
		if(!($recnum =  &GetQualifier("record", $ordermatch)))
		{
		    $recnum = $currec;
		}
		else
		{
		    if ($dbname eq "pages")
                    {
			$recnum = $PageList{$recnum};
		    }
		    elsif ($dbname eq "products")
                    {
			$recnum = $ProductsList{$recnum};
		    }
		    else 
		    {
			$recnum = $currec;
		    }
		}
	    }
	    $orderlink2 = $orderlink;
	    $orderlink2 =~ s/DBNAME/$dbname/;
	    $orderlink2 =~ s/ITEMNUM/$recnum/;

	    s/<\s*order[^>]*>/$orderlink2/gi;
	}
	    
	s/<\s*\/order\s*>/<\/a>/gi;
	s/<\s*checkout\s*>/$colink/gi;
	s/<\s*\/checkout\s*>/<\/a>/gi;
	
	
	
	
	if (/\<\s*field[^>]*\>/i)
	{
	    local($field) = $&;     
	    
	    
	    local($dbname) = $field;
	    ($dbname =~ s/\<.*db=\"?(\w*)\"?.*\>/$1/i) || ($dbname = $curdb);   
	    
	    local($recnum) = $field;
	    if(!($recnum = &GetQualifier("rnum",$field)))
            {
                local($arecnum) = $field;
		if(!($arecnum =  &GetQualifier("record", $field)))
                {
                    $recnum = $currec;
                }
                else
                {
                    if ($dbname eq "pages")
                    {
                        $recnum = $PageList{$arecnum};
                    }
                    elsif ($dbname eq "products")
                    {
                        $recnum = $ProductsList{$arecnum};
                    }
		    else
		    {
			$recnum = $currec;
		    }
		}
	    }
	    
	    local($fname) = $field;
	    (($fname = GetQualifier("name",$field))) || ($fname = undef);
	    
	    if ($dbname && ($recnum || $recnum eq "0") && $fname) 
	    {
		local(%db) = &InitDatabase($dbname);
		local(%rec) = &GetRecord(*db,$recnum);	
		
		$field = &GetField(*db,*rec,$fname);
		
	    }
	    else
	    {
		$field = "";
	    }
	    
	    
	    s/\<\s*field[^>]*\>/$field/gi;
	    
	}
	    
	    
	if (/\<\s*curalign=[^>]*\>/i)
	{
	    local($alignmatch) = $&;
		
	    if($alignmatch =~ s/\<\s*curalign=\"?(\w*)\"?\s*\>/$1/i)
	    {
		$CURALIGN = $alignmatch;
	    }
	    
	    s/\<\s*curalign[^>]*\>/ /gi;
	}
	
	if (/<\s*product[^>]*>/i)
	{
	    local($tempalign);
	    local($product) = $&;
	    
	    
	    local($record) = $product;
	    if (!($record = GetQualifier("rnum",$product)))
            {
	        if (!($record = GetQualifier("record",$product)))
                {
                    $record = $currec;
                }
                else
                {
		    $record = $ProductsList{$record};
		}
	    }

	    local($align) = $product;
	    if ($align =~ s/<.*align=\"?(\w*)\"?.*>/$1/i)
	    {
		$tempalign = $CURALIGN;
		
		$CURALIGN = $align;
	    }
	    
	    if (($record) || ($record eq "0"))
	    {
		local(%db) = &InitDatabase("products");
		local(%rec) = &GetRecord(*db, $record);
		$product = &Emit(*db, *rec);
		
		chop($product);
	    }
	    else
	    {
		$product = "";
	    }
	    
	    s/<\s*product[^>]*>/$product/i;
	    
	    if (defined($tempalign))
	    {
		$CURALIGN = $tempalign;
	    }
	}
	
	
	if (/<\s*pagelink[^>]*>/i)
	{
	    local($tempalign);
	    local($pagelink) = $&;
	    
	    
	    local($record) = $pagelink;
	    if(!($record =  &GetQualifier("rnum", $pagelink)))
            {
		if(!($record =  &GetQualifier("record", $pagelink)))
                {
                    $record = $currec;
                }
                else
                {
		    $record = $PageList{$record};
		}
	    }
    
	    local($align) = $pagelink;
	    if ($align =~ s/<.*align=\"?(\w*)\"?.*>/$1/i)
	    {
		$tempalign = $CURALIGN;
		$CURALIGN = $align;
	    }
	    
	    if (($record) || ($record eq "0"))
	    {
		local(%db) = &InitDatabase("pages");
		local(%rec) = &GetRecord(*db, $record);
		$pagelink = &Emit(*db, *rec, "LINK");

		chop($pagelink);
	    }
	    
	    s/<\s*pagelink[^>]*>/$pagelink/i;
	    
	    if (defined($tempalign))
	    {
		$CURALIGN = $tempalign;
	    }
	    
	}
	print;
    }
    return 1;

}

sub BadPrintError {

    print STDOUT &plSSMsgCatGet(SSMSGDiskFull,"Bad write error.  Your disk may be full.");
    exit;

}

1;


