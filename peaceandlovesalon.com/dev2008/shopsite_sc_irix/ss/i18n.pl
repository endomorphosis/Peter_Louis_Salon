
require "msgcat.pl";

($USERCLASS_MERCHANT) = 0;
($USERCLASS_BUYER) = 1;

sub plSSMsgCatOpen
{
	my($datadir) = &GetDataDir();
	my($nlsdir) = &GetNlsDir();
	my($locale) = &GetMerchantLocale();


	if ( -r "$nlsdir/$locale/ShopSiteMsgPl.tct" )
		{
		&plMsgCatOpen("$nlsdir/$locale/ShopSiteMsgPl.tct");
		}
	elsif ( -r "$nlsdir/ShopSiteMsgPl.tct" )
		{
		&plMsgCatOpen("$nlsdir/$locale/ShopSiteMsgPl.tct");
		}
}

sub plSSMsgCatGet
{
	($msgcat_msgid) = @_[0];
	($msgcat_defmsg) = @_[1];
	$rmsg = &plMsgCatGet($msgcat_msgid, $msgcat_defmsg);
	return($rmsg);
}


sub GetMerchantLocale()
{
    my($bodir, $slash);
    my($aapath, $MerchantLocale, $locale);
    my($suffix, $aafile, $key, @Fields);
    
    $suffix = ".aa";
    $aafile = $ENV{"REMOTE_USER"};
    $aafile = "$aafile$suffix";

    $bodir = &GetBoDir();
    $slash = "/";
    $aapath = "$bodir$slash$aafile";
    $key = "MerchantLocale";
    $locale = &GetAAToken($aapath, $key);

    @Fields = split(/\|/ ,$locale);
    $MerchantLocale = $Fields[0];

    return $MerchantLocale;
    
}

sub GetDataDir
{
  my($suffix, $aafile, $key, $nlsdir);

  $suffix = ".aa";
  $aafile = $ENV{"REMOTE_USER"};
  $aafile = "$aafile$suffix";
  $key = "datadir";
  $datadir = &GetAAToken($aafile, $key);
  return($datadir);
}

sub GetBoDir
{
  my($suffix, $aafile, $key, $bodir);

  $suffix = ".aa";
  $aafile = $ENV{"REMOTE_USER"};
  $aafile = "$aafile$suffix";
  $key = "bo_dir";
  $bodir = &GetAAToken($aafile, $key);
  return($bodir);
}

sub GetNlsDir
{
  my($suffix, $aafile, $key, $bodir, $nlsdir);

  $suffix = ".aa";
  $aafile = $ENV{"REMOTE_USER"};
  $aafile = "$aafile$suffix";
  $key = "bo_dir";
  $nlsdir = sprintf "%s/nls", &GetAAToken($aafile, $key);
  return($nlsdir);
}


sub GetAAToken
{
  my($aafile, $key) = @_;
  my($value, $fullpath, $cwd, $script_filename, $script_name, $document_root, $field);


  $value = $AArrays{$aafile, $key};
  if ($value) {return $value;}

  use Cwd;
  $cwd = getcwd();
  $cwd .= "/" unless $cwd =~ /\/$/;
  $ENV{"SCRIPT_FILENAME"} =~ /^(.*)\/[^\/]*$/;
  $script_filename = $1;
  $ENV{"SCRIPT_NAME"} =~ /^(.*)\/[^\/]*$/;
  $script_name = $1;
  $document_root = $ENV{"DOCUMENT_ROOT"};
  if ($aafile =~ /\//)
    { $fullpath = $aafile; }
  elsif ( -r $aafile )
    { $fullpath = $aafile; }
  elsif ( -r "./$aafile" )
    { $fullpath = "./$aafile"; }
  elsif ( -r "$cwd$aafile" )
    { $fullpath = "$cwd$aafile"; }
  elsif ( -r "$script_filename/$aafile" )
    { $fullpath = "$script_filename/$aafile"; }
  elsif ( -r "$script_name/$aafile" )
    { $fullpath = "$script_name/$aafile"; }
  elsif ( -r "$document_root$script_name/$aafile" )
    { $fullpath = "$document_root$script_name/$aafile"; chdir $fullpath;}
  open AA, $fullpath;
  while (<AA>)
    {
    ($field,$value) = split /: /;
    chomp($value);
    $AArrays{$aafile, $field} = $value;
    }
  close AA;
  chdir $cwd;
  return $AArrays{$aafile, $key};
}

1;
