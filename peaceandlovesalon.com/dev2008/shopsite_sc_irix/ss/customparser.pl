





require "template.pl";



sub CustomParser {
    local(*curdb, *currec) = @_; 
    local($custom_template);
    local($coutput);
    local($filename) = &Field('File name');
    local($num) = $CUR_REC{'number'};
    local($moreinfo) = "";
    local($doingmoreinfo) = 0;


    $custom_template = &GetTemplate(*curdb,*currec);
    $coutput = $custom_template;
    $moreoutput = "";

    if (($TYPE eq "LINK") && ($db{"name"} eq "pages")) { 
	$coutput =~ s/.*\[--\s*DEFINE\s*LINK_TO_PAGE\s*--\](.*?)\[--\s*END_DEFINE.*?--\].*/$1/is;
	
    } elsif ($TYPE eq "PAGE") { 
	if (!($filename =~ /\S+/))
	{
	    $filename = "page".$num.".html";
	}

	if (!(-e "$OUTPUT_DIR/$filename") ||
	    (!(-M "$OUTPUT_DIR/$filename" <= 0) &&
	     (&IsIn($num, "pages"))
	     )
	    )
	{

	    $| = 1;
	    
	    $message = sprintf(&plSSMsgCatGet(SSMSGErrOpeningDirFile, "Error opening %s/%s"), $OUTPUT_DIR, $filename);
	    &NewFile("$OUTPUT_DIR/$filename") ||
		die $message;

	} else {
	    return;
	}
        $coutput =~ s/.*\[--\s*DEFINE\s*PAGE\s*--\](.*?)\[--\s*END_DEFINE.*?--\].*/$1/is;
    } else { #It's a Product
	if (!($filename =~ /\S+/))
        {
            $filename = "product".$num.".html";
        }

	$moreinfo = &Field('Display more information page?');
	if ($moreinfo eq "checked") {
	    $moreoutput = $coutput;
	    $moreoutput =~ s/.*\[--\s*DEFINE\s*MORE_INFO_PAGE\s*--\](.*?)\[--\s*END_DEFINE.*?--\].*/$1/is;
	}
	    $coutput =~ s/.*\[--\s*DEFINE\s*PRODUCT\s*--\](.*?)\[--\s*END_DEFINE.*?--\].*/$1/is;
	
    }



    while ($coutput) {
	local($newline);
	$coutput =~ s/([^\n]*?\n)//;
	$newline = $1;

	while ($newline =~ /\[--\s*(.*?)--\]/i)
	{
	    $toreplace = "";
	    $tag = $1;

	    $toreplace = &FindMatch($tag);

            if ($toreplace =~ /IF\s+(.*)\s*/i) {
                local($condition) = $1;
                local($ifloop);
		
                $ifloop = &GetIFCode($condition, $coutput);
                $coutput =~ s/.*?\n\s*\[--\s*END_IF\s*--\]/$ifloop/is;

                $toreplace = "";
            }


	    if (($toreplace =~ /LOOP ITEMS/i) || 
		($toreplace =~ /LOOP PRODUCTS/i) ||
		($toreplace =~ /LOOP LINKS/i) )
	    {
		
		local( $i, @ITEM);
		local($itemloop,$newoutput);
		local($loopkind) = $toreplace;
		$newoutput = "";
		
		$coutput =~ s/(.*?\n)\s*\[--\s*END_LOOP.*?--\]//is;
		$itemloop = $1;
		
		while ($itemloop =~ /\[--\s*(.*?)\s*--\]/i)
		{
		    local($subtoreplace,$subtag);
		    $subtoreplace = "";
		    $subtag = $1;

		    $subtoreplace = &FindMatch($subtag);

		    if ($subtoreplace =~ /IF\s+(.*)\s*/i) {
			local($condition) = $1;
			local($ifloop, $preifloop);

			$preifloop = $itemloop;
			$preifloop =~ s/\[--.*?--\]\s*\n(.*\n)/$1/is;
			
			$ifloop = &GetIFCode($condition, $preifloop);

			$itemloop =~ s/\[--.*?--\]\s*\n.*?\n\s*\[--\s*END_IF\s*--\]/[--REPLACEME--]$ifloop/is;

			$subtoreplace = "";
		    }
		    
		    $itemloop =~ s/\[--.*?--\]/$subtoreplace/i;
		    
		}

		
		$ITEM[0]=undef; 
		$ITEM[1]=undef;
		&ResetRecord;
		for ($i=0; @ITEM = &NextItem; $i++) {
		    local ($loopline, $thisloop);
		    $thisloop = $itemloop;

		    if (!defined($ITEM[0]) || !defined($ITEM[1])) {last;}
		    
		    &NewDB( $ITEM[0] );
		    &SetRecord( $ITEM[1] );

		    if ($loopkind =~ /LOOP PRODUCTS/i) {
			if ($CUR_DB{'name'} eq "products") {
			    while ($thisloop) {
				$thisloop =~ s/([^\n]*?\n)//;
				$loopline = $1;
				if ($loopline =~ /!PRODUCT!/i) {
				    if (!defined(&Emit( *CUR_DB, 
						       *CUR_REC, 
						       "LINK" )))
				    {
					printf STDOUT &plSSMsgCatGet(SSMSGErrEmittingX,"ERROR EMITTING: %s\n"), 
					$@;
				    }
				} else {
				    print $loopline;
				}
			    }
			}
		    }
		    elsif ($loopkind =~ /LOOP LINKS/i) {
			if ($CUR_DB{'name'} eq "pages") {
			    while ($thisloop) {
                                $thisloop =~ s/([^\n]*?\n)//;
                                $loopline = $1;
                                if ($loopline =~ /!LINK!/i) {
				    if (!defined(&Emit( *CUR_DB, 
						       *CUR_REC, 
						       "LINK" )))
				    {
					printf STDOUT &plSSMsgCatGet(SSMSGErrEmittingX,"ERROR EMITTING: %s\n"), $@;
				    }
				} else {
				    print $loopline;
				}
			    }
			}
		    }
		    elsif ($loopkind =~ /LOOP ITEMS/i) {
			while ($thisloop) {
			    $thisloop =~ s/([^\n]*?\n)//;
			    $loopline = $1;
			    if ($loopline =~ /!ITEM!/i) {
				if (!defined(&Emit( *CUR_DB, 
						   *CUR_REC, 
						   "LINK" )))
				{
				    printf STDOUT &plSSMsgCatGet(SSMSGErrEmittingX,"ERROR EMITTING: %s\n"), $@;
				}
			    } else {
				print $loopline;
			    }
			}
		    }


		    &EndNewDB;
		    $ITEM[0]=undef;
		    $ITEM[1]=undef;
		    
		}
		$toreplace = "";
	    }


	    $newline =~ s/\[--.*?--\]/$toreplace/i;

        }

	print $newline;

	if ((!$coutput) && ($moreoutput)) {
	    if (!(-e "$OUTPUT_DIR/$filename") ||
		(!(-M "$OUTPUT_DIR/$filename" <= 0) &&
		 (&IsIn($num, "products"))
		 )
		) 
	    {
		$message = sprintf(&plSSMsgCatGet(SSMSGErrOpeningDirFile,
						  "Error opening %s/%s"), 
				   $OUTPUT_DIR, $filename);
		&NewFile("$OUTPUT_DIR/$filename") ||
		    die $message;
		$coutput = $moreoutput;
		$doingmoreinfo = 1;
	    }
	}

    } 


    if (($TYPE eq "PAGE")||($doingmoreinfo == 1)) {
	&EndNewFile;
    }
}

sub FindMatch {
    local($pre) = @_;
    local($replace) = "";

    if ($pre =~ /\AHEADER.*/i) {
	open(PH,"$DATA_DIR/uheader.dat");
	while(<PH>) {	
	    s/\000//g;
	    $replace .= $_;
	}
	close PH;
    }
    
    elsif ($pre =~ /\AFOOTER.*/i) {
	open(PH,"$DATA_DIR/ufooter.dat");
	while(<PH>) {
	    s/\000//g;
	    $replace .= $_;
	}
	close PH;
    }

    elsif ($pre =~ /\AIF/i) {
	$replace = $pre;
    }
    
    elsif (( $pre =~ /\A(PAGE)\.(\S+)(.*)/i ) ||
           ( $pre =~ /\A(PRODUCT)\.(\S+)(.*)/i)) {
	local($inDB) = $1;
	local($fldname) = $2;
	local($removeHTML) = $3;

	$replace = &GetFieldValue($inDB,$fldname);
	
	if ($removeHTML =~ /REMOVE_HTML/) {
	    $replace =~ s/<.*?>//s;
	}

	if ($replace eq "") {
	    $replace = "<!-- Empty field [$fldname] -->";
	}

    }
    
    elsif ( $pre =~ /\AOUTPUT_DIRECTORY_URL.*/i ) {
	$replace = $OUTPUT_URL;
    }
    
    elsif ( $pre =~ /\ASEARCH_FORM.*/i ) {
	$replace = "
<center>
<form action=\"$CGI_URL/productsearch.cgi?storeid=$STOREID\" method=post>
<input type=text name=\"search_field\" size=40>
<input type=hidden name=\"storeid\" value=\"$STOREID\">
";

	if ($Search eq "") {$Search = "Search";}
	if (($SearchCk eq "checked") || ($SearchImg eq ""))
	{
	    $replace .= "
<input type=submit value=\"$Search\">
";
	} else {
	    $replace .= "
<input type=image src=\"$OUTPUT_URL/media/$SearchImg\" align=top border=\"0\"
 name=\"$Search\" alt=\"$Search\">
";
	}
	
	$replace .="
</form>
</center>
";
    }
	
    elsif ( $pre =~ /\ASHOPPING_CART_URL.*/i ) {
	$replace = "$CGI_URL/order.cgi?storeid=$STOREID";
	$replace .= "&function=show";
    }


    elsif ( $pre =~ /\A(LOOP\s+\S+).*/i ) {
	$replace = $1;
    }

    elsif (( $pre =~ /\A(ITEM).*/i ) ||
	   ( $pre =~ /\A(LINK).*/i ) ||
	   ( $pre =~ /\A(PRODUCT).*/i )) {
	$replace = "!$1!";
    }

    elsif ( $pre =~ /\ACALL\s*.*\s*/i ) {
	local($command);
	local(@args) = ();
	$pre =~ s/CALL\s+(.*)\s*/$1/i;

	if ($pre =~ /(\S+)\((.*)\)/) {
	    $command = $1;
	    $pre = $2;
	    
	    $command =~ s/.*\/(\S+?)/$1/g;
	    $command = "./".$command;
	    push @args, $command;
	    
	    $pre =~ s/STORE_ID/$STOREID/ig;
	    $pre =~ s/OUTPUT_DIRECTORY_URL/$OUTPUT_URL/ig;
	    $pre =~ s/OUTPUT_DIRECTORY_PATH/$OUTPUT_DIR/ig;
	    $pre =~ s/SHOPPING_CART_URL/$CGI_URL/ig;
	    $pre =~ s/DATA_DIRECTORY_PATH/$DATA_DIR/ig;

	    while ($pre =~ /.*?PRODUCT\.([^\,,^\s]+)/i) {
		local($rp);
		$rp = &GetFieldValue("PRODUCT",$1);
		$pre =~ s/PRODUCT\.(\S+)/$rp/i;
	    }
	    while ($pre =~ /.*?PAGE\.([^\,,^\s]+)/i) {
                local($rp);
                $rp = &GetFieldValue("PAGE",$1);
                $pre =~ s/PAGE\.(\S+?)/$rp/i;
            }

	    while ($pre) {
		if ($pre =~ /(.*?)\s*,\s*(.*)/) {
		    push @args, $1;
		    $pre = $2;
		} else {
		    push @args, $pre;
		    $pre = "";
		}
	    }

	} else {
	    $command = $pre;
	    $command = s/.*\/(.*?)/$1/g;
	    push @args, $command;
	}

	push (@tempstack, select);
	open SAVEOUT, ">&STDOUT";
	open STDOUT, ">callout.$$";
	select STDOUT; $| = 1;
	
	system(@args);

	close STDOUT;
	open STDOUT, ">&SAVEOUT";
	
	select pop(@tempstack);

	open COFILE, "callout.$$";
	while (<COFILE>) {
	    $replace .= $_;
	}
	close COFILE;
	unlink "callout.$$";
    }


    elsif ( $pre =~ /MORE_INFO\.TextColor.*/i) {
	local ($mitmp);
	$mitmp = AAGetToken("$DATA_DIR/moreinfodata.aa",
			    "moreinfo_textcolor");
	if (!(mitmp =~ /\S+/)) {
	    $replace = "#000000";
	} else {
	    $replace = "#".$mitmp;
	}
    }
    elsif ( $pre =~ /MORE_INFO\.BackgroundColor.*/i) {
        local ($mitmp);
        $mitmp = AAGetToken("$DATA_DIR/moreinfodata.aa",
                            "moreinfo_bgcolor");
        if (!(mitmp =~ /\S+/)) {
            $replace = "#FFFFFF";
        } else {
            $replace = "#".$mitmp;
        }
    }
    elsif ( $pre =~ /MORE_INFO\.LinkColor.*/i) {
        local ($mitmp);
        $mitmp = AAGetToken("$DATA_DIR/moreinfodata.aa",
                            "moreinfo_linkcolor");
        if (!(mitmp =~ /\S+/)) {
            $replace = "#0000FF";
        } else {
            $replace = "#".$mitmp;
        }
    }
    elsif ( $pre =~ /MORE_INFO\.VisitedColor.*/i) {
        local ($mitmp);
        $mitmp = AAGetToken("$DATA_DIR/moreinfodata.aa",
                            "moreinfo_vlinkcolor");
        if (!(mitmp =~ /\S+/)) {
            $replace = "#FF0000";
        } else {
            $replace = "#".$mitmp;
        }
    }
    elsif ( $pre =~ /MORE_INFO\.ActiveColor.*/i) {
        local ($mitmp);
        $mitmp = AAGetToken("$DATA_DIR/moreinfodata.aa",
                            "moreinfo_activecolor");
        if (!(mitmp =~ /\S+/)) {
            $replace = "#00FF00";
        } else {
            $replace = "#".$mitmp;
        }
    }
    elsif ( $pre =~ /MORE_INFO\.BackgroundImage.*/i) {
        local ($mitmp);
        $mitmp = AAGetToken("$DATA_DIR/moreinfodata.aa",
                            "moreinfo_bgimage");
	$replace = $mitmp;
    }

    else {
	$replace = "<!-- Unmatched ShopSite tag: [$pre] -->";
    }

    return $replace;
}

sub GetFieldValue{
    local($inDB,$fldname) = @_;
    local($realfldname) = "";
    local($replace) = "";
    local($fType) = "";
    local($formatNumber) = 0;

    if ( $fldname =~ /AddToCartURL/i ) {
        $replace = "$CGI_URL/order.cgi?storeid=$STOREID";
        $replace .= "&dbname=$CUR_DB{'name'}&itemnum=$num&function=add";

	return $replace;
    }
    elsif ($fldname =~ /RecordNumber/i) {
	return $num;
    }
    elsif ($fldname =~ /MoreInfoURL/i) {
	$replace = "$OUTPUT_URL/$filename";
	return $replace;
    }
    
    $realfldname = &FindFieldMatch($inDB,$fldname);

    $replace = &Field($realfldname);
    $fType = &FieldType($realfldname);
    if (($realfldname eq "File name") && !($replace =~ /\S+/)) {
	if ($CUR_CB{'name'} eq "products") {
	    $replace = "product".$num.".html";
	} else {
	    $replace = "page".$num.".html";
	}
    }

    elsif ($realfldname =~ /\ABackground Image\Z/) {
	if ($replace eq "none") {
	    $replace="";
	} else {
	    $replace =~ s/src="(.+?)"\s.*/$1/;
	    $replace =~ s/\s//s;
	}
    }

    elsif ($realfldname =~ /.*graphic.*/i) {
      if ($realfldname !~ /\ADisplay.*/i) {
	if ($replace eq "none") {
	  $replace = "";
	} else {
	  $replace = "<img $replace >";
	}
      }
    }    

    elsif ($realfldname =~ /.*Color.*/) {
      $replace =~ s/\S+\s\((\S+)\)/$1/;
    }
    
    elsif ($realfldname =~ /\ASale On\Z/i ) {
      my($SaleAmount);
      $SaleAmount = &Field("Sale Amount");
      if ($SaleAmount eq "") {
	$replace = $SaleAmount;
      }
    }
    
    elsif ($realfldname =~ /\ASale Amount\Z/i ) {
      my($Price);
      $Price = &Field("Price");
      $replace = &CalcSale($Price, $replace);
      if ($fldname =~ /AltSaleAmount/i ) {
	$formatNumber = 2;
      } else {
	$formatNumber = 1;
      }
    }

    elsif ($realfldname =~ /\APrice\Z/i ) {
      if ($fldname =~ /AltPrice/i ) {
	$formatNumber = 2;
      } else {
	$formatNumber = 1;
      }
    }
    
    if($formatNumber)
      {
	if ($formatNumber eq 1) 
	  {
	    $replace = &pricestring($replace);
	  }
	elsif ($formatNumber eq 2) 
	  {
	    $replace = &apricestring($replace);
	  }
      }
    
    return $replace;

}


sub GetIFCode {
    local($condition,$code) = @_ ;
    local($ifloop) = "";
    local($elseloop) = "";

    $ifloop = $code;
    
    $ifloop =~ s/(.*?\n)\s*\[--\s*END_IF\s*--\].*\n/$1/is;

    if ($ifloop =~ /(.*\n)\s*\[--\s*ELSE\s*--\]\s*\n(.*\n)/is) {
	$ifloop = $1;
	$elseloop = $2;
    }

    $condition = &FindMatch($condition);
    if (($condition eq "") ||  
	($condition =~ /<!-- Empty field \[.*?\] -->/))
    {
	$ifloop = $elseloop;
    }

    return $ifloop
}


sub FindFieldMatch {
    local($inDB,$fld) = @_;

    if ($inDB =~ /\APAGE\Z/i) {
	if ($fld =~ /\AName\Z/i)           { return "Name"; }
	if ($fld =~ /DisplayName/i)        { return "Display Name?"; }
	if ($fld =~ /\AGraphic\Z/i)        { return "Graphic"; }
	if ($fld =~ /DisplayGraphic/i)     { return "Display graphic?"; }
	if ($fld =~ /Text([0-9])/i)        { return "Text $1"; }
	if ($fld =~ /LinkName/i)           { return "Link Name"; }
	if ($fld =~ /LinkGraphic/i)        { return "Link Graphic"; }
	if ($fld =~ /LinkText/i)           { return "Link Text"; }
	if ($fld =~ /TextWrap/i)           { return "Text Wrap"; }
	if ($fld =~ /LayOut/i)             { return "Lay out"; } 
	if ($fld =~ /Columns/i)            { return "Columns"; } 
	if ($fld =~ /DisplayColumnBorders/i) { 
	    return "Display column borders?"; } 
	if ($fld =~ /PageWidth/i)          { return "Page Width"; } 
	if ($fld =~ /DisplayPageHeader/i) { 
	    return "Display Universal Header?"; } 
	if ($fld =~ /DisplayPageFooter/i) { 
	    return "Display Universal Footer?"; } 
	if ($fld =~ /TextColor/i)          { return "Text Color"; } 
        if ($fld =~ /BackgroundColor/i)    { return "Background Color"; }
        if ($fld =~ /LinkColor/i)          { return "Link Color"; }
        if ($fld =~ /VisitedLinkColor/i)   { return "Visited Link Color"; }
        if ($fld =~ /ActiveLinkColor/i)    { return "Active Link Color"; }
        if ($fld =~ /BackgroundImage/i)    { return "Background Image"; }
        if ($fld =~ /FileName/i)           { return "File name"; }
        if ($fld =~ /MetaKeywords/i)       { return "Meta:Keywords"; }
	if ($fld =~ /MetaDescription/i)    { return "Meta:Description"; }

    } 
    else {
	if ($fld =~ /\AName\Z/i)           { return "Name"; }
        if ($fld =~ /\APrice\Z/i)          { return "Price"; }
        if ($fld =~ /SaleAmount/i)         { return "Sale Amount"; }
        if ($fld =~ /Taxable/i)            { return "Taxable"; }
	if ($fld =~ /TaxWareCode/i)        { return "TaxWare Code"; }
	if ($fld =~ /\AVAT\Z/i)            { return "VAT"; }
        if ($fld =~ /Weight/i)             { return "Weight"; }
	if ($fld =~ /ShippingCharge/i)     { return "Shipping Charge"; }
        if ($fld =~ /GroundShipping/i)     { return "Ground Shipping"; }
        if ($fld =~ /SecondDayShipping/i)  { return "Second Day Shipping"; }
        if ($fld =~ /NextDayShipping/i)    { return "Next Day Shipping"; }
        if ($fld =~ /Shipping([0-9])/i)    { return "Shipping $1"; }
        if ($fld =~ /\ASKU\Z/i)            { return "SKU"; }
        if ($fld =~ /\AGraphic\Z/i)        { return "Graphic"; }
        if ($fld =~ /ProductDescription/i) { return "Product Description"; }
        if ($fld =~ /OptionText/i)         { return "Option Text"; }
        if ($fld =~ /OptionFiniteText/i)   { return "Option Finite Text"; }
        if ($fld =~ /OptionsBox/i)         { return "Options Box?"; }
        if ($fld =~ /DisplayMoreInformationPage/i) { 
	    return "Display more information page?"; }
        if ($fld =~ /MoreInformationText/i) { 
	    return "More information text"; }
        if ($fld =~ /MoreInformationGraphic/i) { 
	    return "More information graphic"; }
        if ($fld =~ /FileName/i)           { return "File name"; }
        if ($fld =~ /DisplayName/i)        { return "Display Name?"; }
        if ($fld =~ /DisplaySKU/i)         { return "Display SKU?"; }
        if ($fld =~ /DisplayPrice/i)       { return "Display Price?"; }
        if ($fld =~ /DisplayGraphic/i)     { return "Display Graphic?"; }
        if ($fld =~ /SaleOn/i)             { return "Sale On"; }
        if ($fld =~ /NameStyle/i)          { return "Name Style"; }
        if ($fld =~ /NameSize/i)           { return "Name Size"; }
        if ($fld =~ /PriceStyle/i)         { return "Price Style"; }
        if ($fld =~ /PriceSize/i)          { return "Price Size"; }
        if ($fld =~ /SKUStyle/i)           { return "SKU Style"; }
        if ($fld =~ /SKUSize/i)            { return "SKU Size"; }
        if ($fld =~ /DescriptionStyle/i)   { return "Description Style"; }
        if ($fld =~ /DescriptionSize/i)    { return "Description Size"; }
        if ($fld =~ /ImageAlignment/i)     { return "Image Alignment"; }
        if ($fld =~ /TextWrap/i)           { return "Text Wrap"; }
        if ($fld =~ /AddToCartButton/i)    { return "Order Button"; }
        if ($fld =~ /ViewCartButton/i)     { return "Checkout Button"; }
        if ($fld =~ /AltPrice/i)           { return "Price"; }
        if ($fld =~ /AltSaleAmount/i)      { return "Sale Amount"; }
    }
    
    return "";
}


1;


