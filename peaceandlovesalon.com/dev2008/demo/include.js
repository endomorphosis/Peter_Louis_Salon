/*
** $Id: include.js,v 1.2 2003/09/14 19:44:34 damonp Exp $
**
** Copyright (C) 2002 siliconsys.com <dev@siliconsys.com>
** See LICENSE file in root directory of this installation for
** licensing information.
*/

	var Ver4 = parseInt(navigator.appVersion) >= 4
	var Nav4 = ((navigator.appName == "Netscape") && Ver4)
	var IE4 = ((navigator.userAgent.indexOf("MSIE") != -1) && Ver4)

	function pop_win(url, win, ht, wd) {
  		popwin = document.open(url,win,"width="+wd+",height="+ht+",toolbar=no,resizable=yes,scrollbars=yes");
 	}

	function confirm_action(url, action) {
		var is_confirmed = confirm("Are you sure you want to " + action + "?");
		if(is_confirmed) {
			window.location.href = url;
		}
	}

	function confirm_submit(form_name, action, id) {
		var is_confirmed = confirm("Are you sure you want to " + action + "?");
		if(is_confirmed) {
			document[form_name].id.value = id;
			document[form_name].submit();
		}
	}

	function submit_cat()	{
		if(document.cat_form.sel_category.options.selectedIndex != 0 &&
					document.cat_form.sel_category.options.selectedIndex != '')	{
			document.cat_form.submit();
		}	else return false;
	}

	function highlight(row, new_color) {
	    if (typeof(row.style) == 'undefined' || typeof(row.cells) == 'undefined') {
	        return false;
	    }
	    var num_rows = row.cells.length;
	    for (var c = 0; c < num_rows; c++) {
	        row.cells[c].bgColor = new_color;
	    }
	    return true;
	}