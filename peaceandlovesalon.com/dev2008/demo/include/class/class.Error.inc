<?php
/*
** $Id: class.Error.inc,v 1.1 2003/04/10 20:20:47 damonp Exp $
**
**
*/

	class Error {

		var $_file = "Unknown";
		var $_msg = array();
		var $_msg_arr = array();
		var $_classname = "";
		var $_debug	= false;

/***********************************
**	constructor
***********************************/
		function Error($classname) {
			$this->_classname = $classname;
		}
/***********************************
**	public methods
***********************************/
		function Halt($msg, $num, $line="Unknown") {

			$this->_msg_arr['msg'] 	= "<b>$this->_classname error:</b> $msg";
			$this->_msg_arr['file'] = "<b>File</b>: $this->_file";
			$this->_msg_arr['line'] = "<b>Line</b>: $line";
			$this->_msg[] = $this->_msg_arr;
			if ($this->_debug == "DEBUG" || $this->_debug == "VERBOSE") {
				$cmd_line = getenv("REQUEST_URI");
				if($cmd_line == '') {
					printf("$this->_classname error: %s\n", $msg);
					if ($this->_debug == "DEBUG" || $this->_debug == "VERBOSE") {
						printf("File: %s\n", $this->_file);
						printf("Line: %s\n", $line);
						die("Session halted");
					}
				}	else {
					printf("<code><b>$this->_classname error:</b> %s</code><br>\n", nl2br($msg));
					printf("<code><b>File</b>: %s</code><br>\n", $this->_file);
					printf("<code><b>Line</b>: %s</code><br>\n", $line);
					die("<code>Session halted</code>");
				}
			}	elseif($this->_debug == 'SILENT') {
				die();
			}	else {
				if($php_self == '') printf("$this->_classname error: %s\nSession halted", $msg);
				else printf("<code><b>$this->_classname error:</b> %s</code><br>\nSession halted<br>\n", $msg);
				die();
			}
		}

		function Warn($msg, $num, $line="Unknown") {
			global $PHP_SELF;
			$this->_msg_arr['msg'] 	= "<b>$this->_classname warning:</b> $msg";
			$this->_msg_arr['file'] = "<b>File</b>: $this->_file";
			$this->_msg_arr['line'] = "<b>Line</b>: $line";
			$this->_msg[] = $this->_msg_arr;
			if ($this->_debug == "DEBUG" || $this->_debug == "VERBOSE") {
				$cmd_line = getenv("REQUEST_URI");
				if($cmd_line == '') {
					printf("$this->_classname warning: %s\n", $msg);
					if ($this->_debug == "DEBUG" || $this->_debug == "VERBOSE") {
						printf("File: %s\n", $this->_file);
						printf("Line: %s\n", $line);
					}
				}	else {
					printf("<code><b>$this->_classname warning:</b> %s</code><br>\n", nl2br($msg));
					printf("<code><b>File</b>: %s</code><br>\n", $this->_file);
					printf("<code><b>Line</b>: %s</code><br>\n", $line);
				}
			}	elseif($this->_debug == 'SILENT') {

			}	else {
				if($php_self == '') printf("$this->_classname warrning: %s\n", $msg);
				else printf("<code><b>$this->_classname error:</b> %s</code><br>\n", $msg);
			}
		}

		function ReturnMsg() {
			if(! is_array($this->_msg))
				return false;
			while($cur = current($this->_msg)) {
				$msg = $cur['msg'];
				$file = $cur['file'];
				$line = $cur['line'];
				$buffer .= "$msg<br>\n";
				if($this->_debug == "VERBOSE") {
					$buffer .= "$file<br>\n";
					$buffer .= "$line<br>\n";
				}
				$cur = next($this->_msg);
			}
			return $buffer;
		}

		function SetDebug($debug) {
			$this->_debug = $debug;
		}

		function SetFile($file) {
			$this->_file = $file;
		}

	}
?>