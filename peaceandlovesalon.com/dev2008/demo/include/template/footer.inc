<?php
/*
**	$Id: footer.inc,v 1.3 2003/10/27 05:51:38 damonp Exp $
**
** Copyright (C) 2002 siliconsys.com <dev@siliconsys.com>
** See LICENSE file in root directory of this installation for
** licensing information.
*/

/*
**	NOTE If you remove the link back to siliconsys.com and copyright information
**		you are violating the terms of the software license.  We do monitor
** 	installations (per the terms of the license) from time to time and can pursue
**		damages at our option.
*/
?>

</td></tr>
<tr><td height="10" background="./image/pixel_black.png"><img src="./image/pixel_black.png" height="1" width="1"></td></tr>
</table>

<p class="footer" align="center">
Powered by <a href="http://www.siliconsys.com/redirect/phpcatalog/phpc/" class="footer">PHPCatalog&#8482;</a> v<? echo $CFG->appversion; ?>
&nbsp;&copy; 1999-2004 <a href="http://www.siliconsys.com/redirect/index/phpc/" class="footer">siliconsys.com</a>
</p>
</body>
</html>
