<html>
<head>
<title>{pagetitle}</title>
<meta content="text/html; charset=windows-1252" http-equiv="Content-Type">
<link rel="stylesheet" href="./admin.css" type="text/css">
<script src="./jscript/admin.js" language="Javascript1.1"></script>
</head>
<body>
<table border="0" cellPadding="6" cellSpacing="0" width="98%" align="center">
<tr>
<td colspan="2">
<table border="0" cellPadding="0" cellSpacing="0" width="100%" align="center">
<tr><td colspan="4" background="{base}/image/pixel_black.png" height="1"><img src="{base}/image/pixel_black.png" height="1" width="1"></td></tr>
<tr vAlign="middle" height="50" bgcolor="#cccccc">
<td background="{base}/image/pixel_black.png" width="1" rowspan="2"><img src="{base}/image/pixel_black.png" height="1" width="1"></td>
<td align="left" valign="top" width="75%">
<table border="0" cellPadding="0" cellSpacing="0" height="100%" width="100%" align="center" bgcolor="#cccccc">
<tr height="50%">
<td rowspan="3" width="5"><img src="{base}/image/pixel_gray.png" height="1" width="1"></td>
<td nowrap><font size="5" face="verdana"><b>PHPCatalog</b></font></td>
</tr>
<tr height="50%" valign="bottom"><td><div id="text"><img src="{base}/image/pixel_gray.png"></div></td></tr>
</table>
</td>
<td valign="bottom" width="25%">
<table border="0" cellPadding="0" cellSpacing="0" width="100%" align="right" bgcolor="#cccccc">
<tr>
<td align="right"><a href="{me}?section=index" onMouseOver="flash_blurb('help index');" onMouseOut="flash_blurb('');"><img src="{base}/image/icon/help.png" title="help" border="0"></a></td>
</tr>
</table>
</td>
<td background="{base}/image/pixel_black.png" width="1" rowspan="4"><img src="{base}/image/pixel_black.png" height="1" width="1"></td>
</tr>
<tr><td colspan="2" bgcolor="#cccccc" height="5"><img src="{base}/image/pixel_gray.png" height="1" width="1"></td></tr>
<tr><td colspan="4" background="{base}/image/pixel_black.png" height="1"><img src="{base}/image/pixel_black.png" height="1" width="1"></td></tr>
</table>
</td>
</tr>
<tr>
<td height="350" width="100%" vAlign="top">
<table border="0" cellPadding="0" cellSpacing="0" width="100%">
<tr><td colspan="3" background="{base}/image/pixel_black.png" height="1"><img src="{base}/image/pixel_black.png" height="1" width="1"></td></tr>
<tr>
<td rowspan="3" background="{base}/image/pixel_black.png" width="1"><img src="{base}/image/pixel_black.png" height="1" width="1"></td>
<td>
<table border="0" cellPadding="4" cellSpacing="0" width="100%">
<tr height="1"><td>
{msg}
</td></tr>
<tr>
<td height="350" vAlign="top">
{content}
</td>
</tr>
</table>
</td>
<td background="{base}/image/pixel_black.png" width="1"><img src="{base}/image/pixel_black.png" height="1" width="1"></td>
</tr>
<tr><td colspan="3" background="{base}/image/pixel_black.png" height="1"><img src="{base}/image/pixel_black.png" height="1" width="1"></td></tr>
</table>
<font class="small">
{bottom}
</font>
</td></tr>
<tr><td align="center" class="footer">PHPCatalog&#8482; {appversion} &copy; 1999 - 2003 <a href="http://www.siliconsys.com/" target="_new">siliconsys.com</a></td></tr>
</table>
</body>
</html>