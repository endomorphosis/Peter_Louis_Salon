
<!-- START admin/form -->
<form name="product_form" enctype="multipart/form-data" action="{me}" onsubmit="return validate();" method="post">
<input type="hidden" name="id" value="{product_id}">
<input type="hidden" name="function" value="{function}">
<input type="hidden" name="old_thumb_path" value="{thumb_path}">
<input type="hidden" name="old_image_path" value="{image_path}">
<input type="hidden" name="image_id" value="{image_id}">
<input type="hidden" name="thumb_id" value="{thumb_id}">
<input type="hidden" name="sel_category" value="{sel_category}">
<input type="hidden" name="max_price" value="{max_price}">
<table cellpadding="0" cellspacing="0" border="0" width="100%">
<tr><td background="{base}/image/pixel_black.png" height="1" vAlign="bottom"><img src="{base}/image/pixel_black.png" height="1" width="1"></td></tr>
<tr bgcolor="{color_tbl2}">
<td>
<table cellpadding="4" cellspacing="2" border="0" width="100%">
<tr height="28" bgcolor="{color_label}"><td colspan="5"><img src="{base}/image/pixel_dk_gray.png" height="20" width="1"></td></tr>
<tr>
<td width="20%" bgcolor="{color_label}" class="label">product sku</td>
<td colspan="3"><input type="text" name="product_sku" size="8" value="{product_sku}" class="input"></td>
</tr>
<tr>
<td bgcolor="{color_label}" class="label">product name</td>
<td colspan="3"><input type="text" name="product_name" size="45" value="{product_name}" class="input"></td>
</tr>
<tr>
<td bgcolor="{color_label}" class="label">price</td>
<td colspan="2">{currency_before}<input type="text" name="product_price" size="10" value="{product_price}" class="input">{currency_after}</td>
<td class="small">set to 0.00 for price on request</td>
</tr>
<tr>
<td bgcolor="{color_label}" class="label">color</td>
<td colspan="3"><input type="text" name="product_color" size="18" value="{product_color}" class="input"></td>
</tr>
<tr>
<td bgcolor="{color_label}" class="label">size</td>
<td colspan="3"><input type="text" name="product_size" size="18" value="{product_size}" class="input"></td>
</tr>
<tr>
<td bgcolor="{color_label}" class="label">category</td>
<td>
{select_category_input}
</td>
<td><input type="text" name="product_category" size="18" class="input"></td>
<td class="small">to create a subcategory, select parent and enter category at left</td>
</tr>
<tr>
<td bgcolor="{color_label}" class="label">description</td>
<td colspan="3"><textarea name="product_desc" cols="60" rows="5" wrap="virtual" class="input">{product_desc}</textarea></td>
</tr>
<tr>
<td bgcolor="{color_label}" class="label">image file</td>
<td colspan="2">{full_input}</td>
<td class="small"><a href="javascript:pop_win('./view.php?image={image_path}', 'image', {img_ht}, {img_wd});">{image_path}</a></td>
</tr>
{im_image_scale}
<tr>
<td bgcolor="{color_label}" class="label">thumbnail generation</td>
<td>yes<input type="radio" name="gen_thumb" value="yes">&nbsp;no<input type="radio" name="gen_thumb" value="no" checked></td>
{thumb_scale_form}
</tr>
<tr>
<td bgcolor="{color_label}" class="label">thumbnail file</td>
<td colspan="2">{thumb_input}</td>
<td class="small"><a href="javascript:pop_win('./view.php?image={thumb_path}', 'image', {thumb_ht}, {thumb_wd});">{thumb_path}</a></td>
</tr>
{im_thumb_scale}
{im_extra_images}
<tr>
<td bgcolor="{color_label}" class="label">use tag</td>
<td colspan="3">
For Sale<input type="radio" name="product_use_tag" value="OPEN" {radio_OPEN}><img src="{base}/image/pixel_white.png" height="1" width="5">
Sold<input type="radio" name="product_use_tag" value="SOLD" {radio_SOLD}><img src="{base}/image/pixel_white.png" height="1" width="5">
Special<input type="radio" name="product_use_tag" value="SPECIAL" {radio_SPECIAL}>
</td>
</tr>
{related_input}
{db_fields_input}
<tr bgcolor="{color_label}"><td colspan="5" align="center"><input type="submit" name="submit" value="{button}" class="input"></td></tr>
</form>
</table>
</td>
</tr>
<tr><td background="{base}/image/pixel_black.png" height="1" vAlign="top"><img src="{base}/image/pixel_black.png" height="1" width="1"></td></tr>
</table>
<!-- FINISH admin/form -->
