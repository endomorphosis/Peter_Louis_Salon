
<!-- START admin/list -->
<table cellpadding="0" cellspacing="0" border="0" width="100%">
<form name="cat_form" action="{me}" method="get">
<tr><td colspan="10" background="{base}/image/pixel_black.png" height="1" vAlign="bottom"><img src="{base}/image/pixel_black.png" height="1" width="1"></td></tr>
<tr bgcolor="{color_tbl2}" height="28">
<td width="1%">&nbsp;</td>
<td width="98%" colspan="8">
<table cellpadding="0" cellspacing="0" border="0" align="center" width="100%" bgcolor="{cellcolor1}">
<tr bgcolor="{color_tbl2}">
<td align="left" width="33%">
{select_category}
</td>
</form>
<td align="center" width="33%" class="list">
listing items <b>{l_start}</b> - <b>{l_end}</b> of <b>{l_total}</b>
</td>
<form name="search_form" action="{me}" method="get">
<input type="hidden" name="function" value="search">
<td align="right" width="33%">
<b>search</b><img src="{base}/image/pixel_white.png" height="1" width="5"><input type="text" name="query" value="" class="input">
</td>
</form>
</tr>
</table>
</td>
<td width="1%">&nbsp;</td>
</tr>
<tr><td colspan="10" background="{base}/image/pixel_black.png" height="1" vAlign="top"><img src="{base}/image/pixel_black.png" height="1" width="1"></td></tr>
<tr bgcolor="{color_label}" height="25" class="selectlabel">
<td width="1%">&nbsp;</td>
<td width="7%"><a href="./?sort=id|{product_id_dir}&sel_category={sel_category}{s_extra_vars}" class="{product_id_label}">id</a>{product_id_dir_disp}</td>
<td width="10%"><a href="./?sort=sku|{product_sku_dir}&sel_category={sel_category}{s_extra_vars}" class="{product_sku_label}">sku</a>{product_sku_dir_disp}</td>
<td width="20%"><a href="./?sort=category|{product_category_dir}&sel_category={sel_category}{s_extra_vars}" class="{product_category_label}">category</a>{product_category_dir_disp}</td>
<td width="35%"><a href="./?sort=name|{product_name_dir}&sel_category={sel_category}{s_extra_vars}" class="{product_name_label}">name</a>{product_name_dir_disp}</td>
<td width="15%"><a href="./?sort=price|{product_price_dir}&sel_category={sel_category}{s_extra_vars}" class="{product_price_label}">price</a>{product_price_dir_disp}</td>
<td width="15%" colspan="3" align="center" class="label">action</td>
<td width="1%">&nbsp;</td>
</tr>

<!-- BEGIN row -->
<tr bgcolor="{row_color}" class="list" onMouseOver="highlight(this, '{color_hilite}');" onMouseOut="highlight(this, '{row_color}');" onMouseDown="highlight(this, '{color_select}');">
<td width="1%">&nbsp;</td>
<td nowrap>{product_id}</td>
<td nowrap>{product_sku}</td>
<td nowrap>{product_category}</td>
<td>{product_name}</td>
<td nowrap>{f_product_price}</td>
<td width="5%" align="center"><a class="menu" href="{me}?function=edit&id={product_id}&sort={sort}&sel_category={sel_category}"><img src="{base}/image/icon/edit.png" title="edit" border="0"></a></td>
<td width="5%" align="center"><a class="menu" href="{me}?function=delete&id={product_id}&sort={sort}&sel_category={sel_category}"><img src="{base}/image/icon/delete.png" title="delete" border="0"></a></td>
<td width="5%" align="center"><a class="menu" href="{me}?function=view&id={product_id}&sort={sort}&sel_category={sel_category}"><img src="{base}/image/icon/view.png" title="view" border="0"></a></td>
<td width="1%">&nbsp;</td>
</tr>

<!-- END row -->
<tr><td colspan="10" vAlign="top" height="3" bgcolor="{row_color}"><img src="{base}/image/pixel_gray.png" height="1" width="1"></td></tr>
<tr><td colspan="10" background="{base}/image/pixel_black.png" height="1" vAlign="top"><img src="{base}/image/pixel_black.png" height="1" width="1"></td></tr>
<tr><td colspan="10" vAlign="top" height="3">
{prev_next}
</td></tr>
<tr height="30" valign="bottom"><td colspan="9"><a href="{me}?function=add&sel_category={sel_category}">add product</td></tr>
</table>
<!-- FINISH admin/list -->
