<?php
/*
** $Id: config.inc,v 1.6 2003/09/16 21:24:53 damonp Exp $
**
** Copyright (C) 2002 siliconsys.com <dev@siliconsys.com>
** See LICENSE file in root directory of this installation for
** licensing information.
*/

	/* define a generic object */
	class object {};

	/* setup the configuration object */
	$CFG = new object;

/***********************************
**	begin configuration parameters
***********************************/
	$CFG->dbtype		=	'mysql';			//	database server type (mysql, ifx, oci, pg)
	$CFG->dbhost		=	'localhost';   // hostname of db server
	$CFG->dbname		=	'PHPCatalog';  // database name
	$CFG->dbuser		=	'';      		// database user authorized for this database
	$CFG->dbpass		=	'';		      // password for this user

	$CFG->wwwroot		=	'http://yourdomain.com/url/path/to/PHPCatalog';  // without ending slash, url path to PHPCatalog
	$CFG->dirroot		=	'/dir/path/to/PHPCatalog';  							// full directory path to PHPCatalog; windows systems use the form 'c:/path/to/PHPCatalog'  (with forward slashes '/' instead of backslashes '\')

	$CFG->libdir		=	$CFG->dirroot . '/include';
	$CFG->imagedir		=	$CFG->dirroot . '/image';

	$CFG->admin_items_page	= 20;

	/* catalog interface display variables */
	$CFG->max_img_size	=	'500000'; // in kb
	$CFG->max_price		=	'100000';

	define('DEBUG', true);																// uncomment this line for debugging

	if(defined('DEBUG'))	error_reporting(E_ALL & ~E_NOTICE);
	else	{
		error_reporting(0); 											// uncomment this line and
		//error_reporting(E_ALL & ~E_NOTICE);						// comment this line if you are having problems
	}

/***********************************
**	end configuration parameters
**	do not edit anything below this
***********************************/

	$CFG->msg			=	array();
	$CFG->page_arr		=	array();

	$CFG->appversion	=	'2.6.12';
	$CFG->prod_dir		= $CFG->imagedir . '/products/';

	define('T_STAMP', time());
	define('SHORT_TIME', "m/d/y G:i");
	define('LONG_TIME', "j M Y g:i:s T");

	/* load up standard libraries */
	require_once($CFG->libdir . '/class/class.Template.inc');
	require_once($CFG->libdir . '/stdlib.inc');
	require_once($CFG->libdir . '/phpcatalog.inc');

	/* connect to the database */
	$CFG->dbh = dbi_conn($CFG->dbtype, $CFG->dbname, defined('DEBUG'));

	/*	initialize template */
	$T	= new	Template($CFG->libdir . '/template', 'remove');

	/* start session, register vars */
	session_start();
	if(get_cfg_var('register_globals'))	session_register('SESSION');

 	$form = add_all_slashes($_POST);

	$action			= passed_var('action');
	$function		= passed_var('function');
	$id 				= passed_var('id', 'both', 'integer');
	$method			= passed_var('method');
	$mod_name 		= passed_var('mod_name');
	$query 			= passed_var('query');
	$sel_category 	= passed_var('sel_category', 'get');
	$sort				= passed_var('sort', 'get');
	$start			= passed_var('start', 'get', 'integer');


	if(! defined('NO_PROCESS')) {
		set_default_null($start, 0);
		set_default_null($catalog_id, 1);
		$CFG->catalog_id = $catalog_id;
		set_default($_SESSION['SESSION']['category'], '%');
		$_SESSION['SESSION']['category'] = isset($form['sel_category']) ? $form['sel_category']:$_SESSION['SESSION']['category'];
		get_config($catalog_id, defined('ADMIN'));
	}

	$CFG->phpc_table 	= 'phpc_' . $_SESSION['config']['catalog_name'];

	if(defined('ADMIN')) {
		require_once($CFG->libdir . '/phpcatalog_admin.inc');
	}
?>