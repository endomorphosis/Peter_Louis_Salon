<?php
/*
**	$Id: phpcatalog.inc,v 1.6 2003/09/16 20:31:48 damonp Exp $
**
** Copyright (C) 2002 siliconsys.com <dev@siliconsys.com>
** See LICENSE file in root directory of this installation for
** licensing information.
*/

/***********************************
**	functions
***********************************/

	function select_category($selected='', $auto_submit=false, $input_name='') {
		global $CFG;

		$CFG->dbh->Query("SELECT *
								FROM phpcatalog_categories
								WHERE catalog_id = $CFG->catalog_id
									AND parent_id > 0
								ORDER BY parent_id, category_name",
								__FILE__, __LINE__);
		$categories = array();
		while($r = $CFG->dbh->FetchObject())
			$categories[$r->parent_id][$r->category_id] = $r->category_name;

		$on_change 	= $auto_submit ? "onchange=\"submit_cat();\"":'';
		$input_name	= $input_name == '' ? 'sel_category':$input_name;
		$return_val = "<select name=\"$input_name\" $on_change class=\"input\">\n<option value=\"%\">Select a category</option>\n";
		$return_val .= build_sel_categories(1, $selected, $categories, $return_val, '');

		$return_val .= "</select>\n";
		return $return_val;
	}

	function build_sel_categories($parent_id, $selected, &$categories, &$return_val, $indent='') {
		if(! is_array($categories[$parent_id])) return;
		foreach($categories[$parent_id] as $cat_id => $cat_name) {
			if(strlen($cat_name) > 30)
				$cat_name = substr($cat_name, 0, 30) . '..';
			if($cat_id == $selected) $return_val .= "<option value=\"$cat_id\" selected>$indent $cat_name</option>\n";
			else $return_val .= "<option value=\"$cat_id\">$indent $cat_name</option>\n";
			unset($categories[$parent_id]);
			$return_val .= build_sel_categories($cat_id, $selected, $categories, $return_val, $indent.'&nbsp;&nbsp;');
		}
	}

 	function has_children($id) {
		global $CFG, $T;

		$CFG->dbh->Query("SELECT 1
								FROM phpcatalog_categories
								WHERE parent_id = $id",
								__FILE__, __LINE__);
		$return = $CFG->dbh->RowCount > 0 ? true:false;
		return $return;
	}

	function get_categories($parent_id=0, $page='') {
		global $CFG, $T;

		$CFG->dbh->Query("SELECT category_id, category_name
								FROM phpcatalog_categories
								WHERE parent_id = '$parent_id'
								ORDER BY category_name",
								__FILE__, __LINE__);
		if($page == '') $page = 'page';
		else $pre = 'sub_';

		$T->set_block($page, $pre.'cat_row', $pre.'cat_rows');
		if($CFG->dbh->RowCount > 0) {
			while($r = $CFG->dbh->FetchObject('', 1)) {
				$row_color = $row_color == $CFG->color_tbl1 ? $CFG->color_tbl2:$CFG->color_tbl1;
				$T->set_var(array(
									'row_color'			=> $row_color,
									'category_name'	=> $r->category_name,
									'category_id'		=> $r->category_id));
				$T->parse($pre.'cat_rows', $pre.'cat_row', true);
			}
		}	else {
			$T->set_var($pre.'cat_rows', '');
			$T->implodeBlock($pre.'cat_row');
		}
	}

	function get_category_name($id) {
		global $CFG;

		if(!is_numeric($id)) return false;
		$CFG->dbh->Query("SELECT category_name
								FROM phpcatalog_categories
								WHERE category_id = $id
									AND catalog_id = 1",
								__FILE__, __LINE__);
		if($CFG->dbh->RowCount > 0) {
			$r = $CFG->dbh->FetchObject('', 1);
			return stripslashes($r->category_name);
		}	else return false;
	}

	function format_product_row(&$t, $row, $format) {
		global $CFG;

		$row = process_db_output($row);
		if($row->product_price == 0 && ! defined('ADMIN')) {
			$product_price = $_SESSION['config']['price_on_request'];
			$search_vars	= array('/PRODUCT_SKU/', '/PRODUCT_ID/', '/PRODUCT_NAME/', '/&quot;/');
			$replace_vars	= array($row->product_sku, $row->product_id, $row->product_name, '"');
			$product_price = preg_replace($search_vars, $replace_vars, $product_price);
		} 	else $product_price = format_currency($row->product_price);

		$prod_dir	= str_replace($CFG->dirroot, $CFG->wwwroot, $CFG->prod_dir);
		$image_dir	= str_replace($CFG->dirroot, $CFG->wwwroot, $CFG->imagedir);

		$thumb_path	= ($row->thumb_path != '' && file_exists($CFG->prod_dir . '/' . $row->thumb_path)) ? $row->thumb_path:'not_available.png';
		$image_path	= ($row->image_path != '' && file_exists($CFG->prod_dir . '/' . $row->image_path)) ? $row->image_path:'not_available.png';

		$thumb_path = $thumb_path == 'not_available.png' ? $image_dir . '/not_available.png':$prod_dir . $thumb_path;
		$image_path = $image_path == 'not_available.png' ? $image_dir . '/not_available.png':$prod_dir . $image_path;

		$t->set_var(array(
								'product_id'			=> $row->product_id,
								'product_sku'			=> $row->product_sku,
								'product_category'	=> $row->product_category,
								'cat_id'					=> $row->category_id,
								'product_desc'			=> $row->product_desc,
								'thumb_path'			=> $thumb_path,
								'image_path'			=> $image_path,
								'product_name'			=> $row->product_name,
								'product_price'		=> $row->product_price,
								'f_product_price'		=> $product_price,
								'product_color'		=> $row->product_color,
								'product_size'			=> $row->product_size,
								'product_use_tag'		=> $row->product_use_tag,
								'use'						=> ''), __FILE__, __LINE__);

		switch($format) {
			case('detail'):
				if($row->product_use_tag == 'SOLD') $t->set_var('use', 'SOLD');
				$t->set_var(array(
									'product_desc'		=> stripslashes(nl2br($row->product_desc))));
				break;

			case('edit'):
				$t->set_var(array(
									'radio_'.$row->product_use_tag	=> 'checked',
									'product_desc'		=> stripslashes($row->product_desc),
									'thumb_path'		=> $row->thumb_path,
									'image_path'		=> $row->image_path));
				break;

			case('list'):
				if($row->product_use_tag == 'SOLD') $t->set_var('use', 'SOLD');
				$t->set_var(array(
									'product_desc'		=> stripslashes(nl2br($row->product_desc))));
				break;

			case('verify'):
				$t->set_var(array(
									'product_desc'		=> stripslashes(nl2br($row->product_desc)),
									'f_product_price'	=> format_currency($row->product_price)));
				break;

			case('view'):
				$t->set_var(array(
									'product_desc'		=> nl2br(stripslashes($row->product_desc))));
				break;

			default:
				$t->set_var(array(
									'radio_'.$row->product_use_tag	=> 'checked',
									'product_desc'		=> stripslashes($row->product_desc)));

				break;
		}
	}

	function format_currency($price, $format=true)	{
		global $CFG, $T;

		$price = number_format($price, 2,	$_SESSION['config']['decimal_sep'], $_SESSION['config']['thousand_sep']);
		if($format)	{
			if($_SESSION['config']['currency_location']  == 'after')	$price = $price . ' ' . $_SESSION['config']['currency_sym'];
			else	$price = $_SESSION['config']['currency_sym'] . ' ' . $price;
		}

		return $price;
	}

	function get_config($id, $get=false) {
		global $CFG;

		if($_SESSION['config']['catalog_id'] == $id && ! $get) {
			$CFG->catalog_id = $id;
			$_SESSION['catalog_id'] = $id;
			return $_SESSION['config'];
		}

		if(! is_array($_SESSION['config'])) $_SESSION['config'] = array();
		$CFG->dbh->Query("SELECT *
								FROM phpcatalog_config
								WHERE catalog_id = '$id'",
								__FILE__, __LINE__);

   	$return_arr = array();
   	while($r = $CFG->dbh->FetchObject('', 1)) {
	  		if(defined('ADMIN')) {
	  			$r->config_value = clean_entities($r->config_value);
  				$_SESSION['config'][$r->config_key]	= $r->config_value;
	  			$return_arr[$r->config_key]			= $r->config_value;
			}	elseif(substr($r->config_key, 0, 6) != 'admin_') {
  				$_SESSION['config'][$r->config_key]	= $r->config_value;
	  			$return_arr[$r->config_key]			= $r->config_value;
	  		}
	   }

	   $CFG->dbh->Free($r);
   	return $return_arr;
	}

	function specials_display($num) {
		global $T, $CFG;

		if($num == 0) return;
		$db_limit = $num;
		$T->set_file('specials', 'tmpl.box.inc');
		$T->set_var('box_title', 'Specials');
		$CFG->dbh->Query("SELECT product_id, product_name
								FROM $CFG->phpc_table
								WHERE product_use_tag = 'SPECIAL'
								ORDER BY product_name
								LIMIT $db_limit",
								__FILE__, __LINE__);

		if($CFG->dbh->RowCount < 1) return;
		while($r = $CFG->dbh->FetchObject('', 1))
			$box_content .= "<tr><td width=\"2\">&nbsp;</td><td><a href=\"./?function=detail&id=$r->product_id\" class=\"boxbody\">$r->product_name</a></td><td width=\"2\">&nbsp;</td></tr>\n";
		$T->set_var('box_content', $box_content);
		$T->parse('specials', 'specials');
		array_push($CFG->page_arr, 'specials');
	}

	function stats_log_hit($id) {
		global $CFG, $_SESSION;

		$CFG->dbh->Query("INSERT
								INTO phpcatalog_stats(t_stamp, product_id)
								VALUES ('" . T_STAMP . "', '$id')",
								__FILE__, __LINE__);

		if(date('D', T_STAMP) == 'Sun')	{
			$stats_duration = $_SESSION['config']['stats_duration'];
			set_default_null($stats_duration, 14);
			$stats_duration = $stats_duration * 86400;
			$CFG->dbh->Query("DELETE
									FROM phpcatalog_stats
									WHERE t_stamp < " . (T_STAMP - $stats_duration),
									__FILE__, __LINE__);
		}

		return true;
	}

	function stats_display($num, $duration) {
		global $T, $CFG;

		if($num == 0) return;
		$db_limit = $num;
		$duration = T_STAMP - $duration * 24 * 60 * 60;
		$T->set_file('top_stats', 'tmpl.box.inc');
		$T->set_var('box_title', 'Top Products');
		$CFG->dbh->Query("SELECT COUNT(*) as num_views,
									s.t_stamp, c.product_id, c.product_name
								FROM $CFG->phpc_table c,
									phpcatalog_stats s
								WHERE s.product_id = c.product_id
									AND s.t_stamp > $duration
								GROUP BY c.product_id
								ORDER BY num_views DESC
								LIMIT $db_limit",
								__FILE__, __LINE__);

		if($CFG->dbh->RowCount < 1) return;
		while($r = $CFG->dbh->FetchObject('', 1)) {
			if($r->num_views < 1) continue;
			$box_content .= "<tr><td width=\"2\">&nbsp;</td><td><a href=\"./?function=detail&id=$r->product_id\" class=\"boxbody\">$r->product_name</a>&nbsp;<font class=\"small\">($r->num_views)</font></td><td width=\"2\">&nbsp;</td></tr>\n";
		}
		$T->set_var('box_content', $box_content);
		$T->parse('top_stats', 'top_stats');
	}
?>