<?php
/*
**	$Id: stdlib.inc,v 1.7 2003/09/16 20:31:48 damonp Exp $
**
** Copyright (C) 2002 siliconsys.com <dev@siliconsys.com>
** See LICENSE file in root directory of this installation for
** licensing information.
*/

/***********************************
**	variable processing
***********************************/

	function add_all_slashes($vars) {
		if(get_magic_quotes_gpc()) return $vars;
		foreach($vars as $key => $val)
			if(! is_scalar($val)) {
				foreach($val as $sub_key => $sub_val)
					$form[$key][$sub_key] = addslashes($sub_val);
			}	else $form[$key] = addslashes($val);
		return $form;
	}

	function process_db_input($input)	{
		if(is_array($output) || is_object($output))
			foreach($input as $key => $val)
				$input[$key] = trim(rev_clean_entities($val));
		return $input;
	}

	function process_db_output($output)	{
		if(is_array($output) || is_object($output))
			foreach($output as $key => $val)
				$output->$key = trim(clean_entities($val));
		return $output;
	}

	function clean_entities($var) {
		$str_search_vars 	= array('"', '\'', '$', "�", '&#133;');
		$str_replace_vars	= array("&quot;", "&#039;", "&#36;", "&#039;", '...');
		return str_replace($str_search_vars, $str_replace_vars, stripslashes($var));
	}

	function rev_clean_entities($var) {
		$str_search_vars 	= array("&#36;", '&quot;', '&#039;');
		$str_replace_vars	= array('$', '"', '\'');
		return addslashes(str_replace($str_search_vars, $str_replace_vars, stripslashes($var)));
	}

	function clean_html($text)	{
		$search = array("'<script[^>]*?>.*?</script>'si",	// strip out javascript
						"'<[\/\!]*?[^<>]*?>'si",			// strip out html tags
						"'([\r\n])[\s]+'"						// strip out white space
						);
		$replace = array("",
							"",
							"\\1"
							);

		$text = preg_replace($search, $replace, $text);

		return $text;
	}

	function passed_var($var, $from='both', $cast='string')	{
		set_default_null($cast, 'text');
		switch($from)	{
			case('both'):
				$var = $_GET[$var] != '' ? $_GET[$var]:$_POST[$var];
				break;
			default:
			case('get'):
				$var = $_GET[$var];
				break;
			case('post'):
				$var = $_POST[$var];
				break;
		}

		$var = clean_html($var);
		settype($var, $cast);
		return $var;
	}

	function set_default(&$var, $default='') {
		if(! isset($var)) {
			$var = $default;
			return true;
		}	else return false;
	}

	function set_default_null(&$var, $default='') {
		if($var == '') {
			$var = $default;
			return true;
		}	else return false;
	}

	function extra_vars($remove='')	{
		global $form;

		$rem_arr = array('id', 'function', 'sel_category');
		$rem_arr	= array_merge($remove, $rem_arr);

		foreach($_GET as $key => $val)	{
			if(!in_array($key, $rem_arr))
				$extra_vars .= $key . '=' . $val . '&';
		}

		if(isset($form['query']) && $form['query'] != '' && ! isset($_GET['query'])) $extra_vars .= 'query=' . urlencode($form['query']) . ' ';
		if($extra_vars != '')	{
			$extra_vars = substr($extra_vars, 0, -1);
			$extra_vars = '&' . $extra_vars;
		}
		return $extra_vars;
	}

/***********************************
**	message/template processing
***********************************/

	function push_msg($msg, $status, &$msg_arr) {
		global $CFG;

		if(! $status)
			$msg_arr[] = $msg . "<br>\n";
		$msg_arr[] = "<font class=\"" . $status . "\"><img src=\"" . $CFG->wwwroot . "/image/icon/$status.png\" align=\"absmiddle\">&nbsp;&nbsp;" . $msg . "</font><br>\n";
		return true;
	}

	function finish_msg($msg_arr='') {
		global $CFG;

		$msg_arr = $msg_arr != '' ? $msg_arr:$CFG->msg;
		$msg = '';
		if(! is_array($msg_arr))
			return false;
		foreach($msg_arr as $cur)
			$msg .= $cur ."\n";
		return $msg;
	}

	function push_template() {
		global $CFG, $T, $form;

		$T->set_var(array(
							'msg'		=>	finish_msg($CFG->msg)));

		if(! defined('ADMIN'))	{
			specials_display($_SESSION['config']['num_specials'], FULL_SPECIALS_DISPLAY);
			stats_display($_SESSION['config']['num_stats'], $_SESSION['config']['stats_duration']);
		}

		array_push($CFG->page_arr, 'content');

		foreach($CFG->page_arr as $page)
			$T->parse($page, $page);
		reset($CFG->page_arr);

		array_push($CFG->page_arr, 'page');
		array_push($CFG->page_arr, 'out');

		$T->parse('out', $CFG->page_arr);
		$T->finish('out');
		$T->p('out');

		if(! defined('ADMIN') && ! USE_PAGE) include($CFG->tmpl_footer);

	 	if(defined('DEBUG')) {
	 		echo "<pre>debugging output:\nto disable comment out line like: define('DEBUG', true); in config.inc</pre>";
			echo "<form><pre><b>FILES</b>\n<textarea cols=60 rows=5 wrap=virtual style=\"font-size: 11px; font-face: arial; overflow: auto;\">";
			var_dump($_FILES);
			echo "</textarea>\n\n<b>_GET</b>\n<textarea cols=60 rows=5 wrap=virtual style=\"font-size: 11px; font-face: arial; overflow: auto;\">";
			var_dump($_GET);
			echo "</textarea>\n\n<b>_POST</b>\n<textarea cols=60 rows=10 wrap=virtual style=\"font-size: 11px; font-face: arial; overflow: auto;\">";
			var_dump($_POST);
			echo "</textarea>\n\n<b>_SESSION</b>\n<textarea cols=60 rows=15 wrap=virtual style=\"font-size: 11px; font-face: arial; overflow: auto;\">";
			var_dump($_SESSION);
			echo "</textarea>\n\n<b>form</b>\n<textarea cols=60 rows=10 wrap=virtual style=\"font-size: 11px; font-face: arial; overflow: auto;\">";
			var_dump($form);
			echo "</textarea></form></pre>";
		}
	}

/***********************************
**	data processing/database
***********************************/

	function dbi_conn($db_type='', $db_name='', $verbose=false) {
		global $CFG;

		$db_type = $db_type == '' ? $CFG->dbtype:$db_type;
		$db_name = $db_name == '' ? $CFG->dbname:$db_name;
		require_once($CFG->libdir . '/class/class.DBI_' . $db_type . '.inc');
		$dbh = new DBI($CFG->dbhost, $CFG->dbuser, $CFG->dbpass, $db_name);
		if($verbose) $dbh->SetDebug($verbose);
		return $dbh;
	}

	function query_loop_array($arr, $default='', $prefix='<option', $suffix='</option>', $found_str='selected') {
		global $CFG;

   	$output = '';
   	foreach($arr as $key => $val) {
   		if(is_array($default)) $selected = empty($default[$key]) ? '' : $found_str;
   		else $selected = ($key == $default) ? $found_str : '';
	   		$output .= $prefix . ' value="' . $key . '"' . $selected . '>' . $val . $suffix . "\n";
   	}
   	return $output;
   }

   function query_loop_db($query, $default='', $prefix='<option', $suffix='</option>', $found_str='selected') {
    global $CFG;

   	$output = '';
   	$CFG->dbh->Free();
   	$result = $CFG->dbh->Query($query);
   	$num_fields = $CFG->dbh->FieldCount;
   	if($num_fields == 3) {
	   	while (list($val, $label1, $label2) = $CFG->dbh->FetchArray($result)) {
	   		if(is_array($default)) $selected = empty($default[$val]) ? '' : $found_str;
	   		else $selected = ($val == $default) ? $found_str : '';
				$label = substr($label, 0, 30);
	   		$output .= $prefix . ' value="' . $val .'"' . $selected . '>' . $label1 . ' ' . $label2 . $suffix . "\n";
	   	}
	   }	else	{
	   	while (list($val, $label) = $CFG->dbh->FetchArray($result)) {
	   		if(is_array($default)) $selected = empty($default[$val]) ? '' : $found_str;
	   		else $selected = ($val == $default) ? $found_str : '';
				$label = substr($label, 0, 30);
	   		$output .= $prefix . ' value="' . $val .'"' . $selected . '>' . $label . $suffix . "\n";
	   	}
	   }
   	return $output;
   }

/***********************************
**	url processing
***********************************/

	function full_me() {
		if(getenv('REQUEST_URI')) 		$me = getenv('REQUEST_URI');
		elseif(getenv('PATH_INFO')) 	$me = getenv('PATH_INFO');
		elseif($GLOBALS['PHP_SELF']) 	$me = $GLOBALS['PHP_SELF'];

		$HTTPS = getenv('HTTPS');
		$SERVER_PROTOCOL = getenv('SERVER_PROTOCOL');
		$HTTP_HOST = getenv('HTTP_HOST');
		$protocol = (isset($HTTPS) && $HTTPS == 'on') ? 'https://':'http://';
		return $protocol . $HTTP_HOST . $me;
	}

	function me() {
		$me = full_me();
		$me = substr($me, -1, 1) == '/' ? 'index.php':$me;
		if($commapos = strpos($me, '?')) return substr($me, 0, $commapos);
		else return $me;
	}

	function redirect($url, $message='', $delay=0) {
		echo "<meta http-equiv='Refresh' content='$delay; url=$url'>\n";
		if(! empty($message)) echo "<div style='font-family: Arial, Sans-serif; font-size: 12pt;' align=center>$message</div>";
		die;
	}

 	function query_str($remove='') {
		if(! is_array($remove))	$remove = array($remove);
		$buffer = '';
		foreach($_GET as $key => $val)	{
			if(in_array($key, $remove))	continue;
			$buffer .= '&' . $key . '=' . $val;
		}
		$buffer = $buffer != '' ? '?' . substr($buffer, 1):'';
		return $buffer;
	}

/***********************************
**	error/debug processing
***********************************/

	function local_error($error_msg) {
		print('<center><font color="red">' . $error_msg . "</font></center><br>\n");
	}

	function throw_error($error_msg='') {
		global $CFG, $T;

		$T->set_var('content', '');

		if(defined('ADMIN')) {
			$T->set_file('page', 'admin/tmpl.page.inc');
			$T->set_file('content', 'admin/tmpl.error.inc');
		}	else	{
			$T->set_file('page', 'tmpl.page.inc');
			$T->set_file('content', 'tmpl.error.inc');
		}

		$T->set_var(array(
								  	'me'			=>	me(),
								  	'full_me'	=> full_me(),
									'base'		=> $CFG->wwwroot,
									'category'	=> $d_category));

		if($error_msg != '') $msg = $error_msg;
		elseif($_SESSION['error_msg'] != '') $msg = $_SESSION['SESSION']['error_msg'];
		elseif($_SESSION['login']['error_msg'] != '') $msg = $_SESSION['login']['error_msg'];
		else $msg = 'an unknown error has occured';
		unset($_SESSION['error_msg']);
		unset($_SESSION['login']['error_msg']);
		$T->set_var('error_msg', $msg);

		if(! defined('ADMIN'))	include($CFG->tmpl_header);
		push_template();
		if(! defined('ADMIN'))	include($CFG->tmpl_footer);
		die;
	}

	function debug_var($var, $var_name='') {
		$tab = strlen($var_name) > 6 ? "\t":"\t\t";
		$var_name = $var_name != '' ? '$' . $var_name . ' ' . $tab . ' = ':'';
		print('<pre> ' . $var_name);
		var_dump($var);
		print("</pre>\n");
	}

/***********************************
**	misc
***********************************/
	function valid_email($address)	{
		$ret_val = false;
		if(eregi("^[0-9a-z]([-_.]?[0-9a-z])*@[0-9a-z]([-.]?[0-9a-z])*\\.[a-z]{2,3}$", $address, $check))	{
			$host = substr($address, strpos($address, '@')+1);

			$host=$host . '.';
			if(function_exists('getmxrr'))	{
				if(getmxrr($host, $mxhosts) == false && gethostbyname($host) == $host) $ret_val = false;
				else	$ret_val = true;
			}	else	{
				if(gethostbyname($host) == $host) $ret_val = false;
				else	$ret_val = true;
			}
		}
		return $ret_val;
	}


	function gz_readfile($file_path)	{
		if(! $zp = @gzopen($file_path, 'rb')) throw_error('could not open file: <b>' . basename($file_path) . '</b> for reading');
		while(! gzeof($zp))
			$buffer .= @gzread($zp, 4096);

		@gzclose($zp);
		return $buffer;
	}

	function create_object($classname, $constructor_param = '') {
		global $CFG;

		require_once($CFG->libdir . '/class/class.' . $classname . '.inc');
		if($constructor_param == '') $obj = new $classname;
		else $obj = new $classname($constructor_param);
		return $obj;
	}
?>