<?php
/*
** $Id: view.php,v 1.3 2003/09/14 19:44:34 damonp Exp $
**
** Copyright (C) 2002 siliconsys.com <dev@siliconsys.com>
** See LICENSE file in root directory of this installation for
** licensing information.
*/

	define('ADMIN', true);

	require_once('../include/config.inc');

	$url_image	= passed_var('image', 'get', 'string');

/*************************************
 * main
 ************************************/
 	$prod_dir	= str_replace($CFG->dirroot, $CFG->wwwroot, $CFG->prod_dir);
	$image_dir	= str_replace($CFG->dirroot, $CFG->wwwroot, $CFG->imagedir);

	$image_path	= ($url_image != '' && file_exists($CFG->prod_dir . '/' . $url_image)) ? $url_image:'not_available.png';
	$image_path = $image_path == 'not_available.png' ? $image_dir . '/not_available.png':$prod_dir . $url_image;

	$T->set_file('page', 'admin/tmpl.image_view.inc');
	$T->set_var('image', $image_path);
	push_template();
?>