<?php
/*
** $Id: config.php,v 1.3 2003/09/16 20:31:48 damonp Exp $
**
** Copyright (C) 2002 siliconsys.com <dev@siliconsys.com>
** See LICENSE file in root directory of this installation for
** licensing information.
*/


	define('ADMIN', true);
	define('SECTION', 'config');

	require_once('../include/config.inc');

	$catalog_id		= passed_var('catalog_id', 'both', 'integer');
	$table			= passed_var('table');

	ksort($CFG->menu_config);
	foreach($CFG->menu_config as $menu_item)
		$menu_config .= "<tr class=\"box\"><td nowrap><img src=\"{base}/image/pixel_white.png\" height=\"1\" width=\"5\">$menu_item</td></tr>\n";
	$T->set_var('menu_config', $menu_config);
	$T->set_file('menu_box_content', 'admin/tmpl.menu_config.inc');
	$T->parse('menu_config', 'menu_config');

/****************
 * main
 ***************/
	switch($function) {
/****************
 * categories
 ***************/
		case('categories'):
			$cat_list = true;
			if(isset($form['action'])) {
				switch($form['action']) {
					case('add'):
						$CFG->dbh->Query("INSERT
												INTO phpcatalog_categories
													(catalog_id, category_name, parent_id)
												VALUES
													('$CFG->catalog_id', '$form[category_name]', '$form[parent_id]')",
												__FILE__, __LINE__);
						push_msg("category: <b>$form[category_name]</b> added", 'info', $CFG->msg);
						break;
					case('delete'):
						delete_category($id);
						break;

					case('edit'):
						$CFG->dbh->Query("UPDATE phpcatalog_categories
												SET
													category_name = '$form[category_name]'
												WHERE category_id = $form[id]",
												__FILE__, __LINE__);
						push_msg("category: <b>$form[category_name]</b> updated", 'info', $CFG->msg);
						break;
				}
			}	else {
				switch($action) {
					case('add'):
						$r = get_category_info($id, false);
						$T->set_var('parent_name', $r->category_name);
						$T->set_file('content', 'admin/tmpl.category_form.inc');
						$cat_list = false;
						break;
					case('edit'):
						$r = get_category_info($id);
						$T->set_file('content', 'admin/tmpl.category_form.inc');
						$cat_list = false;
						break;
				}
			}
			if($cat_list) @category_list(0);
			break;
/****************
 * config
 ***************/
		case('config'):
			if(strtolower($form['submit']) == 'save') {
				foreach($form as $key => $val) {
					if(substr($key, 0, 4) == 'cfg_') {
						$short_key = str_replace('cfg_', '', $key);
						if(is_array($val)) {
							foreach($val as $cur_key => $cur_val) {
								$arr_key = $short_key . '_' . $cur_key;
								$config_arr[$arr_key] = $cur_val;
							}
						}	else	$config_arr[$short_key] = $val;
					}
				}

				if(is_array($config_arr)) {
					foreach($config_arr as $key => $val)	{
						$val = process_db_output($val);

						if($val != '')	{
							$CFG->dbh->Query("SELECT config_value
													FROM phpcatalog_config
													WHERE catalog_id = $form[catalog_id]
														AND config_key = '$key'",
													__FILE__, __LINE__);
							if($CFG->dbh->RowCount > 0) {
								$CFG->dbh->Query("UPDATE phpcatalog_config
														SET config_value = '$val'
														WHERE catalog_id = $form[catalog_id]
															AND config_key = '$key'",
													__FILE__, __LINE__);
							}	else {
								$CFG->dbh->Query("INSERT
														INTO phpcatalog_config
														VALUES ('', '$form[catalog_id]', '$key',
														'$val')",
														 __FILE__, __LINE__);
							}
						}	else {
							$CFG->dbh->Query("DELETE
													FROM phpcatalog_config
													WHERE catalog_id = $form[catalog_id]
														AND config_key = '$key'",
													__FILE__, __LINE__);
						}
					}
				}

				$config = get_config($CFG->catalog_id, true);
				push_msg("configuration for catalog: <b>" . $config['catalog_name'] . "</b> updated", 'info', $CFG->msg);
			}	else {
				$config = get_config($CFG->catalog_id, true);
				$T->set_file('content', 'admin/tmpl.config.inc');
				$T->set_var(array(
								'currency_loc_' . $config['currency_location']		=> ' checked'));

				foreach($config as $key => $val)
					$T->set_var($key, $val);
			}
			break;
	}

	push_template();

/****************
 * functions
 ***************/
	function category_list($id, &$row_color, $indent='') {
		global $CFG, $T;

		set_default_null($id, 0);
		$indent .= '&nbsp;&nbsp;&nbsp;';
		$T->set_file('content', 'admin/tmpl.category_list.inc');
		$T->set_block('content', 'category_row', 'category_rows');
		$dbh = dbi_conn($CFG->dbtype);
		$dbh->Query("SELECT category_id, category_name
						FROM phpcatalog_categories
						WHERE parent_id = $id
							AND catalog_id = $CFG->catalog_id
						ORDER BY category_name",
						__FILE__, __LINE__);
		while($r = $dbh->FetchObject('', 1)) {
			$row_color = $row_color == $CFG->color_tbl1 ? $CFG->color_tbl2:$CFG->color_tbl1;
			$T->set_var(array(
								'row_color'			=> $row_color,
								'category_name'	=> stripslashes($r->category_name),
								'category_id'		=> $r->category_id,
								'indent'				=> $indent));
			$T->parse('category_rows', 'category_row', true);
			category_list($r->category_id, $row_color, $indent);
		}
	}

	function delete_category($id) {
		global $CFG;

		$r = get_category_info($id);
		$category_name = $r->category_name;
		if($r->parent_id == 0) throw_error("you cannot delete the top level category: <b>$category_name</b>");
		$CFG->dbh->Query("SELECT product_id
								FROM $CFG->phpc_table
								WHERE category_id = $id",
								__FILE__, __LINE__);
		if($CFG->dbh->RowCount > 0) throw_error("category: <b>$category_name</b> contains <b>" . $CFG->dbh->RowCount . "</b> products. delete products first");
		$CFG->dbh->Query("SELECT category_id
								FROM phpcatalog_categories
								WHERE parent_id = $id",
								__FILE__, __LINE__);
		if($CFG->dbh->RowCount > 0) throw_error("category: <b>$category_name</b> contains <b>" . $CFG->dbh->RowCount . "</b> children. delete children first");
		$CFG->dbh->Query("DELETE
								FROM phpcatalog_categories
								WHERE category_id = $id",
								__FILE__, __LINE__);
		push_msg("category: <b>$category_name</b> deleted", 'info', $CFG->msg);
		return true;
	}
?>