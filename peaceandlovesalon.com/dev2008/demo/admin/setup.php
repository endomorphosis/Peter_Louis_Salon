<?php
/*
**	$Id: setup.php,v 1.3 2003/09/14 19:44:34 damonp Exp $
**
** Copyright (C) 2002 siliconsys.com <dev@siliconsys.com>
** See LICENSE file in root directory of this installation for
** licensing information.
*/

	define('SECTION', 'setup');
	define('NO_PROCESS', true);
	define('ADMIN', true);

	if(file_exists('../include/config.inc'))
		include_once('../include/config.inc');
	else	{
		echo "Cannot find config.inc, cannot continue.";
		die();
	}
	$function	= 'user';
	$action 		= passed_var('action');
	$filename 	= passed_var('filename');

	error_reporting(E_ALL & ~E_NOTICE);

	if(! isset($_SESSION['SESSION']['count'])) $_SESSION['SESSION']['count'] = 0;
	else $_SESSION['SESSION']['count']++;
	if(strtolower($form['submit']) == 'save' && $form['filename'] != '')	file_edit_save($form);

?>
<html>
<head>
<title>PHPCatalog Setup</title>
<style type="text/css">
<!--
body { font-family: Arial,Helvetica,sans-serif; font-size: 10pt; color: #000000; }
a:visited { COLOR: blue; }
td { font-family: Arial,Helvetica,sans-serif; font-size: 10pt; padding:0px 0px 0px 20px}
h1 { font-size: 12pt; color: black; font-family: Arial,Helvetica,sans-serif; color: #00309C; }
-->
</style>
</head>
<?

/* Handle special cases */
	if(isset($action)) {
		switch($action) {
			case('db'):
				$_SESSION['SESSION']['step'] = 'db';
				break;

			case('db_check'):
				$_SESSION['SESSION']['step'] = 'db_check';
				break;

			case('db_drop'):
				$_SESSION['SESSION']['step'] = 'db_drop';
				break;

			case('edit'):
				if(file_exists($CFG->dirroot . '/include/config.inc'))	file_edit_form($filename);
				break;

			case('phpinfo'):
				phpinfo();
				exit;
				break;

			case('reset'):
				$_SESSION['SESSION']['count'] = 0;
				print("The session counter has been reset.<br>");
				$show_info = true;
				break;

			case('session_reset'):
				$_SESSION['SESSION'] = null;
				print("The session has been reset.<br>");
				$show_info = true;
				break;

			case('user'):
				$_SESSION['SESSION']['step'] = 'user';
				break;

			case(''):
			default:
				$_SESSION['SESSION']['step'] = '';
				$show_info = true;
				break;
		}
	}

	if($show_info)	{
		/* Server variables */
		$server	= getenv('SERVER_SOFTWARE');
		$port		= getenv('SERVER_PORT');
		$name		= getenv('SERVER_NAME');
		$lang		= getenv('HTTP_ACCEPT_LANGUAGE');

		/* PHP variables */
		$phpver	= split_php_version(phpversion());
		$ftp 		= extension_loaded('ftp');
		$gd		= extension_loaded('gd');
		$gettext	= extension_loaded('gettext');
		$ifx		= extension_loaded('ifx');
		$mysql 	= extension_loaded('mysql');
		$pgsql 	= extension_loaded('pgsql');
		$oci8		= extension_loaded('oci8');
		$xml 		= extension_loaded('xml');
		$gif		= imagetypes() & IMG_GIF;
		$jpeg		= imagetypes() & IMG_JPG;
		$png		= imagetypes() & IMG_PNG;
		$magic	= !get_magic_quotes_runtime();
		$global	= ini_get('register_globals');
		$upload	= ini_get('file_uploads');
?>
<table align="center">
<tr><td align="center">
<h2>PHPCatalog Setup</h2>
</td></tr>
<tr><td height="50">
<h1>Webserver Information</h1>
</td></tr>
<tr><td>
<blockquote>
<table border="0" cellpadding="0" cellspacing="2" bgcolor="#dddddd" width="600">
<tr><td colspan="3" height="10"><img src="../image/pixel_gray.png"></td></tr>
<tr><td width="40%"><li>Server:</td><td width="5%">&nbsp;</td><td width="55%"><b><?php echo $server; ?></b></td></tr>
<tr><td><li>Name:</td><td>&nbsp;</td><td><b><?php echo $name; ?></b></td></tr>
<tr><td><li>Port:</td><td>&nbsp;</td><td><b><?php echo $port; ?></b></td></tr>
<tr><td><li>Language:</td><td>&nbsp;</td><td><b><?php echo $lang; ?></b></td></tr>
<tr><td colspan="3" height="10"><img src="../image/pixel_gray.png"></td></tr>
</table>
</blockquote>
</td></tr>
<tr><td height="50">
<h1>PHP Version</h1>
</td></tr>
<tr><td>
<blockquote>
<table border="0" cellpadding="0" cellspacing="2" bgcolor="#dddddd" width="600">
<tr><td colspan="3" height="10"><img src="../image/pixel_gray.png"></td></tr>
<tr><td colspan="3"><li><a href="<? echo me(); ?>?action=phpinfo">View phpinfo() screen</a></td></tr>
<tr><td width="40%"><li>PHP Version:</td><td width="5%">&nbsp;</td><td width="55%"><b><?php echo phpversion(); ?></b></td></tr>

<?php
		print("<tr><td><li>PHP Major Version:</td><td>&nbsp;</td><td><b>$phpver[major]</b></td></tr>\n");
		if (isset($phpver['minor'])) print("<tr><td><li>PHP Minor Version:</td><td>&nbsp;</td><td><b>$phpver[minor]</b></li>\n");
		if (isset($phpver['subminor'])) print("<tr><td><li>PHP Subminor Version:</td><td>&nbsp;</td><td><b>$phpver[subminor]</b></li>\n");
		print("<tr><td><li>PHP Version Classification:</td><td>&nbsp;</td><td><b>$phpver[class]</b></li>\n");
		if ($phpver['major'] < '4.0') {
			echo '<tr><td colspan="3"><li><font color="red">You need to upgrade to PHP4. PHP3 will not work.</font></td></tr>';
			$requires = 1;
		}	elseif ($phpver['class'] == 'beta' || $phpver['class'] == 'unknown') {
			echo '<tr><td colspan="3"><li><font color="red">This is a beta/prerelease version of PHP4. You need to upgrade to a release version.</font></td></tr>';
			$requires = 1;
	 	}	elseif ($phpver['major'] == '4.0') {
			if($phpver['minor'] < '3') {
				echo '<tr><td colspan="3"><li><font color="red">This version of PHP is not supported. You need to upgrade to a more recent version.</font></td></tr>';
				$requires = 1;
			}	else {
				echo '<tr><td colspan="3"><li><font color="green">You are running a supported version of PHP.</font></td></tr>';
		  	}
	 	}	elseif ($phpver['major'] == '4.1' || $phpver['major'] == '4.2' || $phpver['major'] == '4.3' || $phpver['major'] == '4.4') {
			echo '<tr><td colspan="3"><li><font color="green">You are running a supported version of PHP.</font></td></tr>';
	 	}	else echo '<tr><td colspan="3"><li><font color="orange">Wow, a mystical version of PHP from the future. Let <a href="mailto:dev@siliconsys.com">dev@siliconsys.com</a> know what version you have so we can fix this script.</font></td></tr>';
	 	if (! empty($requires)) echo "<tr><td colspan=\"3\"><li>PHPCatalog requires PHP 4.0.3 or greater.</td></tr>";
?>

<tr><td colspan="3" height="10"><img src="../image/pixel_gray.png"></td></tr>
</table>
</blockquote>
</td></tr>
<tr><td height="50">
<h1>PHP Module Capabilities</h1>
</td></tr>
<tr><td>
<blockquote>
<table border="0" cellpadding="0" cellspacing="2" bgcolor="#dddddd" width="600">
<tr><td colspan="3" height="10"><img src="../image/pixel_gray.png"></td></tr>
<tr><td width="40%"><li>FTP Support:</td><td width="5%">&nbsp;</td><td width="65%"><? echo status($ftp, 'Yes', 'No'); ?></td></tr>
<tr><td><li>GD Libraries Included:</td><td>&nbsp;</td><td><? echo status($gd, 'Yes', 'No'); ?></td></tr>
<tr><td><li>Gettext Support:</td><td>&nbsp;</td><td><? echo status($gettext, 'Yes', 'No'); ?></td></tr>
<tr><td><li>Mcrypt Support:</td><td>&nbsp;</td><td><? echo status($mcrypt, 'Yes', 'No'); ?></td></tr>
<tr><td><li>Informix Support:</td><td>&nbsp;</td><td><? echo status($ifx, 'Yes', 'No'); ?></td></tr>
<tr><td><li>MySQL Support:</td><td>&nbsp;</td><td><? echo status($mysql, 'Yes', 'No'); ?></td></tr>
<tr><td><li>OCI8 Support:</td><td>&nbsp;</td><td><? echo status($oci8, 'Yes', 'No'); ?></td></tr>
<tr><td><li>PostgreSQL Support:</td><td>&nbsp;</td><td><? echo status($pgsql, 'Yes', 'No'); ?></td></tr>
<tr><td><li>XML Support:</td><td>&nbsp;</td><td><? echo status($xml, 'Yes', 'No'); ?></td></tr>
<tr><td colspan="3" height="10"><img src="../image/pixel_gray.png"></td></tr>
</table>
</blockquote>
<?
	if(! ($ifx || $mysql || $oci8 || $pgsql)) $errors[] = "You have no database support built into your PHP version. PHPCatalog cannot run without database support.";
	else {

	}
?>
</td></tr>
<tr><td height="50">
<h1>Miscellaneous PHP Settings</h1>
</td></tr>
<tr><td>
<blockquote>
<table border="0" cellpadding="0" cellspacing="2" bgcolor="#dddddd" width="600">
<tr><td colspan="3" height="10"><img src="../image/pixel_gray.png"></td></tr>
<tr><td width="40%"><li>magic_quotes_runtime set to Off:</td><td width="5%">&nbsp;</td><td width="55%"><? echo status($magic, 'Yes', 'No'); ?></td></tr>
<? if (!$magic) $errors[] = 'magic_quotes_runtime may cause problems with database inserts, etc. Turn it off.'; ?>
<?
	$open_basedir = @get_cfg_var('open_basedir');
	if (! empty($open_basedir)) {
		print("<tr><td colspan=\"3\"><li><font color=\"red\"><b>open_basedir</b> restriction in affect and set to <b>$open_basedir</b>, this may not allow uploading files to this installation directory.</font></td></tr>");
		$pos = strpos($open_basedir, '.');
		if($pos === false) $errors[] = "open_basedir restriction in affect and incorrectly configured. Uploading files will not be possible.";
	}
?>
<tr><td><li>File Uploads:</td><td>&nbsp;</td><td><? echo status($upload, 'Yes', 'No'); ?></td></tr>
<tr><td><li>Register Globals:</td><td>&nbsp;</td><td><? echo status($global, 'Yes', 'No'); ?></td></tr>
<tr><td><li>GIF Image Support:</td><td>&nbsp;</td><td><? echo status($gif, 'Yes', 'No'); ?></td></tr>
<tr><td><li>JPEG Image Support:</td><td>&nbsp;</td><td><? echo status($jpeg, 'Yes', 'No'); ?></td></tr>
<tr><td><li>PNG Image Support:</td><td>&nbsp;</td><td><? echo status($png, 'Yes', 'No'); ?></td></tr>
<?
	if(! ($gif || $jpeg || $png)) $errors[] = 'You have no image support built into your PHP version. Thumbnail generation will be disabled.';
	else {

	}
?>
<tr><td colspan="3" height="10"><img src="../image/pixel_gray.png"></td></tr>
</table>
</blockquote>
</td></tr>
<tr><td height="50">
<h1>PHP Sessions</h1>
</td></tr>
<tr><td>
<blockquote>
<table border="0" cellpadding="0" cellspacing="2" bgcolor="#dddddd" width="600">
<tr><td colspan="3" height="10"><img src="../image/pixel_gray.png"></td></tr>
<tr><td width="100%">
<li>Session counter: <b><?php echo $_SESSION['SESSION']['count']; ?></b></li>
<li>To reload this page: <a href="<? echo me(); ?>">click here</a>. The counter should increment by one each time.</li>
<li>To reset the counter: <a href="<? echo me(); ?>?action=reset">click here</a></li>
<li>To reset the session entirely: <a href="<? echo me(); ?>?action=session_reset">click here</a>. Do this if you get errors and want to start setup again.</li>
</td></tr>
<tr><td colspan="3" height="10"><img src="../image/pixel_gray.png"></td></tr>
</table>
</blockquote>
</td></tr>

<?php
	switch($CFG->dbtype) {
		case('mysql'):
			$table = 'mysql';
			break;
		case('pg'):
			$table = 'template1';
			break;
	}

	if($table != '') $CFG->dbtype;
	$dbh = dbi_conn($type, $CFG->dbname);

?>

<tr><td height="50">
<h1>PHPCatalog Config</h1>
</td></tr>
<tr><td>
<blockquote>
<table border="0" cellpadding="0" cellspacing="2" bgcolor="#dddddd" width="600">
<tr><td colspan="3" height="10"><img src="../image/pixel_gray.png"></td></tr>
<tr><td width="35%"><li>Version:</td><td width="5%">&nbsp;</td><td width="60%"><b><? echo $CFG->appversion; ?></b></td></tr>
<tr><td><li>Dir root:</td><td>&nbsp;</td><td><b><? echo $CFG->dirroot; ?></b></td></tr>
<tr><td colspan="2">&nbsp;</td><td>perhaps: <i><?
	echo str_replace('\\', '/', substr(dirname(__FILE__), 0, -6));
?></i></td></tr>
<tr><td><li>WWW root:</td><td>&nbsp;</td><td><b><? echo $CFG->wwwroot; ?></b></td></tr>
<tr><td colspan="2">&nbsp;</td><td>perhaps: <i><?
	echo str_replace('\\', '/', substr($_SERVER['PHP_SELF'], 0, -16));
?></i></td></tr>
<tr><td><li>Database driver:</td><td>&nbsp;</td><td><b><? echo $CFG->dbtype; ?></b></td></tr>
<tr><td><li>Database name:</td><td>&nbsp;</td><td><b><? echo $CFG->dbname; ?></b></td></tr>
<tr><td><li>Database user:</td><td>&nbsp;</td><td><b><? echo $CFG->dbuser; ?></b></td></tr>
<tr><td><li>Database password:</td><td>&nbsp;</td><td><b><? echo $CFG->dbpass; ?></b></td></tr>
<tr><td><li>Database connection opened:</td><td>&nbsp;</td><td><? echo status($dbh, 'Yes', 'No'); ?></td></tr>
<tr><td colspan="3" height="10"><img src="../image/pixel_gray.png"></td></tr>
</table>
</blockquote>

<?
	if($action != 'edit')	print("To edit these configuration parameters <a href=\"" . me() . "?action=edit&filename=/include/config.inc\">click here</a>");
?>

</td></tr>
<tr><td height="50">
<h1>PHPCatalog Modules</h1>
</td></tr>
<tr><td>
<blockquote>
<table border="0" cellpadding="0" cellspacing="2" bgcolor="#dddddd" width="600">
<tr><td colspan="3" height="10"><img src="../image/pixel_gray.png"></td></tr>
<?
	if(is_array($modules)) {
		if(empty($modules))	{
			print("<tr><td><li>No installed public modules</td></tr>");
		}	else	{
			sort($modules);
			foreach($modules as $module) {
				$module_status = true;
				print("<tr><td width=\"25%\"><li>$module:</td><td width=\"5%\">&nbsp;</td><td width=\"20%\">" . status($module_status, "Initialized", "Error") . "</td><td width=\"50%\">Edit this config file <a href=\"" . me() . "?action=edit&filename=/include/module/$module/$module"."_config.inc\">click here</a></td></tr>");
			}
		}
	}	else print "<tr><td><li><font color=\"red\">This version does not support modules</font></td></tr>";

?>
<tr><td colspan="3" height="10"><img src="../image/pixel_gray.png"></td></tr>
</table>
</blockquote>

<?

	$dbh = dbi_conn($CFG->dbtype, $CFG->dbname);
	if(! $dbh->_db_name && $_SESSION['SESSION']['step'] != '' && $_SESSION['SESSION']['step'] != 'db_drop')
		$errors[] = "Cannot connect to database: <b>$CFG->dbname</b>, check if it exists in $CFG->dbtype. To go back to database creation <a href=\"" . me() . "?action=\">click here</a>";

	$dbh->Query("DROP TABLE IF EXISTS phpc_tmp_privcheck");

	$dbh->Query("CREATE TABLE phpc_tmp_privcheck (tmp int)");

	if($dbh->RowsAffected() >= 0)
		$dbh->Query("DROP TABLE phpc_tmp_privcheck");
	else
		$errors[] = "Cannot create or drop tables on db: <b>$CFG->dbname</b>. Check priviliges for user: <b>$CFG->dbuser</b>.";

?>

</td></tr>
<tr><td height="50">
<h1>Report</h1>
</td></tr>
<tr><td>
<blockquote>
<table border="0" cellpadding="0" cellspacing="2" bgcolor="#dddddd" width="600">
<tr><td colspan="3" height="10"><img src="../image/pixel_gray.png"></td></tr>
<?
	if(! is_array($errors)) {
		define("NO_ERRORS", true);
		print("<tr><td><li><font color=\"green\"><b>No problems found. PHPCatalog should run fine on your system.</b></font></td></tr>");
		if(is_array($msgs))
			foreach($msgs as $msg) print("<tr><td><li><font color=\"green\"><b>$msg</b></font></td></tr>");
	}	else {
		define("NO_ERRORS", false);
		foreach($errors as $error) print("<tr><td><li><font color=\"red\"><b>$error</b></font></td></tr>");
	}
?>
<tr><td colspan="3" height="10"><img src="../image/pixel_gray.png"></td></tr>
</table>
</blockquote>
</td></tr>
<?
	}
?>

<tr><td height="50">

<?
	if(NO_ERRORS) {
		switch($_SESSION['SESSION']['step']) {
			case(''):
?>

<h1>Step: 1</h1>
To initialize the current database <a href="<? echo me(); ?>?action=db">click here</a>.<br>
<font color="red">This will create PHPCatalog tables in the database: <b><? echo $CFG->dbname ?></b>, any old PHPCatalog tables will be overwritten.</font><br>
<?
				break;

			case('db_drop'):
?>
<h1>Step: 2</h1>
<h1>Database Creation</h1>
</td></tr>
<tr><td>
<blockquote>
<table border="0" cellpadding="0" cellspacing="2" bgcolor="#dddddd" width="600">
<tr><td colspan="3" height="10"><img src="../image/pixel_gray.png"></td></tr>
<?

				$r = $dbh->Query("DROP DATABASE IF EXISTS $CFG->dbname", __FILE__, __LINE__);
				print("<tr><td><code>Dropping PHPCatalog database: <b>$CFG->dbname</b></code></td><td width=\"10\">&nbsp;</td><td>" . status($r, 'Yes', 'No') . "</td></tr>\n");
				$r = $dbh->Query("CREATE DATABASE $CFG->dbname;", __FILE__, __LINE__);
				print("<tr><td><code>Creating PHPCatalog database: <b>$CFG->dbname</b></code></td><td width=\"10\">&nbsp;</td><td>" . status($r, 'Yes', 'No') . "</td></tr>\n");

?>
<tr><td colspan="3" height="10"><img src="../image/pixel_gray.png"></td></tr>
</table>
<li>If you recieved no database errors above your database was created correctly.
</blockquote>
To install the tables <a href="<? echo me(); ?>?action=db">click here</a>
<?
				break;

			case('db'):
?>
<h1>Step: 2</h1>
<h1>Database setup</h1>
</td></tr>
<tr><td>
<blockquote>
<table border="0" cellpadding="0" cellspacing="2" bgcolor="#dddddd" width="600">
<tr><td colspan="3" height="10"><img src="../image/pixel_gray.png"></td></tr>
<?

				$dbh = dbi_conn($CFG->dbtype, $table);
				$filename = $CFG->dirroot . '/admin/createdb_' . $CFG->dbtype;
				$r = db_read_sql_file($filename);
				print("<tr><td><code>Creating PHPCatalog base tables</code></td><td width=\"10\">&nbsp;</td><td>" . status($r, 'Yes', 'No') . "</td></tr>\n");
				print("<tr><td><code>Inserting default catalog configuration</code></td><td width=\"10\">&nbsp;</td><td>" . status($r, 'Yes', 'No') . "</td></tr>\n");
				$modules = array_merge($modules, $admin_modules);
				foreach($modules as $module) {
					if(function_exists('m_' . $module . '_createdb')) {
						eval($r = 'm_' . $module . '_createdb();');
						print("<tr><td><code>Creating module: <b>$module</b> tables</code></td><td width=\"10\">&nbsp;</td><td>" . status($r, 'Yes', 'No') . "</td></tr>\n");
					}
				}

				print("<tr><td><code>Inserting default module configuration</code></td><td width=\"10\">&nbsp;</td><td>" . status($r, 'Yes', 'No') . "</td></tr>\n");

?>
<tr><td colspan="3" height="10"><img src="../image/pixel_gray.png"></td></tr>
</table>
<li>If you recieved no database errors above your tables should be created correctly.
</blockquote>
To check the tables <a href="<? echo me(); ?>?action=db_check">click here</a>
<?
				break;

			case('db_check'):

?>
<h1>Step: 3</h1>
<h1>Database Check</h1>
<blockquote>
<li><font color="green"><b>All tables check out</b></font><br>
</blockquote>
<?
				if(defined('USER')) print("To edit the default admin user <a href=\"" . me() . "?action=user\">click here</a>\n");
				else {
?>
<h1>Complete!</h1>
<h1><font color="green"><b>PHPCatalog installation completed!</b></font></h1>
<p>Now delete this setup file and go to: <a href="<? echo $CFG->wwwroot; ?>/admin/config.php?function=config"><? echo $CFG->wwwroot; ?>/admin/config.php</a> to complete the configuration of the default catalog.</p>
<p>Be sure to comment out the following line in <? echo $CFG->libdir; ?>/config.inc to remove debugging information from the page display:</p>
<blockquote>
<code>
	define('DEBUG', false);		// uncomment this line for debugging
</code>
</blockquote>
<?
					install($modules);
				}
				break;

			case('user'):
				if(strtolower($form['submit'])) {
					$pwd = crypt($form['password'], $CFG->m_user_salt);
					$CFG->dbh->Query("UPDATE phpcatalog_users
											SET
												user_email		= '$form[email]',
												user_password 	= '$pwd',
												user_name		= '$form[user_name]'
											WHERE user_id = '1'");
?>
<h1>Step: 4</h1>
<h1>Create admin user</h1>
</td></tr>
<tr><td>
<blockquote>
<table border="0" cellpadding="0" cellspacing="2" bgcolor="#dddddd" width="600">
<tr><td colspan="3" height="10"><img src="../image/pixel_gray.png"></td></tr>
<tr><td colspan="3"><font color="green"><li><b>User creation completed</b></font></td></tr>
<tr><td>User:</td><td width="5%">&nbsp;</td><td><b><? echo $form['user_name']; ?></b></td></tr>
<tr><td>Password:</td><td>&nbsp;</td><td><b><? echo $form['password']; ?></b></td></tr>
<!-- <tr><td><li>:</td><td>&nbsp;</td><td><? echo status(true, 'Yes', 'No'); ?></td></tr> -->
<tr><td colspan="3" height="10"><img src="../image/pixel_gray.png"></td></tr>
</table>
</blockquote>
<h1>Complete!</h1>
<h1><font color="green"><b>PHPCatalog installation completed!</b></font></h1>
<p>Now delete this setup file and go to: <a href="<? echo $CFG->wwwroot; ?>/admin/config.php?function=config"><? echo $CFG->wwwroot; ?>/admin/config.php</a> to complete the configuration of the default catalog.</p>
<p>Be sure to comment out the following line in <? echo $CFG->libdir; ?>/config.inc to remove debugging information from the page display:</p>
<blockquote>
<code>
	define('DEBUG', false);		// uncomment this line for debugging
</code>
</blockquote>
<?
					install($modules);
				}	else {
?>
<h1>Step: 4</h1>
<h1>Create admin user</h1>
<table border="0" cellpadding="0" cellspacing="2" bgcolor="#dddddd" width="600">
<tr><td colspan="3" height="10"><img src="../image/pixel_gray.png"></td></tr>
<form method="post" action="<? echo me(); ?>">
<input type="hidden" name="action" value="user">
<tr><td>Enter the admin user name:</td><td><input type"=text" name="user_name"></td></tr>
<tr><td>Enter the admin password:</td><td><input type="text" name="password"></td></tr>
<tr><td>Enter the admin email:</td><td><input type="text" name="email"></td></tr>
<tr><td colspan="2"><input type="submit" name="submit" value="submit"></td></tr>
</form>
<tr><td colspan="3" height="10"><img src="../image/pixel_gray.png"></td></tr>
</table>
<?
				}
				break;
			}
		}

?>
</td></tr>
<tr><td>
<br><br><br><br>
<center><font size="1">PHPCatalog&#8482; <? echo $CFG->appversion; ?> &copy; 2002 <a href="http://www.siliconsys.com/" target="_new">siliconsys.com</a></font></center>
<br><br>
</td></tr>
</table>
</body>
</html>

<?

/***********************************
**	functions
***********************************/
	function status($var, $yes, $no) {
		if ($var) return "<font color=\"green\"><b>$yes</b></font>";
		else return "<font color=\"red\"><b>$no</b></font>";
	}

	function split_php_version($version) {
		if (strlen($version) >= 3 && $version[1] == '.') {
			 $phpver['major'] = substr($version, 0, 3);
			 $version = substr(strtolower($version), 3);
		} else {
			 $phpver['major'] = $version;
			 $phpver['class'] = 'unknown';
			 return $phpver;
		}
		if ($version[0] == '.') $version = substr($version, 1);
		$s = strspn($version, '0123456789');
		if ($s == 0) {
			 $phpver['subminor'] = $version;
			 $phpver['class'] = 'beta';
			 return $phpver;
		}
		$phpver['minor'] = substr($version, 0, $s);
		if ((strlen($version) > $s) && ($version[$s] == '.' || $version[$s] == '-')) $s++;
		$phpver['subminor'] = substr($version, $s);
		if ($phpver['subminor'] == 'cvs' || $phpver['subminor'] == 'dev' || substr($phpver['subminor'], 0, 2) == 'rc') {
			 unset($phpver['subminor']);
			 $phpver['class'] = 'dev';
		}	else {
			 if (!$phpver['subminor']) unset($phpver['subminor']);
			 $phpver['class'] = 'release';
		}
		return $phpver;
	}

	function file_edit_form($filename) {
		global $CFG;

		if(defined('EDIT')) return false;
		define('EDIT', true);
		$filename = $CFG->dirroot . $filename;
		if(! is_writeable($filename))	local_error('<b>' . basename($filename) . '</b> is not writeable on your system.  I cannot save this file.');
		if($fp = @fopen($filename, 'r'))
			if($edit_file = @fread($fp, filesize($filename)))
				if(@fclose($fp)) {
?>
filename: <b><? echo $filename; ?></b><br>
<font color="red"><b>Only edit the fields in the specified configuration area, unless you know what you are doing.</b></font>
<form action="<? echo me();?>" method="post">
<input type="hidden" name="filename" value="<? echo $filename ?>">
<textarea name="edit_file" cols="80" rows="20" wrap="off" class="input">
<? echo $edit_file; ?>
</textarea><br>
<input type="submit" name="submit" value="save">
</form>
<br><br><br><br>
<center><font size="1">PHPCatalog&#8482; <? echo $CFG->appversion; ?> &copy; 2002 <a href="http://www.siliconsys.com/" target="_new">siliconsys.com</a></font></center>
<br><br>
</body>
</html>

<?
					die;
				}	else	local_error("cannot close file: <b>$filename</b>");
			else	local_error("cannot read file: <b>$filename</b>");
		else	local_error("cannot open file: <b>$filename</b> for reading");
	}

	function file_edit_save($form) {
		global $CFG;

		$filename = $form['filename'];
		if($fp = @fopen($filename, 'w')) {
			$edit_file = stripslashes($form['edit_file']);
			if(@fwrite($fp, $edit_file, strlen($edit_file))) {
				if(@fclose($fp)) {
?>
<li><font color="green"><b>File: <? echo basename($filename); ?> written</b></font><br>
<?
					return true;
				}	else	local_error("cannot close file: <b>$filename</b>");
			}	else	local_error("cannot read file: <b>$filename</b>");
		}	else	local_error("cannot open file: <b>$filename</b> for reading");
	}
?>