/*
** $Id: admin.js,v 1.2 2003/09/16 20:31:48 damonp Exp $
**
** Copyright (C) 2002 siliconsys.com <dev@siliconsys.com>
** See LICENSE file in root directory of this installation for
** licensing information.
*/

	var Ver4 = parseInt(navigator.appVersion) >= 4
	var Nav4 = ((navigator.appName == "Netscape") && Ver4)
	var IE4 = ((navigator.userAgent.indexOf("MSIE") != -1) && Ver4)

	function pop_win(loc, win, ht, wd, scroll) {
		if(scroll == '') scroll = 'yes';
  		popwin = document.open(loc,win,"width="+wd+",height="+ht+",toolbar=no,resizable=yes,scrollbars="+scroll);
 	}

	function help_win(sn, fn) {
  		popwin = document.open("./help.php?section="+sn+"&function="+fn,"help","width=450,height=500,toolbar=no,resizable=yes,scrollbars=yes");
 	}

	function confirm_action(url, action) {
		var is_confirmed = confirm("Are you sure you want to " + action + "?");
		if(is_confirmed) {
			window.location.href = url;
		}
	}

	function confirm_submit(form_name, action, id) {
		var is_confirmed = confirm("Are you sure you want to " + action + "?");
		if(is_confirmed) {
			document[form_name].id.value = id;
			document[form_name].submit();
		}
	}

	function submit_cat()	{
		if(document.cat_form.sel_category.options.selectedIndex != 0 &&
					document.cat_form.sel_category.options.selectedIndex != '')	{
			document.cat_form.submit();
		}	else return false;
	}

	function flash_blurb(blurb) {
		this.text.innerText = blurb;
	}

	function highlight(row, new_color) {
	    if (typeof(row.style) == 'undefined' || typeof(row.cells) == 'undefined') {
	        return false;
	    }
	    var num_rows = row.cells.length;
	    for (var c = 0; c < num_rows; c++) {
	        row.cells[c].bgColor = new_color;
	    }
	    return true;
	}

	function isDigit(string) {
	    if (! string) return false;
	    var Chars = "0123456789-.";

	    for (var i = 0; i < string.length; i++) {
	       if (Chars.indexOf(string.charAt(i)) == -1)
	          return false;
	    }
	    return true;
	}

	function checkDecimals(string) {
	    var Chars = ".";
	    var count = 0;

	    for (var i = 0; i < string.length; i++) {
	       if (Chars.indexOf(string.charAt(i)) != -1)
	         ++count;
	    }
	    if(count > 1)
	    	return false;
	    else
	    	return true;
	}
