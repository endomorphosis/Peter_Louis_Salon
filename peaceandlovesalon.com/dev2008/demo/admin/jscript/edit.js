/*
** $Id: edit.js,v 1.2 2003/09/16 20:31:48 damonp Exp $
**
** Copyright (C) 2002 siliconsys.com <dev@siliconsys.com>
** See LICENSE file in root directory of this installation for
** licensing information.
*/

	function validate()
	{
	   if(document.product_form.product_sku.value == ""){
	      alert("Please enter an SKU code.");
	      document.product_form.product_sku.focus();
	      return false;
	   }
	   if(document.product_form.product_name.value == ""){
	      alert("Please enter a product name.");
	      document.product_form.product_name.focus();
	      return false;
	   }
	   if(document.product_form.product_price.value == ""){
	      alert("Please enter the price.");
	      document.product_form.product_price.focus();
	      return false;
	   }
	   if(! isDigit(document.product_form.product_price.value) ||
	   			! checkDecimals(document.product_form.product_price.value)) {
	      alert("Product price must be a decimal number.");
	      document.product_form.product_price.focus();
	      return false;
	   }
	   if(document.product_form.product_category.value == "" &&
	   		document.product_form.select_category_input.value == "%") {
	      alert("Please enter a category or select from the pull-down.");
	      document.product_form.product_category.focus();
	      return false;
	   }
	   if(document.product_form.product_desc.value == ""){
	      alert("Please enter a description.");
	      document.product_form.product_desc.focus();
	      return false;
	   }
	   return true;
	}
