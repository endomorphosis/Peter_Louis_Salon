jQuery("#users").jqGrid({        
   	url:'ajax_users_view.php?nd='+new Date().getTime(),
	datatype: "json",
	height:300,
   	colNames:['Id','FName', 'LName','Discount','Edit','Del'],
   	colModel:[
   		{name:'id',index:'id', width:120},
   		{name:'fname',index:'fname', width:120},
		{name:'lname',index:'lname', width:120},
		{name:'discount',index:'discount', width:120},
		{name:'discount',index:'discount', width:120},
		{name:'email',index:'email', width:120}
   	],
   	rowNum:15,
//   	rowList:[10,20,30],
   	imgpath: 'images',
   	pager: jQuery('#pagerb'),
   	sortname: 'id',
    viewrecords: true,
    sortorder: "asc"
});
var timeoutHnd;
var flAuto = false;

function doSearch(ev){
	if(!flAuto)
		return;
//	var elem = ev.target||ev.srcElement;
	if(timeoutHnd)
		clearTimeout(timeoutHnd)
	timeoutHnd = setTimeout(gridReload,500)
}

function gridReload(){
	var fname = jQuery("#fname").val();
	var lname = jQuery("#lname").val();
	jQuery("#users").setUrl("ajax_users_view.php?fname="+fname+"&lname="+lname);
	jQuery("#users").setPage(1);
	jQuery("#users").trigger("reloadGrid");
}
function enableAutosubmit(state){
	flAuto = state;
	jQuery("#submitButton").attr("disabled",state);
}
