<link rel="stylesheet" type="text/css" media="screen" href="themes/grid.css" />

<script src="js/jquery.js" type="text/javascript"></script>
<script src="js/jquery.jqDynTabs.js" type="text/javascript"></script>
<script src="js/jquery.jqGrid.js" type="text/javascript"></script>



<div class="h">Search By:</div>
<div>
	First Name<br />
	<input type="text" id="fname" onkeydown="doSearch(arguments[0]||event)" />
	<input type="checkbox" id="autosearch" onclick="enableAutosubmit(this.checked)"> Enable Autosearch <br/>
</div>
<div>
	Last Name<br>
	<input type="text" id="lname" onkeydown="doSearch(arguments[0]||event)" />
	<button onclick="gridReload()" id="submitButton" style="margin-left:30px;">Search</button>
</div>
<div>
	Email<br>
	<input type="text" id="email" onkeydown="doSearch(arguments[0]||event)" />
</div>

<br />
<table id="users" class="scroll" cellpadding="0" cellspacing="0">
</table>
<div id="pagerb" class="scroll" style="text-align:center;"></div>
<script src="ajax_users.js" type="text/javascript"> </script>
<br />
