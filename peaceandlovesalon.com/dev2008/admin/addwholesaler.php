<?php session_start();
if(!isset($_SESSION[USER_NAME])){
?>
<script>
var str="index.php";
location.replace(str);
</script>
<?
}
?>
<script type="text/javascript" src="js/admin.js"></script>
<script src="js/jquery.js" type="text/javascript" language="javascript"></script>
<script language="javascript">
//<!---------------------------------+
//  Developed by Roshan Bhattarai 
//  Visit http://roshanbh.com.np for this script and more.
//  This notice MUST stay intact for legal use
// --------------------------------->
$(document).ready(function()
{
	
	$("#username").blur(function()
	{
		
		if(document.getElementById('username').value!=''){
		//remove all the class add the messagebox classes and start fading
		$("#msgbox").removeClass().addClass('messagebox').text('Checking...').fadeIn("slow");
		//check the username exists or not from ajax
		$.post("user_availability.php",{ user_name:$(this).val()} ,function(data)
        {
		  if(data=='no') //if username not avaiable
		  {
		  	$("#msgbox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
			  //add message and change the class of the box and start fading
			  $(this).html('UserName exists').addClass('messageboxerror').fadeTo(900,1);
			});		
          }
		  else
		  {
		  	$("#msgbox").fadeTo(200,0.1,function()  //start fading the messagebox
			{ 
			  //add message and change the class of the box and start fading
			  $(this).html('Available').addClass('messageboxok').fadeTo(900,1);	
			});
		  }
				
        });
		}
 
	});

	
	
	$("#email").blur(function()
	{
		
		if(document.getElementById('email').value!=''){

		//remove all the class add the messagebox classes and start fading
		$("#msgbox1").removeClass().addClass('messagebox').text('Checking...').fadeIn("slow");
		//check the username exists or not from ajax
		if(checkemail($(this).val())){
		
		$.post("user_availability.php",{ email:$(this).val() } ,function(data)
        {
		  if(data=='no') //if username not avaiable
		  {
		  	$("#msgbox1").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
			  //add message and change the class of the box and start fading
			  $(this).html('Email exists').addClass('messageboxerror').fadeTo(900,1);
			});		
          }
		  else
		  {
		  	$("#msgbox1").fadeTo(200,0.1,function()  //start fading the messagebox
			{ 
			  //add message and change the class of the box and start fading
			  $(this).html('Email available').addClass('messageboxok').fadeTo(900,1);	
			});
		  }
				
        });
			}else
			{
		  	$("#msgbox1").fadeTo(200,0.1,function()  //start fading the messagebox
			{ 
			  //add message and change the class of the box and start fading
			  $(this).html('Email is not valid').addClass('messageboxerror').fadeTo(900,1);	
			});
			
			}	
 
 		}
	});

});
</script>
<style>
.toptext
{
font:Arial, Helvetica, sans-serif;
font-size:14px;
font-weight:bold;
}
.billing
{
font:Arial, Helvetica, sans-serif;
font-size:10px;
font-weight:bold;
}


</style>
<table height="100%" width="54%"align="left" cellpadding="2" cellspacing="0"background="images/bg-cream.gif">
<tr>
<td style="padding-left:50px;" valign="top" align="left">
<form action="add_wholesaler_save.php" id="addwholesale" name="addwholesale" enctype="multipart/form-data" method="post" onSubmit="return IsBlankHolesaler()">
  <table width="641" align="left" style="border:1px solid #000000;">
  <tr>
  	<td height="24" colspan="3" align="center" valign="top" class="toptext">Add New Wholesale User</td>
  </tr>
  <tr>
    <td height="24" colspan="3" align="center" valign="top">
    <?php 
		if($_REQUEST['f']=='y')
		{
		?>
   <div id="msg">Wholesalers Added Successfuly</div> 
		<?php 
		}
		elseif($_REQUEST['f']=='n')
		{
		?>	
   <div id="msg">Wholesalers Added Successfuly</div> 
		<?php 
		}
		?>
    </td>
  </tr>
  <tr>
    <td align="right">First Name<font color="red" size="+1">*</font></td>
    <td width="488" align="left"><input name="fname" type="text" id="fname" value="" size="60" /></td>
  </tr>
  <tr>
    <td align="right">Last Name</td>
    <td align="left"><input name="lname" type="text" id="lname" value="" size="60" /></td>
  </tr>
  <tr>
    <td align="right">Email<font color="red" size="+1">*</font></td>
    <td align="left">
    	<input name="email" type="text" id="email" value="" size="60" />
      <span id="msgbox1" style="display:none"></span>
    </td>
  </tr>
  <tr>
    <td align="right">User Name<font color="red" size="+1">*</font></td>
    <td align="left">
    		<input name="username" type="text" id="username" value="" size="60"/>
       <span id="msgbox" style="display:none"></span>
		</td>
  </tr>
  <tr>
    <td align="right">Password<font color="red" size="+1">*</font></td>
    <td align="left">
    	<input name="pass" type="password" id="pass" value="" size="60" />
    </td>
  </tr>
  <tr>
    <td align="right">Confirm <font color="red" size="+1">*</font></td>
    <td align="left"><input name="conpass" type="password" id="conpass" value="" size="60" /></td>
  </tr>	
  <tr>
    <td align="right">Discount<font color="red" size="+1">*</font> </td>
    <td align="left"><input name="discount" type="text" id="discount" value="" size="60" /></td>
  </tr>	
	<tr>
    <td colspan="2">
      <table border="0" class="addr">
        <tbody>
          <tr>
		  <td style="padding-left:40px;" class="billing">Billing Address</td>
		  <td height="50" class="billing" style="padding-left:40px;">
			Shipping Address<br/>
			<input onClick="copy()" name="usebilling" id="usebilling" type="checkbox">
			Check here if billing and shipping same
		  </td>
		  </tr>
		  <tr>
          	<td width="273" valign="top">
              <table class="bill_addr">
                <tbody>
                  <tr>
                  	<td align="right" class="addr">Title<font color="red" size="+1">*</font></td>
                  	<td align="left" class="addr_val">
                  		<input class="addr" name="title" id="title" value="" size="10" type="text">                    </td>
                  </tr>
                  <tr>
                  	<td align="right" class="addr">
                  		First<font color="red" size="+1">*</font>                    </td>
                  	<td align="left" class="addr_val">
                  		<input class="addr" name="first" id="first" value="" size="18" type="text">                    </td>
                  </tr>
                  <tr>
                  	<td align="right" class="addr">
                  		Middle                  	</td>
                  	<td align="left" class="addr_val">
                  		<input class="addr" name="middle" id="middle" value="" size="18" type="text">                  	</td>
                  </tr>
                  <tr>
                  	<td align="right" class="addr">
                  		Last</td>
                  	<td align="left" class="addr_val">
                  		<input class="addr" name="last" id="last" value="" size="28" type="text">                  	</td>
                  </tr>
                  <tr>
                  	<td align="right" class="addr">
                  		Suffix                  	</td>
                  	<td align="left" class="addr_val">
                  		<input class="addr" name="suffix" id="suffix" value="" size="10" type="text">                    </td>
                  </tr>
                  <tr>
                  	<td align="right" class="addr">
                  		Company                  	</td>
                  	<td align="left" class="addr_val">
                  		<input class="addr" name="company" id="company" value="" size="28" type="text">                  	</td>
                  </tr>
                  <tr>
                  	<td align="right" class="addr">
                  		Address 1
                  		<font color="red" size="+1">*</font>                    </td>
                  	<td align="left" class="addr_val">
                  		<input class="addr" name="address" id="address" value="" size="28" type="text">                  	</td>
                  </tr>
                  <tr>
                  	<td align="right" class="addr">
                  		Address 2                  	</td>
                  	<td align="left" class="addr_val">
                  		<input class="addr" name="address2" id="address2" value="" size="28" type="text">                  	</td>
                  </tr>
                  <tr>
                  	<td align="right" class="addr">
                  		City</td>
                  	<td align="left" class="addr_val">
                  		<input class="addr" name="city" id="city" value="" size="28" type="text">                  	</td>
                  </tr>
                  <tr>
                  	<td align="right" class="addr">
                  		State/Province</td>
                  	<td align="left" class="addr_val">
                    <select class="addr" name="state" id="state">
					<?php 
                      include'database.php';
                      $db = new Database();
                      $sql_states = "SELECT * FROM state ";
                      $rows_states=$db->Get($sql_states);
                      for($i=0;$i<$db->RowsCount;$i++){
                      ?>                       
                      <option value="<?=$rows_states[$i]['stateskey']?>"><?=$rows_states[$i]['states']?></option>
                      <?php } ?>                       
                    </select>                  	
                      </td>
                  </tr>
                  <tr>
                  	<td align="right" class="addr">
                  		Zip Code<font color="red" size="+1">*</font>                  	</td>
                  	<td align="left" class="addr_val">
                  		<input class="addr" name="zip" id="zip" value="1208" size="10" type="text">                  	</td>
                  </tr>
                  <tr>
                  	<td align="right" class="addr">
                  		Country</td>
                  	<td align="left" class="addr_val">
                  		<input name="country" id="country" value="United States" type="text">                  	</td>
                  </tr>
                  <tr>
                  	<td align="right" class="addr">
                  		Phone Number</td>
                  	<td align="left" class="addr_val">
                  		<input class="addr" name="phone" id="phone" value="" size="18" type="text">                  	</td>
                  </tr>
                </tbody>
              </table>          	</td>
          	<td width="288" valign="top">
              <table class="ship_addr">
                <tbody>
                  <tr>
                  	<td align="right" class="addr">
                  		Title<font color="red" size="+1">*</font> </td>
                  	<td class="addr_val">
                  		<input class="addr" name="shiptitle" id="shiptitle" value="" size="10" type="text">                  	</td>
                  </tr>
                  <tr>
                  	<td align="right" class="addr">
                  		First<font color="red" size="+1">*</font>                  	</td>
                  	<td class="addr_val">
                  		<input class="addr" name="shipfirst" id="shipfirst" value="" size="18" type="text">                  	</td>
                  </tr>
                  <tr>
                  	<td align="right" class="addr">
                  		Middle                  	</td>
                  	<td class="addr_val">
                  		<input class="addr" name="shipmiddle" id="shipmiddle" value="" size="18" type="text">                  	</td>
                  </tr>
                  <tr>
                  	<td align="right" class="addr">
                  		Last</td>
                  	<td class="addr_val">
                  		<input class="addr" name="shiplast" id="shiplast" value="" size="28" type="text">                  	</td>
                  </tr>
                  <tr>
                  	<td align="right" class="addr">
                  		Suffix                  	</td>
                  	<td class="addr_val">
                  		<input class="addr" name="shipsuffix" id="shipsuffix" value="" size="10" type="text">                  	</td>
                  </tr>
                  <tr>
                  	<td align="right" class="addr">
                  		Company                  	</td>
                  	<td class="addr_val">
                  		<input class="addr" name="shipcompany" id="shipcompany" value="" size="28" type="text">                  	</td>
                  </tr>
                  <tr>
                  	<td align="right" class="addr">
                  		Shipping Address 1<font color="red" size="+1">*</font>                  	</td>
                  	<td class="addr_val">
                  		<input class="addr" name="shipaddress" id="shipaddress" value="" size="28" type="text">                  	</td>
                  </tr>
                  <tr>
                  	<td align="right" class="addr">
                  		Shipping Address 2                  	</td>
                  	<td class="addr_val">
                  		<input class="addr" name="shipaddress2" id="shipaddress2" value="" size="28" type="text">                  	</td>
                  </tr>
                  <tr>
                  	<td align="right" class="addr">
                  		City</td>
                  	<td class="addr_val">
                  		<input class="addr" name="shipcity" id="shipcity" value="" size="28" type="text">                  	</td>
                  </tr>
                  <tr>
                  	<td align="right" class="addr">
                  		State/Province</td>
                  	<td class="addr_val">
                  		<select class="addr" name="shipstate" id="shipstate">
                    <?php 
											$sql_states = "SELECT * FROM state ";
											$rows_states=$db->Get($sql_states);
											for($j=0;$j<$db->RowsCount;$j++){
                    ?>                       
                    <option value="<?=$rows_states[$j]['stateskey']?>"><?=$rows_states[$j]['states']?></option>
                    <?php } ?>                       
                  		</select>                  
                     </td>
                  </tr>
                  <tr>
                  	<td align="right" class="addr">
                  		Zip Code<font color="red" size="+1">*</font>                  	</td>
                  	<td class="addr_val">
                  		<input class="addr" name="shipzip" id="shipzip" value="1208" size="10" type="text">                  	</td>
                  </tr>
                  <tr>
                  	<td align="right" class="addr">
                  		Country</td>
                  	<td class="addr_val">
                  		<input name="shipcountry" id="shipcountry" value="United States" type="text">                  	</td>
                  </tr>
                  <tr>
                  	<td align="right" class="addr">
                  		Phone Number</td>
                  	<td class="addr_val">
                  		<input class="addr" name="shipphone" id="shipphone" value="" size="18" type="text">                  	</td>
                  </tr>
                </tbody>
              </table>          	</td>
          </tr>
        <tr>
        	<td class="addr_foot" colspan="1">
        		<font color="red" size="+1">*</font> Required Field        	</td>
        </tr>
        </tbody>
      </table>    </td>
  </tr>
  <tr>
    <td colspan="2"><input type="submit" value="submit" /></td>
    <td width="1" height="50" align="left">&nbsp;</td>
  </tr>
</table>
</form>
</td>
</tr>
</table>