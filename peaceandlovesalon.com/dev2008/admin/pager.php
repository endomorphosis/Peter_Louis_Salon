<?
class Pager{
	function Pager($per_page_item, $limit_page){
		define(PER_PAGE, $per_page_item, true);
		define(LIMIT_PAGE, $limit_page, true);
	}
	function MakeUrl($url, $param)
	{
		$size = explode("?", $url);
		$size = sizeof($size);
		//die($size."=");
		
		//$index = strpos("?", $url);
		
		if($size>1){
			$url .= "&";
		}
		else{
			$url .= "?";
		}
		
		$url .= $param;
		
		return $url;
	}
	function Show($curr_page, $total, $url)
	{
		$total_page = ceil($total/PER_PAGE);
		if($curr_page<=0){
			$curr_page = 1;
		}
		if($curr_page>$total_page){
			$curr_page = $total_page;
		}
		$pos = floor($curr_page/LIMIT_PAGE);
		if($curr_page%LIMIT_PAGE==0){
			$pos--;
		}
		$start = ceil($pos * LIMIT_PAGE+1);
		if($start<=0){
			$start = 1;
		}
		else if($start>$total){
			$start = $total;
		}
		$pages = "";
		$end = $start + LIMIT_PAGE;
		if($end>$total_page){
			$end = $total_page;
		}
		if($start>LIMIT_PAGE){
			$link = $this->MakeUrl($url, "page=".($start-1));
			
			$pages .= '<a href="'.$link.'" title="Previous">'.htmlspecialchars('<<').'</a>&nbsp;';
		}
		$counter = 0;
		for($i=$start;$i<=$end;$i++){
			$p = "";
			$link = $this->MakeUrl($url, "page=".($i));
			
			$p .= '<a href="'.$link.'">';
			$p .= $i;	
			$p .= '</a>&nbsp;';
			
			if($curr_page==$i){
				$p = '<span id="curr_page">'.$i."</span>&nbsp;";
			}
			$pages .= $p;
			$counter++;
			if($counter==LIMIT_PAGE){
				$i++;
				break;
			}
		}
		$i--;
		if($i<$total_page){
			$link = $this->MakeUrl($url, "page=".($i+1));
			$pages .= '<a href="'.$link.'" title="Next">'.htmlspecialchars('>>').'</a>&nbsp;';
		}
		$data["PAGER"] = $pages;
		$data["START"] = (($curr_page-1)*PER_PAGE);
		$data["PER_PAGE"] = PER_PAGE;
		return $data;
	}
}

?>

