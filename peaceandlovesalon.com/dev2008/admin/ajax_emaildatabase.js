jQuery("#users").jqGrid({        
   	url:'ajax_emaildatabase_view.php?nd='+new Date().getTime(),
	datatype: "json",
	height:300,
   	colNames:['FName', 'LName', 'Email'],
   	colModel:[
   		{name:'fname',index:'fname', width:220},
   		{name:'lname',index:'lname', width:220},
		{name:'email',index:'email', width:200}
   	],
   	rowNum:15,
//   	rowList:[10,20,30],
   	imgpath: 'images',
   	pager: jQuery('#pagerb'),
   	sortname: 'bill_id',
    viewrecords: true,
    sortorder: "asc"
});
var timeoutHnd;
var flAuto = false;

function doSearch(ev){
	if(!flAuto)
		return;
//	var elem = ev.target||ev.srcElement;
	if(timeoutHnd)
		clearTimeout(timeoutHnd)
	timeoutHnd = setTimeout(gridReload,500)
}

function gridReload(){
	var fname = jQuery("#fname").val();
	jQuery("#users").setUrl("ajax_emaildatabase_view.php?fname="+fname);
	jQuery("#users").setPage(1);
	jQuery("#users").trigger("reloadGrid");
}
function enableAutosubmit(state){
	flAuto = state;
	jQuery("#submitButton").attr("disabled",state);
}
