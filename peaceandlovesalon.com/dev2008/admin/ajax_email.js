jQuery("#users").jqGrid({        
   	url:'ajax_users_view.php?nd='+new Date().getTime(),
	datatype: "json",
	height:300,
   	colNames:['Id','FName', 'LName','User','Email','Discount','Edit','Del'],
   	colModel:[
   		{name:'id',index:'id', width:30},
		{name:'username',index:'username', width:80},
   		{name:'fname',index:'fname', width:150},
		{name:'lname',index:'lname', width:150},
		{name:'email',index:'email', width:100},
		{name:'discount',index:'discount', width:100},
		{name:'discount',index:'discount', width:60},
		{name:'email',index:'email', width:60}
   	],
   	rowNum:15,
//   	rowList:[10,20,30],
   	imgpath: 'images',
   	pager: jQuery('#pagerb'),
   	sortname: 'id',
    viewrecords: true,
    sortorder: "asc"
});
var timeoutHnd;
var flAuto = false;

function doSearch(ev){
	if(!flAuto)
		return;
//	var elem = ev.target||ev.srcElement;
	if(timeoutHnd)
		clearTimeout(timeoutHnd)
	timeoutHnd = setTimeout(gridReload,500)
}

function gridReload(){
	var fname = jQuery("#fname").val();
	var lname = jQuery("#lname").val();
	var email = jQuery("#email").val();
	jQuery("#users").setUrl("ajax_users_view.php?fname="+fname+"&lname="+lname+"&email="+email);
	jQuery("#users").setPage(1);
	jQuery("#users").trigger("reloadGrid");
}
function enableAutosubmit(state){
	flAuto = state;
	jQuery("#submitButton").attr("disabled",state);
}
