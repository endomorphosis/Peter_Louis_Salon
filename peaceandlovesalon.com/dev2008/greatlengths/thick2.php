<table cellspacing="0" cellpadding="0" width="839" border="0"> 
<tbody> 
<tr> 
<td> 
<table id="table1" cellspacing="0" cellpadding="4" width="100%" border="0"> 
<tbody> 
<tr valign="top"> 
<td colspan="2">
			<?php include'sub-menue.php';?>
</td>
</tr>
</tbody>
</table>
</td>
</tr> 
<tr> 
<td>&nbsp;</td></tr> 
<tr> 
<td> 
<table id="table2" cellspacing="0" cellpadding="5" width="100%" align="center" border="0"> 
<tbody> 
<tr> 
<td>
<font class="bodytext">
  <a href="template.php?f=thick1">
  	<img height="38" alt="" src="greatlengths/media/prev.gif" width="46" border="0" />
  </a>
</font>
</td>
</tr> 
<tr> 
<td>
  <img height="437" alt="" src="greatlengths/media/annika05.jpg" width="330" align="left" border="0" /> 
  <font class="bodytext"> 
    <p align="justify">
    Naturally, your hair is exposed to severe strains by heat, wind, dryness and constant friction. Our research has shown that the more healthy human hair is, the better it can cope with these strains in the long run. This is because its natural cuticle layer, combined with regular care, is the best protection for it. In addition, its cuticle layer makes your hair resilient - without its resilience you could not perm your hair and any coloring would soon fade. The cuticle layer of Great Lengths hair is intact, so it will respond to any treatment in the same way, or better, than your own hair would.
    <br/>
    These strands may be removed without any damage to your own hair!
    </p>
    </font>
</td>
</tr>
</tbody>
</table>
</td>
</tr> 
<tr> 
<td>
			<?php include'sub-menue.php';?>
</td>
</tr>
</tbody>
</table>
