<table width="839" border="0" cellspacing="0" cellpadding="0">
<tr> 
<td valign="top"> 
	<?php include'greatlengths/sub-menue.php';?>
</td>
</tr>
<tr>
  <td>
    <table class="body-table" cellspacing="0" cellpadding="5" width="490" align="left" border="0"> 
    <tbody> 
      <tr> 
        <td>
        <img height="389" alt="" src="greatlengths/media/sara02.jpg" width="330" border="0" />
        </td>
        <td valign="top">
          <img height="158" alt="" src="greatlengths/media/sara01.jpg" width="105" border="0" />
          <br />
          <font class="bodytext">This is what many women dream of: long, thick healthy hair flowing down to their shoulders in gentle waves. </font> 
          <p><font class="bodytext">Until now, only few women have had this dream come true because their own hair is only rarely capable of coping with the strains it is exposed to during all the years it needs to grow. </font></p> 
          <p><font class="bodytext">One of the purposes of Great Lengths is to remedy this matter: our system offers all women the opportunity to make this dream come true! </font></p>
        </td>
      </tr> 
      <tr> 
        <td>
          <font class="bodytext"><b>How Long Are You Going to Wait for Long Hair?</b></font> 
          <p><font class="bodytext">Human hair grows approximately 1 cm per month on average. This implies that if you want to have very long hair you have to wait more than 5 years. Use Great Lengths and you will have up to 60-cm long hair within a few hours!</font> </p> 
          <p><font class="bodytext"><a href="template.php?f=long1">
          <img height="38" alt="" src="greatlengths/media/next.gif" width="46" border="0" />
          </a>
          </font>
          </p>
        </td> 
        <td>
          <img height="304" alt="" src="greatlengths/media/sara05.jpg" width="220" border="0" />
        </td>
      </tr>
    </tbody>
    </table>
  </td> 
</tr>
<tr>
<td>
	<?php include'greatlengths/sub-menue.php';?>
</td>
</tr>
</table>
