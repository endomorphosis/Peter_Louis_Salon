<table id="table2" border="0" cellpadding="5" cellspacing="0" width="840">
<tbody>
<tr>
<td colspan="2">
<table id="table1" cellspacing="0" cellpadding="4" width="100%" border="0"> 
<tbody> 
<tr valign="top"> 
<td colspan="2">
			<?php include'sub-menue.php';?>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>



<table id="table2" bgcolor="#e9e9e9" border="0" cellpadding="5" cellspacing="0" width="100%">
<tbody>
<tr>
<td colspan="2" bgcolor="#003366">
<center>
    <font class="bodytext-alt"><b><font color="#FFFFFF">The hairy details - in short!</font></b>      </font>
</center>
</td>
</tr>
<tr>
<td>
  <font style="font-size: 9pt;" face="Verdana">
  <font class="bodytext">
  <br><b>To ensure the highest quality and long lasting new hairstyle, the hair used in your new extensions must have certain essential properties. These properties may be summarised under four headings:</b></font>
  </font>
  <p align="justify">
  <font style="font-size: 9pt;" face="Verdana"><img alt="" src="greatlengths/media/uno.gif" border="0" height="29" width="30"><font class="bodytext">Mother Nature provided hair with a scaly coating known as the "cuticle". The cuticle acts as a natural protection and is responsible for the shine and resilience of your hair, locking in essential moisture and protecting the hair against the harsh elements (wind, sun, pollution, etc.) to which it is exposed.<b> Hair without cuticle would rapidly dry out, become brittle and break, and appear lifeless and dull. (For reasons which are explained later, cuticles are removed from an available source of inferior quality hair.)</b></font> </font></p>
  <p><font style="font-size: 9pt;" face="Verdana"><img alt="" src="greatlengths/media/hair.jpg" border="2" height="139" width="480"> </font></p>
  <p align="justify"><font style="font-size: 9pt;" face="Verdana"><img alt="" src="greatlengths/media/due.gif" border="0" height="29" width="30"><font class="bodytext">In your own natural growth, the individual cuticle scales are naturally aligned in the same direction. Hairs next to each other are easy to comb because of this.<b> But what would happen if the cuticle scales where not all facing the same direction? imagine a velcro fastening. The reason the fastening grips so well is because the burrs which interlock with each other are all aligned in different directions. This is clearly the opposite of what is required for hair exrensions because, in this case, interlocking would soon lead to completely matted and tangled hair.</b></font> </font></p>
  <p align="justify"><font style="font-size: 9pt;" face="Verdana"><img alt="" src="greatlengths/media/tre.gif" border="0" height="29" width="30"><font class="bodytext">When combed wet, natural hair with cuticle will stretch by approximately 10%. Hair in which the cuticle has been removed (later explained) is much more elastic.<b> Because of their different stretch, mixing these two qualities together can produce disasterous results, especially inthe case where the cuticle has not been fully removed from the inferior quality hair and may be facing in the opposite direction to that of your natural hair.</b></font> </font></p>
  <p align="justify"><font style="font-size: 9pt;" face="Verdana"><img alt="" src="greatlengths/media/quattro.gif" border="0" height="29" width="30"><font class="bodytext">The hair of typical European women has an oval cross-section and is relatively thin; it is normally between 0,06mm and 0,09mm thick.<b> The hair used for hair extensions must have the same characteristic in order to provide easy maintenance and a <u>lasting</u> natural appearance and feel.</b></font> </font></p>
</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top"><font style="font-size: 9pt;" face="Verdana"><a href="template.php?f=history1"><img alt="previous" src="greatlengths/media/prev.gif" border="0" height="38" width="46"></a>&nbsp;<a href="template.php?f=history3"><img alt="next" src="greatlengths/media/next.gif" border="0" height="38" width="46"></a> </font>
</td>
</tr>
</tbody>
</table>

<table id="table2" border="0" cellpadding="5" cellspacing="0" width="840">
<tr>
<td>
<table id="table1" cellspacing="0" cellpadding="4" width="100%" border="0"> 
<tbody> 
<tr valign="top"> 
<td colspan="2">
			<?php include'sub-menue.php';?>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</table>