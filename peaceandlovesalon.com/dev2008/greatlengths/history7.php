<table id="table2" border="0" cellpadding="5" cellspacing="0" width="840">
<tbody>
<tr>
<td colspan="2">
<table id="table1" cellspacing="0" cellpadding="4" width="100%" border="0"> 
<tbody> 
<tr valign="top"> 
<td colspan="2">
			<?php include'sub-menue.php';?>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>

<table id="table3" wdith="100%" bgcolor="#e9e9e9" border="0" cellpadding="2" cellspacing="2">
<tbody>
<tr>
<td colspan="2" bgcolor="#003366">
<center>
    <font class="bodytext-alt"><b><font color="#FFFFFF">The Solution</font></b>      </font>
</center>
</td>
</tr>
<tr>
<td colspan="2">
  <font class="bodytext"></font>
  <p align="justify"><font class="bodytext" style="font-size: 9pt;" face="Verdana"><b>Actually, because of its wonderful quality, Indian hair does provide the solution but not with the collection method described before. The solution is based on hair which is collected with all its cuticle perfectly aligned in the same direction. This hair comes in a plait...</b> </font></p>
  <p align="justify"><font style="font-size: 9pt;" face="Verdana"><font class="bodytext">Indian women traditionally cut off their long plait of hair either at their wedding ? as a symbol of starting a new part of their life ? or if they want to ask a special favour or offer thanks to a deity at a religious temple. Collecting and using this hair to produce hair extensions, neatly solves a number of problems:</font> </font></p>
  <p></p>
</td>
</tr>
<tr>
<td>
	<font style="font-size: 9pt;" face="Verdana"><img alt="" src="greatlengths/media/indiana.jpg" border="2" height="380" width="250"></font>
</td>
<td>
  <p align="justify"><font style="font-size: 9pt;" face="Verdana"><img alt="" src="greatlengths/media/point2.gif" border="0" height="18" width="20"><font class="bodytext"><b>The cuticle layer of this hair is still perfectly healthy and intact since, as mentioned, Indian girls rarely visit hair salons to get chemical treatments.</b></font> </font></p>
  <p align="justify"><font style="font-size: 9pt;" face="Verdana"><img alt="" src="greatlengths/media/point2.gif" border="0" height="18" width="20"><font class="bodytext"><b>Weaving the hair into a plait ensures that it is all aligned in one direction.</b></font><br>&nbsp;</font></p>
  <p align="justify"><font style="font-size: 9pt;" face="Verdana"><img alt="" src="greatlengths/media/point2.gif" border="0" height="18" width="20"><font class="bodytext"><b>The hair is homogeneous in colour and thickness.</b></font> </font></p>
  <p align="justify"><font style="font-size: 9pt;" face="Verdana"><font class="bodytext">This precious and high quality hair provides the best solution for an extremely reliable hair extension service.</font> <br>&nbsp;</font></p>
  <center><font style="font-size: 9pt;" face="Verdana"><img alt="" src="greatlengths/media/timbro2.gif" border="0" height="81" width="84"> </font></center>
</td>
</tr>
  <tr>
  <td colspan="2">
  <p align="justify"><font class="bodytext" style="font-size: 9pt;" face="Verdana"><b>However, as we all know, even this wonderful quality hair would be seriously damaged if a traditional bleaching process were used in order to provide all the different colours, including light blondes, needed in hair extensions.</b></font></p>
</td>
</tr>
<tr>
<td colspan="2" align="right">
	<font style="font-size: 9pt;" face="Verdana"><a href="template.php?f=history6"><img alt="previous" src="greatlengths/media/prev.gif" border="0" height="38" width="46"></a>&nbsp;<a href="template.php?f=history8"><img alt="next" src="greatlengths/media/next.gif" border="0" height="38" width="46"></a></font>
</td>
</tr>
</tbody>
</table>


<table id="table2" border="0" cellpadding="5" cellspacing="0" width="840">
<tr>
<td>
<table id="table1" cellspacing="0" cellpadding="4" width="100%" border="0"> 
<tbody> 
<tr valign="top"> 
<td colspan="2">
			<?php include'sub-menue.php';?>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</table>