<table class="topsubmenu" cellspacing="0" cellpadding="0" width="839" border="0"> 
  <tbody> 
    <tr>
      <td>
			<?php include'greatlengths/sub-menue.php';?>
			</td>
    </tr>
    <tr> 
      <td> 
        <table id="table2" cellspacing="0" cellpadding="5" width="100%" align="center" border="0"> 
          <tbody> 
            <tr> 
              <td>
                <font class="bodytext">
                	Great Lengths' birthplace is <b>London</b>, one of the world�s fashion centres. <b>David Gold</b>, founder of Great Lengths, had set himself the goal to develop a hair extension system as simple to use as cutting or drying. When in 1991 he had developed and patented the Great Lengths principle he had simultaneously discovered the first method of thickening thin and fine hair in a very simple way.
            		</font>
              </td> 
              <td width="164">
	              <img height="224" alt="" src="greatlengths/media/c06.jpg" width="164" border="2" />
              </td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr> 
    <tr> 
      <td> 
        <table id="table3" cellspacing="0" cellpadding="5" width="100%" align="center" border="0"> 
          <tbody> 
            <tr> 
              <td width="164">
                <img height="245" alt="" src="greatlengths/media/f08.jpg" width="164" border="2" />
              </td> 
              <td>
                <font class="bodytext">This invention set off a <b>revolution</b> in 1991. For the first time David Gold could offer hair extensions and hair thickening in one system. Great Lengths has become a sign of our times: men - and particularly women - have started preferring non aggressive technical solutions and natural products.</font> 
                <p><font class="bodytext">In this context Great Lengths has also been one step ahead from the start: so far no other system has developed a similarly non compromising&nbsp; technique of attaching additional strands to the customer�s own hair. It goes without saying that <b>only perfectly 100% natural highest quality human</b> hair is used.</font></p>
              </td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr> 
    <tr> 
      <td> 
        <table id="table4" cellspacing="0" cellpadding="5" width="100%" align="center" border="0"> 
          <tbody> 
            <tr> 
              <td>
          	    <font class="bodytext">It is therefore no wonder that Great Lengths should have established itself on the worldwide market within a few years. Today <b>top hairdressers</b> in Great Britain, Italy, France, Austria, Germany, Norway, Finland, Sweden, Denmark, Holland, Belgium, Greece, Spain, Portugal, Switzerland, Luxembourg, the U. S. A., Canada, Brazil, Australia, New Zealand, Russia, Hong Kong, the United Arab Emirates, South Africa, the Caribbean, Japan, Mexico and many more countries are offering Great Lengths with increasing success.</font>
              </td> 
              <td width="164">
            		<img height="238" alt="" src="greatlengths/media/c01.jpg" width="164" border="2" />
            	</td>
            </tr>
          </tbody>
          </table>
      </td>
    </tr> 
    <tr>
      <td>
			<?php include'greatlengths/sub-menue.php';?>
      </td>
    </tr>
  </tbody>
</table>