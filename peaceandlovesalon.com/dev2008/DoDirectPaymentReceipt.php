<?php session_start();

require_once 'CallerService.php';
require_once 'classorder.php';


$order = new Order();
//echo '<br/>';
$paymentType = $order->paymentType;
//echo '<br/>'.$paymentType;
$amount = $order->amount;
//echo '<br/>'.$amount;
$creditCardType = $order->creditCardType;
//echo '<br/>'.$creditCardType;
$creditCardNumber = $order->creditCardNumber;
//echo '<br/>'.$creditCardNumber;
$expDateMonth = $order->expDateMonth;
//echo '<br/>'.$expDateMonth;
$expDateYear = $order->expDateYear;
//echo '<br/>'.$expDateYear;
$cvv2Number = $order->cvv2Number;
//echo '<br/>'.$cvv2Number;
$currencyCode = $order->currencyCode;
//echo '<br/>'.$currencyCode;
$firstName = $order->bfname;
//echo '<br/>'.$firstName;
$lastName = $order->blastname;
//echo '<br/>'.$lastName;
$address1 = $order->baddress;
//echo '<br/>'.$address1;
$city = $order->bcity;
//echo '<br/>'.$city;
$state = $order->bstate;
//echo '<br/>'.$state;
$zip = $order->bzip;
//echo '<br/>'.$zip;





/* Construct the request string that will be sent to PayPal.
   The variable $nvpstr contains all the variables and is a
   name value pair string with & as a delimiter */
$nvpstr="&PAYMENTACTION=$paymentType&AMT=$amount&CREDITCARDTYPE=$creditCardType&ACCT=$creditCardNumber&EXPDATE=".$expDateMonth.$expDateYear."&CVV2=$cvv2Number&FIRSTNAME=$firstName&LASTNAME=$lastName&STREET=$address1&CITY=$city&STATE=$state".
"&ZIP=$zip&COUNTRYCODE=US&CURRENCYCODE=$currencyCode";


/*$nvpstr="&PAYMENTACTION=Sale&AMT=1.00&CREDITCARDTYPE=Visa&ACCT=4595258908900506&EXPDATE=012010&CVV2=962&FIRSTNAME=John&LASTNAME=Doe&STREET=123+Fake+St&CITY=San+Jose&STATE=CA&ZIP=95131&COUNTRYCODE=US&CURRENCYCODE=USD";
*/
//echo $nvpstr;

/* Make the API call to PayPal, using API signature.
   The API response is stored in an associative array called $resArray */





$resArray=hash_call("doDirectPayment",$nvpstr);

//print_r($resArray);
/* Display the API response back to the browser.
   If the response from PayPal was a success, display the response parameters'
   If the response was an error, display the errors received using APIError.php.
   */
$ack = strtoupper($resArray["ACK"]);

	if($ack=="SUCCESS")  
	{
		$addorder = new Order();
		$addorder->addinfo();
	}
   else
   {
    $_SESSION['reshash']=$resArray;
	$location = "APIError.php";
	header("Location: $location");
   }
//print_r($resArray);

?>

<html>
<head>
    <title>PayPal PHP SDK - DoDirectPayment API</title>
    <link href="sdk.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<br>
	<center>
	<font size=2 color=black face=Verdana><b>Do Direct Payment</b></font>
	<br><br>

	<b>Thank you for your payment!</b><br><br>
	
    <table width = 400>
        <tr>
            <td>
                Transaction ID:</td>
            <td><?=$resArray['TRANSACTIONID'] ?></td>
        </tr>
        <tr>
            <td>
                Amount:</td>
            <td><?=$currencyCode?> <?=$resArray['AMT'] ?></td>
        </tr>
        <tr>
            <td>
                AVS:</td>
            <td><?=$resArray['AVSCODE'] ?></td>
        </tr>
        <tr>
            <td>
                CVV2:</td>
            <td><?=$resArray['CVV2MATCH'] ?></td>
        </tr>
    </table>
    </center>
    <a class="home" id="CallsLink" href="http://www.peterlouissalon.com/dev2008/">Home</a>
</body>




