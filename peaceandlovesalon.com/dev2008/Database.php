<? 
class Database
{
	var $HasRow = false;
	var $HasError = false;
	var $ErrorMsg = "";
	var $RowsCount = 0;
	var $Rows;
	
	function Database()
	{
		$this->Connect();
		
	}
	function Connect()
	{
		$dbh = mysql_connect (DB_HOST, DB_USER, DB_PASSWORD) or die ('Unable to connect:: ' . mysql_errno() . ": " . mysql_error());
		mysql_select_db (DB_NAME);
	}
	function Init()
	{
		$this->RowsCount = 0;
		$this->HasRow = false;
		$this->HasError = false;
		$this->Rows = "";
	}
	function IsError()
	{
		if(mysql_error()){
			$this->Init();
			$this->ErrorMsg = mysql_error();
			$this->HasError = true;
		}
		return $this->HasError;
	}
	function GetSecureString($input)
	{
		$output = "";
		
		$output = str_replace("\'", "''", $input);
		//die($output);
		return $output;
	}
	function Insert($sql)
	{
		$res = mysql_query($sql);
		if(!$this->IsError()){
			return mysql_insert_id();
		}
		
		return 0;
	}
	function Execute($sql)
	{
		$res = mysql_query($sql);
		if(!$this->IsError()){
			return mysql_affected_rows();
		}
		
		return 0;
	}
	
	function Get($sql)
	{
		$this->Init();
		//$this->HasRow = "false";
		
		$res = mysql_query($sql);
		
		if(!$this->IsError()){
			$this->RowsCount = mysql_num_rows($res);
			
			if($this->RowsCount>0){
				$this->HasRow = true;
				$ResultSet = "";//new array();
				for($i=0;$i<$this->RowsCount;$i++){
					$ResultSet[$i] = mysql_fetch_array($res);
				}
				$this->Rows = $ResultSet;
				return $ResultSet;
			}
		}
		//die($this->HasRow."=");
		return false;
	}
}
?>