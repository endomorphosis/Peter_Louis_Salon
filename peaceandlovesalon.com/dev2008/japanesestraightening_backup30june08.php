<table id="table1" width="100%" border="0">
<tbody>
<tr>
<td width="40">&nbsp;</td>
<td>&nbsp;</td></tr>
<tr>
<td width="40">&nbsp;</td>
<td>
<table cellspacing="0" cellpadding="0" width="100%" border="0">
<tbody>
<tr>
<td><font class="small"><b><img height="27" alt="Hair Services" src="hair_services_147x27.gif" width="147" align="absBottom" border="0" /><font size="1">&nbsp;</font></b></font> 
<p><font class="title" face="Arial" color="#333333" size="2"><b>A Straight Hair Story: Japanese Hair Straightening Fan</b></font></p>
<table id="table1" cellspacing="0" cellpadding="0" width="100%" border="0">
<tbody>
<tr>
<td align="middle"><a href="jap4big.jpg" target="_blank"></a></td></tr></tbody></table>
<p><font class="bodytext"><font face="Arial" color="#333333" size="2"><img height="93" alt="" src="coarse_dry200.jpg" width="200" align="right" border="0" /></font><font face="Arial" size="2" style="text-align:justify;">It was by coincidence that I rented "The Stepford Wives" only days before becoming a model for the Japanese Hair Straightening System aka Thermal Reconditioning. Like the women in the film, <b><i>all Japanese hair straightening women share a common beauty trait: Straight, silky, shiny hair!</i></b> Like most curly tops, I own a variety of hats, and know every trick to smooth the frizzies.</font></font></p>
<p><font class="bodytext"><font face="Arial" size="2"><img height="93" alt="" src="Curly200.jpg" width="200" align="left" border="0" />But, there's one big advantage of living in the 21st Century; it's the constant stream of scientific breakthroughs in the billion-dollar beauty business that help put things like the frizzies behind us once and for all. The Japanese Hair Straightening System is a great example of this, and I'd like to share my experience with you. Originating in Japan, <b>this natural, Japanese Straightening System is used in conjunction with a straightening iron that seals in the hair's moisture and shine once the Japanese straightening process is completed.</b></font></font></p>
<p align="left"><font class="bodytext"><font face="Arial" size="2"><a href="jap1big.jpg" target="_blank"><img height="93" alt="" src="LCourse200.jpg" width="200" align="left" border="0" /></a> starting the process though, you're invited to <i>have a consultation where you'll learn all about the product and what you can expect. </i>I found the most impressive part of the meeting was the "after" pictures they showed me of their clients. The hair looked amazing, but it was really the satisfied expressions of the clients that convinced me to go ahead. I had never opted, in all my years of sleeking back and pony tailing, to have my hair straightening because I was convinced it would be ruined for good. But this time I thought things were going to be different. </font></font></p>
<p><font class="bodytext"><font face="Arial" size="2"><img height="93" alt="" src="ultr_dry200.jpg" width="200" align="left" border="0" /></font> <b>The Japanese Straightening took five hours - with chemical smells low - and excitement high! The team was meticulous and they processed small sections of hair at a time. The stylists took detailed notes on the health and color or my mane for future maintenance. And since Japanese hair straightening never loses its straightness, you simply return for two or three touchups a year.</b></font></font></p>
<table id="table2" cellspacing="0" cellpadding="0" width="100%" border="0">
<tbody>
<tr>
<td align="middle"><a href="/jap6big.jpg" target="_blank"></a></td></tr></tbody></table>
<p><font class="bodytext"><font face="Arial" size="2">After the process, I was so excited that I cut my hair to my chin, fulfilling my lifelong dream of an "Isabella Rosellini" like hairdo. After washing my hair and realizing this was no longer a fantasy, I started wondering what the heck was in this stuff: <b>The Japanese Straightening System AKA Thermal Reconditioning loosens the hair's cystine protein bonds, reshapes the bonds by straightening the hair cells with the use of a heat iron then makes it permanent using a bromide neutralizer. This is the basis for the The Japanese Straightening System.</b></font></font></p>
<p><font face="Arial" color="#000000" size="2"><a href="/jap3big.jpg" target="_blank"></a>To test the claim of hair becoming <b>humidity proof</b>, I journeyed to the humid Santa Monica Pier. Walking through, I eyed a mirror on the side of the carousel. Well, I had to sneak a peek, and lo and behold - I was Isabella like! My hair anyway! It was straighter than anyone on the pier! The Japanese System Hair Straightening is serious hair processing with results that are no laughing matter. ... On the practical side, I no longer scare the FedEx driver when I answer the door in the morning, and this new beauty process has cut down my making up time by 75% - resulting in an increase of my making out time by 25%! I'm sure it's the hair!!! </font></p>
<p><font face="Arial" color="#000000" size="2">My new straight hair gives me added confidence (and gets me extra compliments). Now I can focus on color over cut. I have the time for lining my eyes extra well, for cleaning my brushes first - and even having that third cup of coffee!</font></p>
<table id="table3" cellspacing="0" cellpadding="0" width="100%" border="0">
<tbody>
<tr>
<td align="middle"><a href="/jap2big.jpg" target="_blank"></a></td>
<td align="middle"><a href="/jap5big.jpg" target="_blank"></a></td></tr></tbody></table><font face="Arial" size="2">
<hr />
</font>
<p><font class="bodytext"><font size="2"><font face="Arial"><b>What:</b> This unique service can take anywhere from three-and-a-half to six hours and costs upwards of $500. After washing and drying hair, a Japanese essential oil/aloe straightening serum is applied to small sections of hair at a time, then left to soak in. Following more washing and drying, baby irons are used to flatten hair almost strand by strand. Next comes a neutralizing solution and, yes, another wash and dry. <br /><br /><b>Why:</b> With this treatment, even curly or coarse hair is not only straight and frizz-free for months, but actually healthier and softer, boasting more shine and gloss. Clients can still curl and style, but most are happy to wake up everyday with a blow-out-straight 'do. And many swear they pinch pennies all year for the pleasure.</font></font></font></p><font face="Arial" size="2">
<hr />
</font>
<p><font class="title" face="Arial" color="#333333" size="2"><b>JAPANESE HAIR STRAIGHTENING HISTORY</b></font></p>
<p><font class="bodytext" face="Arial" size="2">The first question that is asked when the clients hear about Japanese Hair Straightening aka Thermal Reconditioning ( which is like saying Champagne aka sparkling wine ) is "Why do the Japanese need a relaxer? They already have straight hair!" Contrary to popular belief, a high percentage of the Japanese population has wavy or kinky curl to their hair.</font></p>
<p><font class="bodytext" face="Arial" size="2">LISCIO "The First Japanese Straightening System"</font></p>
<p><font class="bodytext" face="Arial" size="2">To respond to the desire of clients to have straight hair, Milbon introduced Liscio in Japan on 1996. Unlike existing relaxers with active ingredients based on Hydroxide and lye, thio-based Liscio is more gentle for the hair thereby straightening the hair while leaving it in wonderful condition. Milbon was the first manufacturer to receive Japanese governmental approval for its unique thermal reconditioning system. Liscio contains a specially formulated solution and iron, which protects and repairs hair from heat damage.</font></p>
<p><font class="bodytext" face="Arial" size="2">In 1998 Shinbi International brought Liscio to the US and has developed the product and procedure even further to enable the process to be performed on just about any client that walks through the salon door. With added protein treatments and supporting products for the thermal reconditioning procedure, it is not uncommon to hear clients proclaim that their hair condition actually has improved after the procedure. We at the Peter Louis Salon are confident that our line of products and unique techniques will meet your desire to have beautiful straight hair.</font></p>

<p><font class="bodytext" face="Arial" size="2">YUKO SYSTEM "Japanese Straightening"</font></p>
<p><font class="bodytext"><font size="2"><font face="Arial"><u>Activator Solution Difference-</u>While the main active ingredient is the same, YUKO system only has 1 solution strength, which doesn't allow for customization for individual client's hair.</font></font></font></p>
<p><font class="bodytext"><font size="2"><font face="Arial"><u>No Protection Cream System</u>-- Yuko System doesn't have protection cream system so highlighted hair or hair that's been previously relaxed cannot be processed.</font></font></font></p>
<p><font class="bodytext" face="Arial" size="2">RUSK STR8 "Thermal Reconditioning"</font></p>
<p><font class="bodytext"><font size="2"><font face="Arial"><u>Activator Solution Difference</u> Same thio based activator. However, it does not contain any heat protecting ingredient. So it is likely that hair sustains damage during the ironing procedure.</font></font></font></p>
<p><font class="bodytext"><font size="2"><font face="Arial"><u>Iron Difference--</u> Unlike Liscio, which has customized thermal iron for thermal reconditioning with different features, Rusk Iron doesn't include these features. Most importantly, its lack of temperature control doesn't allow for processing various hair in different conditions that can lead to burnt hair.</font></font></font></p>
<p><font class="bodytext" face="Arial" size="2">Furthermore, the iron lacks an evenly heated plate, which lessens the effect of the ironing procedure.</font></p>
<p><font class="bodytext"><font size="2"><font face="Arial"><u>No Protection Cream System</u> --Like the Yuko System, Rusk doesn't have a protection cream system. As a result, the same limitation in processing the hair from Yuko system applies to Rusk.</font></font></font></p>
<p><font class="bodytext" face="Arial" size="2">--Procedures such as I-straight and Paul Brown are similar to Rusk in its active ingredients and supporting products. In comparing results and solution content, its comparable to the Liscio straight variation system. However, the Liscio system is still superior to these systems in a lot of ways due to its versatility of use and restoring formula.</font></p>
<p><font class="bodytext" face="Arial" size="2">BIO IONIC "Thermal Reconditioning"</font></p>
<p><font class="bodytext"><font size="2"><font face="Arial"><u></font></font></font></p>
<p><font class="bodytext"><font size="2"><font face="Arial"><u>Iron Difference</u> Uneven heating plate on its irons can cause damage to the fragile hair during the thermal reconditioning.</font></font></font></p>
<p><font class="bodytext"><font size="2"><font face="Arial"><u>Additional Maintenance</u> Bio-Ionic clients are required to purchase a special brush and blowdry to maintain its straight hair while other systems do not require such purchase.</font></font></font></p>
<p></p></u></td></tr></tbody></table></td></tr></tbody></table>