<?php

class Order 
{
		var $btitle;
		var $email;
		var $bfname;
		var $bmiddlename;
		var $blastname;
		var $bsuffix;
		var $bcompany;
		var $baddress;
		var $baddress2;
		var $bcity;
		var $bstate;
		var $bzip;
		var $bcountry;
		var $bphone;
		
		var $stitle;
		var $sfname;
		var $smname;
		var $slname;
		var $ssuffix;
		var $scompany;
		var $saddres;
		var $saddress2;
		var $scity;
		var $sstate;
		var $szip;
		var $scountry;
		var $sphone;
		
		var $creditCardType;
		var $creditCardNumber;
		var $expDateMonth;
		var $padDateMonth;
		var $expDateYear;
		var $cvv2Number;
		var $amount;
		var $currencyCode;
		var $paymentType;
	
	
	
	function Order()
	{
	
		$this->email = $_REQUEST['email'];
		$this->btitle = $_REQUEST['title'];
		$this->bfname = $_REQUEST['first'];
		$this->bmiddlename = $_REQUEST['middle'];
		$this->blastname = $_REQUEST['last'];
		$this->bsuffix = $_REQUEST['suffix'];
		$this->bcompany = $_REQUEST['company'];
		$this->baddress = $_REQUEST['address'];
		$this->baddress2 = $_REQUEST['address2'];
		$this->bcity = $_REQUEST['city'];
		$this->bstate = $_REQUEST['state'];
		$this->bzip = $_REQUEST['zip'];
		$this->bcountry = $_REQUEST['country'];
		$this->bphone = $_REQUEST['phone'];
		
		$this->stitle = $_REQUEST['shiptitle'];
		$this->sfname = $_REQUEST['shipfirst'];
		$this->smname = $_REQUEST['shipmiddle'];
		$this->slname = $_REQUEST['shiplast'];
		$this->ssuffix = $_REQUEST['shipsuffix'];
		$this->scompany = $_REQUEST['shipcompany'];
		$this->saddres = $_REQUEST['shipaddress'];
		$this->saddress2 = $_REQUEST['shipaddress2'];
		$this->scity = $_REQUEST['shipcity'];
		$this->sstate = $_REQUEST['shipstate'];
		$this->szip = $_REQUEST['shipzip'];
		$this->scountry = $_REQUEST['shipcountry'];
		$this->sphone = $_REQUEST['shipphone'];
		
		$this->paymentType ="Sale";
		$this->creditCardType =urlencode( $_POST['creditCardType']);
		$this->creditCardNumber = urlencode($_POST['creditCardNumber']);
		$this->expDateMonth =urlencode( $_POST['expDateMonth']);
		$this->padDateMonth = str_pad($expDateMonth, 2, '0', STR_PAD_LEFT);
		$this->expDateYear =urlencode( $_POST['expDateYear']);
		$this->cvv2Number = urlencode($_POST['cvv2Number']);
		$this->amount = urlencode($_SESSION['carts']['total']);
		$this->currencyCode="USD";
	}
	
	function addinfo()
	{
		include_once('db.php');
		include_once('database.php');
		$today = date("Y-m-d H:i:s");
		$db = new Database();
		$sql_bill = "insert into bill(title,email,fname,mname,lname,company,suffix,address1,address2,city,state,zip,country,phone,bill_datetime) values ('".$this->btitle."','".$this->email."','".$this->bfname."','".$this->bmiddlename."','".$this->blastname."','".$this->bcompany."','".$this->bsuffix."','".$this->baddress."','".$this->baddress2."','".$this->bcity."','".$this->bstate."','".$this->bzip."','".$this->bcountry."','".$this->bphone."','".$today."')";		
		$bill_id=$db->Insert($sql_bill);
		$sql_ship = "insert into ship(title,fname,mname,lname,company,suffix,address1,address2,city,state,zip,country,phone) values ('".$this->stitle."','".$this->sfname."','".$this->smiddlename."','".$this->slastname."','".$this->scompany."','".$this->ssuffix."','".$this->saddress."','".$this->saddress2."','".$this->scity."','".$this->sstate."','".$this->szip."','".$this->scountry."','".$this->sphone."')";		
		$ship_id=$db->Insert($sql_ship);
		$sql_order = "insert into orders (bill_id,ship_id,subtotal,shipping,tax,vat,total) values(".$bill_id.",".$ship_id.",".$_SESSION['carts']['subtotal'].",".$_SESSION['carts']['shipping'].",".$_SESSION['carts']['tax'].",".$_SESSION['carts']['vat'].",".$_SESSION['carts']['total'].")";
		$order_id = $db->Insert($sql_order);
		
		foreach($_SESSION['cart'] as $key=>$value)
		{
			$sql_orderitem = "insert into orderitem(order_id,name,sku,unitprice,quantity) values(".$order_id.",'".$_SESSION['cart'][$key]['prodName']."','".$_SESSION['cart'][$key]['sku']."',".$_SESSION['cart'][$key]['price'].",".$_SESSION['cart'][$key]['qty'].")";
			$db->Insert($sql_orderitem);
		}
		
		return $db->IsError();

	}
}

?>