<table border="0" width="100%" cellpadding="0" cellspacing="0">
<tbody>
<tr>
<td width="40">&nbsp;</td>
<td>&nbsp;</td></tr>
<tr>
<td width="40">&nbsp;</td>
<td>
<table id="table1" border="0" cellpadding="11" cellspacing="0" width="100%">
<tbody>
<tr>
<td>
<table id="table2" border="0" cellpadding="0" cellspacing="0" width="100%">
<tbody>
<tr>
<td><img alt="Hair Services" src="hair_services_147x27.gif" border="0" height="27" width="147"> <br><br>
<table id="table3" border="0" cellpadding="0" cellspacing="0" width="100%">

<tbody>
<tr class="stripe-three">
<td colspan="3"><font class="bodytext-alt" color="#ffffff"><b><font color="white">&nbsp;HAIR DESIGN SERVICES</font></b></font></td></tr>
<tr>
<td width="30%"><font class="bodytext">Haircut with Peter Louis</font></td>
<td nowrap="nowrap" width="40%"><font class="bodytext">$150</font></td>
<td width="30%"></td></tr>
<tr>
<td width="30%"><font class="bodytext">Haircut with Stylist</font></td>
<td width="40%"><font class="bodytext">$50 - $125</font></td>
<td width="30%"></td></tr>

<tr>
<td width="30%"><font class="bodytext">Style/Set</font></td>
<td width="40%"><font class="bodytext">$50 - $150</font></td>
<td width="30%"></td></tr>
<tr>
<td width="30%"><font class="bodytext">Updo</font></td>
<td width="40%"><font class="bodytext">$75 - $150</font></td>
<td width="30%"></td></tr>
<tr>
<td width="30%"><font class="bodytext">Japanese Straightening*</font></td>
<td width="40%"><font class="bodytext">from $500 &amp; up</font></td>

<td width="30%"></td></tr>
<tr>
<td width="30%"><font class="bodytext">Mini Japanese Straightening*</font></td>
<td width="40%"><font class="bodytext">from $300 &amp; up</font></td>
<td width="30%"></td></tr>
<tr>
<td width="30%"><font class="bodytext">Relaxer/Perms</font></td>
<td width="40%"><font class="bodytext">from $150 and up</font></td>
<td width="30%"></td></tr>
<tr>
<td width="30%"><font class="bodytext">Hair Extensions*</font></td>

<td width="40%"><font class="bodytext">Price determined at consultation</font></td>
<td width="30%"><font class="small">using the revolutionary technology of <a href="template.php?f=salonpic">Great Lengths</a></font></td></tr>
<tr>
<td width="30%"></td>
<td colspan="2"><font class="small">* Nonrefundable deposit is required</font></td></tr>
<tr class="stripe-three">
<td colspan="3"><font class="bodytext-alt"><b><font color="white">&nbsp;HAIR COLOR</font></b></font></td></tr>
<tr>
    <td width="30%" height="18"><font class="bodytext">Single Process</font></td>
    <td width="40%"><!--<font class="bodytext">$85 - $125</font>--><font class="bodytext">$75 - $125</font></td>
    <td width="30%"></td>
</tr>
<tr>
    <td width="30%" height="18" valign="top"><font class="bodytext">Double Process</font></td>
    <td width="40%"><font class="bodytext">$90 - $150</font></td>
    <td width="30%"></td>
</tr>
<tr>
    <td width="30%" height="18" valign="top"><font class="bodytext">Extra Color Dose</font></td>
    <td width="40%"><font class="bodytext">$20</font></td>
    <td width="30%"></td>
</tr>

<tr>
    <td  colspan="3"></td>    
</tr>
<tr class="stripe-three">
<td height="20" colspan="3"><font class="bodytext-alt"><b><font color="white">&nbsp;HIGHLIGHTS</font></b></font></td>
</tr>
<tr>
<td width="30%" height="15" valign="bottom"><font class="bodytext">Full</font></td>

<td width="40%"><font class="bodytext">$150 - $300</font></td>
<td width="30%"></td></tr>
<tr>
<td width="30%" height="15" valign="top"><font class="bodytext">Corrective Hair Color</font></td>
<td width="40%"><font class="bodytext">from $150 &amp; up</font></td>
<td width="30%"></td></tr>
<tr valign="top">
<td width="30%" height="15"><font class="bodytext">Gloss</font></td>
<td width="40%"><font class="bodytext">from $60 &amp; up</font></td>

<td width="30%"></td></tr>
<tr>
<td width="30%"></td>
<td width="40%"></td>
<td width="30%"></td></tr>
<tr class="stripe-three">
<td colspan="3"><font class="bodytext-alt"><b><font color="white">&nbsp;SPECIALS</font></b></font></td></tr>
<tr>
<td colspan="3"><font class="bodytext">Call each salon for their monthly promotion: <a href="template.php?f=contact">contact information</a> </font></td></tr></tbody></table>
<p><font class="small"><b>Pricing</b> All prices are subject to change without prior notice.</font></p>

<p><font class="small"><b>Cancellations</b> Cancellations should be made 72 hours in advance. Nonrefundable Deposit is required for appointments on procedures as Japanese Hair Straightening and Hair Extensions.</font></p>
<p><font class="small"><b>Arrival</b> Please check in at the reception at least 15 minutes prior to your appointment to prepare for your treatment.</font></p>
<p><font class="small"><b>Let us know</b> Please inform your technician if you are pregnant, have injuries, are using Retin A, Accutane, or have had any recent laser or plasticsurgery.</font></p>
<p><font class="small"><b>Hours of operation</b><br>Monday-Saturday 10 am-8 pm<br>and Sunday by appointment only.</font></p>
<p><font class="bodytext"><b>Peter Louis Salon</b><br>143 E 57th Street<br>(@Lexington Ave.)<br>New York, NY 10022<br>212.319.0019</font></p>

<p></p></td>
</tr></tbody></table></td></tr></tbody></table>
<p></p></td></tr></tbody>
</table>