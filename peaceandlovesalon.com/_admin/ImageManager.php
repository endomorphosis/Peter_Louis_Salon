<?
/**

  * Create a thumbnail image from $inputFileName no taller or wider than
  * $maxSize. Returns the new image resource or false on error.
  * Author: mthorn.net
  */

define('MAX_WIDTH', 800, true);
define('MAX_HEIGHT', 600, true);
define('MIN_SIZE', 100, true);

//define(MB, 1048576, true); // 1MB


define('MAX_FILE_SIZE', 1048576*2, true); // 2MB 


class ImageManager{

	var $image_resize = false;
	var $max_size = 150;
	var $file_name = "";
	var $dest_folder = "";
	var $dest_file = "";
	var $src_file = "";
	var $src_file_name = "";
	var $src_file_ext = "";
	var $error_msg = "";
	
	function ImageManager(){}
	function GetFileName()
	{
		$out_file_name = "";
		
		$this->src_file_ext = strtolower(substr($this->src_file, strrpos($this->src_file, '.')));
		$this->src_file_name = strtolower(substr($this->src_file, 0, strrpos($this->src_file, '.')));
		
		$out_file_name = str_replace(" ", "_", $this->src_file);
		$f = $this->dest_folder.$out_file_name;
		
		while(file_exists($f)){
		
			$out_file_name = $this->src_file_name."_".rand(1, 100000).$this->src_file_ext;
			$out_file_name = str_replace(" ", "_", $out_file_name);
			$f = $this->dest_folder.$out_file_name;
		}
		
		return $out_file_name;
	}
	function GetFileSize($src)
	{
		$size = 0;
		if(file_exists($src)){
			$size = filesize($src);
		}
		
		$size = round($size);
		return $size;
	}
	function isValidImage($src)
	{
		$size = getimagesize($src);
		$valid = false;
		switch($size["mime"]){
			case "image/jpeg":
			case "image/jpg":
			case "image/gif":
			case "image/png":
				$valid =  true;
				break;
		}
		return $valid;
		
	}
	function Upload()
	{
		$src = $_FILES[$this->file_name]["tmp_name"];
		
		if(!is_uploaded_file($src)){
			$error_msg = "Error in upload";
			return false;
		}
		
		if(!$this->isValidImage($src)){
			$error_msg = "Invalid image file";
			return false;
		}
		$size = $this->GetFileSize($src);
		if($size <= 0 || $size > MAX_FILE_SIZE)
		{
			$error_msg = "Image size is too large or empty";
			return false;
		}
		
		$this->src_file = $_FILES[$this->file_name]["name"];
		$this->dest_file = $this->GetFileName();
		$dest = $this->dest_folder.$this->dest_file;
		
		if(!move_uploaded_file($src, $dest)){
			$error_msg = "Unable to upload";
			return false;
		}
		
		return true;
	}
	function thumbnail($inputFileName, $maxWidth, $maxHeight)
	{
		if(file_exists($inputFileName)){
			$info = getimagesize($inputFileName);
			$type = isset($info['type']) ? $info['type'] : $info[2];
		
			// Check support of file type
			if ( !(imagetypes() & $type) )
			{
				// Server does not support file type
				return false;
			}
		
			$width = isset($info['width']) ? $info['width'] : $info[0];
			$height = isset($info['height']) ? $info['height'] : $info[1];
		
			// Calculate aspect ratio
			
			if($maxWidth>MAX_WIDTH)
			{
				$maxWidth = MAX_WIDTH;
			}
			if($maxHeight>MAX_HEIGHT)
			{
				$maxHeight = MAX_HEIGHT;
			}
			/*
			if($maxSize<MIN_SIZE){
				$maxSize = MIN_SIZE;		
			}
			*/
			
			$wRatio = $maxWidth / $width;
		
			$hRatio = $maxHeight / $height;
		
			// Using imagecreatefromstring will automatically detect the file type
		
			$sourceImage = imagecreatefromstring(file_get_contents($inputFileName));
		
			// Calculate a proportional width and height no larger than the max size.
		
			if ( ($width <= $maxWidth) && ($height <= $maxHeight) )
			{
				// Input is smaller than thumbnail, do nothing
				return $sourceImage;
			}
			elseif ( ($wRatio * $height) < $maxHeight )
			{
				// Image is horizontal
				$tHeight = ceil($wRatio * $height);
				$tWidth = $maxWidth;
				
				//die("h");
				
				if($tHeight>$maxHeight){
					$tHeight = $maxHeight;						
					$tWidth = ceil($tHeight/$height*$width);
				}
				
			}
			else
			{
				// Image is vertical
				
				$tWidth = ceil($hRatio * $width);
				$tHeight = $maxHeight;
				
				if($tWidth>$maxWidth){
					$tWidth = $maxWidth;						
					$tHeight = ceil($tWidth/$width*$height);					
				}
				
			}
			
			//die($tWidth." x ".$tHeight);
			$thumb = imagecreatetruecolor($tWidth, $tHeight);
		
			if ( $sourceImage === false )
			{
				// Could not load image
				return false;
			}
		
			// Copy resampled makes a smooth thumbnail
			imagecopyresampled($thumb, $sourceImage, 0, 0, 0, 0, $tWidth, $tHeight, $width, $height);
			imagedestroy($sourceImage);
		
			return $thumb;
		}
		
		return NULL;
	
	}
	
	function SaveImage($src, $width, $height, $dest)
	{
		$im = $this->thumbnail($src, $width, $height);
		return $this->imageToFile($im, $dest);
	}

	
	function imageToFile($im, $fileName, $quality = 80)
	{
		
		$ext = strtolower(substr($fileName, strrpos($fileName, '.')));
	 
		switch ( $ext )
		{
			case '.gif':
				imagegif($im, $fileName);
				break;
			case '.jpg':
			case '.jpeg':
				imagejpeg($im, $fileName, $quality);
				break;
			case '.png':
				imagepng($im, $fileName);
				break;
			case '.bmp':
				imagewbmp($im, $fileName);
				break;
			default:
				return false;
		}
		return true;
	}
}
?>