<?php

	include('sc_config.inc.php');

	$pm_charset = 'iso-8859-1';

	session_start();


// Ajax config code
if (!defined("PATH_SEPARATOR"))
  define("PATH_SEPARATOR", getenv("COMSPEC")? ";" : ":");
ini_set("include_path", ini_get("include_path").PATH_SEPARATOR.dirname(__FILE__));
// Ajax config code


	require_once('class/class.dynaimage.php');
	require_once('class/class.query.php');


// Declare Ajax lib
require_once 'ajax/JsHttpRequest.php';

// Create main library object
// Set page encoding. Important!
$JsHttpRequest =& new JsHttpRequest($pm_charset);
// Get query


$row_id = @$_POST['row_id'];
$folder0 = @$_POST['folder0'];
$name = @$_POST['name'];
$id = @$_POST['id'];
$Item_id = @$_POST['Item_id'];
$product_type = @$_POST['product_type'];
$brand = @$_POST['brand'];
$Item_desc = @$_POST['Item_desc'];
$Item_name = @$_POST['Item_name'];
$weight = @$_POST['weight'];
$created = @$_POST['created'];

$action = @$_GET['action'];
$row_id_list = @$_GET['row_id_list'];


	switch ($action) {

		case 'delete':

			$id_array = explode('|', $row_id_list);
			if ($id_array) {
				// delete images
				foreach ($id_array as $key=>$image_id) {
					$pict = new dynaimage($image_id, $folder0, 0, $parent_folder);
					$pict->delete($pict->file());
				}

				$sql_id_str = '('.implode(',', $id_array).')';

				$query = '
DELETE FROM `pete67`.`itemdesc` WHERE `id` in '.$sql_id_str.'
				';

				$q = new query($query);

				if (!$q->exec()) {
					$error = 'Cannot delete rows selected!';
				}
			} else {
				$error = 'No rows to delete!';
			}

			break;

		case 'add':

			
			if ($chairman) {
	$query = '
update itemdesc set chairman = "0"
	';

	$q = new query($query);
	$q->update();
			}


			$query = '
INSERT INTO `pete67`.`itemdesc`
(`id` , `Item_id` , `Item_name` , `product_type` , `brand` , `Item_desc` , `weight`)
	VALUES
(\''.mysql_real_escape_string($id).'\',
\''.mysql_real_escape_string($Item_id).'\',
\''.mysql_real_escape_string($Item_name).'\',
\''.mysql_real_escape_string($product_type).'\',
\''.mysql_real_escape_string($brand).'\',
\''.mysql_real_escape_string($Item_desc).'\',
\''.mysql_real_escape_string($weight).'\'
)
			';

			$q = new query($query);
			$new_row_id = $q->insert();

			if (!$new_row_id) {
				$error = $query.'Cannot save new row in DB!';
			} else {
				// read the DB row as some data (ie dates) could
				// be changed by DB
				$query = '
SELECT *
	FROM itemdesc
WHERE id = "'.mysql_real_escape_string($new_row_id).'"
				';

				$q = new query($query);
				$row_data = $q->select1();
			}

			break;

		default:

			
			if ($chairman) {
	$query = '
update `pete67`.`itemdesc` set chairman = "0"
	';

	$q = new query($query);
	$q->update();
			}


			$query = '
UPDATE `pete67`.`itemdesc` SET
`Item_name` = \''.mysql_real_escape_string($Item_name).'\',
`product_type` = \''.mysql_real_escape_string($product_type).'\',
`brand` = \''.mysql_real_escape_string($brand).'\',
`Item_desc` = \''.mysql_real_escape_string($Item_desc).'\',
`weight` = \''.mysql_real_escape_string($weight).'\'
WHERE `itemdesc`.`id` ='.mysql_real_escape_string($row_id).' LIMIT 1
			;';
			$q = new query($query);

			$result = $q->update();

			if (!$result) {
				$error =  $query;
			} else {
				// read the DB row as some data (ie dates) could
				// be changed by DB
				$query = '
SELECT *
	FROM itemdesc
WHERE id = "'.mysql_real_escape_string($row_id).'"
				';

				$q = new query($query);
				$row_data = $q->select1();

			}

			break;
	}


// Prepare result as a PHP array
$_RESULT = array(
	'status' => isset($error)?$error:'ok',
	'new_row_id' => @$new_row_id?$new_row_id:'',
'cell_value0' => @$new_row_id,
'cell_value1' => htmlspecialchars(@$row_data['Item_id']),
'cell_value2' => htmlspecialchars(@$row_data['Item_name']),
'cell_value3' => htmlspecialchars(@$row_data['product_type']),
'cell_value4' => htmlspecialchars(@$row_data['brand']),
'cell_value5' => htmlspecialchars(@$row_data['Item_desc']),
'cell_value6' => htmlspecialchars(@$row_data['weight']),
);

?>