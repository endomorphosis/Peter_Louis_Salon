<?php

	$pm_charset = 'iso-8859-1';

	session_start();


// Ajax config code
if (!defined("PATH_SEPARATOR"))
  define("PATH_SEPARATOR", getenv("COMSPEC")? ";" : ":");
ini_set("include_path", ini_get("include_path").PATH_SEPARATOR.dirname(__FILE__));
// Ajax config code


	require_once('class/class.query.php');


// Declare Ajax lib
require_once 'ajax/JsHttpRequest.php';

// Create main library object
// Set page encoding. Important!
$JsHttpRequest =& new JsHttpRequest($pm_charset);
// Get query


$page = (int)@$_POST['page'];
$page_size = (int)@$_POST['page_size'];

$dl_start_with = (int)@$_POST['dl_start_with'];
$dl_size = (int)@$_POST['dl_size'];


	// if there is no such page in table, take the last one
	$flag = false;
	while (!$flag) {

		$query = '
SELECT * FROM sg_big_table ORDER BY id
		';

		// paging on
		if ($page_size) {
			$query .= ' limit '.($page*$page_size+$dl_start_with).', '.
					($dl_size?$dl_size:$page_size);
		} else {
			if ($dl_size) {
				$query .= ' limit '.$dl_start_with.', '.$dl_size;
			}
		}

		$q = new query($query);

		$data = $q->select();

		if (!$data && $page) $page--;
			else $flag = 1;
	}

	// count total number of pages
	if ($page_size) {
		$query = '
SELECT count(id) AS number FROM sg_big_table
		';
		$total_data = $q->select1($query);
		$total_rows = $total_data['number'];
		$total_pages = (int)(($total_rows-1)/$page_size)+1;
	} else {
		$total_pages = 1;
	}

	if ($dl_size) {
		$query_real_data = 'SELECT id AS number FROM sg_big_table';

		if ($page_size) {
			$query_real_data .= ' limit '.($page*$page_size).', '.$page_size;
		}

		$q = new query($query_real_data);

		$real_data = $q->select();
		$real_page_size = count($real_data);
	} else {
		$real_page_size = count($data);
	}

	if ($data) {
		$xml_data = '<'.'?xml version="1.0" encoding="UTF-8"?'.'>
<rows page="'.$page.'" pagesize="'.$page_size.'" realpagesize="'.$real_page_size.'" totalpages="'.$total_pages.'">
		';

		// prepare xml tree
		foreach ($data as $key=>$row) {
			$xml_data .= '
	<row id="'.$row['id'].'">
		<cell>'.$row['id'].'</cell>
		<cell>'.$row['number'].'</cell>
	</row>
			';
		}

		$xml_data .= '
</rows>';
	} else {
		$error = 'Cannot retrieve data from DB!';
	}

// Prepare result as a PHP array
$_RESULT = array(
	'status' => isset($error)?$error:'ok',
	'xml_data' => @$xml_data
);

// Debug info
//if (strpos($client, 'error') !== false) {
//  callUndefinedFunction();
//}

//Response text
//echo "<b>REQUEST_URI:</b> ".$_SERVER['REQUEST_URI']."<br>";
//echo "<b>Loader used:</b> ".$JsHttpRequest->LOADER;
?>