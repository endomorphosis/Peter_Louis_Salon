<?php

	include('sc_config.inc.php');

	$id = @$_GET['id'];
	$folder = @$_GET['folder'];
	$max_width = @$_GET['max_width'];
	$max_height = @$_GET['max_height'];

	if ($id) {

		require_once('class/class.dynaimage.php');

		$pict = new dynaimage($id, $folder, 0, $parent_folder);

		// no image found
		if (!$pict->check()) exit;

		$type = $pict->m_ext=="jpg"?"jpeg":$pict->m_ext;

		header("Content-type: image/$type");

		if ($type == 'gif' || !$max_width || !$max_height){
			echo readfile($pict->file());
		} else {

		$filename = $pict->file();
    
		if($type == "jpeg")
			$image1 = ImageCreateFromJpeg($filename);
		else if($type == "gif")
			$image1 = ImageCreateFromGif($filename);
		else $image1 = ImageCreateFromPng($filename);
    
		$size1 = GetImageSize($filename);
		$width1 = $size1[0];
		$height1 = $size1[1];

		$width = $width1;
		$height = $height1;


		// calc new width and height so that to constrain proportions and
		// they do not exceed max values
		if ($width > $max_width) {
			$height = (int)($height*$max_width/$width);
			$width = $max_width;
		}

		if ($height > $max_height) {
			$width = (int)($width*$max_height/$height);
			$height = $max_height;
		}


		$image = ImageCreateTrueColor($width, $height);
		if($type == "gif"){
			$image = $image1;
		}else ImageCopyResized($image, $image1, 0, 0, 0, 0, $width, $height, $width1, $height1);
    
		if($type == "jpeg") ImageJpeg($image);
			else if($type == "gif") ImageGif($image);
				else ImagePng($image);
    
		}

	}

?>