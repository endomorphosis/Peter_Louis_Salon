<?php if (!defined("_class_dynaimage_inc")) : ?>
<?php define("_class_dynaimage_inc",1); ?>
<?php
class dynaimage{
    var $m_id;
    var $m_folder;
    var $m_preview;
    var $m_basepath;
    var $m_ext;
    var $m_length;
    var $m_preview_size;

    function dynaimage($id=0, $folder='', $preview=0, $basepath='files/image',
                       $length=6, $preview_size=100){
        $this->m_id = $id;
        $this->m_folder = $folder;
        $this->m_preview = $preview;
        $this->m_basepath = $basepath;
        $this->m_length = $length;
        $this->m_preview_size = $preview_size;

        $exts = array("gif", "jpg", "png");
        while(list($k, $v) = each($exts)){
          if(file_exists($this->file().".$v")){
            $this->m_ext = $v;
            break;
          }
        }

    }

    function name(){
        $name = $this->m_id;
        while (strlen($name) < $this->m_length) $name = '0'.$name;
        if ($this->m_preview) $name .= 's';
        return $name;
    }

    function file(){
        $file = "";
        if($this->m_basepath) $file .= $this->m_basepath.'/';
        $file .= $this->m_folder.'/'.$this->name();
        if(isset($this->m_ext) && $this->m_ext) $file .= '.'.$this->m_ext;
        return $file;
    }

    function check(){                                  
        return file_exists($this->file());
    }

    function upload($sourcefile){
        $sourcefile = str_replace("\\\\", "\\", $sourcefile); // Win32
        if (!file_exists($sourcefile)) return 0;

        $this->delete();

        $size = getimagesize($sourcefile);

        switch($size[2]){
          case 1:
            $type = "gif";
            break;
          case 2:
            $type = "jpg";
            break;
          case 3:
            $type = "png";
            break;
          default:
            $type = "";
        }
        $this->m_ext = $type;

        return copy($sourcefile, $this->file());
    }

    function delete(){
        if ($this->check()) return @unlink($this->file());
    }
}
?>
<? endif; ?>