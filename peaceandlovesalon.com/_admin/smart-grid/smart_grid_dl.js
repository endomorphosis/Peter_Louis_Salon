/*
	Dynamic loading functions
*/

	// get index of first existing row in grid preceeding row with number
	// row_index
	SmartGrid.prototype.dl_get_prev_row_index = function(row_index) {
		var i;

		for (i=row_index; i>=0; i--) {
			if (this.dl_is_row_exists(i)) {
				return i;
			}
		}

		return -1;
	}

	// get index of first existing row in grid following row with number
	// row_index
	SmartGrid.prototype.dl_get_next_row_index = function(row_index) {
		var i;

		for (i=row_index; i<this.real_page_size; i++) {
			if (this.dl_is_row_exists(i)) {
				return i;
			}
		}

		return -1;
	}

	// add spacer (space row) to imitate rows_num empty rows in grid
	SmartGrid.prototype.dl_add_spacer = function(prev_row, rows_num, id) {
		var i;
//alert(this.dl_get_row_height());
		var row_height = rows_num*this.dl_get_row_height();

		var data_tr = document.createElement('TR');
		prev_row.parentNode.insertBefore(data_tr, prev_row.nextSibling);

		var spacer_id_prefix = this.dl_spacer_id_prefix();
		var frozen_prefix = this.common_grid?this.get_frozen_prefix():'';

		// make unique id
		if (!id) id = frozen_prefix+spacer_id_prefix+(new Date().getTime());
		data_tr.setAttribute('id', id);
		data_tr.setAttribute('name', spacer_id_prefix);
		var spacer_height = row_height + 'px';
		data_tr.style.height = spacer_height;

		var col_len = this.columns.length;
		for (i=0; i<col_len; i++) {
			var new_cell = data_tr.insertCell(i);
			if (i == 0) new_cell.style.height = spacer_height;
		}

		return data_tr;		
	}

	// returns id for spacer row
	SmartGrid.prototype.dl_spacer_id_prefix = function() {
		return 'sg_dl_spacer';
	}

	// change spacer (space row) height
	SmartGrid.prototype.dl_change_spacer = function(spacer_obj, rows_num) {
		var i;
//alert(this.dl_get_row_height());
		var row_height = rows_num*this.dl_get_row_height();

		var spacer_height = row_height + 'px';
		spacer_obj.style.height = spacer_height;
		spacer_obj.cells[0].style.height = spacer_height;
		
	}

	// determines data row height
	SmartGrid.prototype.dl_get_row_height = function() {
		var default_height = 20;
		var rows = this.get_rows();

		if (!rows[0]) return default_height;
		var height = rows[0].offsetHeight;

		return height?height:default_height;
	}

	// load data corresponding to rows visible in grid
	SmartGrid.prototype.dl_load_data = function() {
		var visible_rows = this.dl_get_visible_rows();
		var first_visible_row = visible_rows[0];
		var last_visible_row = visible_rows[1];

/*
		var top_pos = this.content_div.scrollTop;
		var row_height = this.dl_get_row_height();
		var first_visible_row = Math.floor(top_pos/row_height);
		var last_visible_row = Math.floor((top_pos+this.content_div.offsetHeight)/row_height);
*/

		// find first not loaded row
//alert(first_visible_row + ' => ' + last_visible_row);
		var start_with = -1;
		var rows_num = 0;
		var i;
		for (i=first_visible_row; i<=last_visible_row&&i<this.real_page_size; i++) {
			if (!this.rows[i] && start_with < 0) {
				start_with = i;
			}

			if (start_with >= 0) {
				if (!this.rows[i]) rows_num++;
					else break;
			}
		}

//alert(start_with + ' => ' + rows_num);
		if (start_with >= 0) {
			this.load_xml(this.data_url, start_with, rows_num);
		} else {
			// make visible rows smart and invisible - common ones
			this.dl_resmart_rows();
		}

//		alert('Loaing initiated => ' + top_pos + ' => ' + row_height + ' => ' +
//				first_visible_row);
	}

	// check if row with number row_index in grid is visible
	SmartGrid.prototype.dl_is_row_visible = function(row_index) {
		var top_pos = this.content_div.scrollTop;
		var row_height = this.dl_get_row_height();
		var first_visible_row = Math.floor(top_pos/row_height);
		var last_visible_row = Math.floor((top_pos+this.content_div.offsetHeight)/row_height);

		if (row_index>=first_visible_row && row_index<=last_visible_row) {
			return true;
		}

		return false;
	}

	// check if row with number row_index in grid is exists yet
	// always returns row of common grid!!!
	SmartGrid.prototype.dl_is_row_exists = function(row_index) {
		if (this.common_grid) return this.common_grid.dl_is_row_exists(row_index);

		if (!this.rows[row_index]) return false;
		var row_id = this.get_id_attribute(this.rows[row_index]['id']);
		var row_obj = document.getElementById(row_id);

		return row_obj?row_obj:false;
	}

	// check if row cells are Smart Cells (enough to check one cell only)
	SmartGrid.prototype.dl_is_row_smart = function(row_index) {
		var row_obj = this.dl_is_row_exists(row_index);
		if (!row_obj) return false;

		return SmartCells.get_cell(row_obj.cells[0])?true:false;
	}

	// make row cells Smart
	SmartGrid.prototype.dl_do_smart_row = function(row_index) {
		var row_obj = this.dl_is_row_exists(row_index);
		if (!row_obj) return false;

		var row_id = row_obj.getAttribute('id');

		if (this.frozen_grid) {
			var frozen_row_obj = this.get_frozen_row_analog(row_obj);
			var frozen_row_id = frozen_row_obj.getAttribute('id');
		}

		var col_len = this.columns.length;
		var j;
		for (j=0; j<col_len; j++) {
			if (this.frozen_col_num && j < this.frozen_col_num) {
				var cur_cell = frozen_row_obj.cells[j];

				// make the cell Smart
				this.frozen_grid.do_smart_cell(cur_cell, frozen_row_id, j,
									this.rows[row_index]['data'][j]);
			} else {
				var cur_cell = row_obj.cells[j-this.frozen_col_num];

				// make the cell Smart
				this.do_smart_cell(cur_cell, row_id, j,
									this.rows[row_index]['data'][j]);
			}

		}

		// set tab order for the row smart cells
		this.set_smart_row_tab_order(row_obj);

	}

	// unmake row cells Smart
	SmartGrid.prototype.dl_undo_smart_row = function(row_index) {
		var row_obj = this.dl_is_row_exists(row_index);
		if (!row_obj) return false;

		var row_id = row_obj.getAttribute('id');

		if (this.frozen_grid) {
			var frozen_row_obj = this.get_frozen_row_analog(row_obj);
		}


		// cancel editing if active cell belongs to the row
		if (SmartCells.active_cell &&
			SmartCells.active_cell.params['row_id'] == row_id) {
			SmartCells.active_cell.cancel_edit();
		}


		var col_len = this.columns.length;
		var j;
		for (j=0; j<col_len; j++) {
			if (this.frozen_col_num && j < this.frozen_col_num) {
				var cur_cell = frozen_row_obj.cells[j];
			} else {
				var cur_cell = row_obj.cells[j-this.frozen_col_num];
			}
//clear();
//show_val('row_id = ' + row_id);
//show_val('row_id_index = ' + this.get_row_index(row_id));
//show_val('frozen_row_id = ' + frozen_row_obj.id);
//show_val('frozen_row_id_index = ' + this.frozen_grid.get_row_index(frozen_row_obj.id));
//show_val('cur_cell_id = ' + cur_cell.id);

			var sc = SmartCells.get_cell(cur_cell);
			// store current cell value in cache
			this.rows[row_index]['data'][j] = sc.get_current_value();

			SmartCells.delete_smart_cell(sc);
		}

	}

	// make smart rows in visible area only
	SmartGrid.prototype.dl_resmart_rows = function() {
		var i;
		var visible_rows = this.dl_get_visible_rows();
		var first_visible_row = visible_rows[0];
		var last_visible_row = visible_rows[1];

		for (i=0; i<this.real_page_size; i++) {
			if (!this.dl_is_row_exists(i)) continue;

			if (i >= first_visible_row && i <= last_visible_row) {
				if (!this.dl_is_row_smart(i)) {
					this.dl_do_smart_row(i);
				}
			} else {
				if (this.dl_is_row_smart(i)) {
					this.dl_undo_smart_row(i);
				}
			}
		}

	}

	// get fist and last visible (even partially) rows
	SmartGrid.prototype.dl_get_visible_rows = function() {
		var top_pos = this.content_div.scrollTop;
		var row_height = this.dl_get_row_height();
		var first_visible_row = Math.floor(top_pos/row_height);
		var last_visible_row = Math.floor((top_pos+this.content_div.offsetHeight)/row_height);

		return Array(first_visible_row, last_visible_row);
	}

	// after we deleted rows selected some spacers follow one after other
	// so, join such spacers
	SmartGrid.prototype.dl_join_spacers = function() {
		var rows = this.get_rows();
		var rows_len = rows.length;

		var i;
		var last_spacer = null;
		var spacer_name = this.dl_spacer_id_prefix();
		for (i=0; i<rows_len; i++) {
			var row = rows[i];
			if (row.getAttribute('name') == spacer_name) {
				if (last_spacer) {
					// determine heights of this and last spacers in rows
					var spacer_height = this.dl_get_height_in_rows(row);
					var last_spacer_height = this.dl_get_height_in_rows(last_spacer);

//alert(last_spacer_height + ' => ' + spacer_height);
					// leave only one spacer that has height as two
					this.dl_change_spacer(last_spacer,
											spacer_height+last_spacer_height);

					// remove second spacer
					row.parentNode.removeChild(row);
					i--;
					rows_len--;
					
				} else {
					last_spacer = row;
				}
			} else {
				last_spacer = null;
			}
		}

	}

	// return height of the spacer in rows
	SmartGrid.prototype.dl_get_height_in_rows = function(spacer_obj) {
		var row_height = this.dl_get_row_height();

		return parseInt(spacer_obj.style.height)/row_height;
	}

	// returns index in this.rows for row with id = row_id
	SmartGrid.prototype.dl_get_row_index = function(row_id) {
		var i;
		for (i in this.rows) {
//alert(this.get_id_attribute(this.rows[i]['id']) + ' => ' + row_id);
			if (this.get_id_attribute(this.rows[i]['id']) == row_id) return i;
		}
		return -1;
	}

/*
	!Dynamic loading functions
*/