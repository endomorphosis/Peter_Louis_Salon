/*
	Frozen columns functions
*/

	SmartGrid.prototype.freeze_columns = function(col_num) {
		var col_length = this.columns.length;

		if (col_num <= 0 || col_num >= col_length) return;

		// create table, parent for two grids (common and frozen)
		this.super_main_table = document.createElement('TABLE');

		this.super_main_table.border = 0;
		this.super_main_table.cellSpacing = 0;
		this.super_main_table.cellPadding = 0;
		this.super_main_table.width = "100%";
		this.super_main_table.style.tableLayout = "fixed";

		var super_main_row = this.super_main_table.insertRow(0);
		var frozen_cell = super_main_row.insertCell(0);

		// determine width of frozen grid
		var i;
		var frozen_width = 0;
		for (i=0; i<col_num; i++) {
			frozen_width += Number(this.columns[i]['col_width']);
		}

		// frozen grid's div
		var frozen_div = document.createElement("DIV");
		var frozen_id = this.get_frozen_prefix() + this.obj.getAttribute('id');
		frozen_div.setAttribute('id', frozen_id);
		frozen_div.style.width = frozen_width + 'px';
		frozen_div.style.height = this.obj.style.height;

		frozen_cell.appendChild(frozen_div);
		frozen_cell.width = frozen_width;
//frozen_cell.bgColor = 'blue';

		var common_cell = super_main_row.insertCell(1);
//common_cell.bgColor = 'green';

		common_cell.appendChild(this.main_table);

		this.obj.appendChild(this.super_main_table);


		// store number of frozen columns
		this.frozen_col_num = col_num;

		// create frozen grid
		this.frozen_grid = new SmartGrid(frozen_id);
		this.frozen_grid.obj.className = this.get_frozen_prefix() + 'sg_main_div';
		this.frozen_grid.common_grid = this;

		this.frozen_grid.frozen_div = frozen_div;
		this.frozen_grid.frozen_cell = frozen_cell;
//		this.frozen_grid.frozen_grid = true;
		// hide vertical scrollbar of the frozen grid


		if (is_opera) {
			this.frozen_grid.content_div.style.overflow = "scroll";
			this.content_div.style.overflow = "scroll";
		} else {
			this.frozen_grid.content_div.style.overflowX = "scroll";
			this.frozen_grid.content_div.style.overflowY = "hidden";

			// make common grid horizontal scrollbar to be always visible
			this.content_div.style.overflowX = "scroll";
		}


		// Opera does not support overflowY, so cancel all scroll attempts
		var onscroll = function(e){
			var content_div = e.target?e.target:window.event.srcElement;
			var grid = content_div.grid;

			// scroll frozen grid if any
			if (grid.common_grid) {
				content_div.scrollTop = grid.common_grid.content_div.scrollTop;
			}
		};
		sc_attach_event(this.frozen_grid.content_div, 'scroll', onscroll);


		for (i=0; i<col_num; i++) {
			this.frozen_grid.columns[i] = this.columns[i];
		}

		this.frozen_grid.show();


	}

	// return row from frozen grid with the same index as row row_obj
	// in common grid (if reverse set then search in common grid and suppose
	// that row_obj is in frozen one)
	SmartGrid.prototype.get_frozen_row_analog = function(row_obj, reverse) {
		if (!this.frozen_grid && !this.common_grid || !row_obj) return null;

		var src_id = row_obj.getAttribute('id');

		// frozen rows
		if (reverse) {
			var id = this.remove_frozen_prefix(src_id);
		} else {
			var id = this.get_frozen_id_attribute(src_id);
		}

		return document.getElementById(id);

/*
		var row_index = this.get_row_index(row_obj.getAttribute('id'));
		if (row_index < 0) return null;

		// frozen rows
		if (reverse) {
			var rows = this.common_grid.get_rows();
		} else {
			var rows = this.frozen_grid.get_rows();
		}
		return rows[row_index];
*/
	}

	// returns id of frozen row that corresponds to common row with id = row_id
	SmartGrid.prototype.get_frozen_id_attribute = function(id) {
		return this.get_frozen_prefix() + id;
	}

	// returns prefix used for frozen rows
	SmartGrid.prototype.get_frozen_prefix = function() {
		return 'frozen_';
	}

	// removes frozen prefix from row id
	SmartGrid.prototype.remove_frozen_prefix = function(row_id) {
		var re = new RegExp('^'+this.get_frozen_prefix(), '');
		return row_id.replace(re, '');
	}

/*
	!Frozen columns functions
*/