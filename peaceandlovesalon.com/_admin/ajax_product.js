jQuery("#product").jqGrid({        
   	url:'ajax_product_view.php?nd='+new Date().getTime(),
	datatype: "json",
	height:400,
   	colNames:['Index','Item_name', 'Item_price','Item_type', 'Isshow','Edit','Delete'],
   	colModel:[
   	{name:'id',index:'id', width:100},
		{name:'Item_name',index:'Item_name', width:280},
   	{name:'Item_price',index:'Item_price', width:150},
		{name:'Item_type',index:'Item_type', width:150},
		{name:'Isshow',index:'Isshow', width:100},
		{name:'edit',index:'edit', width:100},
		{name:'delete',index:'delete', width:100}


   	],
   	rowNum:15,
//   	rowList:[10,20,30],
   	imgpath: 'images',
   	pager: jQuery('#pagerb'),
   	sortname: 'id',
    viewrecords: true,
    sortorder: "asc"
});
var timeoutHnd;
var flAuto = false;

function doSearch(ev){
	if(!flAuto)
		return;
//	var elem = ev.target||ev.srcElement;
	if(timeoutHnd)
		clearTimeout(timeoutHnd)
	timeoutHnd = setTimeout(gridReload,500)
}

function gridReload(){
	var name = jQuery("#name").val();
	var price = jQuery("#price").val();
	var type = jQuery("#type").val();
	jQuery("#product").setUrl("ajax_product_view.php?name="+name+"&price="+price+"&type="+type);
	jQuery("#product").setPage(1);
	jQuery("#product").trigger("reloadGrid");
}
function enableAutosubmit(state){
	flAuto = state;
	jQuery("#submitButton").attr("disabled",state);
}
