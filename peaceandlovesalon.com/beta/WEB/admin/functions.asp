<%
'openconnection.asp creates a connection object by the name of conn

'Discounts
dim bDiscountOn, dDiscountToDate, sDiscountText, cDiscountMinPurchase, cDiscountAmountOff, bDiscountFreeShipping

'Settings
dim FeaturedCategoryID, FeaturedCategoryName

const conRootDept = ""
dim sMenuString,sMenuStringHTML,sMainID,sShowItemsFrom,sShowAsList,sRedirect,sParentID
sDeptID = conRootDept

const conMerchantPhone = "718-493-1181"
'const conMerchantEMail = "service@shopdetails.com"
const conMerchantEMail = "jj@spotlightdesign.com"

dim sPathStringHTML,sSubID,sDeptID,lDeptID,sParentName,sSubName,sDeptName,i,iBrandID,sBrandName,sBrandHeaderImage,bShowItems,bShowBrandSelector,sItem,sBarImage,sTitle
sSubID = ""
sDeptID = ""
iBrandID = 0

sItem = Request.QueryString("Item") & ""

'CheckCache

if request.querystring("dept")<>"" and isNumeric(request.querystring("dept")) then
	iBrandID = CInt(Request.QueryString("dept"))
'	if iBrandID = 6	then	'Isap
'		iBrandID = 5	'as per request 1/17/03
'	end if
	GetBrandInfo(iBrandID)
end if

if request.querystring("cl")<>"" and isNumeric(request.querystring("cl")) then
	sSubID = request.querystring("cl")
	sDeptID = left(sSubID,2)

'	Select Case sDeptID
'		case "10"
'			sDeptName = "Earings"
'		case "20"
'			sDeptName = "Necklaces"
'		case "30"
'			sDeptName = "Bracelets"
'	end select

	sDeptName = GetSubName(sDeptID)
	sSubName = GetSubName(sSubID)
	sParentName = GetSubName(left(sSubID,len(sSubID)-2))
End if

if iBrandID <> 0 then
	sBarImage = "brands/" & sBrandHeaderImage
elseif sDeptID <> "" then
	select case sDeptID
		case "10"
			sBarImage = "main-boys.gif"
		case "20"
			sBarImage = "main-girls.gif"
		case "30"
			sBarImage = "main-infants.gif"
	end select
end if

if sBarImage = "" then 
	if ucase(Right(Request.ServerVariables("SCRIPT_NAME"),10)) = "BRANDS.ASP" then
		sBarImage = "main-brands.gif"
	else
		sBarImage = "main-tc.gif"
	end if
end if

sPathStringHTML=GetPathString()

if len(sSubID) > 1 then bShowItems = true
if sSubID = "" and iBrandID<>0 then bShowItems = true
bShowBrandSelector = bShowItems
if Request.QueryString("search") <> "" then bShowItems = true

if sSubName <> "" then
	sTitle = " - " & sSubName
end if
if iBrandID <> 0 then
	sTitle = sTitle & " - " & sBrandName
end if


sub CheckCache()
	dim key
	'clear cache after 12 hrs
	if now() - Application("CacheTime") > (1 / 24 * 12) then
		for each key in Application.Contents
			Application(key) = empty
		next
		Application("CacheTime") = now()
	end if
end sub

function GetSubName(sSubID)
	dim rs
	if sSubID = "" then
		GetSubName = "Details"
	else
		if Application("Sub" & sSubID) = "" then
			set rs = Server.CreateObject("ADODB.Recordset")
			rs.open "Select Name from pp_Collections where Collection = '" & sSubID & "';",conn
			if rs.eof then
				GetSubName = "-"
			else
				GetSubName = rs("Name")
			end if
			Application("Sub" & sSubID) = GetSubName
		else
			GetSubName = Application("Sub" & sSubID)
		end if
	end if
end function

function GetBrandInfo(iBrandID)
	'sets the global variables, sBrandName and sBrandHeaderImage
	dim rs
	if Application("BrandName" & iBrandID) = "" then
		set rs = Server.CreateObject("ADODB.Recordset")
		rs.open "Select Department from pp_Departments where ID = " & iBrandID & ";",conn
		if rs.eof then
			Application("BrandName" & iBrandID) = "-"	'so we don't keep looking for something that doesn't exist
		else
			Application("BrandName" & iBrandID) = rs("Department").value
'			Application("BrandHeader" & iBrandID) = rs("HeaderImage")
		end if
	end if
	sBrandName = Application("BrandName" & iBrandID)
'	sBrandHeaderImage = Application("BrandHeader" & iBrandID)
end function

function GetPathString()
	dim sTmp
	sTmp = "<a class=""path"" href=""./"">Home</a>"
	if len(sSubID) > 2 then
		sTmp = sTmp & " &gt; <a class=""path"" href=""catalog.asp?dept=" &  sDeptId & """>" & sDeptName & "</a>"
	end if
	if len(sSubID) > 5 then
		for i =  4 to len(sSubID) - 1 step 2
			sTmp = sTmp & " &gt; <a class=""path"" href=""catalog.asp?dept=" &  left(sSubID,i) & """>" & GetSubName(left(sSubID,i)) & "</a>"
		next
	end if
	if sSubID <> "" then
		if iBrandID <> 0 or Request.QueryString("Item") <> "" then 
			sTmp = sTmp & " &gt; <a class=""path"" href=""catalog.asp?dept=" & sSubID & """>" & sSubName & "</a>"
		else
			sTmp = sTmp & " &gt; <span class=""path"">" & sSubName & "</span>"
		end if
	End if
	if sSubID = "" and iBrandID <> 0 then
		sTmp = sTmp & " &gt; <a class=""path"" href=""Brands.asp"">Shop by Brand</a>"
	end if
	if iBrandID <> 0 then
		if Request.QueryString("Item") <> "" then
			sTmp = sTmp & " &gt; <a class=""path"" href=""catalog.asp?dept=" & sSubID & "&Brand=" & iBrandID & """>" & sBrandName & "</a>"
		else
			sTmp = sTmp & " &gt; <span class=""path"">" & sBrandName & "</span>"
		end if
	end if
	if Request.QueryString("search") <> "" then
		sTmp = sTmp & " &gt; <span class=""path"">Search: '" & Request.QueryString("search") & "'</span>"
	end if
	GetPathString = sTmp
end function

function GetSubCategories(sSubID)
	'returns recordset of Categories for that SubID
	dim sSQL
	sSQL = "SELECT id, department FROM pp_Departments WHERE dept LIKE '" & sSubID & "__' ORDER BY dept;"
	'response.write sSQL
	set GetSubCategories = server.CreateObject("ADODB.Recordset")
	GetSubCategories.Open sSQL,conn
end function

function GetBrandsInSub(sSubID)
	dim sSQL
	'if sSubID <> "" then 'get brands in this sub, otherwise get all brands
		sSQL = "SELECT DISTINCT ID, BrandName FROM pp_qryBrandsInSub WHERE dept LIKE '" & sSubID & "%' ORDER BY BrandName;"
	'else
	'	sSQL = "SELECT ID, BrandName FROM pp_Brands ORDER BY BrandName;"
	'end if
	'response.write sSQL
	set GetBrandsInSub = server.CreateObject("ADODB.Recordset")
	GetBrandsInSub.Open sSQL,conn
end function

function GetBrands()
	dim sSQL
		sSQL = "SELECT ID, BrandName, ThumbNail FROM pp_Brands ORDER BY BrandName;"
	set GetBrands = server.CreateObject("ADODB.Recordset")
	GetBrands.Open sSQL,conn
end function

Function GetProductList()
	dim sSQL
	if Request.QueryString("search") <> "" then
		sSQL = "SELECT * FROM pp_Products"
		sSQL = sSQL & " INNER JOIN Departments ON Products.DepartmentID = Departments.ID"
		sSQL = sSQL & " WHERE ((Products.Description LIKE '%" & MakeSQLSafe(Request.QueryString("search")) & "%')"
		sSQL = sSQL & " or (Products.ProductNumber = '" & MakeSQLSafe(Request.QueryString("search")) & "'))"
		if sSubID <> "" then
			sSQL = sSQL & " AND ((Products.ProductNumber) In (SELECT ProductNumber FROM pp_ProductCollection WHERE dept LIKE '" & sSubID & "%'))"
		End if
		sSQL = sSQL & " ORDER By ProductNumber;"
	else
		if sSubID <> "" then
			if len(sSubID) > 1 then
				sSQL = "SELECT * FROM pp_qryProductList WHERE Collection LIKE '" & sSubID & "%'"
				if iBrandID <> 0 then
					sSQL = sSQL & " AND DepartmentID = " & iBrandID
				end if
			else
				sSQL = "SELECT Collection, ProductNumber FROM pp_qryProductList WHERE False"
			end if
			sSQL = sSQL & " ORDER By Collection, ProductNumber;"
		else
			if iBrandID <> 0 then
				sSQL = "SELECT * FROM pp_Products WHERE [show on web] and DepartmentID = " & iBrandID
			else
				sSQL = "SELECT ProductNumber FROM pp_Products WHERE False"
			end if
			sSQL = sSQL & " ORDER By ProductNumber;"
		end if	'if sSubID <> ""
		
	end if
	set GetProductList = server.CreateObject("ADODB.Recordset")
	GetProductList.CursorLocation = adUseClient
'	response.write sSQL
	GetProductList.Open sSQL,conn
End function

Function GetItem(sItem)
	dim sSQL
	sSQL = "SELECT * FROM pp_Products WHERE ProductNumber = '" & MakeSQLSafe(sItem) & "'"
	set GetItem = Server.CreateObject("ADODB.Recordset")
	GetItem.Open sSQL, conn
End Function

Function GetChoices(sItem,sCode)
	dim sSQL
	sSQL = "SELECT * FROM pp_Choices"
	sSQL = sSQL & " WHERE Location = 'NY' AND ProductNumber = '" & MakeSQLSafe(sItem) & "'"
	if sCode <> "" then
		sSQL = sSQL & " AND Code = '" & MakeSQLSafe(sCode) & "'"
	end if
	sSQL = sSQL & " ORDER BY Description, Code;"
	set GetChoices = server.CreateObject("ADODB.Recordset")
	GetChoices.Open sSQL,conn
End Function

Function MakeSQLSafe(s)
	if s = "" then
		MakeSQLSafe = "null"
	else
		MakeSQLSafe = Replace(s,"'", "''")
	end if
End Function

'Function SizeFields()
'	SizeFields = Array("1M", "3M","6M", "9M", "12M", "18M", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "12", "14", "16", "18", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "O/S", "T0", "T1", "T2", "T3", "T4", "T5", "S", "M", "L", "48", "50", "52", "54", "56", "58")
'End Function

Function AddField(rs,sField,sForm)
	dim sTmp
	on error resume next
	sTmp = Request.Form(sForm)
	if sTmp <> "" then
		rs(sField) = sTmp
	else
		rs(sField) = null
	end if
	if Err then
		response.write "Error while saving <b>" & sField & "</b><br>Value: '" & sTmp & "'<br><span style=""color:red"">" & Err.Description & "</span>"
		response.end
	end if
	on error goto 0
End function

Function AddFieldByValue(rs,sField,sValue)
	on error resume next
	if sValue <> "" then
		rs(sField) = sValue
	else
		rs(sField) = null
	end if
	if Err then
		response.write "Error while saving <b>" & sField & "</b><br>Value: '" & sTmp & "'<br><span style=""color:red"">" & Err.Description & "</span>"
		response.end
	end if
	on error goto 0
End function


Function GetWishList(CustomerID)
	'returns recordset of Products for Specific CategoryID
	dim cmd
	set cmd = server.createobject("ADODB.Command")
	cmd.ActiveConnection = conn
	cmd.CommandText = "spGetWishList"
	cmd.CommandType = adCmdStoredProc
	cmd.parameters.append cmd.CreateParameter ("@CustomerID",adInteger,adParamInput,,CustomerID)
	set GetWishList = server.CreateObject("ADODB.Recordset")
	GetWishList.CursorLocation = adUseClient
	GetWishList.Open cmd,,adOpenDynamic,adLockReadOnly
End function


Function isEMail(byref sString)
	'using by ref since we are trimming it. (even though it should be trimmed, the function should not change the value of its parameters!?!.
	dim iAtPos
	isEMail = False
	if isNull(sString) then
		exit function
	end if
	if instr(sString," ") then
		exit function
	end if
	sString = trim(sString)
	if len(sString) >= 7 then 'x@xx.xx
		iAtPos = instr(sString,"@")
		if iAtPos > 1 then
			if instr(iAtPos + 3,sString,".") then
				isEMail = True
			end if
		end if
	end if
End Function

Sub SendEMail(sFrom, sReplyTo, sTo, sSubject, sBody, bIsHTML)
	dim objMail
	set objMail = Server.CreateObject("Persits.MailSender")
	objMail.host = "mail.spotlightdesign.com"
	objMail.From = sFrom
	if sReplyTo <> "" then
		objMail.AddReplyTo sReplyTo
	End if
	objMail.AddAddress sTo
	objMail.Subject = sSubject
	objMail.Body = sBody
	objMail.isHTML = bIsHTML
	objMail.Send

	set objMail = nothing
End Sub

Function GetDiscountsRS()
	dim sSQL
	sSQL = "SELECT * FROM pp_Discounts WHERE ID = 0 AND '" & Date() & "' Between DateFrom and DateTo;"
	set GetDiscountsRS = Server.CreateObject("ADODB.Recordset")
	GetDiscountsRS.open sSQL, conn()
End Function

Function GetDiscounts()
	dim rsDiscounts, aDiscountInfo, i
	if isEmpty(Application("DiscountInfo")) then
		'set rsDiscounts = GetDiscountsRS()
		'if rsDiscounts.eof then
			aDiscountInfo = Array(False)
		'else
		'	aDiscountInfo = Array(True,rsDiscounts("DateTo").value, rsDiscounts("Text").value, rsDiscounts("MinPurchase").value, rsDiscounts("AmountOff").value, rsDiscounts("FreeShipping").value)
		'End if
		Application("DiscountInfo") = aDiscountInfo
	else
		aDiscountInfo = Application("DiscountInfo")
	End if
	bDiscountOn = aDiscountInfo(0)
	if bDiscountOn then
		dDiscountToDate = aDiscountInfo(1)
		sDiscountText = aDiscountInfo(2)
		cDiscountMinPurchase = aDiscountInfo(3)
		cDiscountAmountOff = aDiscountInfo(4)
		bDiscountFreeShipping = aDiscountInfo(5)
	End if
End Function

function EmailRegistered(sEmail)
	EmailRegistered = instr("|jj@spotlightdesign.com|mendel@spotlightdesign.com|landcha2@aol.com|friedpotat@aol.com|bluffaloop@aol.com|libaandru@hotmail.com|surie@bethrivkah.com|dcunin@hotmail.com|mysty@tellurian.net|", "|" & lcase(sEmail) & "|") >0
End Function

Function ShippingRates(lItems, lValue)
	dim aRates(3,1), cInsurance
	cInsurance = Int((lValue - 1)/100)
	aRates(0,0) = "GND"
	aRates(0,1) = 5 + (lItems * 0)' + cInsurance
	aRates(1,0) = "3DS"
	aRates(1,1) = 15 + (lItems * 0)' + cInsurance
	aRates(2,0) = "2DA"
	aRates(2,1) = 20 + (lItems * 0)' + cInsurance
	aRates(3,0) = "1DA"
	aRates(3,1) = 25 + (lItems * 0)' + cInsurance
	ShippingRates = aRates
End Function


CheckCache 
GetDiscounts()
%>