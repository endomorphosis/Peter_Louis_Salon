<!--#include file="uploadit.inc" -->
<link href="include/Style.css" rel="stylesheet" type="text/css">


<%

'Response.Write(request("mObjName"))

   DIM ufd, isErr
   ufd = ""
   isErr = ""
   ufd = TRIM(SESSION("uploadDir"))
   mObjName = request("mObjName")
   
   IF Request.querystring("mObjName") <> "" Then
	session("mObjName") =  request("mObjName")
   end if
   
   IF mObjName="" THEN mObjName=session("mObjName")
%>   

<html>
<style type="text/css">
<!--
.style1 {color: #FFFFFF}
-->
</style>
<head>
<title>
::Peter Louis Salon::
</title>
</head>
<body topmargin="0" leftmargin="0">
<% 
'response.Write(Request.TotalBytes)

If Len( Request.TotalBytes) > 0 Then

	Init     '- function in uploadit.inc that pulls out the file and all the form data
%>
	<CENTER>
<table border="0" cellpadding="0" cellspacing="5" width="100%" >
  <tr>
  <td width="100%" >
    <TABLE WIDTH="95%" BORDER="0" align="center" cellspacing="0" cellpadding="3" class="tBorder">
		<TR bgcolor='#003399'>
			
          <TD ALIGN='LEFT' bgcolor="#165C86" class="style1"> 
			<FONT size='2' face='Arial, Helvetica, sans-serif'><B>Peter Louis 
			Salon - Product Image Upload</B></font> </TD>
		</TR>
		<TR>
		<TD>
			<TABLE WIDTH='100%' BORDER='0' cellspacing='0' cellpadding='0' RULES=ROWS>
              <TR> 
                <TD WIDTH='27%'> <FONT size="2" face="Verdana, Arial, Helvetica, sans-serif" class="font1">File 
                  Name:</font> </TD>
                <TD class="font1"> <font size="2" face="Verdana, Arial, Helvetica, sans-serif">
                  
				  <%if Request.TotalBytes <= 153600 then%>
				  <%mfName = getFileName("txtFile")
						Response.Write mfName
						
						if session("mObjName") = "logo_small" then
							session("l_small") = mfName
						else
							session("l_large") = mfName
						end if
						%>
				  <%end if%>	
				  
                  </font></TD>
              </TR>
              <TR> 
                <TD> <FONT size="2" face="Verdana, Arial, Helvetica, sans-serif" class="font1">Content 
                  Type:</font> </TD>
                <TD class="font1"> <font size="2" face="Verdana, Arial, Helvetica, sans-serif"><%=getContentType("txtFile")%> </font></TD>
              </TR>
              <%
				if Request.TotalBytes <= 153600 then
					isUploaded = saveAs("txtFile", "txtSaveAs")
				end if
				
				IF isUploaded = True THEN %>
              <TR> 
                <TD class="font1"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Result:</font></TD>
                <TD ><B>File uploaded successfully.</B></TD>
              </TR>
              <%ELSEIF isErr="" THEN%>
              <TR> 
                <TD class="font1"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Result:</font></TD>
                <TD class="font1"><FONT size='2' face='Verdana, Arial, Helvetica, sans-serif' color='#FF0000'> 
                  <%if Request.TotalBytes > 153600 then%>
                  <b>Image not uploaded. Image upload size should be below 100kb. 
                  Please choose another image.</b> 
                  <%end if%>
                  </FONT></TD>
              </TR>
              <%END IF%>
            </TABLE>
		</TD>
		</TR>
	</TABLE>
      <div align="center">
        <%End If%>
        <br>
        <font color="#FF0000" size="1" face="Verdana, Arial, Helvetica, sans-serif">Upload 
        and host this image (must be under 100K and in JPG format). Remembered thumb & large images name must be same.</font><br>
      </div>

<center><br>
        <%if session("mObjName") <> "" then%>
        <font size="2" face="Verdana, Arial, Helvetica, sans-serif">Select the 
        Thumbnail Image first:</font> 
        <%else%>
        <b>
        <font size="2" face="Verdana, Arial, Helvetica, sans-serif">Click on 
DONE to return:</font> 
        </b> 
        <%end if%>
      </center>

<script language="JavaScript">
function file_validation(thisform) {

   	if (thisform.txtFile.value == "") {
		alert ("File must be browse before upload");
		thisform.txtFile.focus();
		return false;
	}

	f1 = thisform.txtFile.value;		
       	temp_string1 = f1.indexOf(".JPG");
	   	temp_string2 = f1.indexOf(".jpg");
		//temp_string3 = f1.indexOf(".png");	
						
		if (temp_string1 == -1 && temp_string2 == -1)
	{
		alert ("You can only upload 'jpg' format file. Please select valid file format.");
		thisform.txtFile.focus();
		return false;
	}

return true;
}
</script>


<form name="frmUpload" ACTION="uploadfile.asp" method="POST" onsubmit="return file_validation(this);" enctype="multipart/form-data">
  <table WIDTH="98%" BORDER="0" align="center" cellspacing="0" cellpadding="2"  class="tBorder">
    <tr> 
            <td WIDTH="30%"><font size="2" face="Verdana, Arial, Helvetica, sans-serif" class="font1">
			<%if session("mObjName") <> "" then%>
			Thumb Image:
			<%else%>
			<b>Image Uploaded:</b>
			<%end if%>
			</font></td>
      <td WIDTH="70%">
		<font class="font1"> 
			<input type="file" name="txtFile" SIZE="25"  class="btnNormal">
	        </font>
      </td>
    </tr>
    <tr> 
      <td><Input type="HIDDEN" name="txtSaveAs" VALUE="<%=ufd%>"></td>
    </tr>
    <tr> 
      <td>&nbsp;</td>
      <td> <font size="2" face="Arial, Helvetica, sans-serif">
        <input TYPE="SUBMIT" NAME="cmdSubmit" VALUE=" Upload Now " class="btn1">
        <%if session("mObjName") = "" then%>
		<input TYPE="BUTTON" VALUE=" Done " onClick="doSubmit();" class="btn">
        <%end if%>
		</font></td>
    </tr>
  </table>
</form>

  <CENTER>	
  <hr size="1" width="95%" color="#C0C0C0">
        <font size="1"> &copy; Copyright 2006 Peter Louis Salon. All Rights Reserved.</font> 
      </CENTER>	
    </td>
  </tr>
</table>
</body>
</html>


<%
if mfName <> "" then
pfile = mfName
LeftString = pfile    
end if   
%>

<script language="javascript">
	function doSubmit()
	{
		window.opener.document.form1.img.value='<%=LeftString%>'; 
		window.close();
	}
</script>