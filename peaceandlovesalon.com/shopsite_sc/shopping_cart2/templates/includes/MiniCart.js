<script language="JavaScript" type="text/javascript">
function DisplayMiniCart(name,style) {
  var cookies=document.cookie;  //read in all cookies
  var start = cookies.indexOf(name + "=");  //set start to beginning of ss_cart cookie
  var cartvalues = "";
  var linecount = 0;
  var start1;
  var end1;
  var tmp;

  // Start Output
  document.write("<div class=\"MiniCart\">\n");

  if (style == "Detail")
  {
    document.write("<table class=\"MiniCart\" border=\"0\">\n");
    document.write("<tr>\n");
    document.write("<th class=\"MiniCart\" colspan=\"3\">");
[-- IF ANALYTICS_MULTI_DOMAIN --]
    document.write("<a class=\"MiniCart\" href=\"javascript:__utmLinker(\'[-- SHOPPING_CART_URL --]\');\">");
[-- ELSE --]
    document.write("<a class=\"MiniCart\" href=\"[-- SHOPPING_CART_URL --]\">");
[-- END_IF --]
    document.write("[-- STORE.SC_YourShoppingCart --]");
    document.write("<\/a><\/th><\/tr>");
  }
  else if (style == "Summary")
  {
[-- IF ANALYTICS_MULTI_DOMAIN --]
    document.write("<a class=\"MiniCart\" href=\"javascript:__utmLinker(\'[-- SHOPPING_CART_URL --]\');\">");
[-- ELSE --]
    document.write("<a class=\"MiniCart\" href=\"[-- SHOPPING_CART_URL --]\">");
[-- END_IF --]
    document.write("[-- STORE.SC_YourShoppingCart --]");
    document.write("<\/a>");
  }
  else
  {
[-- IF ANALYTICS_MULTI_DOMAIN --]
    document.write("<a class=\"MiniCart\" href=\"javascript:__utmLinker(\'[-- SHOPPING_CART_URL --]\');\">");
[-- ELSE --]
    document.write("<a class=\"MiniCart\" href=\"[-- SHOPPING_CART_URL --]\">");
[-- END_IF --]
    document.write("<img src=\"[-- OUTPUT_DIRECTORY_URL --]/[-- VAR.Media --]/cart-[-- VAR.MiniCartColor --].gif\" border=\"0\" name=\"cart\" align=\"top\">");
    document.write("<\/a>&nbsp;");
  }

  if (start == -1)  //No cart cookie
  {
    if (style == "Detail")
    {
      document.write("<\/table>\n");
    }
    else if ((style == "ItemCount") || (style == "Subtotal"))
    {
[-- IF ANALYTICS_MULTI_DOMAIN --]
      document.write("<a class=\"MiniCart\" href=\"javascript:__utmLinker(\'[-- SHOPPING_CART_URL --]\');\">");
[-- ELSE --]
      document.write("<a class=\"MiniCart\" href=\"[-- SHOPPING_CART_URL --]\">");
[-- END_IF --]
      document.write("0 [-- STORE.Items --]");
      document.write("<\/a>&nbsp;");
      document.write("<\/div>\n");
    }
    else
    {
      document.write("<\/div>\n");
    }      
  }
  else   //cart cookie is present
  {
    start = cookies.indexOf("=", start) +1;  
    var end = cookies.indexOf(";", start);  
    if (end == -1)
    {
      end = cookies.length;
    }
    cartvalues = unescape(cookies.substring(start,end)); //read in just the cookie data

    start = 0;
    while ((start = cartvalues.indexOf("|", start)) != -1)
    {
      start++;
      end = cartvalues.indexOf("|", start);
      if (end != -1)
      {
        linecount++;

/* none of the styles use the number of line items
        if ((linecount == 1) && (style != "Detail"))  // Number of Line Items
        {
          tmp = cartvalues.substring(start,end);
          colon = tmp.indexOf(":", 0);
          if ((style == "ItemCount") || (style == "Subtotal"))
          {
[-- IF ANALYTICS_MULTI_DOMAIN --]
            document.write("<a class=\"MiniCart\" href=\"javascript:__utmLinker(\'[-- SHOPPING_CART_URL --]\');\">");
[-- ELSE --]
            document.write("<a class=\"MiniCart\" href=\"[-- SHOPPING_CART_URL --]\">");
[-- END_IF --]
          }

          if (style == "Summary")
          {
            document.write("<br>[-- STORE.Contains --] <b>");
          } 
          document.write(tmp.substring(colon+1,end - start));
          if (style == "Summary")
          {
            document.write("<\/b>");
          }
          if ((tmp.substring(colon+1,end - start)) == 1 )
          {
            document.write(" [-- STORE.Item --]");
          }
          else
          {
            document.write(" [-- STORE.Items --]");
          }
          if (style == "ItemCount")
          {
            document.write("<\/a>");
          } 
          else if (style == "Subtotal")
          {
            document.write(": ");
          } 
          else if (style == "Summary")
          {
            document.write("<br>[-- STORE.Subtotal --]: <b>");
          }
        }
*/

        if ((linecount == 2) && (style != "Detail"))  // Total Quantity of Items
        {
          tmp = cartvalues.substring(start,end);
          colon = tmp.indexOf(":", 0);
          if ((style == "ItemCount") || (style == "Subtotal"))
          {
[-- IF ANALYTICS_MULTI_DOMAIN --]
            document.write("<a class=\"MiniCart\" href=\"javascript:__utmLinker(\'[-- SHOPPING_CART_URL --]\');\">");
[-- ELSE --]
            document.write("<a class=\"MiniCart\" href=\"[-- SHOPPING_CART_URL --]\">");
[-- END_IF --]
          }

          if (style == "Summary")
          {
            document.write("<br>[-- STORE.Contains --] <b>");
          } 
          document.write(tmp.substring(colon+1,end - start));
          if (style == "Summary")
          {
            document.write("<\/b>");
          }
          if ((tmp.substring(colon+1,end - start)) == 1 )
          {
            document.write(" [-- STORE.Item --]");
          }
          else
          {
            document.write(" [-- STORE.Items --]");
          }
          if (style == "ItemCount")
          {
            document.write("<\/a>");
          } 
          else if (style == "Subtotal")
          {
            document.write(": ");
          } 
          else if (style == "Summary")
          {
            document.write("<br>[-- STORE.Subtotal --]: <b>");
          }
        }




        if (linecount == 3)  // Product Subtotal
        {
          if ((style == "Subtotal") || (style == "Summary"))
          {
            tmp = cartvalues.substring(start,end);
            colon = tmp.indexOf(":", 0);
            document.write(tmp.substring(colon+1,end - start));
            if (style == "Summary") 
            {
              document.write("<\/b>");
            }
            else
            {
            document.write("<\/a>");
            }
          }
          else if (style == "Detail")
          {
            start1 = start;
            end1 = end;
            document.write("<tr><td class=\"MiniCartHead\">[-- STORE.Qty --]<\/td>");
            document.write("<td class=\"MiniCartHead\">[-- STORE.Product --]<\/td>");
            document.write("<td class=\"MiniCartHead\">[-- STORE.Price --]<\/td><\/tr>\n");
          }
        }

        if ((linecount > 3) && (style == "Detail"))  // individual products
        {
          tmp = cartvalues.substring(start,end);
          colon = tmp.indexOf(":", 0);
          document.write("<tr>");
          document.write("<td class=\"MiniCartQty\">");
          document.write(tmp.substring(0,colon));
          document.write("<\/td><td class=\"MiniCartProduct\">");
          colon2 = tmp.indexOf(":", colon+1);
          document.write(tmp.substring(colon2+1,end - start));
          document.write("<\/td><td class=\"MiniCartPrice\">");
          document.write(tmp.substring(colon+1,colon2));
          document.write("<\/td><\/tr>\n");
        }
        start = end;
      }
      else
        break;
    } // end while loop

    //close minicart HTML
    if (style != "Detail")
    {
      document.write("<\/div>\n");
    }
    else
    {
      document.write("<tr>");
      document.write("<td class=\"MiniCartSubtotalText\" colspan=\"2\">Subtotal<\/td>");
      document.write("<td class=\"MiniCartSubtotal\">");
      tmp = cartvalues.substring(start1,end1);
      colon = tmp.indexOf(":", 0);
      document.write(tmp.substring(colon+1,end1 - start1));
      document.write("<\/td>");
      document.write("<\/tr>");
      document.write("<\/table>\n");
      document.write("<\/div>\n");
    }
  }
}
</script>
