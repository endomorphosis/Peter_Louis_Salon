
#Texturize Include File Template
#Copyright 2008 ShopSite inc. Design by Lauren

<div id="minicart">
<script language="javascript">
var cookies=document.cookie;  //read in all cookies
var start = cookies.indexOf("ss_cart_[-- STORE_Serial_Number --]="); 
var cartvalues = "";
var linecount = 0;
var start1;
var end1;
var tmp;

// Start Output
  document.write("<a href=\"[-- SHOPPING_CART_URL --]\""); 
  document.write("style=\"text-decoration: underline;");
  document.write("color: [-- VAR.MiniCartColor --];");
  document.write("\">");
[-- IF SC_USE_SECURITY --]
  [-- IF VAR.Secure "yes" --]
  document.write("<img src=\"[-- STORE.SC_Secure_Image_URL --]/media/themesmedia/cart-[-- VAR.MiniCartImage --].gif\" border=\"0\" name=\"cart\" align=\"top\">");
  [-- ELSE --]
  document.write("<img src=\"[-- OUTPUT_DIRECTORY_URL --]/media/themesmedia/cart-[-- VAR.MiniCartImage --].gif\" border=\"0\" name=\"cart\" align=\"top\">");
  [-- END_IF --]
[-- ELSE --]
  document.write("<img src=\"[-- OUTPUT_DIRECTORY_URL --]/media/themesmedia/cart-[-- VAR.MiniCartImage --].gif\" border=\"0\" name=\"cart\" align=\"top\">");
[-- END_IF --]
  document.write("</a> ");

if (start == -1)  //No cart cookie
{
  document.write("<a href=\"[-- SHOPPING_CART_URL --]\"");
  document.write("style=\"text-decoration: underline;");
  document.write("color: [-- VAR.MiniCartColor --];");
  document.write("\">");
  document.write("0 [-- STORE.Items --]");
  document.write("</a> ");
}
else   //cart cookie is present
{
  start = cookies.indexOf("=", start) +1;  
  var end = cookies.indexOf(";", start);  

  if (end == -1)
  {
    end = cookies.length;
  }

  cartvalues = unescape(cookies.substring(start,end)); //read in just the cookie data

  start = 0;
  while ((start = cartvalues.indexOf("|", start)) != -1)
  {
    start++;
    end = cartvalues.indexOf("|", start);
    if (end != -1)
    {
      linecount++;

      if (linecount == 2) // Total Quantity of Items
      {
        tmp = cartvalues.substring(start,end);
        colon = tmp.indexOf(":", 0);
        document.write("<a href=\"[-- SHOPPING_CART_URL --]\"");
        document.write("style=\"text-decoration: underline;");
        document.write("color: [-- VAR.MiniCartColor --];");
        document.write("\">");
        document.write(tmp.substring(colon+1,end - start));
        if ((tmp.substring(colon+1,end - start)) == 1 )
        {
          document.write(" [-- STORE.Item --]");
        }
        else
        {
          document.write(" [-- STORE.Items --]");
        }
        document.write(": ");
      }

      if (linecount == 3)  // Product Subtotal
      {
        tmp = cartvalues.substring(start,end);
        colon = tmp.indexOf(":", 0);
        document.write(tmp.substring(colon+1,end - start));
        document.write("</a>");
      }

      start = end;
    }
    else
      break;
    }
  } // end while loop

  //close minicart HTML
</script>
</div>