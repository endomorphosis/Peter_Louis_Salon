#Curved Include File
#Copyright 2008 ShopSite Inc. Design by Lauren

      <h3>[-- STORE.SC_YourShoppingCart --]</h3>
[-- IF MiniCart --]
<script language="javascript">
function alternate(id){
 if(document.getElementsByTagName){  
   var table = document.getElementById(id);  
   var rows = table.getElementsByTagName("tr");  
   for(i = 0; i < rows.length; i++){          
 //manipulate rows
     if(i % 2 == 0){
       rows[i].className = "prod_list";
     }else{
       rows[i].className = "prod_alt";
     }      
   }
 }
}
</script>

<script language="javascript">
var cookies=document.cookie;  //read in all cookies
var start = cookies.indexOf("ss_cart_[-- STORE_Serial_Number --]="); 
var cartvalues = "";
var linecount = 0;
var start1;
var end1;
var tmp;

// Start Output
document.write("<table id=\"prod_list\" align=\"center\" class=\"boxedin\" width=\"99%\">\n");

if (start == -1)  //No cart cookie
{
}
else   //cart cookie is present
{
  start = cookies.indexOf("=", start) +1;  
  var end = cookies.indexOf(";", start);  

  if (end == -1)
  {
    end = cookies.length;
  }

  cartvalues = unescape(cookies.substring(start,end)); //read in just the cookie data

  start = 0;
  while ((start = cartvalues.indexOf("|", start)) != -1)
  {
    start++;
    end = cartvalues.indexOf("|", start);
    if (end != -1)
    {
      linecount++;

      if (linecount == 3)  // Product Subtotal
      {
        start1 = start;
        end1 = end;
        document.write("<tr><td bgcolor=\"#FFFFFF\"><b>Qty</b></td>");
        document.write("<td align=\"center\" bgcolor=\"#FFFFFF\"><b>Product</b></td>");
        document.write("<td align=\"center\" bgcolor=\"#FFFFFF\"><b>Price</b></td></tr>\n");
      }

      if (linecount > 3)  // individual products
      {
        tmp = cartvalues.substring(start,end);
        colon = tmp.indexOf(":", 0);
        document.write("<tr class=\"prod_list\">");
        document.write("<td align=\"center\">");
        document.write(tmp.substring(0,colon));
        document.write("</td><td>");
        colon2 = tmp.indexOf(":", colon+1);
        document.write(tmp.substring(colon2+1,end - start));
        document.write("</td><td align=\"right\">");
        document.write(tmp.substring(colon+1,colon2));
        document.write("</td></tr>\n");
      }

      start = end;
    }
    else
    {
      break;
    }
  } // end while loop

  //close minicart HTML
  document.write("<tr>");
  document.write("<td colspan=\"2\" align=\"right\" bgcolor=\"#FFFFFF\"><b>Subtotal</b></td>");
  document.write("<td align=\"right\" bgcolor=\"#FFFFFF\"><b>");
  tmp = cartvalues.substring(start1,end1);
  colon = tmp.indexOf(":", 0);
  document.write(tmp.substring(colon+1,end1 - start1));
  document.write("</b></td>");
  document.write("</tr>");
}
  document.write("<tr>");
  document.write("<td align=\"right\" colspan=\"3\" bgcolor=\"#FFFFFF\"><a class=\"mini\" href=\"[-- SHOPPING_CART_URL --]\" rel=\"nofollow\">Continue to [-- STORE.SC_YourShoppingCart --]</a></td>");
  document.write("</tr>");

  document.write("</table>\n");

</script>
[-- ELSE --]
     <p class="boxedin" align="right">
<a class="mini" href="[-- SHOPPING_CART_URL --]" rel="nofollow">Continue to [-- STORE.SC_YourShoppingCart --]</a>
[-- IF ShopSiteSecurityImage --]<br><center>[-- ShopSiteSecurityImage --]</center>[-- END_IF --]
      </p>
[-- END_IF --]
