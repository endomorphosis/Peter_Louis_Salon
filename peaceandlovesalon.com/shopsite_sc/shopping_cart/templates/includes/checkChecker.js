<script language="JavaScript" type="text/javascript">
/* Stunk's Mighty Check Checker Thingy */

/* Copyright (c) 2008, Stunkworks
Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE. */

function checkChecker (thisForm) {
  // Change the warning text to suit your preference.
  var myWarning = 'Please select a product to purchase.';
  
  var myForm = document.getElementById(thisForm);
  var gotCheck = false;
  var isChecked = false;

  for (i=0; i < myForm.length; i++) {
    if (myForm[i].type == "checkbox") {
      gotCheck = true;
      if (myForm[i].checked) {
        isChecked = true;
      }
    }
  }
  if (gotCheck == true) {
    if (isChecked == true) {
      return true;
    }
    else {
      alert(myWarning);
      return false;
    }
  }
  else {
    return true;
  }
}

</script>
