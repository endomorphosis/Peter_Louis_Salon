<script language="JavaScript" type="text/javascript">
  function DisplayLogName(name) {
    var cookies=document.cookie;
    var start = cookies.indexOf(name + "=");
    var name = "";
    var start1;
    var end1;
    var tmp;
    var signed_in = -1;

    if (start != -1) {
      start = cookies.indexOf("=", start) +1;
      var end = cookies.indexOf("|", start);
      if (end != -1) {
        signed_in = cookies.indexOf("|yes", start);
        name = unescape(cookies.substring(start,end-1));
        if (signed_in != -1) {
/* Links When Signed In */
          document.write("<span class=\"regtext\">" + name + "<\/span>");
          document.write("<br>");
[-- IF ANALYTICS_MULTI_DOMAIN --]
          document.write("<a class=\"reglink\" href=\"javascript:__utmLinker(\'[-- CR_View_Edit_URL --]\');\">[-- STORE.ViewEdit --]<\/a>");
[-- ELSE --]
          document.write("<a class=\"reglink\" href=\"[-- CR_View_Edit_URL --]\">[-- STORE.ViewEdit --]<\/a>");
[-- END_IF --]
          document.write("<br>");
[-- IF ANALYTICS_MULTI_DOMAIN --]
          document.write("<a class=\"reglink\" href=\"javascript:__utmLinker(\'[-- CR_Sign_Out_URL --]\');\">[-- STORE.SignOut --]<\/a>");
[-- ELSE --]
          document.write("<a class=\"reglink\" href=\"[-- CR_Sign_Out_URL --]\">[-- STORE.SignOut --]<\/a>");
[-- END_IF --]

        }
        else {
/* Links When Signed Out */
          document.write("<span class=\"regtext\">[-- STORE.280 --]<\/span>");
          document.write("<br>");
        }
      }
    }
    if (signed_in == -1) {
/* Links When NOT Signed In */
[-- IF ANALYTICS_MULTI_DOMAIN --]
      document.write("<a class=\"reglink\" href=\"javascript:__utmLinker(\'[-- CR_Sign_In_URL --]\');\">[-- STORE.ToSignIn --]<\/a>");
[-- ELSE --]
      document.write("<a class=\"reglink\" href=\"[-- CR_Sign_In_URL --]\">[-- STORE.ToSignIn --]<\/a>");
[-- END_IF --]
      document.write("<br>");
[-- IF ANALYTICS_MULTI_DOMAIN --]
      document.write("<a class=\"reglink\" href=\"javascript:__utmLinker(\'[-- CR_Register_URL --]\');\">[-- STORE.ToRegister --]<\/a>");
[-- ELSE --]
      document.write("<a class=\"reglink\" href=\"[-- CR_Register_URL --]\">[-- STORE.ToRegister --]<\/a>");
[-- END_IF --]
      //document.write("<br>");
    }
  }
</script>
