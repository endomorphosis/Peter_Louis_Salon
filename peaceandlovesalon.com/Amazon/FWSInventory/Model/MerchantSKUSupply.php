<?php
/** 
 *  PHP Version 5
 *
 *  @category    Amazon
 *  @package     Amazon_FWSInventory
 *  @copyright   Copyright 2008 Amazon Technologies, Inc.
 *  @link        http://aws.amazon.com
 *  @license     http://aws.amazon.com/apache2.0  Apache License, Version 2.0
 *  @version     2009-07-31
 */
/******************************************************************************* 
 *    __  _    _  ___ 
 *   (  )( \/\/ )/ __)
 *   /__\ \    / \__ \
 *  (_)(_) \/\/  (___/
 * 
 *  Amazon FWS Inventory PHP5 Library
 *  Generated: Fri Nov 20 09:59:57 PST 2009
 * 
 */

/**
 *  @see Amazon_FWSInventory_Model
 */
require_once ('Amazon/FWSInventory/Model.php');  

    

/**
 * Amazon_FWSInventory_Model_MerchantSKUSupply
 * 
 * Properties:
 * <ul>
 * 
 * <li>MerchantSKU: string</li>
 * <li>FulfillmentNetworkSKU: string</li>
 * <li>ASIN: string</li>
 * <li>Condition: string</li>
 * <li>TotalSupplyQuantity: int</li>
 * <li>InStockSupplyQuantity: int</li>
 * <li>EarliestAvailability: Amazon_FWSInventory_Model_Timepoint</li>
 * <li>SupplyDetail: Amazon_FWSInventory_Model_SupplyDetail</li>
 *
 * </ul>
 */ 
class Amazon_FWSInventory_Model_MerchantSKUSupply extends Amazon_FWSInventory_Model
{


    /**
     * Construct new Amazon_FWSInventory_Model_MerchantSKUSupply
     * 
     * @param mixed $data DOMElement or Associative Array to construct from. 
     * 
     * Valid properties:
     * <ul>
     * 
     * <li>MerchantSKU: string</li>
     * <li>FulfillmentNetworkSKU: string</li>
     * <li>ASIN: string</li>
     * <li>Condition: string</li>
     * <li>TotalSupplyQuantity: int</li>
     * <li>InStockSupplyQuantity: int</li>
     * <li>EarliestAvailability: Amazon_FWSInventory_Model_Timepoint</li>
     * <li>SupplyDetail: Amazon_FWSInventory_Model_SupplyDetail</li>
     *
     * </ul>
     */
    public function __construct($data = null)
    {
        $this->_fields = array (
        'MerchantSKU' => array('FieldValue' => null, 'FieldType' => 'string'),
        'FulfillmentNetworkSKU' => array('FieldValue' => null, 'FieldType' => 'string'),
        'ASIN' => array('FieldValue' => null, 'FieldType' => 'string'),
        'Condition' => array('FieldValue' => null, 'FieldType' => 'string'),
        'TotalSupplyQuantity' => array('FieldValue' => null, 'FieldType' => 'int'),
        'InStockSupplyQuantity' => array('FieldValue' => null, 'FieldType' => 'int'),
        'EarliestAvailability' => array('FieldValue' => null, 'FieldType' => 'Amazon_FWSInventory_Model_Timepoint'),
        'SupplyDetail' => array('FieldValue' => array(), 'FieldType' => array('Amazon_FWSInventory_Model_SupplyDetail')),
        );
        parent::__construct($data);
    }

        /**
     * Gets the value of the MerchantSKU property.
     * 
     * @return string MerchantSKU
     */
    public function getMerchantSKU() 
    {
        return $this->_fields['MerchantSKU']['FieldValue'];
    }

    /**
     * Sets the value of the MerchantSKU property.
     * 
     * @param string MerchantSKU
     * @return this instance
     */
    public function setMerchantSKU($value) 
    {
        $this->_fields['MerchantSKU']['FieldValue'] = $value;
        return $this;
    }

    /**
     * Sets the value of the MerchantSKU and returns this instance
     * 
     * @param string $value MerchantSKU
     * @return Amazon_FWSInventory_Model_MerchantSKUSupply instance
     */
    public function withMerchantSKU($value)
    {
        $this->setMerchantSKU($value);
        return $this;
    }


    /**
     * Checks if MerchantSKU is set
     * 
     * @return bool true if MerchantSKU  is set
     */
    public function isSetMerchantSKU()
    {
        return !is_null($this->_fields['MerchantSKU']['FieldValue']);
    }

    /**
     * Gets the value of the FulfillmentNetworkSKU property.
     * 
     * @return string FulfillmentNetworkSKU
     */
    public function getFulfillmentNetworkSKU() 
    {
        return $this->_fields['FulfillmentNetworkSKU']['FieldValue'];
    }

    /**
     * Sets the value of the FulfillmentNetworkSKU property.
     * 
     * @param string FulfillmentNetworkSKU
     * @return this instance
     */
    public function setFulfillmentNetworkSKU($value) 
    {
        $this->_fields['FulfillmentNetworkSKU']['FieldValue'] = $value;
        return $this;
    }

    /**
     * Sets the value of the FulfillmentNetworkSKU and returns this instance
     * 
     * @param string $value FulfillmentNetworkSKU
     * @return Amazon_FWSInventory_Model_MerchantSKUSupply instance
     */
    public function withFulfillmentNetworkSKU($value)
    {
        $this->setFulfillmentNetworkSKU($value);
        return $this;
    }


    /**
     * Checks if FulfillmentNetworkSKU is set
     * 
     * @return bool true if FulfillmentNetworkSKU  is set
     */
    public function isSetFulfillmentNetworkSKU()
    {
        return !is_null($this->_fields['FulfillmentNetworkSKU']['FieldValue']);
    }

    /**
     * Gets the value of the ASIN property.
     * 
     * @return string ASIN
     */
    public function getASIN() 
    {
        return $this->_fields['ASIN']['FieldValue'];
    }

    /**
     * Sets the value of the ASIN property.
     * 
     * @param string ASIN
     * @return this instance
     */
    public function setASIN($value) 
    {
        $this->_fields['ASIN']['FieldValue'] = $value;
        return $this;
    }

    /**
     * Sets the value of the ASIN and returns this instance
     * 
     * @param string $value ASIN
     * @return Amazon_FWSInventory_Model_MerchantSKUSupply instance
     */
    public function withASIN($value)
    {
        $this->setASIN($value);
        return $this;
    }


    /**
     * Checks if ASIN is set
     * 
     * @return bool true if ASIN  is set
     */
    public function isSetASIN()
    {
        return !is_null($this->_fields['ASIN']['FieldValue']);
    }

    /**
     * Gets the value of the Condition property.
     * 
     * @return string Condition
     */
    public function getCondition() 
    {
        return $this->_fields['Condition']['FieldValue'];
    }

    /**
     * Sets the value of the Condition property.
     * 
     * @param string Condition
     * @return this instance
     */
    public function setCondition($value) 
    {
        $this->_fields['Condition']['FieldValue'] = $value;
        return $this;
    }

    /**
     * Sets the value of the Condition and returns this instance
     * 
     * @param string $value Condition
     * @return Amazon_FWSInventory_Model_MerchantSKUSupply instance
     */
    public function withCondition($value)
    {
        $this->setCondition($value);
        return $this;
    }


    /**
     * Checks if Condition is set
     * 
     * @return bool true if Condition  is set
     */
    public function isSetCondition()
    {
        return !is_null($this->_fields['Condition']['FieldValue']);
    }

    /**
     * Gets the value of the TotalSupplyQuantity property.
     * 
     * @return int TotalSupplyQuantity
     */
    public function getTotalSupplyQuantity() 
    {
        return $this->_fields['TotalSupplyQuantity']['FieldValue'];
    }

    /**
     * Sets the value of the TotalSupplyQuantity property.
     * 
     * @param int TotalSupplyQuantity
     * @return this instance
     */
    public function setTotalSupplyQuantity($value) 
    {
        $this->_fields['TotalSupplyQuantity']['FieldValue'] = $value;
        return $this;
    }

    /**
     * Sets the value of the TotalSupplyQuantity and returns this instance
     * 
     * @param int $value TotalSupplyQuantity
     * @return Amazon_FWSInventory_Model_MerchantSKUSupply instance
     */
    public function withTotalSupplyQuantity($value)
    {
        $this->setTotalSupplyQuantity($value);
        return $this;
    }


    /**
     * Checks if TotalSupplyQuantity is set
     * 
     * @return bool true if TotalSupplyQuantity  is set
     */
    public function isSetTotalSupplyQuantity()
    {
        return !is_null($this->_fields['TotalSupplyQuantity']['FieldValue']);
    }

    /**
     * Gets the value of the InStockSupplyQuantity property.
     * 
     * @return int InStockSupplyQuantity
     */
    public function getInStockSupplyQuantity() 
    {
        return $this->_fields['InStockSupplyQuantity']['FieldValue'];
    }

    /**
     * Sets the value of the InStockSupplyQuantity property.
     * 
     * @param int InStockSupplyQuantity
     * @return this instance
     */
    public function setInStockSupplyQuantity($value) 
    {
        $this->_fields['InStockSupplyQuantity']['FieldValue'] = $value;
        return $this;
    }

    /**
     * Sets the value of the InStockSupplyQuantity and returns this instance
     * 
     * @param int $value InStockSupplyQuantity
     * @return Amazon_FWSInventory_Model_MerchantSKUSupply instance
     */
    public function withInStockSupplyQuantity($value)
    {
        $this->setInStockSupplyQuantity($value);
        return $this;
    }


    /**
     * Checks if InStockSupplyQuantity is set
     * 
     * @return bool true if InStockSupplyQuantity  is set
     */
    public function isSetInStockSupplyQuantity()
    {
        return !is_null($this->_fields['InStockSupplyQuantity']['FieldValue']);
    }

    /**
     * Gets the value of the EarliestAvailability.
     * 
     * @return Timepoint EarliestAvailability
     */
    public function getEarliestAvailability() 
    {
        return $this->_fields['EarliestAvailability']['FieldValue'];
    }

    /**
     * Sets the value of the EarliestAvailability.
     * 
     * @param Timepoint EarliestAvailability
     * @return void
     */
    public function setEarliestAvailability($value) 
    {
        $this->_fields['EarliestAvailability']['FieldValue'] = $value;
        return;
    }

    /**
     * Sets the value of the EarliestAvailability  and returns this instance
     * 
     * @param Timepoint $value EarliestAvailability
     * @return Amazon_FWSInventory_Model_MerchantSKUSupply instance
     */
    public function withEarliestAvailability($value)
    {
        $this->setEarliestAvailability($value);
        return $this;
    }


    /**
     * Checks if EarliestAvailability  is set
     * 
     * @return bool true if EarliestAvailability property is set
     */
    public function isSetEarliestAvailability()
    {
        return !is_null($this->_fields['EarliestAvailability']['FieldValue']);

    }

    /**
     * Gets the value of the SupplyDetail.
     * 
     * @return array of SupplyDetail SupplyDetail
     */
    public function getSupplyDetail() 
    {
        return $this->_fields['SupplyDetail']['FieldValue'];
    }

    /**
     * Sets the value of the SupplyDetail.
     * 
     * @param mixed SupplyDetail or an array of SupplyDetail SupplyDetail
     * @return this instance
     */
    public function setSupplyDetail($supplyDetail) 
    {
        if (!$this->_isNumericArray($supplyDetail)) {
            $supplyDetail =  array ($supplyDetail);    
        }
        $this->_fields['SupplyDetail']['FieldValue'] = $supplyDetail;
        return $this;
    }


    /**
     * Sets single or multiple values of SupplyDetail list via variable number of arguments. 
     * For example, to set the list with two elements, simply pass two values as arguments to this function
     * <code>withSupplyDetail($supplyDetail1, $supplyDetail2)</code>
     * 
     * @param SupplyDetail  $supplyDetailArgs one or more SupplyDetail
     * @return Amazon_FWSInventory_Model_MerchantSKUSupply  instance
     */
    public function withSupplyDetail($supplyDetailArgs)
    {
        foreach (func_get_args() as $supplyDetail) {
            $this->_fields['SupplyDetail']['FieldValue'][] = $supplyDetail;
        }
        return $this;
    }   



    /**
     * Checks if SupplyDetail list is non-empty
     * 
     * @return bool true if SupplyDetail list is non-empty
     */
    public function isSetSupplyDetail()
    {
        return count ($this->_fields['SupplyDetail']['FieldValue']) > 0;
    }




}