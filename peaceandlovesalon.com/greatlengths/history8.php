<table id="table2" border="0" cellpadding="5" cellspacing="0" width="840">
<tbody>
<tr>
<td colspan="2">
<table id="table1" cellspacing="0" cellpadding="4" width="100%" border="0"> 
<tbody> 
<tr valign="top"> 
<td colspan="2">
			<?php include'sub-menue.php';?>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>

<table id="table3" wdith="100%" bgcolor="#e9e9e9" border="0" cellpadding="2" cellspacing="2">
<tbody>
<tr>
<td colspan="2" bgcolor="#003366">
<center>
    <font class="bodytext-alt"><b><font color="#FFFFFF">The Method</font></b>      </font>
</center>
</td>
</tr>
<tr>
<td>
<font class="bodytext"></font>
<p align="justify"><font class="bodytext" style="font-size: 9pt;" face="Verdana">Traditionally in hair salons or hair processing factories, bleaching substances are used to break up the pigment molecules (felamelamine) in the hair and thus lighten its colour. However, it is well known that the substances used (peroxide or ammonium derivatives) are also extremely harmful to the surrounding hair structure. The lighter the bleaching, the more damage to the structure of the hair and the more elastic it will become. </font></p>
<p align="justify"><font style="font-size: 9pt;" face="Verdana"><font class="bodytext"><b>But there is an incredible, although lengthy process, which may be used to lighten the hair whilst keeping its structure intact and healthy and without any damage to its cuticle.</b></font> </font></p>
<p align="justify"><font style="font-size: 9pt;" face="Verdana"><font class="bodytext">Scientists in the wool and cashmere industry adopt a sophisticated method of removing the pigments in wool fibres rather than breaking them up by bleaching them. Have you ever wondered how wool can become so white and yet maintain its quality and strength?<br>We have all seen beautiful white or pastel colour cashmere pullovers and yet the only snow white sheep exist in cartoons or in children?s storybooks...!</font> </font></p>

<p>&nbsp;</p>
<center><font style="font-size: 9pt;" face="Verdana"><img alt="" src="greatlengths/media/pecore.jpg" border="2" height="238" width="351"> </font></center>
<p align="justify"><font style="font-size: 9pt;" face="Verdana"><font class="bodytext">David Gold, before founding Great Lengths, had connections with this specialised textile sector. He carried out further development work on this scientific wool ?depigmentising? process which he had access to.</font> </font></p>
<p align="justify"><font style="font-size: 9pt;" face="Verdana"><font class="bodytext"><b>The carefully guarded secret is an osmosis process which, as a result of Gold?s research, has now been adapted to work on human hair; the composition of which is very similar to that of wool. The colour pigments are released slowly and carefully from the hair during a process lasting up to 15 days.</b></font> </font></p>
<p align="justify"><font style="font-size: 9pt;" face="Verdana"><font class="bodytext"><b>Never before has this method been utilised for human hair processing and, coupled with the exceptional integrity of the Indian ?temple hair?, it truly provides a reliable and unsurpassed hair quality ? so good that this hair will withstand the most rigorous of lifestyles including subsequent chemical treatments such as perming if so desired</b>.</font> </font></p>
<p>&nbsp;</p>
</td>
</tr>
<tr>
<td height="50" valign="top" bordercolor="#000080" bgcolor="#99ccff">
<p align="justify"><font class="bodytext" style="font-size: 9pt;" face="Verdana">Colour fastness is also a major achievement as a result of this new process. Similar to the color fastness obtained in the cashmere industry, new color molecules inserted into fibers previously ?de-pigmentised? in this manner find a proper space to occupy rather than having to try to squeeze into a space occupied by fragments of broken "bleached" molecules.</font></p></td></tr>
<tr>
<td height="60" valign="bottom">
<p align="justify"><font class="bodytext" style="font-size: 9pt;" face="Verdana">We hope that the information provided will shed light on some of the mystery surrounding the booming world of hair extensions. <font color="#000080">A correct choice will make an enormous difference between an unhappy or an extremely satisfying experience....</font></font></p>
</td>
</tr>
<tr>
<td align="right">
<font style="font-size: 9pt;" face="Verdana"><a href="template.php?f=history7"><img alt="previous" src="greatlengths/media/prev.gif" border="0" height="38" width="46"></a></font>
</td>
</tr>
</tbody>
</table>


<table id="table2" border="0" cellpadding="5" cellspacing="0" width="840">
<tr>
<td>
<table id="table1" cellspacing="0" cellpadding="4" width="100%" border="0"> 
<tbody> 
<tr valign="top"> 
<td colspan="2">
			<?php include'sub-menue.php';?>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</table>