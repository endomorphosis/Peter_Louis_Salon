<table cellspacing="0" cellpadding="0" width="839" border="0"> 
<tbody> 
<tr> 
<td> 
<table id="table1" cellspacing="0" cellpadding="4" width="100%" border="0"> 
<tbody> 
<tr valign="top"> 
<td colspan="2">
			<?php include'sub-menue.php';?>
</td>
</tr>
</tbody>
</table>
</td>
</tr> 
<tr> 
<td>&nbsp;</td></tr> 
<tr> 
<td> 


<table id="table2" align="center" border="0" cellpadding="2" cellspacing="0" width="100%">
<tbody>
<tr>
<td rowspan="5">
  <ul>
    <li><font class="bodytext">Hair extension</font> </li>
    <li><font class="bodytext">Hair thickening to achieve more volume</font> </li>
    <li><font class="bodytext">Highlight, lowlight effects</font> </li>
    <li><font class="bodytext">Being creative with crazy colours</font> </li>
    <li><font class="bodytext">More than 40 different and ever increasing hair colours</font> </li>
    <li><font class="bodytext">4 standard lengths: 30 cm,<br>40 cm, 50 cm, 60 cm</font> </li>
    <li><font class="bodytext">3 types of strands of different thickness: standard, fine, extrafine</font> </li>
    <li><font class="bodytext">8 "crazy" strands: blue, green, yellow, red, pink, orange, violet, apricot</font> </li>
    <li><font class="bodytext">strands with white diamonds in black, brown, red and blond hair as well as blue diamonds in blond hair</font> </li>
  </ul>
</td>
<td align="right" width="267">
<font class="bodytext">
<a href="template.php?f=methodds5">
<img alt="" src="greatlengths/media/next.gif" border="0" height="38" width="46">
</a>
</font>
</td>
</tr>
<tr>
<td width="267">
<img alt="" src="greatlengths/media/capelli4.jpg" border="0" height="227" width="267">
</td>
</tr>
<tr>
<td align="middle" width="267">
<font class="small">
With Great Lengths hair extensions you can perm the strands to make them as curly as you wish.
</font>
</td>
</tr>
<tr>
<td width="267">
<img alt="" src="greatlengths/media/capelli5.jpg" border="0" height="167" width="267">
</td>
</tr>
<tr>
<td align="middle" width="267">
<font class="small">The matching thickness of strand is selected according to hair type and length!</font>
</td>
</tr>
<tr>
<td colspan="2">
<img alt="" src="greatlengths/media/apparecchio.jpg" border="0" height="243" width="319">
</td>
</tr>
<tr> 
<td valign="top" colspan="3">
<font class="bodytext">
  <b>
  <a href="template.php?f=methodds3">
	  <img alt="" src="greatlengths/media/fprev.gif" align="absbottom" border="0" height="20" width="20"> 
  	back
  </a> 
  -
  Page:
  <a href="template.php?f=methodds1">
  	1
  </a>
  | 
  <a href="template.php?f=methodds2">
  	2
  </a>
  |
  <a href="template.php?f=methodds3">
  	3
  </a> 
  | 
  	4
  | 
  <a href="template.php?f=methodds5">
  	5
  </a> 
  - 
  <a href="template.php?f=methodds5">
  	next
  	<img height="20" alt="" src="greatlengths/media/fnext.gif" width="20" align="absBottom" border="0" />
  </a>
  </b>
</font> 
</td>
</tr>
</tbody>

</table

></td>
</tr> 
<tr> 
<td>
			<?php include'sub-menue.php';?>
</td>
</tr> 
<tr> 
<td>&nbsp;</td>
</tr>
</tbody>
</table>