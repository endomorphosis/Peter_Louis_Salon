<table id="table2" border="0" cellpadding="5" cellspacing="0" width="840">
<tbody>
<tr>
<td colspan="2">
<table id="table1" cellspacing="0" cellpadding="4" width="100%" border="0"> 
<tbody> 
<tr valign="top"> 
<td colspan="2">
			<?php include'sub-menue.php';?>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>

<table id="table2" wdith="100%" bgcolor="#e9e9e9" border="0" cellpadding="2" cellspacing="2">
<tbody>
<tr>
<td colspan="2" bgcolor="#003366">
<center>
    <font class="bodytext-alt"><b><font color="#FFFFFF">Chinese hair</font></b>      </font>
</center>
</td>
</tr>
<tr>
<td colspan="2">
  <font style="font-size: 9pt;" face="Verdana"><font class="bodytext"><br><b>Chinese hair is very often used for hair extensions, but this does not mean it is necessarily very suitable for the job.</b></font> </font>
  <p>&nbsp;</p>
</td>
</tr>

<tr>
  <td>
      <font style="font-size: 9pt;" face="Verdana"><img alt="" src="greatlengths/media/cinese.jpg" border="2" height="373" width="250"></font>
  </td>
  <td>
      <p align="justify"><font style="font-size: 9pt;" face="Verdana"><font class="bodytext">Chinese hair is "considerably thicker" than European hair; it is dark black and, by contrast with European hair, has a round cross-section. To prepare this hair for use in hair extensions, it must first be made thinner.</font> </font></p>
      <p align="justify"><font style="font-size: 9pt;" face="Verdana"><img alt="" src="greatlengths/media/point.gif" border="0" height="22" width="20"><font class="bodytext">To make the hair thinner, it is placed in an acid bath. <b>This almost completely destroys the cuticle making the hair brittle and dull.</b></font><br>&nbsp;</font></p>
      <p align="justify"><font style="font-size: 9pt;" face="Verdana"><img alt="" src="greatlengths/media/point.gif" border="0" height="22" width="20"><font class="bodytext"><b>The shine must be restored to the hair. </b>So it is coated with a silicon layer.</font><br>&nbsp;</font></p>
      <p align="justify"><font style="font-size: 9pt;" face="Verdana"><img alt="" src="greatlengths/media/point.gif" border="0" height="22" width="20"><font class="bodytext">This gives the hair an impressive sheen. <b>But not for long, because after a few washes, the silicon layer is removed and the hair becomes dull and brittle again.</b></font><br>&nbsp;</font></p>
  </td>
</tr>
<tr>
  <td colspan="2">
      <p align="justify"><font style="font-size: 9pt;" face="Verdana"><font class="bodytext">Chinese hair is suitable for use <b>in wigs and hair pieces</b> because wigs and hair pieces are generaly cleaned with dry shampoo, so the silicon layer re-mains intact and the hair keeps its lovely sheen.</font> </font></p>
      <p align="justify"><font face="Verdana"><font class="bodytext"><b><span style="font-size: 9pt;">Chinese hair is very suitable for wigs and hair pieces,<br></span><font style="font-size: 9pt;" color="#ff0000">but for European women it is certainly not the best solution!</font></b></font><span style="font-size: 9pt;"> </span></font></p>
  </td>
</tr>
<tr>
<td>
	<font style="font-size: 9pt;" face="Verdana"><img alt="" src="greatlengths/media/timbro1.gif" align="right" border="0" height="77" width="80"> </font>
</td>
<td align="right">
	<font style="font-size: 9pt;" face="Verdana"><a href="template.php?f=history4"><img alt="previous" src="greatlengths/media/prev.gif" border="0" height="38" width="46"></a>&nbsp;<a href="template.php?f=history6"><img alt="next" src="greatlengths/media/next.gif" border="0" height="38" width="46"></a> </font>
</td>
</tr>
</tbody>
</table>



<table id="table2" border="0" cellpadding="5" cellspacing="0" width="840">
<tr>
<td>
<table id="table1" cellspacing="0" cellpadding="4" width="100%" border="0"> 
<tbody> 
<tr valign="top"> 
<td colspan="2">
			<?php include'sub-menue.php';?>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</table>