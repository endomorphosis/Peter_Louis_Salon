<table cellspacing="0" cellpadding="0" width="839" border="0"> 
<tbody> 
<tr> 
<td> 
<table id="table1" cellspacing="0" cellpadding="4" width="100%" border="0"> 
<tbody> 
<tr valign="top"> 
<td colspan="2">
			<?php include'sub-menue.php';?>
</td>
</tr>
</tbody>
</table>
</td>
</tr> 
<tr> 
<td>&nbsp;</td></tr> 
<tr> 
<td> 

<table id="table2" align="center" border="0" cellpadding="2" cellspacing="2" width="100%">
<tbody>
<tr>
<td rowspan="3" valign="top">
<font class="bodytext">
Our wide range of Great Lengths strands of various lengths, thicknesses and colours, including "crazy" strands and strands with diamonds, theoretically offers you a large variety of possibilities out of which you can select your new superb head of hair!
</font>
</td>
<td align="right">
<font class="bodytext">
<a href="template.php?f=methodds4">
<img alt="" src="greatlengths/media/next.gif" border="0" height="38" width="46">
</a>
</font>
</td>
</tr>
<tr>
<td>
<img alt="" src="greatlengths/media/capelli1.jpg" border="0" height="252" width="302">
</td>
</tr>
<tr>
<td>
<font class="small">Blond, red, brown and black: a total of over 40 different colours and shades group round these 4 basic hair colours!
</font>
</td>
</tr>
<tr>

<td>
<img alt="" src="greatlengths/media/capelli3.jpg" border="0" height="147" width="148">
</td>
<td>
<img alt="" src="greatlengths/media/capelli2.jpg" border="0" height="148" width="302">
</td>
</tr>
<tr>
<td valign="top">
<font class="small">
Single "crazy" strands are quickly applied - use this opportunity to continually achieve new effects!
</font>
</td>
<td align="middle" valign="top">
<font class="small">
For our diamond strands we exclusively use Swarovski crystals!
</font>
</td>
</tr>
</tbody>
</table>
<tr> 
<td valign="top" colspan="3">
<font class="bodytext">
  <b>
  <a href="template.php?f=methodds2">
	  <img alt="" src="greatlengths/media/fprev.gif" align="absbottom" border="0" height="20" width="20"> 
  	back
  </a> 
  -
  Page:
  <a href="template.php?f=methodds1">
  	1
  </a>
  | 
  <a href="template.php?f=methodds2">
  	2
  </a>
  |
  	3
  | 
  <a href="template.php?f=methodds4">
  	4
  </a> 
  | 
  <a href="template.php?f=methodds5">
  	5
  </a> 
  - 
  <a href="template.php?f=methodds4">
  	next
  	<img height="20" alt="" src="greatlengths/media/fnext.gif" width="20" align="absBottom" border="0" />
  </a>
  </b>
</font> 
</td>
</tr>
</td>
</tr> 

<tr> 
<td>
			<?php include'sub-menue.php';?>
</td>
</tr> 
<tr> 
<td>&nbsp;</td>
</tr>
</tbody>
</table>