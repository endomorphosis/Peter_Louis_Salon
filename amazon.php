<?php
/** 
 *  PHP Version 5
 *
 *  @category    Amazon
 *  @package     Amazon_FWSOutbound
 *  @copyright   Copyright 2007 Amazon Technologies, Inc.
 *  @link        http://aws.amazon.com
 *  @license     http://aws.amazon.com/apache2.0  Apache License, Version 2.0
 *  @version     2007-08-02
 */
/******************************************************************************* 
 *    __  _    _  ___ 
 *   (  )( \/\/ )/ __)
 *   /__\ \    / \__ \
 *  (_)(_) \/\/  (___/
 * 
 *  Amazon FWS Outbound PHP5 Library
 *  Generated: Wed Mar 19 13:03:03 PDT 2008
 * 
 */

/**
 * Create Fulfillment Order  Sample
 */

include_once ('/home/peterlou/public_html/Amazon/FWSOutbound/Samples/.config.inc.php'); 

/************************************************************************
 * Instantiate Implementation of Amazon FWSOutbound
 * 
 * AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY constants 
 * are defined in the .config.inc.php located in the same 
 * directory as this sample
 ***********************************************************************/
 $service = new Amazon_FWSOutbound_Client(AWS_ACCESS_KEY_ID, 
                                       AWS_SECRET_ACCESS_KEY);
 
/************************************************************************
 * Uncomment to try out Mock Service that simulates Amazon_FWSOutbound
 * responses without calling Amazon_FWSOutbound service.
 *
 * Responses are loaded from local XML files. You can tweak XML files to
 * experiment with various outputs during development
 *
 * XML files available under Amazon/FWSOutbound/Mock tree
 *
 ***********************************************************************/
 // $service = new Amazon_FWSOutbound_Mock();

/************************************************************************
 * Setup request parameters and uncomment invoke to try out
 * sample for Create Fulfillment Order Action
 ***********************************************************************/
 // @TODO: set request. Action can be passed as Amazon_FWSOutbound_Model_CreateFulfillmentOrder
 // object or array of parameters
	function state_to_twoletter( $state_name ) {
		
		$state = array();
		$state['ALABAMA']='AL';
		$state['ALASKA']='AK';
		$state['AMERICAN SAMOA']='AS';
		$state['ARIZONA']='AZ';
		$state['ARKANSAS']='AR';
		$state['CALIFORNIA']='CA';
		$state['COLORADO']='CO';
		$state['CONNECTICUT']='CT';
		$state['DELAWARE']='DE';
		$state['DISTRICT OF COLUMBIA']='DC';
		$state['FEDERATED STATES OF MICRONESIA']='FM';
		$state['FLORIDA']='FL';
		$state['GEORGIA']='GA';
		$state['GUAM']='GU';
		$state['HAWAII']='HI';
		$state['IDAHO']='ID';
		$state['ILLINOIS']='IL';
		$state['INDIANA']='IN';
		$state['IOWA']='IA';
		$state['KANSAS']='KS';
		$state['KENTUCKY']='KY';
		$state['LOUISIANA']='LA';
		$state['MAINE']='ME';
		$state['MARSHALL ISLANDS']='MH';
		$state['MARYLAND']='MD';
		$state['MASSACHUSETTS']='MA';
		$state['MICHIGAN']='MI';
		$state['MINNESOTA']='MN';
		$state['MISSISSIPPI']='MS';
		$state['MISSOURI']='MO';
		$state['MONTANA']='MT';
		$state['NEBRASKA']='NE';
		$state['NEVADA']='NV';
		$state['NEW HAMPSHIRE']='NH';
		$state['NEW JERSEY']='NJ';
		$state['NEW MEXICO']='NM';
		$state['NEW YORK']='NY';
		$state['NORTH CAROLINA']='NC';
		$state['NORTH DAKOTA']='ND';
		$state['NORTHERN MARIANA ISLANDS']='MP';
		$state['OHIO']='OH';
		$state['OKLAHOMA']='OK';
		$state['OREGON']='OR';
		$state['PALAU']='PW';
		$state['PENNSYLVANIA']='PA';
		$state['PUERTO RICO']='PR';
		$state['RHODE ISLAND']='RI';
		$state['SOUTH CAROLINA']='SC';
		$state['SOUTH DAKOTA']='SD';
		$state['TENNESSEE']='TN';
		$state['TEXAS']='TX';
		$state['UTAH']='UT';
		$state['VERMONT']='VT';
		$state['VIRGIN ISLANDS']='VI';
		$state['VIRGINIA']='VA';
		$state['WASHINGTON']='WA';
		$state['WEST VIRGINIA']='WV';
		$state['WISCONSIN']='WI';
		$state['WYOMING']='WY';

		// Canadian Provinces
		// edited 12-5-07
		$state['ALBERTA']='AB';
		$state['BRITISH COLUMBIA']='BC';
		$state['MANITOBA']='MB';
		$state['NEW BRUNSWICK']='NB';
		$state['LABRADOR']='NL';
		$state['NEWFOUNDLAND']='NL';
		$state['NORTHWEST TERRITORIES']='NT';
		$state['NOVA SCOTIA']='NS';
		$state['NUNAVUT']='NU';
		$state['ONTARIO']='ON';
		$state['PRINCE EDWARD ISLAND']='PE';
		$state['QUEBEC']='QC';
		$state['SASKATCHEWAN']='SK';
		$state['YUKON']='YT';

		return $state[strtoupper( $state_name )]; 
		
	}



//$data = 'a:11:{s:4:"name";s:15:"benjamin barber";s:8:"address1";s:20:"3338 sw kelly ave #3";s:8:"address2";s:0:"";s:4:"city";s:8:"portland";s:5:"state";s:6:"oregon";s:7:"country";s:2:"US";s:7:"zipcode";s:5:"97201";s:5:"phone";s:12:"555-555-5555";s:7:"orderid";s:2:"87";s:9:"timestamp";s:20:"2009-16-10T04:53:36Z";s:5:"items";a:2:{i:0;a:3:{s:3:"sku";s:12:"AX-J39E-WNGX";s:6:"itemid";i:45;s:3:"qty";s:1:"1";}i:1;a:3:{s:3:"sku";s:5:"90042";s:6:"itemid";i:263;s:3:"qty";s:1:"1";}}}';

//$data = unserialize($data);
$_REQUEST['data'] = str_replace("\\", "", $_REQUEST["data"]);
$data = unserialize($_REQUEST['data']);
print_r($_REQUEST);
// Create Request

//Create Address
$address = new Amazon_FWSOutbound_Model_Address();
$address-> setName($data['name']);
$address-> setLine1($data['address1']);
$address-> setLine2($data['address2']);
$address-> setCity($data['city']);
$address-> setStateOrProvinceCode(state_to_twoletter($data['state']));
$address-> setCountryCode($data['country']);
$address-> setPostalCode($data['zipcode']);
$address-> setPhoneNumber($data['phone']);

//Create item
foreach($data['items'] as $items){
$request = new Amazon_FWSOutbound_Model_CreateFulfillmentOrder();
$item = new Amazon_FWSOutbound_Model_CreateFulfillmentOrderItem();
echo '<br/><br/>';
print_r($items);
//Set item parameters
$item-> setMerchantSKU($items['sku']);
$item-> setMerchantFulfillmentOrderItemId($items['itemid']);
$item-> setQuantity($items['qty']);
$request-> setItem($item);
$request-> setDestinationAddress($address);
$request-> setMerchantFulfillmentOrderId($data['orderid']."-".$items['itemid']);
$request-> setDisplayableOrderId($data['orderid']."-".$items['itemid']);
$request-> setDisplayableOrderDateTime($data['timestamp']);
$request-> setDisplayableOrderComment('Thank you for your order!');
$request-> setShippingSpeedCategory('Standard');


invokeCreateFulfillmentOrder($service, $request);

}

//Create Request


//Set Request Parameters

/**
  * Create Fulfillment Order Action Sample
  * Request for Amazon to send items from the merchant's inventory to a
  * destination address.
  *   
  * @param Amazon_FWSOutbound_Interface $service instance of Amazon_FWSOutbound_Interface
  * @param mixed $request Amazon_FWSOutbound_Model_CreateFulfillmentOrder or array of parameters
  */
  function invokeCreateFulfillmentOrder(Amazon_FWSOutbound_Interface $service, $request) 
  {
      try {
              $response = $service->createFulfillmentOrder($request);
              
                echo ("Service Response\n");
                echo ("=============================================================================\n");

                echo("        CreateFulfillmentOrderResponse\n");
                if ($response->isSetResponseMetadata()) { 
                    echo("            ResponseMetadata\n");
                    $responseMetadata = $response->getResponseMetadata();
                    if ($responseMetadata->isSetRequestId()) 
                    {
                        echo("                RequestId\n");
                        echo("                    " . $responseMetadata->getRequestId() . "\n");
                    }
                } 

     } catch (Amazon_FWSOutbound_Exception $ex) {
         echo("Caught Exception: " . $ex->getMessage() . "\n");
         echo("Response Status Code: " . $ex->getStatusCode() . "\n");
         echo("Error Code: " . $ex->getErrorCode() . "\n");
         echo("Error Type: " . $ex->getErrorType() . "\n");
         echo("Request ID: " . $ex->getRequestId() . "\n");
         echo("XML: " . $ex->getXML() . "\n");
	 echo($data);
     }
 }
